/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MockApplet.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.61 $
 */

package net.sf.darwin.user.applet;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JApplet;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.OptionMap;
import net.sf.darwin.api.base.OptionSource;
import net.sf.darwin.api.base.OptionSource_Applet;
import net.sf.darwin.api.base.Timed;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.Version;
import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.OptionsPanel;
import net.sf.darwin.ui.base.Painter;
import net.sf.darwin.ui.base.VisualizationException;
import net.sf.darwin.ui.base.VisualizationFactory;
import net.sf.darwin.ui.base.VisualizationModel;
import net.sf.darwin.ui.impl.ControlPanel_Default;
import net.sf.darwin.ui.impl.Messages;
import net.sf.darwin.ui.impl.VisualizerTabs;
import net.sf.darwin.ui.impl.swing.ControlPanel_;
import net.sf.darwin.ui.impl.swing.EvolutionaryApplet;
import net.sf.darwin.ui.impl.swing.OptionMap_Applet;
import net.sf.darwin.ui.impl.swing.Title;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * The minimum workable size for width for this applet is 625. The height should
 * be around 400.
 * 
 * @author Robin Hillyard
 */
public class Applet_Darwin extends JApplet {

	/**
	 * Public primary constructor.
	 */
	public Applet_Darwin() {
		super();
		this._title = new Title(true, "Darwin Client App"); //$NON-NLS-1$
		// XXX this was as follows:
		//		this._title = new Title(true, Messages.getString("Applet_Darwinian.0") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
		this._optionSource = new OptionSource_Applet(this);
		final Map<String, Object> optionMap = new HashMap<String, Object>();
		optionMap.put(BeanContainer.OPT_GENERATIONS, Integer.valueOf(0));
		this._optionMap = new OptionMap_Applet(optionMap, this);
	}

	/**
	 * Method to add a visualization model to this applet. Note that the
	 * visualization models are kept in a list until the visualizer tabs have
	 * been set up. At that point, the method {@link #addVisualizers()} is
	 * invoked.
	 * 
	 * As a side-effect, we set the component property of the visualizationModel
	 * to be <code>this</code>.
	 * 
	 * @param visualizationModel
	 */
	public void addVisualizationModel(final VisualizationModel visualizationModel) {
		getVisualizationModels().add(visualizationModel);
		// FIXME -- figure out what this is supposed to do...
		// visualizationModel.setComponent(this);
	}

	/**
	 * shut down the evolution.
	 * 
	 * TODO we should also break down all of the user interface components.
	 * 
	 * @see java.applet.Applet#destroy()
	 */
	@Override
	public void destroy() {
		LOG.info("Destroying " + getTitle() + " applet"); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			if (getEvolution() != null && getEvolution().stoppable()) {
				final boolean ok = getEvolution().shutdown();
				if (LOG.isDebugEnabled())
					LOG.debug("destroy(): applet shut down evolution with result: " + ok); //$NON-NLS-1$
				else if (!ok)
					LOG.info("destroy(): applet shut down evolution but not cleanly."); //$NON-NLS-1$
			} else
				LOG.warn("destroy(): cannot shut down evolution"); //$NON-NLS-1$
		} catch (final EvolutionException e) {
			final String message = "Exception shutting down evolution";//$NON-NLS-1$
			// XXX should not need to warn.
			LOG.warn(message, e);
			throw new RuntimeException(message, e);
		}
		super.destroy();
	}

	/**
	 * @see com.rubecula.darwin.visualization.swing.Paints#getPainter()
	 */
	public Painter getPainter() {
		return getVisualizerTabs().getPainter();
	}

	/**
	 * @param parameterName
	 * @param defaultValue
	 * @return a String which is either the parameter value specified in the
	 *         applet parameter list, or if that is missing, then the default
	 *         value.
	 */
	public String getParameter(final String parameterName, final String defaultValue) {
		final String value = getParameter(parameterName);
		return value != null ? value : defaultValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.applet.Applet#getParameterInfo()
	 */
	@Override
	public String[][] getParameterInfo() {
		return PARAMETER_INFO;
	}

	@Override
	public final void init() {
		// First invoke the super-method.
		super.init();
		setup();
	}

	// /**
	// * {@inheritDoc}
	// *
	// * @see net.sf.darwin.api.base.OptionGetter#getOptions(java.util.Map)
	// */
	// @SuppressWarnings("boxing")
	// @Override
	// public OptionMap<String, Object> getOptions(final Map<String, Object>
	// defaults) {
	// try {
	// final OptionMap<String, Object> result = new OptionMap<String,
	// Object>(defaults, new OptionSource_Applet(this));
	// // FIXME should not need the rest of this method now.
	// final String optKey = BeanContainer.OPT_VALIDATE;
	// final String paramKey = APPLET_PARAM_VALIDATE;
	// updateOptionValue(result, optKey, paramKey);
	// result.put(BeanContainer.OPT_DTD, getParameter(APPLET_PARAM_DTD,
	// (String) defaults.get(BeanContainer.OPT_DTD)));
	// result.put(BeanContainer.OPT_CONFIG_FILE,
	// getParameter(APPLET_PARAM_BEANS, (String)
	// defaults.get(BeanContainer.OPT_CONFIG_FILE)));
	// result.put(BeanContainer.OPT_DEBUG,
	// getBooleanParameter(APPLET_PARAM_DEBUG, (Boolean)
	// defaults.get(BeanContainer.OPT_DEBUG)));
	// result.put(BeanContainer.OPT_SHOW_BEANS,
	// getBooleanParameter(APPLET_PARAM_SHOW_BEANS, (Boolean)
	// defaults.get(BeanContainer.OPT_SHOW_BEANS)));
	// return result;
	// } catch (final Exception e) {
	//			throw new RuntimeException("problem setting up applet options", e); //$NON-NLS-1$
	// }
	// }

	// /**
	// *
	// */
	// @SuppressWarnings("boxing")
	// protected Map<String, Object> getOptionsMap() {
	// final Map<String, Object> optionsMap = new HashMap<String, Object>();
	// final boolean validate = getBooleanParameter(APPLET_PARAM_VALIDATE,
	// false);
	// optionsMap.put(BeanContainer.OPT_VALIDATE, validate);
	// if (validate) {
	// final String dtd = getParameter(APPLET_PARAM_DTD);
	// if (dtd != null)
	// this._beanContainer.setValidate(true, new URL(dtd));
	// }
	// // set the filename for bean container configuration.
	// this._beanContainer.setConfigurationByResource(getClass(),
	// getParameter(APPLET_PARAM_BEANS, getDefaultBeansFilename()));
	//
	// // set the debug flag according to the applet parameter "debug"
	// this._beanContainer.setDebug(getBooleanParameter(APPLET_PARAM_DEBUG,
	// false));
	//
	// // this._beanContainer.doMain(this._beanContainer, new
	// // ArrayList<String>());
	//
	// // impose this as an external bean in the container.
	// this._beanContainer.imposeBean(BeanContainer.BEAN_APPLET, this);
	//
	// // Now we configure the bean container.
	// this._beanContainer.configure();
	//
	// if (getBooleanParameter(APPLET_PARAM_SHOW_BEANS, false))
	// this._beanContainer.showBeans();
	//
	// }

	/**
	 * @param title
	 * 
	 * @see javax.swing.JLabel#setText(java.lang.String)
	 */
	public void setAppletTitle(final String title) {
		getTitle().setText(title);
	}

	/**
	 * @param controlPanel
	 *            the controlPanel to set
	 */
	public void setControlPanel(final ControlPanel_ controlPanel) {
		this.controlPanel = controlPanel;
	}

	/**
	 * @param optionsPanel
	 *            the optionsPanel to set
	 */
	public void setOptionsPanel(final OptionsPanel optionsPanel) {
		this._optionsPanel = optionsPanel;
	}

	/**
	 * Update the system according to a user-interface property change. If the
	 * property relates to the evolution process itself, we invoke
	 * {@link #changeEvolution(String, Object)}. Otherwise, we invoke
	 * {@link #updateModel(String, Object)} and then {@link #repaint()}.
	 * 
	 * @see net.sf.darwin.ui.base.ControlAction#setProperty(java.lang.String,
	 *      java.lang.Object)
	 */
	public void setProperty(final String name, final Object value) throws VisualizationException {
		LOG.info("property update: " + name + "=" + value); //$NON-NLS-1$ //$NON-NLS-2$
		if (name.equalsIgnoreCase(ControlAction.TIME_DELAY))
			changeEvolution(name, value);
		else {
			updateModel(name, value);
			repaint();
		}
	}

	/**
	 * @see net.sf.darwin.ui.base.ControlAction#setState(java.lang.String)
	 */
	public void setState(final String state) {
		if (state.equalsIgnoreCase(ControlAction.NEXT)) {
			getEvolution().next();
			doCensus();
		} else if (state.equalsIgnoreCase(ControlAction.START))
			getEvolution().resume();
		else if (state.equalsIgnoreCase(ControlAction.STOP)) {
			getEvolution().pause();
			doCensus();
		} else if (state.equalsIgnoreCase(ControlAction.QUIT)) {
			setEvolution(null);
			this.stop();
			this.destroy();
		}
	}

	/**
	 * @see net.sf.darwin.ui.base.ControlAction#settable(java.lang.String)
	 */
	public boolean settable(final String propertyName) {
		if (getEvolution() != null && propertyName.equalsIgnoreCase(ControlAction.TIME_DELAY))
			return getEvolution().stoppable();
		return false;
	}

	/**
	 * @param visualizationFactory
	 *            the visualizationFactory to set
	 */
	public void setVisualizationFactory(final VisualizationFactory visualizationFactory) {
		this.visualizationFactory = visualizationFactory;
	}

	/**
	 * @param visualizerTabs
	 *            the visualizer tabs
	 * 
	 *            XXX consider defining this public in interface
	 */
	public void setVisualizerTabs(final VisualizerTabs visualizerTabs) {
		this.visualizerTabs = visualizerTabs;
	}

	/**
	 * Initialize the evolution.
	 * 
	 * @see java.applet.Applet#start()
	 */
	@Override
	public void start() {
		super.start();
		if (getEvolution() != null) {
			getEvolution().init();
			LOG.info(getTitle() + " applet started"); //$NON-NLS-1$
		} else
			LOG.warn("start(): evolution is unset"); //$NON-NLS-1$
	}

	/**
	 * Cleanup the evolution
	 * 
	 * @see java.applet.Applet#stop()
	 */
	@Override
	public void stop() {
		super.stop();
		if (getEvolution() != null) {
			getEvolution().cleanup();
			LOG.info(getTitle() + " applet stopped"); //$NON-NLS-1$
		}
	}

	/**
	 * This is required for double-buffering to work correctly.
	 * 
	 * @see java.awt.Container#update(java.awt.Graphics)
	 */
	@Override
	public void update(final Graphics graphics) {
		paint(graphics);
	}

	/**
	 * 
	 */
	void doCensus() {
		if (getEvolution() != null)
			for (final Evolvable evolvable : getEvolution().getEvolvableKeys()) {
				// TODO consider doing this via polymorphism
				if (evolvable instanceof Taxon) {
					final Taxon taxon = (Taxon) evolvable;
					taxon.censusMe(taxon.getCensusTaker(), getName());
				}
				// else do nothing
			}
	}

	/**
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 * 
	 *            XXX consider defining this public in interface
	 * @throws VisualizationException
	 */
	protected void addVisualizer(final String key, final VisualizationModel visualizationModel) throws VisualizationException {
		addVisualizer(key, visualizationModel, null);
	}

	/**
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 * @param icon
	 * @param tip
	 * 
	 *            XXX consider defining this public in interface
	 * @throws VisualizationException
	 */
	protected void addVisualizer(final String key, final VisualizationModel visualizationModel, final Icon icon, final String tip)
			throws VisualizationException {
		final VisualizerTabs tabs = getVisualizerTabs();
		if (tabs != null)
			tabs.addVisualizer(key, visualizationModel, icon, tip);
		else
			throw new DarwinException("Applet_Darwinian.addVisualizer(...): visualizer tabs not set up"); //$NON-NLS-1$
	}

	/**
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 * @param tip
	 * 
	 *            XXX consider defining this public in interface
	 * @throws VisualizationException
	 */
	protected void addVisualizer(final String key, final VisualizationModel visualizationModel, final String tip)
			throws VisualizationException {
		addVisualizer(key, visualizationModel, null, tip);
	}

	/**
	 * Method to add visualizers to this applet's visualizer tabs. The default
	 * implementation is to iterate through the list of visualization models in
	 * {@link #_visualizationModels} and for each individual visualization
	 * model, to take it's title as it's key and add a visualizer based on that
	 * key and that model. It's important to note that visualizers are looked up
	 * by events which note a change in a Visualizable, by keying on the
	 * visualizable's identifier. Therefore, it is expected that the title of
	 * the visualizable model corresponds to the identifier of the visualizable.
	 * 
	 * TODO consider that we may not have a 1:1 correspondence between
	 * visualizers and visualizable models.
	 * 
	 * May to be overridden by extenders.
	 * 
	 * @throws VisualizationException
	 */
	protected void addVisualizers() throws VisualizationException {
		for (final VisualizationModel visualizationModel : getVisualizationModels()) {
			addVisualizer(visualizationModel.getTitle(), visualizationModel);
		}
		getVisualizationModels().clear();
	}

	/**
	 * @param controlAction
	 *            the object to implement the control actions
	 * @return a newly created {@link ControlPanel_Default} object.
	 * 
	 *         TODO consider using factory class.
	 */
	@SuppressWarnings("static-method")
	protected JPanel createControlPanel(final ControlAction controlAction) {
		return new ControlPanel_Default(controlAction);
	}

	/**
	 * @return a new empty {@link JLabel}
	 */
	@SuppressWarnings("static-method")
	protected JLabel createEastPanel() {
		return new JLabel(EMPTY);
	}

	/**
	 * @param opaque
	 *            true if this panel should be opaque
	 * @return a the {@link #_title} with opaque property set accordingly.
	 */
	protected JPanel createNorthPanel(final boolean opaque) {
		getTitle().setOpaque(opaque);
		return getTitle();
	}

	/**
	 * @return {@link #getControlPanel()}.
	 * 
	 */
	protected JPanel createSouthPanel() {
		return getControlPanel();
	}

	/**
	 * @return a new empty {@link JLabel}
	 */
	@SuppressWarnings("static-method")
	protected JLabel createWestPanel() {
		return new JLabel(EMPTY);
	}

	/**
	 * @param parameterName
	 * @param defaultValue
	 * @return the value of the applet parameter specified or the default value.
	 * 
	 * @see #getParameter(String, String)
	 */
	protected boolean getBooleanParameter(final String parameterName, final boolean defaultValue) {
		return Boolean.valueOf(getParameter(parameterName, Boolean.valueOf(defaultValue).toString())).booleanValue();
	}

	/**
	 * @return the controlPanel
	 */
	protected JPanel getControlPanel() {
		return this.controlPanel;
	}

	/**
	 * TODO eliminate
	 * 
	 * Subclasses should override this as appropriate.
	 * 
	 * @return {@link #FILENAME_BEANS_XML}.
	 */
	@SuppressWarnings("static-method")
	protected String getDefaultBeansFilename() {
		return FILENAME_BEANS_XML;
	}

	/**
	 * @return the evolution
	 */
	protected EvolutionProxy getEvolution() {
		return this.evolution;
	}

	/**
	 * @return the optionsPanel
	 */
	protected OptionsPanel getOptionsPanel() {
		return this._optionsPanel;
	}

	/**
	 * @return {@link #_title}
	 */
	protected Title getTitle() {
		return this._title;
	}

	/**
	 * @return the current value of visualizationFactory.
	 * 
	 */
	protected VisualizationFactory getVisualizationFactory() {
		return this.visualizationFactory;

	}

	/**
	 * XXX consider defining this public in interface
	 * 
	 * @return the visualizerTabs
	 */
	protected VisualizerTabs getVisualizerTabs() {
		return this.visualizerTabs;
	}

	/**
	 * @param evolution
	 *            the evolution to set
	 */
	protected void setEvolution(final EvolutionProxy evolution) {
		this.evolution = evolution;
	}

	/**
	 * 
	 */
	protected void setupLayout() {
		setLayout(new BorderLayout());
		add(createNorthPanel(true), BorderLayout.NORTH);
		add(createWestPanel(), BorderLayout.WEST);
		add(createEastPanel(), BorderLayout.EAST);
		add(createSouthPanel(), BorderLayout.SOUTH);
	}

	/**
	 * As always when a user-interface action causes a change to the model which
	 * backs a widget, two things must happen: we must fire a change event so
	 * that other parts of the system know that the property has changed.
	 * Finally, we must repaint the user-interface in case something visual has
	 * changed. This step, however, is not the responsibility of this method (it
	 * is invoked by the method which calls this method).
	 * 
	 * @param name
	 *            the user-interface property that has changed.
	 * @param value
	 *            the new value.
	 * @throws VisualizationException
	 */
	protected void updateModel(final String name, final Object value) throws VisualizationException {
		// Get any bean property dependence on this property.
		// beanProperty should be of the form: beanIdentifier.propertyName
		final String beanProperty = getOptionsPanel().getDependentBeanProperties().get(name);
		if (beanProperty != null)
			try {
				final Object bean = null; // TODO replace
				// getBeanContainer().setBeanProperty(beanProperty,
				// value);
				LOG.info("Updated bean property: " + beanProperty + " to " + value); //$NON-NLS-1$//$NON-NLS-2$
				// TODO consider other types of object too
				// TODO consider doing this via polymorphism
				if (bean instanceof Environment)
					((Environment) bean).fireEnvironmentChanged();
				else if (bean instanceof EcoFactor)
					((EcoFactor) bean).getEnvironment().fireEnvironmentChanged();
			} catch (final Exception e) {
				throw new VisualizationException("setProperty exception for bean property: " + beanProperty, e); //$NON-NLS-1$
			}
	}

	/**
	 * @param name
	 * @param value
	 * @throws VisualizationException
	 */
	private void changeEvolution(final String name, final Object value) throws VisualizationException {
		final int rate = (int) Math.round(RATE_DETERMINANT / ((Number) value).doubleValue());
		try {
			// TODO consider doing this via polymorphism
			if (getEvolution() instanceof Timed)
				((Timed) getEvolution()).setRate(rate);
			else
				throw new VisualizationException("setProperty(): " + name + " evolution is does not support variable rate"); //$NON-NLS-1$//$NON-NLS-2$
		} catch (final EvolutionException e) {
			throw new VisualizationException("setProperty(): " + name + " exception while shutting down evolution", e); //$NON-NLS-1$//$NON-NLS-2$
		}
	}

	/**
	 * 
	 */
	private void connectWithServer() {
		// XXX Auto-generated method stub

	}

	/**
	 * @return {@link #_visualizationModels}
	 */
	private Collection<VisualizationModel> getVisualizationModels() {
		return this._visualizationModels;
	}

	/**
	 * Method which is called by invoked by {@link #init()}, after invoking the
	 * super-method.
	 */
	@SuppressWarnings("unchecked")
	private void setup() {
		setupLayout();
		connectWithServer();
		setAppletTitle("Applet Administration Console"); //$NON-NLS-1$
		// TODO check the following cast
		getOptionsPanel().setComponentMap(createOptionsComponentMap(), (Collection<? extends HasExpressions>) getBeans());
		add(getVisualizerTabs(), BorderLayout.CENTER);
		add(getControlPanel(), BorderLayout.SOUTH);
		setVisualizationDimensions(getVisualizationFactory());
		try {
			addVisualizers();
		} catch (final VisualizationException e) {
			LOG.warn("setup(): exception thrown", e); //$NON-NLS-1$
		}
	}

	/**
	 * @param vizFact
	 */
	private void setVisualizationDimensions(final VisualizationFactory vizFact) {
		vizFact.setHeight(getHeight());
		vizFact.setWidth(getWidth());
	}

	private static final String APPLET_PARAM_GEN = "generations"; //$NON-NLS-1$

	/**
	 * The number of milliseconds in one minute
	 */
	private static final int RATE_DETERMINANT = 60000;

	/**
	 * validate parameter name (defaults to false).
	 */
	private static final String APPLET_PARAM_VALIDATE = "validate"; //$NON-NLS-1$

	/**
	 * the parameter to specify a specific URL for validation.
	 */
	private static final String APPLET_PARAM_DTD = "dtd"; //$NON-NLS-1$

	/**
	 * debug
	 */
	private static final String APPLET_PARAM_DEBUG = "debug"; //$NON-NLS-1$

	private static final String APPLET_PARAM_SHOW_BEANS = "showBeans"; //$NON-NLS-1$

	private static final String APPLET_PARAM_BEANS = "beans"; //$NON-NLS-1$

	private static final long serialVersionUID = 6251221911709691733L;

	private static final String FILENAME_BEANS_XML = "beans.xml"; //$NON-NLS-1$

	/**
	 * Generation
	 */
	static final String GENERATION = Messages.getString("EvolutionaryApplet.0"); //$NON-NLS-1$

	private static final String EMPTY = ""; //$NON-NLS-1$

	/**
	 * TODO need to fix this. This URL is good for development:
	 * <code>file:../config/pepperedMoth.properties</code> But to deploy as an
	 * applet you will have to set the parameter {@link #PROPERTIES_URL} as an
	 * applet (embed) parameter in the HTML.
	 */
	protected static final String DEFAULT_PROPERTIES_URL = "pepperedMoth.properties"; //$NON-NLS-1$

	protected static final String PROPERTIES_URL = "PropertiesUrl"; //$NON-NLS-1$

	protected EvolutionProxy evolution;

	/**
	 * This is the control panel which allows the user to adjust settings.
	 */
	protected JPanel controlPanel;

	protected final transient Title _title;

	/**
	 * TitleApplication
	 */
	public static final String P_TITLE_APPLICATION = "TitleApplication"; //$NON-NLS-1$

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(EvolutionaryApplet.class);

	/**
	 * enableSlider
	 */
	protected static final String APPLET_PARAM_ENABLE_SLIDER = "enableSlider"; //$NON-NLS-1$

	private static final String APPLET_PARAM_JAR = "jar"; //$NON-NLS-1$

	@SuppressWarnings("nls")
	private static final String[][] PARAMETER_INFO = new String[][] { { "width", "Integer", "display width" },
			{ "height", "Integer", "display height" },
			{ APPLET_PARAM_GEN, "Integer", "the maximum number of generations", BeanContainer.OPT_GENERATIONS },
			{ APPLET_PARAM_ENABLE_SLIDER, "Boolean", "enable slide option" },
			{ APPLET_PARAM_SHOW_BEANS, "Boolean", "enable showing of beans", BeanContainer.OPT_SHOW_BEANS },
			{ APPLET_PARAM_VALIDATE, "Boolean", "enable validation", BeanContainer.OPT_VALIDATE },
			{ APPLET_PARAM_DEBUG, "Boolean", "enable debug", BeanContainer.OPT_DEBUG },
			{ APPLET_PARAM_DTD, "java.net.URL", "DTD URL", BeanContainer.OPT_DTD },
			{ APPLET_PARAM_JAR, "java.net.URL", "DTD URL" },
			{ APPLET_PARAM_BEANS, "String", "configuration file", BeanContainer.OPT_CONFIG_FILE } };

	private static final String LBL_VERSION = "Version"; //$NON-NLS-1$

	private static final String LBL_SHOW_IDENTIFIERS = "Show Identifiers"; //$NON-NLS-1$

	private final OptionSource<String> _optionSource;

	private final OptionMap<String, Object> _optionMap;

	private VisualizationFactory visualizationFactory;

	private final Collection<VisualizationModel> _visualizationModels = new ArrayList<VisualizationModel>();

	private VisualizerTabs visualizerTabs;

	private OptionsPanel _optionsPanel;

	/**
	 * Needs to be overridden by extenders.
	 * 
	 * @return the newly fabricated options component map
	 */
	private static Map<String, Component> createOptionsComponentMap() {
		final Map<String, Component> map = new HashMap<String, Component>();
		map.put(LBL_VERSION, new JLabel(Version.DARWIN_VERSION));
		map.put(LBL_SHOW_IDENTIFIERS, new JCheckBox());
		return map;
	}

	/**
	 * @return
	 */
	private static Collection<Object> getBeans() {
		final Collection<Object> beans = new ArrayList<Object>();
		// for (final String key : getBeanContainer().getBeanKeys())
		// beans.add(getBeanContainer().getBean(key));
		return beans;
	}

}
