/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: EvolutionProxy
 * Created on: Mar 8, 2012
 */

package net.sf.darwin.user.applet;

import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Evolvable;

/**
 * @author rhillya
 * 
 */
public class EvolutionProxy {

	/**
	 * 
	 */
	public void cleanup() {
		// XXX Auto-generated method stub

	}

	/**
	 * @return
	 */
	public Iterable<Evolvable> getEvolvableKeys() {
		// XXX Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public void init() {
		// XXX Auto-generated method stub

	}

	/**
	 * 
	 */
	public void next() {
		// XXX Auto-generated method stub

	}

	/**
	 * 
	 */
	public void pause() {
		// XXX Auto-generated method stub

	}

	/**
	 * 
	 */
	public void resume() {
		// XXX Auto-generated method stub

	}

	/**
	 * @return
	 */
	@SuppressWarnings("static-method")
	public boolean shutdown() throws EvolutionException {
		// XXX Auto-generated method stub
		return false;
	}

	/**
	 * @return
	 */
	public boolean stoppable() {
		// XXX Auto-generated method stub
		return false;
	}

}
