/**
 * Cryptobase Project.
 * Copyright (C) 2010  UnitedHealth Group
 *
 * Module: ClientUtilities.java
 * Created on Dec 9, 2010
 */

package net.sf.darwin.user.applet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Filter;

import javax.net.SocketFactory;

import net.sf.darwin.api.base.ClientServerProtocol;

/**
 * @author rhillya
 * 
 *         TODO strip out the Client class stuff, leaving only the
 *         {@link #main(String[])} method and its utilities.
 * 
 */
public class Client {
	/**
	 * 
	 */
	public Client() {
		this(null, true);
	}

	/**
	 * @param socketFactory
	 *            XXX
	 * @param lineOriented
	 *            XXX
	 * 
	 */
	public Client(final SocketFactory socketFactory, final boolean lineOriented) {
		super();
		this._lineOriented = lineOriented;
		this.setSocketFactory(socketFactory != null ? socketFactory : SocketFactory.getDefault());
	}

	/**
	 * @param sServer
	 * @param port
	 * @return the socket on which the connection was made
	 * @throws IOException
	 * @throws UnknownHostException
	 */
	public Socket connect(final String sServer, final int port) throws IOException, UnknownHostException {
		System.out.println("contacting " + sServer + " on port " + port); //$NON-NLS-1$ //$NON-NLS-2$
		final Socket socket = getSocketFactory().createSocket(sServer, port);
		System.out.println("connected with socket: " + socket); //$NON-NLS-1$
		return socket;
	}

	/**
	 * @param socket
	 * @param clientReader
	 * @param clientWriter
	 * @param preamble
	 * @return
	 * @throws IOException
	 */
	public String converseWithServer(final Socket socket, final Reader clientReader, final Writer clientWriter,
			final String preamble) throws IOException {
		final InputStreamReader reader = new InputStreamReader(socket.getInputStream());
		final ResponseProcessorString responseProcessor = new ResponseProcessorString(this._lineOriented ? new BufferedReader(
				reader) : reader, clientWriter);
		final Thread responseThread = new Thread(responseProcessor, "server"); //$NON-NLS-1$
		responseThread.start();
		final Thread userThread = new Thread(new UserProcessor(new BufferedReader(clientReader), new PrintWriter(
				socket.getOutputStream(), true), preamble), "user"); //$NON-NLS-1$
		userThread.start();
		try {
			responseThread.join();
			userThread.join();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
		// in.close();
		socket.close();
		return responseProcessor.getToken();
	}

	/**
	 * @param sServer
	 *            the String form of the URL of the server
	 * @param port
	 *            the port number to which we connect
	 * @param clientReader
	 *            a source of commands which will be processed by the user
	 *            processor and, thence, sent to the server. Typically, reader
	 *            is connected to the console and therefore provides commands
	 *            entered by the user. An alternative would be strings provided
	 *            for unit testing.
	 * @param clientWriter
	 *            the writer which connects typically to the console, but could
	 *            also connect with a string writer for unit testing
	 * @param filter
	 *            a filter which is passed in to the response processor
	 * @param preamble
	 *            this is passed into the user processor.
	 * @return the token that was provided by the server.
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public String converseWithServer(final String sServer, final int port, final Reader clientReader, final Writer clientWriter,
			final Filter filter, final String preamble) throws UnknownHostException, IOException {
		return converseWithServer(connect(sServer, port), clientReader, clientWriter, preamble);
	}

	/**
	 * @return the socketFactory
	 */
	public SocketFactory getSocketFactory() {
		return this._socketFactory;
	}

	/**
	 * @param socketFactory
	 *            the socketFactory to set
	 */
	public void setSocketFactory(final SocketFactory socketFactory) {
		this._socketFactory = socketFactory;
	}

	/**
	 * 
	 */
	public static final String DEAD = "Dead"; //$NON-NLS-1$

	private SocketFactory _socketFactory;

	final boolean _lineOriented;

	private final boolean alive = true;

	/**
	 * 
	 */
	public static final String TOKEN = "Token"; //$NON-NLS-1$

	private static boolean debug = false;

	/**
	 * @author rhillya
	 * 
	 */
	static final class ResponseProcessorString implements Runnable {

		/**
		 * @param input
		 *            a reader based on the input stream which is connected to
		 *            the output of the socket which corresponds with the server
		 *            (this reader may be buffered or not)
		 * @param output
		 *            the writer which connects typically to the console, but
		 *            could also connect with a string writer for unit testing
		 * @param viable
		 *            an implementer of {@link Viable} which maintains the
		 *            viable status of which we use to control this processor
		 *            thread. Typically, it refers to the parent process (the
		 *            client itself).
		 * @param filter
		 *            a filter which operates on the input and determines the
		 *            appropriate action to take, such as copying the input to
		 *            the output.
		 * 
		 */
		public ResponseProcessorString(final Reader input, final Writer output) {
			super();
			this._input = input;
			this._output = output;
		}

		/**
		 * TODO we don't do anything with the token yet.
		 * 
		 * @return the token
		 */
		public String getToken() {
			return this.token;
		}

		@Override
		public void run() {
			try {
				while (true) {
					final String str = getInputString(this._input);
					if (str != null) {
						debug("received from server: " + str.length() + " chars: \"", str + "\""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					} else
						break;
				}
			} catch (final IOException e) {
				System.err.println("client response processor exception: " + e.getLocalizedMessage()); //$NON-NLS-1$
			} finally {
				// setAlive(false);
			}
		}

		/**
		 * Method to get some input as a String.
		 * 
		 * If the reader is a BufferedReader, then a line will be returned
		 * (including its terminating newline), otherwise a String of up to 2048
		 * chars which may or may not include a newline character.
		 * 
		 * @param reader
		 *            the input reader
		 * @return a String or null if there was no input available.
		 * @throws IOException
		 */
		private String getInputString(final Reader reader) throws IOException {
			final char[] buffer = new char[2048];
			if (reader instanceof BufferedReader) {
				final String result = ((BufferedReader) reader).readLine();
				if (result != null)
					return result;
				return null;
			}
			final int read = reader.read(buffer);
			if (read > 0)
				return new String(buffer, 0, read);
			return null;
		}

		/**
		 * @param tok
		 *            the token to set
		 */
		private void setToken(final String tok) {
			this.token = tok;
		}

		private final Reader _input;

		private final Writer _output;

		private String token;
	}

	/**
	 * @author rhillya
	 * 
	 */
	static final class UserProcessor implements Runnable {

		/**
		 * @param reader
		 *            a source of commands which will be sent to the server.
		 *            Typically, reader is connected to the console and
		 *            therefore provides commands entered by the user.
		 * @param writer
		 *            the writer to which we pass on the commands. Typically,
		 *            this connects to the input stream of the socket which
		 *            corresponds with the server.
		 * @param viable
		 *            an implementer of {@link Viable} which maintains the
		 *            viable status of which we use to control this processor
		 *            thread. Typically, it refers to the parent process (the
		 *            client itself).
		 * @param preamble
		 *            if this is not null, it is written to the writer (with a
		 *            newline attached) before looping.
		 * 
		 */
		public UserProcessor(final BufferedReader reader, final PrintWriter writer, final String preamble) {
			super();
			this._reader = reader;
			this._writer = writer;
			this._preamble = preamble;
			this.ok = true;
		}

		@Override
		public void run() {
			if (this._preamble != null)
				this._writer.println(this._preamble);
			while (this.ok) {
				try {
					if (this._reader.ready()) {
						final String userInput = this._reader.readLine();
						if (userInput != null) {
							this._writer.println(userInput);
							debug("received from user: " + userInput.length() + " chars: \"", userInput + "\""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						}
					}
					try {
						Thread.sleep(100);
					} catch (final InterruptedException e) {
						// XXX OK to eat this exception
					}
				} catch (final Exception e) {
					System.err.println("ClientUtilities$UserProcessor thread has ended due to the following exception"); //$NON-NLS-1$
					e.printStackTrace();
					return;
				}
			}
			try {
				this._writer.close();
				this._reader.close();
			} catch (final IOException e) {
				e.printStackTrace();
				return;
			}
		}

		private final boolean ok;

		private final String _preamble;

		private final BufferedReader _reader;

		private final PrintWriter _writer;
	}

	/**
	 * @return the debug
	 */
	public static boolean isDebug() {
		return debug;
	}

	/**
	 * @param args
	 */
	public static void main(final String[] args) {

		String sServer = "localhost"; //$NON-NLS-1$
		int port = 9001;
		int arg = 0;
		boolean ssl = false;
		while (arg < args.length) {
			final String argument = args[arg];
			if (arg == 0 && argument.startsWith("s")) {//$NON-NLS-1$
				ssl = true;
				port++;
				System.out.println("using SSL"); //$NON-NLS-1$
			}
			if (arg == 1 && argument != null)
				sServer = argument;
			if (arg == 2 && argument != null)
				port = Integer.parseInt(argument);
			arg++;
		}

		try {
			final InputStreamReader reader = new InputStreamReader(System.in);
			final OutputStreamWriter writer = new OutputStreamWriter(System.out);
			switch (port) {
			case 9001:
			case 9002:
				new ClientUtilities(SocketFactory.getDefault(), true).converseWithServer(sServer, port, reader, writer,
						ClientServerProtocol.GREETING_CLIENT);
				break;

			default:
			}
		} catch (final IOException e) {
			System.err.println("conversation I/O problem: "); //$NON-NLS-1$
			e.printStackTrace(System.err);
		}
	}

	/**
	 * @param dbg
	 *            the debug to set
	 */
	public static void setDebug(final boolean dbg) {
		Client.debug = dbg;
	}

	/**
	 * Output should occur only when in debug mode
	 * 
	 * @param prefix
	 * @param string
	 */
	static void debug(final String prefix, final String string) {
		if (isDebug())
			System.out.println(prefix + string);
	}
}
