/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: RouteFitness.java
 * Created on Apr 1, 2009
 * @version $Revision: 1.21 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.Fitness_;
import net.sf.darwin.api.impl.FitnessFunction_PseudoPoisson;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Trait;

import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.Evaluator;

/**
 * Application-specific Fitness implementation for the Traveling Salesman
 * problem.
 * 
 * @author Robin Hillyard
 * 
 */
/**
 * @author Robin Hillyard
 * 
 */
public class RouteFitness extends Fitness_ implements HasExpressions {

	/**
	 * 
	 */
	public RouteFitness() {
		this((Evaluator) null);
	}

	/**
	 * TODO use a real function listener (instead of null)
	 * 
	 * @param evaluator
	 * 
	 */
	public RouteFitness(final Evaluator evaluator) {
		super(new FitnessFunction_PseudoPoisson(evaluator), null);
	}

	/**
	 * @return the adjustment
	 */
	public double getAdjustment() {
		return adjustment;
	}

	/**
	 * @return the client factor as set by {@link #setClientFactor(double)}.
	 */
	public double getClientFactor() {
		return clientFactor;
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Fitness#getEnvironmentFactor()
	 */
	@Override
	public Number getEnvironmentFactor() {
		return environmentFactor;
	}

	/**
	 * @see com.rubecula.darwin.foundation.HasExpressions#getExpressions()
	 */
	@Override
	public ExpressionMap getExpressions() {
		final ExpressionMap result = new ExpressionMap();
		final FitnessFunction fitnessFunction = getFitnessFunction();
		if (fitnessFunction instanceof EvalExpressionMutable) {
			result.addExpression("Fitness: Route / Travel Time", (EvalExpressionMutable) fitnessFunction); //$NON-NLS-1$			
		}
		return result;
	}

	/**
	 * Calculate the fitness for the trait and the factor.
	 * 
	 * @throws FitnessException
	 * 
	 * @see com.rubecula.darwin.domain.helper.Fitness#getFitness(com.rubecula.darwin.domain.helper.Trait,
	 *      com.rubecula.darwin.domain.helper.EcoFactor)
	 */
	@Override
	public double getFitness(final Trait trait, final EcoFactor factor) throws FitnessException {
		// TODO consider doing this via polymorphism
		if (trait instanceof Quantifiable) {
			final double travelTime = ((Quantifiable) trait).doubleValue();
			if (factor instanceof EcoFactor_Clients) {
				return calculateFitness(travelTime, ((EcoFactor_Clients) factor).getRadius(), trait.getIdentifier(),
						factor.getIdentifier());
			}
			return 0;
		}
		return 0;
	}

	/**
	 * @return the scale property.
	 */
	public double getScale() {
		return scale;
	}

	/**
	 * @return the timeToTravel property.
	 */
	public double getTimeToTravel() {
		return timeToTravel;
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Fitness#getWeight(String,
	 *      com.rubecula.darwin.domain.helper.EcoFactor)
	 */
	@Override
	public int getWeight(final String character, final EcoFactor factor) {
		if (character.equals(FACTOR_TRAVEL_TIME)) {
			// TODO consider doing this via polymorphism
			if (factor instanceof EcoFactor_Clients) {
				return 1;
			}
			return 0;
		}
		return 0;
	}

	/**
	 * Scale adjustment field by value of factor (if not null).
	 * 
	 * @see com.rubecula.darwin.domain.fitness.Fitness_#resetAdjustment(java.lang.Number,
	 *      String)
	 */
	@Override
	public void resetAdjustment(final Number factor, final String trait) {
		if (factor != null && FACTOR_TRAVEL_TIME.equals(trait))
			this.adjustment *= factor.doubleValue();
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Fitness#resetEnvironmentFactor(String,
	 *      EcoFactor)
	 */
	@Override
	public void resetEnvironmentFactor(final String trait, final EcoFactor factor) {
		if (FACTOR_TRAVEL_TIME.equals(trait) && factor instanceof EcoFactor_Clients)
			environmentFactor = Double.valueOf(((EcoFactor_Clients) factor).getClients().size() / getClientFactor() + 1);
		adjustment = 1;
	}

	/**
	 * This is a factor which adjusts the for the number of clients in the
	 * route. The more clients, the less perfect our route is likely to be so we
	 * adjust here. The formula for the adjustment is:
	 * <code>clientFactor / (N + clientFactor)</code> where <code>N</code> is
	 * the number of clients.
	 * 
	 * An appropriate value is around 10 or 12. The higher the value, the closer
	 * the adjustment comes to 1.0 and the lower the fitness values for routes
	 * with many clients. When fitness values are low, more organisms are culled
	 * and we have therefore a smaller gene pool to evolve.
	 * 
	 * OTOH, if this value is too small, the more likely the time to travel the
	 * route will actually be too good (less then the expected) possibly
	 * favoring some routes that take longer than other routes.
	 * 
	 * @param clientFactor
	 */
	public void setClientFactor(final double clientFactor) {
		this.clientFactor = clientFactor;
	}

	/**
	 * Set the approximate size of the radius of points of interest in the same
	 * distance units as used in the client map.
	 * 
	 * @param scale
	 */
	public void setScale(final double scale) {
		this.scale = scale;
	}

	/**
	 * Set the time to travel a unit distance, where time is in the same units
	 * as the times-to-travel eco factor and the distance units are the same as
	 * those used for the client positions.
	 * 
	 * @param timeToTravel
	 */
	public void setTimeToTravel(final double timeToTravel) {
		this.timeToTravel = timeToTravel;
	}

	/**
	 * @return 0.5.
	 * 
	 * @see com.rubecula.darwin.domain.fitness.Fitness_#bandwidth(String)
	 */
	@Override
	protected double bandwidth(final String key) {
		return 0.5;
	}

	/**
	 * Calculate fitness according to the pseudo-Poisson distribution function.
	 * The value used is <code>travelTime/radius/pi</code>, scaled down by the
	 * value of {@link #getTimeToTravel()} which is in theory the time travel
	 * one unit of distance and also by (nClients/ {@link #getClientFactor()} +
	 * 1) .
	 * 
	 * The target (an estimate of the perfect value) is 1 scaled down by
	 * {@link #getScale()}.
	 * 
	 * The shape factor is the result of calling {@link #bandwidth(String)}.
	 * 
	 * Ideally, the target should be higher than and very close to 1.0; the
	 * value should be slightly greater than the target;
	 * 
	 * If the values are inappropriate, you can adjust them by changing the time
	 * to travel setting, the scale setting, the client factor setting, or the
	 * bandwidth value.
	 * 
	 * If the resulting fitness values are too low, the population will suffer.
	 * 
	 * The fitness calculated and returned will be:
	 * <ul>
	 * <li>NaN if value is less than target (in which case a warning will be
	 * given);</li>
	 * <li>very close to 1.0 if the value is just slightly greater than the
	 * target</li>
	 * <li>around 0.63 if the value is greater than the target by the value of
	 * {@link #bandwidth(String)}.</li>
	 * </ul>
	 * 
	 * @param travelTime
	 * @param radius
	 * @param traitId
	 * @param factorId
	 * @return the fitness, as evaluated by
	 *         {@link #calculateFitness(double, double, String, String)} with
	 *         travelTime/radius/pi/adjustment for the first parameter.
	 * @throws FitnessException
	 */
	@Override
	protected double calculateFitness(final double travelTime, final double radius, final String traitId, final String factorId)
			throws FitnessException {
		final double factor = getEnvironmentFactor() != null ? getEnvironmentFactor().doubleValue() : 1;
		return super.calculateFitness(travelTime / radius / Math.PI / factor, getAdjustment(), traitId, factorId);

	}

	/**
	 * Scale down the factor by the "scale". This is a fudge factor to bring the
	 * comparison with the target to an appropriate value. It depends on the
	 * kind of route problem being solved. The factor might be different for a
	 * mailman versus a fedex man for example.
	 * 
	 * If you see warnings "Fitness calculation issue...", try increasing the
	 * scale (which will result in a smaller return value from this method).
	 * 
	 * @see com.rubecula.darwin.domain.fitness.Fitness_#scaleFactor(String)
	 */
	@Override
	protected double scaleFactor(final String key) {
		if (key.equals(EcoFactor_Clients.FACTOR_CLIENTS)) {
			if (getScale() != 0)
				return 1 / getScale();
			throw new DarwinException("RouteFitness.scaleFactor(): scale property must be non-zero"); //$NON-NLS-1$			
		}
		return 1;
	}

	/**
	 * Scale down the travel time by the "timeToTravel".
	 * 
	 * 
	 * @see com.rubecula.darwin.domain.fitness.Fitness_#scaleTrait(java.lang.String)
	 */
	@Override
	protected double scaleTrait(final String key) {
		if (key.equals(FACTOR_TRAVEL_TIME)) {
			if (getTimeToTravel() != 0)
				return 1.0 / getTimeToTravel();
			throw new DarwinException("RouteFitness.scaleTrait(): timeToTravel property must be non-zero"); //$NON-NLS-1$
		}
		return 1;
	}

	/**
	 * 
	 */
	private static final String FACTOR_TRAVEL_TIME = EcoFactor_TravelTimes.FACTOR_TRAVEL_TIME;

	/**
	 * Distance scale factor. THIS MUST BE SET to something sensible. Generally
	 * speaking, it should be approximately the radius of the circle that best
	 * approximates the points (clients) to be visited, in whatever units the
	 * point coordinates are defined in.
	 */
	private double scale = 0; // distance units

	/**
	 * Time scale factor. THIS MUST BE SET to something sensible. Generally
	 * speaking, it should be approximately the expected time (in the same units
	 * in which the travel times are expressed) to travel one of the distance
	 * units referenced above in property {@link #scale}. For example, if you
	 * are driving a car mainly on highways, and your travel times are in
	 * minutes and your client coordinates are in miles, then a value of
	 * something like 1.2 is appropriate.
	 */
	private double timeToTravel = 0; // in time units

	private double clientFactor = 12;

	private double adjustment = 1;

	private Number environmentFactor;
}