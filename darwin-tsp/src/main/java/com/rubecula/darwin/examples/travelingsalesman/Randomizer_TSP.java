/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Randomizer_TSP.java
 * Created on Jul 1, 2009
 * @version $Revision: 1.7 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.impl.Randomizer;

import org.apache.commons.math.random.RandomGenerator;

/**
 * @author Robin Hillyard
 * 
 */
public class Randomizer_TSP extends Randomizer {

	/**
	 * @param random
	 */
	public Randomizer_TSP(final RandomGenerator random) {
		super(random);
	}

	/**
	 * @param random
	 * @param bands
	 */
	public Randomizer_TSP(final RandomGenerator random, final int[] bands) {
		super(random, bands);
	}

	/**
	 * @param random
	 * @param bands
	 * @param reseed
	 */
	public Randomizer_TSP(final RandomGenerator random, final int[] bands, final boolean reseed) {
		super(random, bands, reseed);
	}

}
