/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mutator_TSP.java
 * Created on Mar 3, 2007
 * @version $Revision: 1.35 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.impl.Mutator_Random;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Locus;

import org.apache.commons.math.random.RandomGenerator;

/**
 * @author Robin Hillyard
 * 
 */
public class Mutator_TSP extends Mutator_Random<Number> {

	/**
	 * @param random
	 */
	public Mutator_TSP(final RandomGenerator random) {
		super(random);
	}

	/**
	 * @param random
	 * @param odds
	 */
	public Mutator_TSP(final RandomGenerator random, final int odds) {
		super(random, odds);
	}

	/**
	 * @param random
	 * @param bands
	 */
	public Mutator_TSP(final RandomGenerator random, final int[] bands) {
		super(random, bands);
	}

	/**
	 * @see com.rubecula.darwin.domain.genetics.Mutator_Random#mutate(com.rubecula.darwin.domain.helper.Allele)
	 */
	@Override
	public Allele<Number> mutate(final Allele<Number> allele) {
		if (getRandom().nextIndex() == 1)
			return mutateAllele(allele);
		return allele;
	}

	/**
	 * Mutate the given genome. If the randomizer gets a hit (i.e. a mutation is
	 * called for), the next value from the randomizer determines whether to add
	 * a new gene (values 0-3) or to remove the current gene (value 4).
	 * 
	 * @see com.rubecula.darwin.domain.genetics.Mutator_#mutate(com.rubecula.darwin.domain.helper.Genome)
	 */
	@Override
	public <U> Genome<U, Number> mutate(final Genome<U, Number> genome) {
		final Genome<U, Number> mutant = super.mutate(genome);
		final ListIterator<Gene<Number>> geneIterator = mutant.listIterator();
		while (geneIterator.hasNext()) {
			final Gene<Number> gene = geneIterator.next();
			final int bandRandom = getRandom().nextIndex();
			if (bandRandom == 2)
				doInsertionDeletion(geneIterator, gene, mutant.size(), 3, 10);
			else if (bandRandom == 1)
				doAlleleSwitch(geneIterator, gene);
			// else do nothing
		}

		return mutant;
	}

	/**
	 * Iterate through the genome, removing pairs of complementary genes.
	 * Normally, the caller will invoke the method again if this method returns
	 * true, otherwise, it will consider that all work has been performed and no
	 * further simplification is possible.
	 * 
	 * @return true if the genome has been simplified.
	 * 
	 * @see com.rubecula.darwin.domain.genetics.Mutator_#simplify(com.rubecula.
	 *      darwin.domain.helper.Genome, Expresser<E, U>)
	 */
	@Override
	public <U> boolean simplify(final Genome<U, Number> genome, final Map<Locus<Number>, Expresser<U>> expresserMap) {
		final boolean changed = removeNoopGenes(genome);
		final Collection<Gene<Number>> complimentaries = findComplementaryGenes(genome, expresserMap);
		if (removeComplementaryGenes(genome, complimentaries) || changed)
			return true;

		// in case there were no changes, we will try to invoke changes with the
		// super-method
		return super.simplify(genome, expresserMap);
	}

	/**
	 * @param geneIterator
	 * @param gene
	 */
	private void doAlleleSwitch(final ListIterator<Gene<Number>> geneIterator, final Gene<Number> gene) {
		// final Allele mutant = mutateAllele(gene.getAllele(0));
		final Allele<Number> mutant = mutate(gene.getAllele(0));
		final Gene<Number> replacement = GeneFactory.makeHaploid(gene.getLocus(), mutant.getIdentifier());
		if (LOG.isDebugEnabled())
			LOG.debug("replacing " + gene + " by " + replacement); //$NON-NLS-1$ //$NON-NLS-2$
		geneIterator.remove();
		geneIterator.add(replacement);
	}

	/**
	 * Do either an replication of a gene or the deletion of a gene. In general,
	 * replication is twice as probably as deletion. However, deletion never
	 * brings the total number of genes below minSize and replication never
	 * brings the total number of genes above maxSize.
	 * 
	 * @param geneIterator
	 * @param gene
	 * @param genomeSize
	 *            XXX
	 * @param minSize
	 *            XXX
	 * @param maxSize
	 *            XXX
	 */
	private void doInsertionDeletion(final ListIterator<Gene<Number>> geneIterator, final Gene<Number> gene,
			final int genomeSize, final int minSize, final int maxSize) {
		final int r = getRandom().nextInt(3);
		if (r == 2) {
			if (genomeSize > minSize) {
				if (LOG.isDebugEnabled())
					LOG.debug("mutate(): deletion of " + gene); //$NON-NLS-1$
				geneIterator.remove();
			}
		} else {
			if (genomeSize < maxSize) {
				final Locus<Number> locus = gene.getLocus();
				if (locus != null) {
					if (LOG.isDebugEnabled())
						LOG.debug("mutate(): replication of " + gene); //$NON-NLS-1$
					geneIterator.add(GeneFactory.makeHaploid(locus, gene.getAllele(0).getIdentifier()));
				} else
					throw new DarwinException("mutate(): locus not set"); //$NON-NLS-1$
			}
		}
	}

	/**
	 * @param genome
	 * @param expresserMap
	 *            TODO
	 * @return a collection of genes each of which is the first in a pair of
	 *         complementary genes.
	 */
	private <U> Collection<Gene<Number>> findComplementaryGenes(final Genome<U, Number> genome,
			final Map<Locus<Number>, Expresser<U>> expresserMap) {
		final Collection<Gene<Number>> complimentaries = new ArrayList<>();
		Gene<Number> prior = null;
		final ListIterator<Gene<Number>> geneIterator = genome.listIterator();
		while (geneIterator.hasNext()) {
			final Gene<Number> gene = geneIterator.next();
			if (prior != null && prior.getLocus().equals(gene.getLocus())) {
				if (isComplementary(prior, gene, expresserMap.get(gene.getLocus()))) {
					complimentaries.add(prior);
					prior = null;
				} else
					prior = gene;
			} else
				prior = gene;
		}
		return complimentaries;
	}

	/**
	 * Remove an instances of the swap gene "stay" (whose value is 0).
	 * 
	 * @param genome
	 * @return
	 */
	private boolean removeNoopGenes(final Genome genome) {
		final Iterator<Gene> iterator = genome.listIterator();
		while (iterator.hasNext()) {
			final Gene gene = iterator.next();
			final Locus locus = gene.getLocus();
			// TODO consider doing this via polymorphism
			if (locus instanceof Locus_TSP && ((Locus_TSP) locus).isSwap()) {
				final Allele allele = gene.getAllele(0);
				if (allele instanceof Allele_Number) {
					if (((Allele_Number) allele).getIntegerValue() == 0)
						iterator.remove();
				}
			}
		}
		return false;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2133766534125475807L;

	/**
	 * @param allele
	 * @return
	 */
	private static Allele<Number> mutateAllele(final Allele<Number> allele) {
		final Object value = allele.getValue();
		// TODO consider doing this via polymorphism
		if (value instanceof Number) {
			final int number = ((Number) value).intValue();
			final Allele<Number> newAllele = mutateAllele(allele, number);
			final Allele<Number> result = newAllele;
			result.setLocus(allele.getLocus());
			addToLocusIfUnknown(result);
			if (LOG.isDebugEnabled())
				LOG.debug("Allele mutation from: " + allele + " to " + result); //$NON-NLS-1$//$NON-NLS-2$
			return result;
		}
		throw new DarwinException("Mutator_TSP: unsuitable allele: " + allele); //$NON-NLS-1$
	}

	/**
	 * @param allele
	 * @param number
	 * @return
	 */
	private static Allele<Number> mutateAllele(final Allele<Number> allele, final int number) {
		final String key = allele.getIdentifier();
		if (key.equals(Allele_Number.BACKWARD))
			return Allele_Number.makeNumberAllele(Allele_Number.FORWARD, -number);
		if (key.equals(Allele_Number.FORWARD))
			return Allele_Number.makeNumberAllele(Allele_Number.BACKWARD, -number);
		if (key.equals(Allele_Number.STAY))
			return Allele_Number.makeNumberAllele(Allele_Number.NEXT, 0);
		if (key.equals(Allele_Number.NEXT))
			return Allele_Number.makeNumberAllele(Allele_Number.STAY, 1);
		return Allele_Number.makeNumberAllele("mutant", -number); //$NON-NLS-1$
	}

	/**
	 * @param genome
	 * @param complimentaries
	 * @return
	 */
	private static <E, U> boolean removeComplementaryGenes(final Genome<U, Number> genome,
			final Collection<Gene<Number>> complimentaries) {
		boolean changes = false;
		for (final Gene<Number> gene : complimentaries) {
			final ListIterator<Gene<Number>> iterator = genome.listIterator();
			while (iterator.hasNext()) {
				if (iterator.next() == gene) {
					changes = true;
					iterator.remove();
					if (iterator.hasNext()) {
						iterator.next();
						iterator.remove();
					} else
						throw new DarwinException("Mutator.simplify(): logic error"); //$NON-NLS-1$
				}
			}
		}
		return changes;
	}
}
