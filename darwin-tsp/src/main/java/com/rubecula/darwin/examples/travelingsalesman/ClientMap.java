/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ClientMap.java
 * Created on Jul 4, 2009
 * @version $Revision: 1.9 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.darwin.core.Environment;

/**
 * This is a map of all the clients.
 * 
 * @author Robin Hillyard
 * 
 *         TODO consider extending AToString
 * 
 */
public class ClientMap {

	/**
	 * Primary Constructor
	 * 
	 * @param environment
	 */
	public ClientMap(final Environment environment) {
		super();
		_environment = environment;
		_map = new HashMap<String, Client>();
	}

	/**
	 * @param key
	 * @param value
	 * @return the previous client for the given key (or null)
	 */
	public Client addClient(final String key, final Client value) {
		final Client result = _map.put(key, value);
		updateEnvironment();
		return result;
	}

	/**
	 * @return the map as set by {@link #setMap(Map)}.
	 */
	public Map<String, Client> getMap() {
		return _map;
	}

	/**
	 * @param map
	 */
	public void setMap(final Map<String, Client> map) {
		_map.clear();
		for (final Entry<String, Client> e : map.entrySet()) {
			final Client client = e.getValue();
			client.setIdentifier(e.getKey());
			_map.put(e.getKey(), client);
		}
		// for (final String key : map.keySet()) {
		// final Client client = map.get(key);
		// client.setIdentifier(key);
		// _map.put(key, client);
		// }
		updateEnvironment();
	}

	/**
	 * XXX consider making this private
	 */
	public void updateEnvironment() {
		_environment.update(EcoFactor_Clients.FACTOR_CLIENTS, _map);
	}

	Environment getEnvironment() {
		return _environment;
	}

	private final Environment _environment;

	private final Map<String, Client> _map;

}
