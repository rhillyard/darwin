/**
 * RUBECULA COMMON UTILITIES.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Swap.java
 * Created on Jun 27, 2009
 * @version $Revision: 1.1 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

/**
 * @author Robin Hillyard
 * 
 */
public interface Swap<T> {

	/**
	 * Swap the first (0th) element of underlying elements of the iterator with
	 * the indexth element.
	 * 
	 * @param index
	 */
	public abstract void swap(int index);

}
