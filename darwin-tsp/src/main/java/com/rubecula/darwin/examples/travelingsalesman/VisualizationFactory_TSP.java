/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizationFactory_TSP.java
 * Created on Apr 22, 2007
 * @version $Revision: 1.14 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.awt.Color;
import java.awt.Point;
import java.util.Map;

import net.sf.darwin.core.Individual;
import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.base.VisualizationFactory;

/**
 * @author Robin Hillyard
 * 
 *         TODO consider extending AToString
 */
public class VisualizationFactory_TSP implements VisualizationFactory {

	/**
	 * @param width
	 * @param height
	 */
	public VisualizationFactory_TSP(final int width, final int height) {
		setWidth(width);
		setHeight(height);
	}

	/**
	 * @return the height
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/**
	 * @return the width
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/**
	 * @see com.rubecula.darwin.visualization.VisualizationFactory#makeAvatar(com.rubecula.darwin.foundation.Individual,
	 *      java.util.Map)
	 */
	@Override
	public Avatar makeAvatar(final Individual individual, final Map<String, Object> properties) {
		return null;
	}

	/**
	 * @see com.rubecula.darwin.visualization.VisualizationFactory#makeColor(com.rubecula.darwin.foundation.Individual)
	 */
	@Override
	public Color makeColor(final Individual individual) {
		return null;
	}

	/**
	 * @see com.rubecula.darwin.visualization.VisualizationFactory#makeLocation(com.rubecula.darwin.foundation.Individual)
	 */
	@Override
	public Point makeLocation(final Individual individual) {
		return null;
	}

	/**
	 * @see com.rubecula.darwin.visualization.VisualizationFactory#setHeight(int)
	 */
	@Override
	public void setHeight(final int height) {
		this.height = height;
	}

	/**
	 * @see com.rubecula.darwin.visualization.VisualizationFactory#setWidth(int)
	 */
	@Override
	public void setWidth(final int width) {
		this.width = width;
	}

	private int width;

	private int height;

}
