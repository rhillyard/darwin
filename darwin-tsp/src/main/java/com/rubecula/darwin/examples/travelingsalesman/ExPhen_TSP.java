/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExPhen_TSP.java
 * Created on Jul 13, 2009
 * @version $Revision: 1.16 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.Iterator;

import net.sf.darwin.api.base.ExPhen_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExPhen;

/**
 * @author Robin Hillyard
 * 
 */
public class ExPhen_TSP extends ExPhen_ {

	/**
	 * @param factor
	 * @param value
	 */
	public ExPhen_TSP(final EcoFactor factor, final Object value) {
		super(factor, value);
	}

	/**
	 * @return the Route
	 */
	public Route getRoute() {
		// TODO consider doing this via polymorphism
		if (getValue() instanceof Route) {
			final Route route = (Route) getValue();
			return route;
		}
		throw new DarwinException("logic error: value not a Route"); //$NON-NLS-1$
	}

	/**
	 * @return true if the travel time for the environment is less than the
	 *         given best time.
	 * @see com.rubecula.darwin.domain.environment.ExPhen_#evaluate(Object)
	 */
	@Override
	protected boolean evaluate(final Object bestTime) {
		if (bestTime instanceof Number) {
			final double newTravelTime = getRoute().getTravelTime(
					(EcoFactor_TravelTimes) getEcoFactor().getEnvironment().getEcoSystem()
							.getFactor(EcoFactor_TravelTimes.FACTOR_TRAVELTIMES));
			// TODO make this debug only
			LOG.info("ExPhen.evaluate: new travel time=" + newTravelTime + ", best time=" + bestTime); //$NON-NLS-1$//$NON-NLS-2$
			if (newTravelTime < ((Number) bestTime).doubleValue())
				return true;
			return false;
		}
		throw new DarwinException("evaluate(): criterion is not a Number: " + bestTime.getClass()); //$NON-NLS-1$
	}

	/**
	 * @return a new instance of EcoFactor_Clients which has the various clients
	 *         belonging to the {@link Route} which is the value of this
	 *         {@link ExPhen}.
	 */
	@Override
	protected EcoFactor getRevisedFactor() {
		final EcoFactor_Clients revisedFactor = new EcoFactor_Clients();
		final Iterator<Client> iterator = getRoute().iterator();
		while (iterator.hasNext())
			revisedFactor.addClient(iterator.next());
		return revisedFactor;
	}

	/**
	 * @param factor
	 *            the updated eco factor
	 * @param value
	 *            the value of this ExPhen
	 */
	@Override
	protected void postProcess(final EcoFactor factor, final Object value) {
		final double newTravelTime = ((Route) value).getTravelTime((EcoFactor_TravelTimes) factor.getEnvironment().getEcoSystem()
				.getFactor(EcoFactor_TravelTimes.FACTOR_TRAVELTIMES));
		ExPhen_.LOG.info("new travel time is: " + newTravelTime); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8171635370945456726L;

}
