/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Painter_TSP.java
 * Created on Apr 22, 2007
 * @version $Revision: 1.7 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.awt.Graphics;

import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.base.Painter;
import net.sf.darwin.ui.base.VisualizationModel;

/**
 * @author Robin Hillyard
 * 
 */
public class Painter_TSP implements Painter {

	/**
	 * 
	 */
	public Painter_TSP() {
		this("no title"); //$NON-NLS-1$
	}

	/**
	 * @param title
	 */
	public Painter_TSP(final String title) {
		super();
		_title = title;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return _title;
	}

	/**
	 * @see com.rubecula.darwin.visualization.Painter#isIdentify()
	 */
	@Override
	public boolean isIdentify() {
		return false;
	}

	/**
	 * @see com.rubecula.darwin.visualization.Painter#paintBase(java.awt.Graphics,
	 *      int, int)
	 */
	@Override
	public void paintBase(final Graphics g, final int width, final int height) {
		// do nothing
	}

	/**
	 * @see com.rubecula.darwin.visualization.Painter#paintIndividual(java.awt.Graphics,
	 *      com.rubecula.darwin.visualization.Avatar)
	 */
	@Override
	public void paintIndividual(final Graphics g, final Avatar avatar) {
		// do nothing
	}

	/**
	 * @see com.rubecula.darwin.visualization.Painter#paintVisualization(com.rubecula.darwin.visualization.VisualizationModel,
	 *      java.awt.Graphics, int, int)
	 */
	@Override
	public void paintVisualization(final VisualizationModel visualizationModel, final Graphics g, final int width,
			final int height) {
		// do nothing
	}

	/**
	 * @see com.rubecula.darwin.visualization.Painter#setIdentify(boolean)
	 */
	@Override
	public void setIdentify(final boolean identify) {
		// do nothing
	}

	private final String _title;

}
