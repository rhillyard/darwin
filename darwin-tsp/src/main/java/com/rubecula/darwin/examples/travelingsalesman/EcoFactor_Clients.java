/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoFactor_Clients.java
 * Created on Feb 8, 2007
 * @version $Revision: 1.22 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.core.EcoFactor;

/**
 * This eco factor represents the clients that currently need to be visited and
 * their two-dimensional locations.
 * 
 * As new clients are added, or current clients are removed, the environment of
 * course changes and selection favors the new fastest route.
 * 
 * @author Robin Hillyard
 * 
 */
public final class EcoFactor_Clients extends EcoFactor_TSP {

	/**
	 * Construct an eco factor for the traveling salesman problem.
	 */
	public EcoFactor_Clients() {
		super(FACTOR_CLIENTS, new HashMap<Integer, Client>());
	}

	/**
	 * @see com.rubecula.darwin.domain.environment.EcoFactor_#clone()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		final EcoFactor_Clients clone = (EcoFactor_Clients) super.clone();
		clone.forceValue(((HashMap<Integer, Client>) clone.getValue()).clone());
		return clone;
	}

	/**
	 * Get a specific client from the given order.
	 * 
	 * @param i
	 * @return the ith client.
	 */
	public Client getClient(final int i) {
		return getClients().get(Integer.valueOf(i));
	}

	/**
	 * @return the list of clients in no particular order.
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, Client> getClients() {
		return (Map<Integer, Client>) getValue();
	}

	/**
	 * TODO consider renaming since this is not a pure getter.
	 * 
	 * @return the radius of the circle which best fits the clients.
	 */
	public double getRadius() {
		if (this.radius == 0.0) {
			final Collection<Client> values = getClients().values();
			int xTotal = 0;
			int yTotal = 0;
			int total = 0;
			for (final Client client : values) {
				xTotal += client.getX();
				yTotal += client.getY();
				total++;
			}
			final double xCenter = (1.0 * xTotal) / total;
			final double yCenter = (1.0 * yTotal) / total;

			double totalDistance = 0;
			for (final Client client : values) {
				final double x = client.getX() - xCenter;
				final double y = client.getY() - yCenter;
				final double distance = Math.sqrt(x * x + y * y);
				totalDistance += distance;
			}

			this.radius = totalDistance / total;
		}
		return this.radius;
	}

	/**
	 * @return the number of stops (clients) in this {@link EcoFactor}.
	 */
	public int getStops() {
		return getClients().size();
	}

	/**
	 * Move all of the clients at positions pos or higher up by one. Put the
	 * given client at position pos.
	 * 
	 * @param pos
	 * @param client
	 */
	public void moveClient(final int pos, final Client client) {
		final Map<Integer, Client> clients = getClients();
		for (int i = clients.size() - 2; i >= pos; i--)
			clients.put(Integer.valueOf(i + 1), clients.get(Integer.valueOf(i)));
		clients.put(Integer.valueOf(pos), client);
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Attribute_#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(final Object value) {
		if (!getValue().equals(value))
			forceValue(value);
	}

	/**
	 * @param client
	 * @return true if the client was indeed added (as opposed to replacing one)
	 *         [should always return true].
	 */
	boolean addClient(final Client client) {
		final Map<Integer, Client> clients = getClients();
		final Client put = clients.put(Integer.valueOf(clients.size()), client);
		this.radius = 0.0;
		return put == null;
	}

	/**
	 * @param value
	 */
	private void forceValue(final Object value) {
		super.setValue(value);
		this.radius = 0.;
		if (LOG.isDebugEnabled())
			LOG.debug("EcoFactor_Clients:setValue(): " + value); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5126070171373848121L;

	/**
	 * clients
	 */
	public static final String FACTOR_CLIENTS = "clients"; //$NON-NLS-1$

	private transient double radius = 0.0;

}
