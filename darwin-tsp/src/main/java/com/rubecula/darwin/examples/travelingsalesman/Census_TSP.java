/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_TSP.java
 * Created on Feb 8, 2007
 * @version $Revision: 1.27 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.io.PrintWriter;

import net.sf.darwin.api.base.Census_Darwinian;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Colony;

/**
 * @author Robin Hillyard
 * 
 */
public class Census_TSP extends Census_Darwinian {

	/**
	 * @param printWriter
	 */
	public Census_TSP(final PrintWriter printWriter) {
		super(printWriter);
	}

	/**
	 * @see com.rubecula.darwin.domain.world.Census_Darwinian#census(Censusible,
	 *      int, java.lang.Object)
	 */
	@Override
	public void census(final Censusible individual, final int depth, final Object context) {
		// TODO check: & consider doing this via polymorphism
		if (individual instanceof Colony) {
			final Colony colony = (Colony) individual;
			final String message = Messages.getString("Census_TSP.0") + COLON + colony.getIdentifier() + COLON + Messages.getString("Census_TSP.1") + COLON //$NON-NLS-1$ //$NON-NLS-2$
					+ colony.getCount() + " in context: " + context; //$NON-NLS-1$
			getPrintWriter().println(message);
		} else
			super.census(individual, depth, context);
	}

	private static final String COLON = ": "; //$NON-NLS-1$

}
