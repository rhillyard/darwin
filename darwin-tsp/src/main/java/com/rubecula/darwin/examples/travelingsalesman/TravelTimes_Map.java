/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TravelTimes_Map.java
 * Created on May 5, 2009
 * @version $Revision: 1.8 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.Map;

import net.sf.darwin.core.Base;
import net.sf.tostring0.ToString;

/**
 * @author Robin Hillyard
 * 
 */
public final class TravelTimes_Map extends Base implements TravelTimes {

	/**
	 * 
	 */
	public TravelTimes_Map() {
		super();
	}

	/**
	 * @see com.rubecula.darwin.examples.travelingsalesman.TravelTimes#getTravelTime(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public Integer getTravelTime(final String from, final String to) {
		final Integer fromTo = mapLookup(from, to);
		if (fromTo != null)
			return fromTo;
		return mapLookup(to, from);
	}

	/**
	 * @return the {@link #travelTimes} as set by {@link #setTravelTimes(Map)}.
	 * 
	 */
	@ToString
	public Map<String, Map<String, Integer>> getTravelTimes() {
		return this.travelTimes;
	}

	/**
	 * @param travelTimes
	 */
	public void setTravelTimes(final Map<String, Map<String, Integer>> travelTimes) {
		this.travelTimes = travelTimes;
	}

	/**
	 * @param from
	 * @param to
	 * @return
	 */
	private Integer mapLookup(final String from, final String to) {
		final Map<String, Integer> map = getTravelTimes().get(from);
		if (map != null)
			return map.get(to);
		return null;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6762553313968839233L;

	private Map<String, Map<String, Integer>> travelTimes;

}
