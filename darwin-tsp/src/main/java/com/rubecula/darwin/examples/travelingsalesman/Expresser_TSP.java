/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser_TSP.java
 * Created on Mar 4, 2009
 * @version $Revision: 1.35 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

import net.sf.darwin.api.base.Expresser_;
import net.sf.darwin.api.factory.ExPhenFactory;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Expression;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;

/**
 * @author Robin Hillyard
 * 
 */
public final class Expresser_TSP extends Expresser_<Number> {

	/**
	 * @param character
	 * @param ignored
	 *            TODO
	 * 
	 */
	public Expresser_TSP(final Pharacter<Number> character, final Number ignored) {
		super(character, ignored);
	}

	/**
	 * TODO this is apparently called more often than necessary
	 * 
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.Expresser_#express(net.sf.darwin.core.Colony,
	 *      net.sf.darwin.core.Gene[])
	 */
	@Override
	public <E, V> Collection<Expression<Number>> express(final Colony<E, Number, V> colony, final Gene<V>... genes) {
		final Collection<Expression<Number>> result = new ArrayList<>();
		final Environment<E> environment = colony.getEnvironment();
		final EcoFactor<E> factor = environment.getEcoSystem().getFactor(EcoFactor_Clients.FACTOR_CLIENTS);
		// TODO consider doing this via polymorphism
		if (factor instanceof EcoFactor_Clients) {
			final Route route = createRoute((EcoFactor_Clients) factor, genes);
			result.add(ExPhenFactory.makeExtendedPhenotype(ExPhen_TSP.class, factor, route));
			final double travelTime = route.getTravelTime((EcoFactor_TravelTimes) environment.getEcoSystem().getFactor(
					EcoFactor_TravelTimes.FACTOR_TRAVELTIMES));
			if (LOG.isDebugEnabled())
				LOG.debug("express: genes " + showGenes(genes) + " -> " + route + " & time: " + travelTime); //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
			final Trait<Number> trait = TraitFactory.makeVariable(_character, travelTime);
			result.add(trait);
		}

		if (result.size() == 0)
			LOG.warn("Warning: no expressions expressed"); //$NON-NLS-1$

		return result;
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Expresser#express(com.rubecula.darwin.domain.helper.Gene[])
	 */
	@Override
	public Trait express(final Gene... genes) {
		// No traits are returned by expression (only ExPhen objects)
		return null;
	}

	/**
	 * In this implementation, if the locus is a swap, then we simply return the
	 * allele passed in because two identical swaps are complementary.
	 * Otherwise, we invoke the super-method.
	 * 
	 * @see com.rubecula.darwin.domain.environment.Expresser_#getComplement(com.rubecula.darwin.domain.helper.Locus,
	 *      java.lang.String)
	 */
	@Override
	public String getComplement(final Locus locus, final String allele) {
		// TODO consider doing this via polymorphism
		if (locus instanceof Locus_TSP && ((Locus_TSP) locus).isSwap())
			return allele;
		return super.getComplement(locus, allele);
	}

	/**
	 * In this implementation of isComplementary, there are two situations: swap
	 * or rotate. Two adjacent swaps are complementary if the alleles are equal.
	 * Two adjacent rotations are complementary if the sum of the number of
	 * places rotated is zero.
	 * 
	 * @see com.rubecula.darwin.domain.environment.Expresser_#isComplementary(com.rubecula.darwin.domain.helper.Locus,
	 *      java.lang.String, java.lang.String)
	 */
	@Override
	public boolean isComplementary(final Locus locus, final String allele1, final String allele2) {
		// TODO consider doing this via polymorphism
		if (locus instanceof Locus_TSP && ((Locus_TSP) locus).isSwap())
			return allele1.equals(allele2);
		final Allele alleleP = locus.getAllele(allele1);
		if (alleleP instanceof Allele_Number) {
			final Allele allele = locus.getAllele(allele2);
			if (allele instanceof Allele_Number)
				return (((Allele_Number) alleleP).getIntegerValue() + ((Allele_Number) allele).getIntegerValue()) == 0;
			throw new DarwinException("Allele_Number.isComplementary(Allele): allele not of type Allele_Number"); //$NON-NLS-1$
		}
		throw new DarwinException("Locus_TSP.isComplementary(String,String): alleleP not of type Allele_Number"); //$NON-NLS-1$
	}

	/**
	 * @param factor
	 * @param genes
	 * @return
	 */
	private Route createRoute(final EcoFactor_Clients factor, final Gene[] genes) {
		int swap = 0;
		int rotation = 0;
		if (LOG.isTraceEnabled())
			LOG.trace("Create Route from " + genes.length + " genes: "); //$NON-NLS-1$ //$NON-NLS-2$

		for (int i = 0; i < genes.length; i++) {
			final Gene gene = genes[i];
			if (LOG.isTraceEnabled())
				LOG.trace("   gene: " + gene); //$NON-NLS-1$
			final Allele allele = gene.getAllele(0);
			if (allele instanceof Allele_Number) {
				final int val = ((Allele_Number) allele).getIntegerValue();
				final Locus locus = gene.getLocus();
				if (locus instanceof Locus_TSP) {
					if (((Locus_TSP) locus).isSwap())
						swap += val;
					else
						rotation += val;
				} else
					LOG.warn("locus ignored: " + locus); //$NON-NLS-1$
			}
		}
		final ArrayList<Client> clientList = new ArrayList<>();
		for (final Integer key : new TreeSet<>(factor.getClients().keySet())) {
			clientList.add(factor.getClients().get(key));
		}
		final Route route = new Route(clientList);
		if (route.size() > 0) {
			if (LOG.isTraceEnabled())
				LOG.trace("create route: " + rotation + "/" + swap); //$NON-NLS-1$ //$NON-NLS-2$
			route.swap(rotation, swap);
		}
		return route;
	}

	private String showGenes(final Gene... genes) {
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < genes.length; i++) {
			result.append(genes[i].getBases());
			if (i < genes.length - 1)
				result.append(","); //$NON-NLS-1$
		}
		return result.toString();
	}

}
