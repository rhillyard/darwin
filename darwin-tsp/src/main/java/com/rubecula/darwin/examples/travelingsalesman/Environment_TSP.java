/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Environment_TSP.java
 * Created on Jul 4, 2009
 * @version $Revision: 1.16 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.Collection;
import java.util.Map;

import net.sf.darwin.api.base.Environment_Susceptible;
import net.sf.darwin.api.base.Sink_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Realm;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
public class Environment_TSP extends Environment_Susceptible implements TakesTravelTime {

	/**
	 * @param realm
	 */
	public Environment_TSP(final Realm realm) {
		super(realm);
	}

	/**
	 * @return the default route that goes to each client in turn or null if
	 *         there are insufficient clients
	 */
	public Route formRoute() {
		final EcoFactor_Clients clients = (EcoFactor_Clients) getClients();
		if (clients.getStops() > 2)
			return Route.createRoute(clients);
		return null;
	}

	/**
	 * @see com.rubecula.darwin.examples.travelingsalesman.TakesTravelTime#getTravelTime()
	 * @see #formRoute()
	 */
	@Override
	public double getTravelTime() {
		final Route route = formRoute();
		if (route != null)
			return route
					.getTravelTime((EcoFactor_TravelTimes) getEcoSystem().getFactor(EcoFactor_TravelTimes.FACTOR_TRAVELTIMES));
		return 0;
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Environment#init()
	 */
	@Override
	public void init() {
		// nothing to do
	}

	/**
	 * @see com.rubecula.darwin.domain.environment.Environment_#update(java.lang.String,
	 *      java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean update(final String factorKey, final Object source) {
		if (factorKey.equals(EcoFactor_Clients.FACTOR_CLIENTS)) {
			if (source instanceof Map)
				return doUpdate(((Map<String, Client>) source), (EcoFactor_Clients) getEcoSystem().getFactor(factorKey));
			return false;
		}
		return super.update(factorKey, source);
	}

	/**
	 * @return the eco factor name {@link EcoFactor_Clients#FACTOR_CLIENTS}
	 */
	EcoFactor getClients() {
		return getEcoSystem().getFactor(EcoFactor_Clients.FACTOR_CLIENTS);
	}

	/**
	 * This implementation forms the route from this {@link Environment_TSP} and
	 * logs its details.
	 * 
	 * @see com.rubecula.darwin.domain.environment.Environment_Susceptible#logUpdate()
	 */
	@Override
	protected void logUpdate() {
		final Route route = formRoute();
		LOG.info("updated route: " + route); //$NON-NLS-1$
		LOG.info("updated travel time: " + ((TakesTravelTime) this).getTravelTime()); //$NON-NLS-1$
	}

	/**
	 * This implementation updates the EcoFactor_Clients factor of the
	 * environment from the client map (which is what source actually is).
	 * 
	 * @see com.rubecula.darwin.domain.environment.Environment_Susceptible#updateFromSource(java.lang.Object)
	 */
	@Override
	protected boolean updateFromSource(final Object source) {
		if (source instanceof ClientMap)
			return update(EcoFactor_Clients.FACTOR_CLIENTS, ((ClientMap) source).getMap());
		throw new DarwinException("logic error: source is not a ClientMap"); //$NON-NLS-1$
	}

	/**
	 * @param clientMap
	 * @param factor
	 *            XXX
	 * @return true if we added a client.
	 */
	private boolean addOneClientToFactorMap(final Map<String, Client> clientMap, final EcoFactor_Clients factor) {

		final Collection<Client> values = clientMap.values();
		for (final Client client : values) {
			if (factor.getClients().containsValue(client))
				continue;
			final boolean updated = factor.addClient(client);
			if (updated) {
				Sink_.sinkOrLog("Added client: " + client, getSink(), LOG); //$NON-NLS-1$
			}
			return updated;
		}
		throw new DarwinException("updateEnvironment(): logic error"); //$NON-NLS-1$
	}

	/**
	 * @param clientMap
	 * @param factor
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean doUpdate(final Map<String, Client> clientMap, final EcoFactor_Clients factor) {
		boolean updated = false;
		while (((Map<Integer, Client>) factor.getValue()).size() < 3
				&& clientMap.size() > ((Map<Integer, Client>) factor.getValue()).size()) {
			updated |= addOneClientToFactorMap(clientMap, factor);
		}
		if (clientMap.size() > ((Map<Integer, Client>) factor.getValue()).size()) {
			updated |= addOneClientToFactorMap(clientMap, factor);
		}
		if (updated)
			fireEnvironmentChanged();
		return updated;
	}
}
