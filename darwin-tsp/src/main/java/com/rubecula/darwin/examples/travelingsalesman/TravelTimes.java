/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TravelTimes.java
 * Created on May 7, 2009
 * @version $Revision: 1.6 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

/**
 * @author Robin Hillyard
 * 
 */
public interface TravelTimes {

	/**
	 * @param from
	 * @param to
	 * @return the travel time, as an Integer, between from and to.
	 */
	public abstract Integer getTravelTime(String from, String to);

}