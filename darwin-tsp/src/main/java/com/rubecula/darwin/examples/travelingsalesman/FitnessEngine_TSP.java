/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngine_TSP.java
 * Created on Apr 29, 2009
 * @version $Revision: 1.13 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.FitnessEngine_;
import net.sf.darwin.core.Realm;

import com.rubecula.jexpression.Evaluator;

/**
 * @author Robin Hillyard
 * 
 */
public class FitnessEngine_TSP extends FitnessEngine_ {

	/**
	 * 
	 */
	public FitnessEngine_TSP() {
		this(null);
	}

	/**
	 * @param evaluator
	 * @param realm
	 */
	public FitnessEngine_TSP(final Evaluator evaluator, final Realm realm) {
		super(evaluator, realm);
	}

	/**
	 * @param realm
	 */
	public FitnessEngine_TSP(final Realm realm) {
		this(null, realm);
	}

	/**
	 * @param clientFactor
	 */
	public void setClientFactor(final double clientFactor) {
		_fitness.setClientFactor(clientFactor);
	}

	/**
	 * @param scale
	 */
	public void setScale(final double scale) {
		_fitness.setScale(scale);
	}

	/**
	 * @param idealSpeed
	 */
	public void setTimeToTravel(final double idealSpeed) {
		_fitness.setTimeToTravel(idealSpeed);
	}

	/**
	 * @see com.rubecula.darwin.domain.fitness.FitnessEngine_#registerFitnesses(com.rubecula.jexpression.Evaluator)
	 */
	@Override
	protected void registerFitnesses(final Evaluator evaluator) {
		_fitness = new RouteFitness(evaluator);
		putFitness("Time/Clients", _fitness); //$NON-NLS-1$
		getRealm().getFitnessCache().register(this);
	}

	private RouteFitness _fitness;

}
