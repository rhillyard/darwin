/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Route.java
 * Created on Jun 26, 2009
 * @version $Revision: 1.11 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.Collection;
import java.util.ListIterator;

import net.sf.darwin.core.DarwinException;

/**
 * Class to represent a route between {@link Client} objects. The underlying
 * representation is of a set of objects arranged in a circular list. Calling
 * {@link #listIterator(int)} gives us a route starting at a particular client
 * in the list and going around the circular route until we have visited all the
 * clients.
 * 
 * We can switch any two clients and that operation actually switches the
 * clients in the underlying list.
 * 
 * @author Robin Hillyard
 * 
 */
public class Route extends CircularLinkedList<Client> {

	/**
	 * 
	 */
	Route() {
		super();
	}

	/**
	 * @param c
	 */
	Route(final Collection<Client> c) {
		super(c);
	}

	/**
	 * @param clients
	 */
	Route(final EcoFactor_Clients clients) {
		this(clients.getClients().values());
	}

	/**
	 * @param factor
	 * @return the travel time for the given factor
	 */
	public double getTravelTime(final EcoFactor_TravelTimes factor) {
		if (size() > 1) {
			if (factor != null) {
				final TravelTimes travelTimes = factor.getTravelTimes();
				final ListIterator<Client> iterator = listIterator();
				boolean first = true;
				Client start = null;
				Client last = null;
				double result = 0;
				while (iterator.hasNext()) {
					final Client client = iterator.next();
					if (last != null) {
						result += getTravelTime(travelTimes, last, client);
					}
					last = client;
					if (first)
						start = client;
					first = false;
				}
				result += getTravelTime(travelTimes, last, start);
				return result;
			}
			throw new DarwinException("no travel times defined"); //$NON-NLS-1$
		}
		throw new DarwinException("getTravelTimes(): logic error: only one client"); //$NON-NLS-1$

	}

	/**
	 * @see com.rubecula.darwin.examples.travelingsalesman.CircularLinkedList#listIterator(int)
	 */
	@Override
	public ListIterator<Client> listIterator(final int index) {
		return super.listIterator(CircularLinkedList.usefulModulo(index, size()));
	}

	/**
	 * @param index1
	 * @param index2
	 */
	@SuppressWarnings("unchecked")
	public void swap(final int index1, final int index2) {
		final ListIterator<Client> listIterator = listIterator(index1);
		// TODO consider doing this via polymorphism
		if (listIterator instanceof Swap) {
			((Swap) listIterator).swap(index2);
		} else
			throw new DarwinException("Route.swap() logic error"); //$NON-NLS-1$
	}

	/**
	 * @param travelTimes
	 * @param last
	 * @param client
	 * @return
	 */
	private double getTravelTime(final TravelTimes travelTimes, final Client last, final Client client) {
		if (!client.equals(last)) {
			final Integer travelTime = travelTimes.getTravelTime(last.getIdentifier(), client.getIdentifier());
			if (travelTime != null)
				return travelTime.doubleValue();
			throw new DarwinException("getTravelTime(): no defined travel time from " + last + " to " + client); //$NON-NLS-1$//$NON-NLS-2$
		}
		throw new DarwinException("getTravelTime(): logic error (last=client): " + last); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7760999079265656981L;

	/**
	 * @param clients
	 * @return a newly instantiated Route based on <code>clients</code>.
	 */
	public static Route createRoute(final EcoFactor_Clients clients) {
		return new Route(clients);
	}

}
