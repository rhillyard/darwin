/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Client.java
 * Created on Feb 6, 2007
 * @version $Revision: 1.16 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.StringTokenizer;

import net.sf.darwin.api.base.EnvironmentNode;
import net.sf.darwin.core.Nameable;
import net.sf.tostring0.Identifiable;

/**
 * @author Robin Hillyard
 * 
 *         TODO consider extending AToString
 */
public class Client implements EnvironmentNode, Identifiable, Nameable, Comparable<Client> {

	/**
	 * @param name
	 * @param x
	 * @param y
	 */
	public Client(final String name, final int x, final int y) {
		super();
		_name = name;
		_x = x;
		_y = y;
	}

	/**
	 * For now, we sort clients according to the dictionary order of the
	 * identifier (see note...)
	 * 
	 * Note: this class has a natural ordering that is inconsistent with equals.
	 * 
	 * @param o
	 * @return the order as =1,0, or 1
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final Client o) {
		return getIdentifier().compareTo(o.getIdentifier());
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Client other = (Client) obj;
		if (_name == null) {
			if (other._name != null)
				return false;
		} else if (!_name.equals(other._name))
			return false;
		if (_x != other._x)
			return false;
		if (_y != other._y)
			return false;
		return true;
	}

	/**
	 * @return {@link #_name}.
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return _name;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return _x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return _y;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_name == null) ? 0 : _name.hashCode());
		result = prime * result + _x;
		result = prime * result + _y;
		return result;
	}

	/**
	 * @see com.rubecula.darwin.foundation.Nameable#setIdentifier(java.lang.String)
	 */
	@Override
	public void setIdentifier(final String name) {
		_name = name;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getIdentifier() + "@[" + _x + "," + _y + "]"; //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
	}

	private String _name; // not final because we have to be able to rename it

	private final int _x;

	private final int _y;

	/**
	 * @param clientString
	 *            the coordinates of the clinet in the form <code>x:y</code>
	 * @return a new Client formed from the <code>clientString</code>
	 */
	public static Client valueOf(final String clientString) {
		final StringTokenizer tokenizer = new StringTokenizer(clientString, ":"); //$NON-NLS-1$
		final int count = tokenizer.countTokens();
		if (count != 2)
			throw new RuntimeException("badly formed client string: " + clientString); //$NON-NLS-1$
		// Note that it wouldn't be a good idea to inline these next three!
		final String x = tokenizer.nextToken();
		final String y = tokenizer.nextToken();
		return new Client(null, Integer.parseInt(x), Integer.parseInt(y));
	}

}
