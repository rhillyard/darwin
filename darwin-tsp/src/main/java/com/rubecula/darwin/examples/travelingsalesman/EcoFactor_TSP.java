/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoFactor_TSP.java
 * Created on Aug 6, 2009
 * @version $Revision: 1.19 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.EcoFactor_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
public abstract class EcoFactor_TSP extends EcoFactor_ {

	/**
	 * @param identifier
	 * @param value
	 */
	protected EcoFactor_TSP(final String identifier, final Object value) {
		super(identifier, value);
	}

	/**
	 * @return false
	 * @see com.rubecula.darwin.foundation.Individual#isNew()
	 */
	@Override
	public boolean isNew() {
		return false;
	}

	/**
	 * @return false
	 * @see com.rubecula.darwin.foundation.Individual#isVisible()
	 */
	@Override
	public boolean isVisible() {
		return false;
	}

	/**
	 * @param other
	 *            the factor which will be used to update this factor
	 * @return true if this {@link EcoFactor} has been updated
	 * @see com.rubecula.darwin.domain.helper.EcoFactor#update(com.rubecula.darwin.domain.helper.EcoFactor)
	 */
	@Override
	public boolean update(final EcoFactor other) {
		if (getIdentifier().equals(EcoFactor_Clients.FACTOR_CLIENTS)) {
			// TODO consider doing this via polymorphism
			if (other instanceof EcoFactor_Clients) {
				setValue(((EcoFactor_Clients) other).getClients());
				LOG.info("updated ecoFactor: " + this); //$NON-NLS-1$
				return true;
			}
			throw new DarwinException("update(): factor is not an EcoFactor_Clients: " + other.getClass()); //$NON-NLS-1$
		}
		return false;
	}
}
