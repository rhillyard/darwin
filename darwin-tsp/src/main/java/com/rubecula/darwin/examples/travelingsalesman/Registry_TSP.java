/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Registry_PepperedMoth.java
 * Created on Apr 11, 2009
 * @version $Revision: 1.10 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Map;

import net.sf.darwin.api.base.Registry_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Organism;

/**
 * @author Robin Hillyard
 * 
 */
public class Registry_TSP extends Registry_<Number, Number, Number> {

	/**
	 * @param writer
	 *            the writer to which output will be sent.
	 * 
	 */
	public Registry_TSP(final PrintWriter writer) {
		super();
		this._writer = writer;
	}

	/**
	 * Note the births
	 * 
	 * @see com.rubecula.darwin.domain.world.Registry_#registerBirths(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerBirths(final Colony<Number, Number, Number> colony,
			final Collection<Organism<Number, Number, Number>> neonates) {
		final Map<Object, Integer> map = getOrganismFrequencyMap(neonates);
		this._writer.println("Births for population " + colony + //$NON-NLS-1$
				": " + map); //$NON-NLS-1$
	}

	private final PrintWriter _writer;

}
