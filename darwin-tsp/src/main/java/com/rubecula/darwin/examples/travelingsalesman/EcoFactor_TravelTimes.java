/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoFactor_TravelTimes.java
 * Created on May 5, 2009
 * @version $Revision: 1.11 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

/**
 * This eco factor represents the travel times between various clients. The
 * value of this factor changes only slowly (in the background) and in the
 * traveling salesman problem, doesn't change at all.
 * 
 * XXX we don't currently override clone here because the value,
 * {@link TravelTimes} is not normally mutable.
 * 
 * @author Robin Hillyard
 * 
 */
public final class EcoFactor_TravelTimes extends EcoFactor_TSP {

	/**
	 */
	public EcoFactor_TravelTimes() {
		this(new TravelTimes_Map());
	}

	/**
	 * @param travelTimes
	 */
	public EcoFactor_TravelTimes(final TravelTimes travelTimes) {
		super(FACTOR_TRAVELTIMES, travelTimes);
	}

	/**
	 * @return the travel times (i.e. result of calling {@link #getValue()}.
	 */
	public TravelTimes getTravelTimes() {
		return (TravelTimes) getValue();
	}

	/**
	 * @param travelTimes
	 */
	public void setTravelTimes(final TravelTimes travelTimes) {
		setValue(travelTimes);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7401876036141006331L;

	/**
	 * travelTimes
	 */
	public static final String FACTOR_TRAVELTIMES = "travelTimes"; //$NON-NLS-1$

	/**
	 * XXX investigate why this is different from {@link #FACTOR_TRAVELTIMES}.
	 */
	public static final String FACTOR_TRAVEL_TIME = "Travel Time"; //$NON-NLS-1$

}
