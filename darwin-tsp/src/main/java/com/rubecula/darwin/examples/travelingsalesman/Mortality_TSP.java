/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mortality_TSP.java
 * Created on Mar 3, 2007
 * @version $Revision: 1.14 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.Mortality_;

import org.apache.commons.math.random.RandomGenerator;

/**
 * @author Robin Hillyard
 * 
 */
public final class Mortality_TSP extends Mortality_ {

	/**
	 * @param random
	 * @param infantMortality
	 *            probability of infants dying immediately
	 */
	public Mortality_TSP(final RandomGenerator random, final double infantMortality) {
		super(random);
		this._infantMortality = infantMortality;

	}

	/**
	 * Method to calculate the mortality for an organism as 1 less the simple
	 * product of viabilityFactor, fitness and sustainabilityIndex. The
	 * viability factor (VF) = (1-IM)^(age+1) where IM is the infant mortality
	 * (20%). The sustainabilityIndex (SI) = 1 if saturation less than 1; else
	 * SI = (1 - saturation)^2.
	 * 
	 * @see com.rubecula.darwin.domain.helper.Mortality#calculateMortality(int,double,double)
	 */
	@Override
	public double calculateMortality(final int age, final double fitness, final double saturation) {
		// First calculate the sustainability index (the same for all organisms)
		final double sustainabilityIndex = Math.max((1 - saturation) * (1 - saturation), 1);

		// Next calculate the viability factor due to age of this organism
		final double viablityFactor = Math.pow((1 - this._infantMortality), age + 1);

		// Now multiply all factors together to get the overall viability
		final double viability = fitness * fitness * viablityFactor * sustainabilityIndex;

		// Now subtract from 1 to get the mortality
		final double result = 1 - viability;

		if (LOG.isTraceEnabled()) // this method gets called a lot so we'll
			// "guard" this line of code
			LOG.trace("calculate mortality: age=" + age + ", fitness=" + fitness + ", saturation=" + saturation + ": mortality is " + result); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		return result;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 877455325407683898L;

	/**
	 * 0.2
	 */
	public static final double $InfantMortalityDefault = 0.2;

	private final double _infantMortality;

}
