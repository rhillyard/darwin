/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Allele_Number.java
 * Created on Feb 6, 2007
 * @version $Revision: 1.12 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.Allele_;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.DarwinException;

/**
 * This class is at the heart of the solution to the traveling salesman problem.
 * An Allele_Number immediately follows an Allele
 * 
 * @author Robin Hillyard
 * 
 */
public class Allele_Number extends Allele_<Number> {

	/**
	 * @param key
	 * @param value
	 */
	public Allele_Number(final String key, final Number value) {
		super(key, value);
	}

	/**
	 * @return C for stay, G for next, A for backward, T for forward.
	 * @see com.rubecula.darwin.domain.helper.Basic#getBases()
	 */
	@Override
	public String getBases() {
		final String identifier = getIdentifier();
		int i = getIntegerValue();
		if (identifier.equals(BACKWARD))
			i = 2;
		else if (identifier.equals(FORWARD))
			i = 3;
		else if (identifier.equals("-1")) //$NON-NLS-1$
			i = 2;
		if (i < 0 || i > 3)
			throw new DarwinException("Allele_Number: cannot for bases"); //$NON-NLS-1$
		return getLocus().getAlphabet().substring(i, i + 1);
	}

	/**
	 * @return the value of this allele, as an int
	 */
	public int getIntegerValue() {
		return ((Integer) getValue()).intValue();
	}

	private static final long serialVersionUID = -2981774702790043985L;

	/**
	 * 
	 */
	static final String ROUTE_SWAP = "RouteSwap"; //$NON-NLS-1$

	/**
	 * 
	 */
	static final String ROUTE_ROTATION = "RouteRotation"; //$NON-NLS-1$

	/**
	 * forward
	 */
	public static final String FORWARD = "forward"; //$NON-NLS-1$

	/**
	 * backward
	 */
	public static final String BACKWARD = "backward"; //$NON-NLS-1$

	/**
	 * next
	 */
	public static final String NEXT = "next"; //$NON-NLS-1$

	/**
	 * stay
	 */
	public static final String STAY = "stay"; //$NON-NLS-1$

	/**
	 * @param type
	 * @param value
	 * @return the newly constructed Allele
	 */
	@SuppressWarnings("boxing")
	public static Allele<Number> makeNumberAllele(final String type, final int value) {
		return new Allele_Number(type, value);
	}
}
