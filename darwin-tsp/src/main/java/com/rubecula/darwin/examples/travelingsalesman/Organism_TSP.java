/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Organism_TSP.java
 * Created on Jan 2, 2010
 * @version $Revision: 1.6 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.HashMap;

import net.sf.darwin.api.impl.Organism_Asexual;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Organism;

/**
 * @author Robin Hillyard
 * 
 */
public final class Organism_TSP extends Organism_Asexual<Number, Number, Number> {

	/**
	 * unused constructor
	 */
	@SuppressWarnings("deprecation")
	private Organism_TSP() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1280366456286466683L;

	static final String P_STEPS = "steps"; // XXX fix //$NON-NLS-1$

	/**
	 * Method to create a new organism by seeding.
	 * 
	 * FIXME this does nothing with the geneParams.
	 * 
	 * @param i
	 * @param colony
	 *            XXX
	 * @return a newly seeded {@link Organism_Asexual} object
	 */
	public static Organism<Number, Number, Number> createOrganism(final int i, final Colony<Number, Number, Number> colony) {
		final HashMap<String, Object> geneParams = new HashMap<>();
		geneParams.put(Organism_TSP.P_STEPS, Integer.valueOf(i));
		return Organism_Asexual.<Number, Number, Number> seed(colony);
	}

}
