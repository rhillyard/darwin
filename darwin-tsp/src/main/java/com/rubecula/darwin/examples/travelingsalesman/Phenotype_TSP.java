/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Phenotype_TSP.java
 * Created on Feb 7, 2007
 * @version $Revision: 1.21 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.Phenotype_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Phenome;

/**
 * @author Robin Hillyard
 * 
 *         TODO this should really be generic and constructed by factory class
 */
public final class Phenotype_TSP extends Phenotype_ {

	/**
	 */
	public Phenotype_TSP() {
		super(null);
	}

	/**
	 * @param clients
	 *            array derived directly from the environment
	 * @return method to get the clients in their order of visitation.
	 */
	public Client[] getClientsInOrder(final Client[] clients) {
		final int[] order = getOrderOfClientVisits(clients);
		final Client[] result = new Client[clients.length];
		for (int i = 0; i < result.length; i++)
			result[i] = clients[order[i]];
		return result;
	}

	/**
	 * @param environment
	 * @return method to get the clients in their order of visitation.
	 */
	public Client[] getClientsInOrder(final Environment environment) {
		return getClientsInOrder(getClients(environment));
	}

	/**
	 * @param clients
	 * @return
	 */
	private int[] getOrderOfClientVisits(final Client[] clients) {
		final boolean[] visited = new boolean[clients.length];
		for (int i = 0; i < visited.length; i++)
			visited[i] = false;
		visited[0] = true; // this is home
		final int[] order = new int[visited.length];
		int visitedCount = 1;

		while (visitedCount < visited.length)
			visitedCount = processTraits(visited, order, visitedCount);

		return order;
	}

	/**
	 * @param visited
	 * @param order
	 * @param count
	 *            the current number of visited clients
	 * @return the new value of visitedCount
	 */
	private int processTraits(final boolean[] visited, final int[] order, final int visitedCount) {
		final int count = visitedCount;
		for (final Object key : getKeys()) {
			// FIXME restore this
			// final Variant variant = getTrait(key);
			// if (variant instanceof Trait_TS) {
			// _currentIndex += ((Trait_TS) variant).intValue();
			// _currentIndex = _currentIndex % visited.length;
			// while (visited[_currentIndex]) {
			// _currentIndex++;
			// _currentIndex = _currentIndex % visited.length;
			// }
			// visited[_currentIndex] = true;
			// order[count++] = _currentIndex;
			// }
		}
		// TODO restore the above lines and fix.
		if (count <= visitedCount)
			throw new DarwinException("PhenotypeRoute.processTraits(): visitedCount has not increased"); //$NON-NLS-1$
		return count;
	}

	/**
	 * @param phenome
	 * @param result
	 */
	@SuppressWarnings("unused")
	private void updateFittestInPhenome(final Phenome phenome, final double result) {
		final Object data = phenome.getData();
		if (data instanceof Double) {
			final Double fittest = (Double) data;
			if (result > fittest.doubleValue())
				phenome.setData(new Double(result));
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4702230551570243853L;

	/**
	 * TODO move this somewhere (it isn't related to Phenotype)
	 * 
	 * @param environment
	 * @return an array of Client objects which form the clients of this
	 *         environment.
	 */
	public static Client[] getClients(final Environment environment) {
		return ((EcoFactor_Clients) environment.getEcoSystem().getFactor(EcoFactor_Clients.FACTOR_CLIENTS)).getClients().values()
				.toArray(new Client[] {});
	}

	/**
	 * TODO move this somewhere (it isn't related to Phenotype)
	 * 
	 * @param environment
	 * @return an array of Client objects which form the clients of this
	 *         environment.
	 */
	public static String[] getNames(final Environment environment) {
		return ((EcoFactor_Clients) environment.getEcoSystem().getFactor(EcoFactor_Clients.FACTOR_CLIENTS)).getClients().keySet()
				.toArray(new String[] {});
	}

	// /**
	// * This is somewhat arbitrary - but it needs to be remembered from each
	// call
	// * of processTraits() to the next.
	// */
	// private final int _currentIndex = 0;

}
