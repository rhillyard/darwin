/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OptionsPanel_TSP.java
 * Created on Apr 22, 2007
 * @version $Revision: 1.11 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.awt.Component;
import java.util.Map;

import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.OptionsPanel_;

/**
 * @author Robin Hillyard
 * 
 */
public class OptionsPanel_TSP extends OptionsPanel_ {

	/**
	 * @param controlAction
	 */
	public OptionsPanel_TSP(final ControlAction controlAction) {
		super(controlAction);
	}

	/**
	 * @param controller
	 * @param componentMap
	 */
	public OptionsPanel_TSP(final ControlAction controller, @SuppressWarnings("unused") final Map<String, Component> componentMap) {
		this(controller);
	}

	/**
	 * @see com.rubecula.darwin.visualization.swing.OptionsPanel_#setComponentMap(java.util.Map)
	 */
	// @Override
	public void setComponentMap(final Map<String, Component> componentMap) {
		// XXX check that it's OK to do nothing
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6602781598355534985L;

}
