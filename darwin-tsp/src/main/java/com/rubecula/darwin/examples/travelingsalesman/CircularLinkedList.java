/**
 * RUBECULA COMMON UTILITIES.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: CircularLinkedList.java
 * Created on Jun 27, 2009
 * @version $Revision: 1.2 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.AbstractSequentialList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * @author Robin Hillyard
 * 
 *         Based on java.util.LinkedList which I would prefer to extend but it's
 *         full of private classes.
 */
public class CircularLinkedList<E> extends AbstractSequentialList<E> implements Deque<E>, Cloneable, java.io.Serializable {
	/**
	 * Constructs an empty list.
	 */
	public CircularLinkedList() {
		header.next = header.previous = header;
	}

	/**
	 * Constructs a list containing the elements of the specified collection, in
	 * the order they are returned by the collection's iterator.
	 * 
	 * @param c
	 *            the collection whose elements are to be placed into this list
	 * @throws NullPointerException
	 *             if the specified collection is null
	 */
	public CircularLinkedList(final Collection<? extends E> c) {
		this();
		addAll(c);
	}

	/**
	 * Appends the specified element to the end of this list.
	 * 
	 * <p>
	 * This method is equivalent to {@link #addLast}.
	 * 
	 * @param e
	 *            element to be appended to this list
	 * @return <tt>true</tt> (as specified by {@link Collection#add})
	 */
	@Override
	public boolean add(final E e) {
		addBefore(e, header);
		return true;
	}

	/**
	 * Inserts the specified element at the specified position in this list.
	 * Shifts the element currently at that position (if any) and any subsequent
	 * elements to the right (adds one to their indices).
	 * 
	 * @param index
	 *            index at which the specified element is to be inserted
	 * @param element
	 *            element to be inserted
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 */
	@Override
	public void add(final int index, final E element) {
		addBefore(element, (index == size ? header : entry(index)));
	}

	/**
	 * Appends all of the elements in the specified collection to the end of
	 * this list, in the order that they are returned by the specified
	 * collection's iterator. The behavior of this operation is undefined if the
	 * specified collection is modified while the operation is in progress.
	 * (Note that this will occur if the specified collection is this list, and
	 * it's nonempty.)
	 * 
	 * @param c
	 *            collection containing elements to be added to this list
	 * @return <tt>true</tt> if this list changed as a result of the call
	 * @throws NullPointerException
	 *             if the specified collection is null
	 */
	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return addAll(size, c);
	}

	/**
	 * Inserts all of the elements in the specified collection into this list,
	 * starting at the specified position. Shifts the element currently at that
	 * position (if any) and any subsequent elements to the right (increases
	 * their indices). The new elements will appear in the list in the order
	 * that they are returned by the specified collection's iterator.
	 * 
	 * @param index
	 *            index at which to insert the first element from the specified
	 *            collection
	 * @param c
	 *            collection containing elements to be added to this list
	 * @return <tt>true</tt> if this list changed as a result of the call
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 * @throws NullPointerException
	 *             if the specified collection is null
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		if (index < 0 || index > size)
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size); //$NON-NLS-1$ //$NON-NLS-2$
		final Object[] a = c.toArray();
		final int numNew = a.length;
		if (numNew == 0)
			return false;
		modCount++;

		final Entry<E> successor = (index == size ? header : entry(index));
		Entry<E> predecessor = successor.previous;
		for (int i = 0; i < numNew; i++) {
			final Entry<E> e = new Entry<E>((E) a[i], successor, predecessor);
			predecessor.next = e;
			predecessor = e;
		}
		successor.previous = predecessor;

		size += numNew;
		return true;
	}

	/**
	 * Inserts the specified element at the beginning of this list.
	 * 
	 * @param e
	 *            the element to add
	 */
	@Override
	public void addFirst(final E e) {
		addBefore(e, header.next);
	}

	/**
	 * Appends the specified element to the end of this list.
	 * 
	 * <p>
	 * This method is equivalent to {@link #add}.
	 * 
	 * @param e
	 *            the element to add
	 */
	@Override
	public void addLast(final E e) {
		addBefore(e, header);
	}

	/**
	 * Removes all of the elements from this list.
	 */
	@Override
	public void clear() {
		Entry<E> e = header.next;
		while (e != header) {
			final Entry<E> next = e.next;
			e.next = e.previous = null;
			e.element = null;
			e = next;
		}
		header.next = header.previous = header;
		size = 0;
		modCount++;
	}

	/**
	 * Returns a shallow copy of this <tt>LinkedList</tt>. (The elements
	 * themselves are not cloned.)
	 * 
	 * @return a shallow copy of this <tt>LinkedList</tt> instance
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() {
		CircularLinkedList<E> clone = null;
		try {
			clone = (CircularLinkedList<E>) super.clone();
		} catch (final CloneNotSupportedException e) {
			throw new InternalError();
		}

		// Put clone into "virgin" state
		clone.header = new Entry<E>(null, null, null);
		clone.header.next = clone.header.previous = clone.header;
		clone.size = 0;
		clone.modCount = 0;

		// Initialize clone with our elements
		for (Entry<E> e = header.next; e != header; e = e.next)
			clone.add(e.element);

		return clone;
	}

	/**
	 * Returns <tt>true</tt> if this list contains the specified element. More
	 * formally, returns <tt>true</tt> if and only if this list contains at
	 * least one element <tt>e</tt> such that
	 * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
	 * 
	 * @param o
	 *            element whose presence in this list is to be tested
	 * @return <tt>true</tt> if this list contains the specified element
	 */
	@Override
	public boolean contains(final Object o) {
		return indexOf(o) != -1;
	}

	/**
	 * @since 1.6
	 */
	@Override
	public Iterator<E> descendingIterator() {
		throw new UnsupportedOperationException("descendingIterator"); //$NON-NLS-1$
	}

	/**
	 * Retrieves, but does not remove, the head (first element) of this list.
	 * 
	 * @return the head of this list
	 * @throws NoSuchElementException
	 *             if this list is empty
	 * @since 1.5
	 */
	@Override
	public E element() {
		return getFirst();
	}

	/**
	 * Returns the element at the specified position in this list.
	 * 
	 * @param index
	 *            index of the element to return
	 * @return the element at the specified position in this list
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 */
	@Override
	public E get(final int index) {
		return entry(index).element;
	}

	/**
	 * Returns the first element in this list.
	 * 
	 * @return the first element in this list
	 * @throws NoSuchElementException
	 *             if this list is empty
	 */
	@Override
	public E getFirst() {
		if (size == 0)
			throw new NoSuchElementException();

		return header.next.element;
	}

	/**
	 * Returns the last element in this list.
	 * 
	 * @return the last element in this list
	 * @throws NoSuchElementException
	 *             if this list is empty
	 */
	@Override
	public E getLast() {
		if (size == 0)
			throw new NoSuchElementException();

		return header.previous.element;
	}

	/**
	 * Returns the index of the first occurrence of the specified element in
	 * this list, or -1 if this list does not contain the element. More
	 * formally, returns the lowest index <tt>i</tt> such that
	 * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
	 * or -1 if there is no such index.
	 * 
	 * @param o
	 *            element to search for
	 * @return the index of the first occurrence of the specified element in
	 *         this list, or -1 if this list does not contain the element
	 */
	@Override
	public int indexOf(final Object o) {
		int index = 0;
		if (o == null) {
			for (Entry<E> e = header.next; e != header; e = e.next) {
				if (e.element == null)
					return index;
				index++;
			}
		} else {
			for (Entry<E> e = header.next; e != header; e = e.next) {
				if (o.equals(e.element))
					return index;
				index++;
			}
		}
		return -1;
	}

	/**
	 * Returns the index of the last occurrence of the specified element in this
	 * list, or -1 if this list does not contain the element. More formally,
	 * returns the highest index <tt>i</tt> such that
	 * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
	 * or -1 if there is no such index.
	 * 
	 * @param o
	 *            element to search for
	 * @return the index of the last occurrence of the specified element in this
	 *         list, or -1 if this list does not contain the element
	 */
	@Override
	public int lastIndexOf(final Object o) {
		int index = size;
		if (o == null) {
			for (Entry<E> e = header.previous; e != header; e = e.previous) {
				index--;
				if (e.element == null)
					return index;
			}
		} else {
			for (Entry<E> e = header.previous; e != header; e = e.previous) {
				index--;
				if (o.equals(e.element))
					return index;
			}
		}
		return -1;
	}

	// Positional Access Operations

	/**
	 * Returns a list-iterator of the elements in this list (in proper
	 * sequence), starting at the specified position in the list. Obeys the
	 * general contract of <tt>List.listIterator(int)</tt>.
	 * <p>
	 * 
	 * The list-iterator is <i>fail-fast</i>: if the list is structurally
	 * modified at any time after the Iterator is created, in any way except
	 * through the list-iterator's own <tt>remove</tt> or <tt>add</tt> methods,
	 * the list-iterator will throw a <tt>ConcurrentModificationException</tt>.
	 * Thus, in the face of concurrent modification, the iterator fails quickly
	 * and cleanly, rather than risking arbitrary, non-deterministic behavior at
	 * an undetermined time in the future.
	 * 
	 * @param index
	 *            index of the first element to be returned from the
	 *            list-iterator (by a call to <tt>next</tt>)
	 * @return a ListIterator of the elements in this list (in proper sequence),
	 *         starting at the specified position in the list
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 * @see List#listIterator(int)
	 */
	@Override
	public ListIterator<E> listIterator(final int index) {
		return new ListItr(index);
	}

	/**
	 * Adds the specified element as the tail (last element) of this list.
	 * 
	 * @param e
	 *            the element to add
	 * @return <tt>true</tt> (as specified by {@link Queue#offer})
	 * @since 1.5
	 */
	@Override
	public boolean offer(final E e) {
		return add(e);
	}

	// Deque operations
	/**
	 * Inserts the specified element at the front of this list.
	 * 
	 * @param e
	 *            the element to insert
	 * @return <tt>true</tt> (as specified by {@link Deque#offerFirst})
	 * @since 1.6
	 */
	@Override
	public boolean offerFirst(final E e) {
		addFirst(e);
		return true;
	}

	/**
	 * Inserts the specified element at the end of this list.
	 * 
	 * @param e
	 *            the element to insert
	 * @return <tt>true</tt> (as specified by {@link Deque#offerLast})
	 * @since 1.6
	 */
	@Override
	public boolean offerLast(final E e) {
		addLast(e);
		return true;
	}

	/**
	 * Retrieves, but does not remove, the head (first element) of this list.
	 * 
	 * @return the head of this list, or <tt>null</tt> if this list is empty
	 * @since 1.5
	 */
	@Override
	public E peek() {
		if (size == 0)
			return null;
		return getFirst();
	}

	// Search Operations

	/**
	 * Retrieves, but does not remove, the first element of this list, or
	 * returns <tt>null</tt> if this list is empty.
	 * 
	 * @return the first element of this list, or <tt>null</tt> if this list is
	 *         empty
	 * @since 1.6
	 */
	@Override
	public E peekFirst() {
		if (size == 0)
			return null;
		return getFirst();
	}

	/**
	 * Retrieves, but does not remove, the last element of this list, or returns
	 * <tt>null</tt> if this list is empty.
	 * 
	 * @return the last element of this list, or <tt>null</tt> if this list is
	 *         empty
	 * @since 1.6
	 */
	@Override
	public E peekLast() {
		if (size == 0)
			return null;
		return getLast();
	}

	// Queue operations.

	/**
	 * Retrieves and removes the head (first element) of this list
	 * 
	 * @return the head of this list, or <tt>null</tt> if this list is empty
	 * @since 1.5
	 */
	@Override
	public E poll() {
		if (size == 0)
			return null;
		return removeFirst();
	}

	/**
	 * Retrieves and removes the first element of this list, or returns
	 * <tt>null</tt> if this list is empty.
	 * 
	 * @return the first element of this list, or <tt>null</tt> if this list is
	 *         empty
	 * @since 1.6
	 */
	@Override
	public E pollFirst() {
		if (size == 0)
			return null;
		return removeFirst();
	}

	/**
	 * Retrieves and removes the last element of this list, or returns
	 * <tt>null</tt> if this list is empty.
	 * 
	 * @return the last element of this list, or <tt>null</tt> if this list is
	 *         empty
	 * @since 1.6
	 */
	@Override
	public E pollLast() {
		if (size == 0)
			return null;
		return removeLast();
	}

	/**
	 * Pops an element from the stack represented by this list. In other words,
	 * removes and returns the first element of this list.
	 * 
	 * <p>
	 * This method is equivalent to {@link #removeFirst()}.
	 * 
	 * @return the element at the front of this list (which is the top of the
	 *         stack represented by this list)
	 * @throws NoSuchElementException
	 *             if this list is empty
	 * @since 1.6
	 */
	@Override
	public E pop() {
		return removeFirst();
	}

	/**
	 * Pushes an element onto the stack represented by this list. In other
	 * words, inserts the element at the front of this list.
	 * 
	 * <p>
	 * This method is equivalent to {@link #addFirst}.
	 * 
	 * @param e
	 *            the element to push
	 * @since 1.6
	 */
	@Override
	public void push(final E e) {
		addFirst(e);
	}

	/**
	 * Retrieves and removes the head (first element) of this list.
	 * 
	 * @return the head of this list
	 * @throws NoSuchElementException
	 *             if this list is empty
	 * @since 1.5
	 */
	@Override
	public E remove() {
		return removeFirst();
	}

	/**
	 * Removes the element at the specified position in this list. Shifts any
	 * subsequent elements to the left (subtracts one from their indices).
	 * Returns the element that was removed from the list.
	 * 
	 * @param index
	 *            the index of the element to be removed
	 * @return the element previously at the specified position
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 */
	@Override
	public E remove(final int index) {
		return remove(entry(index));
	}

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present. If this list does not contain the element, it is
	 * unchanged. More formally, removes the element with the lowest index
	 * <tt>i</tt> such that
	 * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
	 * (if such an element exists). Returns <tt>true</tt> if this list contained
	 * the specified element (or equivalently, if this list changed as a result
	 * of the call).
	 * 
	 * @param o
	 *            element to be removed from this list, if present
	 * @return <tt>true</tt> if this list contained the specified element
	 */
	@Override
	public boolean remove(final Object o) {
		if (o == null) {
			for (Entry<E> e = header.next; e != header; e = e.next) {
				if (e.element == null) {
					remove(e);
					return true;
				}
			}
		} else {
			for (Entry<E> e = header.next; e != header; e = e.next) {
				if (o.equals(e.element)) {
					remove(e);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Removes and returns the first element from this list.
	 * 
	 * @return the first element from this list
	 * @throws NoSuchElementException
	 *             if this list is empty
	 */
	@Override
	public E removeFirst() {
		return remove(header.next);
	}

	/**
	 * Removes the first occurrence of the specified element in this list (when
	 * traversing the list from head to tail). If the list does not contain the
	 * element, it is unchanged.
	 * 
	 * @param o
	 *            element to be removed from this list, if present
	 * @return <tt>true</tt> if the list contained the specified element
	 * @since 1.6
	 */
	@Override
	public boolean removeFirstOccurrence(final Object o) {
		return remove(o);
	}

	/**
	 * Removes and returns the last element from this list.
	 * 
	 * @return the last element from this list
	 * @throws NoSuchElementException
	 *             if this list is empty
	 */
	@Override
	public E removeLast() {
		return remove(header.previous);
	}

	/**
	 * Removes the last occurrence of the specified element in this list (when
	 * traversing the list from head to tail). If the list does not contain the
	 * element, it is unchanged.
	 * 
	 * @param o
	 *            element to be removed from this list, if present
	 * @return <tt>true</tt> if the list contained the specified element
	 * @since 1.6
	 */
	@Override
	public boolean removeLastOccurrence(final Object o) {
		if (o == null) {
			for (Entry<E> e = header.previous; e != header; e = e.previous) {
				if (e.element == null) {
					remove(e);
					return true;
				}
			}
		} else {
			for (Entry<E> e = header.previous; e != header; e = e.previous) {
				if (o.equals(e.element)) {
					remove(e);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element.
	 * 
	 * @param index
	 *            index of the element to replace
	 * @param element
	 *            element to be stored at the specified position
	 * @return the element previously at the specified position
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 */
	@Override
	public E set(final int index, final E element) {
		final Entry<E> e = entry(index);
		final E oldVal = e.element;
		e.element = element;
		return oldVal;
	}

	/**
	 * Returns the number of elements in this list.
	 * 
	 * @return the number of elements in this list
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Returns an array containing all of the elements in this list in proper
	 * sequence (from first to last element).
	 * 
	 * <p>
	 * The returned array will be "safe" in that no references to it are
	 * maintained by this list. (In other words, this method must allocate a new
	 * array). The caller is thus free to modify the returned array.
	 * 
	 * <p>
	 * This method acts as bridge between array-based and collection-based APIs.
	 * 
	 * @return an array containing all of the elements in this list in proper
	 *         sequence
	 */
	@Override
	public Object[] toArray() {
		final Object[] result = new Object[size];
		int i = 0;
		for (Entry<E> e = header.next; e != header; e = e.next)
			result[i++] = e.element;
		return result;
	}

	/**
	 * Returns an array containing all of the elements in this list in proper
	 * sequence (from first to last element); the runtime type of the returned
	 * array is that of the specified array. If the list fits in the specified
	 * array, it is returned therein. Otherwise, a new array is allocated with
	 * the runtime type of the specified array and the size of this list.
	 * 
	 * <p>
	 * If the list fits in the specified array with room to spare (i.e., the
	 * array has more elements than the list), the element in the array
	 * immediately following the end of the list is set to <tt>null</tt>. (This
	 * is useful in determining the length of the list <i>only</i> if the caller
	 * knows that the list does not contain any null elements.)
	 * 
	 * <p>
	 * Like the {@link #toArray()} method, this method acts as bridge between
	 * array-based and collection-based APIs. Further, this method allows
	 * precise control over the runtime type of the output array, and may, under
	 * certain circumstances, be used to save allocation costs.
	 * 
	 * <p>
	 * Suppose <tt>x</tt> is a list known to contain only strings. The following
	 * code can be used to dump the list into a newly allocated array of
	 * <tt>String</tt>:
	 * 
	 * <pre>
	 * String[] y = x.toArray(new String[0]);
	 * </pre>
	 * 
	 * Note that <tt>toArray(new Object[0])</tt> is identical in function to
	 * <tt>toArray()</tt>.
	 * 
	 * @param a
	 *            the array into which the elements of the list are to be
	 *            stored, if it is big enough; otherwise, a new array of the
	 *            same runtime type is allocated for this purpose.
	 * @return an array containing the elements of the list
	 * @throws ArrayStoreException
	 *             if the runtime type of the specified array is not a supertype
	 *             of the runtime type of every element in this list
	 * @throws NullPointerException
	 *             if the specified array is null
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a) {
		if (a.length < size)
			a = (T[]) java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
		int i = 0;
		final Object[] result = a;
		for (Entry<E> e = header.next; e != header; e = e.next)
			result[i++] = e.element;

		if (a.length > size)
			a[size] = null;

		return a;
	}

	/**
	 * @see java.util.AbstractCollection#toString()
	 */
	@SuppressWarnings("nls")
	@Override
	public String toString() {
		final Object[] array = toArray();
		final StringBuilder result = new StringBuilder("[");
		for (int i = 0; i < array.length; i++) {
			result.append(array[i]);
			if (i < array.length - 1)
				result.append(", ");
		}
		result.append("]");
		return result.toString();
	}

	Entry<E> addBefore(final E e, final Entry<E> entry) {
		final Entry<E> newEntry = new Entry<E>(e, entry, entry.previous);
		newEntry.previous.next = newEntry;
		newEntry.next.previous = newEntry;
		size++;
		modCount++;
		return newEntry;
	}

	E remove(final Entry<E> e) {
		if (e == header)
			throw new NoSuchElementException();

		final E result = e.element;
		e.previous.next = e.next;
		e.next.previous = e.previous;
		e.next = e.previous = null;
		e.element = null;
		size--;
		modCount++;
		return result;
	}

	/**
	 * Returns the indexed entry.
	 */
	private Entry<E> entry(final int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size); //$NON-NLS-1$ //$NON-NLS-2$
		Entry<E> e = header;
		if (index < (size >> 1)) {
			for (int i = 0; i <= index; i++)
				e = e.next;
		} else {
			for (int i = size; i > index; i--)
				e = e.previous;
		}
		return e;
	}

	/**
	 * Reconstitute this <tt>LinkedList</tt> instance from a stream (that is
	 * deserialize it).
	 */
	@SuppressWarnings("unchecked")
	private void readObject(final java.io.ObjectInputStream s) throws java.io.IOException, ClassNotFoundException {
		// Read in any hidden serialization magic
		s.defaultReadObject();

		// Read in size
		final int localsize = s.readInt();

		// Initialize header
		header = new Entry<E>(null, null, null);
		header.next = header.previous = header;

		// Read in all elements in the proper order.
		for (int i = 0; i < localsize; i++)
			addBefore((E) s.readObject(), header);
	}

	/**
	 * Save the state of this <tt>LinkedList</tt> instance to a stream (that is,
	 * serialize it).
	 * 
	 * @serialData The size of the list (the number of elements it contains) is
	 *             emitted (int), followed by all of its elements (each an
	 *             Object) in the proper order.
	 */
	private void writeObject(final java.io.ObjectOutputStream s) throws java.io.IOException {
		// Write out any hidden serialization magic
		s.defaultWriteObject();

		// Write out size
		s.writeInt(size);

		// Write out all elements in the proper order.
		for (Entry<E> e = header.next; e != header; e = e.next)
			s.writeObject(e.element);
	}

	transient Entry<E> header = new Entry<E>(null, null, null);

	transient int size = 0;

	private static final long serialVersionUID = 876323262645176354L;

	private static class Entry<E> {
		Entry(final E element, final Entry<E> next, final Entry<E> previous) {
			this.element = element;
			this.next = next;
			this.previous = previous;
		}

		@SuppressWarnings("nls")
		@Override
		public String toString() {
			return "Entry: '" + element + "' next: " + next.element + " prev: " + previous.element;
		}

		E element;

		Entry<E> next;

		Entry<E> previous;
	}

	@SuppressWarnings("synthetic-access")
	private class ListItr implements ListIterator<E>, Swap<E> {
		ListItr(final int index) {
			if (index < 0 || index > size)
				throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size); //$NON-NLS-1$ //$NON-NLS-2$
			if (index < (size >> 1)) {
				next = header.next;
				for (nextIndex = 0; nextIndex < index; nextIndex++)
					next = next.next;
			} else {
				next = header;
				for (nextIndex = size; nextIndex > index; nextIndex--)
					next = next.previous;
			}
			start = index;
		}

		@Override
		public void add(final E e) {
			checkForComodification();
			lastReturned = header;
			addBefore(e, next);
			nextIndex++;
			expectedModCount++;
		}

		@Override
		public boolean hasNext() {
			if (noPrevious)
				return true;
			if (nextIndex == start)
				noNext = true;
			return !noNext;
		}

		@Override
		public boolean hasPrevious() {
			if (noNext)
				return true;
			if (nextIndex == start)
				noPrevious = true;
			return !noPrevious;
		}

		@Override
		public E next() {
			checkForComodification();
			if (!hasNext())
				throw new NoSuchElementException();
			noPrevious = false;
			if (next.element == null)
				next = header.next;
			lastReturned = next;

			next = next.next;
			nextIndex++;
			if (nextIndex >= size)
				nextIndex -= size;
			if (nextIndex == start)
				noNext = false;
			return lastReturned.element;
		}

		@Override
		public int nextIndex() {
			return nextIndex;
		}

		@Override
		public E previous() {
			if (!hasPrevious())
				throw new NoSuchElementException();

			noNext = false;
			next = next.previous;
			if (next.element == null)
				next = header.previous;
			lastReturned = next;
			nextIndex--;
			if (nextIndex < 0)
				nextIndex += size;
			if (nextIndex == start)
				noPrevious = size > 0;
			checkForComodification();
			return lastReturned.element;
		}

		@Override
		public int previousIndex() {
			return nextIndex - 1;
		}

		@Override
		public void remove() {
			checkForComodification();
			final Entry<E> lastNext = lastReturned.next;
			try {
				CircularLinkedList.this.remove(lastReturned);
			} catch (final NoSuchElementException e) {
				throw new IllegalStateException();
			}
			if (next == lastReturned)
				next = lastNext;
			else
				nextIndex--;
			lastReturned = header;
			expectedModCount++;
		}

		@Override
		public void set(final E e) {
			if (lastReturned == header)
				throw new IllegalStateException();
			checkForComodification();
			lastReturned.element = e;
		}

		/**
		 * @see com.rubecula.darwin.examples.travelingsalesman.Swap#swap(int)
		 */
		@Override
		public void swap(final int location) {
			final int index = usefulModulo(location, size);
			if (index > 0) {
				Entry<E> pointer = next;
				final Entry<E> first = pointer;
				final E save = pointer.element;
				for (int i = 0; i < index; i++) {
					pointer = pointer.next;
					if (pointer.element == null)
						pointer = header.next;
				}
				final E temp = pointer.element;
				pointer.element = save;
				first.element = temp;
			}
		}

		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder("{"); //$NON-NLS-1$
			while (hasNext())
				result.append(next().toString() + ", "); //$NON-NLS-1$
			while (hasPrevious())
				previous();
			result.append("}"); //$NON-NLS-1$
			return result.toString();
		}

		final void checkForComodification() {
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
		}

		private Entry<E> lastReturned = header;

		private Entry<E> next;

		private final int start;

		private int nextIndex;

		private boolean noPrevious = size > 0;

		private boolean noNext = false;

		private int expectedModCount = modCount;
	}

	public static int usefulModulo(final int top, final int bottom) {
		if (bottom != 0) {
			final int quotient = top / bottom;
			final int x = top - ((quotient - 1) * bottom);
			return x % bottom;
		}
		throw new RuntimeException("cannot take modulo wrt 0"); //$NON-NLS-1$
	}
}
