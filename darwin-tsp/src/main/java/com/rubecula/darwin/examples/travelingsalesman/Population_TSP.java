/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Population_TSP.java
 * Created on Feb 8, 2007
 * @version $Revision: 1.64 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Map;

import net.sf.darwin.api.base.Colony_;
import net.sf.darwin.api.base.Sink_;
import net.sf.darwin.api.impl.Population_Managed;
import net.sf.darwin.api.impl.Trait_Variable;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.ExPhen;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.ProcessBest;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.ValueException;

/**
 * Population model for the Traveling Salesman
 * 
 * @author Robin Hillyard
 * 
 */
public class Population_TSP extends Population_Managed<Number, Number, Number> {

	/**
	 * @param identifier
	 */
	public Population_TSP(final String identifier) {
		super(identifier, Population_TSP.ALL_CLIENTS);
	}

	@Override
	protected Object determineResult() {
		final Colony colony = getColony(0);
		final Environment environment = colony.getEnvironment();
		final double travelTime = ((TakesTravelTime) environment).getTravelTime();
		return Double.valueOf(travelTime);
	}

	/**
	 * First we determine the criterion (to be used later) which is the travel
	 * time of the current environment. This is passed into the setCriterion
	 * method of the processor for processing the best in environment. Then we
	 * invoke {@link #processBestFit(ProcessBest)} with a newly created instance
	 * of {@link NoteTravelTime} as its processor.
	 * 
	 * NOTE there are two casts in this method, both of which must work by
	 * definition of the application. This implementation gets the travel time
	 * for the current environment and passes that as the criterion parameter
	 * into {@link #processBestFit(ProcessBest)}.
	 * 
	 * TODO this should be merged with findAndProcessBestFit() and perhaps
	 * {@link Colony#setupGeneration()}.
	 * 
	 * @return true if all is well
	 */
	@Override
	protected boolean findAndProcessBestFit() {
		final Organism<Number, Number, Number> organism = getBestInEnvironment().getObject();
		Colony<Number, Number, Number> colony = getColony(0);
		if (organism != null)
			colony = organism.getColony();
		logBestOrganism(colony);
		final Environment environment = colony.getEnvironment();
		final Object criterion = Double.valueOf(((TakesTravelTime) environment).getTravelTime());
		((ProcessBestInEnvironment) getBestInEnvironment().getProcessor()).setCriterion(criterion);
		final Phenome phenome = getTaxon().getPhenome();
		final FitnessEngine fitnessEngine = phenome.getFitnessEngine();
		Number factor = Double.valueOf(1);
		for (int i = 0; i < 10; i++) {
			fitnessEngine.setFitnessAdjustment(phenome.getCharacterKeys(), factor);
			try {
				return processBestFit(new NoteTravelTime());
			} catch (final ValueException e) {
				factor = getDiscrepancy(e);
			}
		}
		throw new DarwinException("findAndProcessBestFit: value exception after trying to adjust fitness factors"); //$NON-NLS-1$
	}

	/**
	 * @return true if the environment has stabilized -- there will be no
	 *         further changes
	 * @see com.rubecula.darwin.domain.world.Population_Managed#isEnvironmentStable(com.rubecula.darwin.domain.helper.Environment)
	 */
	@Override
	protected boolean isEnvironmentStable(final Environment environment) {
		final EcoFactor_Clients clients = (EcoFactor_Clients) environment.getEcoSystem().getFactor(
				EcoFactor_Clients.FACTOR_CLIENTS);
		final int currentClients = clients.getClients().size();
		final int allClients = ((ClientMap) getTaxon().getProperty(ALL_CLIENTS)).getMap().size();
		return currentClients >= allClients;
	}

	@Override
	protected void logBestFitness(final Organism<Number, Number, Number> organism, final Phenotype<Number> phenotype)
			throws ValueException {
		LOG.info("best fitness: " + organism.getValue() + " for " //$NON-NLS-1$ //$NON-NLS-2$
				+ phenotype.getTrait(EcoFactor_TravelTimes.FACTOR_TRAVEL_TIME));
	}

	/**
	 * 
	 */
	@Override
	protected void logBestOrganism(final Colony<Number, Number, Number> colony) {
		// TODO why don't we get object in first line
		LOG.info("Best solution for generation " + getGeneration() + ": " + getBestInEnvironment().toString()); //$NON-NLS-1$ //$NON-NLS-2$
		final Organism<Number, Number, Number> organism = getBestInEnvironment().getObject();
		if (organism != null) {
			LOG.info("... object: " + organism.toString()); //$NON-NLS-1$
			final Phenotype<Number> phenotype = organism.getPhenotype();
			for (final ExPhen<Number> exPhen : phenotype.getExtendedPhenotypes()) {
				// TODO consider doing this via polymorphism
				if (exPhen instanceof ExPhen_TSP) {
					final Route route = ((ExPhen_TSP) exPhen).getRoute();
					LOG.info("... route: " + route); //$NON-NLS-1$
					final double travelTime = route.getTravelTime((EcoFactor_TravelTimes) colony.getEnvironment().getEcoSystem()
							.getFactor(EcoFactor_TravelTimes.FACTOR_TRAVELTIMES));
					LOG.info("... travel time: " + travelTime); //$NON-NLS-1$
				}
			}
		} else
			LOG.info("... no best organism yet for newly updated environment"); //$NON-NLS-1$
	}

	/**
	 * In this implementation, we get the client map from the founderEnvironment
	 * eco factor called {@link EcoFactor_Clients#FACTOR_CLIENTS}. Then we pass
	 * that along with its size and the founderColony and the newest client into
	 * {@link #makeDaughterColonies(Colony, int, Object)}.
	 * 
	 * @return the number of daughter colonies created.
	 * @see com.rubecula.darwin.domain.world.Population_Managed#makeDaughterColonies(com.rubecula.darwin.domain.helper.Colony,
	 *      com.rubecula.darwin.domain.helper.Environment)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected int makeDaughterColonies(final Colony<Number, Number, Number> founderColony,
			final Environment<Number> founderEnvironment) {
		// XXX consider checking the type of founderEnvironment
		final EcoFactor_Clients factor = (EcoFactor_Clients) founderEnvironment.getEcoSystem().getFactor(
				EcoFactor_Clients.FACTOR_CLIENTS);
		final Map<Integer, Client> map = (Map<Integer, Client>) factor.getValue();
		final int size = map.size();
		return makeDaughterColonies(founderColony, size, map.get(Integer.valueOf(size - 1)));
	}

	/**
	 * @param founderColony
	 * @param newClient
	 * @param pos
	 * @return the new daughter colony
	 * @throws CloneNotSupportedException
	 */
	@Override
	protected Colony<Number, Number, Number> makeDaughterColony(final Colony<Number, Number, Number> founderColony,
			final Object newClient, final int pos) throws CloneNotSupportedException {
		final Colony_ clone = (Colony_) ((Colony_) founderColony).clone();
		((EcoFactor_Clients) clone.getEnvironment().getEcoSystem().getFactor(EcoFactor_Clients.FACTOR_CLIENTS)).moveClient(pos,
				(Client) newClient);
		return clone;
	}

	/**
	 * This method loops through the extended phenotypes provided and for each
	 * invoke {@link ExPhen#applyToEnvironment(Object)}, passing in the
	 * <code>criterion</code>. If the result is true for any extended phenotype,
	 * the returned result will be true.
	 * 
	 * @param exphens
	 * @param criterion
	 *            the criterion which will be used by the method
	 *            {@link ExPhen#applyToEnvironment(Object)}. Generally speaking,
	 *            the higher this value, the more likely the environment (in
	 *            particular, the eco factor(s) specified by an extended
	 *            phenotype) will be updated.
	 * @return true if the environment of an extended phenotype has been updated
	 */
	@Override
	protected boolean processExtendedPhenotypes(final Collection<ExPhen<Number>> exphens, final Object criterion) {
		boolean updated = false;
		if (exphens.size() == 0)
			LOG.warn("processExtendedPhenotypes(): no extended phenotypes"); //$NON-NLS-1$
		for (final ExPhen<Number> exPhen : exphens)
			updated |= exPhen.applyToEnvironment(criterion);
		return updated;
	}

	/**
	 * @param i
	 *            application-specific parameter
	 * @return a newly created organism based on the integer i
	 */
	@Override
	protected Organism<Number, Number, Number> seedOrganism(final Colony<Number, Number, Number> colony, final int i) {
		return Organism_TSP.createOrganism(i, colony);
	}

	/**
	 * @return a String for the first descriptive message about the environment
	 */
	@Override
	protected String showEnvironment1(final Environment environment) {
		return "Clients: " + ((Environment_TSP) environment).getClients(); //$NON-NLS-1$
	}

	/**
	 * @return a String for the second descriptive message about the environment
	 */
	@Override
	protected String showEnvironment2(final Environment environment) {
		return "Best travel time is: " + ((TakesTravelTime) environment).getTravelTime(); //$NON-NLS-1$
	}

	/**
	 * @param e
	 * @return
	 */
	private Number getDiscrepancy(final ValueException e) {
		final Throwable cause = e.getCause();
		if (cause instanceof FitnessException) {
			switch (((FitnessException) cause).getProblem()) {
			case PoissonTargetExceedsValue:
				return ((FitnessException) cause).getDiscrepancy();

			default:
				throw new DarwinException("findAndProcessBestFit: fitness exception cannot be handled", cause); //$NON-NLS-1$;
			}
		}
		throw new DarwinException("findAndProcessBestFit: exception is not a FitnessException and cannot be handled"); //$NON-NLS-1$;
	}

	/**
	 * 
	 */
	public static final String ALL_CLIENTS = "AllClients"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String LOG_MSG_2 = "best organism updated for generation {0} with travel time: {1}"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String LOG_MSG_1 = "Fitness for {0}:  {1}"; //$NON-NLS-1$

	private static final long serialVersionUID = 2525642321685236726L;

	/**
	 */
	final static class NoteTravelTime implements ProcessBest<Organism<Number, Number, Number>> {
		@Override
		public void onUpdate(final Organism<Number, Number, Number> object) {
			final Organism<Number, Number, Number> organism = object;
			final Population<Number, Number, Number> population = organism.getColony().getPopulation();
			final Phenotype<Number> phenotype = organism.getPhenotype();
			if (phenotype != null) {
				Object travelTime = null;
				for (final String key : phenotype.getKeys()) {
					final Trait trait = phenotype.getTrait(key);
					// TODO consider doing this via polymorphism
					if (trait instanceof Trait_Variable) {
						if (trait.getCharacterKey().equals(EcoFactor_TravelTimes.FACTOR_TRAVEL_TIME)) {
							travelTime = ((Trait_Variable) trait).getValue();
						}
					}
				}
				if (travelTime != null)
					LOG.info(MessageFormat.format(LOG_MSG_2, Integer.valueOf(population.getTaxon().getGeneration()), travelTime));
				for (final ExPhen<Number> exPhen : phenotype.getExtendedPhenotypes()) {
					String message;
					try {
						message = MessageFormat.format(LOG_MSG_1, exPhen.getValue(), object.getValue());
						final Sink sink = object.getColony().getSink();
						Sink_.sinkOrLog(message, sink, LOG);
						try {
							sink.flush();
						} catch (final IOException e) {
							throw new DarwinException("onUpdate(): sink flush exception", e); //$NON-NLS-1$
						}
					} catch (final ValueException e1) {
						// This should never be thrown here because we won't
						// update with a bad value.
						throw new DarwinException("onUpdate(): value exception", e1); //$NON-NLS-1$
					}
				}

			} else
				throw new DarwinException("onUpdate(): logic error: organism already dead: " + organism); //$NON-NLS-1$
		}
	}

}
