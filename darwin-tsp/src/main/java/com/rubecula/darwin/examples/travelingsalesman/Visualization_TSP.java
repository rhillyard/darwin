/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualization_TSP.java
 * Created on Apr 22, 2007
 * @version $Revision: 1.9 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.core.Countable;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.VisualizationFactory;
import net.sf.darwin.ui.impl.Visualization_VisualizableListener;

/**
 * @author Robin Hillyard
 * 
 */
public class Visualization_TSP extends Visualization_VisualizableListener {

	/**
	 * @param factory
	 */
	public Visualization_TSP(final VisualizationFactory factory) {
		super(factory);
	}

	/**
	 * @see com.rubecula.darwin.visualization.swing.Visualization_VisualizableListener#onChange(com.rubecula.darwin.foundation.Visualizable,
	 *      java.lang.Object)
	 */
	@Override
	public void onChange(final Visualizable source, final Object context) {
		super.onChange(source, context);
		if (source != null) {
			System.out.println("Number of individuals in " + source.getIdentifier() + ": " + ((Countable) source).getCount()); //$NON-NLS-1$ //$NON-NLS-2$			
		}
	}

}
