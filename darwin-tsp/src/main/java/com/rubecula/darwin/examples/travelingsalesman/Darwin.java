/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Darwin.java
 * Created on Oct 4, 2009
 * @version $Revision: 1.16 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.net.MalformedURLException;
import java.net.URL;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.Evolution_;
import net.sf.darwin.api.impl.BeanContainer_BeanPot;
import net.sf.darwin.api.impl.ClockWatcher_Default;
import net.sf.darwin.api.impl.GenerationListener_Standard;
import net.sf.darwin.core.BeanContainerException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.beanpot.BeanPot;

/**
 * Class to start up a Darwin application (not an applet).
 * 
 * <p>
 * The syntax of the command line parameters is:
 * 
 * <pre>
 * Darwin[-d][-gN][configurationfilename]
 * </pre>
 * 
 * The configuration filename should be specified as a full path name in the
 * following form: <code>/C:/.../../file.xml</code> (yes, you can use the
 * forward slashes on Windows). If you do not start the filename with a "/"
 * character, the system will look for the configuration file in the
 * <code>com.rubecula.darwin.startup</code> package under the
 * <code>/src/main/resoure/</code> source folder. This is useful for running
 * demo programs provided by Darwin contributors.
 * 
 * Currently, there are only two such demos, called outOfTheBox.xml (the default
 * demo if you don't specify a configuration file at all) and
 * travelingSalesman.xml.
 * 
 * There are two other optional parameters:
 * </p>
 * <p>
 * <dl>
 * <dt>-d</dt>
 * <dd>(which sets debug mode)</dd>
 * <dt>-gN</dt>
 * <dd>where N is the maximum number of generations you'd like to run (by
 * default it will run forever, or until some other condition terminates it).</dd>
 * </dl>
 * </p>
 * 
 * <p>
 * Note that this class is designed for use with the BeanPot as the bean
 * container. However, by changing the class {@link BeanContainer}, and making
 * some other relatively minor changes in this class, it should be possible to
 * use Spring or some other bean container instead.
 * 
 * </p>
 * 
 * TODO use typeof where appropriate
 * 
 * @author Robin Hillyard
 * 
 */
public class Darwin {

	/**
	 * 
	 */
	public Darwin() {
		super();
	}

	/**
	 * @param beanContainer
	 *            TODO This hasn't really been tested properly
	 * @param maxGenerations
	 * @param debug
	 * @param configurationFile
	 * @throws BeanContainerException
	 */
	protected void configureAndRunEvolution(final BeanContainer beanContainer, final int maxGenerations, final boolean debug,
			final String configurationFile) throws BeanContainerException {
		// TODO create methods in BeanContainer for this
		BeanPot.setPreConfigData(new Object[] { new GenerationListener_Standard(), new ClockWatcher_Default() });
		BeanPot.setPostConfigData(Integer.valueOf(maxGenerations));
		if (configurationFile.startsWith("/")) //$NON-NLS-1$
			beanContainer.setConfiguration(configurationFile);
		else
			beanContainer.setConfigurationByResource(getClass(), configurationFile);
		beanContainer.setDebug(debug);
		beanContainer.configure();

		if (debug)
			beanContainer.showBeans();

		final long startTime = System.currentTimeMillis();
		beanContainer.runBeans();
		System.out
				.println("Real time elapsed for evolution: " + Evolution_.getElapsedTime(System.currentTimeMillis() - startTime)); //$NON-NLS-1$
	}

	/**
	 * @param args
	 */
	protected void doMain(final String[] args) {
		int maxGenerations = 0;
		boolean debug = false;
		String configurationFile = CONFIG_OUT_OF_THE_BOX;
		final BeanContainer beanContainer = new BeanContainer_BeanPot();

		for (int i = 0; i < args.length; i++) {
			final String arg = args[i];
			if (arg.startsWith("-")) { //$NON-NLS-1$
				if (arg.substring(1).equalsIgnoreCase("d")) //$NON-NLS-1$
					debug = true;
				else if (arg.substring(1).startsWith("g")) { //$NON-NLS-1$
					try {
						maxGenerations = Integer.parseInt(arg.substring(2));
					} catch (final NumberFormatException e) {
						System.err.println("Darwin exception for arg: " + arg + ": " + e.getLocalizedMessage()); //$NON-NLS-1$ //$NON-NLS-2$
					}
				} else if (arg.substring(1).startsWith("v")) { //$NON-NLS-1$
					try {
						if (arg.length() > 2)
							beanContainer.setValidate(true, new URL(arg.substring(2)));
						beanContainer.setValidate();
					} catch (final MalformedURLException e) {
						System.err.println("Darwin exception for arg: " + arg + ": " + e.getLocalizedMessage()); //$NON-NLS-1$ //$NON-NLS-2$
					}
				} else
					System.err.println(SYNTAX);
			} else
				configurationFile = arg;
		}

		System.out.println("Darwin application starting with configurationFile: " + configurationFile); //$NON-NLS-1$

		try {
			configureAndRunEvolution(beanContainer, maxGenerations, debug, configurationFile);
		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			beanContainer.cleanup();
		}
	}

	/**
	 * 
	 */
	private static final String SYNTAX = "Syntax: Darwin [-d] [-gN] [-v[dtdUrl]] [configurationFile]"; //$NON-NLS-1$

	/**
	 * 
	 */
	protected static final String CONFIG_OUT_OF_THE_BOX = "outOfTheBox.xml"; //$NON-NLS-1$

	protected static final Log LOG = LogFactory.getLog(Darwin.class);

	/**
	 * Darwin's own class loader.
	 */
	public static class DarwinClassLoader extends ClassLoader {

		/**
		 * @param logIt
		 *            XXX
		 * 
		 */
		public DarwinClassLoader(final boolean logIt) {
			super();
			_logIt = logIt;
		}

		/**
		 * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
		 */
		@Override
		protected synchronized Class<?> loadClass(final String name, final boolean resolve) throws ClassNotFoundException {
			// TODO this does not work as expected: even after a class has been
			// loaded, the findLoadedClass method returns null, presumably
			// because it is in the parent class loader that the class has
			// actually been loaded previously.
			if (_logIt && ((Class<?>) findLoadedClass(name) == null))
				System.out.println("Darwin loading class " + name); //$NON-NLS-1$				
			return super.loadClass(name, resolve);
		}

		private final boolean _logIt;

	}

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		LOG.info("Darwin application begun using args:"); //$NON-NLS-1$
		for (int i = 0; i < args.length; i++)
			LOG.info(args[i]);
		// BeanContainer.setClassLoader(new DarwinClassLoader(true));
		new Darwin().doMain(args);
	}

}
