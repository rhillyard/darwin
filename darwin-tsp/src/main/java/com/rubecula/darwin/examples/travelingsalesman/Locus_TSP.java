/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Locus_TSP.java
 * Created on Jul 1, 2009
 * @version $Revision: 1.21 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import net.sf.darwin.api.base.Locus_Random;

import org.apache.commons.math.random.RandomGenerator;

/**
 * @author Robin Hillyard
 * 
 */
public class Locus_TSP extends Locus_Random<Number> {

	/**
	 * @param swap
	 *            true if this locus affects the swapping of route clients.
	 * @param identifier
	 * @param random
	 */
	public Locus_TSP(final boolean swap, final String identifier, final RandomGenerator random) {
		super(identifier, random);
		this._swap = swap;
	}

	/**
	 * @return {@link #_swap}, as defined by the constructor.
	 */
	public boolean isSwap() {
		return this._swap;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5174056033928188275L;

	private final boolean _swap;

}
