/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Application_PepperedMoth.java
 * Created on Apr 24, 2009
 * @version $Revision: 1.2 $
 */

package com.rubecula.darwin.examples.travelingsalesman;

import java.util.Set;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.Evolvable_;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.GenerationListener;

/**
 * <p>
 * Example application which runs the peppered moth evolution without an
 * interactive user interface.
 * </p>
 * <p>
 * In this example, we solve the traveling salesman problem in order (N^2) time
 * (where N is the number of clients to be visited). A brute force approach
 * would take order (N!) time.
 * </p>
 * <p>
 * This solution does not guarantee to find the best solution of course (unlike
 * the brute force approach), but it is expected to find a pretty good solution.
 * </p>
 * <p>
 * The specific problem is for a New England Regional Sales Manager to visit 9
 * of his customers in and around New England and returning home (actually, the
 * way the problem is posed, he can start at any client and must return there).
 * The problem where he starts from a home/depot and returns there is of course
 * slightly different (it's simply a question of solving the given problem and
 * then arranging to find the closest pair of points and starting/finishing with
 * side routes from those to home/depot.
 * </p>
 * <p>
 * The way that this problem is solved here is to start with four clients and to
 * find the best solution (finding the best solution for three clients is
 * trivial -- they form a triangle). We allow the solution to converge (in
 * practice, we wait 2*N generations) and then add a new client (arbitrarily) to
 * the route. Again, we allow the solution to converge. Step-by-step we keep
 * adding one client to the (hopefully) best route for the existing clients.
 * </p>
 * <p>
 * A route (at first it is arbitrarily chosen) based on the current list of
 * clients forms part of the environment in which the the organisms (candidate
 * solutions) are trying to survive. At the end of each generation, the best
 * current route is found and all of the genomes of the organisms are now
 * normalized such that they now express that route. That way, we retain the
 * same population of candidate genomes. When the solution converges (currently
 * this is somewhat arbitrarily determined - see above), a new client is added.
 * </p>
 * <p>
 * At any time that we pick a new best route or we add a client to the route, we
 * must signal to the all of the organisms that the environment has changed.
 * </p>
 * <p>
 * Finally, once we have converged after all 9 clients have been visited we
 * report the results and quit.
 * </p>
 * <p>
 * The genetics of this solution are based on an asexual, that is haploid, life
 * form (like a bacterium). The genome of an organims is a string of
 * instructions which express a particular route. The mechanism for expression
 * is that we start with the current best route (part of the environment) and
 * step through the genes of the genome. Each gene will be either a rotation or
 * a swap. A rotation rotates the route relative to the current route and the
 * swap switches the 0th and Nth clients of the current route.
 * </p>
 * <p>
 * In practice, as the problem is defined, the shortest driving time to visit
 * all 9 clients is 888 minutes. The best that this current version of the
 * solution has managed is 890 minutes. In order to improve the likelihood of
 * finding the right solution, we could adjust several factors: the number of
 * generations we allow for convergence, the population size, the mutation rate,
 * the fitness calculation, and perhaps other factors. All of these can be
 * configured via the configuration file beansApplication.xml in the
 * TravelingSalesmanOld resource package.
 * <p>
 * 
 * TODO When we update the ecofactor because of higher fitness, we should
 * double-check that we're not increasing the travel time. Unknown Task
 * 
 * @author Robin Hillyard
 * 
 */
public class TravelingSalesmanOld {

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final boolean debug = false;
		int maxGenerations = 0;
		if (args.length > 0)
			maxGenerations = Integer.parseInt(args[0]);
		try {
			BeanContainer.setConfiguration(TravelingSalesmanOld.class, FILENAME_BEANS_XML);
			BeanContainer.setDebug(true);
			BeanContainer.configure();

			if (debug)
				BeanContainer.showBeans();

			final Evolution evolution = (Evolution) BeanContainer.getBean("Evolution"); //$NON-NLS-1$
			evolution.addListener(new GenerationListener() {
				public void onGeneration(final Evolvable evolvable) {
					if (evolvable != null)
						BeanContainer.getWriter().println("Evolvable " + evolvable.getIdentifier() + ": generation=" //$NON-NLS-1$ //$NON-NLS-2$
								+ evolvable.getGeneration());
					else
						BeanContainer.getWriter().println("TravelingSalesmanOld evolution is finished"); //$NON-NLS-1$

				}
			});
			evolution.init();

			if (maxGenerations > 0) {
				final Set<Evolvable> evolvables = evolution.getEvolvableKeys();
				for (final Evolvable evolvable : evolvables) {
					((Evolvable_) evolvable).setMaxGenerations(maxGenerations);
				}
			}

			evolution.run();
			final boolean complete = evolution.waitUntilComplete(250);
			BeanContainer.getWriter().println("Traveling Salesman is " + (complete ? "" : "not ") + "complete"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			if (!evolution.shutdown()) {
				System.err.println("evolution has not shut down properly"); //$NON-NLS-1$
			}
		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			BeanContainer.cleanup();
		}
	}

	/**
	 * 
	 */
	public TravelingSalesmanOld() {
		super();
	}

	/**
	 * beansApplication.xml
	 */
	private static final String FILENAME_BEANS_XML = "beansApplication.xml"; //$NON-NLS-1$

}
