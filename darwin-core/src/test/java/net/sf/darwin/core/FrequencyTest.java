package net.sf.darwin.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

@SuppressWarnings({ "nls", "static-method" })
public class FrequencyTest {

	@Test
	public void testAdd() {
		final Frequency frequency = new Frequency();
		frequency.add(1);
		assertEquals(1, frequency.intValue());
	}

	@Test
	public void testCompareTo() {
		final Frequency frequency1 = new Frequency(1);
		final Frequency frequency = new Frequency(1);
		assertEquals(0, frequency1.compareTo(frequency));
		frequency.add(1);
		assertEquals(-1, frequency1.compareTo(frequency));
		frequency.add(-2);
		assertEquals(1, frequency1.compareTo(frequency));
	}

	@Test
	public void testDecrement() {
		final Frequency frequency = new Frequency(1);
		frequency.decrement();
		assertEquals(0, frequency.intValue());
	}

	@Test
	public void testEqualsObject() {
		final Frequency frequency = new Frequency(1);
		assertEquals(new Frequency(1), frequency);
	}

	@Test
	public void testHashCode() {
		final Frequency frequency = new Frequency(1);
		final int hash = frequency.hashCode();
		assertEquals(new Frequency(1).hashCode(), hash);
	}

	@Test
	public void testIncrement() {
		final Frequency frequency = new Frequency();
		frequency.increment();
		assertEquals(1, frequency.intValue());
	}

	@Test
	public void testToString() {
		final Frequency frequency = new Frequency(1);
		assertEquals("1", frequency.toString());
	}

}
