package net.sf.darwin.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.JexpressionException;
import com.rubecula.jexpression.Term;

@SuppressWarnings({ "nls", "static-method" })
public class ExpressionMapTest {

	@Test
	public void testAddExpression() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map = new ExpressionMap();
		map.addExpression("x", eval);
		final int size = map.size();
		assertEquals(1, size);
	}

	@Test
	public void testClear() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map = new ExpressionMap();
		map.addExpression("x", eval);
		map.clear();
		final int size = map.size();
		assertEquals(0, size);
	}

	@Test
	public void testEqualsObject() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map1 = new ExpressionMap();
		map1.addExpression("x", eval);
		final ExpressionMap map2 = new ExpressionMap();
		map2.addExpression("x", eval);
		assertEquals(map1, map2);
	}

	@Test
	public void testGet() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map = new ExpressionMap();
		map.addExpression("x", eval);
		final EvalExpressionMutable expressionMutable = map.get("x");
		assertEquals(eval, expressionMutable);
	}

	@Test
	public void testHashCode() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map1 = new ExpressionMap();
		map1.addExpression("x", eval);
		final int hashCode = map1.hashCode();
		final ExpressionMap map2 = new ExpressionMap();
		map2.addExpression("x", eval);
		final int alt = map2.hashCode();
		assertEquals(alt, hashCode);
	}

	@Test
	public void testIsEmpty() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map = new ExpressionMap();
		map.addExpression("x", eval);
		assertFalse(map.isEmpty());
		map.clear();
		assertTrue(map.isEmpty());
	}

	@Test
	public void testKeySet() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map = new ExpressionMap();
		map.addExpression("x", eval);
		final List<String> list = new ArrayList<String>(map.keySet());
		assertEquals(Arrays.asList(new String[] { "x" }), list);
	}

	@Test
	public void testPutAll() {
		final MockEvalExpressionMutable eval = new MockEvalExpressionMutable();
		final ExpressionMap map = new ExpressionMap();
		map.addExpression("x", eval);
		map.addExpression("y", eval);
		final ExpressionMap map2 = new ExpressionMap();
		map2.putAll(map);
		final List<String> list = new ArrayList<String>(map2.keySet());
		assertEquals(new ArrayList<String>(map.keySet()), list);
	}

	public static final class MockEvalExpressionMutable implements EvalExpressionMutable {
		@Override
		public String getExpression() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Term[] getExpressionTerms() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isMutable() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void setExpression(final CharSequence... arg0) throws JexpressionException {
			// TODO Auto-generated method stub

		}

		@Override
		public void setExpression(final String arg0) throws JexpressionException {
			// TODO Auto-generated method stub

		}
	}

}
