/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Viability.java
 * Created on Jan 19, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 */
public interface Viability {

	/**
	 * @return the stats
	 */
	public Stats getStats();

	/**
	 * @param organism
	 *            XXX
	 * @return true if this object should continue to live in the next
	 *         generation.
	 * @throws FitnessException
	 */
	public abstract <E, P, G> boolean resetViability(Organism<E, P, G> organism) throws FitnessException;

}
