/**
 * Samson AI Project.
 * Copyright (C) 2013  Optum
 *
 * Module: Auditable
 * Created on: Sep 22, 2014
 */

package net.sf.darwin.core;

/**
 * Objects which implement this Auditable interface are expected to implement
 * IToString or something similar
 * 
 * @author robinhillyard
 *
 */
public interface Auditable {
	// Marker interface
}
