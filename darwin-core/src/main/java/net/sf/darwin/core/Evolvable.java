/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.core;

import java.applet.Applet;
import java.io.Serializable;

import net.sf.tostring0.Identifiable;

/**
 * <p>
 * This interface defines the concept of an Evolvable, something which can
 * evolve. This concept is central to the <b><i>Darwin</i></b> framework and
 * forms part of a triad of fundamental concepts:
 * <ul>
 * <li>{@link Evolution};</li>
 * <li>{@link Evolvable}, typically a {@link Taxon};</li>
 * <li>user interface, typically, but not necessarily, an {@link Applet}.</li>
 * </ul>
 * 
 * The properties of an Evolvable are as follows:
 * <ul>
 * <li>generation (read-only property);</li>
 * <li>finished (read-only property);</li>
 * <li>the next generation (invoking {@link #nextGeneration()} is what causes
 * the {@link Evolvable} to evolve a generation at a time):
 * <li>the ability to be seeded.</li>
 * </ul>
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.15 $
 */

public interface Evolvable extends Identifiable, Generational, Theological, Terminal, Serializable {

	/**
	 * @return the sequence number of the current generation.
	 */
	public abstract int getGeneration();

	/**
	 * @return the result
	 */
	public Object getResult();

}
