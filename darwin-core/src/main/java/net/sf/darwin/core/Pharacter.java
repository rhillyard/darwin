/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Pharacter.java
 * Created on May 29, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.Set;

import net.sf.tostring0.Identifiable;

/**
 * This interface represents the concept of a Phenotypic Character. The name was
 * chosen to avoid confusion with {@link Character}. A Pharacter can take the
 * value of any of a finite set of {@link Variant}s.
 * 
 * {@link Pharacter} is to {@link Locus} as {@link Phenotype} is to
 * {@link Genome}, or Trait is to Gene, or {@link Phenome} is to {@link Genomic}
 * .
 * 
 * An example of {@link Pharacter} would be "eye color". Brown, hazel, blue,
 * etc. are specific {@link Variant}s.
 * 
 * A specific trait takes as its value a specific variant from a character.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Pharacter
 */
public interface Pharacter<P> extends Identifiable, SexLinked {

	/**
	 * Method to add a trait to this character.
	 * 
	 * @param variant
	 *            A value which this character can take.
	 * @return the key to this variant.
	 */
	public abstract String addVariant(Variant<P> variant);

	/**
	 * @param variant
	 * @return the key of the trait for this Pharacter, otherwise null
	 */
	public abstract String getKey(Variant<P> variant);

	/**
	 * @param key
	 * @return the trait for this character indicated by key.
	 */
	public abstract Variant<P> getVariant(String key);

	/**
	 * @return the set of keys of the variants.
	 */
	public abstract Set<String> getVariantKeys();

	/**
	 * @return the entire list of traits for this character.
	 * 
	 *         Getter/setter typically used by reflection.
	 */
	public abstract Collection<Variant<P>> getVariants();

	/**
	 * sex
	 */
	public static final String SEX = "sex"; //$NON-NLS-1$

}
