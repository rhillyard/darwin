/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GeneticsException.java
 * Created on Jun 3, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 */
public class GeneticsException extends Exception {

	/**
	 * @param message
	 */
	public GeneticsException(final String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public GeneticsException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public GeneticsException(final Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private GeneticsException() {
		// Not used
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6983234867899685983L;

}
