/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Desirable.java
 * Created on Feb 26, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.core;

import com.rubecula.jexpression.EvalExpressionMutable;

/**
 * This interface defines a method to get a male's desirability.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Desirable extends EvalExpressionMutable {

	/**
	 * @param viability
	 *            true if the male is viable
	 * @param random
	 *            a random number between 0 and 1
	 * @param age
	 *            the age of the male
	 * @return the desirability either hard-coded or based on the current
	 *         customized formula
	 */
	public abstract double getDesirability(final boolean viability, final double random, final int age);

}