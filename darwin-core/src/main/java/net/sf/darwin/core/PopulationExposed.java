/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PopulationExposed.java
 * Created on Mar 29, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.core;

/**
 * MBean interface for Population. The existence of this interface forces any
 * Population bean to be registered as an MBean for JMX (unless the
 * configuration file explicitly says mbean="false").
 * 
 * Any properties in here are visible (exposed) to a JMX agent.
 * 
 * @author Robin Hillyard
 * 
 */
public interface PopulationExposed extends Countable {

	// no additional methods
}
