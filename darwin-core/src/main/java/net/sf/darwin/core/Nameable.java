/**
 * Rubecula Common Utilitiy Project.
 * Copyright (C) 2006 Rubecula Software, LLC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  
 * Identifiable
 * $Source: /cvsroot/darwin/Darwin/src/main/java/com/rubecula/darwin/foundation/Nameable.java,v $
 * Created on Jun 8, 2006
 *
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * <p>
 * Type: Identifiable
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2006, Rubecula Software, LLC.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.2 $
 */

public interface Nameable extends Identifiable {

	/**
	 * @param name
	 *            the String which will be used to identify this object.
	 */
	abstract public void setIdentifier(String name);

}
