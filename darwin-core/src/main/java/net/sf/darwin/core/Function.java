/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Function.java
 * Created on Sep 23, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

import com.rubecula.jexpression.Valuable;

/**
 * @author Robin Hillyard
 * 
 */
public interface Function extends Valuable, Identifiable {

	/**
	 * Method to add a function listener.
	 * 
	 * @param listener
	 */
	public abstract void addListener(FunctionListener listener);
}
