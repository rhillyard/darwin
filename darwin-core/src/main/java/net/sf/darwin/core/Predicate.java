/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Predicate.java
 * Created on Dec 9, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

/**
 * General interface to enable the evaluation of arbitrary truths.
 * 
 * @author Robin Hillyard
 * @param <X>
 * @param <Y>
 * 
 */
public interface Predicate<X, Y> {

	/**
	 * @param obj1
	 *            an arbitrary object
	 * @param obj2
	 *            an arbitrary object
	 * @return a result based on the values <code>obj1</code> and
	 *         <code>obj2</code>.
	 */
	public abstract boolean evaluate(X obj1, Y obj2);
}
