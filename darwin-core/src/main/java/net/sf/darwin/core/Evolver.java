/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution.java
 * Created on Aug 2, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.core;

import java.applet.Applet;
import java.util.Set;

/**
 * <p>
 * This interface defines the concept of an Evolver. The Evolver interface is a
 * super-interface of {@link Evolution} and deals with the fundamentals of
 * stepping through an evolvable system, generation by generation.
 * 
 * The properties of an Evolver are as follows:
 * <ul>
 * <li>a map of {@link Evolvable} evolvables and their generation frequencies
 * (in ticks of the clock);</li>
 * <li>a list (unordered) of {@link GenerationListener}s -- after each tick, any
 * evolver which has undergone a new generation will be passed to this listener;
 * </li>
 * <li>two modes of running [ONLY ONE MODE FOR EVOLVER: stepping]:
 * <ul>
 * <li>continuous running (with actions <code>start</code>, <code>pause</code>,
 * <code>resume</code>, <code>stop</code>)</li>
 * <li>stepping (with action <code>next</code>)</li>
 * </ul>
 * </li>
 * <li>the ability to seed the evolvables by invoking their
 * {@link Evolvable#seedMembers()} methods (this is really just a convenience
 * method).</li>
 * </ul>
 * </p>
 * <p>
 * Note that in general, there will be two modes of updating the user interface,
 * during evolution. The generation listeners fire at the completion of each
 * generation. If you also want to visualize updates at more points during the
 * completion of a generation, then you should also set up
 * {@link VisualizableListener}s on an {@link Visualizable} objects (such as
 * {@link Population}s or {@link Environment}s).
 * 
 * </p>
 * 
 * @author Robin Hillyard
 * 
 */
public interface Evolver extends Clocked {

	/**
	 * Add an evolvable object which undergoes a new generation once every tick
	 * of the clock.
	 * 
	 * @param evolvable
	 *            an {@link Evolvable} object, typically a {@link Taxon}.
	 */
	public abstract void addEvolvable(Evolvable evolvable);

	/**
	 * Add an evolvable object which undergoes a new generation once every
	 * <code>ticks</code> ticks of the clock.
	 * 
	 * @param evolvable
	 *            an {@link Evolvable} object, typically a {@link Taxon}.
	 * @param ticks
	 *            the number of ticks of the clock per generation.
	 */
	public abstract void addEvolvable(Evolvable evolvable, int ticks);

	/**
	 * Add a listener to the evolution process.
	 * 
	 * @param listener
	 * @return true if the listener was added
	 */
	public abstract boolean addListener(GenerationListener listener);

	/**
	 * @param listener
	 * @return true if successful
	 */
	public abstract boolean addVisualizableListener(final VisualizableListener listener);

	/**
	 * Method which is called before all user-interface components get
	 * destroyed. When an {@link EvolutionaryApplet} is employed as the
	 * user-interface, this method is called ny the {@link Applet#stop()}
	 * method.
	 */
	public abstract void cleanup();

	/**
	 * @return the clockWatcher
	 */
	public ClockWatcher getClockWatcher();

	/**
	 * @return the set of {@link Evolvable} objects.
	 */
	public abstract Set<Evolvable> getEvolvableKeys();

	/**
	 * Method which is called after all user-interface issues have been dealt
	 * with. When an {@link EvolutionaryApplet} is employed as the
	 * user-interface, this method is called by the {@link Applet#start()}
	 * method.
	 */
	public abstract void init();

	/**
	 * Increment the clock by one tick, firing new generations as appropriate.
	 * 
	 * @return true if there is more evolution to do.
	 * @throws EvolutionException
	 */
	public abstract boolean next() throws EvolutionException;

	/**
	 * @param evolvable
	 */
	public abstract void removeEvolvable(Evolvable evolvable);

	/**
	 * Seed the currently registered evolvables by calling
	 * {@link Evolvable#seedMembers()} on each one.
	 */
	public abstract void seedEvolvables();

	/**
	 * @param clockWatcher
	 *            the clockWatcher to set
	 */
	public void setClockWatcher(final ClockWatcher clockWatcher);

}