/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Ploidy.java
 * Created on Mar 4, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.core;

/**
 * This interface defines the concept of "ploidy", that is to say whether a
 * genetic object is diploid or haploid, i.e. does it have two sets of genes, or
 * only one?
 * 
 * Note: this was formerly called "Oid".
 * 
 * @author Robin Hillyard
 * 
 */
public interface Ploidy {

	/**
	 * Method to return the genetic form for a genome, that is to say the number
	 * of independent copies of each gene there are.
	 * 
	 * @return the "ploidy" which will normally be either {@link #HAPLOID}, i.e.
	 *         simple or {@link #DIPLOID}, i.e. double.
	 */
	public abstract int getPloidy();

	/**
	 * 2 (two genes at a locus).
	 */
	public final static int DIPLOID = 2;

	/**
	 * 1 (one gene at a locus).
	 */
	public final static int HAPLOID = 1;

}