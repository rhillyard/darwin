/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Attribute.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.core;

/**
 * Defines the methods supported by Attribute objects.
 * 
 * @author Robin Hillyard
 *
 * @param <K>
 *            the key type, usually String
 * @param <V>
 *            the value type
 */
public interface Attribute<K, V> {

	/**
	 * @return the "key" of this Attribute
	 */
	public abstract K getAttribute();

	/**
	 * Method to return the value for this attribute.
	 * 
	 * @return value of (private field) _Value as an Object.
	 */
	public abstract V getValue();

	/**
	 * Mutating method to set the value of this attribute.
	 * 
	 * @param value
	 *            the new value for the attribute.
	 * 
	 */
	public abstract void setValue(V value);

}