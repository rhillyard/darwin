/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Realm.java
 * Created on Aug 7, 2009
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.core;

/**
 * Interface which defines the concept of the "model" which is undergoing
 * evolution. Realm/model is loosely defined as all of the genetic and
 * phenotypic information as well as all of the populations, environments, etc.
 * The realm/model does not directly undergo evolution but {@link Taxon}s and
 * populations do.
 * 
 * TODO consider adding methods to keep lists of the populations and
 * environments belonging to a realm (instead of the taxon keeping the
 * populations and nothing at all keeping track of environments).
 * 
 * TODO consider making Realm implement Evolver and have it be the one and only
 * evolver for the Evolution bean, thus the getEvolvables method would be part
 * of the realm, not part of the Evolution bean.
 * 
 * Here follow some further to do items, which are more general in nature but
 * which need a place for them to be recorded in the source. Since every
 * application based on Darwin must have a realm, we put them here.
 * 
 * TODO Fix general packaging/architectural issues, as reported by SonarJ
 * 
 * TODO implement gene switching
 * 
 * TODO Have more beans implement HasExpressions and get rid of the specific
 * code for mating, etc.
 * 
 * TODO get rid of the use of properties (other than for language) -- everything
 * should be configured in the configuration file.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Realm extends HasPhenotypeCache {

	/**
	 */
	public void flushCaches();

	/**
	 * @return the fitness cache for this realm.
	 */
	public FitnessCache getFitnessCache();
	// currently no other methods
}
