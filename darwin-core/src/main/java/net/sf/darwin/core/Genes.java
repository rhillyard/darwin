/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genes.java
 * Created on Mar 5, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.core;

import java.util.List;
import java.util.ListIterator;

/**
 * This interface represents the read-only aspects of (possibly a subset of) a
 * Genome, although the Ploidy is not included here.
 * 
 * TODO consider renaming this family of types as Genome...
 * 
 * @author Robin Hillyard
 * 
 * @param <G>
 *            the type of the genetic information which is encoded in this set
 *            of genes
 */
public interface Genes<G> extends Basic, Ploidy {

	/**
	 * @param locus
	 *            an integer from 0...
	 * @return the Gene at the specified locus position.
	 */
	public abstract Gene<G> getGene(int locus);

	public abstract List<Gene<G>> getGenes();

	/**
	 * @return the sex gene (if there is one), else null
	 */
	public abstract Gene<Boolean> getSexGene();

	/**
	 * TODO eliminate this redundant method
	 * 
	 * @return the number of genes
	 */
	public abstract ListIterator<Gene<G>> listIterator();

	/**
	 * TODO eliminate this redundant method
	 * 
	 * @return the number of genes
	 */
	public abstract int size();

	public abstract <X> X[] toArray(X[] a);

}