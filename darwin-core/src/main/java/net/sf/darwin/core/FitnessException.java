/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessException.java
 * Created on Mar 8, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 *         CONSIDER making less specific in API
 */
public class FitnessException extends Exception {

	/**
	 * @param string
	 * @param discrepancy
	 * @param problem
	 *            XXX
	 */
	public FitnessException(final String string, final Double discrepancy, final FitnessProblem problem) {
		super(string);
		this._discrepancy = discrepancy;
		this._problem = problem;
	}

	/**
	 * @return the discrepancy
	 */
	public Double getDiscrepancy() {
		return this._discrepancy;
	}

	/**
	 * @return the problem
	 */
	public FitnessProblem getProblem() {
		return this._problem;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4853825664694722952L;

	private final Double _discrepancy;

	private final FitnessProblem _problem;

}
