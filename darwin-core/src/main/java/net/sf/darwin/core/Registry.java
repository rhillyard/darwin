/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Registry.java
 * Created on Apr 11, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.core;

import java.util.Collection;

/**
 * Definition of a registry of births, deaths and marriages.
 * 
 * @author Robin Hillyard
 * 
 *         NOTE: since implementers of this interface do not require fields
 *         referencing type E, it is tempting to supply the generic type T in
 *         the three methods defined here. Unfortunately, that causes some type
 *         family inconsistencies.
 * 
 *
 * @param <E>
 *            the type of the information in the environment which is
 *            administered by this Registry
 * @param <P>
 *            the type of the phenotypic information exhibited by the Organisms
 *            which are administered by this Registry
 * @param <G>
 *            the type of the genetic information exhibited by the Organisms
 *            which are administered by this Registry
 */
public interface Registry<E, P, G> {

	/**
	 * TODO consider changing parameter to Colony
	 * 
	 * @param colony
	 * @param neonates
	 * 
	 */
	public abstract void registerBirths(Colony<E, P, G> colony, Collection<Organism<E, P, G>> neonates);

	/**
	 * @param colony
	 * @param deaths
	 * 
	 */
	public abstract void registerDeaths(Colony<E, P, G> colony, Collection<Organism<E, P, G>> deaths);

	/**
	 * @param colony
	 * @param marriages
	 * 
	 */
	public abstract void registerMarriages(Colony<E, P, G> colony, Collection<Mating> marriages);

}
