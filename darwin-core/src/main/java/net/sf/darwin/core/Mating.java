/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mating.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

/**
 * <p>
 * Defines the mating of a pair of organisms, one male, one female.
 * </p>
 * TODO this class should be based on generic types E, P, G
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.2 $
 */
public interface Mating {

	/**
	 * Method to create progeny for this mating. This method may be called any
	 * number of times and the progeny should be different each time.
	 * 
	 * @return a new organism, formed by combining the genomes of the gametes
	 *         spawned from the male and the female.
	 */
	public abstract <P, G> Nuclear<P, G> progeny();

}