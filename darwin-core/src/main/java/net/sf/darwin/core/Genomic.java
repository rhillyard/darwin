/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genomic.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.Map;

import net.sf.tostring0.Identifiable;

/**
 * <p>
 * Defines the genetic information contained in a Taxon. A Genomic may be either
 * haploid (a single strand of genes) or diploid (a double strand), though,
 * theoretically, a Genomic could have any number of genes at a particular
 * locus. This number is the "ploidy" and can be accessed via
 * {@link #getPloidy()}. <br>
 * A genomic object is essentially a list of Locus instances. <br>
 * Other properties of genomic include: an implementation of the {@link Meiosis}
 * interface. <br>
 * A note about type names. This type uses the name "Genomic". An alternative
 * would have been "Genomics" (but that's a little ugly) and also, an adjectival
 * form is often what we want when naming an interface (corresponding to a
 * concept). A Genomic object then denotes the general structure, organization,
 * plan, etc. of all the genomes of the individuals of a interbreeding group of
 * organisms (generally known as a species). The Genomic object is what
 * determines the chromosomal structure, for instance. <br>
 * This type family was formerly known as "Genome..."
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.12 $
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            Genomic
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Genomic
 */
public interface Genomic<P, G> extends Ploidy, Replicator<P, G>, Countable, Identifiable {

	/**
	 * Method to add a chromosome at the end of this Genomic.
	 * 
	 * @see #addChromosome(int, Chromosome)
	 * @param chromosome
	 *            a chromosome to be inserted into this Genomic.
	 * @return true if the chromosome was added.
	 */
	public abstract boolean addChromosome(Chromosome<G> chromosome);

	/**
	 * Method to add a chromosome at the specified index.
	 * 
	 * @see java.util.List#add(int, java.lang.Object)
	 * @param index
	 *            the index at which the new object will appear.
	 * @param chromosome
	 *            a chromosome to be inserted into this Genomic.
	 */
	public abstract void addChromosome(int index, Chromosome<G> chromosome);

	/**
	 * Method to create a new phenotype from a collection of genes by expressing
	 * each and every gene, one at a time, using the appropriate
	 * {@link Expresser}.
	 * 
	 * This method may be overwritten by implementers of {@link Genomic} but
	 * that would be unusual. An example of why you might want to overwrite it
	 * would be to allow the expression of several genes together or to express
	 * more than one trait from any one gene.
	 * 
	 * TODO remove this signature as the colony is never actually used AFAIK
	 * 
	 * @param phenotype
	 *            the phenotype to which the expressions will be added.
	 * 
	 * @param colony
	 *            the population in which the genes are being expressed (usually
	 *            this is null but the mechanism is available to allow
	 *            expression to be influenced by population (or the environment
	 *            in which the population is found).
	 * @param genes
	 */
	public abstract <E> void express(Phenotype<P> phenotype, Colony<E, P, G> colony, Genes<G> genes);

	/**
	 * Method to create a new phenotype from a collection of genes by expressing
	 * each and every gene, one at a time, using the appropriate
	 * {@link Expresser}.
	 * 
	 * This method may be overwritten by implementers of {@link Genomic} but
	 * that would be unusual. An example of why you might want to overwrite it
	 * would be to allow the expression of several genes together or to express
	 * more than one trait from any one gene.
	 * 
	 * In this signature, the colony is not passed. Note, that normally
	 * subclasses do not need to implement this method explicitly.
	 * 
	 * @param phenotype
	 *            the phenotype to which the expressions will be added.
	 * @param genes
	 */
	public abstract <E> void express(Phenotype<P> phenotype, Genes<G> genes);

	/**
	 * This returns the base alphabet for the genome. Typically, the value
	 * returned is "CGAT".
	 * 
	 * @return the alphabet in which the bases are expressed.
	 */
	public abstract String getAlphabet();

	/**
	 * @param index
	 * @return the chromosome defined by <code>index</code> (0..N). Note that
	 *         sex chromosomes are never counted in the index. Thus the first
	 *         non-sex chromosome in the genomic is at index 0, etc.
	 */
	public abstract Chromosome<G> getChromosome(int index);

	/**
	 * Getter/setter typically used by reflection.
	 * 
	 * @return the chromosomes as a collection of Chromosome objects.
	 */
	public abstract Collection<Chromosome<G>> getChromosomes();

	/**
	 * @param locus
	 *            the locus (or null) for which we want the expresser
	 * @return the specific expresser for the given locus else, if no locus
	 *         given, the common expresser for all loci.
	 */
	public abstract Expresser<P> getExpresser(Locus<G> locus);

	public abstract Map<Locus<G>, Expresser<P>> getExpressers();

	/**
	 * @param n
	 *            which must be greater than or equal to 0 and less than
	 *            {@link #getLocusCount()}.
	 * @return the nth locus, by counting through each chromosome.
	 */
	public abstract Locus<G> getLocus(int n);

	/**
	 * @return the total number of loci, over all chromosomes.
	 */
	public abstract int getLocusCount();

	/**
	 * @return the application-specified implementer of {@link Meiosis}
	 */
	public abstract Meiosis<G> getMeiosis();

	/**
	 * @return the mutator
	 */
	public abstract Mutator<G> getMutator();

	/**
	 * @return the total number of loci including sex loci
	 */
	public abstract int getTotalLocusCount();

	/**
	 * TODO eliminate this method. Doesn't belong here.
	 * 
	 * Non-mutating method to mutate this Genome and return a mutated copy.
	 * 
	 * @param genome
	 * 
	 * @return either <code>this</code> Genome or a mutated copy.
	 */
	public abstract Genome<P, G> mutate(Genome<P, G> genome);

	/**
	 * TODO eliminate this method. Doesn't belong here.
	 * 
	 * Normalize the given genome with respect to the reference genome. For
	 * example, if genome and reference are identical to start with, genome will
	 * be modified such that it is now empty. This is particularly useful in
	 * asexual systems.
	 * 
	 * @param genome
	 * @param reference
	 */
	public abstract void normalize(Genome<P, G> genome, Genome<P, G> reference);

	/**
	 * Method to register an implementer of {@link Expresser} for a specific
	 * {@link Locus}.
	 * 
	 * @param locus
	 * @param expresser
	 * @return the previous version of expresser, if any.
	 */
	public abstract Expresser<P> putExpresser(Locus<G> locus, Expresser<P> expresser);

	/**
	 * This is the preferred method to set the chromosomes. Any prior
	 * chromosomes will be discarded.
	 * 
	 * Getter/setter typically used by reflection.
	 * 
	 * @param chromosomes
	 */
	public abstract void setChromosomes(Collection<Chromosome<G>> chromosomes);

	/**
	 * Set the common expresser (for all loci). Note that the expresser map is
	 * used in preference to this expresser.
	 * 
	 * @param expresser
	 */
	public abstract void setExpresser(Expresser<P> expresser);

	/**
	 * @param genome
	 *            the genome to be simplified
	 * 
	 *            TODO consider narrowing genome to Genotype.
	 * 
	 * @return true if the genome has changed as a result of simplification.
	 */
	public abstract boolean simplify(Genome<P, G> genome);

	public static final String STANDARD_ALPHABET = "CGAT"; //$NON-NLS-1$

}