/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualizable.java
 * Created on Aug 6, 2009
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.core;

import java.util.Collection;

import net.sf.tostring0.Identifiable;

/**
 * @author Robin Hillyard
 * 
 */
public interface Visualizable extends Identifiable, Countable, Censusible {

	/**
	 * @return a collection of individuals which make up this
	 *         {@link Visualizable}
	 */
	public abstract Collection<Individual> getIndividuals();

	/**
	 * @return the realm in which this {@link Visualizable} object exists.
	 */
	public abstract Realm getRealm();

	/**
	 * @return an output destination for informational messages
	 */
	public abstract Sink getSink();
}
