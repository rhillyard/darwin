/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Theological.java
 * Created on Jan 5, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

/**
 * Defines the concept of a supreme being which has the power to create living
 * objects out of nothing, and of course kill them at will.
 * 
 * It's a convenient way to get our evolution simulations started, that is to
 * say when we want to play "God".
 * 
 * TODO consider a better name for this concept, such as Theocratic or
 * Omnipotent.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Theological {

	/**
	 * Method to cull all members of an Evolvable so we can make a fresh start.
	 * The complementary method to {@link #seedMembers()}.
	 */
	public abstract void cullMembers();

	/**
	 * Method to seed a this {@link Evolvable} which a certain number of
	 * members.
	 */
	public abstract void seedMembers();

}
