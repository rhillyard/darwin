/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mutator.java
 * Created on Feb 7, 2007
 * @version $Revision: 1.16 $
 */

package net.sf.darwin.core;

import java.util.Map;

import net.sf.tostring0.Identifiable;

/**
 * Defines the methods for gene and genome mutators.
 * 
 * Mutation may occur in one of the following ways:
 * <ul>
 * <li>a change to the bases of a gene which creates a different allele (which
 * may be previously unknown or may correspond to an already known allele;</li>
 * <li>the duplication of a gene, which may or may not have an effect on the
 * phenotype;</li>
 * <li>the elimination of a gene, which may or may not have an effect on the
 * phenotype (but usually there is a negative-fitness effect);</li>
 * </ul>
 * 
 * TODO Allow for other types of mutation, perhaps using a list of Mutators
 * 
 * CONSIDER moving the mutate(Genome) and similar signatures to a different
 * non-parameterized type
 * 
 * @author Robin Hillyard
 */
public interface Mutator<V> extends Identifiable {

	/**
	 * @return true if this mutator does not actually change anything
	 */
	public abstract boolean isIdentity();

	/**
	 * Method to take an allele and mutate it. For instance a gene might have
	 * one base pair copied incorrectly, thus giving rise to a new allele. In
	 * nature, this kind of mutation is rare. By contract, if the allele is
	 * unchanged, it will itself be returned.
	 * 
	 * @param allele
	 *            the original allele
	 * @return either the original allele or a mutated version of it.
	 * 
	 *         TODO it seems to me now that this mutation should operate on a
	 *         Gene, not on an Allele, but there again we have to remember that
	 *         the word "gene" is used in two different ways in common parlance.
	 */
	public abstract Allele<V> mutate(Allele<V> allele);

	/**
	 * CONSIDER eliminating this method from this interface -- it really belongs
	 * in Genome -- Feature Envy: this method should not be here in a Mutator
	 * 
	 * Method to take a genome and mutate it. For instance, a genome might be
	 * mutated if a sequence of genes is "accidentally" repeated, or skipped. In
	 * nature, this kind of mutation is exceptionally rare, but can be found,
	 * for example, in the different human sensitivities to Serotonin. See Matt
	 * Ridley's excellent book "Genomic". By contract, if the allele is
	 * unchanged, it will itself be returned.
	 * 
	 * @param genome
	 *            the original genome
	 * @return either the original genome or a mutated version of it.
	 */
	public abstract <U> Genome<U, V> mutate(Genome<U, V> genome);

	/**
	 * CONSIDER eliminating this method from this interface -- it really belongs
	 * in Genome Feature Envy: this method should not be here in a Mutator
	 * 
	 * Normalize the given genome with respect to the reference genome. For
	 * example, if genome and reference are identical to start with, genome will
	 * be modified such that it is now empty. This is particularly useful in
	 * asexual systems.
	 * 
	 * @param genome
	 *            the genome to normalize
	 * @param reference
	 *            the reference genome
	 */
	public abstract <U> void normalize(Genome<U, V> genome, Genome<U, V> reference);

	/**
	 * CONSIDER eliminating this method from this interface -- it really belongs
	 * in Genome
	 * 
	 * Simplify the genome, typically by removing any non-functional groups of
	 * genes. After simplification, expression should be identical with the
	 * expression before simplification.
	 * 
	 * @param genome
	 *            the original genome
	 * 
	 *            TODO consider narrowing to Genotype
	 * @param expresserMap
	 *            TODO
	 * @return true if the genome has changed as a result of simplification.
	 */
	public abstract <U> boolean simplify(Genome<U, V> genome, Map<Locus<V>, Expresser<U>> expresserMap);
}
