package net.sf.darwin.core;

/**
 * Well, sometimes Java is simply bizarre. The {@link Integer} class is
 * immutable, and cannot be extended to allow mutability. Hence this class
 * Frequency which isn't immutable, but can only be updated by incrementing or
 * decrementing (no multiplication, division, etc.).
 * 
 * Note that this type is not of the usual type family: there is no interface or
 * abstract type to support this, hence we have public non-bean methods that are
 * not defined in an external interface.
 * 
 * @author Robin Hillyard
 * 
 */
public class Frequency implements Comparable<Frequency> {

	/**
	 * construct a new Frequency with value 0.
	 */
	public Frequency() {
		this(0);
	}

	/**
	 * construct a new Frequency with value i.
	 * 
	 * @param i
	 */
	public Frequency(final int i) {
		super();
		this.i = i;
	}

	/**
	 * @param inc
	 *            the amount to increment frequency by
	 */
	public void add(final int inc) {
		this.i += inc;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final Frequency o) {
		final int thisVal = this.i;
		final int anotherVal = o.i;
		return (thisVal < anotherVal ? -1 : (thisVal == anotherVal ? 0 : 1));
	}

	/**
	 * decrement this Frequency by one
	 */
	public void decrement() {
		this.i--;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Frequency other = (Frequency) obj;
		if (this.i != other.i)
			return false;
		return true;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + intValue();
		return result;
	}

	/**
	 * increment this Frequency by one
	 */
	public void increment() {
		this.i++;
	}

	/**
	 * @return the value (field {@link #i})
	 */
	public int intValue() {
		return this.i;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return intValue() + ""; //$NON-NLS-1$
	}

	private int i;
}