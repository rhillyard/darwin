/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ProcessBest.java
 * Created on Dec 29, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * Class to define a callback method for processing a newly crowned "best"
 * object, as managed by the {@link Best} interface.
 * 
 * @author Robin Hillyard
 * 
 * @param <T>
 */
public interface ProcessBest<T extends Identifiable> {

	/**
	 * Method to be called whenever a new {@link Best} object is found.
	 * 
	 * @param object
	 */
	public abstract void onUpdate(final T object);

}