/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: CacheSignature.java
 * Created on Oct 2, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

/**
 * Interface to define the notion of a cache key signature. See
 * {@link #getSignature()} for more detail.
 * 
 * @author Robin Hillyard
 * 
 */
public interface CacheSignature {

	/**
	 * @return a "signature" for this object which can be used to form part of a
	 *         cache key. The signature is similar in a sense to the hash code
	 *         but typically, it is based more on mutable components of an
	 *         object than is the hash code.
	 */
	public abstract String getSignature();

}