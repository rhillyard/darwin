/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Trait.java
 * Created on May 29, 2009
 * @version $Revision: 1.21 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * A {@link Trait} is the phenotypic analog to a (genotypic) {@link Gene}.
 * 
 * A trait has no intrinsic value -- its information content is derived via
 * lookup. The two significant properties of a trait are:
 * <dl>
 * <dt>phenotypic character</dt>
 * <dd>a reference to a {@link Pharacter} which defines what sort of trait this
 * is, for example, flower color;</dd>
 * <dt>variant</dt>
 * <dd>a reference key to a specific {@link Variant} of the phenotypic
 * character, for example, purple or white. The key is used to look up the
 * variant from among those variants which make up the phenotypic character.</dd>
 * </dl>
 * 
 * @author Robin Hillyard
 *
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            trait
 */
public interface Trait<P> extends Expression<P>, Censusible, Identifiable {

	/**
	 * @return the Phenotypic character of which this Trait is a specific value.
	 */
	public abstract Pharacter<P> getCharacter();

	/**
	 * TODO consider eliminating this method.
	 * 
	 * @return this trait's character key
	 */
	public String getCharacterKey();

	/**
	 * @return the object for this Trait which should be able to distinguish it
	 *         from other traits. For a variable trait, return the result of
	 *         {@link #getValue()}. For other traits, return this.
	 */
	public abstract Object getKey();

	/**
	 * TODO consider renaming this method as getParent().
	 * 
	 * @return the phenotype to which this trait belongs.
	 */
	public abstract Phenotype<P> getPhenotype();

	/**
	 * TODO consider eliminating this method.
	 * 
	 * @return the value of the underlying variant.
	 */
	public abstract P getValue();

	/**
	 * TODO consider eliminating this method.
	 * 
	 * @return the variant for this trait.
	 */
	public abstract Variant<P> getVariant();

	/**
	 * @return the key to the variant in the context of the character.
	 */
	public abstract String getVariantKey();

	/**
	 * @param traits
	 */
	public abstract void setPhenotype(Phenotype<P> traits);

}
