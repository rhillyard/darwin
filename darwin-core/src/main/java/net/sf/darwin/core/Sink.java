/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Sink.java
 * Created on Sep 16, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.core;

import java.io.Closeable;
import java.io.Flushable;

/**
 * Interface to capture the essence of place where output can go, a sink in the
 * engineering sense of sources and sinks.
 * 
 * The Java folks should have defined this already.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Sink extends Appendable, Closeable, Flushable {

	/**
	 * 
	 */
	public static final String NEWLINE = "\n"; //$NON-NLS-1$
	// no further signatures
}
