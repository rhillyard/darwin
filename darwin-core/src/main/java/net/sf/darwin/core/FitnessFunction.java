/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction.java
 * Created on Apr 6, 2009
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * @author Robin Hillyard
 * 
 */
public interface FitnessFunction extends Identifiable {

	/**
	 * @return the probability density function for the variate value
	 *         <code>value</code> given a continuous distribution with ideal
	 *         value: <code>target</code> and shape factor: <code>factor</code>.
	 * @param value
	 *            the variate value
	 * @param target
	 *            the ideal value of the variate (typically the mean value for a
	 *            normal distribution).
	 * @param factor
	 *            a shape factor for the distribution (typically the standard
	 *            deviation for a normal distribution).
	 * @throws FitnessException
	 */
	public abstract double getFitness(double value, double target, double factor) throws FitnessException;

	/**
	 * shapeFactor
	 */
	public static final String VAR_SHAPE_FACTOR = "shapeFactor"; //$NON-NLS-1$

	/**
	 * target
	 */
	public static final String VAR_TARGET = "target"; //$NON-NLS-1$

	/**
	 * value
	 */
	public static final String VAR_VALUE = "value"; //$NON-NLS-1$

	/**
	 * $
	 */
	public static final String PREFIX_VAR_RPN = "$"; //$NON-NLS-1$

}
