/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Gene.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.17 $
 */

package net.sf.darwin.core;

/**
 * <p>
 * Defines the operations and properties of a Gene. A Gene is an object which is
 * found at the locus of a genome. In a diploid system there will be two "genes"
 * at any given locus. In most (haploid) systems there is one. A gene can take
 * as its "value" an {@link Allele} which is valid at the given {@link Locus}.
 * </p>
 * <p>
 * Care must be taken to distinguish between a Gene and a {@link Locus}. In this
 * framework, Gene is used to model the actual alleles that are present in the
 * {@link Genome} of a particular {@link Organism}. Locus, on the other hand, is
 * used to model the set of possible alleles that can appear at a locus of a
 * {@link Genomic}.
 * </p>
 * 
 * @author Robin Hillyard
 *
 * @param <G>
 *            the type of the genetic information which is encoded in this Gene
 */
public interface Gene<G> extends Auditable, Ploidy, Basic, Censusible {

	/**
	 * Accessor method to return the indexth allele for this gene.
	 * 
	 * @param index
	 *            which allele to get.
	 * @return the value of the allele at that index.
	 */
	public abstract Allele<G> getAllele(int index);

	/**
	 * @return the number of alleles for the locus of this gene
	 */
	public abstract int getAlleleCount();

	/**
	 * Accessor method to return the key to the indexth allele for this gene.
	 * 
	 * @param index
	 *            which allele to get.
	 * @return key of the allele within the locus, at that index.
	 */
	public abstract String getAlleleKey(int index);

	/**
	 * @return the locus at which this gene appears
	 */
	public abstract Locus<G> getLocus();

	/**
	 * Method to get a mutated copy of a Gene.
	 * 
	 * @param mutator
	 * 
	 * 
	 * @return a mutated copy (which may be identical if no mutations have
	 *         occurred)
	 */
	public abstract Gene<G> mutate(Mutator<G> mutator);

	/**
	 * Mutator method to set the indexth allele for this gene.
	 * 
	 * @param index
	 *            which allele is to be set.
	 * @param allele
	 *            the value of the allele to set.
	 */
	public abstract void setAllele(int index, String allele);

	/**
	 * Set the locus for this gene.
	 * 
	 * @param locus
	 */
	public abstract void setLocus(Locus<G> locus);

}