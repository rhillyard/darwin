/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Fitness.java
 * Created on Apr 1, 2009
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.core;

/**
 * Type representing a pair whose mutual fitness can be estimated. The pair is
 * one Variant and one EcoFactor. They maybe be orthogonal, in which case their
 * weight will be zero. Or they maybe be relevant in which case their weight
 * will be non-zero and their fitness will be a value between 0 and 1.
 * 
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            trait value type
 * @param <E>
 *            eco-factor value type
 */
public interface Fitness<P, E> {

	/**
	 * Compare fitness 1 with fitness 2 (fitter values are higher values).
	 * 
	 * @param fitness1
	 * @param fitness2
	 * @return -1 if fitness1 is greater than fitness2, etc.
	 */
	public int compare(final double fitness1, final double fitness2);

	/**
	 * @return the value that is dependent on the environment
	 */
	public abstract Number getEnvironmentFactor();

	/**
	 * @param trait
	 * @param factor
	 * @return the fitness between 0 (unfit) and 1 (perfect fit).
	 * @throws FitnessException
	 */
	public abstract double getFitness(Trait<P> trait, EcoFactor<E> factor) throws FitnessException;

	/**
	 * @param character
	 *            the identifier (or key) of the phenotypic character whose
	 *            traits/variants will be measured for fitness against the given
	 *            factor.
	 * @param factor
	 * @return the relative weighting for this fitness (0: not a significant
	 *         fitness match)
	 */
	public abstract int getWeight(String character, EcoFactor<E> factor);

	/**
	 * Scale the current adjustment by the factor passed in.
	 * 
	 * @param adjustment
	 *            if 1 is passed in then nothing changes.
	 * @param trait
	 *            XXX
	 */
	public void resetAdjustment(Number adjustment, String trait);

	/**
	 * @param trait
	 *            XXX
	 * @param factor
	 *            XXX
	 */
	public abstract void resetEnvironmentFactor(String trait, EcoFactor<E> factor);
}