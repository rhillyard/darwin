/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Terminal.java
 * Created on Mar 3, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 */
public interface Terminal {

	/**
	 * @return true if we have done enough already to this object (for instance,
	 *         for an evolvable object, this means that we have reached a
	 *         maximum number of generations).
	 */
	public abstract boolean isFinished();

}
