/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * <p>
 * Defines how alleles are chosen during Meiosis.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.9 $
 * @param <G>
 *            the type of the genetic information which is processed by this
 *            Meiosis implementer
 * 
 */
public interface Meiosis<G> extends Identifiable {

	/**
	 * This method determines which allele will be chosen for a particular gene
	 * locus.
	 * 
	 * @param gene
	 *            the gene from which an allele will be chosen
	 * @param index
	 *            a value which will be used to determine the allele to use
	 *            (except if &lt;0 then a random number is chosen).
	 * 
	 * @return the key of the allele chosen.
	 */
	public String doMeiosis(Gene<G> gene, int index);

	/**
	 * TODO this method should be eliminated and implemented in a
	 * non-feature-envy manner
	 * 
	 * This method determines which alleles will be chosen for a complete
	 * chromosome.
	 * 
	 * @param genomic
	 *            the genomic
	 * @param genes
	 *            the genes to peform meiosis on
	 * @param chromosome
	 *            may be null, in which case any gene will qualify
	 * 
	 * @return a haploid genome with loci corresponding to the chromosome (if
	 *         given)
	 */
	public <T> Genome<T, G> doMeiosis(Genomic<T, G> genomic, Genes<G> genes, Chromosome<G> chromosome);

}
