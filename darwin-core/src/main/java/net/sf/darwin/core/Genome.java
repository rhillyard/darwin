/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genome.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.16 $
 */

package net.sf.darwin.core;

/**
 * <p>
 * Defines the genetic information contained in an {@link Organism} belonging to
 * a {@link Taxon}. A Genome may contain any number of instances of {@link Gene}
 * , though in practice the number is determined by the {@link Genomic} which
 * defines this Genome. The structure of Genome is approximately parallel to
 * that of {@link Genomic}.
 * </p>
 * 
 * TODO consider restructuring a Genome such that is contains one or two
 * Genotypes (according to the Ploidy) and that each Genotype is a (haploid)
 * collection of Genes with a single allele each. This would clarify the concept
 * of a Genotype. It would also mean a lot of work.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Genotype
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Genotype
 */
public interface Genome<P, G> extends Genotype<P, G> {

	/**
	 * Flush the cached phenotype for this genome/colony combination. This must
	 * be called for any significant population change in the colony, or change
	 * to the colony's environment. A significant change is one that would
	 * result in a different phenotype being returned.
	 * 
	 * TODO consider implementing VisualizableListener instead, but the
	 * semantics are slightly different in that we want to note changes to
	 * populations and/or environments.
	 * 
	 * @param colony
	 */
	public abstract void environmentChanged(Visualizable colony);

	/**
	 * Express the genes of this Genome in the context of population and return
	 * the result as a Phenotype.
	 * 
	 * @param colony
	 *            the population this genome belongs to, often ignored.
	 * 
	 * @return a Phenotype corresponding to the expressed genes of this Genome,
	 *         in the context of population.
	 */
	public abstract <E> Phenotype<P> express(Colony<E, P, G> colony);

	/**
	 * Method to fuse this (empty) haploid genome with <code>other</code> genome
	 * into a new diploid genome. In theory, at least, there is no implied
	 * ordering of the two gametes. The resulting genome should express as a
	 * phenotype regardless of the ordering of the arguments x and y. At
	 * meiosis, the ordering of the parents x, y is semi-significant - but the
	 * effect should be masked by the seeding of the random number generator for
	 * a mating.
	 * 
	 * This and the other gamete must conform to the same Genomic, otherwise, no
	 * fertilization can take place.
	 * 
	 * TODO in theory, we could allow this and two other Genes parameters to
	 * combine into a triploid result.
	 * 
	 * @param a
	 *            the gamete to be fused into a zygote
	 * @param b
	 *            the gamete to be fused into a zygote
	 * @return the resulting zygote.
	 * 
	 */
	public void fertilize(Genes<G> a, Genes<G> b);

	/**
	 * @param id
	 * @return the Gene whose locus has the identifier id
	 */
	public abstract Gene<G> getGene(String id);

	/**
	 * @return the genomic to which this genome conforms.
	 * 
	 */
	public Genomic<P, G> getGenomic();

	/**
	 * @return the sex of this genome.
	 * @throws GeneticsException
	 */
	public boolean getSex() throws GeneticsException;

	/**
	 * @return a set of genes which is complementary to this genome.
	 */
	public abstract Genes<G> invert();

	public abstract boolean isSexual();

	/**
	 * Method to mutate a Genome using a particular mutator. Implementers of
	 * this method should normally return a copy of this Genome, rather than
	 * simply a reference (although if the mutator is the identity mutator, it
	 * is acceptable to return a reference to the original).
	 * 
	 * @param mutator
	 *            the mutator
	 * @return possibly mutated copy of this (or possibly a reference to the
	 *         unmutated original).
	 */
	public Genome<P, G> mutate(final Mutator<G> mutator);

}