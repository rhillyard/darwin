/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Basic.java
 * Created on Jul 14, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 *         TODO consider moving this method into Genetic or some other
 *         interface. Also consider eliminating {@link #getBases()} since it is
 *         typically the same as {@link CacheSignature#getSignature()}.
 */
public interface Basic extends CacheSignature {

	/**
	 * @return a set of bases in the base alphabet of the genome (e.g. "CAGT")
	 *         which correspond to this genetic object. These bases are used to
	 *         determine certain mutations and crossover situations.
	 */
	public abstract String getBases();

}
