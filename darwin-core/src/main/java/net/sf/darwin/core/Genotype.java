/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genotype.java
 * Created on Aug 13, 2009
 * @version $Revision: 1.28 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * A Genotype is a (diploid or haploid) set of genes which can be manipulated.
 * 
 * TODO consider defining a Genotype as a haploid set of genes.
 * 
 * @author Robin Hillyard
 * 
 */
/**
 * @author robinhillyard
 *
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Genotype
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Genotype
 */
@SuppressWarnings("unused")
public interface Genotype<P, G> extends Auditable, Ploidy, Genes<G>, Censusible, Identifiable {

	/**
	 * Add <code>gene</code> to this Genotype at its end.
	 * 
	 * @param gene
	 *            an implementer of {@link Gene}.
	 * @return true if successful.
	 */
	public abstract boolean addGene(Gene<G> gene);

	/**
	 * Add a Gene to this Genotype at a specific index.
	 * 
	 * @param locus
	 *            the locus of the gene.
	 * @param gene
	 *            the gene to be added.
	 */
	public abstract void addGene(int locus, Gene<G> gene);

	/**
	 * Add the given genes to this Genotype. The new genes will be located at
	 * the end of the current set.
	 * 
	 * @param genes
	 */
	public abstract void addGenes(Genes<G> genes);

	/**
	 * @param frequencyMap
	 */
	public void doCensus(final FrequencyMap<Allele<G>> frequencyMap);

}
