/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GenerationListener.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.core;

/**
 * Defines a callback method for invocation whenever a {@link Evolution} runs.
 * Note that in general, this will happen more frequently than when the
 * population changes.
 * 
 * @author Robin Hillyard
 */
public interface GenerationListener {

	/**
	 * This method is called whenever the {@link Evolvable} implementer
	 * completes a new generation or when the {@link Evolution} itself is
	 * completely exhausted (has no more evolvables to work with).
	 * 
	 * @param evolvable
	 *            the implementer of {@link Evolvable} which has completed a
	 *            generation or null if the {@link Evolution} itself has
	 *            completed.
	 * 
	 *            NOTE: implementers of this method must be prepared to accept
	 *            null as the parameter.
	 */
	public abstract void onGeneration(Evolvable evolvable);

	/**
	 * 1000
	 */
	public static final int $1_SECOND = 1000;
}
