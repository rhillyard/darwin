/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.core;

/**
 * <p>
 * This application-independent exception class is used to track runtime
 * exceptions which may occur in the Darwin classes.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.6 $
 */
public class DarwinException extends RuntimeException {

	/**
	 * @param arg0
	 */
	public DarwinException(final String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DarwinException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Unused empty constructor
	 */
	@SuppressWarnings("unused")
	private DarwinException() {
		// unused constructor
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7313101479634194142L;

}
