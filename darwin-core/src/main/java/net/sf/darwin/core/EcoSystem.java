/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoSystem
 * Created on: Feb 25, 2012
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.Set;

/**
 * This interface represents an EcoSystem which consists of several
 * {@link EcoFactor}s, all of the same type.
 * 
 * @author rhillya
 * 
 */
public interface EcoSystem<E> extends Countable {

	public abstract Object add(final EcoFactor<E> factor);

	public abstract Set<String> factorKeys();

	public abstract double getDoubleValue(final String name);

	public abstract EcoFactor<E> getFactor(final String key);

	public abstract void setEcoFactors(final Collection<EcoFactor<E>> ecoFactors);

	public abstract Collection<EcoFactor<E>> values();
}