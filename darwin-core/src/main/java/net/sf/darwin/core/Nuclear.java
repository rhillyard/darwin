/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Nuclear.java
 * Created on Feb 20, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.core;

/**
 * Defines operations supported by a Nuclear object. This is a {@link Genetic}
 * object which does not belong to a colony (although it records its "born-to"
 * attribute (in the sense that a Navajo will use it - where we replace Clan for
 * Colony) in the colonyId.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Nuclear<P, G> extends Genetic<P, G>, Censusible {

	/**
	 * @return the colony id for this object
	 */
	public abstract String getColonyId();

	/**
	 * Set a new population id for this object (it is changing colonies, or
	 * clans, in mid-life)
	 * 
	 * @param id
	 *            the new id
	 */
	public abstract void setColonyId(String id);
}
