/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Choosy.java
 * Created on Feb 26, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.core;

import com.rubecula.jexpression.EvalExpressionMutable;

/**
 * This interface defines a method which defines how choosy a female is about
 * finding a mate in the same population.
 * 
 * @author Robin Hillyard
 */
public interface Choosy extends EvalExpressionMutable {

	/**
	 * Get the choosiness of a female for finding a mate.
	 * 
	 * @param random
	 * @return a value between 0 and infinity. The higher the value, the more
	 *         choosy she is. A value of 0 means that all males are eligible and
	 *         a female will never need to look outside the population for a
	 *         mate.
	 */
	public abstract double getMinimumDesirability(final double random);

}