/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessCache.java
 * Created on Feb 25, 2012
 * @version $Revision: $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 *         CONSIDER merging this interface with PhenotypeCache
 * 
 */
public interface FitnessCache<P, E> extends Cache {

	/**
	 * @param traits
	 *            the traits being whose fitness has been evaluated.
	 * @param fitnessEngine
	 *            the fitness engine which evaluated the fitness.
	 * @param environment
	 *            the environment the fit object resides in.
	 * @param fitness
	 *            the fitness
	 */
	public abstract void addFitness(final TraitMap<P> traits, final FitnessEngine<P, E> fitnessEngine,
			final Environment<E> environment, final Double fitness);

	/**
	 * Flush the cache for the given combination of traits/engine. Note that you
	 * cannot flush the cache for <i>all</i> fitnesses relating to traits only,
	 * or fitnesses relating to the engine only.
	 * 
	 * @param traits
	 *            the traits being whose fitness has been evaluated.
	 * @param engine
	 *            the fitness engine which evaluated the fitness.
	 * @param environment
	 *            the environment the fit object resides in.
	 */
	public abstract void flush(final TraitMap<P> traits, final FitnessEngine<P, E> engine, final Environment<E> environment);

	/**
	 * CONSIDER removing parameter fitnessEngine
	 * 
	 * CONSIDER implementing a Guava Loading Cache
	 * 
	 * CONSIDER returning Number instead of Double
	 * 
	 * @param traits
	 *            the traits being whose fitness has been evaluated.
	 * @param fitnessEngine
	 *            the fitness engine which evaluates the fitness.
	 * @param environment
	 *            the environment the fit object resides in.
	 * @return the cached phenotype (or null if none) - unless it is not active
	 *         in which case always null.
	 */
	public abstract Double getFitness(final TraitMap<P> traits, final FitnessEngine<P, E> fitnessEngine,
			final Environment<E> environment);

	/**
	 * register the given fitness engine with this cache. Once done, any fitness
	 * function changes which occur will cause this cache to be completely
	 * flushed.
	 * 
	 * @param fitnessEngine
	 */
	public abstract void register(final FitnessEngine<P, E> fitnessEngine);

}