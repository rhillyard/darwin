/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenotypeCache.java
 * Created on Feb 25, 2012
 * @version $Revision: $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * @author Robin Hillyard
 * 
 */
public interface PhenotypeCache<P> extends Cache {

	/**
	 * @param genes
	 *            the genes being expressed
	 * @param identifiable
	 *            the identifiable object, typically a population.
	 * @param phenotype
	 */
	public abstract void addPhenotype(final Basic genes, final Identifiable identifiable, final Phenotype<P> phenotype);

	/**
	 * @param genes
	 * @param identifiable
	 */
	public abstract void flush(final Basic genes, final Identifiable identifiable);

	/**
	 * @param genes
	 *            the genes from which we derive the phenotype
	 * @param identifiable
	 *            the identifiable object (and its environment).
	 * @return the cached phenotype (or null if none) - unless it is not active
	 *         in which case always null.
	 */
	public abstract Phenotype<P> getPhenotype(final Basic genes, final Identifiable identifiable);

}