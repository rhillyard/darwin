/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution.java
 * Created on Aug 2, 2009
 * @version $Revision: 1.21 $
 */

package net.sf.darwin.core;

import java.applet.Applet;
import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * This interface defines the concept of an Evolution. This concept is central
 * to the <b><i>Darwin</i></b> framework and forms part of a triad of
 * fundamental concepts:
 * <ul>
 * <li>{@link Evolution};</li>
 * <li>{@link Evolvable}, typically a {@link Taxon};</li>
 * <li>user interface, typically, but not necessarily, an {@link Applet}.</li>
 * </ul>
 * 
 * The properties of an Evolution are as follows:
 * <ul>
 * <li>a map of {@link Evolvable} evolvables and their generation frequencies
 * (in ticks of the clock);</li>
 * <li>a list (unordered) of {@link GenerationListener}s -- after each tick, any
 * evolver which has undergone a new generation will be passed to this listener;
 * </li>
 * <li>two modes of running:
 * <ul>
 * <li>continuous running (with actions <code>start</code>, <code>pause</code>,
 * <code>resume</code>, <code>stop</code>)</li>
 * <li>stepping (with action <code>next</code>)</li>
 * </ul>
 * </li>
 * <li>active (a read-only property) -- see {@link #isActive(boolean)};</li>
 * <li>the ability to schedule an (arbitrary) {@link Runnable} event at a future
 * time;</li>
 * <li>the ability to seed the evolvables by invoking their
 * {@link Evolvable#seedMembers()} methods (this is really just a convenience
 * method).</li>
 * </ul>
 * </p>
 * <p>
 * Note that in general, there will be two modes of updating the user interface,
 * during evolution. The generation listeners fire at the completion of each
 * generation. If you also want to visualize updates at more points during the
 * completion of a generation, then you should also set up
 * {@link VisualizableListener}s on an {@link Visualizable} objects (such as
 * {@link Population}s or {@link Environment}s).
 * 
 * </p>
 * 
 * @author Robin Hillyard
 * 
 */
public interface Evolution extends Evolver, Runnable {

	/**
	 * @param ignoreEvents
	 *            XXX
	 * @return true if this evolution is currently active (with valid timer,
	 *         task and evolvables).
	 * @throws EvolutionException
	 */
	public abstract boolean isActive(boolean ignoreEvents) throws EvolutionException;

	/**
	 * @return true if {@link EvolutionTask#isPaused()} returns true for the
	 *         evolution task.
	 * 
	 *         XXX is this OK? what if evolutionTask is null?
	 * 
	 * @see com.rubecula.darwin.evolution.EvolutionTask#isPaused()
	 */
	public boolean isPaused();

	/**
	 * suppress all new generations until {@link #resume()} is called.
	 * 
	 * @throws EvolutionException
	 */
	public abstract void pause() throws EvolutionException;

	/**
	 * cancel the previous call of {@link #pause()}.
	 * 
	 * @throws EvolutionException
	 */
	public abstract void resume() throws EvolutionException;

	/**
	 * Schedule an arbitrary event for execution after a specific number of
	 * milliseconds. The event will fire is scheduled independently of any new
	 * generations (and will run in a different thread).
	 * 
	 * Typically, this will be used to change a property in the environment at a
	 * certain time.
	 * 
	 * TODO consider implementing in such a way that we can specify a certain
	 * number of ticks (the ability to pause/resume makes this complex).
	 * 
	 * @param millisecs
	 *            the number of millisecs before the event is to be run.
	 * @param event
	 *            the event to be run.
	 * @return the {@link Future} for this event, returned by the scheduler
	 */
	public abstract Future<?> scheduleEvent(long millisecs, Runnable event);

	/**
	 * A terminal shut-down of the Evolution process (including both evolution
	 * and scheduled events). Subsequent calls to this Evolution object will not
	 * succeed. Compare {@link #pause()} and {@link #stop()}.
	 * 
	 * @return the result of calling
	 *         {@link ScheduledExecutorService#awaitTermination(long, TimeUnit)}
	 *         which, roughly speaking, will be true if everything shut down in
	 *         an orderly fashion.
	 * @throws EvolutionException
	 * @see net.sf.darwin.core.Evolution#stop()
	 */
	public abstract boolean shutdown() throws EvolutionException;

	/**
	 * 
	 * @return true if everything is OK after the stop.
	 * @throws EvolutionException
	 */
	public abstract boolean stop() throws EvolutionException;

	/**
	 * @return true if we have permission to stop this {@link Evolution}.
	 */
	public boolean stoppable();

	/**
	 * Wait for this {@link Evolution} to become inactive, then return.
	 * 
	 * @param delay
	 *            delay between tests (in milliseconds)
	 * 
	 * @return true if the evolution is really complete.
	 * @throws EvolutionException
	 */
	public abstract boolean waitUntilComplete(final int delay) throws EvolutionException;

	/**
	 * TODO I don't think we actually need this.
	 * 
	 * @param writer
	 * @throws IOException
	 */
	public abstract void writeEvolvables(final Writer writer) throws IOException;

}