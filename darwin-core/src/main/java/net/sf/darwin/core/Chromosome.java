/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Chromosome.java
 * Created on May 31, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.core;

import java.util.Collection;

import net.sf.tostring0.Identifiable;

/**
 * <p>
 * Defines part of a Genomic.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.9 $
 */
public interface Chromosome<V> extends Auditable, SexLinked, Progenitor, Identifiable {

	/**
	 * Method to add a Locus at the specified index.
	 * 
	 * @see java.util.List#add(int, java.lang.Object)
	 * @param index
	 *            the index at which the new object will appear.
	 * @param locus
	 *            a locus to be inserted into this Genomic.
	 * @throws GeneticsException
	 */
	public abstract void addLocus(int index, Locus<V> locus) throws GeneticsException;

	/**
	 * Method to add a Locus at the end of this Genomic.
	 * 
	 * @see #addLocus(int, Locus)
	 * @param locus
	 *            a locus to be inserted into this Genomic.
	 * @return true if the locus is added successfully.
	 * @throws GeneticsException
	 */
	public abstract boolean addLocus(Locus<V> locus) throws GeneticsException;

	public abstract String getAlphabet();

	/**
	 * @return the number of loci in this chromosome
	 */
	public abstract int getCount();

	/**
	 * Getter/setter typically used by reflection.
	 * 
	 * @return the loci as a collection of Locus objects.
	 */
	public abstract Collection<Locus<V>> getLoci();

	/**
	 * @param index
	 * @return the locus defined by <code>index</code> (0..N)
	 */
	public abstract Locus<V> getLocus(int index);

	/**
	 * This is now the preferred method to set the loci. Any prior loci will be
	 * forgotten.
	 * 
	 * Getter/setter typically used by reflection.
	 * 
	 * @param loci
	 */
	public abstract void setLoci(Collection<Locus<V>> loci);

	/**
	 * sex
	 */
	public static final String SEX = "sex"; //$NON-NLS-1$

}