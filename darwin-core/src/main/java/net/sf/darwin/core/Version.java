/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Version.java
 * Created on Oct 20, 2009
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.core;

/**
 * This interface is used solely to provide the version number of the Darwin
 * framework.
 * 
 * Note that it is necessary to update this version whenever a new release is
 * made.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Version {

	/**
	 * The current version of the Darwin framework
	 */
	final static String DARWIN_VERSION = "3.0.0"; //$NON-NLS-1$

}
