/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Progenitor.java
 * Created on Jul 1, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

/**
 * This marker interface represents the "parent" of a {@link Locus}. The parent
 * may be either the {@link Chromosome} in which the {@link Locus} is found, or
 * the predecessor locus in a group of polygenic loci.
 * 
 * <pre>
 * So with double-shotted guns and colours nailed unto the mast,
 * I tamed your insignificant progenitor at last!
 * </pre>
 * 
 * W.S. Gilbert, <i>The Gondoliers</i>.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Progenitor {
	// marker interface for Chromosome and Locus
}
