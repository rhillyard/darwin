/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mortal.java
 * Created on Feb 20, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.core;

/**
 * Interface to define the operations supported by a "mortal" object, i.e. one
 * that has a finite lifespan.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Mortal extends Individual {

	/**
	 * Mutating method to "age" this organism by one generation.
	 */
	public abstract void age();

	/**
	 * @return the current age of this organism
	 */
	public abstract int getAge();

	/**
	 * @return true if this organism is part of a population model (generally
	 *         this is true, except temporarily when an organism is first born
	 *         or newly dead)
	 */
	public abstract boolean isViable();

	/**
	 * @param viable
	 *            if false, then we (temporarily) mark this organism as not
	 *            being part of a population model.
	 */
	public abstract void setViable(boolean viable);

}
