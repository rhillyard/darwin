/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Environment.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.30 $
 */

package net.sf.darwin.core;

import java.util.Collection;

/**
 * Defines the concept of an Environment, modeled as a map of {@link EcoFactor}
 * objects. A {@link Population} of {@link Organism}s exist in an Environment,
 * whose properties (the EcoFactor objects) can vary over time.
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <E>
 *            the type of environmental information modeled by this Environment
 */
public interface Environment<E> extends Visualizable, CacheSignature, Cloneable {

	/**
	 * Add an eco factor to this environment.
	 * 
	 * This is not the favored method to add eco factors to an environment.
	 * Normally, we would do them all at once using
	 * {@link #setEcoFactors(Collection)}.
	 * 
	 * @param factor
	 *            the eco factor, whose identifier will be used as the key.
	 * @return the previous eco factor with the same identifier (if any),
	 *         otherwise null.
	 */
	public abstract Object addFactor(EcoFactor<E> factor);

	/**
	 * Mutating method to add a listener to changes in this Environment.
	 * 
	 * @param listener
	 *            the listener.
	 * @return true if the listener was successfully added.
	 */
	public abstract boolean addListener(EnvironmentListener<E> listener);

	/**
	 * Method to allow an external object to notify this Environment that it has
	 * changed, typically as the result of an external agent, for example a user
	 * action which has invoked {@link #update(String, Object)}.
	 */
	public abstract void fireEnvironmentChanged();

	/**
	 * Method to retrieve the value of the eco factor defined by
	 * <code>name</code>.
	 * 
	 * @param name
	 *            the name (identifier, key) of the eco factor.
	 * @return the eco factor value, as a double. If the value cannot be
	 *         rendered as double, the result is undefined.
	 * 
	 *         XXX consider eliminating this method.
	 */
	public abstract double getEcoFactorValue(String name);

	/**
	 * @return the eco system for this Environment.
	 * 
	 *         CONSIDER having multiple eco systems in an Environment, each with
	 *         a different type V
	 */
	public abstract EcoSystem<E> getEcoSystem();

	/**
	 * @return the ideal population
	 */
	public abstract long getIdealPopulation();

	/**
	 * @return the current set of environment listeners.
	 * 
	 *         Getter/Setter typically called by reflection.
	 */
	public abstract Collection<EnvironmentListener<E>> getListeners();

	/**
	 * Initialize this environment programmatically (normally, initialization is
	 * done in the configuration).
	 */
	public abstract void init();

	public abstract void setEcoSystem(final EcoSystem<E> ecoSystem);

	/**
	 * @param idealPopulation
	 */
	public abstract void setIdealPopulation(long idealPopulation);

	/**
	 * Reset the set of environment listeners according to the collection
	 * provided. Any prior listeners will be lost.
	 * 
	 * @param listeners
	 * 
	 *            Getter/Setter typically called by reflection.
	 */
	public abstract void setListeners(Collection<EnvironmentListener<E>> listeners);

	/**
	 * Find and set the eco factor to have the factor value, then, if
	 * successful, invoke {@link #fireEnvironmentChanged()}.
	 * 
	 * @param factorKey
	 * @param factorValue
	 * @return true if the environment was updated.
	 */
	public abstract boolean update(String factorKey, E factorValue);

	/**
	 * 100
	 */
	public static final long IDEAL_POPULATION_DEFAULT = 100L;

	/**
	 * 
	 */
	public static final String S_ID_UNIDENTIFIED = "<unidentified>"; //$NON-NLS-1$

}