/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Attraction.java
 * Created on Jun 18, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.core;

/**
 * This interface enables sexual selection.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Attraction {

	/**
	 * Compare the sex-linked traits of the male and the female and determine
	 * the attraction. The attraction isn't necessarily mutual -- it may only
	 * apply to one sex for the other.
	 * 
	 * @param female
	 *            the sex-linked traits for the female.
	 * @param male
	 *            the sex-linked traits for the male.
	 * 
	 * @return a value: 1.0 very attractive; 0.0 unattractive.
	 */
	public abstract <T> double getAttraction(TraitMap<T> female, TraitMap<T> male);

	/**
	 * @return true if all pairs of trait maps will yield the same attraction
	 *         value.
	 */
	public abstract boolean isUniform();

}
