/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Taxon.java
 * Created in 2007
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

import java.io.IOException;
import java.util.Collection;

/**
 * Defines the methods supported by a unit of evolutionary computation (Taxon).
 * Try not to get too hung up on the name "Taxon": a rose by any other name...
 * Indeed, "unit", "system", "species", "clade", "phylogenetic entity", etc.
 * would all be reasonable names for this type family. Essentially a Taxon is an
 * evolvable object which "owns" a collection of {@link Organism}s which all
 * share the same {@link Genomic} and {@link Phenome}, in other words what we
 * commonly refer to as a <i>species</i>. If you want to model the interaction
 * of two such species, for example predator and prey, then you need to have
 * more than one taxon in your Evolution. The chief properties (fixed unless
 * otherwise specified) of a taxon are:
 * <dl>
 * <dt>realm</dt>
 * <dd>the implementer of {@link Realm} (i.e. the "model") to which the taxon
 * belongs;</dd>
 * <dt>populations</dt>
 * <dd>a (variable) set of {@link Population} instances but TODO populations
 * should belong to the realm, not the taxon;</dd>
 * <dt>fecundity</dt>
 * <dd>the implementer of the {@link Fecundity} interface, which defines how
 * well organisms of the taxon reproduce;</dd>
 * <dt>genomic</dt>
 * <dd>the implementer of the {@link Genomic} interface, which defines the
 * genomic organization for all organisms of the taxon;</dd>
 * <dt>phenome</dt>
 * <dd>the implementer of the {@link Phenome} interface, which defines the
 * phenome for all organisms of the taxon;</dd>
 * <dt>mortality</dt>
 * <dd>the implementer of the {@link Mortality} interface, which defines how
 * likely organisms of the taxon are to die;</dd>
 * <dt>chooser</dt>
 * <dd>the implementer of the {@link MateChoice} interface, which defines how
 * organisms of the taxon choose a mate;</dd>
 * <dt>population listeners</dt>
 * <dd>a (variable) set of implementers of the {@link VisualizableListener}
 * interface, each of which will be notified via its callback method when the
 * population changes (typically at the end of each generation)</dd>
 * <dt>census taker</dt>
 * <dd>the implementer of the {@link Census} interface, which defines how census
 * operations are performed on the organisms.</dd>
 * </dl>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.2 $
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            taxon
 * @param <G>
 *            the type of the genetic information which is encoded in this taxon
 */
public interface Taxon<E, P, G> extends Evolvable, Registry<E, P, G>, HasPhenotypeCache, Censusible {

	/**
	 * Mutating method to add a population to the system.
	 * 
	 * @param population
	 *            an implementer of {@link Population}.
	 * @return true if the population was added successfully.
	 */
	public abstract int addPopulation(Population<E, P, G> population);

	/**
	 * Mutating method to add, for each of this system's populations, a
	 * listener, i.e. an implementer of {@link VisualizableListener}.
	 * 
	 * @param listener
	 *            the listener of population change events.
	 */
	public abstract void addVisualizableListener(VisualizableListener listener);

	/**
	 * Method to enumerate this population for census purposes.
	 * 
	 * @throws IOException
	 * 
	 */
	public abstract void doCensus() throws IOException;

	/**
	 * @return the application-specific implementer of {@link Census}.
	 */
	public abstract Census getCensusTaker();

	/**
	 * @return the application-specific implementer of {@link MateChoice}.
	 */
	public abstract MateChoice getChooser();

	/**
	 * @return the application-specific implementer of {@link Fecundity}.
	 */
	public abstract Fecundity getFecundity();

	/**
	 * @return the Genomic for this {@link Taxon} System.
	 */
	public abstract Genomic<P, G> getGenomic();

	/**
	 * @return the application-specific implementer of {@link Mortality}.
	 */
	public abstract Mortality getMortality();

	/**
	 * Method to get the phenome for this system, i.e. the implementer of
	 * {@link Phenome}.
	 * 
	 * @return value of (private) field _Phenome.
	 */
	public abstract Phenome<P> getPhenome();

	/**
	 * @param key
	 * @return the property for the key.
	 */
	public abstract Object getProperty(Object key);

	/**
	 * @return the realm to which this system belongs
	 */
	public abstract Realm getRealm();

	/**
	 * @return the seed population
	 * 
	 */
	public abstract int getSeedPopulation();

	/**
	 * Getter/setter typically called by reflection.
	 * 
	 * @return the population listeners as a collection.
	 */
	public abstract Collection<VisualizableListener> getVisualizableListeners();

	/**
	 * @return true if this is the last generation we will process
	 */
	public abstract boolean isLastGeneration();

	/**
	 * @param populations
	 * 
	 *            Getter/setter typically called by reflection.
	 * 
	 */
	public abstract void setPopulations(Collection<Population<E, P, G>> populations);

	/**
	 * Method to set an arbitrary property for this system.
	 * 
	 * @param key
	 * @param value
	 * @return the previous value of the property for this key, if any.
	 */
	public abstract Object setProperty(String key, Object value);

	/**
	 * Getter/setter typically called by reflection.
	 * 
	 * @param listeners
	 */
	public abstract void setVisualizableListeners(Collection<VisualizableListener> listeners);

}