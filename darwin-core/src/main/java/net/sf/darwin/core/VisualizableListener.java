/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizableListener.java
 * Created on Feb 2, 2007
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.core;

/**
 * Defines the mechanism for notifying interested objects about changes in a
 * {@link Visualizable}, such as a {@link Population}.
 * 
 * CONSIDER refactoring so that this is in api package
 * 
 * @author Robin Hillyard
 * 
 */
public interface VisualizableListener {

	/**
	 * This method is a callback to notify the listener that a the
	 * <code>source</code> has changed and may need an update to the visual
	 * representaion.
	 * 
	 * @param source
	 *            the source of the change, usually a population.
	 * @param context
	 *            XXX
	 */
	public abstract void onChange(Visualizable source, Object context);
}
