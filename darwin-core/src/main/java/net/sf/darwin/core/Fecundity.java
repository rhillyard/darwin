/**
 * Created on Oct 22, 2003
 *
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package net.sf.darwin.core;

/**
 * <p>
 * Defines methods to determine how fecund will be a particular mating in a
 * population, i.e. how many progeny should be created per generation for each
 * mating pair.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.4 $
 * 
 */
public interface Fecundity {

	/**
	 * Determine the number of progeny for each pair per generation.
	 * 
	 * @param colony
	 *            the colony to which the progeny will be born.
	 * 
	 * @return a number greater than 0
	 */
	public <E, P, G> int getFecundity(Colony<E, P, G> colony);
}
