/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExpressionMap.java
 * Created on Sep 23, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.rubecula.jexpression.EvalExpressionMutable;

/**
 * @author Robin Hillyard
 * 
 *         TODO consider extending AToString
 */
public class ExpressionMap {

	/**
	 * 
	 */
	public ExpressionMap() {
		super();
		this._map = new HashMap<>();
	}

	/**
	 * TODO consider renaming this as putExpression.
	 * 
	 * @param key
	 * @param value
	 * @return the previous value, if any
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	public EvalExpressionMutable addExpression(final String key, final EvalExpressionMutable value) {
		return getMap().put(key, value);
	}

	/**
	 * 
	 * @see java.util.Map#clear()
	 */
	public void clear() {
		getMap().clear();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExpressionMap other = (ExpressionMap) obj;
		if (getMap() == null) {
			if (other.getMap() != null)
				return false;
		} else if (!getMap().equals(other.getMap()))
			return false;
		return true;
	}

	/**
	 * @param key
	 * @return the
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public EvalExpressionMutable get(final Object key) {
		return getMap().get(key);
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getMap() == null) ? 0 : getMap().hashCode());
		return result;
	}

	/**
	 * @return value of {@link Map#isEmpty()} for {@link #_map}
	 * @see java.util.Map#isEmpty()
	 */
	public boolean isEmpty() {
		return getMap().isEmpty();
	}

	/**
	 * @return value of {@link Map#keySet()} for {@link #_map}
	 * @see java.util.Map#keySet()
	 */
	public Set<String> keySet() {
		return getMap().keySet();
	}

	/**
	 * @param map
	 *            the map to be merged in with this.
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	public void putAll(final ExpressionMap map) {
		for (final String key : map.keySet())
			addExpression(key, map.get(key));
	}

	/**
	 * @return value of {@link Map#size()} for {@link #_map}
	 * @see java.util.Map#size()
	 */
	public int size() {
		return getMap().size();
	}

	/**
	 * @return {@link #_map}
	 */
	private Map<String, EvalExpressionMutable> getMap() {
		return this._map;
	}

	private final Map<String, EvalExpressionMutable> _map;

}
