/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Allele.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * Models the concept of an Allele, that is to say a specific instance of a
 * {@link Gene}s which "compete" for presence at a {@link Locus}
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Allele
 */
public interface Allele<G> extends Attribute<String, G>, Basic, Censusible, Identifiable {

	/**
	 * @return the locus to which this allele can belong.
	 */
	public abstract Locus<G> getLocus();

	/**
	 * @param locus
	 */
	public abstract void setLocus(Locus<G> locus);

}