/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Replicator.java
 * Created on Mar 5, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 *         CONSIDER can we remove the generic types?
 * 
 */
public interface Replicator<P, G> {

	/**
	 * Method to create a "gamete", a single (haploid) set of genes, for example
	 * an X or Y chromosome in the human genome.
	 * 
	 * @param genes
	 *            the Genome from which the gamete will be created via a process
	 *            of meiosis.
	 * @return a newly constructed gamete in the form of a genome.
	 */
	public abstract Genome<P, G> createGamete(Genes<G> genes);
}