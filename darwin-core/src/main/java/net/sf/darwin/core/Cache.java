/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Cache.java
 * Created on Oct 1, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.core;

/**
 * This defines a general caching mechanism where the cache is made up of
 * entries of the form: <code>key -> value</code>, for example when some
 * significant amount of calculation is required to generate value and we
 * therefore prefer to look it up to see if it's already been calculated. The
 * key is what we use to look it up (and therefore each key should be unique and
 * typically, but not necessarily, point to a different value.
 * 
 * TODO consider moving this to the foundation package (along with other
 * {@link Cache}-related classes)
 * 
 * @author Robin Hillyard
 * 
 */
public interface Cache extends Cache_MBean {

	/**
	 * Clear all entries from the cache
	 */
	public abstract void flush();

}
