/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Colony.java
 * Created on Aug 6, 2009
 * @version $Revision: 1.20 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.List;

/**
 * This super-interface of {@link Population} addresses the more basic aspects
 * of an {@link Evolvable}, {@link Visualizable} and {@link Countable} group.
 * 
 * In particular, this interface knows nothing about {@link Organism}s.
 * 
 * TODO consider switching names between this and {@link Population}.
 * 
 * @author Robin Hillyard
 * 
 * @param <E>
 *            the type of the information in the environment which is inhabited
 *            by this colony
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            colony
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            colony
 */
public interface Colony<E, P, G> extends Visualizable, Theological, Cloneable, Insular {

	/**
	 * 
	 * @param organism
	 * @return true if we added the organism
	 */
	public abstract boolean addOrganism(Organism<E, P, G> organism);

	/**
	 * Cleanup (that's a euphemism) the organisms in this Colony. Normally, the
	 * cleanup consists of killing off all of the "marked" organisms. This
	 * method is called for each Colony of a population during the invocation of
	 * the {@link Population#nextGeneration()} method.
	 */
	public abstract void cleanupGeneration();

	/**
	 * Method to yield an unique identifier within this colony.
	 * 
	 * @return a string which represents the next integer in a sequence.
	 * 
	 *         TODO use the generation number for id
	 * 
	 *         TODO consider putting this in a factory class
	 */
	public abstract String createIdentifier();

	/**
	 * Method to get the environment in which this colony thrives.
	 * 
	 * @return the environment.
	 */
	public abstract Environment<E> getEnvironment();

	/**
	 * 
	 * @return a collection of all the Organisms in this Population
	 */
	public abstract Collection<Organism<E, P, G>> getOrganisms();

	/**
	 * @return the population to which this colony belongs
	 */
	public abstract Population<E, P, G> getPopulation();

	/**
	 * Method to determine the population saturation. One of the Eco Factors is
	 * the ideal population which the environment can support.
	 * 
	 * @return current population divided by ideal population (unless the latter
	 *         is undefined in which case the value 1 is returned).
	 */
	public abstract double getSaturation();

	/**
	 * @param fittestOriginal
	 *            the fitness value of the fittest we know of so far
	 * @param pardonsPerColony
	 * @return a new value of fitness
	 * @throws FitnessException
	 */
	public abstract double grantPardons(double fittestOriginal, int pardonsPerColony) throws FitnessException;

	/**
	 * @param viability
	 *            the viability which will determine whether an organism is
	 *            viable or not.
	 * @throws FitnessException
	 */
	public abstract void markDeadOrganisms(Viability viability) throws FitnessException;

	/**
	 * @param bestOrganism
	 */
	public abstract void normalizeGenomes(Organism<E, P, G> bestOrganism);

	/**
	 * @param context
	 *            something that characterizes what caused the population change
	 * 
	 */
	public abstract void populationChanged(Object context);

	/**
	 * Set the index of this population within its parent Taxon System.
	 * 
	 * TODO consider removing this and always use {@link List#indexOf(Object)}.
	 * 
	 * @param index
	 */
	public abstract void setIndex(int index);

	/**
	 * @param isolated
	 *            the isolated to set
	 */
	public abstract void setIsolated(boolean isolated);

	/**
	 * Set the number of daughter colonies we expect this colony to have.
	 * 
	 * @param size
	 */
	public abstract void setNDaughters(int size);

	/**
	 * @param population
	 *            the population to set
	 */
	public abstract void setPopulation(Population<E, P, G> population);

	/**
	 * @return true if all OK
	 */
	public abstract boolean setupGeneration();

	/**
	 * @param n
	 *            the range of integers from which to choose (i.e. the odds
	 *            against spin returning true).
	 * @return true if the random number source yields a zero
	 */
	public abstract boolean spin(int n);

	/**
	 * Thin (decimate) the organisms of this colony by a factor of
	 * <code>thinFactor</code>. Literal decimation would occur if the thinFactor
	 * was 10.
	 * 
	 * @param thinFactor
	 *            the factor by which the population is to be reduced.
	 * @throws FitnessException
	 */
	public abstract void thin(int thinFactor) throws FitnessException;

}
