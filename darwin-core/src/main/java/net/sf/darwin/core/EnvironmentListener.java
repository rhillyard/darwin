/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EnvironmentListener.java
 * Created on Feb 2, 2007
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.core;

/**
 * Defines the mechanism for notifying interested objects about changes in an
 * environment.
 * 
 * @author Robin Hillyard
 *
 * @param <E>
 *            the type of environmental information that will be changed.
 *            Tempting to make E a generic only on the method, but for
 *            consistency, it must be in the type itself.
 */
public interface EnvironmentListener<E> {

	/**
	 * This method is called on a environment listener to notify it that the
	 * environment <code>p</code> has changed.
	 * 
	 * @param env
	 *            the environment which has changed.
	 */
	public abstract void onEnvironmentChange(Environment<E> env);
}
