/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.core;

/**
 * <p>
 * Defines how mortality is calculated for individual objects (organisms) in
 * Taxons.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.4 $
 */
public interface Mortality {

	/**
	 * Method to calculate the mortality for an organism of a given age and
	 * fitness in an environment.
	 * 
	 * @param age
	 *            the age of the organism in generations.
	 * @param fitness
	 *            the fitness of the organism for an environment.
	 * @param saturation
	 *            a measure of the current population saturation (1 corresponds
	 *            to the ideal population supportable by the environment.
	 * @return the probability of death before the next generation (between 0
	 *         and 1).
	 * 
	 *         TODO consider inlining this into isMarked (ie. pass in age,
	 *         fitness, saturation, and return boolean)
	 */
	public abstract double calculateMortality(int age, double fitness, double saturation);

	/**
	 * @param mortality
	 *            the probability that an organism will die before the next
	 *            generation (a value between 0 and 1).
	 * @return true if that organism is actually marked for death (i.e. if the
	 *         mortality value exceeds the current threshold)
	 * 
	 */
	public abstract boolean isMarked(double mortality);

}