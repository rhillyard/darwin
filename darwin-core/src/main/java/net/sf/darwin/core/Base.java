/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Base.java
 * Created on Jul 17, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

import java.io.Serializable;

import net.sf.tostring0.AToString;

/**
 * This is the base class used by most of the classes in Darwin.
 * 
 * The properties of the class are:
 * <ul>
 * <li>it is Serializable</li>
 * <li>it extends the AToString class which makes debugging easy. If you prefer
 * not to use the ToString package, thus necessitating writing your own
 * toString() methods, or allowing Eclipse, or whatever to create them for you,
 * then all you have to do here is have Base extend {@link Object}.</li>
 * </ul>
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
public abstract class Base extends AToString implements Serializable {

	/**
	 * 
	 */
	protected Base() {
		super();
	}

}
