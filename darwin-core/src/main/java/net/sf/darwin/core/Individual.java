/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Individual.java
 * Created on Aug 6, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * This concept represents an individual member of a {@link Visualizable}
 * community, an {@link Organism}, for example.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Individual extends Identifiable {

	/**
	 * @return the {@link Visualizable} object of which this {@link Individual}
	 *         is a member.
	 */
	public abstract Visualizable getVisualizable();

	/**
	 * @return true if this individual has not yet been visualized.
	 */
	public abstract boolean isNew();

	/**
	 * @return true if this individual should be visualized.
	 */
	public abstract boolean isVisible();

}
