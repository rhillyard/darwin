/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Phenotype.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.17 $
 */

package net.sf.darwin.core;

import java.util.Collection;

/**
 * <p>
 * Defines the methods supported by instances of Phenotype interface. The
 * phenotype can be evaluated for fitness in the context of an environment. It
 * can also be evaluated for attraction in the context of a sexual mating
 * ("sexual selection").
 * </p>
 * <p>
 * A phenotype consists of a Set of Traits backed by HashMap. The set of traits
 * is defined by the system's Phenome.
 * </p>
 * 
 * TODO consider renaming this family of types as Phenome...
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.17 $
 * 
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Phenotype
 */
public interface Phenotype<P> extends Fit, Auditable, TraitMap<P> {

	/**
	 * @param expression
	 */
	public abstract void addExtended(ExPhen<P> expression);

	/**
	 * @param key
	 * @param trait
	 * @return the key of the newly added trait
	 */
	public abstract String addTrait(String key, Trait<P> trait);

	/**
	 * Add the given <code>trait</code> to this Phenotype.
	 * 
	 * @param trait
	 *            the trait.
	 * @return the key that this trait is indexed under.
	 */
	public abstract String addTrait(Trait<P> trait);

	/**
	 * @param character
	 * @param traitFrequencies
	 */
	public void doCensus(final Pharacter<P> character, final FrequencyMap<Trait<P>> traitFrequencies);

	/**
	 * @return a collection of extended phenotype, {@link ExPhen}, objects.
	 */
	public abstract Collection<ExPhen<P>> getExtendedPhenotypes();

	/**
	 * @return the sex-linked traits as a TraitMap
	 */
	public abstract TraitMap<P> getSexTraits();

}