/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExPhen.java
 * Created on Jun 30, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.core;

/**
 * Type to represent an "extended phenotype" that is to say a variant of an
 * {@link EcoFactor} which has been modified by an organism whose {@link Genome}
 * is expressed as the {@link ExPhen}.
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            extended phenotype
 */
public interface ExPhen<P> extends Expression<P> {

	/**
	 * Using the value of this extended phenotype, determine whether or not to
	 * update the environment accordingly.
	 * 
	 * By way of an illustration, suppose our extended phenotype is a dam built
	 * by a beaver. Is it substantial enough to hold back water (the criterion)?
	 * If so, determine how much the water level will rise and mark the
	 * appropriate area as being under water (i.e. change the environment).
	 * 
	 * @param criterion
	 *            application-specific criterion for determining how to process
	 *            this ExPhen
	 * @return true if the environment has been updated.
	 */
	public abstract boolean applyToEnvironment(Object criterion);

	/**
	 * @return a modified version of one of the eco factors belonging to the
	 *         environment in which the expressed {@link Genome} resides.
	 */
	public abstract EcoFactor<P> getEcoFactor();

	/**
	 * @return the "value" of this extended phenotype.
	 */
	public abstract Object getValue();
}
