/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Locus.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import net.sf.tostring0.Identifiable;

/**
 * <p>
 * Defines a Locus at which one of several Alleles may appear. The set of
 * possible alleles is backed by a HashSet so that ordering is not important. <br>
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.10 $
 * 
 *
 * @param <G>
 *            the type of the genetic information which is encoded in this Locus
 */
public interface Locus<G> extends Auditable, Progenitor, Identifiable {

	/**
	 * Method to add an allele to this locus.
	 * 
	 * @param allele
	 *            An allele which can possibly appear at this locus.
	 * @param frequency
	 *            The relative frequency with which this allele appears at this
	 *            locus in the gene pool. This is used when seeding a
	 *            population.
	 * @return the key of the new allele.
	 */
	public abstract String add(Allele<G> allele, int frequency);

	/**
	 * Method to add an allele to this locus.
	 * 
	 * @param allele
	 *            An allele which can possibly appear at this locus.
	 * @return the key of the newly added allele.
	 */
	public abstract String addAllele(Allele<G> allele);

	/**
	 * @param key
	 * @return the allele from this locus corresponding to key.
	 */
	public abstract Allele<G> getAllele(String key);

	/**
	 * @return the map of alleles for this locus.
	 * 
	 *         Getter/setter typically used by reflection.
	 */
	public abstract Map<String, Allele<G>> getAlleleMap();

	/**
	 * @return a list of allele values for this locus.
	 */
	public abstract Collection<Allele<G>> getAlleleValues();

	/**
	 * @return the alphabet for this Chromosome
	 */
	public abstract String getAlphabet();

	/**
	 * @return the {@link Chromosome} to which this locus belongs.
	 */
	public abstract Chromosome<G> getChromosome();

	/**
	 * @param allele
	 *            the key for this allele.
	 * @return result of invoking {@link List#indexOf(Object)} on the #list with
	 *         the parameter allele. Will return -1 if the allele is not found.
	 */
	public abstract String getKey(Allele<G> allele);

	/**
	 * @return the owning {@link Chromosome} for a non-polygenic locus (or the
	 *         first such), otherwise get the polygenic locus which in turn
	 *         points to this one.
	 */
	public abstract Progenitor getParent();

	/**
	 * @return either another locus, with which this locus is polygenic, or
	 *         null. The target polygenic locus is itself not directly "owned"
	 *         by the chromosome. Instead its parent is this Locus.
	 */
	public abstract Locus<?> getPolygenic();

	/**
	 * Method to determine if this Locus may contain allele with a non-zero
	 * frequency
	 * 
	 * @param allele
	 *            the key to the allele
	 * @return true if locus contains allele and frequency is greater than 0
	 */
	public abstract boolean isLegal(String allele);

	/**
	 * Make a gene with the alleles specified by the variable number of allele
	 * indexes.
	 * 
	 * @param keys
	 *            variable number (according to the ploidy) of keys designating
	 *            which alleles will make up this locus.
	 * @return the new Gene.
	 */
	public abstract Gene<G> makeGene(String... keys);

	/**
	 * Method to randomly pick an allele from the set of possible alleles, and
	 * chosen according to the frequencies specified for each allele.
	 * 
	 * @return the index of a randomly chosen legal allele for this locus.
	 */
	public abstract String pickAllele();

	/**
	 * @param chromosome
	 */
	public abstract void setParent(Progenitor chromosome);

	/**
	 * @return the number of alleles in this Locus
	 */
	public abstract int size();

	/**
	 * 
	 */
	public static final String ID_SEX = "sex"; //$NON-NLS-1$

}