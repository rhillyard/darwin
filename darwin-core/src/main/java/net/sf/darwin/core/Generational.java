/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Generational.java
 * Created on Jul 19, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.core;

/**
 * This interface defines the operation which triggers a new generation for an
 * object that can undergo successive generations. It is normally implemented by
 * objects which are subsidiary to the object driving the evolution. So, for
 * example, {@link Population} implements {@link Generational} but a
 * {@link Taxon} implements the full {@link Evolvable} interface (which in turn
 * extends this interface).
 * 
 * @author Robin Hillyard
 * 
 */
public interface Generational {

	/**
	 * Mutating method to invoke the next generation of a {@link Evolvable}
	 * object. This is where all the actual work is done.
	 * 
	 * @return true if the operation was successful. If the result is false,
	 *         then something has gone wrong and the application should not
	 *         expect subsequent generations to behave sensibly.
	 * 
	 */
	public abstract boolean nextGeneration();

}