/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census.java
 * Created on Nov 9, 2009
 * @version $Revision: 1.13 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 */
public interface Census {

	/**
	 * Collect and process census information about the individual specified.
	 * 
	 * @param individual
	 *            the object to be censused.
	 * @param depth
	 *            the number of levels of depth which shall be censused
	 *            <i>below</i> this object.
	 * @param context
	 *            an arbitrary object which defines the context in which this
	 *            census is happening.
	 */
	public abstract void census(Censusible individual, int depth, Object context);

	/**
	 * Append the string to the current census.
	 * 
	 * @param string
	 *            the string value of the census
	 * @param context
	 *            an arbitrary object which defines the context in which this
	 *            census is happening.
	 */
	public abstract void present(String string, Object context);

}
