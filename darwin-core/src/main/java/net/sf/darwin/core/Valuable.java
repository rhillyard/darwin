/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Valuable.java
 * Created on Dec 29, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * Interface to define the properties of an object which has an intrinsic value.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Valuable extends Identifiable {

	/**
	 * Get the value which we can use to compare this object with other
	 * {@link Valuable} objects, for example in conjunction with {@link Best}
	 * interface.
	 * 
	 * NOTE: that this method gets called a lot. Therefore, if invoking it
	 * requires a significant amount of work, the {@link Valuable}
	 * implementation should consider caching the value.
	 * 
	 * @return the value for this object which will be used to determine which
	 *         is the "best".
	 * @throws ValueException
	 */
	public abstract Number getValue() throws ValueException;
}
