/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Phenome.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.core;

import java.util.Collection;

/**
 * <p>
 * Defines the methods for implementations of the {@link Phenome} interface. A
 * Phenome is the physical (phenotypic) counterpart to the Genomic and forms
 * part of a Taxon. A Phenome is essentially a list of Traits with their values.
 * </p>
 * 
 * TODO consider renaming this family of types as Phenomics ...
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.14 $
 * 
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Phenome
 */
public interface Phenome<P> {

	/**
	 * Add a character to this phenome.
	 * 
	 * @param character
	 * @return the index of the new character.
	 */
	public abstract int addCharacter(Pharacter<P> character);

	/**
	 * @param id
	 * @return the character with given id, else null
	 */
	public abstract Pharacter<P> getCharacter(String id);

	/**
	 * @return the set of character keys for this Phenome
	 */
	public Collection<String> getCharacterKeys();

	/**
	 * @return the characters
	 */
	public Collection<Pharacter<P>> getCharacters();

	/**
	 * Method to return the current state of the application-specific data
	 * previously set in {@link #setData(Object)}.
	 * 
	 * TODO check that we really want to use this mechanism for storing fitness.
	 * 
	 * @return application-specific data.
	 */
	public abstract Object getData();

	/**
	 * Method to get the application-supplied fitness engine for this phenome.
	 * 
	 * @return an implementation of FitnessEngine.
	 */
	public abstract FitnessEngine getFitnessEngine();

	/**
	 * Method which can be used to set some application-specific data prior to a
	 * expressing a genome as a phenotype. To retrieve the current value of this
	 * data at some later time, see {@link #getData()}.
	 * 
	 * @param data
	 *            application-specific data.
	 */
	public abstract void setData(Object data);

	/**
	 * @return the number of characters for this phenome.
	 */
	public abstract int size();

}