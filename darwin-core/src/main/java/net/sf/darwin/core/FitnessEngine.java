/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.core;

import java.util.Collection;

/**
 * <p>
 * Registry for fitness functions for Taxon (Evolutionary Computation) Systems.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.15 $
 */
public interface FitnessEngine<P, E> extends HasExpressions, CacheSignature {

	/**
	 * @param phenotype
	 * @param environment
	 * @return the fitness for this phenotype in the context of environment
	 * 
	 *         TODO consider narrowing type of phenotype to TraitMap
	 * @throws FitnessException
	 */
	public abstract double calculateFitness(final Phenotype<P> phenotype, final Environment<E> environment)
			throws FitnessException;

	/**
	 * The base implementation of this method (which should normally be
	 * appropriate for all usage) simply goes through all possible pairings of
	 * trait/ecofactor (from the given traits and the ecofactors belonging to
	 * the environment) and gets the appropriate setup. If the setup is
	 * non-null, we invoke it with the object that is passed in to this method.
	 * 
	 * @param characterKeys
	 *            XXX
	 * @param environment
	 * 
	 */
	public void onEnvironmentChange(Collection<String> characterKeys, final Environment<E> environment);

	/**
	 * Register a fitness pair with this fitness engine.
	 * 
	 * @param key
	 *            a key by which the fitness may be retrieved
	 * @param fitness
	 *            the fitness evaluator
	 * 
	 * @return true if registration was successful.
	 */
	public abstract Fitness<P, E> putFitness(String key, final Fitness<P, E> fitness);

	/**
	 * The base implementation of this method (which should normally be
	 * appropriate for all usage) simply goes through all possible pairings of
	 * trait/ecofactor (from the given traits and the ecofactors belonging to
	 * the environment) and gets the appropriate setup. If the setup is
	 * non-null, we invoke it with the object that is passed in to this method.
	 * 
	 * @param characterKeys
	 *            XXX
	 * @param adjustment
	 *            the object to be passed in to the setup method.
	 */
	public void setFitnessAdjustment(Collection<String> characterKeys, final Number adjustment);

	/**
	 * TODO consider renaming this to addListener(FitnessEngineListener ...)
	 * 
	 * Register the cache for this fitness engine.
	 * 
	 * @param fitnessCache
	 */
	public abstract void setFitnessCache(FitnessCache<P, E> fitnessCache);

	/**
	 * Remove the fitness identified by key
	 * 
	 * @param key
	 * @return the old value of Fitness for the key
	 */
	public abstract Fitness<P, E> unregisterFitness(String key);

}