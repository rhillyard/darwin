/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Quantifiable.java
 * Created on Nov 6, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

/**
 * This interface is in lieu of a Java interface to do the same thing. Yes, I
 * understand that there are some efficiency issues with making all of the
 * {@link Number} types implement an interface rather than extend an abstract
 * class, but isn't it about time that we had smart enough optimizing compilers
 * to sort this stuff out?
 * 
 * @author Robin Hillyard
 * 
 */
public interface Quantifiable {

	/**
	 * @return this object expressed as a double.
	 */
	public double doubleValue();

}
