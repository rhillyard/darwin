/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mate.java
 * Created on Jul 17, 2010 (interface extracted from former class Mate.java)
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.ToString;

/**
 * Interface to model a potential (male) mate. Objects of this class are used to
 * choose the best male as a mate for a female.
 * 
 * @author Robin Hillyard
 * 
 *         TODO this class should be based on generic types E, P, G
 */
public interface Mate {

	/**
	 * @return the desirability
	 */
	@ToString
	public abstract double getDesirability();

	/**
	 * @return the (male) mate
	 */
	@ToString
	public abstract <E, P, G> Organism<E, P, G> getMale();

}