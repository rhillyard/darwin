/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoFactor.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.core;

/**
 * Defines the methods supported by the EcoFactor interface. An
 * {@link Environment} is a set of {@link EcoFactor} instances.
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <E>
 *            the type of the environmental information which is expressed in
 *            this eco factor
 */
public interface EcoFactor<E> extends Attribute<String, E>, Individual, Censusible {

	/**
	 * @return the environment to which this factor belongs.
	 */
	public abstract Environment<E> getEnvironment();

	/**
	 * @param environment
	 */
	public abstract void setEnvironment(Environment<E> environment);

	/**
	 * @param other
	 *            the factor which will be used to update this factor
	 * @return true if this EcoFactor has been updated.
	 */
	public boolean update(final EcoFactor<E> other);

}