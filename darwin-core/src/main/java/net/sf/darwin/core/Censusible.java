/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Censusible.java
 * Created on Nov 9, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This interface defines the census operation. Such object maybe censused by a
 * {@link Census} object.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Censusible {

	/**
	 * Method to census this object.
	 * 
	 * @param census
	 *            the census object which will present the results of taking
	 *            this census
	 * @param context
	 *            an arbitrary object which can be used by the census object to
	 *            provide appropriate context for this object.
	 * @return true if we should continue censusing this object at deeper
	 *         levels. If the result is false, then we do not call
	 *         {@link #getCensusibleChildren()}.
	 */
	public abstract boolean censusMe(final Census census, final Object context);

	/**
	 * Method to return a list of censusible children of this object.
	 * 
	 * @return either null or an {@link ArrayList} of censusible objects.
	 */
	public abstract Collection<? extends Censusible> getCensusibleChildren();

}
