/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Population.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.26 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.List;

import net.sf.tostring0.Identifiable;

/**
 * Defines the properties (methods) which characterize a population in an
 * Evolutionary Computation. Briefly, a population consists of a collection of
 * instances of {@link Organism}, and which is subject to an {@link Environment}
 * . A population forms part of a {@link Taxon}, which in turn provides
 * definitions for the genetic and physical aspects of the organisms. Thus a
 * population is inter-breeding.
 * 
 * @author Robin Hillyard
 * 
 * @param <E>
 *            the type of the information in the environment which is inhabited
 *            by this population
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            population
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            population
 * 
 * <br/>
 *            CONSIDER whether we can remove the E type.
 */
public interface Population<E, P, G> extends Identifiable, Generational, Auditable, Censusible, PopulationExposed, Theological,
		Terminal {

	/**
	 * Add a colony to this population.
	 * 
	 * @param colony
	 * @return result of calling
	 */
	public abstract int addColony(final Colony<E, P, G> colony);

	/**
	 * @return the colonies
	 */
	public Collection<Colony<E, P, G>> getColonies();

	/**
	 * TODO consider using the id of the colony instead of the index.
	 * 
	 * @param index
	 * @return the indexth colony from the list
	 * 
	 */
	public Colony<E, P, G> getColony(int index);

	/**
	 * @return the sequence
	 */
	public int getSequence();

	/**
	 * Method to get the Taxon to which this population belongs.
	 * 
	 * @return this population's {@link Taxon} system.
	 */
	public abstract Taxon<E, P, G> getTaxon();

	/**
	 * @return the total number of organisms in all colonies of this population
	 */
	public int getTotal();

	/**
	 * Set the index of this population within its parent Taxon System.
	 * 
	 * TODO consider removing this and always use {@link List#indexOf(Object)}.
	 * 
	 * @param index
	 */
	public abstract void setIndex(int index);

	/**
	 * Set the system to which this population belongs. This is normally set
	 * when the population is added to the system.
	 * 
	 * @param system
	 */
	public abstract void setTaxon(final Taxon<E, P, G> system);

	/**
	 * Method which is called on this Population when the evolution is finished.
	 * 
	 * @param cause
	 *            XXX
	 */
	public abstract void wrapUp(String cause);

}