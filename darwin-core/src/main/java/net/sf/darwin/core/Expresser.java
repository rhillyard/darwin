/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser.java
 * Created on Mar 4, 2009
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.core;

/**
 * Defines the operations supported by an "expresser", that is an object that
 * can take a Gene (or Genome) and express that gene(s) in the context of an
 * environment.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            Expresser
 */
public interface Expresser<P> {

	/**
	 * Express the genes.
	 * 
	 * CONSIDER should this return an Expression?
	 * 
	 * @param genes
	 * 
	 * @return a {@link Trait}
	 */
	public abstract <G> Trait<P> express(@SuppressWarnings("unchecked") Gene<G>... genes);

	/**
	 * @return the character which the expressed trait will exhibit.
	 */
	public abstract Pharacter<P> getCharacter();

	/**
	 * Get the complementary allele key, such that
	 * {@link #isComplementary(Locus, String, String)} will be true.
	 * 
	 * @param locus
	 * @param allele
	 * @return the complementary allele key.
	 */
	public abstract <G> String getComplement(Locus<G> locus, String allele);

	/**
	 * @param locus
	 * @param allele1
	 * @param allele2
	 * @return true if the alleles are complementary, i.e. if expressed
	 *         sequentially, the result would be the same as if they had not
	 *         been expressed at all.
	 */
	public abstract <G> boolean isComplementary(Locus<G> locus, String allele1, String allele2);

}