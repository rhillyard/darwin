/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: RandomIterable
 * Created on: Feb 25, 2012
 */

package net.sf.darwin.core;

/**
 * @author rhillya
 * 
 * @param <E>
 */
public interface RandomIterable<E> extends Iterable<E> {

	/**
	 * @return the remainder of the collection, ie. those not contained in this
	 *         iterator.
	 */
	public abstract Iterable<E> getRemainder();

	/**
	 * @return true if this iterator represents only a sample of the complete
	 *         collection passed in to the constructor.
	 */
	public abstract boolean isSample();

	/**
	 * Re-examine the underlying collection and rebuild it based on new values
	 * from the random number source, i.e. the list will be in a new order. If
	 * the underlying collection has changed, it may be that the result of
	 * calling {@link #isSample()} will be different from previously.
	 */
	public abstract void reset();

	/**
	 * @return the size
	 */
	public abstract int size();

}