/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Variant.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.core;

import net.sf.tostring0.Identifiable;

/**
 * Defines the methods support by instances of the {@link Variant} interface.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Variant<P> extends Attribute<String, P>, Identifiable {

	/**
	 * @return the {@link Pharacter} of which this is variant.
	 */
	public abstract Pharacter<P> getCharacter();
}