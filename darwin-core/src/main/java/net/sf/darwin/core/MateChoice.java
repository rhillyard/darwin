/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 21, 2003
 *
 */

package net.sf.darwin.core;

import java.util.Collection;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Defines methods to determine how mates are chosen. In the current
 * implementation, male mates are always chosen by females, never the other way
 * around.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.10 $
 */
public interface MateChoice {

	/**
	 * Choose the best (male) mate for the given <code>female</code>, from among
	 * all of the organisms at the <code>lek</code>.
	 * 
	 * @param female
	 * @param lek
	 *            a collection of organisms from which to choose a (male) mate
	 *            for the given female, (note that not all of the organisms are
	 *            male, some are competing females).
	 * @return an appropriate male (or null if none live up to expectations).
	 */
	public <E, P, G> Organism<E, P, G> chooseMate(final Organism<E, P, G> female, final Lek<E, P, G> lek);

	/**
	 * First, we call {@link MateChoice#pairUp(Organism, Lek)} for the
	 * <code>female</code>, <code>lek</code> and the appropriate genomic. If
	 * this is not-null, we return it. Otherwise, we invoke
	 * {@link Colony#isIsolated()} and IF the result is false, AND if the
	 * alternative lek is not null and its colony is not the same as this
	 * colony, THEN we invoke {@link MateChoice#pairUp(Organism, Lek)} for the
	 * <code>female</code>, <code>alternativeLek</code> and the appropriate
	 * genomic. We return the result.
	 * 
	 * This method should only be called if the female organism is in fact
	 * female.
	 * 
	 * @param female
	 *            the organism for whom we require a mate.
	 * @param lek
	 *            XXX
	 * @param alternativeLek
	 *            XXX
	 * @return a newly constructed {@link Mating} object, or null.
	 */
	public <E, P, G> Mating createPairBond(final Organism<E, P, G> female, final Lek<E, P, G> lek,
			final Lek<E, P, G> alternativeLek);

	/**
	 * Method to enumerate a set of mating pairs: females from this Colony and
	 * males from either the <code>lek</code> or the <code>alternativeLek</code>
	 * .
	 * 
	 * Unlike the rest of the Darwin package, there IS an asymmetry here between
	 * the sexes. Every female organism is involved in exactly one mating per
	 * generation. However, males are chosen randomly and can therefore be
	 * involved in any number of matings in a given generation.
	 * 
	 * @param organisms
	 *            XXX
	 * @param lek
	 * @param alternativeLek
	 * 
	 * @return a Vector of Mating objects.
	 */
	public <E, P, G> Collection<Mating> findPairs(final Collection<Organism<E, P, G>> organisms, final Lek<E, P, G> lek,
			final Lek<E, P, G> alternativeLek);

	/**
	 * Method to determine the desirability of a given male in the eyes, etc. of
	 * a given female.
	 * 
	 * @param female
	 *            the choosy female.
	 * @param male
	 *            the poor schmuck male.
	 * @return the desirability from 0 (not desirable) through infinity (totally
	 *         desirable).
	 */
	public abstract <E, P, G> double getDesirabilityIndex(Organism<E, P, G> female, Organism<E, P, G> male);

	/**
	 * Method to get a "lek", that is to say a population of breeding males.
	 * 
	 * @param colony
	 *            the colony from which we choose a lek (may be null).
	 * @return a restartable Iterator with an appropriate number of individuals
	 *         chosen from the population, based on the sampleFraction for this
	 *         MateChoice. Note that this method will return null if
	 *         <code>colony</code> is null.
	 */
	public <E, P, G> Lek<E, P, G> getLek(Colony<E, P, G> colony);

	/**
	 * @return a value for the desirability threshold for choosing a mate (same
	 *         scale as the desirability index, see
	 *         {@link #getDesirabilityIndex(Organism, Organism)}
	 */
	public abstract double getMinimumDesirability();

	/**
	 * Getter/setter typically called by reflection, but also called via this
	 * interface.
	 * 
	 * @return the random
	 */
	public abstract RandomGenerator getRandom();

	/**
	 * Method to create a {@link Mating} object based on the female given and a
	 * male chosen from the lek.
	 * 
	 * @param female
	 * @param lek
	 *            a collection of organisms from which to choose a (male) mate
	 *            for the given female, (note that not all of the organisms are
	 *            male, some are competing females).
	 * @return a newly constructed {@link Mating} object.
	 * 
	 */
	public <E, P, G> Mating pairUp(final Organism<E, P, G> female, final Lek<E, P, G> lek);

	/**
	 * Getter/setter typically called by reflection.
	 * 
	 * @param random
	 */
	public abstract void setRandom(final RandomGenerator random);

}
