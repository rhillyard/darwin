/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ValueException.java
 * Created on Mar 8, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.core;

/**
 * @author Robin Hillyard
 * 
 *         CONSIDER making this a less specific exception
 */
public class ValueException extends Exception {

	/**
	 * 
	 */
	public ValueException() {
		// XXX Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ValueException(final String message) {
		super(message);
		// XXX Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ValueException(final String message, final Throwable cause) {
		super(message, cause);
		// XXX Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public ValueException(final Throwable cause) {
		super(cause);
		// XXX Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8630827078781605538L;

}
