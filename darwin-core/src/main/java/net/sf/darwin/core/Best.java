/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Best.java
 * Created on Dec 27, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.core;

import java.util.Collection;

import net.sf.tostring0.Identifiable;

/**
 * This interface defines the methods available on an object which keeps track
 * of the "best" of an (unrelated) group of {@link Identifiable} objects. The
 * objects may or may not be part of some Collection.
 * 
 * By best, we actually mean the largest value (as a double).
 * 
 * @author Robin Hillyard
 * 
 * @param <T>
 */
public interface Best<T extends Identifiable> extends Valuable {

	/**
	 * @return the object which has the best value.
	 */
	public abstract T getObject();

	/**
	 * @return the processor
	 */
	public ProcessBest<T> getProcessor();

	/**
	 * Reset this {@link Best} object as if it had been newly constructed.
	 */
	public abstract void reset();

	/**
	 * This method updates this Best object for each of the candidates in the
	 * given collection.
	 * 
	 * @param candidates
	 * @return true if we actually did update the best value (i.e. candidates
	 *         was not empty).
	 * @throws ValueException
	 */
	public boolean update(final Collection<T> candidates) throws ValueException;

	/**
	 * Method to conditionally update the best value.
	 * 
	 * @param candidate
	 *            the T which is the new proposal for best object. Note that a
	 *            new candidate may or may not have a different value from
	 *            previous objects. That's to say if two candidates are the
	 *            same, they will have the same value, but not vice versa.
	 * @param override
	 *            this is only referenced if this {@link Best} object has a
	 *            non-zero value for convergence. If the override is true, then
	 *            an equivalent object will always trigger the processor and
	 *            return true, regardless of whether convergence has been
	 *            achieved.
	 * @return true if the update is "complete", i.e. the best object has
	 *         converged and (if any) the processor has been invoked.
	 * @throws ValueException
	 */
	public abstract boolean update(final T candidate, final boolean override) throws ValueException;

}