/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FrequencyMap.java
 * Created on Feb 25, 2012
 * @version $Revision: $
 */

package net.sf.darwin.core;

import java.util.Set;

/**
 * @author Robin Hillyard
 * 
 * @param <T>
 */
public interface FrequencyMap<T> {

	/**
	 * Update the frequency map based on the given key. If key is not already
	 * present in the map, we create a new entry with key and value 0. Then we
	 * add 1 to the value corresponding to key.
	 * 
	 * @param key
	 */
	public abstract void add(final T key);

	/**
	 * Update the frequency map based on the given key. If key is not already
	 * present in the map, we create a new entry with key and value 0. Then we
	 * add 1 to the value corresponding to key.
	 * 
	 * @param key
	 * @param increment
	 *            the amount to add to the frequency for key
	 */
	public abstract void add(final T key, final int increment);

	/**
	 * 
	 * @see java.util.Map#clear()
	 */
	public abstract void clear();

	/**
	 * @param key
	 * @return true if the map contains a frequency for key
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	public abstract boolean containsKey(final T key);

	/**
	 * @param key
	 * @return the frequency of key (or 0 if the key is unknown)
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public abstract int get(final T key);

	/**
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * @return the total of all the frequencies
	 */
	public abstract int getTotal();

	/**
	 * @return true if map is empty
	 * @see java.util.Map#isEmpty()
	 */
	public abstract boolean isEmpty();

	/**
	 * @return the set of values whose frequencies are known
	 * @see java.util.Map#keySet()
	 */
	public abstract Set<T> keySet();

	/**
	 * @param key
	 * @return the value for the key before removal or 0 if the key was unknown.
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	public abstract int remove(final T key);

	/**
	 * @return the number of entries in the map.
	 * @see java.util.Map#size()
	 */
	public abstract int size();

}