/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Organism.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.21 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.Map;

/**
 * Defines methods on an individual organism within a Taxon. An Organism
 * exhibits the following properties:
 * <dl>
 * <dt>genome</dt>
 * <dd>the (possibly) unique genome for the individual, (from which may be
 * derived a Phenotype in the context of a Genomic);</dd>
 * <dt>age</dt>
 * <dd>the age of the individual (a count of the number of times the method
 * {@link #age()} has been called on it);</dd>
 * <dt>phenotype</dt>
 * <dd>the phenotype of this individual (not necessarily a fixed property - may
 * be derived on the fly);</dd>
 * <dt>colony</dt>
 * <dd>the colony to which this organism belongs;</dd>
 * </dl>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.21 $
 * 
 * @param <E>
 *            the type of the information in the environment which is inhabited
 *            by this Organism
 * @param <P>
 *            the type of the phenotypic information which is characteristic of
 *            this Organism
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Organism
 */
public interface Organism<E, P, G> extends Mortal, Genetic<P, G>, EnvironmentListener<E>, Censusible, ComparableValue, Auditable {

	/**
	 * Create a {@link Mate} object based on this (male) organism and its
	 * desirability to the given <code>female</code>.
	 * 
	 * @param chooser
	 *            the object which will calculate the desirability of this
	 *            organism to the given <code>female</code>.
	 * @param female
	 *            the organism looking for a mate.
	 * @return if organism is male, then a newly constructed Mate based on the
	 *         organism and that organism's desirability as a mate; otherwise
	 *         null.
	 */
	public Mate createMate(final MateChoice chooser, final Organism<E, P, G> female);

	/**
	 * @param chooser
	 *            the {@link MateChoice} instance which will form the bond
	 * @param lek
	 *            the breeding males from which we will choose a mate
	 * @param alternativeLek
	 *            may be null
	 * @return a new Mating object which is a pair bond between this organism
	 *         (assuming it's a female) and a male from the population
	 *         specified.
	 */
	public Mating createPairBond(final MateChoice chooser, Lek<E, P, G> lek, Lek<E, P, G> alternativeLek);

	/**
	 * @param howMany
	 *            the number of progeny to be born.
	 * @return a collection of progeny from the given organism.
	 */
	public abstract Collection<Organism<E, P, G>> getAsexualProgeny(int howMany);

	/**
	 * Method to determine to which population this organism currently belongs.
	 * 
	 * Getter/setter typically called by reflection.
	 * 
	 * @return the population to which this organism belongs.
	 */
	public abstract Colony<E, P, G> getColony();

	/**
	 * @param fitnessEngine
	 *            the {@link FitnessEngine} which will calculate the fitness of
	 *            this {@link Organism}.
	 * @return the fitness of this organism in its environment
	 * @throws FitnessException
	 */
	public Number getFitness(final FitnessEngine fitnessEngine) throws FitnessException;

	/**
	 * @return the cellular object underlying this organism
	 */
	public abstract Nuclear<P, G> getNuclear();

	/**
	 * @return the phenotype for this organism.
	 */
	public abstract Phenotype<P> getPhenotype();

	/**
	 * TODO consider change the name and sense of the result so that the method
	 * returns true if it stays alive.
	 * 
	 * @param fitness
	 *            the fitness of the organism whose mortality we are discovering
	 * @return true if the organism's number is up
	 */
	public boolean isMortal(final double fitness);

	/**
	 * @param referenceGenome
	 * @param cache
	 * @param environment
	 */
	public abstract void normalizeGenome(final Genome<P, G> referenceGenome, final Map<String, Genome<P, G>> cache,
			final Environment<E> environment);

	/**
	 * Mutating method to change the current population for this organism, as a
	 * result of migration. If this is called programatically, the call should
	 * be followed by invoking {@link #onEnvironmentChange(Environment)},
	 * although that isn't necessary when "killing" an organism.
	 * 
	 * Getter/setter typically called by reflection.
	 * 
	 * @param colony
	 *            the new population to which this organism now belongs.
	 */
	public abstract void setColony(Colony<E, P, G> colony);

	/**
	 * @param viability
	 *            XXX
	 * @return true if this Organism's viability has been reset.
	 * @throws FitnessException
	 */
	public boolean setViability(Viability viability) throws FitnessException;

}