/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Phenome.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.core;

import java.util.Collection;
import java.util.Set;

import net.sf.tostring0.Identifiable;

/**
 * Defines the operations on a read-only collection of {@link Trait} instances.
 * 
 * XXX consider to rename this family of types as Phenotype...
 * 
 * TODO consider combining this type with Expressions so that both can be named
 * Phenotype.
 * 
 * @author Robin Hillyard
 */
public interface TraitMap<P> extends CacheSignature, Censusible, Identifiable {

	/**
	 * @return the set of traits.
	 */
	public abstract Set<String> getKeys();

	/**
	 * Accessor method to retrieve a trait from the map by name
	 * 
	 * @param name
	 * @return the Variant corresponding to <code>name</code>.
	 */
	public abstract Trait<P> getTrait(String name);

	/**
	 * TODO not required.
	 * 
	 * @return the set of traits.
	 * 
	 *         Getter/setter typically called by reflection.
	 * 
	 */
	public abstract Collection<Trait<P>> getTraits();

	/**
	 * Method to get the number of traits in this map.
	 * 
	 * TODO consider eliminating this -- use getTraits().size() instead.
	 * 
	 * @return the number of traits in this map
	 * 
	 * @see java.util.HashMap#size()
	 */
	public abstract int size();

}