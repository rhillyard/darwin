/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizationModelTest.java
 * Created on Apr 7, 2009
 * @version $Revision: 1.37 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

import net.sf.darwin.api.impl.Environment_Muirian;
import net.sf.darwin.api.impl.Genomic_Sexual;
import net.sf.darwin.api.impl.Meiosis_Locus;
import net.sf.darwin.api.impl.Phenome_Gouldian;
import net.sf.darwin.api.impl.Realm_Wallacian;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.ui.base.Avatar_;
import net.sf.darwin.ui.base.RepaintListener;
import net.sf.darwin.ui.base.VisualizationModel;
import net.sf.darwin.ui.impl.VisualizationModel_Standard;

import org.junit.Test;

import com.rubecula.beanpot.Serializer;
import com.rubecula.beanpot.serialize.SerializerFactory;
import com.rubecula.jexpression.Evaluator;

/**
 * @author Robin Hillyard
 * 
 */
public class VisualizationModelTest extends TestBase {

	/**
	 * Test method for
	 * {@link net.sf.darwin.ui.base.VisualizationModel_#VisualizationModel_(java.lang.String)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testVisualizationModel_() throws Exception {
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		environment.setIdealPopulation(100L);
		final Population<?, ?, ?> population = createPopulation(new Genomic_Sexual<>("test genomic", new Meiosis_Locus<>(
				getRandom()), getRandom()), environment, new Phenome_Gouldian(new FitnessEngineTester()), getRandom(), true);
		// TODO need work here.
		final Colony<?, ?, ?> colony = population.getColony(0);
		final VisualizationModel model = new VisualizationModel_Standard(colony);
		final ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
		final PrintWriter writer1 = new PrintWriter(outputStream1);
		model.setRepaintListener(new TestComponent(writer1));
		((Taxon_) population.getTaxon()).setSeedPopulation(1);
		population.seedMembers();
		for (final Organism<?, ?, ?> organism : colony.getOrganisms()) {
			model.addAvatar(new AvatarTest(organism));
		}
		model.fireModelChange();
		writer1.flush();
		assertEquals("census1", "model changed" + NL, outputStream1.toString());
		model.setRepaintListener(null);
		final ByteArrayOutputStream outputStream2 = new ByteArrayOutputStream();
		final PrintWriter writer2 = new PrintWriter(outputStream2);
		model.setRepaintListener(new TestComponent(writer2));
		for (final Organism<?, ?, ?> organism : colony.getOrganisms()) {
			model.removeIndividual(model.findAvatar(organism));
		}
		writer2.flush();
		assertEquals("census2", "", outputStream2.toString());
		final Serializer serializer = SerializerFactory.createSerializer("visualizationModel.out");
		serializer.writeObject(model);
	}

	static class AvatarTest extends Avatar_ {

		/**
		 * @param individual
		 *            the individual which is modeled by this avatar.
		 */
		protected AvatarTest(final Individual individual) {
			super(individual);
		}

		@Override
		public String toString() {
			return getIndividual().getIdentifier();
		}

		private static final long serialVersionUID = -2859484062169795985L;

	}

	static class FitnessEngineTester extends FitnessEngine_ {

		/**
		 * 
		 */
		public FitnessEngineTester() {
			super(null, null);
			// XXX Auto-generated constructor stub
		}

		@Override
		public double calculateFitness(final Phenotype phenotype, final Environment environment) {
			return 1.0;
		}

		@Override
		protected void registerFitnesses(final Evaluator evaluator) {
			// do nothing
		}

	}

	/**
	 * @author Robin Hillyard
	 * 
	 */
	private static final class TestComponent implements RepaintListener {
		/**
		 * @param writer
		 *            XXX
		 * 
		 */
		public TestComponent(final PrintWriter writer) {
			super();
			this._writer = writer;
		}

		@Override
		public void doRepaint() {
			this._writer.println("model changed"); //$NON-NLS-1$
		}

		private static final long serialVersionUID = 1L;

		private final PrintWriter _writer;
	}
}
