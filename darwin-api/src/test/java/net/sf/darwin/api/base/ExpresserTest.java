/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExpresserTest.java
 * Created on Jun 5, 2009
 * @version $Revision: 1.16 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.impl.Expresser_Mendelian;
import net.sf.darwin.api.impl.Locus_Triversian;
import net.sf.darwin.api.impl.Pharacter_Fisherian;
import net.sf.darwin.api.impl.Variant_Basic;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "static-method" })
public class ExpresserTest {

	/**
	 */
	@Test
	public void testExpresser1() {
		final Pharacter character = new Pharacter_Fisherian("test character");
		final Variant variantRed = new Variant_Basic(K_RED, Color.red);
		final Variant variantBlue = new Variant_Basic(K_BLUE, Color.blue);
		character.addVariant(variantRed);
		character.addVariant(variantBlue);
		final Expresser_ expresser = new Expresser_Mendelian(character, K_T, K_RED);
		final Trait trait = expresser.createTrait(K_RED);
		assertEquals("index", K_RED, trait.getVariantKey());
		assertEquals("value", Color.red, trait.getValue());
		assertEquals("character", character, trait.getCharacter());
	}

	/**
	 */
	@Test
	public void testExpresser2() {
		final Pharacter character = new Pharacter_Fisherian("test character");
		final Variant variantRed = new Variant_Basic(K_RED, Color.red);
		final Variant variantBlue = new Variant_Basic(K_BLUE, Color.blue);
		character.addVariant(variantRed);
		character.addVariant(variantBlue);
		final Locus locus = new Locus_Triversian("test locus", null);
		final Allele a = AlleleFactory.makeDominanceAllele(ID_X, true);
		final Allele b = AlleleFactory.makeDominanceAllele(ID_X, false);
		locus.addAllele(a);
		locus.addAllele(b);
		final Expresser expresser = new Expresser_Mendelian(character, K_T, K_RED);
		// Heterozygous
		final Trait trait1 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_F));
		assertEquals("index", K_RED, trait1.getVariantKey());
		// Homozygous recessive
		final Trait trait2 = expresser.express(GeneFactory.makeDiploid(locus, K_F, K_F));
		assertEquals("index", K_BLUE, trait2.getVariantKey());
		assertEquals("value", Color.BLUE, trait2.getValue());
		// Homozygous dominant
		final Trait trait3 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_T));
		assertEquals("index", K_RED, trait3.getVariantKey());
	}

	/**
	 */
	@Test
	public void testExpresser3() {
		final Pharacter character = new Pharacter_Fisherian("test character");
		final Variant variantRed = new Variant_Basic(K_RED, Color.red);
		final Variant variantBlue = new Variant_Basic(K_BLUE, Color.blue);
		character.addVariant(variantRed);
		character.addVariant(variantBlue);
		final Expresser expresser = new Expresser_Mendelian(character, K_T, K_BLUE);
		final Locus locus = new Locus_Triversian("test locus", null);
		final Allele a = AlleleFactory.makeDominanceAllele(ID_X, true);
		final Allele b = AlleleFactory.makeDominanceAllele(ID_X, false);
		locus.addAllele(a);
		locus.addAllele(b);
		// Heterozygous
		final Trait trait1 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_F));
		assertEquals("index", K_BLUE, trait1.getVariantKey());
		// Homozygous recessive
		final Trait trait2 = expresser.express(GeneFactory.makeDiploid(locus, K_F, K_F));
		assertEquals("index", K_RED, trait2.getVariantKey());
		// Homozygous dominant
		final Trait trait3 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_T));
		assertEquals("index", K_BLUE, trait3.getVariantKey());
	}

	/**
	 */
	@Test
	public void testExpresser4() {
		final Pharacter character = new Pharacter_Fisherian("test character");
		final Variant variantRed = new Variant_Basic(K_RED, Color.red);
		final Variant variantBlue = new Variant_Basic(K_BLUE, Color.blue);
		character.addVariant(variantRed);
		character.addVariant(variantBlue);
		final Expresser expresser = new Expresser_Mendelian(character, K_F, K_RED);
		final Locus locus = new Locus_Triversian("test locus", null);
		final Allele a = AlleleFactory.makeDominanceAllele(ID_X, true);
		final Allele b = AlleleFactory.makeDominanceAllele(ID_X, false);
		locus.addAllele(a);
		locus.addAllele(b);
		// Heterozygous
		final Trait trait1 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_F));
		assertEquals("index", K_RED, trait1.getVariantKey());
		// Homozygous recessive
		final Trait trait2 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_T));
		assertEquals("index", K_BLUE, trait2.getVariantKey());
		// Homozygous dominant
		final Trait trait3 = expresser.express(GeneFactory.makeDiploid(locus, K_F, K_F));
		assertEquals("index", K_RED, trait3.getVariantKey());
	}

	/**
	 */
	@Test
	public void testExpresser5() {
		final Pharacter character = new Pharacter_Fisherian("test character");
		final Variant variantRed = new Variant_Basic(K_RED, Color.red);
		final Variant variantBlue = new Variant_Basic(K_BLUE, Color.blue);
		character.addVariant(variantRed);
		character.addVariant(variantBlue);
		final Expresser expresser = new Expresser_Mendelian(character, K_F, K_BLUE);
		final Locus locus = new Locus_Triversian("test locus", null);
		final Allele a = AlleleFactory.makeDominanceAllele(ID_X, true);
		final Allele b = AlleleFactory.makeDominanceAllele(ID_X, false);
		locus.addAllele(a);
		locus.addAllele(b);
		// Heterozygous
		final Trait trait1 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_F));
		assertEquals("index", K_BLUE, trait1.getVariantKey());
		// Homozygous recessive
		final Trait trait2 = expresser.express(GeneFactory.makeDiploid(locus, K_T, K_T));
		assertEquals("index", K_RED, trait2.getVariantKey());
		// Homozygous dominant
		final Trait trait3 = expresser.express(GeneFactory.makeDiploid(locus, K_F, K_F));
		assertEquals("index", K_BLUE, trait3.getVariantKey());
	}

	private static final String ID_X = "x"; //$NON-NLS-1$

	private static final String K_BLUE = "blue"; //$NON-NLS-1$

	private static final String K_RED = "red"; //$NON-NLS-1$

	private static final String K_T = AlleleFactory.makeDominanceAllele(ID_X, true).getIdentifier();

	private static final String K_F = AlleleFactory.makeDominanceAllele(ID_X, false).getIdentifier();

}
