/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunctionTest.java
 * Created on Apr 9, 2009
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.impl.FitnessFunction_Normal;
import net.sf.darwin.api.impl.FitnessFunction_ScaledNormal;
import net.sf.darwin.core.FitnessFunction;

import org.junit.Test;

import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.EvaluatorFactory;
import com.rubecula.jexpression.EvaluatorMutable;

/**
 * @author Robin Hillyard
 * 
 */
public class FitnessFunctionTest {

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction0() throws Exception {
		final FitnessFunction function = new TestFitnessFunction();
		final double baseFitness = 0.398942;
		final double tolerance = 0.000001;
		final double deltaValue = 0.001;
		final double mean = 0;
		final double value = 0;
		final double sigma = 1.0;
		final double deltaFitness = -baseFitness * value * deltaValue / sigma;
		assertEquals("fitness", baseFitness, function.getFitness(value, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness + deltaFitness, function.getFitness(value + deltaValue, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness - deltaFitness, function.getFitness(value - deltaValue, mean, sigma), tolerance);
	}

	/**
	 * Test method for
	 * {@link FitnessFunction#getFitness(double, double, double)}
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction1() throws Exception {
		final FitnessFunction function = new TestFitnessFunction();
		final double baseFitness = 0.24197;
		final double tolerance = 0.000001;
		final double deltaValue = 0.001;
		final double mean = 0;
		final double value = 1.0;
		final double sigma = 1.0;
		final double deltaFitness = -baseFitness * value * deltaValue / sigma;
		assertEquals("fitness", baseFitness, function.getFitness(value, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness + deltaFitness, function.getFitness(value + deltaValue, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness - deltaFitness, function.getFitness(value - deltaValue, mean, sigma), tolerance);
	}

	/**
	 * Test method for
	 * {@link FitnessFunction#getFitness(double, double, double)}
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction2() throws Exception {
		final Evaluator evaluator = EvaluatorFactory.createEvaluator("com.rubecula.jexpression.jep.Evaluator_JEP"); //$NON-NLS-1$
		final FitnessFunction function = new TestFitnessFunction(evaluator);
		assertEquals("fitness", NORMAL_PDF_0, function.getFitness(0, 0, 1.0), 0.001);
	}

	/**
	 * Test method for
	 * {@link FitnessFunction#getFitness(double, double, double)}
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction3() throws Exception {
		final Evaluator evaluator = EvaluatorFactory.createEvaluator("com.rubecula.jexpression.jep.Evaluator_JEP"); //$NON-NLS-1$
		final TestFitnessFunction function = new TestFitnessFunction();
		function.setEvaluator(evaluator);
		assertEquals("fitness", 0.398942, function.getFitness(0, 0, 1.0), 0.001);
	}

	/**
	 * Test method for
	 * {@link FitnessFunction#getFitness(double, double, double)}
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction4() throws Exception {
		final Evaluator evaluator = EvaluatorFactory.createEvaluator("com.rubecula.jexpression.jep.Evaluator_JEP"); //$NON-NLS-1$
		final TestFitnessFunction function = new TestFitnessFunction();
		function.setEvaluator(evaluator);
		final String expression = "1-abs(" + FitnessFunction.VAR_TARGET + "-" + FitnessFunction.VAR_VALUE + ")/"
				+ FitnessFunction.VAR_SHAPE_FACTOR;
		function.setExpression(expression);
		assertEquals("expression", expression, function.getExpression());
		assertEquals("fitness", 1.0, function.getFitness(0, 0, 1.0), 0.001);
		assertEquals("fitness", 0.8, function.getFitness(0.1, 0, 0.5), 0.001);
		function.reset();
		function.setEvaluator(null);
		assertEquals("fitness", 0.398942, function.getFitness(0, 0, 1.0), 0.001);
	}

	/**
	 * Test method for
	 * {@link FitnessFunction#getFitness(double, double, double)},
	 * {@link FitnessFunction_Normal#getValue()},
	 * {@link FitnessFunction_Normal#setExpression(String)},
	 * {@link FitnessFunction_Normal#setEvaluator(Evaluator)}.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testFitnessFunction5() throws Exception {
		final EvaluatorMutable evaluator = EvaluatorFactory.createEvaluatorMutableDefault();
		final TestFitnessFunction function = new TestFitnessFunction();
		function.setEvaluator(evaluator);
		final String expression = "1-abs(" + FitnessFunction.VAR_TARGET + "-" + FitnessFunction.VAR_VALUE + ")/"
				+ FitnessFunction.VAR_SHAPE_FACTOR;
		function.setExpression(expression);
		assertEquals("fitness", 1.0, function.getFitness(0, 0, 1.0), 0.001);
		final Number d = function.evaluate(FitnessFunction.VAR_VALUE, 0, FitnessFunction.VAR_TARGET, 0,
				FitnessFunction.VAR_SHAPE_FACTOR, 1);
		assertEquals("fitness", 1.0, d.doubleValue(), 0.001);
		function.setEvaluator(null);
		assertEquals("fitness", 0.398942, function.getFitness(0, 0, 1.0), 0.001);
	}

	/**
	 * {@link FitnessFunction_Normal#getValue()},
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction6() throws Exception {
		final FitnessFunction function = new FitnessFunction_ScaledNormal();
		final double baseFitness = 1.0;
		final double tolerance = 0.000001;
		final double deltaValue = 0.001;
		final double mean = 0;
		final double value = 0;
		final double sigma = 1.0;
		final double deltaFitness = -baseFitness * value * deltaValue / sigma;
		assertEquals("fitness", baseFitness, function.getFitness(value, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness + deltaFitness, function.getFitness(value + deltaValue, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness - deltaFitness, function.getFitness(value - deltaValue, mean, sigma), tolerance);
	}

	/**
	 * {@link FitnessFunction_Normal#getValue()},
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction7() throws Exception {
		final Evaluator evaluator = EvaluatorFactory.createEvaluator("com.rubecula.jexpression.jep.Evaluator_JEP"); //$NON-NLS-1$
		final FitnessFunction function = new FitnessFunction_ScaledNormal(evaluator);
		assertEquals("fitness", 1.0, function.getFitness(0, 0, 1.0), 0.001);
	}

	/**
	 * {@link FitnessFunction_Normal#getValue()},
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessFunction8() throws Exception {
		final FitnessFunction function = new FitnessFunction_ScaledNormal();
		final double baseFitness = 0.882497;
		final double tolerance = 0.000001;
		final double deltaValue = 0.001;
		final double mean = 0;
		final double value = 0.5;
		final double sigma = 1.0;
		final double deltaFitness = -baseFitness * value * deltaValue / sigma;
		assertEquals("fitness", baseFitness, function.getFitness(value, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness + deltaFitness, function.getFitness(value + deltaValue, mean, sigma), tolerance);
		assertEquals("fitness", baseFitness - deltaFitness, function.getFitness(value - deltaValue, mean, sigma), tolerance);
	}

	/**
	 * Test method for
	 * {@link FitnessFunction#getFitness(double, double, double)},
	 * {@link FitnessFunction_Normal#getValue()},
	 * {@link FitnessFunction_Normal#setExpression(String)},
	 * {@link FitnessFunction_Normal#setEvaluator(Evaluator)}.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testFitnessFunction9() throws Exception {
		final EvaluatorMutable evaluator = EvaluatorFactory.createEvaluatorMutableDefault();
		final TestFitnessFunction function = new TestFitnessFunction();
		function.setEvaluator(evaluator);
		final String expression = "1-abs(target-value)/shapeFactor";
		function.setExpression(expression);
		assertEquals("fitness", 1.0, function.getFitness(0, 0, 1.0), 0.001);
		final Number d = function.evaluate("value", 0, "target", 0, "shapeFactor", 1);
		assertEquals("fitness", 1.0, d.doubleValue(), 0.001);
		function.setEvaluator(null);
		assertEquals("fitness", NORMAL_PDF_0, function.getFitness(0, 0, 1.0), 0.001);
	}

	/**
	 * 
	 */
	private static final double NORMAL_PDF_0 = 0.398942;

	/**
	 * Extension of {@link FitnessFunction_Normal} with evaluate(...) and
	 * reset(), setEvaluator() methods overridden for visibility purposes.
	 * 
	 */
	static class TestFitnessFunction extends FitnessFunction_Normal {

		public TestFitnessFunction() {
			super();
		}

		public TestFitnessFunction(final Evaluator evaluator) {
			super(evaluator);
		}

		@Override
		public Number evaluate(final Object... parameters) throws FunctionException {
			return super.evaluate(parameters);
		}

		@Override
		protected void reset() throws FunctionException {
			super.reset();
		}

		@Override
		protected void setEvaluator(final Evaluator evaluator) throws FunctionException {
			super.setEvaluator(evaluator);
		}

	}

}
