/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: QuantifiableFrequencyMapTest.java
 * Created on Nov 6, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class NumericFrequencyMapTest {

	/**
	 * 
	 */
	@SuppressWarnings({ "nls", "static-method" })
	@Test
	public void testNumericFrequencyMap() {
		final NumericFrequencyMap map = new NumericFrequencyMap("test");
		final Number e = Double.valueOf(Math.E);
		final Number pi = Double.valueOf(Math.PI);
		map.add(e);
		assertTrue("check", map.check());
		assertEquals("mean0", Math.E, map.mean(), 0.000001);
		map.add(pi);
		assertEquals("mean0", (Math.E + Math.PI) / 2, map.mean(), 0.000001);
		assertTrue("check", map.check());
		map.add(e);
		assertEquals("mean0", (2 * Math.E + Math.PI) / 3, map.mean(), 0.000001);
		assertTrue("check", map.check());
		assertEquals("total", 3, map.getTotal());
		assertEquals("frequency", 2, map.get(e));
		assertEquals("frequency", 1, map.get(pi));
	}

}
