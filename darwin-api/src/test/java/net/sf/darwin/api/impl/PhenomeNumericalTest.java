/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenomeNumericalTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.base.NumericalTrait;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Variant;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class PhenomeNumericalTest extends TestBase {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Phenome_Numerical#Phenome_Numerical(FitnessEngine, NumericalTrait)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPhenomeNumerical() {
		final NumericalTrait $WingColorValuer = new NumericalTrait() {
			@Override
			public double getTraitValue(final String trait) {
				// The "value" of a color is defined as its position in the
				// $S_WingColors array
				return LookupIndex(trait, ApplicationDefinitions.$Colors);
			}
		};

		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Phenome_Numerical phenome = new Phenome_Numerical(fitnessFunction, $WingColorValuer);
		final FitnessEngine function = phenome.getFitnessEngine();
		assertEquals("fitness", fitnessFunction, function);
		final Variant variant = new Variant_Basic(TEST_VARIANT, new Integer(1));
		final Pharacter character = new Pharacter_Fisherian("test character");
		phenome.addCharacter(character);
		character.addVariant(variant);
		assertEquals("traits", 1, phenome.size());
		assertEquals("test character", character, phenome.getCharacter("test character"));
		assertEquals("test variant", TEST_VARIANT, character.getKey(variant));
		final double value = phenome.getTraitValue(TEST_VARIANT);
		assertEquals("traitValue", -1.0, value, 0.001);
	}

	/**
	 * 
	 */
	private static final String TEST_VARIANT = "test variant"; //$NON-NLS-1$

	/**
	 * Method to look up a String (key) from a list of Strings (keys).
	 * 
	 * @param key
	 *            the String to be looked up.
	 * @param keys
	 *            the array of Strings.
	 * @return the index of the first matching key (if exists), else -1 for no
	 *         match.
	 */
	static int LookupIndex(final String key, final String[] keys) {
		for (int i = 0; i < keys.length; i++)
			if (key.equalsIgnoreCase(keys[i]))
				return i;
		return -1;
	}

}
