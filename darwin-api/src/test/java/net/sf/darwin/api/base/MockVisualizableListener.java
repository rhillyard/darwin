package net.sf.darwin.api.base;

import java.io.IOException;

import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.VisualizableListener_;

public final class MockVisualizableListener extends VisualizableListener_ {

	/**
	 * Public constructor to create a MockVisualizableListener with given
	 * implementations of (print) writer and (census) taker.
	 * 
	 * @param censusTaker
	 */
	public MockVisualizableListener(final Census censusTaker) {
		this._taker = censusTaker;
	}

	/**
	 * Override the abstract method to deal with a change in the population. In
	 * particular, the method {@link Census#census(Censusible, int, Object)} is
	 * called on {@link #_taker}; and then the taker is flushed.
	 * 
	 * @see net.sf.darwin.core.VisualizableListener#onChange(Visualizable,
	 *      Object)
	 */
	@Override
	public void onChange(final Visualizable p, final Object context) {
		getTaker().census(p, 2, context);
		try {
			// TODO consider doing this via polymorphism
			if (getTaker() instanceof Sink) {
				((Sink) getTaker()).flush();
			}
		} catch (final IOException e) {
			throw new RuntimeException("I/O exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * TODO consider using toString0 utilities
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MockVisualizableListener with taker: " + getTaker(); //$NON-NLS-1$
	}

	/**
	 * @return the taker
	 */
	private Census getTaker() {
		return this._taker;
	}

	private final Census _taker;
}