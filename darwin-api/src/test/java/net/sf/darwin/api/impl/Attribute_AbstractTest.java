/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Attribute_AbstractTest.java
 * Created on Feb 23, 2009
 * @version $Revision: 1.24 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.base.Allele_;
import net.sf.darwin.api.base.Attribute_;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.core.Attribute;
import net.sf.tostring0.AToString;
import net.sf.tostring0.Detail;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class Attribute_AbstractTest {

	/**
	 * Test method for {@link Attribute#getAttribute()} and
	 * {@link Attribute#getValue()} and {@link Attribute#toString()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAttribute_Abstract() {
		final String KEY = "key"; //$NON-NLS-1$
		final String VALUE = "value"; //$NON-NLS-1$
		final Attribute_ attribute = new TestAttribute(KEY, VALUE);
		assertEquals(KEY, attribute.getAttribute());
		assertEquals(VALUE, attribute.getValue());
		final String OTHER = "OTHER"; //$NON-NLS-1$
		attribute.setValue(OTHER);
		assertEquals(OTHER, attribute.getValue());
		assertEquals(KEY.substring(0, 1), attribute.toStringId());
		assertEquals("Attribute " + KEY + ": " + OTHER, attribute.toString());
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAuditString() {
		final Allele_ melanism = (Allele_) AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true);
		final AToString noMelanism = (AToString) AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false);
		assertEquals(MockAllele.MELANISM.substring(0, 1), melanism.toStringId());
		assertEquals(MockAllele.MELANISM.toLowerCase().substring(0, 1), noMelanism.toStringId());
		assertEquals("Allele " + MockAllele.MELANISM + ": true",
				melanism.toString(new Detail(true, true, true, false, false, null, false, 10, Integer.MAX_VALUE)));
		assertEquals("Allele " + MockAllele.MELANISM.toLowerCase() + ": false",
				noMelanism.toString(new Detail(true, true, true, false, false, null, false, 10, Integer.MAX_VALUE)));
		assertEquals(MockAllele.MELANISM, melanism.getIdentifier());
	}

	static class TestAttribute extends Attribute_ {

		/**
		 * @param key
		 * @param value
		 */
		protected TestAttribute(final Object key, final Object value) {
			super(key, value);
		}

		/**
		 * @return the String equivalent of {@link #getValue()}.
		 * @see net.sf.darwin.core.Basic#getBases()
		 */
		public String getBases() {
			return getValue().toString();
		}

		/**
		 * @see net.sf.darwin.api.base.Attribute_#toString(net.sf.tostring0.Detail)
		 */
		@Override
		public String toString(final Detail detail) {
			if (detail.isShowDetail()) {
				// TODO consider using the normal audit utilities
				return "Attribute " + getAttribute() + ": " + getValue(); //$NON-NLS-1$ //$NON-NLS-2$
			}
			return getIdentifier().substring(0, this.stringChars);
		}

		private final int stringChars = 1;
	}

}
