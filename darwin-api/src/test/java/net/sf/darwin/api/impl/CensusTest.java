/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: CensusTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.71 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;

import java.io.PrintWriter;

import net.sf.darwin.api.base.Allele_Binary;
import net.sf.darwin.api.base.Census_Sink;
import net.sf.darwin.api.base.MateChoice_Wrightian;
import net.sf.darwin.api.base.Peekable;
import net.sf.darwin.api.base.Taxon_;
import net.sf.darwin.api.base.Taxon_Darwinian;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.ui.impl.VisualizableListener_Census;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.Test;

import com.rubecula.beanpot.StringWriterWriter;

/**
 * @author Robin Hillyard
 * 
 */
public class CensusTest extends TestBase {

	/**
	 * @see net.sf.darwin.api.util.TestBase#setUp()
	 */
	@Override
	public void setUp() {
		super.setUp();
		getRandom().setSeed(0);
	}

	/**
	 * @see net.sf.darwin.api.util.TestBase#tearDown()
	 */
	@Override
	public void tearDown() {
		super.tearDown();
	}

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "deprecation" })
	@Test
	public void testCensus_Standard() throws Exception {
		final int nextSequence = PhenotypeFactory.getNextSequence();
		final Realm realm = new Realm_Wallacian();
		final Genomic_Sexual genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final Chromosome chromosome = new Chromosome_NoSex("no sex", genomic.getAlphabet());
		genomic.addChromosome(chromosome);
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Environment environment = new Environment_Muirian(realm);
		environment.setIdealPopulation(100L); // this is not necessary (default)
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Pharacter character = new Pharacter_Fisherian("test character");
		phenome.addCharacter(character);
		final StringWriterWriter writer = new StringWriterWriter();
		final Census censusTaker = new Census_Standard(writer);
		final double infantMortality = 0.8;
		final Locus locus = new Locus_Triversian("unnamed locus", getRandom());
		final Allele allele1 = AlleleFactory.makeDominanceAllele("x", true);
		((Allele_Binary) allele1).setBases(new String[] { "CA", "GT" });
		final String alleleKey = locus.addAllele(allele1); // keep
		// as
		// constructor
		chromosome.addLocus(locus);
		final Allele allele2 = AlleleFactory.makeDominanceAllele("x", false);
		((Allele_Binary) allele2).setBases(new String[] { "CA", "GT" });
		locus.addAllele(allele2);
		final String variantKey = character.addVariant(new Variant_Basic("red", "red"));
		character.addVariant(new Variant_Basic("blue", "blue"));
		final Expresser expresser = new Expresser_Mendelian(character, alleleKey, variantKey);
		genomic.putExpresser(locus, expresser);
		final Taxon_ system = new Taxon_Darwinian("test system", realm, genomic, phenome, censusTaker, infantMortality,
				new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated(), getRandom());
		system.setSeedPopulation(1);
		// final Taxon system = new
		// Taxon_Darwinian("test system",censusTaker,infantMortality,genomic,phenome);
		final Population population = new Population_Malthusian(TEST_COLONY);
		system.addPopulation(population); // void result
		final Colony colony = new Colony_Mayrian("test colony", environment, getRandom());
		population.addColony(colony);
		system.addVisualizableListener(new VisualizableListener_Census(censusTaker));
		int nextRandom = 0;
		final RandomGenerator random = getRandom();
		if (random instanceof Peekable)
			nextRandom = ((Peekable) random).peekInt(10000);
		population.seedMembers();
		final String buffer = writer.reset();

		assertEquals(
				"census",
				"[seed members] Colony: test colony and count: 1"
						+ NL
						+ "[seed members] Organism_Sexual 0-"
						+ nextRandom
						+ " <phenotype=Ph"
						+ nextSequence
						+ " <extendedPhenotypes=[], traits=[sex:F, test character:r...]>, age=0, nuclear=<colonyId=test colony, genome=XX-CAGT- <genes=[sex=XX, unnamed locus=Xx], ploidy=2>, female=true>, viable=false>"
						+ NL, buffer);
	}

	/**
	 * @throws GeneticsException
	 */
	@SuppressWarnings({ "nls", "deprecation" })
	@Test
	public void testDefaultEnumeration() throws GeneticsException {
		final int nextSequence = PhenotypeFactory.getNextSequence();
		final Realm realm = new Realm_Wallacian();
		final Genomic_Sexual genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final Chromosome chromosome = new Chromosome_NoSex("no sex", genomic.getAlphabet());
		genomic.addChromosome(chromosome);
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Environment environment = new Environment_Muirian(realm);
		environment.setIdealPopulation(100L); // not necessary - default
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Pharacter character = new Pharacter_Fisherian("test character");
		phenome.addCharacter(character);
		final StringWriterWriter writer = new StringWriterWriter();
		final Census censusTaker = new TestCensus(writer);
		final double infantMortality = 0.8;
		final Locus locus = new Locus_Triversian("unnamed locus", getRandom());
		final Allele allele1 = AlleleFactory.makeDominanceAllele("x", true);
		((Allele_Binary) allele1).setBases(new String[] { "CA", "GT" });
		final String allKey = locus.addAllele(allele1);
		chromosome.addLocus(locus);
		final Allele allele2 = AlleleFactory.makeDominanceAllele("x", false);
		((Allele_Binary) allele2).setBases(new String[] { "CA", "GT" });
		locus.addAllele(allele2);
		final String varKey = character.addVariant(new Variant_Basic("red", "red"));
		character.addVariant(new Variant_Basic("blue", "blue"));
		final Expresser expresser = new Expresser_Mendelian(character, allKey, varKey);
		genomic.putExpresser(locus, expresser);
		final Taxon_ system = new Taxon_Darwinian("test system", realm, genomic, phenome, censusTaker, infantMortality,
				new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated(), getRandom());
		final Population population = new Population_Malthusian(TEST_COLONY);
		system.addPopulation(population); // void result
		system.setSeedPopulation(1);
		system.addVisualizableListener(new VisualizableListener_Census(censusTaker));
		final Colony colony = new Colony_Mayrian("test colony", environment, getRandom());
		population.addColony(colony);
		population.seedMembers();
		final String buffer = writer.reset();

		// [seed members] Population: test population and population: 1
		assertEquals(
				"census",
				TEST_CENSUS_FOR_COLONY
						+ TEST_COLONY

						+ " and count: 1"
						+ NL
						+ "[seed members] Organism_Sexual 0-1360 <phenotype=Ph"
						+ nextSequence
						+ " <extendedPhenotypes=[], traits=[sex:F, test character:r...]>, age=0, nuclear=<colonyId=test colony, genome=XX-CAGT- <genes=[sex=XX, unnamed locus=Xx], ploidy=2>, female=true>, viable=false>"
						+ NL, buffer);
	}

	private static final String TEST_CENSUS_FOR_COLONY = "[seed members] Colony: "; //$NON-NLS-1$

	private static final String TEST_COLONY = "test colony"; //$NON-NLS-1$

	/**
	 * @author Robin Hillyard
	 * 
	 */
	private static final class TestCensus extends Census_Sink {
		/**
		 * @param writer
		 *            the writer to which output will be sent.
		 * 
		 */
		public TestCensus(final PrintWriter writer) {
			super(writer);
		}

		/**
		 * @see net.sf.darwin.core.Census#census(net.sf.darwin.core.Censusible,
		 *      int, java.lang.Object)
		 */
		@Override
		public void census(final Censusible individual, final int depth, final Object context) {
			super.census(individual, depth, context);
		}

	}

}
