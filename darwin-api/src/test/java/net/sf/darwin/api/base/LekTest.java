/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: LekTest.java
 * Created on Dec 4, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import net.sf.darwin.api.base.OrganismTest.Fitness;
import net.sf.darwin.api.impl.Environment_Muirian;
import net.sf.darwin.api.impl.FitnessEngine_Simple;
import net.sf.darwin.api.impl.Genomic_Sexual;
import net.sf.darwin.api.impl.Lek_Standard;
import net.sf.darwin.api.impl.Meiosis_Locus;
import net.sf.darwin.api.impl.Organism_Sexual;
import net.sf.darwin.api.impl.Phenome_Gouldian;
import net.sf.darwin.api.impl.Realm_Wallacian;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lek;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Sexual;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("nls")
public class LekTest extends TestBase {

	/**
	 */
	@Override
	@Before
	public void setUp() {
		super.setUp();
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Simple();
		fitnessFunction.putFitness("", new Fitness());
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		try {
			this._population = createPopulation(genomic, environment, phenome, true);
			getRandom().setSeed(1L);
			final Colony colony = this._population.getColony(0);
			Organism_Sexual.seed(colony);
			Organism_Sexual.seed(colony);
			Organism_Sexual.seed(colony);
		} catch (final GeneticsException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Lek_Standard#Lek(Colony, com.rubecula.util.random.Random, double)}
	 * .
	 */
	@Test
	public final void testLekPopulationRandomDouble() {
		final Lek<Object, Object, Object> lek = new Lek_Standard<>(this._population.getColony(0), getRandom(), 1);
		assertEquals("size", 2, lek.size());
		for (final Organism<Object, Object, Object> organism : lek) {
			if (organism instanceof Sexual) {
				assertFalse("sex", ((Sexual) organism).isFemale());
			} else
				fail("logic error");
		}
	}

	private Population<Object, Object, Object> _population;

}
