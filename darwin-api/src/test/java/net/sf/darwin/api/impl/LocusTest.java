/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: LocusTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.27 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Locus;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.BeanPot;
import com.rubecula.beanpot.Serializer;
import com.rubecula.beanpot.serialize.SerializerFactory;

/**
 * @author Robin Hillyard
 * 
 */
public class LocusTest {

	/**
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception {
		this._beanContainer = new BeanContainer_BeanPot();
		this._beanContainer.setConfigurationByResource(LocusTest.class, "testLocus.xml"); //$NON-NLS-1$
		this._beanContainer.setDebug(true);
		this._beanContainer.configure();
		this._random = (RandomGenerator) this._beanContainer.getBean("Random"); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		this._beanContainer.cleanup();
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.Locus#add(net.sf.darwin.core.Allele, int)} .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddAlleleInt() {
		final Locus locus = new Locus_Triversian("unnamed locus", this._random);
		locus.add(AlleleFactory.makeDominanceAllele("x", true), 1);
		locus.add(AlleleFactory.makeDominanceAllele("x", false), 0);
		final Object value = locus.getAllele(locus.pickAllele()).getValue();
		assertEquals("allele value", Boolean.TRUE, value);
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testLocus() {
		final Locus locus = new Locus_Triversian("unnamed locus", this._random);
		final Allele allele = AlleleFactory.makeDominanceAllele("x", true);
		final String key = locus.addAllele(allele);
		assertEquals("key", allele.getIdentifier(), key);
		final boolean b = locus.isLegal(key);
		assertTrue(b);
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testLocusRandomizer() {
		final Randomizer randomizer = new Randomizer(this._random);
		final Locus locus = new Locus_Triversian("unnamed locus", randomizer);
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", true));
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", false));
		final Object value = locus.getAllele(locus.pickAllele()).getValue();
		assertEquals("allele value", Boolean.FALSE, value);
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testLocusRandomizer2() {
		final Locus locus = (Locus) BeanPot.getInstance().getBean("Locus"); //$NON-NLS-1$
		int melanistic = 0;
		for (int i = 0; i < 100000; i++) {
			final Object value = locus.getAllele(locus.pickAllele()).getValue();
			if (value instanceof Boolean) {
				if (((Boolean) value).booleanValue())
					melanistic++;
			} else
				throw new RuntimeException("logic error in test: allele is not Boolean: " + value.getClass());
		}
		// Check that we are in the region of 20000, i.e. 100000 / (1 + 4)
		final int expectedMelanistic = 20000;

		System.out.println("Proportion of melanistic alleles: " + 0.001 * melanistic + "%");

		assertTrue("melanistic", Math.abs(melanistic - expectedMelanistic) < 200);
	}

	/**
	 * Tests: {@link SerializerFactory#createSerializer(String)},
	 * {@link SerializerFactory#createDeserializer(String, boolean)},
	 * {@link Serializer#writeObject(Object)}, {@link Serializer#readObject()},
	 * on an Object of type {@link Locus_Triversian}.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testSerializeLocus() throws Exception {
		final Locus locus = new Locus_Triversian("test locus", this._random);
		final Allele alleleD = AlleleFactory.makeDominanceAllele("x", true);
		locus.add(alleleD, 1);
		locus.add(AlleleFactory.makeDominanceAllele("x", false), 0);
		final Serializer sin = SerializerFactory.createSerializer("tempFile");
		sin.writeObject(locus);
		sin.close();
		final Serializer sout = SerializerFactory.createDeserializer("tempFile", true);
		if (sout.available() > 0) {
			final Locus locusCopy = (Locus) sout.readObject();
			assertTrue("serialization OK", locusCopy.equals(locus));
			assertEquals("available", 0, sout.available());
			final Allele alleleCopy = locusCopy.getAllele(locusCopy.pickAllele());
			assertEquals("allele value", Boolean.TRUE, alleleCopy.getValue());
			assertTrue("alleles", alleleCopy.equals(alleleD));
		} else
			fail("file is empty");
		sout.close();

	}

	private RandomGenerator _random;

	private BeanContainer _beanContainer;
}
