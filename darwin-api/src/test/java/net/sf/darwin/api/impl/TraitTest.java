/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TraitTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class TraitTest {

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testTraitConstructors() {
		assertEquals("id1", TEST_VARIANT, new Variant_Basic(TEST_VARIANT, null).getIdentifier());
		final Integer one = new Integer(1);
		final Variant_Basic variant = new Variant_Basic(TEST_VARIANT, one);
		assertEquals("id2", TEST_VARIANT, variant.getIdentifier());
		assertEquals("value", one, variant.getValue());
	}

	private static final String TEST_VARIANT = "test variant"; //$NON-NLS-1$

}
