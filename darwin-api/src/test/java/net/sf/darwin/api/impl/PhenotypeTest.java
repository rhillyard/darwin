/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenotypeTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import net.sf.darwin.api.base.MockEnum_WC;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.TraitMap;
import net.sf.darwin.core.Variant;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class PhenotypeTest {

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPhenotype() {
		final Phenotype phenotype = new Phenotype_Dawkinsian(null);
		assertEquals("identifier", null, phenotype.getIdentifier()); // default
		// phenotypes
		// always
		// have
		// the
		// null
		// identifier
		final Variant color = new MockVariant(MockEnum_WC.Black);
		final Pharacter character = new Pharacter_Fisherian(COLOR);
		character.addVariant(color);
		final Trait trait = new Trait_Discrete(character, "Black");
		phenotype.addTrait(trait);
		assertEquals("trait", trait, phenotype.getTrait(COLOR));
		assertEquals("size", 1, phenotype.size());
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPhenotype2() {
		final Phenotype phenotype = new Phenotype_Dawkinsian("test");
		final Variant color = new MockVariant(MockEnum_WC.Black);
		final Pharacter character_sex = Pharacter_Sex.getInstance();
		final Pharacter character_nosex = new Pharacter_Fisherian(COLOR);
		character_nosex.addVariant(color);
		final Trait trait = new Trait_Discrete(character_nosex, MockEnum_WC.Black.toString());
		phenotype.addTrait(trait);
		final Trait traitMale = new Trait_Discrete(character_sex, Pharacter_Sex.KEY_M);
		phenotype.addTrait(traitMale);
		final TraitMap sexTraits = phenotype.getSexTraits();
		assertNull("traitColor", sexTraits.getTrait(COLOR));
		assertEquals("trait", traitMale, sexTraits.getTrait(Pharacter.SEX));
		assertEquals("size", 1, sexTraits.size());
	}

	/**
	 * 
	 */
	private static final String COLOR = "color"; //$NON-NLS-1$

}
