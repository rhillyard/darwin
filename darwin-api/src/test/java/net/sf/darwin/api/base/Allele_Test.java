package net.sf.darwin.api.base;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.PrintWriter;

import net.sf.darwin.api.impl.Locus_Triversian;
import net.sf.darwin.api.impl.Random_Standard;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.Detail;

import org.junit.BeforeClass;
import org.junit.Test;

import com.rubecula.beanpot.StringWriterWriter;

public class Allele_Test {

	@Test
	public void censusMe_A$Census$Object() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_<Object> target = new MockAllele(value);
		_locus.addAllele(target);
		final PrintWriter writer = new StringWriterWriter();
		final Census census = new Census_Sink(writer) {
		};
		final Object context_ = null;
		final boolean actual = target.censusMe(census, context_);
		final boolean expected = false;
		assertThat(actual, is(equalTo(expected)));
	}

	@Test
	public void getBases_A$() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_<Object> target = new MockAllele(value);
		final String actual = target.getBases();
		final String expected = "0";
		assertEquals(expected, actual);
	}

	@Test
	public void getCensusibleChildren_A$() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		final Object actual = target.getCensusibleChildren();
		final Object expected = null;
		assertThat(actual, is(equalTo(expected)));
	}

	@Test
	public void getLocus_A$() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		final Locus actual = target.getLocus();
		final Locus expected = null;
		assertThat(actual, is(equalTo(expected)));
	}

	@Test
	public void getSignature_A$() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		final String actual = target.getSignature();
		final String expected = "0";
		assertEquals(expected, actual);
	}

	@Test
	public void instantiation() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		assertThat(target, notNullValue());
	}

	@Test
	public void setLocus_A$Locus() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		final Locus locus = null;
		target.setLocus(locus);
	}

	@Test(expected = DarwinException.class)
	public void setValue_A$Object() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		final Object value_ = null;
		target.setValue(value_);
	}

	@Test
	public void toString_A$Detail() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final Object value = Integer.valueOf(0);
		final Allele_ target = new MockAllele(value);
		final Detail detail = new Detail(true, true, true, false, false, null, false, 10, Integer.MAX_VALUE);
		final String actual = target.toString(detail);
		final String expected = "Allele 0: 0";
		assertEquals(expected, actual);
	}

	@Test
	public void type() throws Exception {
		// TODO auto-generated by JUnit Helper.
		assertThat(Allele_.class, notNullValue());
	}

	private static Locus_Triversian _locus;

	/**
	 * @author Robin
	 * 
	 */
	private static final class MockAllele extends Allele_<Object> {
		/**
		 * @param value
		 */
		MockAllele(final Object value) {
			super(value);
		}

		/**
		 * @see net.sf.darwin.core.Basic#getBases()
		 */
		@Override
		public String getBases() {
			return getValue().toString();
		}

		private static final long serialVersionUID = 1L;
	}

	@BeforeClass
	public static void setup() {
		_locus = new Locus_Triversian("locus", new Random_Standard(0)); //$NON-NLS-1$
	}

}
