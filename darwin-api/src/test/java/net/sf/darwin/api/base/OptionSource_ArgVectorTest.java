package net.sf.darwin.api.base;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import com.rubecula.beanpot.ArgVector;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_Map;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_ObjectMap;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_StringMap;

@SuppressWarnings({ "nls", "static-method", "boxing" })
public class OptionSource_ArgVectorTest {

	@Test
	public void getKeys_A$() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final ArgVector args = new ArgVector(new String[] { "-d", "-x", "100" });
		final ArgVectorProcessor_Map<Object> argProcessor = new ArgVectorProcessor_ObjectMap();
		final OptionSource_ArgVector<Object> target = new OptionSource_ArgVector<Object>(args, argProcessor);
		final Collection<String> actual = new ArrayList<String>(target.getKeys());
		final Collection<String> expected = new ArrayList<String>();
		CollectionUtils.addAll(expected, new String[] { "d", "x" });
		assertThat(actual, is(equalTo(expected)));
	}

	@Test
	public void getOption_A$String() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final ArgVector args1 = new ArgVector(new String[] { "-d", "-x", "100" });
		final ArgVectorProcessor_Map<Object> argProcessor1 = new ArgVectorProcessor_ObjectMap();
		final OptionSource_ArgVector<Object> target1 = new OptionSource_ArgVector<Object>(args1, argProcessor1);
		assertTrue((Boolean) target1.getOption("d"));
		assertThat((Long) target1.getOption("x"), is(equalTo(Long.valueOf(100))));
		final ArgVector args2 = new ArgVector(new String[] { "-d", "-x", "100" });
		final ArgVectorProcessor_Map<String> argProcessor2 = new ArgVectorProcessor_StringMap();
		final OptionSource_ArgVector<String> target2 = new OptionSource_ArgVector<String>(args2, argProcessor2);
		assertNull(target2.getOption("d"));
		assertThat(target2.getOption("x"), is(equalTo("100")));
	}

	@Test
	public void instantiation() throws Exception {
		// TODO auto-generated by JUnit Helper.
		final ArgVector args = new ArgVector(new String[] {});
		final ArgVectorProcessor_Map<Object> argProcessor = null;
		final OptionSource_ArgVector<Object> target = new OptionSource_ArgVector<Object>(args, argProcessor);
		assertThat(target, notNullValue());
	}

	@Test
	public void type() throws Exception {
		// TODO auto-generated by JUnit Helper.
		assertThat(OptionSource_ArgVector.class, notNullValue());
	}

}
