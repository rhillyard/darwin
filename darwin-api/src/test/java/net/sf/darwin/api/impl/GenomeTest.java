/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GenomeTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.43 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.base.Allele_;
import net.sf.darwin.api.base.MockEnum_WC;
import net.sf.darwin.api.base.Mutator_;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;
import net.sf.tostring0.AToString;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class GenomeTest {

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Before
	public void setUp() {
		this._random = new Random_Standard(0L);
		// Don't be tempted to replace these with references to constants (it
		// messes up the allele key mechanism)
		this._alleleX = AlleleFactory.makeDominanceAllele("x", true);
		this._allelex = AlleleFactory.makeDominanceAllele("x", false);
		this._locus = new Locus_Triversian(LOCUSID, this._random);
		this._character = new Pharacter_Fisherian("test");
		final Variant carbonaria = new MockVariant(MockEnum_WC.Carbonaria);
		this._typica = new MockVariant(MockEnum_WC.Typica);
		this._character.addVariant(this._typica);
		this._character.addVariant(carbonaria);
		this._expresser = new Expresser_Mendelian(this._character, this._alleleX.getIdentifier(), this._typica.getIdentifier());
		final Realm realm = new Realm_Wallacian();
		new Environment_Muirian(realm);
	}

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testDiploidGenome() throws Exception {
		final Genomic_Sexual genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(this._random), this._random);
		this._locus.addAllele(this._alleleX);
		this._locus.addAllele(this._allelex);
		final Chromosome chromosome = new Chromosome_NoSex("no sex", genomic.getAlphabet());
		genomic.addChromosome(chromosome);
		chromosome.addLocus(this._locus);
		genomic.setExpresser(this._expresser);
		final Genome genome = new Genome_Linear(genomic, Ploidy.DIPLOID);
		assertEquals("ploidy", 2, genome.getPloidy());

		final Gene gene = GeneFactory.makeDiploid(this._locus, this._locus.getKey(this._alleleX),
				this._locus.getKey(this._allelex));
		genome.addGene(gene);
		assertEquals("size", 1, genome.size());

		final Phenotype phenotype = genome.express(null);
		assertEquals("size", 2, phenotype.size());
		final Trait trait = phenotype.getTrait("test");
		final Variant variant = trait.getVariant();
		assertEquals("variant", "Typica", variant.getIdentifier());
		final Genes gamete = genomic.createGamete(genome);
		final String genomeString = genome.toString();
		assertEquals("genome", "Genome_Linear CAGT- <genes=[" + LOCUSID + "=Xx], ploidy=2>", genomeString);
		assertEquals("size", 1, gamete.size());
		assertEquals("gamete gene", "x", ((AToString) gamete.getGene(0).getAllele(0)).toStringId());
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.factory.GenomeFactory#makeGamete(Genomic)} .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testEquals() throws Exception {
		final Genomic_Asexual genomic = new Genomic_Asexual("test genomic"); //$NON-NLS-1$
		this._locus.addAllele(this._alleleX);
		this._locus.addAllele(this._allelex);
		genomic.addLocus(this._locus);
		final Map<Locus, Expresser> expressers = new HashMap<>();
		expressers.put(this._locus, this._expresser);
		genomic.setExpressers(expressers);
		final Genes gameteA = GenomeFactory.makeGamete(genomic);
		assertEquals("gamete", "x", ((AToString) gameteA.getGene(0).getAllele(0)).toStringId());
		final Genes gameteB = GenomeFactory.makeGamete(genomic);
		final boolean equalsAB = gameteB.equals(gameteA);
		final boolean hashCodeAB = gameteB.hashCode() == gameteA.hashCode();
		assertTrue("hashEquals", equalsAB == hashCodeAB);
		assertTrue(gameteA + " = " + gameteB, equalsAB);
		final Genome gameteC = GenomeFactory.makeGamete(genomic);
		assertFalse(gameteC + " = " + gameteA, gameteC.equals(gameteA));
		assertFalse(gameteB + " = " + gameteC, gameteB.equals(gameteC));
		final Genome gameteD = GenomeFactory.makeGamete(genomic);
		assertTrue(gameteD + " = " + gameteA, gameteD.equals(gameteA));
	}

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testHaploidGenome() throws Exception {
		final Genomic_Asexual<Color, Boolean> genomic = new Genomic_Asexual<>("test genomic"); //$NON-NLS-1$
		final String alleleKey = this._locus.addAllele(this._alleleX);
		genomic.addLocus(this._locus);
		final Map<String, String> mapping = new HashMap<>();
		final String variantKey = this._character.getKey(this._typica);
		mapping.put(alleleKey, variantKey);
		final Expresser expresser = new Expresser_Direct(this._character, mapping, true);
		genomic.putExpresser(this._locus, expresser);
		final Genome genome = new Genome_Linear(genomic, Ploidy.HAPLOID);
		assertEquals("ploidy", 1, genome.getPloidy());
		final Gene gene = GeneFactory.makeHaploid(this._locus, this._locus.getKey(this._alleleX));
		genome.addGene(gene);
		assertEquals("size", 1, genome.size());
		final Phenotype phenotype = PhenotypeFactory.makeDawkinsian();
		genomic.express(phenotype, genome);
		assertEquals("size", 1, phenotype.size());
		final Variant variant = phenotype.getTrait("test").getVariant();
		final String identifier = variant.getIdentifier();
		assertEquals("trait", "Typica", identifier);
		final Genes gamete = genomic.createGamete(genome);
		assertEquals("size", 1, gamete.size());
		assertEquals("gamete gene", "X", ((AToString) gamete.getGene(0).getAllele(0)).toStringId());
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.factory.GenomeFactory#makeGamete(Genomic)} .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMakeGameteAsexual() throws Exception {
		final Genomic_Asexual genomic = new Genomic_Asexual("test genomic"); //$NON-NLS-1$
		this._locus.addAllele(this._alleleX);
		genomic.addLocus(this._locus);
		final Map<Locus, Expresser> expressers = new HashMap<>();
		expressers.put(this._locus, this._expresser);
		genomic.setExpressers(expressers);
		final Genes gamete = GenomeFactory.makeGamete(genomic);
		assertEquals("gamete", "X", ((AToString) gamete.getGene(0).getAllele(0)).toStringId());
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testMutate() throws Exception {
		final Mutator<Boolean> mutator = new Mutator_<Boolean>() {
			@Override
			public Allele<Boolean> mutate(final Allele<Boolean> allele) {
				// TODO use factory method
				return new TestAllele(true);
			}

			private static final long serialVersionUID = -119464429992416537L;
		};
		final Genomic_Asexual genomic = new Genomic_Asexual("test genomic", null, mutator);
		this._locus.addAllele(this._alleleX);
		genomic.addLocus(this._locus);
		genomic.setExpresser(this._expresser);
		final Genes gamete = GenomeFactory.makeGamete(genomic);
		final Allele<Boolean> allele = gamete.getGene(0).getAllele(0);
		assertEquals("gamete", "t", ((AToString) allele).toStringId());
	}

	/**
	 * 
	 */
	private static final String LOCUSID = "testLocus"; //$NON-NLS-1$

	private Locus<Boolean> _locus;

	private Random_Standard _random;

	private Allele<Boolean> _alleleX;

	private Allele<Boolean> _allelex;

	private Expresser _expresser;

	private Pharacter _character;

	private Variant _typica;

	static class TestAllele extends Allele_<Boolean> {

		/**
		 * @param value
		 */
		public TestAllele(final Boolean value) {
			super(value);
		}

		/**
		 * @see net.sf.darwin.core.Basic#getBases()
		 */
		@Override
		public String getBases() {
			return getValue().toString();
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 5674422053311982573L;

	}

	/**
	 * Method to look up a String (key) from a list of Strings (keys).
	 * 
	 * @param key
	 *            the String to be looked up.
	 * @param keys
	 *            the array of Strings.
	 * @return the index of the first matching key (if exists), else -1 for no
	 *         match.
	 */
	static int LookupIndex(final String key, final String[] keys) {
		for (int i = 0; i < keys.length; i++)
			if (key.equalsIgnoreCase(keys[i]))
				return i;
		return -1;
	}

}
