/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EnvironmentTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.16 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.base.Environment_;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.EnvironmentListener;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Realm;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class EnvironmentTest {

	/**
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testAddEcoFactor() {
		final String test = "test";
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		environment.addFactor(new EcoFactor_Humboldtian(test, Math.PI));
		final EcoFactor factor = environment.getEcoSystem().getFactor(test);
		// TODO consider doing this via polymorphism
		if (factor instanceof Quantifiable)
			assertEquals("value", Math.PI, ((Quantifiable) factor).doubleValue(), 0.001);
		else
			fail("eco factor is not numeric");
		final double value2 = environment.getEcoFactorValue(test);
		assertEquals("value2", Math.PI, value2, 0.001);
		try {
			environment.getEcoFactorValue("bad");
			fail("bad value");
		} catch (final Exception e) {
			// OK
		}
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testEnvironment() {
		final Realm realm = new Realm_Wallacian();
		final Environment_<Object> env = new Environment_Muirian<>(realm);
		env.setSink(new Sink_Log());
		assertEquals("count", 0, env.getCount());
		for (final Individual individual : env.getIndividuals())
			assertFalse("visible", individual.isVisible());
		for (final EnvironmentListener listener : env.getListeners())
			listener.onEnvironmentChange(env);
		assertEquals("realm", realm, env.getRealm());
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.Environment#addFactor(net.sf.darwin.core.EcoFactor)}
	 * .
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testSetEcoSystem() {
		final String test = "test";
		final Realm realm = new Realm_Wallacian();
		final Environment_Muirian environment = new Environment_Muirian(realm);
		final Collection<EcoFactor<Number>> ecoFactors = new ArrayList<>();
		ecoFactors.add(new EcoFactor_Humboldtian(test, Math.PI));
		environment.setEcoFactors(ecoFactors);
		final EcoFactor factor = environment.getEcoSystem().getFactor(test);
		// TODO consider doing this via polymorphism
		if (factor instanceof Quantifiable)
			assertEquals("value", Math.PI, ((Quantifiable) factor).doubleValue(), 0.001);
		else
			fail("eco factor is not numeric");
	}

}
