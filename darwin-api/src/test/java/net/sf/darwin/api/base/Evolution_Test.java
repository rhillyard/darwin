package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Evolution_Test {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetClock() throws Exception {
		final MockEvolution evolution = new MockEvolution();
		assertEquals(0, evolution.getClock());
		evolution.next();
		assertEquals(1, evolution.getClock());
	}

	// @Test
	public void testGetElapsedTime() {
		fail("Not yet implemented");
	}

	// @Test
	public void testIsActive() {
		fail("Not yet implemented");
	}

	// @Test
	public void testIsPaused() {
		fail("Not yet implemented");
	}

	@Test
	public void testNext() throws Exception {
		final MockEvolution evolution = new MockEvolution();
		final boolean next = evolution.next();
		assertFalse(next);
	}

	// @Test
	public void testPause() {
		fail("Not yet implemented");
	}

	// @Test
	public void testPostConfigure() {
		fail("Not yet implemented");
	}

	// @Test
	public void testPreConfigure() {
		fail("Not yet implemented");
	}

	// @Test
	public void testResume() {
		fail("Not yet implemented");
	}

	@Test
	public void testScheduleEvent() {
		final MockEvolution evolution = new MockEvolution();
		evolution.scheduleEvent(100, new Runnable() {

			@Override
			public void run() {
				evolution.setDone();

			}
		});
		try {
			Thread.sleep(200);
		} catch (final InterruptedException e) {
			// OK
		}
		evolution.isDone();
	}

	// @Test
	public void testSetWaitUntilComplete() {
		fail("Not yet implemented");
	}

	// @Test
	public void testShutdown() {
		fail("Not yet implemented");
	}

	// @Test
	public void testStop() {
		fail("Not yet implemented");
	}

	// @Test
	public void testStoppable() {
		fail("Not yet implemented");
	}

	// @Test
	public void testWaitUntilComplete() {
		fail("Not yet implemented");
	}

	// @Test
	public void testWriteEvolvables() {
		fail("Not yet implemented");
	}

	public static final class MockEvolution extends Evolution_ {
		public boolean isDone() {
			return this.done;
		}

		public void setDone() {
			this.done = true;
		}

		private boolean done;

		private static final long serialVersionUID = 1L;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

}
