/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: SerializeTest.java
 * Created on Jul 20, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.impl.Attribute_Standard;
import net.sf.darwin.api.impl.Locus_Triversian;
import net.sf.darwin.api.impl.Random_Standard;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Locus;

import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.Serializer;
import com.rubecula.beanpot.serialize.SerializerFactory;

/**
 * @author Robin Hillyard
 * 
 */
public class SerializeTest {

	/**
	 * 
	 */
	@Before
	public void setup() {
		this._random = new Random_Standard(0);
	}

	/**
	 * Tests: {@link SerializerFactory#createSerializer(String)},
	 * {@link SerializerFactory#createDeserializer(String, boolean)},
	 * {@link Serializer#writeObject(Object)}, {@link Serializer#readObject()},
	 * on an Object of type {@link Locus_Triversian}.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testSerializeLocus() throws Exception {
		final Locus locus = new Locus_Triversian("test locus", this._random);
		final Allele alleleD = AlleleFactory.makeDominanceAllele("x", true);
		locus.add(alleleD, 1);
		locus.add(AlleleFactory.makeDominanceAllele("x", false), 0);
		final Serializer sin = SerializerFactory.createSerializer("tempFile");
		sin.writeObject(locus);
		sin.close();
		final Serializer sout = SerializerFactory.createDeserializer("tempFile", true);
		if (sout.available() > 0) {
			final Locus locusCopy = (Locus) sout.readObject();
			assertTrue("serialization OK", locusCopy.equals(locus));
			assertEquals("available", 0, sout.available());
			final Allele alleleCopy = locusCopy.getAllele(locusCopy.pickAllele());
			assertEquals("allele value", Boolean.TRUE, alleleCopy.getValue());
			assertTrue("alleles", alleleCopy.equals(alleleD));
		} else
			fail("file is empty");
		sout.close();

	}

	/**
	 * Tests: {@link SerializerFactory#createSerializer(String)},
	 * {@link SerializerFactory#createDeserializer(String, boolean)},
	 * {@link Serializer#writeObject(Object)}, {@link Serializer#readObject()},
	 * on an Object of type {@link Attribute_Standard}.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public final void testWriteObject() throws Exception {
		final Serializer sin = SerializerFactory.createSerializer("tempFile");
		final Object pi = new Attribute_Standard("pi", java.lang.Math.PI);
		sin.writeObject(pi);
		sin.close();
		final Serializer sout = SerializerFactory.createDeserializer("tempFile", true);
		if (sout.available() > 0) {
			final Object object = sout.readObject();
			final boolean equals = object.equals(pi);
			assertTrue("serialization OK", equals);
			assertEquals("available", 0, sout.available());
		} else
			fail("file is empty");
		sout.close();
	}

	private Random_Standard _random;

}
