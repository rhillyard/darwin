/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GeneTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.43 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.base.Allele_;
import net.sf.darwin.api.base.Mutator_;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mutator;
import net.sf.tostring0.IToString;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class GeneTest {

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAuditString() {
		final Allele wc0 = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true);
		final Allele wc1 = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false);
		final Locus_Triversian locus = new Locus_Triversian("test locus", new Random_Standard());
		final Map<Allele, Integer> map = new HashMap<Allele, Integer>();
		map.put(wc0, ONE);
		map.put(wc1, ONE);
		locus.setAlleles(map);
		final Gene gene = GeneFactory.makeDiploid(locus, MockAllele.MELANISM, MockAllele.NON_MELANISM);
		gene.setLocus(locus);
		if (gene instanceof IToString) {
			assertEquals("audit", "Gene_Diploid test locus=Mm <locus=test locus, alleles=[Melanism, melanism]>", gene.toString());
			assertEquals("audit", "Gene_Diploid test locus=Mm", ((IToString) gene).toStringBrief(true));
			assertEquals("audit", "test locus=Mm", ((IToString) gene).toStringId());
		} else
			fail("gene doesn't implement IToString");
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGeneDiploid() {
		final Allele wc0 = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true);
		final Allele wc1 = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false);
		final Locus locus = new Locus_Triversian("test", null);
		locus.addAllele(wc0);
		locus.addAllele(wc1);
		final Gene gene = GeneFactory.makeDiploid(locus, wc0.getIdentifier(), wc1.getIdentifier());
		assertEquals("ploidy", 2, gene.getPloidy());
		assertEquals("slot 0", wc0, gene.getAllele(0));
		assertEquals("slot 1", wc1, gene.getAllele(1));
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGeneHaploid() {
		final Allele wc = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false);
		final Locus locus = new Locus_Triversian("test", null);
		locus.addAllele(wc);
		final Gene gene = GeneFactory.makeHaploid(locus, wc.getIdentifier());
		assertEquals("ploidy", 1, gene.getPloidy());
		assertEquals("slot 0", wc, gene.getAllele(0));
		try {
			gene.setAllele(1, "X");
			fail("setAllele value out of bounds");
		} catch (final RuntimeException e) {
			// nothing to do
		}
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testMutate() throws Exception {
		final Mutator<Object> mutator = new Mutator_<Object>() {
			@Override
			public Allele<Object> mutate(final Allele<Object> allele) {
				final Allele<Object> mutantAllele = new TestAllele("C");
				allele.getLocus().addAllele(mutantAllele);
				return mutantAllele;
			}

			private static final long serialVersionUID = 4984543689616303442L;
		};
		final Genomic genomic = new Genomic_Asexual("test genomic", null, mutator);
		final Chromosome_NoSex chromosome = new Chromosome_NoSex("test chromosome", genomic.getAlphabet());
		final Locus_Triversian locus = new Locus_Triversian("test locus", null);
		chromosome.addLocus(locus);
		genomic.addChromosome(chromosome);
		final Allele wc = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false);
		locus.addAllele(wc);
		final Gene gene = GeneFactory.makeHaploid(locus, wc.getIdentifier());
		final Gene mutant = gene.mutate(genomic.getMutator());
		assertEquals("mutant", gene.getPloidy(), mutant.getPloidy());
		final Allele mutantAllele = mutant.getAllele(0);
		assertEquals("mutant allele", "C", mutantAllele.getValue());
	}

	private static final Integer ONE = Integer.valueOf(1);

	static class TestAllele extends Allele_<Object> {

		/**
		 * @param value
		 */
		public TestAllele(final Object value) {
			super(value);
		}

		/**
		 * @see net.sf.darwin.core.Basic#getBases()
		 */
		@Override
		public String getBases() {
			return getValue().toString();
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -4319839395344833720L;

	}

}
