/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngine_Test.java
 * Created on Apr 1, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import net.sf.darwin.api.impl.FitnessEngine_Simple;
import net.sf.darwin.api.impl.FitnessFunction_ScaledNormal;
import net.sf.darwin.api.impl.MockEcoFactor;
import net.sf.darwin.api.impl.MockVariant;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.Fitness;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.FunctionListener;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

import org.junit.Test;

import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.Evaluator;

/**
 * @author Robin Hillyard
 * 
 */
public class FitnessEngine_Test extends TestBase {

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFitnessEngine_() {
		final FitnessEngine_<Object, Object> engine = new FitnessEngine_Simple();
		final int fitnesses = engine.countFitnesses();
		final WcSdFitness fitness = new WcSdFitness();
		assertNull(engine.putFitness("WCSD", fitness));
		assertEquals("fitnesses", 1, engine.countFitnesses() - fitnesses);
		for (final Fitness fitness2 : engine.getFitnessValues()) {
			assertEquals("fitness", fitness, fitness2);
		}
		for (final String key : engine.fitnessKeys()) {
			assertEquals("key", "WCSD", key);
			assertEquals("fitness", fitness, engine.getFitness(key));
		}
		assertEquals(fitness, engine.unregisterFitness("WCSD"));
		assertEquals("fitnesses", 0, engine.countFitnesses() - fitnesses);
	}

	// final static public class MockEcoFactor extends EcoFactor_Number {
	//
	// /**
	// * @param value
	// */
	// public MockEcoFactor(final double value) {
	// super(KEY_SOOT_DENSITY, Double.valueOf(value));
	// }
	//
	// /**
	// *
	// */
	// private static final long serialVersionUID = 5399132326839771733L;
	//
	// /**
	// *
	// */
	//		public static final String KEY_SOOT_DENSITY = "SootDensity"; //$NON-NLS-1$
	//
	// }

	// public static class MockVariant extends Variant_ {
	//
	// /**
	// * @param value
	// */
	// public MockVariant(final Enum<MockEnum_WC> value) {
	// super(value.toString(), value);
	// }
	//
	// /**
	// *
	// */
	// private static final long serialVersionUID = 2540165009478049260L;
	//
	// /**
	// * This is only for testing purposes (normally we declare variants in
	// * the XML file).
	// *
	// * {@link MockVariant} with parameter {@link MockEnum_WC#Typica}
	// */
	// public static final MockVariant TYPICA = new
	// MockVariant(MockEnum_WC.Typica);
	//
	// /**
	// * This is only for testing purposes (normally we declare variants in
	// * the XML file).
	// *
	// * {@link MockVariant} with parameter {@link MockEnum_WC#Carbonaria}
	// */
	// public static final MockVariant CARBONARIA = new
	// MockVariant(MockEnum_WC.Carbonaria);
	//
	// }

	public static class WcSdFitness extends Fitness_ implements HasExpressions {

		/**
		 * 
		 */
		public WcSdFitness() {
			this((Evaluator) null, null);
		}

		/**
		 * @param evaluator
		 * @param functionListener
		 *            the function listener (may be null)
		 * 
		 */
		public WcSdFitness(final Evaluator evaluator, final FunctionListener functionListener) {
			super(new FitnessFunction_ScaledNormal(evaluator), functionListener);
		}

		/**
		 * @see net.sf.darwin.core.HasExpressions#getExpressions()
		 */
		@Override
		public ExpressionMap getExpressions() {
			final ExpressionMap result = new ExpressionMap();
			final FitnessFunction fitnessFunction = getFitnessFunction();
			if (fitnessFunction instanceof EvalExpressionMutable) {
				result.addExpression("Fitness: Wing color / soot density", (EvalExpressionMutable) fitnessFunction); //$NON-NLS-1$			
			}
			return result;
		}

		/**
		 * @throws FitnessException
		 * @see net.sf.darwin.core.Fitness#getFitness(net.sf.darwin.core.Trait,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public double getFitness(final Trait trait, final EcoFactor factor) throws FitnessException {
			final Variant variant = trait.getVariant();
			// TODO consider doing this via polymorphism
			if (variant instanceof MockVariant) {
				if (factor instanceof MockEcoFactor) {
					final double sootEquivalent = ((MockEnum_WC) ((MockVariant) variant).getValue()).sootEquivalent();
					final double sootDensity = ((Quantifiable) factor).doubleValue();
					// XXX check that trait.getVariantKey() is correct (maybe
					// should be trait.getCharacterKey())
					return calculateFitness(sootEquivalent, sootDensity, trait.getVariantKey(), factor.getIdentifier());
				}
				return 0;
			}
			return 0;
		}

		/**
		 * @see net.sf.darwin.core.Fitness#getWeight(String,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public int getWeight(final String character, final EcoFactor factor) {
			if (character.equals(MockEnum_WC.ID)) {
				// TODO consider doing this via polymorphism
				if (factor instanceof MockEcoFactor)
					return 1;
				return 0;
			}
			return 0;
		}

		/**
		 * @return 0.25.
		 * 
		 * @see com.rubecula.darwin.domain.fitness.Fitness_#bandwidth(String)
		 */
		@Override
		protected double bandwidth(final String key) {
			return 0.2;
		}

		/**
		 * Scale down the eco factor by a factor of 10 before comparing with the
		 * soot equivalent.
		 * 
		 * @see com.rubecula.darwin.domain.fitness.Fitness_#scaleFactor(String)
		 */
		@Override
		protected double scaleFactor(final String key) {
			return 0.1;
		}

	}
}
