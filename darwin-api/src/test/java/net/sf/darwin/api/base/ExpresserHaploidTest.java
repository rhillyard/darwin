/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExpresserHaploidTest.java
 * Created on Apr 11, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.api.impl.Colony_Mayrian;
import net.sf.darwin.api.impl.Environment_Muirian;
import net.sf.darwin.api.impl.Expresser_Mendelian;
import net.sf.darwin.api.impl.Genomic_Asexual;
import net.sf.darwin.api.impl.Locus_Triversian;
import net.sf.darwin.api.impl.Pharacter_Fisherian;
import net.sf.darwin.api.impl.Population_Malthusian;
import net.sf.darwin.api.impl.Random_Standard;
import net.sf.darwin.api.impl.Realm_Wallacian;
import net.sf.darwin.api.impl.Variant_Basic;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Expression;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.BeanPot;

/**
 * @author Robin Hillyard
 * 
 *         FIXME these four unit tests should be working properly
 */
@SuppressWarnings({ "nls", "static-method" })
public class ExpresserHaploidTest {

	/**
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception {
		BeanPot.setConfigurationByResource(ExpresserHaploidTest.class, "testExpresser.xml"); //$NON-NLS-1$
		BeanPot.setDebug(true);
		BeanPot.configure();
		final BeanPot beanPot = BeanPot.getInstance();
		final Random random = (Random) beanPot.getBean("Random");
		random.setSeed(0);

	}

	/**
	 * 
	 */
	@After
	public void teardown() {
		BeanPot.cleanup();
	}

	/**
	 * 
	 */
	@Test
	public void testExpressGeneEnvironment1() {
		final Locus<Boolean> locus = new Locus_Triversian<>(null, null);
		locus.addAllele(AlleleFactory.makeDominanceAllele(ID_X, true));
		locus.addAllele(AlleleFactory.makeDominanceAllele(ID_X, false));
		final Pharacter<Color> character = new Pharacter_Fisherian<>("test");
		character.addVariant(new Variant_Basic<>(K_RED, Color.RED));
		character.addVariant(new Variant_Basic<>(K_BLUE, Color.BLUE));
		final Expresser<Color> expresser = new Expresser_Mendelian<>(character, K_T, K_RED);
		final Realm realm = new Realm_Wallacian();
		final Environment env = new Environment_Muirian(realm);
		final Population population = new Population_Malthusian("test population");
		final Colony colony = new Colony_Mayrian("test colony", env, null);
		population.addColony(colony);
		checkExpression(expresser, GeneFactory.makeDiploid(locus, K_T, K_T), colony, Color.RED);
		checkExpression(expresser, GeneFactory.makeDiploid(locus, K_T, K_F), colony, Color.RED);
		checkExpression(expresser, GeneFactory.makeDiploid(locus, K_F, K_T), colony, Color.RED);
		checkExpression(expresser, GeneFactory.makeDiploid(locus, K_F, K_F), colony, Color.BLUE);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.Expresser#express(Colony, Gene[])} .
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testExpressGeneEnvironment2() {
		final Realm realm = new Realm_Wallacian();
		final Population population = new Population_Malthusian("junk");
		final Environment env = new Environment_Muirian("env", realm);
		final Colony colony = new Colony_Mayrian("colony", env, null);
		population.addColony(colony);
		final Genomic<Boolean, Boolean> genomic = (Genomic<Boolean, Boolean>) BeanPot.getInstance().getBean("Genomic");
		// Now we will create 1000 genomes and express them.
		// Of the 2000 alleles, we should have a total of about 400 "true"
		// (Melanistic)
		// and about 1600 "false" (Non-melanistic).
		// The resulting 1000 phenotypes should comprise 360 Carbonaria (any
		// "true"
		// allele) and 640 Typica (both alleles false).
		// The latter number is derived as follows:
		// MAX * (4 / 5) ^ 2
		int carbonaria = 0;
		int typica = 0;
		this.melanistic = 0;
		final int MAX = 1000;
		assertEquals(2, genomic.getTotalLocusCount());
		final Map<? extends Allele<Boolean>, ? extends Integer> frequencies = ((Locus_<Boolean>) genomic.getLocus(0))
				.getAlleles();
		int total = 0;
		int nonDominant = 0;
		for (final Allele<Boolean> key : (Set<? extends Allele<Boolean>>) frequencies.keySet()) {
			final String attribute = key.getAttribute();
			final int freq = frequencies.get(key).intValue();
			total += freq;
			if (Character.isLowerCase(attribute.charAt(0)))
				nonDominant = freq;
		}
		final double proportionOfNonDominantAllele = 1. * nonDominant / total;
		final MockEnum_WC[] colors = new MockEnum_WC[MAX];
		for (int i = 0; i < MAX; i++) {
			final MockEnum_WC color = getTraitValue(population, genomic);
			colors[i] = color;
			switch (color) {
			case Typica:
				typica++;
				break;
			case Carbonaria:
				carbonaria++;
				break;
			default:
				break;
			}
		}
		System.out.println("Proportion of melanistic alleles: " + percentage(this.melanistic, 2 * MAX));
		System.out.println("Proportion of typica trait: " + percentage(typica, MAX));
		System.out.println("Proportion of carbonaria trait: " + percentage(carbonaria, MAX));
		assertEquals("melanistic", (1 - proportionOfNonDominantAllele), 1. * this.melanistic / (2 * MAX), 0.025);
		assertEquals("typica", proportionOfNonDominantAllele * proportionOfNonDominantAllele, 1. * typica / MAX, 0.025);
		assertEquals("trait", MockEnum_WC.Typica, colors[0]);
		assertEquals("trait", MockEnum_WC.Typica, colors[2]);
		assertEquals("trait", MockEnum_WC.Typica, colors[3]);
		assertEquals("trait", MockEnum_WC.Carbonaria, colors[4]);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.Expresser#express(Colony, Gene[])} .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testExpressGeneEnvironment3() {
		final Realm realm = new Realm_Wallacian();
		final Population population = new Population_Malthusian("junk");
		final Environment env = new Environment_Muirian("env", realm);
		final Colony colony = new Colony_Mayrian("colony", env, null);
		population.addColony(colony);
		final Genomic<Boolean, Boolean> genomic = (Genomic<Boolean, Boolean>) BeanPot.getInstance().getBean("Genomic");
		// This time we change the frequency of the non-melanistic allele.

		int carbonaria = 0;
		int typica = 0;
		this.melanistic = 0;
		final int MAX = 1000;
		assertEquals(2, genomic.getTotalLocusCount());
		final Locus_Random<Boolean> locus = (Locus_Random<Boolean>) genomic.getLocus(0);
		final Map<Allele<Boolean>, Integer> frequencies = locus.getAlleles();
		int total = 0;
		final int nonDominant = 3;
		for (final Allele<?> key : (Set<? extends Allele>) frequencies.keySet()) {
			final int freq = frequencies.get(key).intValue();
			if (Character.isLowerCase(key.getAttribute().charAt(0))) {
				locus.setFrequency(key.getIdentifier(), nonDominant);
				total += nonDominant;
			} else
				total += freq;
		}
		final double proportionOfNonDominantAllele = 1. * nonDominant / total;
		final MockEnum_WC[] colors = new MockEnum_WC[MAX];
		for (int i = 0; i < MAX; i++) {
			final MockEnum_WC color = getTraitValue(population, genomic);
			colors[i] = color;
			switch (color) {
			case Typica:
				typica++;
				break;
			case Carbonaria:
				carbonaria++;
				break;
			default:
				break;
			}
		}
		System.out.println("Proportion of melanistic alleles: " + percentage(this.melanistic, 2 * MAX));
		System.out.println("Proportion of typica trait: " + percentage(typica, MAX));
		System.out.println("Proportion of carbonaria trait: " + percentage(carbonaria, MAX));
		assertEquals("melanistic", (1 - proportionOfNonDominantAllele), 1. * this.melanistic / (2 * MAX), 0.025);
		assertEquals("typica", proportionOfNonDominantAllele * proportionOfNonDominantAllele, 1. * typica / MAX, 0.025);
		assertEquals("trait", MockEnum_WC.Typica, colors[0]);
		assertEquals("trait", MockEnum_WC.Typica, colors[2]);
		assertEquals("trait", MockEnum_WC.Typica, colors[3]);
		assertEquals("trait", MockEnum_WC.Carbonaria, colors[4]);
	}

	/**
	 * 
	 */
	@Test
	public void testExpressGenomeEnvironment() {
		final RandomGenerator random = new Random_Standard(0);
		final Allele<Boolean> alleleA = AlleleFactory.makeDominanceAllele(ID_X, true);
		final Allele<Boolean> alleleB = AlleleFactory.makeDominanceAllele(ID_X, false);
		final Locus<Boolean> locus = new Locus_Triversian<>(null, random);
		locus.addAllele(alleleA);
		locus.addAllele(alleleB);
		final Pharacter<Color> character = new Pharacter_Fisherian<>("test");
		character.addVariant(new Variant_Basic<>(K_RED, Color.RED));
		character.addVariant(new Variant_Basic<>(K_BLUE, Color.BLUE));
		final Genomic_Asexual<Color, Boolean> genomic = new Genomic_Asexual<>("test");
		genomic.putExpresser(locus, new ExpresserHaploid<>(character, Boolean.TRUE));
		final Genome<Color, Boolean> genome = GenomeFactory.makeLinear(genomic, Ploidy.HAPLOID);
		final Gene<Boolean> gene = GeneFactory.makeHaploid(locus, K_T);
		genome.addGene(gene);
		final Phenotype<Color> phenotype = genome.express(null);
		assertEquals("size", 1, phenotype.size());
		final Object value = alleleA.getValue();
		assertEquals("value", Boolean.TRUE, value);
		final Variant<?> variant = phenotype.getTrait("test").getVariant();
		final String identifier = variant.getIdentifier();
		assertEquals("trait", "red", identifier);
	}

	/**
	 * @param population
	 *            XXX
	 * @param genomic
	 * @return
	 */
	private MockEnum_WC getTraitValue(final Population population, final Genomic<Boolean, Boolean> genomic) {
		final Genome<Boolean, Boolean> gameteX = GenomeFactory.makeGamete(genomic);
		final Genes<Boolean> gameteY = GenomeFactory.makeGamete(genomic);
		final Genome<Boolean, Boolean> genome = GenomeFactory.makeLinear(genomic, Ploidy.DIPLOID);
		genome.fertilize(gameteX, gameteY);
		final Gene<Boolean> gene = genome.getGene(MockEnum_WC.ID);
		final Boolean value0 = gene.getAllele(0).getValue();
		final Boolean value1 = gene.getAllele(1).getValue();
		final Colony colony = population.getColony(0);
		final Phenotype phenotype = genome.express(colony);
		final Variant<?> variant = phenotype.getTrait(MockEnum_WC.ID).getVariant();
		final MockEnum_WC value = (MockEnum_WC) variant.getValue();
		final boolean isDominant = value == MockEnum_WC.Carbonaria;
		final boolean isDomAllele0 = value0.booleanValue();
		final boolean isDomAllele1 = value1.booleanValue();
		if (isDomAllele0)
			this.melanistic++;
		if (isDomAllele1)
			this.melanistic++;
		assertTrue(isDominant == (isDomAllele0 || isDomAllele1));
		return value;
	}

	/**
	 * @param frequency
	 * @param MAX
	 * @return
	 */
	@SuppressWarnings("nls")
	private String percentage(final int frequency, final int MAX) {
		return 100. * frequency / MAX + "%";
	}

	/**
	 * 
	 */
	private static final String ID_X = "x"; //$NON-NLS-1$

	private int melanistic;

	/**
	 * 
	 */
	private static final String K_BLUE = "blue"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String K_RED = "red"; //$NON-NLS-1$

	/**
	 * 
	 */
	static final String K_T = AlleleFactory.makeDominanceAllele(ID_X, true).getIdentifier();

	/**
	 * 
	 */
	static final String K_F = AlleleFactory.makeDominanceAllele(ID_X, false).getIdentifier();

	static class ExpresserHaploid<T> extends Expresser_<T> {

		/**
		 * @param character
		 * @param ignored
		 *            not sure how else to do this
		 */
		protected <V> ExpresserHaploid(final Pharacter<T> character, final V ignored) {
			super(character, ignored);
		}

		@Override
		public <E, V> Collection<Expression<T>> express(final Colony<E, T, V> colony, final Gene<V>... genes) {
			final Collection<Expression<T>> result = new ArrayList<>();
			result.add(express(genes));
			return result;
		}

		@Override
		public <V> Trait<T> express(final Gene<V>... genes) {
			if (genes.length == 1) {
				final Gene<V> gene = genes[0];
				if (gene.getPloidy() == Ploidy.HAPLOID)
					return expressHaploid(gene);
				throw new DarwinException("express: invalid gene ploidy: " + gene.getPloidy()); //$NON-NLS-1$
			}
			throw new DarwinException("express: expects only one gene parameter"); //$NON-NLS-1$
		}

		/**
		 * @param gene
		 * @return
		 */
		private <V> Trait<T> expressHaploid(final Gene<V> gene) {
			final String allele = gene.getAlleleKey(0);
			String trait = null;
			if (allele.equals(K_T))
				trait = K_RED;
			else if (allele.equals(K_F))
				trait = K_BLUE;
			return TraitFactory.makeDiscrete(getCharacter(), trait);
		}

	}

	/**
	 * @param expresser
	 * @param gene
	 * @param colony
	 * @param color1
	 */
	@SuppressWarnings({ "nls", "rawtypes", "unchecked" })
	private static <E, U, W> void checkExpression(final Expresser<U> expresser, final Gene<W> gene, final Colony colony,
			final U color1) {
		final Collection<Expression<U>> expressions = ((Expresser_<U>) expresser).express(colony, gene);
		assertEquals("expressions", 1, expressions.size());
		for (final Expression expression : expressions) {
			if (expression instanceof Trait) {
				final Trait<U> trait = (Trait<U>) expression;
				assertEquals("value", color1, trait.getValue());
			}
		}
	}
}
