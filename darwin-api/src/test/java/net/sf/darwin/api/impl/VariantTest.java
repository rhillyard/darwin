/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TraitFactoryTest.java
 * Created on May 31, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Variant;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class VariantTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.factory.TraitFactory#makeDiscrete(net.sf.darwin.core.Pharacter, String)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testVariant() {
		assertEquals("valuePi", Double.valueOf(Math.PI), variant0.getValue());
		assertEquals("idPi", "pi", variant0.getIdentifier());
		assertEquals("attrPi", "pi", variant0.getAttribute());
		assertNull("character null", variant0.getCharacter());
		final Pharacter character = new Pharacter_Fisherian("test");
		character.addVariant(variant0);
		assertEquals("character", character, variant0.getCharacter());
		assertEquals("valueE", Double.valueOf(Math.E), variant1.getValue());
		assertEquals("idE", "e", variant1.getIdentifier());
	}

	private static Variant variant0;

	private static Variant variant1;

	/**
	 */
	@SuppressWarnings("nls")
	@BeforeClass
	public static void setup() {
		variant0 = new Variant_Basic("pi", new Double(Math.PI));
		variant1 = new Variant_Basic("e", new Double(Math.E));
	}

}
