/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Function_Test.java
 * Created on Apr 16, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.api.base.Function_;
import net.sf.darwin.api.util.TestBase;

import org.junit.Test;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.Evaluator_;
import com.rubecula.jexpression.JexpressionException;
import com.rubecula.jexpression.Notation;
import com.rubecula.jexpression.Term;
import com.rubecula.jexpression.eval.Evaluator_Eval;
import com.rubecula.jexpression.jep.Evaluator_JEP;
import com.rubecula.jexpression.rpn.Evaluator_RPN_Mutable;

/**
 * @author Robin Hillyard
 * 
 */
public class Function_Test extends TestBase {

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testEvaluate() throws Exception {
		final Function_ function = new Function_Testable(new Evaluator_Eval(S_X_X));
		final Number result = function.evaluate("x", 3.0);
		assertEquals("value", 9.0, result.doubleValue(), 0.000001);

	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testFunction_() throws Exception {
		final Function_ function = new Function_Testable(new Evaluator_Eval(S_X_X));
		assertFalse("diff1", function.isDifferent());
		final String expression = function.getExpression();
		assertEquals("expression", S_X_X, expression);
		function.addSymbol("x", 3.0);
		final Number value = function.getValue();
		assertEquals("value", 9.0, value.doubleValue(), 0.000001);
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Function_#getErrorInfo()}.
	 */
	@SuppressWarnings("nls")
	public void testGetErrorInfo() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Function_#getEvaluator()}.
	 */
	@SuppressWarnings("nls")
	public void testGetEvaluator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Function_#normalDistributionDensityFunction(double, double, double)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testNormalDistributionDensityFunction() {
		assertEquals("normal", 0.3989422804014327, Function_.normalDistributionDensityFunction(0.0, 0.0, 1.0), 0.000000000001);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Function_#normalDistributionDensityFunction(double, double, double)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testNormalDistributionDensityFunctionScaled() {
		assertEquals("scaled normal", 1.0, Function_.normalDistributionDensityFunctionScaled(0.0, 0.0, 1.0), 0.000000000001);
		assertEquals("scaled normal", 0.99, Function_.normalDistributionDensityFunctionScaled(0.0, 0.1, 0.5), 0.00005);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Function_#normalDistributionDensityFunction(double, double, double)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testNormalDistributionDensityFunctionScaledRPN() throws Exception {
		final FitnessFunction_ScaledNormal fitFunc = new FitnessFunction_ScaledNormal(new Evaluator_RPN_Mutable());
		assertEquals("scaled normal", 1.0, fitFunc.getFitness(0, 0.0, 1.0), 0.000000000001);
		assertEquals("scaled normal", 0.99, fitFunc.getFitness(0, 0.1, 0.5), 0.00005);
		fitFunc.setExpression(new CharSequence[] { Concept.e, "$shapeFactor 2 * / $value $target - + 2", Concept.pow, "- *",
				Concept.pow });
		final String expression = fitFunc.getExpression();
		assertEquals("func", "E $shapeFactor 2 * / $value $target - + 2 ^ - * ^", expression);
		assertEquals("scaled normal", 1.0, fitFunc.getFitness(0, 0.0, 1.0), 0.000000000001);
		assertEquals("scaled normal", 0.99, fitFunc.getFitness(0, 0.1, 0.5), 0.00005);
		fitFunc.setExpression("E $shapeFactor 2.0 * / $value $target - + 2 ^ - * ^");
		assertEquals("scaled normal", 1.0, fitFunc.getFitness(0, 0.0, 1.0), 0.000000000001);
		final double fitness = fitFunc.getFitness(0, 0.1, 0.5);
		assertEquals("scaled normal", 0.99, fitness, 0.00005);
		final Term[] expressionTerms = fitFunc.getExpressionTerms();
		assertEquals("expression terms", 0, expressionTerms.length);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Function_#removeVariable(java.lang.String)}
	 * .
	 */
	@SuppressWarnings("nls")
	public void testRemoveVariable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Function_#setEvaluator(com.rubecula.jexpression.Evaluator)}
	 * .
	 */
	@SuppressWarnings("nls")
	public void testSetEvaluator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Function_#setExpression(java.lang.String)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testSetExpression() throws Exception {
		final Function_ function = new Function_Testable(new Evaluator_JEP());
		function.setExpression("(x+x)");
		assertTrue("diff1", function.isDifferent());
		final String expression = function.getExpression();
		assertEquals("expression", "(x+x)", expression);
		final Number value = function.evaluate("x", 3.0);
		assertEquals("value", 6.0, value.doubleValue(), 0.000001);
	}

	/**
	 * 
	 */
	private static final String S_X_X = "(x*x)"; //$NON-NLS-1$

	static class Function_Testable extends Function_ {

		/**
		 * @param evaluator
		 */
		public Function_Testable(final Evaluator evaluator) {
			super(evaluator);
			if (!(evaluator instanceof EvalExpressionMutable)) {
				try {
					if (Evaluator_.isDifferent(getExpression(), evaluator, standardFunctionTokens(evaluator.getNotation())))
						throw new RuntimeException("Function_Testable(): originalExpression inconsistent with evaluator"); //$NON-NLS-1$					
				} catch (final JexpressionException e) {
					throw new RuntimeException("Function_Testable(): Jexpression exception thrown", e); //$NON-NLS-1$					
				}
			}
		}

		@Override
		public String getIdentifier() {
			return "testable"; //$NON-NLS-1$
		}

		/**
		 * @param arguments
		 *            should be made up of one Number: x.
		 * @return x * x.
		 * @see net.sf.darwin.api.base.Function_#standardFunction(Object[])
		 */
		@Override
		protected double standardFunction(final Object... arguments) throws FunctionException {
			assert arguments.length == 1 : "Function_Test$Function_Testable.standardValue(): requires 1 argument"; //$NON-NLS-1$
			assert arguments[0] instanceof Number : "Function_Test$Function_Testable.standardValue(): argument must be a Number"; //$NON-NLS-1$
			final double x = ((Number) arguments[0]).doubleValue();
			return x * x;
		}

		/**
		 * @see net.sf.darwin.api.base.Function_#standardFunctionTokens(Notation)
		 */
		@Override
		protected CharSequence[] standardFunctionTokens(final Notation notation) {
			switch (notation) {
			case RPN:
				throw new UnsupportedOperationException("Function_Test does not support RPN notation"); //$NON-NLS-1$
			default:
				return new CharSequence[] { S_X_X };
			}
		}

	}

}
