/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TestBase.java
 * Created on Feb 2, 2007
 * @version $Revision: 1.59 $
 */

package net.sf.darwin.api.util;

import static org.junit.Assert.assertEquals;

import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.sf.darwin.api.base.Allele_Binary;
import net.sf.darwin.api.base.FitnessEngine_;
import net.sf.darwin.api.base.MateChoice_Wrightian;
import net.sf.darwin.api.base.Taxon_Darwinian;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.impl.Census_Standard;
import net.sf.darwin.api.impl.Chromosome_NoSex;
import net.sf.darwin.api.impl.Colony_Mayrian;
import net.sf.darwin.api.impl.Expresser_Mendelian;
import net.sf.darwin.api.impl.Fecundity_Saturated;
import net.sf.darwin.api.impl.Locus_Triversian;
import net.sf.darwin.api.impl.Mortality_Reaper;
import net.sf.darwin.api.impl.Pharacter_Fisherian;
import net.sf.darwin.api.impl.Population_Malthusian;
import net.sf.darwin.api.impl.Realm_Wallacian;
import net.sf.darwin.api.impl.Variant_Basic;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Fitness;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mortality;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.Variant;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.jexpression.Evaluator;

/**
 * @author Robin Hillyard
 * 
 */
public class TestBase {

	/**
	 * Utility method to "burn" a certain number of random numbers from the
	 * source.
	 * 
	 * @param howMany
	 */
	public void burn(final int howMany) {
		for (int i = 0; i < howMany; i++) {
			getRandom().nextInt();
		}
	}

	/**
	 * @param genomic
	 * @param environment
	 * @param phenome
	 * @param doExtras
	 * @return newly created population
	 * @throws GeneticsException
	 */
	public Population createPopulation(final Genomic genomic, final Environment environment, final Phenome phenome,
			final boolean doExtras) throws GeneticsException {
		return createPopulation(genomic, environment, phenome, getRandom(), doExtras);
	}

	/**
	 * @param genomic
	 * @param environment
	 * @param phenome
	 * @param random
	 * @param doExtras
	 * @return newly created population
	 * @throws GeneticsException
	 */
	@SuppressWarnings({ "nls", "boxing" })
	public Population createPopulation(final Genomic genomic, final Environment environment, final Phenome phenome,
			final RandomGenerator random, final boolean doExtras) throws GeneticsException {
		final Realm realm = new Realm_Wallacian();
		final Census censusTaker = new Census_Standard(new PrintWriter(System.out));
		final double infantMortality = 0.8;
		final Locus locus = new Locus_Triversian("test locus", getRandom());
		final Allele_Binary allele0 = (Allele_Binary) AlleleFactory.makeDominanceAllele("x", true);
		locus.addAllele(allele0);
		final Chromosome chromosome = new Chromosome_NoSex("no sex", genomic.getAlphabet());
		chromosome.addLocus(locus);
		genomic.addChromosome(chromosome);
		final Variant variant = new Variant_Basic("test trait", new Double(1));
		final Pharacter character = new Pharacter_Fisherian("test character");
		phenome.addCharacter(character);
		character.addVariant(variant);
		final Mortality reaper = new Mortality_Reaper(random, infantMortality, 0.1);
		final Taxon system = new Taxon_Darwinian("test system", realm, genomic, phenome, censusTaker, reaper,
				new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated());
		// final Taxon system = new
		// Taxon_Darwinian("test system",censusTaker,infantMortality,genomic,phenome);
		final Population population = new Population_Malthusian("test population");
		system.addPopulation(population); // void result
		final Colony colony = new Colony_Mayrian("test colony", environment, random);
		population.addColony(colony);
		assertEquals("Taxon System", system, population.getTaxon());

		if (doExtras) {
			final Expresser expresser = new Expresser_Mendelian(character, allele0.getIdentifier(), "test trait");
			final Variant variantOther = new Variant_Basic("other variant", new Double(2));
			character.addVariant(variantOther);
			final Allele allele1 = AlleleFactory.makeDominanceAllele("x", false);
			locus.addAllele(allele1);
			genomic.putExpresser(locus, expresser);
		}

		return population;
	}

	/**
	 * 
	 */
	@Before
	public void setUp() {
		this._random = new RandomJava(_SEED);
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		// do nothing
	}

	/**
	 * This is only here because otherwise surefire generates an error because
	 * this class does not specify any tests.
	 */
	@Test
	public void testNothing() {
		// do nothing
	}

	/**
	 * @return the randomizer
	 */
	protected RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * @param random
	 *            the random to set
	 * 
	 *            NOTE: If you want to be able to log the random number
	 *            generator, call this method with the following value:
	 *            <code>new Random_Standard(new RandomJava())</code> and update
	 *            the appropriate log properties.
	 */
	protected void setRandom(final RandomGenerator random) {
		this._random = random;
	}

	private static final int _SEED = 0;

	/**
	 * 
	 */
	protected static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$

	private RandomGenerator _random;

	/**
	 * @author Robin Hillyard
	 * 
	 */
	public static class FitnessEngine_Easy extends FitnessEngine_ {

		/**
		 * 
		 */
		public FitnessEngine_Easy() {
			super(null, null);
		}

		/**
		 * @see net.sf.darwin.api.base.FitnessEngine_#calculateFitness(net.sf.darwin.core.Phenotype,
		 *      net.sf.darwin.core.Environment)
		 */
		@Override
		public double calculateFitness(final Phenotype phenotype, final Environment environment) {
			return 1.0;
		}

		/**
		 * @see net.sf.darwin.api.base.FitnessEngine_#putFitness(java.lang.String,
		 *      net.sf.darwin.core.Fitness)
		 */
		@Override
		public Fitness putFitness(final String key, final Fitness fitness) {
			throw new RuntimeException("registering fitnesses with this engine is not allowed"); //$NON-NLS-1$
		}

		@Override
		protected void registerFitnesses(final Evaluator evaluator) {
			// do nothing
		}

	}

	/**
	 * @param clazz
	 * @param methodName
	 * @param paramClasses
	 * @param paramObjects
	 * @return the result of invoking the method with the given parameters.
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 */
	public static Object invokeStaticMethod(final Class<?> clazz, final String methodName, final Class<?>[] paramClasses,
			final Object[] paramObjects) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {

		final Method method = clazz.getDeclaredMethod(methodName, paramClasses);
		method.setAccessible(true);
		return method.invoke(null, paramObjects);
	}

}
