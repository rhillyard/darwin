/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Allele_SexTest.java
 * Created on Oct 31, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Sexual;
import net.sf.tostring0.AToString;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class Allele_SexTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Allele_Sex#Allele_Sex(boolean)} .
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testAllele_Sex() {
		final Allele allele = AlleleFactory.makeSexAllele(true);
		assertEquals("string", "Allele Y: true", allele.toString());
		assertEquals("string", "Y", ((AToString) allele).toStringId());
		assertEquals("female", false, ((Sexual) allele).isFemale());
	}

}
