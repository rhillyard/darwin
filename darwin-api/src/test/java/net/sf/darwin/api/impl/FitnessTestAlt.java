/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngineTester.java
 * Created on Mar 31, 2009
 * @version $Revision: 1.38 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.base.EcoFactor_Number;
import net.sf.darwin.api.base.FitnessEngine_;
import net.sf.darwin.api.base.FitnessFunction_;
import net.sf.darwin.api.base.Fitness_;
import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.api.base.MockEnum_WC;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

import org.junit.Test;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.EvaluatorFactory;
import com.rubecula.jexpression.Notation;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "boxing", "static-method" })
public class FitnessTestAlt {

	/**
	 * @throws Exception
	 * 
	 */
	@Test
	public void testEvaluateFitness1() throws Exception {
		final Evaluator evaluator = EvaluatorFactory.createEvaluatorMutableDefault();
		final FitnessEngine fitnessEngine = new FitnessEngineTester(evaluator);
		final Phenotype phenotype = PhenotypeFactory.makeDawkinsian();
		final Pharacter wingColor = new Pharacter_Fisherian(S_WING_COLOR);
		final Pharacter wingLength = new Pharacter_Fisherian(S_WING_LENGTH);
		final Variant wcTypica = new Variant_Basic(MockEnum_WC.Typica.toString(), 0.4);
		final Variant wl55 = new Variant_Basic(5.5);
		final String keyTypica = wingColor.addVariant(wcTypica);
		final String key55 = wingLength.addVariant(wl55);
		final Trait traitTypica = TraitFactory.makeDiscrete(wingColor, keyTypica);
		final Trait traitLong = TraitFactory.makeDiscrete(wingLength, key55);
		phenotype.addTrait(traitTypica);
		phenotype.addTrait(traitLong);
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		environment.addFactor(new EcoFactor_Humboldtian(S_SOOT_DENSITY, 7.0));
		environment.addFactor(new EcoFactor_Humboldtian(S_RIDGE_WIDTH, 6.0));
		final double fitness = fitnessEngine.calculateFitness(phenotype, environment);
		assertEquals("fitness", 0.1452, fitness, 0.000001);
	}

	/**
	 * @throws Exception
	 * 
	 */
	@Test
	public void testEvaluateFitness2() throws Exception {
		final FitnessEngine fitnessEngine = new FitnessEngineTester(null);
		final Phenotype phenotype = PhenotypeFactory.makeDawkinsian();
		final Pharacter wingColor = new Pharacter_Fisherian(S_WING_COLOR);
		final Pharacter wingLength = new Pharacter_Fisherian(S_WING_LENGTH);
		final Variant wcTypica = new Variant_Basic(MockEnum_WC.Typica.toString(), 0.4);
		final Variant wl55 = new Variant_Basic(5.5);
		final String keyTypica = wingColor.addVariant(wcTypica);
		final String key55 = wingLength.addVariant(wl55);
		final Trait traitTypica = TraitFactory.makeDiscrete(wingColor, keyTypica);
		final Trait traitLong = TraitFactory.makeDiscrete(wingLength, key55);
		phenotype.addTrait(traitTypica);
		phenotype.addTrait(traitLong);
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		environment.addFactor(new EcoFactor_Humboldtian(S_SOOT_DENSITY, 7.0));
		environment.addFactor(new EcoFactor_Humboldtian(S_RIDGE_WIDTH, 6.0));
		final double fitness = fitnessEngine.calculateFitness(phenotype, environment);
		assertEquals("fitness", 0.1452, fitness, 0.000001);
	}

	/**
	 * 
	 */
	static final String S_RIDGE_WIDTH = "RidgeWidth"; //$NON-NLS-1$

	/**
	 * 
	 */
	static final String S_SOOT_DENSITY = MockEcoFactor.KEY_SOOT_DENSITY;

	/**
	 * 
	 */
	static final String S_WING_LENGTH = "WingLength"; //$NON-NLS-1$

	/**
	 * 
	 */
	static final String S_WING_COLOR = "MockEnum_WC"; //$NON-NLS-1$

	static class Fitness extends Fitness_ {

		/**
		 * @param evaluator
		 * 
		 */
		public Fitness(final Evaluator evaluator) {
			super(new FitnessFunction_Test(evaluator), null);
		}

		/**
		 * @param fitnessFunction_Test
		 */
		public Fitness(final FitnessFunction_Test fitnessFunction_Test) {
			super(fitnessFunction_Test, null);
		}

		/**
		 * @see net.sf.darwin.core.Fitness#getEnvironmentFactor()
		 */
		@Override
		public Number getEnvironmentFactor() {
			return null;
		}

		/**
		 * @throws FitnessException
		 * @see net.sf.darwin.core.Fitness#getFitness(net.sf.darwin.core.Trait,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public double getFitness(final Trait trait, final EcoFactor factor) throws FitnessException {
			if (factor instanceof EcoFactor_Number && trait.getValue() instanceof Number)
				return calculateFitness((Double) trait.getValue(), ((EcoFactor_Number) factor).doubleValue(),
						trait.getCharacterKey(), factor.getIdentifier());
			return 0.;
		}

		/**
		 * @see net.sf.darwin.core.Fitness#getWeight(String,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public int getWeight(final String character, final EcoFactor factor) {
			if (character.equals(S_WING_COLOR) && factor.getIdentifier().equals(S_SOOT_DENSITY))
				return 2;
			if (character.equals(S_WING_LENGTH) && factor.getIdentifier().equals(S_RIDGE_WIDTH))
				return 1;
			return 0;
		}

		/**
		 * @see net.sf.darwin.core.Fitness#resetEnvironmentFactor(String,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public void resetEnvironmentFactor(final String trait, final EcoFactor factor) {
			// do nothing
		}

		/**
		 * @return 0.25.
		 * 
		 * @see net.sf.darwin.api.base.Fitness_#bandwidth(String)
		 */
		@Override
		protected double bandwidth(final String factorKey) {
			if (factorKey.equals(S_RIDGE_WIDTH))
				return 0.1;
			return 0.25;
		}

		@Override
		protected double scaleFactor(final String key) {
			if (key.equals(S_SOOT_DENSITY))
				return 0.1;
			if (key.equals(S_RIDGE_WIDTH))
				return 0.1;
			return super.scaleFactor(key);
		}

		@Override
		protected double scaleTrait(final String key) {
			if (key.equals(S_WING_LENGTH))
				return 0.1;
			return super.scaleTrait(key);
		}

	}

	static class FitnessEngineTester extends FitnessEngine_ {

		/**
		 * @param evaluator
		 * 
		 */
		public FitnessEngineTester(final Evaluator evaluator) {
			super(evaluator, null);
		}

		@Override
		protected void registerFitnesses(final Evaluator evaluator) {
			putFitness("", new Fitness(evaluator)); //$NON-NLS-1$
		}

	}

	static class FitnessFunction_Test extends FitnessFunction_ {

		/**
		 */
		public FitnessFunction_Test() {
			super();
		}

		/**
		 * @param evaluator
		 */
		public FitnessFunction_Test(final Evaluator evaluator) {
			super(evaluator);
		}

		/**
		 * @return <code>test</code>.
		 * 
		 * @see com.rubecula.darwin.foundation.Identifiable#getIdentifier()
		 */
		@Override
		public String getIdentifier() {
			return "test"; //$NON-NLS-1$
		}

		/**
		 * @param arguments
		 *            these arguments must be, in order,
		 *            <ol>
		 *            <li>Double: value</li>
		 *            <li>Double: mean</li>
		 *            <li>Double: standard deviation</li>
		 *            </ol>
		 * <br>
		 * 
		 *            Note that the arguments are auto-boxed for passing into
		 *            {@link #getFitnessByMethod(double, double, double)}. Note
		 *            also that we do not protect against cast exceptions.
		 * 
		 * @return the result of invoking
		 *         {@link #getFitnessByMethod(double, double, double)};
		 * 
		 * @see net.sf.darwin.api.base.Function_#standardFunction(java.lang.Object[])
		 */
		@Override
		protected double standardFunction(final Object... arguments) throws FunctionException {
			// No protection against class cast exceptions
			assert arguments.length == 3 : "FitnessTestAlt.standardValue(): requires 3 arguments"; //$NON-NLS-1$
			return getFitnessByMethod(((Number) arguments[0]).doubleValue(), ((Number) arguments[1]).doubleValue(),
					((Number) arguments[2]).doubleValue());
		}

		/**
		 * @return <code>1 - ((value - target) / shapeFactor) pow 2</code>
		 * 
		 * @see net.sf.darwin.api.base.Function_#standardFunctionTokens(Notation)
		 */
		@Override
		protected CharSequence[] standardFunctionTokens(final Notation notation) {
			switch (notation) {
			case RPN:
				throw new UnsupportedOperationException("Fitness test does not support RPN notation");
			default:
				return new CharSequence[] { "1 - ((" + FitnessFunction.VAR_VALUE + " - " + FitnessFunction.VAR_TARGET + ") / " //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
						+ FitnessFunction.VAR_SHAPE_FACTOR + ") ", //$NON-NLS-1$
						Concept.pow, " 2" }; //$NON-NLS-1$
			}
		}

		private static double getFitnessByMethod(final double value, final double mean, final double standardDeviation) {
			final double y = (value - mean) / standardDeviation;
			return 1 - y * y;
		}

	}
}
