/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngineTester.java
 * Created on Apr 1, 2009
 * @version $Revision: 1.22 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.base.MockEnum_WC;
import net.sf.darwin.api.base.MockFitnessEngine;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Trait;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "static-method" })
public class FitnessTest {

	/**
	 * Test method for {@link net.sf.darwin.api.base.Fitness_#Fitness_()}.
	 * 
	 * @throws FitnessException
	 */
	@Test
	public void testFitness_() throws FitnessException {
		final FitnessEngine engine = new MockFitnessEngine();
		final MockVariant carbonaria = MockVariant.CARBONARIA;
		final MockVariant typica = MockVariant.TYPICA;
		final Pharacter character = new Pharacter_Fisherian(MockEnum_WC.ID);
		final String vTypica = character.addVariant(typica);
		final String vCarbonaria = character.addVariant(carbonaria);
		final Trait traitCarbonaria = TraitFactory.makeDiscrete(character, vCarbonaria);
		final Trait traitTypica = TraitFactory.makeDiscrete(character, vTypica);
		final Phenotype phenotypeTypica = PhenotypeFactory.makeDawkinsian();
		phenotypeTypica.addTrait(traitTypica);
		final Phenotype phenotypeCarbonaria = PhenotypeFactory.makeDawkinsian();
		phenotypeCarbonaria.addTrait(traitCarbonaria);
		final MockEcoFactor polluted = new MockEcoFactor(7.0);
		final MockEcoFactor clean = new MockEcoFactor(3.0);
		final Realm realm = new Realm_Wallacian();
		final Environment environmentClean = new MockEnvironment("clean", realm);
		environmentClean.addFactor(clean);
		final Environment environmentPolluted = new MockEnvironment("polluted", realm);
		environmentPolluted.addFactor(polluted);

		assertEquals("fitness carb/polluted", 1.0, engine.calculateFitness(phenotypeCarbonaria, environmentPolluted), 0.001);
		final double fitness = engine.calculateFitness(phenotypeTypica, environmentPolluted);
		assertEquals("fitness typica/polluted", 0.67032, fitness, 0.001);
		assertEquals("fitness carb/clean", 0.67032, engine.calculateFitness(phenotypeCarbonaria, environmentClean), 0.001);
		assertEquals("fitness typica/clean", 1.0, engine.calculateFitness(phenotypeTypica, environmentClean), 0.001);
	}

}
