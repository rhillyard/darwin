/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_PseudoPoissonTest.java
 * Created on Sep 14, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.FitnessProblem;

import org.junit.Test;

import com.rubecula.jexpression.EvaluatorMutable;
import com.rubecula.jexpression.jep.Evaluator_JEP;
import com.rubecula.jexpression.rpn.Evaluator_RPN_Mutable;

/**
 * @author Robin Hillyard
 * 
 */
public class FitnessFunction_PseudoPoissonTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.FitnessFunction_PseudoPoisson#standardFunction(java.lang.Object[])}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testStandardFunction() throws Exception {
		System.out.println("testStandardFunction");
		final FitnessFunction fitnessFunction = new FitnessFunction_PseudoPoisson();
		final double target = 1.0;
		final double shapeFactor = 0.5;
		for (int i = 0; i < 100; i++) {
			final double value = i / 10.;
			try {
				showFitness(fitnessFunction, target, shapeFactor, value);
			} catch (final FitnessException e) {
				final FitnessProblem problem = e.getProblem();
				switch (problem) {
				case PoissonTargetExceedsValue:
					assertTrue("target exceeds value", target > value);
					break;

				default:
					fail("unsupportable fitness problem");
				}
			}
		}
	}

	/**
	 * TODO the first ten values (which should be NaN) are not.
	 * 
	 * Test method for
	 * {@link net.sf.darwin.api.impl.FitnessFunction_PseudoPoisson#standardFunction(java.lang.Object[])}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testStandardFunctionJEP() throws Exception {
		final EvaluatorMutable evaluator = new Evaluator_JEP();
		final FitnessFunction_PseudoPoisson fitnessFunction = new FitnessFunction_PseudoPoisson(evaluator);
		final String expression = fitnessFunction.getExpression();
		final String stdExpression = "1- e ^ (-shapeFactor/(value-target))";
		assertEquals("expression", stdExpression, expression);
		final double target = 1.0;
		final double shapeFactor = 0.5;
		final String modExpression = "1.0 - e ^ (-shapeFactor/(value-target))";
		System.out.println("testStandardFunctionJEP modified: " + modExpression);
		fitnessFunction.setExpression(modExpression);
		for (int i = 0; i < 100; i++) {
			final double value = i / 10.;
			showFitness(fitnessFunction, target, shapeFactor, value);
		}
	}

	/**
	 * TODO the first ten values (which should be NaN) are not.
	 * 
	 * Test method for
	 * {@link net.sf.darwin.api.impl.FitnessFunction_PseudoPoisson#standardFunction(java.lang.Object[])}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testStandardFunctionRPN() throws Exception {
		final EvaluatorMutable evaluator = new Evaluator_RPN_Mutable();
		final FitnessFunction_PseudoPoisson fitnessFunction = new FitnessFunction_PseudoPoisson(evaluator);
		final String expression = fitnessFunction.getExpression();
		final String stdExpression = "E $value $target - + / $shapeFactor * - ^ - 1 +";
		assertEquals("expression", stdExpression, expression);
		final double target = 1.0;
		final double shapeFactor = 0.5;
		final String modExpression = "E $value $target - + / $shapeFactor * - ^ - 1.0 +";
		System.out.println("testStandardFunctionRPN modified: " + modExpression);
		fitnessFunction.setExpression(modExpression);
		for (int i = 0; i < 100; i++) {
			final double value = i / 10.;
			showFitness(fitnessFunction, target, shapeFactor, value);
		}
	}

	/**
	 * This method is solely for testing purposes. TODO move into TestBase or
	 * similar location.
	 * 
	 * @param fitnessFunction
	 * @param target
	 * @param shapeFactor
	 * @param value
	 * @throws FitnessException
	 */
	private static void showFitness(final FitnessFunction fitnessFunction, final double target, final double shapeFactor,
			final double value) throws FitnessException {
		final double fitness = fitnessFunction.getFitness(value, target, shapeFactor);
		System.out.println("fitness for (" + value + ", " + target + ", " + shapeFactor + " = " + fitness); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

}
