/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OrganismTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.54 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.api.impl.EcoFactor_Humboldtian;
import net.sf.darwin.api.impl.Environment_Muirian;
import net.sf.darwin.api.impl.FitnessEngine_Simple;
import net.sf.darwin.api.impl.Genomic_Sexual;
import net.sf.darwin.api.impl.Meiosis_Locus;
import net.sf.darwin.api.impl.Organism_Sexual;
import net.sf.darwin.api.impl.Pharacter_Fisherian;
import net.sf.darwin.api.impl.Phenome_Gouldian;
import net.sf.darwin.api.impl.Realm_Wallacian;
import net.sf.darwin.api.impl.Variant_Basic;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Trait;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class OrganismTest extends TestBase {

	/**
	 * @see net.sf.darwin.api.util.TestBase#setUp()
	 */
	@Override
	public void setUp() {
		super.setUp();
		getRandom().setSeed(0);
	}

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testCalculateMortality() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Simple();
		fitnessFunction.putFitness("", new Fitness());
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		environment.addFactor(new EcoFactor_Humboldtian("test factor", 0.5));
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population_ population = (Population_) createPopulation(genomic, environment, phenome, true);
		// TODO work to do re: Colony
		final Colony colony = population.getColony(0);
		final Organism_ organism = (Organism_) Organism_Sexual.seed(colony);
		final Pharacter character = new Pharacter_Fisherian("test");
		final Trait trait = TraitFactory.makeDiscrete(character, character.addVariant(new Variant_Basic("test trait", 0.6)));
		final Phenotype phenotype = organism.getPhenotype();
		// XXX in order for this to work correctly, phenotype caching must be
		// active
		phenotype.addTrait(trait);

		final double fitness = organism.getFitness(population.getTaxon().getPhenome().getFitnessEngine()).doubleValue();
		// final double fitnessExpectedFormerly = 0.9048374;
		// XXX understand why the expected value has changed
		final double fitnessExpectedNow = 0.9512294245;

		assertEquals("fitness", fitnessExpectedNow, fitness, 0.0005);

		// final double mortalityExpectedFormerly = 0.8022;
		// XXX understand why the expected value has changed
		final double mortalityExpectedNow = 0.80209;
		assertEquals("mortality", mortalityExpectedNow, organism.calculateMortality(fitness), 0.0001);
		organism.age();
		assertEquals("age", 1, organism.getAge());

		// The mortality is much lower now because these organisms have a very
		// high infant mortality,
		// so once they get to their 2nd year, it's much easier to stay alive.
		// final double agedMortalityExpectedFormerly = 0.011;
		// XXX understand why the expected value has changed
		final double agedMortalityExpectedNow = 0.01046;
		assertEquals("mortality-aged", agedMortalityExpectedNow, organism.calculateMortality(fitness), 0.0001);
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testCreateGamete() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Simple();
		fitnessFunction.putFitness("", new Fitness());
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, true);
		// TODO work to do re: Colony
		final Colony colony = population.getColony(0);
		final Organism organism = Organism_Sexual.seed(colony);
		final Genome genome = genomic.createGamete(organism.getGenome());
		assertEquals("gamete size", 1, genome.size());
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Organism_Sexual#seed(Colony)} .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testOrganism() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Simple();
		fitnessFunction.putFitness("", new Fitness());
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, true);
		// TODO work to do re: Colony
		final Colony colony = population.getColony(0);
		final Organism_Sexual organism = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertEquals("population", colony, organism.getColony());
		assertEquals("age", 0, organism.getAge());
		assertEquals("ploidy", Ploidy.DIPLOID, organism.getGenome().getPloidy());
		assertEquals("phenotype size", 2, organism.getPhenotype().size());
		assertTrue("female", organism.isFemale());
	}

	static class Fitness extends Fitness_ {

		/**
		 * 
		 */
		public Fitness() {
			super();
		}

		/**
		 * @see net.sf.darwin.core.Fitness#getEnvironmentFactor()
		 */
		@Override
		public Number getEnvironmentFactor() {
			return null;
		}

		@Override
		public double getFitness(final Trait trait, final EcoFactor factor) throws FitnessException {
			if (factor instanceof EcoFactor_Number && trait.getValue() instanceof Double)
				return calculateFitness(((Double) trait.getValue()).doubleValue(), ((EcoFactor_Number) factor).doubleValue(),
						trait.getCharacterKey(), factor.getIdentifier());
			return 1.;
		}

		@Override
		@SuppressWarnings("nls")
		public int getWeight(final String character, final EcoFactor factor) {
			return character.equals(Pharacter.SEX) ? 0 : (character.equals("test")
					&& factor.getIdentifier().equals("test factor") ? 1 : 0);
		}

		/**
		 * @see net.sf.darwin.core.Fitness#resetEnvironmentFactor(String,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public void resetEnvironmentFactor(final String trait, final EcoFactor factor) {
			// do nothing
		}

		@Override
		protected double bandwidth(final String key) {
			return 0.1;
		}
	}

}
