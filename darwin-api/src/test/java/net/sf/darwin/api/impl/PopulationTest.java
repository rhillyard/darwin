/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PopulationTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.63 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.awt.Point;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.base.MateChoice_Wrightian;
import net.sf.darwin.api.base.Taxon_;
import net.sf.darwin.api.base.Taxon_Darwinian;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mortality;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Variant;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.core.VisualizableListener;
import net.sf.darwin.ui.base.Avatar;
import net.sf.tostring0.IToString;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class PopulationTest extends TestBase {

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAudit() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), //$NON-NLS-1$
				getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		if (population instanceof IToString)
			assertEquals("audit", "test population", ((IToString) population).toStringId());
		else
			fail("gene doesn't implement IToString");

	}

	/**
	 * Test method for {@link net.sf.darwin.core.Population#nextGeneration()}.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testNextGeneration() throws Exception {
		// This is for logging the random numbers...
		// setRandom(new Random_Standard(new RandomJava()));
		final Realm realm = new Realm_Wallacian();
		final Genomic_Sexual genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Environment environment = new Environment_Muirian(realm);
		environment.setIdealPopulation(100L);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Census censusTaker = new Census_Standard(new PrintWriter(System.out));
		final double infantMortality = 0.4;
		final Locus locus = new Locus_Triversian("unnamed locus", getRandom());
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", true));
		final Population population = createPopulation(genomic, environment, phenome, getRandom(), true);
		final Variant variant = new Variant_Basic("test trait", new Double(1));
		final Pharacter_Fisherian character = new Pharacter_Fisherian("test character");
		character.addVariant(variant);
		phenome.addCharacter(character);
		final Mortality reaper = new Mortality_Reaper(getRandom(), infantMortality, 0.1);
		final Taxon_ system = new Taxon_Darwinian("test system", realm, genomic, phenome, censusTaker, reaper,
				new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated());

		getRandom().setSeed(0);
		system.addPopulation(population); // void result
		system.addVisualizableListener(new TestListener());
		system.setSeedPopulation(2);
		final Colony other = new Colony_Mayrian("other", environment, getRandom());
		population.addColony(other);
		final Colony colony = population.getColony(0);
		population.seedMembers();
		assertEquals("count after seed", 2, colony.getCount());
		getRandom().nextDouble(); // burn a value
		getRandom().nextDouble(); // burn a value

		assertTrue("healthy", population.nextGeneration()); //$NON-NLS-1$

		assertEquals("count next", 6, colony.getCount());
		long totalAge = 0;
		long total = 0;
		final Collection<Organism> organisms = colony.getOrganisms();
		for (final Organism organism : organisms) {
			final int age = organism.getAge();
			totalAge += age;
			total++;
		}
		assertEquals("total age", 2, totalAge);
		System.out.println("average age: " + (1.0 * totalAge / total));
	}

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPopulation() throws Exception {
		final Realm realm = new Realm_Wallacian();
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Environment environment = new Environment_Muirian(realm);
		environment.setIdealPopulation(100L);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, new Random_Standard(0), true);
		// TODO need work
		final Colony colony = population.getColony(0);
		assertEquals("phenome", phenome, population.getTaxon().getPhenome());
		assertEquals("environment", environment, colony.getEnvironment());
		assertEquals("fitness", fitnessFunction, population.getTaxon().getPhenome().getFitnessEngine());
		assertEquals("genomic", genomic, population.getTaxon().getGenomic());
		final String nextIdentifier = colony.createIdentifier();
		assertEquals("identifier", "0-1360", nextIdentifier);
		final Taxon_ system = new Taxon_Darwinian("test system", realm, genomic, phenome, new Census_Standard(new PrintWriter(
				System.out)), 0.8, new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated(), getRandom());
		system.addPopulation(population); // void result
		system.addVisualizableListener(new VisualizableListener() {
			@Override
			public void onChange(final Visualizable source, final Object context) {
				if (source instanceof Population) {
					System.out.println("Population " + source.getIdentifier() + " has changed -- population now: "
							+ ((Population) source).getCount());
				}
				System.out.println("Object " + source.getIdentifier() + " has changed");
			}
		});
		system.setSeedPopulation(5);
		population.seedMembers();
		population.seedMembers();
		assertEquals("population", 10, colony.getCount());
		assertEquals("saturation", 0.1, colony.getSaturation(), 0.001);
	}

	static final class TestIndividual implements Avatar {

		/**
		 * @param organism
		 *            the organism corresponding to this individual.
		 * 
		 */
		TestIndividual(final Individual organism) {
			super();
			this._individual = organism;
			System.out.println("New " + toString()); //$NON-NLS-1$
		}

		@Override
		public Color getColor() {
			return null;
		}

		@Override
		public Individual getIndividual() {
			return this._individual;
		}

		@Override
		public Point getLocation() {
			return null;
		}

		@Override
		public double getSize() {
			return 1.0;
		}

		@Override
		public String toString() {
			return "Avatar based on: " + this._individual.toString(); //$NON-NLS-1$
		}

		private static final long serialVersionUID = 6792895788286118947L;

		private final Individual _individual;

	}

	/**
	 * @author Robin Hillyard
	 * 
	 */
	static final class TestListener implements VisualizableListener {

		/**
		 * 
		 */
		public TestListener() {
			super();
			this._model = new HashMap<Organism, Avatar>();
		}

		@Override
		public void onChange(final Visualizable source, final Object context) {
			if (source instanceof Colony) {
				final Colony colony = (Colony) source;
				System.out.println("Population " + colony.getIdentifier() + " has changed -- population now: " //$NON-NLS-1$ //$NON-NLS-2$
						+ colony.getCount());
				final Iterable<Organism> organisms = colony.getOrganisms();
				for (final Organism organism : organisms) {
					if (!organism.isViable()) {
						final Avatar avatar = this._model.get(organism);
						if (avatar == null) {
							if (organism.getAge() == 0) {
								this._model.put(organism, new TestIndividual(organism));
							}
						} else {
							final Avatar removed = this._model.remove(organism);
							System.out.println("removed: " + removed); //$NON-NLS-1$
						}
					}
				}
			} else
				System.out.println("Source " + source.getIdentifier() + " has changed" //$NON-NLS-1$ //$NON-NLS-2$
				);

		}

		private final Map<Organism, Avatar> _model;
	}

}
