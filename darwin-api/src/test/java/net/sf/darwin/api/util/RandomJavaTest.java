/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: RandomLoggedTest.java
 * Created on Jan 12, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class RandomJavaTest {

	/**
	 * Test method for
	 * {@link com.rubecula.util.random.RandomLogged#RandomLogged(long)}.
	 */
	@SuppressWarnings("nls")
	@Test
	public final void testRandomJava() {
		final RandomJava random = new RandomJava(0);
		final int peek1 = random.peek(32);
		assertEquals("peek1", -1155484576, peek1);
		assertEquals("nextInt", peek1, random.nextInt());
		final int peek2 = random.peek(32);
		assertEquals("peek2", -723955400, peek2);
		assertEquals("nextInt", peek2, random.nextInt());
		final int peek3 = random.peekInt(1000);
		assertTrue("peek3", peek3 >= 0 && peek3 < 1000);
		System.out.println("random number: " + peek3);
		assertEquals("nextInt(1000)", peek3, random.nextInt(1000));

	}

}
