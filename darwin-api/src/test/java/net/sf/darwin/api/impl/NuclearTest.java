/*******************************************************************************
 * Copyright (c) 2005, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.NuclearFactory;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Nuclear;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class NuclearTest {

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testClone() {
		final Genomic_Asexual genomic = new Genomic_Asexual("test genomic");
		final Genome genome = GenomeFactory.makeGamete(genomic);
		final Nuclear seed = NuclearFactory.makeClone(genome, MY_CLAN);
		assertTrue(seed instanceof Nuclear_Clone);
		final Genome gamete = genomic.createGamete(seed.getGenome());
		final Nuclear clone = new Nuclear_Clone(gamete, MY_CLAN);
		assertEquals(MY_CLAN, clone.getColonyId());
		assertEquals(gamete, clone.getGenome());
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testToString() {
		final RandomGenerator random = new Random_Standard(0L);
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(random), random);
		final Genome zygote1 = GenomeFactory.makeZygote(genomic);
		final Nuclear seed = NuclearFactory.makeZygote(zygote1, MY_CLAN);
		assertTrue(seed instanceof Nuclear_Zygote);
		assertEquals("toString", "Nuclear_Zygote <colonyId=" + MY_CLAN + ", genome=XX- <genes=[sex=XX], ploidy=2>, female=true>",
				seed.toString());
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testZygote() {
		final RandomGenerator random = new Random_Standard(0L);
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(random), random);
		final Genome zygote1 = GenomeFactory.makeZygote(genomic);
		final Nuclear seed = NuclearFactory.makeZygote(zygote1, MY_CLAN);
		assertTrue(seed instanceof Nuclear_Zygote);
		final Genome gamete = genomic.createGamete(seed.getGenome());
		final Nuclear_Zygote zygote = new Nuclear_Zygote(gamete, MY_CLAN);
		assertTrue(zygote.isFemale());
		assertEquals(MY_CLAN, zygote.getColonyId());
		assertEquals(gamete, zygote.getGenome());
		zygote.setFemale(false);
		zygote.setColonyId(YOUR_CLAN);
		assertFalse(zygote.isFemale());
		assertEquals(YOUR_CLAN, zygote.getColonyId());
	}

	private static final String YOUR_CLAN = "your clan"; //$NON-NLS-1$

	private static final String MY_CLAN = "my clan"; //$NON-NLS-1$

}
