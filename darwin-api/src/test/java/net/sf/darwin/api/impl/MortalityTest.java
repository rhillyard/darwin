/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MortalityTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.17 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.sf.darwin.api.base.Mortality_;
import net.sf.darwin.core.Mortality;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class MortalityTest {

	/**
	 * Test method for
	 * {@link Mortality_Reaper#calculateMortality(int, double, double)}
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testMortality_x() {
		// Construct a mortality object with no infant mortality, and with an
		// actuarial factor of only 10%.
		final Mortality_ reaper = new Mortality_Reaper(new Random_Standard(0L), 0.0, 0.1);
		final int age = 0;
		final double saturation = 1.0;
		assertEquals("mortality", 0.91791500, reaper.calculateMortality(age, 0.4, saturation), TOLERANCE_FINE);
		assertEquals("mortality", 0.9127541, reaper.calculateMortality(age, 0.41, saturation), TOLERANCE_FINE);
		assertEquals("mortality", 0.9230118, reaper.calculateMortality(age, 0.39, saturation), TOLERANCE_FINE);
		assertEquals("mortality", 0.9178637, reaper.calculateMortality(age, 0.4001, saturation), TOLERANCE_FINE);
		assertEquals("mortality", 0.9179663, reaper.calculateMortality(age, 0.3999, saturation), TOLERANCE_FINE);
		assertEquals("mortality", 0.9181199575706697, reaper.calculateMortality(age, 0.4, 1.001), TOLERANCE_FINE);
		assertEquals("mortality", 0.917709532150024, reaper.calculateMortality(age, 0.4, 0.999), TOLERANCE_FINE);
	}

	/**
	 * Test method for
	 * {@link Mortality_Reaper#calculateMortality(int, double, double)}
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testMortality0() {
		final double infantMortality = 0.1;
		final double actuarialFactor = 0.2;
		final Mortality_ reaper = new Mortality_Reaper(new Random_Standard(0L), infantMortality, actuarialFactor);
		final int baseAge = 3;
		final double baseFitness = 0.75;
		final double baseSaturation = 0.4;
		final double baseMortality = 0.60676;
		assertEquals("mortality", baseMortality, reaper.calculateMortality(baseAge, baseFitness, baseSaturation),
				TOLERANCE_FINEST);

		// Check the first (partial) derivative of mortality with respect to age
		final double derivativeFactorWRTage = -actuarialFactor;
		final int deltaAge = 1;
		final double toleranceAge = TOLERANCE_COARSE;
		assertEquals("mortality", getExpectedValue(baseMortality, derivativeFactorWRTage, deltaAge),
				reaper.calculateMortality(baseAge + deltaAge, baseFitness, baseSaturation), toleranceAge);
		assertEquals("mortality", getExpectedValue(baseMortality, derivativeFactorWRTage, -deltaAge),
				reaper.calculateMortality(baseAge - deltaAge, baseFitness, baseSaturation), toleranceAge);

		// Check the first (partial) derivative of mortality with respect to
		// fitness
		final double derivativeFactorWRTfitness = baseSaturation / baseFitness / baseFitness;
		final double deltaFitness = 0.1;
		final double toleranceFitness = TOLERANCE_MEDIUM;
		assertEquals("mortality", getExpectedValue(baseMortality, derivativeFactorWRTfitness, deltaFitness),
				reaper.calculateMortality(baseAge, baseFitness + deltaFitness, baseSaturation), toleranceFitness);
		assertEquals("mortality", getExpectedValue(baseMortality, derivativeFactorWRTfitness, -deltaFitness),
				reaper.calculateMortality(baseAge, baseFitness - deltaFitness, baseSaturation), toleranceFitness);

		// Check the first (partial) derivative of mortality with respect to
		// saturation
		final double derivativeFactorWRTsaturation = -1 / baseFitness;
		final double deltaSaturation = 0.1;
		final double toleranceSaturation = TOLERANCE_MEDIUM;
		assertEquals("mortality", getExpectedValue(baseMortality, derivativeFactorWRTsaturation, deltaSaturation),
				reaper.calculateMortality(baseAge, baseFitness, baseSaturation + deltaSaturation), toleranceSaturation);
		assertEquals("mortality", getExpectedValue(baseMortality, derivativeFactorWRTsaturation, -deltaSaturation),
				reaper.calculateMortality(baseAge, baseFitness, baseSaturation - deltaSaturation), toleranceSaturation);
	}

	/**
	 * Test method for {@link Mortality_Reaper#isMarked(double)}
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testMortality1() {
		// Don't move the ordering of these, as each gets a new random number
		final Mortality_ reaper = new Mortality_Reaper(new Random_Standard(0L), 0.1, 0.16);
		final double mortality = reaper.calculateMortality(3, 0.85, 0.5);
		assertEquals("mortality", 0.596765, mortality, TOLERANCE_FINEST);
		assertFalse("marked", reaper.isMarked(mortality));
		assertTrue("marked", reaper.isMarked(mortality));
		assertFalse("marked", reaper.isMarked(mortality));

		// Now, make it a little more likely that death will occur (more true
		// results).
		reaper.setBias(0.4);
		assertTrue("marked", reaper.isMarked(mortality));
		assertFalse("marked", reaper.isMarked(mortality));
		assertTrue("marked", reaper.isMarked(mortality));
		assertTrue("marked", reaper.isMarked(mortality));

		// Now, make it a little less likely that death will occur (more false
		// results).
		reaper.setBias(0.6);
		assertFalse("marked", reaper.isMarked(mortality));
		assertTrue("marked", reaper.isMarked(mortality));
		assertTrue("marked", reaper.isMarked(mortality));
		assertFalse("marked", reaper.isMarked(mortality));
		assertFalse("marked", reaper.isMarked(mortality));
	}

	/**
	 * Test method for {@link Mortality_Reaper#isMarked(double)}
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMortality2() {
		// Don't move the ordering of these, as each gets a new random number
		final Mortality reaper = new Mortality_Reaper(new Random_Standard(0L));
		assertTrue("marked-1", reaper.isMarked(1.0));
		assertFalse("marked-0", reaper.isMarked(0.0));
		assertFalse("marked-0.5", reaper.isMarked(0.5));
		assertFalse("marked-0.5", reaper.isMarked(0.5));
		assertFalse("marked-0.5", reaper.isMarked(0.5));
		assertTrue("marked-0.5", reaper.isMarked(0.5));
		assertTrue("marked-1", reaper.isMarked(1.0));
		assertTrue("marked-1", reaper.isMarked(1.0));
		assertTrue("marked-1", reaper.isMarked(1.0));
		assertFalse("marked-0", reaper.isMarked(0.0));
		assertFalse("marked-0", reaper.isMarked(0.0));
		assertFalse("marked-0", reaper.isMarked(0.0));
	}

	private static final double TOLERANCE_FINEST = 0.000001;

	private static final double TOLERANCE_FINE = 0.0001;

	private static final double TOLERANCE_MEDIUM = 0.004;

	private static final double TOLERANCE_COARSE = 0.01;

	/**
	 * @param baseValue
	 * @param derivativeFactor
	 * @param delta
	 *            the change in the variable that we are testing
	 * @return
	 */
	private static double getExpectedValue(final double baseValue, final double derivativeFactor, final double delta) {
		return baseValue * (1 + derivativeFactor * delta) - derivativeFactor * delta;
	}
}
