/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GenomicTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Ploidy;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class GenomicTest extends TestBase {

	/**
	 * @see net.sf.darwin.api.util.TestBase#setUp()
	 */
	@Override
	public void setUp() {
		super.setUp();
		getRandom().setSeed(0);
	}

	/**
	 * @see net.sf.darwin.api.util.TestBase#tearDown()
	 */
	@Override
	public void tearDown() {
		super.tearDown();
	}

	/**
	 * Test method for {@link Genomic_Sexual} .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGenomic() throws Exception {
		final String testGenomicIdentifier = "Test Genomic_Sexual";
		final Genomic_Sexual genomic = new Genomic_Sexual(testGenomicIdentifier, new Meiosis_Locus(getRandom()), getRandom());
		final Chromosome chromosome = new Chromosome_NoSex("no sex", genomic.getAlphabet());
		genomic.addChromosome(chromosome);
		assertEquals("ploidy", Ploidy.DIPLOID, genomic.getPloidy());
		// final Meiosis meiosis = genomic.getMeiosis();
		final Locus locus0 = new Locus_Triversian("locus 0", getRandom());
		chromosome.addLocus(locus0);
		locus0.addAllele(AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true));
		locus0.addAllele(AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false));
		assertEquals("size", 1, chromosome.getLoci().size());
		assertEquals("locus0", locus0, chromosome.getLocus(0));
		// final int index = meiosis.getIndex(Ploidy.DIPLOID, 0);
		// assertTrue("meiosis index", index >= 0 && index < Ploidy.DIPLOID);
		final Locus locus1 = new Locus_Triversian("locus 1", getRandom());
		chromosome.addLocus(locus1);
		locus1.addAllele(AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true));
		locus1.addAllele(AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false));
		assertEquals("size", 2, chromosome.getLoci().size());
		// assertEquals("locus1", locus1, genomic.getLocus(2));

	}

	/**
	 * Method to look up a String (key) from a list of Strings (keys).
	 * 
	 * @param key
	 *            the String to be looked up.
	 * @param keys
	 *            the array of Strings.
	 * @return the index of the first matching key (if exists), else -1 for no
	 *         match.
	 */
	static int LookupIndex(final String key, final String[] keys) {
		for (int i = 0; i < keys.length; i++)
			if (key.equalsIgnoreCase(keys[i]))
				return i;
		return -1;
	}

}
