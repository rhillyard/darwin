/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MutatorTest.java
 * Created on Apr 11, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.sf.darwin.core.Allele;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rubecula.beanpot.BeanPot;

/**
 * @author Robin Hillyard
 * 
 */
public class MutatorTest {

	/**
	 * 
	 */
	@After
	public void afterTest() {
		BeanPot.cleanup();
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Mutator_#Mutator_(boolean)}
	 * .
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testMutator_Mutate() {
		final BeanPot beanPot = BeanPot.getInstance();
		final Mutator_Random mutator = (Mutator_Random) beanPot.getBean("Mutator");
		mutator.getRandom().setSeed(0);
		final Allele allele = (Allele) beanPot.getBean("Melanism");
		mutator.setOdds(3); // instead of using the standard 1:100, we use 1:3
		// for testing purposes
		assertTrue("allele", (Boolean) allele.getValue());
		assertTrue("mutant1", (Boolean) mutator.mutate(allele).getValue());
		assertFalse("mutant2", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant3", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant4", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant5", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant6", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant7", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant8", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant9", (Boolean) mutator.mutate(allele).getValue());
		assertFalse("mutant10", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant11", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant12", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant13", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant14", (Boolean) mutator.mutate(allele).getValue());
		assertFalse("mutant15", (Boolean) mutator.mutate(allele).getValue());
		assertFalse("mutant16", (Boolean) mutator.mutate(allele).getValue());
		assertFalse("mutant17", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant18", (Boolean) mutator.mutate(allele).getValue());
		assertFalse("mutant19", (Boolean) mutator.mutate(allele).getValue());
		assertTrue("mutant20", (Boolean) mutator.mutate(allele).getValue());
	}

	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void beforeClass() throws Exception {
		BeanPot.setConfigurationByResource(MutatorTest.class, "testMutator.xml"); //$NON-NLS-1$
		BeanPot.setDebug(true);
		BeanPot.configure();
	}
}
