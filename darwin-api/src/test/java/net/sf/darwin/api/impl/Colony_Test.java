/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Colony_Test.java
 * Created on Jan 18, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class Colony_Test extends TestBase {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Colony_Mayrian#Colony_Mayrian(java.lang.String, net.sf.darwin.core.Environment, org.apache.commons.math.random.RandomGenerator)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public final void testColony_Mayrian1() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		final Colony_Mayrian colony = new Colony_Mayrian("test colony", environment, getRandom());
		population.addColony(colony);
		colony.setNDaughters(4);
		assertEquals("d1", "test colony-A", colony.getDaughterId());
		assertEquals("d2", "test colony-B", colony.getDaughterId());
		assertEquals("d3", "test colony-C", colony.getDaughterId());
		assertEquals("d4", "test colony-D", colony.getDaughterId());
		try {
			assertEquals("d5", "test colony-E", colony.getDaughterId());
			fail("should throw an exception");
		} catch (final Exception e) {
			// OK
		}
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Colony_Mayrian#Colony_Mayrian(java.lang.String, net.sf.darwin.core.Environment, org.apache.commons.math.random.RandomGenerator)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public final void testColony_Mayrian2() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		final Colony_Mayrian colony = new Colony_Mayrian("test colony", environment, getRandom());
		population.addColony(colony);
		colony.setNDaughters(65);
		assertEquals("d1", "test colony-AA", colony.getDaughterId());
		assertEquals("d2", "test colony-AB", colony.getDaughterId());
		assertEquals("d3", "test colony-AC", colony.getDaughterId());
		assertEquals("d4", "test colony-AD", colony.getDaughterId());
	}
}
