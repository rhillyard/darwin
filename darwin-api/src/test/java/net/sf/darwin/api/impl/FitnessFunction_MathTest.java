/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_MathTest.java
 * Created on Apr 6, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import net.sf.darwin.core.FitnessFunction;

import org.junit.Test;

import com.rubecula.jexpression.EvaluatorMutable;
import com.rubecula.jexpression.rpn.Evaluator_RPN_Mutable;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "static-method" })
public class FitnessFunction_MathTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.FitnessFunction_Math#getFitness(double, double, double)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testGetFitness1() throws Exception {
		final FitnessFunction function = new FitnessFunction_Math();
		assertEquals("fitness-0-1", 0.39894228, function.getFitness(0, 0.0, 1), 0.00001);
		assertEquals("fitness-1-1", 0.24195862, function.getFitness(1, 0.0, 1), 0.00001);

	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.FitnessFunction_Math#getFitness(double, double, double)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetFitness2() throws Exception {
		// XXX is this right?
		final FitnessFunction function = new FitnessFunction_Normal();
		assertEquals("fitness-0-1", 0.39894228, function.getFitness(0, 0.0, 1), 0.00001);
		assertEquals("fitness-1-1", 0.24197, function.getFitness(1, 0.0, 1), 0.00001);
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void testHashEquals() throws Exception {
		final EvaluatorMutable evaluator_e = new Evaluator_RPN_Mutable("E");
		final FitnessFunction function1 = new FitnessFunction_Normal(evaluator_e);
		final int hashCode1a = function1.hashCode();
		evaluator_e.setExpression("E - -");
		final int hashCode1b = function1.hashCode();
		assertEquals("", hashCode1a, hashCode1b);
		final FitnessFunction function2 = new FitnessFunction_Normal();
		assertFalse("h12", hashCode1a == function2.hashCode());
	}
}
