/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FecundityTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.core.Colony;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class FecundityTest {

	/**
	 * Test method for {@link Fecundity_Saturated#getFecundity(double)} .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testDefaultFecundity() {
		final Fecundity_Saturated fecundity = new Fecundity_Saturated(new double[] { 0.5, 1 });
		final int d = fecundity.getFecundity(1.6);
		assertEquals("fecundity", 0, d);
	}

	/**
	 * Test method for {@link Fecundity_Saturated#getFecundity(Colony)}
	 */
	@SuppressWarnings("nls")
	@Test
	public void testDefaultFecundityDoubleArray() {
		final Fecundity_Saturated fecundity = new Fecundity_Saturated();
		final int d = fecundity.getFecundity(0.8);
		assertEquals("fecundity", 2, d);
	}

}
