package net.sf.darwin.api.base;

import net.sf.darwin.api.base.FitnessEngine_Test.WcSdFitness;
import net.sf.darwin.core.Realm;

import com.rubecula.jexpression.Evaluator;

final public class MockFitnessEngine extends FitnessEngine_ {

	/**
	 * Public secondary constructor. Invoke #{@link FitnessEngine_} constructor.
	 */
	public MockFitnessEngine() {
		this((Realm) null);
	}

	/**
	 * Public primary constructor. Invoke super-constructor.
	 * 
	 * @param evaluator
	 */
	public MockFitnessEngine(final Evaluator evaluator) {
		this(evaluator, null);
	}

	/**
	 * Public primary constructor. Invoke super-constructor.
	 * 
	 * @param evaluator
	 * @param realm
	 *            XXX
	 */
	public MockFitnessEngine(final Evaluator evaluator, final Realm realm) {
		super(evaluator, realm);
	}

	/**
	 * Public primary constructor. Invoke super-constructor.
	 * 
	 * @param realm
	 *            XXX
	 */
	public MockFitnessEngine(final Realm realm) {
		this(null, realm);
	}

	/**
	 * @param evaluator
	 *            the evaluator (or null) for the fitness function
	 */
	@Override
	protected void registerFitnesses(final Evaluator evaluator) {
		putFitness("MockEnum_WC/SootDensity", new WcSdFitness(evaluator, this)); //$NON-NLS-1$
		final Realm realm = getRealm();
		if (realm != null)
			realm.getFitnessCache().register(this);
	}

}
