/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genomic_SexualTest.java
 * Created on Jun 11, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Pharacter;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class Genomic_SexualTest {

	/**
	 * @throws java.lang.Exception
	 */
	@SuppressWarnings({ "unused", "nls" })
	@Before
	public void setUp() throws Exception {
		this._random = new Random_Standard(0);
		this._mutator = new Mutator_Random<Boolean>(this._random, 2);
		this._meiosis = new Meiosis_Locus<Boolean>(this._random);
		final Genomic_Asexual<Object, Object> genomic = new Genomic_Asexual<>("no sex");
		this._chromosome = new Chromosome_NoSex<Boolean>("nosex", genomic.getAlphabet());
		this._genomic = new Genomic_Sexual<Object, Boolean>("test genomic", this._meiosis, this._mutator, this._random);
		this._genomic.addChromosome(this._chromosome);
	}

	/**
	 */
	@After
	public void tearDown() {
		// do nothing
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Genomic_#addChromosome(net.sf.darwin.core.Chromosome)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddChromosomeChromosome() {
		final Genomic_Asexual<Object, Boolean> genomic = new Genomic_Asexual<>("no sex");
		final Chromosome<Boolean> noSex = new Chromosome_NoSex<>("test chromosome", genomic.getAlphabet());
		assertTrue("added", this._genomic.addChromosome(noSex));
		assertEquals("chromosomes", 2, this._genomic.getCount());
		final Chromosome<?> chromosome0 = this._genomic.getChromosome(0);
		assertFalse("sex chromosome", chromosome0.isSexLinked());
		final Chromosome<?> chromosome1 = this._genomic.getChromosome(1);
		assertEquals("added chromosome", noSex, chromosome1);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Genomic_#putExpresser(net.sf.darwin.core.Locus, net.sf.darwin.core.Expresser)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddExpresser() throws Exception {
		final Pharacter<Object> character = new Pharacter_Fisherian<>("test character");
		final Expresser_Mendelian<Object> expresser = new Expresser_Mendelian<>(character, "", "");
		final Locus_Triversian<Boolean> locus = new Locus_Triversian<>("locus", this._random);
		this._genomic.getChromosome(0).addLocus(locus);
		this._genomic.putExpresser(locus, expresser);
		assertEquals("expresser", expresser, this._genomic.getExpresser(locus));
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddLocusIntLocus() throws Exception {
		final Locus_Triversian<Boolean> locus = new Locus_Triversian<>("locus", this._random);
		final Chromosome_NoSex<Boolean> chromosome = new Chromosome_NoSex<>("no sex", _genomic.getAlphabet());
		chromosome.addLocus(locus);
		this._genomic.addChromosome(chromosome);
		final int chromosomes = this._genomic.getCount();
		assertEquals("chromosomes", 2, chromosomes);
		final int lociTotal = this._genomic.getTotalLocusCount();
		assertEquals("loci", 2, lociTotal);
		final Collection<Locus<Boolean>> loci = this._genomic.getChromosome(1).getLoci();
		assertFalse(loci.contains(this._genomic.getSexGenomic().getLocus(0)));
		assertTrue(loci.contains(locus));
		assertEquals("sex locus", new Locus_Sex(this._random), this._genomic.getSexGenomic().getLocus(0));
		final Locus<?> locus0 = this._genomic.getLocus(0);
		assertEquals("locus0", locus, locus0);
		final Locus_Triversian<Boolean> locus1 = new Locus_Triversian<>("locus1", this._random);
		try {
			chromosome.addLocus(3, locus1);
			fail("should throw exception because index too large");
		} catch (final IndexOutOfBoundsException e) {
			// ok
		}
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddLocusLocus() throws Exception {
		final Locus_Triversian<Boolean> locus = new Locus_Triversian<>("locus", this._random);
		final Chromosome_NoSex<Boolean> chromosome = new Chromosome_NoSex<>("no sex", _genomic.getAlphabet());
		chromosome.addLocus(locus);
		this._genomic.addChromosome(chromosome);
		final int chromosomes = this._genomic.getCount();
		assertEquals("chromosomes", 2, chromosomes);
		final int lociTotal = this._genomic.getTotalLocusCount();
		assertEquals("loci", 2, lociTotal);
		final Collection<Locus<Boolean>> loci = chromosome.getLoci();
		assertFalse(loci.contains(this._genomic.getSexGenomic().getLocus(0)));
		assertTrue(loci.contains(locus));
		assertEquals("sex locus", new Locus_Sex(this._random), this._genomic.getSexGenomic().getLocus(0));
		final Locus<?> locus0 = this._genomic.getLocus(0);
		assertEquals("locus0", locus, locus0);
		try {
			chromosome.addLocus(locus);
			fail("should throw exception because locus is already present");
		} catch (final GeneticsException e) {
			// ok
		}
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Genomic_#createGamete(com.rubecula.darwin.domain.helper.Genes)}
	 * .
	 * 
	 * @throws GeneticsException
	 */
	@SuppressWarnings("nls")
	@Test
	public void testCreateGamete() throws GeneticsException {
		// XXX this is an odd test. Why would we want to create a gamete from a
		// zygote?
		final Genome zygote = GenomeFactory.makeZygote(this._genomic);
		final Genome gamete = this._genomic.createGamete(zygote);
		assertEquals("ploidy", 1, gamete.getPloidy());
		final boolean sex = gamete.getSex();
		assertTrue("sex", sex); // this depends on the random number source
		assertEquals("id", "X", gamete.getIdentifier()); // for now this should
		assertTrue("sex 2", this._genomic.createGamete(zygote).getSex());
		assertTrue("sex 3", this._genomic.createGamete(zygote).getSex());
		assertTrue("sex 3", this._genomic.createGamete(zygote).getSex());
		assertTrue("sex 3", this._genomic.createGamete(zygote).getSex());
		assertTrue("sex 3", this._genomic.createGamete(zygote).getSex());
		// TODO check that we don't always get true!
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Genomic_Sexual#Genomic_Sexual(java.lang.String, net.sf.darwin.core.Meiosis, RandomGenerator)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGenomic_SexualStringMeiosisRandom() {
		final Genomic_Sexual genomic = new Genomic_Sexual("test", this._meiosis, this._random);
		final Mutator<?> mutator = genomic.getMutator();
		assertTrue(mutator.isIdentity());
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Genomic_#getAlphabet()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetAlphabet() {
		final String alphabet = this._genomic.getAlphabet();
		assertEquals("alphabet", "CGAT", alphabet);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Genomic_Sexual#getChromosome(int)} .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetChromosome() {
		final Chromosome<?> chromosome = this._genomic.getChromosome(0);
		final String identifier = chromosome.getIdentifier();
		assertEquals("id", "nosex", identifier);
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Genomic_#getMeiosis()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetMeiosis() {
		final Meiosis<?> meiosis = this._genomic.getMeiosis();
		assertEquals("meiosis", this._meiosis, meiosis);
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testIsSexLinked() {
		// determine if the no-sex chromosome is sex linked (answer should be
		// false)
		final boolean sexLinked = this._genomic.getChromosome(0).isSexLinked();
		assertFalse("sex=linked", sexLinked);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Genomic_#mutate(net.sf.darwin.core.Genome)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMutate() {
		final Genome zygote = GenomeFactory.makeZygote(this._genomic);
		assertEquals("z size", 0, zygote.size());
		final Genome mutant1 = this._genomic.mutate(zygote);
		final Genome mutant2 = this._genomic.mutate(zygote);
		final Genome mutant3 = this._genomic.mutate(zygote);
		assertTrue("ploidy", mutant1.getPloidy() == zygote.getPloidy());
		assertTrue("size", mutant1.size() == zygote.size());
		for (int i = 0; i < zygote.size(); i++) {
			final Gene<?> geneZ = zygote.getGene(i);
			final Gene<?> geneM1 = mutant1.getGene(i);
			final Gene<?> geneM2 = mutant2.getGene(i);
			final Gene<?> geneM3 = mutant3.getGene(i);
			assertTrue("locus " + i, geneM1.getLocus() == geneZ.getLocus());
			// assertEquals(geneM1.getIdentifier(), geneZ.getIdentifier());
			final String a00 = geneZ.getAlleleKey(0);
			final String a01 = geneM1.getAlleleKey(0);
			final String a02 = geneM2.getAlleleKey(0);
			final String a03 = geneM3.getAlleleKey(0);
			final String a10 = geneZ.getAlleleKey(1);
			final String a11 = geneM1.getAlleleKey(1);
			final String a12 = geneM2.getAlleleKey(1);
			final String a13 = geneM3.getAlleleKey(1);
			assertTrue(a00 == a01);
			assertTrue(a00 == a02);
			assertTrue(a00 != a03);
			assertTrue(a10 == a11);
			assertTrue(a10 != a12);
			assertTrue(a10 == a13);
		}
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Genomic_#setChromosomes(java.util.Collection)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testSetChromosomes() {
		final Chromosome<Boolean> noSex = new Chromosome_NoSex<>("test chromosome", _genomic.getAlphabet());
		final Collection<Chromosome<Boolean>> list = new ArrayList<>();
		list.add(noSex);
		this._genomic.setChromosomes(list);
		assertEquals("chromosomes", 1, this._genomic.getCount());
		final Chromosome<?> chromosome0 = this._genomic.getChromosome(0);
		assertFalse("sex chromosome", chromosome0.isSexLinked());
		assertEquals("added chromosome", noSex, chromosome0);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Genomic_#setExpresser(net.sf.darwin.core.Expresser)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testSetExpresser() {
		final Pharacter<Object> character = new Pharacter_Fisherian<>("test character");
		final Expresser_Mendelian<Object> expresser = new Expresser_Mendelian<>(character, "", "");
		this._genomic.setExpresser(expresser);
		assertEquals("expresser", expresser, this._genomic.getExpresser(null));
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Genomic_Sexual#setExpressers(java.util.Map)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testSetExpressers() {
		assertEquals("size", 0, this._genomic.getExpressers().size());
		final Map<Locus<Boolean>, Expresser<Object>> hashMap = new HashMap<>();
		final Pharacter_Fisherian<Object> character = new Pharacter_Fisherian<>("test character");
		hashMap.put(new Locus_Triversian<Boolean>("locus", this._random), new Expresser_Mendelian<>(character, "", ""));
		this._genomic.setExpressers(hashMap);
		assertEquals("size", 1, this._genomic.getExpressers().size());
	}

	/**
	 * TODO I don't really like the fact that the identifier "test genomic"
	 * shows up twice. But it's because the chomorosomes hold the identifier and
	 * ToString doesn't allow us to suppress the identifier.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testToString() {
		final String string = this._genomic.toString();
		assertEquals(
				"toString",
				"Genomic_Sexual test genomic <chromosomes=test genomic, meiosis=Meiosis_Locus, mutator=Mutator_Random, ploidy=2, sexGenomic=test genomic-sex>",
				string);
	}

	private Genomic_Sexual<Object, Boolean> _genomic;

	private RandomGenerator _random;

	private Mutator<Boolean> _mutator;

	private Meiosis<Boolean> _meiosis;

	private Chromosome<Boolean> _chromosome;

}
