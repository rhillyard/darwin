/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FrequencyMapTest.java
 * Created on Nov 6, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.sf.darwin.core.Frequency;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class FrequencyMapTest {

	/**
	 */
	@SuppressWarnings({ "nls", "static-method" })
	@Test
	public void testFrequencyMap1() {
		final FrequencyMap_HashMap<String> map = new FrequencyMap_HashMap<String>("test");
		assertTrue("check", map.check());
		assertEquals("name", "test", map.getName());
		assertEquals("total", 0, map.getTotal());
		assertEquals("size", 0, map.size());
		assertTrue("empty", map.isEmpty());
		assertFalse("ok", map.containsKey("ok"));
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFrequencyMap2() {
		final FrequencyMap_HashMap<String> map = new FrequencyMap_HashMap<String>("test");
		final String hello = "Hello";
		map.add(hello);
		assertTrue("check", map.check());
		assertTrue(hello, map.containsKey(hello));
		assertEquals("total", 1, map.getTotal());
		assertEquals("frequency", 1, map.get(hello));
		for (final Frequency frequency : map.getMap().values()) {
			assertEquals("frequency2", 1, frequency.intValue());
		}
		for (final String key : map.keySet()) {
			assertEquals("key", hello, key);
		}
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testFrequencyMap3() {
		final FrequencyMap_HashMap<String> map = new FrequencyMap_HashMap<String>("test");
		final String hello = "Hello";
		final String goodbye = "Goodbye";
		map.add(hello);
		assertTrue("check", map.check());
		map.add(goodbye);
		assertTrue("check", map.check());
		map.add(hello);
		assertTrue("check", map.check());
		assertEquals("total", 3, map.getTotal());
		assertEquals("frequency", 2, map.get(hello));
		assertEquals("frequency", 1, map.get(goodbye));
		assertEquals("frequency", 2, map.remove(hello));
		assertTrue("check", map.check());
		assertEquals("total", 1, map.getTotal());
		assertEquals("frequency", 0, map.get(hello));
		assertEquals("frequency", 1, map.remove(goodbye));
		assertTrue("check", map.check());
		assertEquals("total", 0, map.getTotal());
		assertEquals("frequency", 0, map.get(goodbye));
		assertTrue("empty", map.isEmpty());
	}

}
