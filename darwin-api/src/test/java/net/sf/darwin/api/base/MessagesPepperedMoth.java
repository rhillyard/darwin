package net.sf.darwin.api.base;

/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Messages.java
 * Created on Feb 20, 2009
 * @version $Revision: 1.9 $
 */

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Robin Hillyard
 * 
 *         FIXME put this in a proper place, with its proper messages file.
 * 
 */
public final class MessagesPepperedMoth {
	private MessagesPepperedMoth() {
		// do nothing
	}

	private static final String BUNDLE_NAME = "net.sf.darwin.api.base.messagesPepperedMoth"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	/**
	 * @param key
	 * @return the string for the key
	 */
	public static String getString(final String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (final MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
