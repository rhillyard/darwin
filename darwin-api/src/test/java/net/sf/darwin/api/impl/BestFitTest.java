/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: BestFitTest.java
 * Created on Dec 20, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.base.Best_;
import net.sf.darwin.api.base.ComparableValue_;
import net.sf.darwin.core.ProcessBest;
import net.sf.darwin.core.ValueException;
import net.sf.tostring0.ToString;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "boxing", "nls", "static-method" })
public class BestFitTest {

	/**
	 * @throws Exception
	 */
	@Test
	public final void testUpdate0() throws Exception {
		final TestBestFit bestFit = new TestBestFit(0, new ProcessBest<IdentifiableTest>() {
			@Override
			public void onUpdate(final IdentifiableTest object) {
				System.out.println("new best word: " + object + " with value: " + object.getValue());
			}
		});
		assertEquals("update 1", true, bestFit.update(new IdentifiableTest("1"), true));
		assertEquals("update 1a", false, bestFit.update(new IdentifiableTest("1"), true));
		assertEquals("update one", false, bestFit.update(new IdentifiableTest("one"), true));
		assertEquals("update 2", true, bestFit.update(new IdentifiableTest("2"), true));
		assertEquals("update 2a", false, bestFit.update(new IdentifiableTest("2"), true));
		assertEquals("update 2b", false, bestFit.update(new IdentifiableTest("2"), true));
	}

	/**
	 * @throws Exception
	 */
	@Test
	public final void testUpdate1() throws Exception {
		final TestBestFit bestFit = new TestBestFit(1, new ProcessBest<IdentifiableTest>() {
			@Override
			public void onUpdate(final IdentifiableTest object) {
				System.out.println("new best word: " + object + " with value: " + object.getValue());
			}
		});
		assertEquals("update 1", false, bestFit.update(new IdentifiableTest("1"), false));
		assertEquals("update 1a", true, bestFit.update(new IdentifiableTest("1"), false));
	}

	/**
	 * test {@link Best_#update(Collection)};
	 * 
	 * @throws Exception
	 */
	@Test
	public final void testUpdate2() throws Exception {
		final Collection<String> words = new ArrayList<String>();
		CollectionUtils.addAll(words, new String[] { "a", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog" });
		final Collection<IdentifiableTest> candidates = new ArrayList<IdentifiableTest>();
		for (final String word : words) {
			candidates.add(new IdentifiableTest(word));
		}
		final TestBestFit bestFit = new TestBestFit("word", candidates, new ProcessBest<IdentifiableTest>() {
			@Override
			public void onUpdate(final IdentifiableTest object) {
				System.out.println("new best word: " + object + " with value: " + object.getValue());
			}
		});
		System.out.println(bestFit.toString());
		assertEquals("best word", "the", bestFit.getObject().getIdentifier());
		assertEquals("best value", 0.7813495675921711, bestFit.getValue().doubleValue(), 0.000001);
	}

	/**
	 *
	 */
	@ToString
	public static class TestBestFit extends Best_<IdentifiableTest> {

		/**
		 * @param convergence
		 *            XXX
		 * @param processor
		 *            XXX
		 * 
		 */
		public TestBestFit(final int convergence, final ProcessBest<IdentifiableTest> processor) {
			super("test object", processor, convergence);
		}

		/**
		 * @param id
		 * @param candidates
		 * @param processor
		 * @throws ValueException
		 */
		public TestBestFit(final String id, final Collection<IdentifiableTest> candidates,
				final ProcessBest<IdentifiableTest> processor) throws ValueException {
			super(id, candidates, processor);
		}

		/**
		 * @see net.sf.darwin.api.base.Best_#isUnique(com.rubecula.darwin.foundation.ComparableValue)
		 */
		@Override
		protected boolean isUnique(final IdentifiableTest candidate) {
			if (getObject() == null)
				return true;
			return !getObject().getIdentifier().equals(candidate.getIdentifier());
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 8271340561555177075L;

	}

	static class IdentifiableTest extends ComparableValue_ {

		/**
		 * @param id
		 *            XXX
		 * 
		 */
		public IdentifiableTest(final String id) {
			super();
			this._id = id;
		}

		/**
		 * @see net.sf.tostring0.Identifiable#getIdentifier()
		 */
		@Override
		public String getIdentifier() {
			return this._id;
		}

		/**
		 * @return a value which represents the dictionary order of this object.
		 * @see net.sf.darwin.core.Valuable#getValue()
		 */
		@Override
		public Number getValue() {
			try {
				return Integer.valueOf(getIdentifier());
			} catch (final NumberFormatException e) {
				double result = 0;
				for (int i = getIdentifier().length() - 1; i >= 0; i--)
					result = (result + (getIdentifier().charAt(i) - 'a' + 1)) / 26.;
				return result;
			}
		}

		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return getIdentifier() + ": " + getValue();
		}

		private final String _id;

	}
}
