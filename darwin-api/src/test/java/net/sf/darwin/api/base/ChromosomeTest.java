/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ChromosomeTest.java
 * Created on Jun 11, 2009
 * @version $Revision: 1.25 $
 */

package net.sf.darwin.api.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.darwin.api.impl.Chromosome_NoSex;
import net.sf.darwin.api.impl.Chromosome_Sex;
import net.sf.darwin.api.impl.Genomic_Sexual;
import net.sf.darwin.api.impl.Locus_Sex;
import net.sf.darwin.api.impl.Locus_Triversian;
import net.sf.darwin.api.impl.Random_Standard;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.Detail;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.Serializer;
import com.rubecula.beanpot.serialize.SerializerFactory;

/**
 * @author Robin Hillyard
 * 
 */
public class ChromosomeTest {

	/**
	 * @throws java.lang.Exception
	 */
	@SuppressWarnings({ "nls" })
	@Before
	public void setUp() throws Exception {
		this._random = new Random_Standard(0);
		final Genomic_Sexual<Object, Object> genomic_sexual = new Genomic_Sexual<>("sexual", null, null, (RandomGenerator) null);
		this._chromosome_NoSex = new Chromosome_NoSex<>("nosex", genomic_sexual.getAlphabet());
		this._chromosome_Sex = new Chromosome_Sex(this._random);
		this._locusSex = new Locus_Triversian<>("locus", this._random);
		this._locusNoSex = new Locus_Triversian<>("locus", this._random);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this._chromosome_NoSex = null;
		this._chromosome_Sex = null;
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#addLocus(int, net.sf.darwin.core.Locus)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddLocusIntLocus() throws Exception {
		final Locus<Boolean> junk1 = new Locus_Triversian<>("junkS", this._random);
		final Locus<Object> junk2 = new Locus_Triversian<>("junkN", this._random);
		this._chromosome_NoSex.add(0, this._locusNoSex);
		this._chromosome_NoSex.add(0, junk2);
		assertEquals("size", 2, this._chromosome_NoSex.getLoci().size());
		assertEquals("locus at 0", junk2, this._chromosome_NoSex.getLocus(0));
		assertEquals("locus at 1", this._locusNoSex, this._chromosome_NoSex.getLocus(1));
		this._chromosome_Sex.add(0, this._locusSex);
		this._chromosome_Sex.add(0, junk1);
		assertEquals("size", 3, this._chromosome_Sex.getLoci().size());
		assertEquals("locus at 0", junk1, this._chromosome_Sex.getLocus(0));
		assertEquals("locus at 1", this._locusSex, this._chromosome_Sex.getLocus(1));
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#addLocus(net.sf.darwin.core.Locus)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAddLocusLocus() {
		this._chromosome_NoSex.add(this._locusNoSex);
		assertEquals("size", 1, this._chromosome_NoSex.getLoci().size());
		assertEquals("locus", this._locusNoSex, this._chromosome_NoSex.getLocus(0));
	}

	/**
	 */
	@Test
	public void testAudit1() {
		final String audit1 = this._chromosome_NoSex.toString();
		final String expected1 = "Chromosome_NoSex nosex <loci=nosex <list=[]>, sexLinked=false>"; //$NON-NLS-1$
		assertEquals("audit", expected1, audit1); //$NON-NLS-1$
		final String audit2 = this._chromosome_Sex.toString(new Detail(true, true, true, false, true, null, true, 10,
				Integer.MAX_VALUE));
		final String expected2 = "Chromosome_Sex sex <loci=sex <list=[sex]>, sexLinked=true>"; //$NON-NLS-1$
		assertEquals("audit", expected2, audit2); //$NON-NLS-1$
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAuditString() {
		assertEquals("audit", "nosex", this._chromosome_NoSex.toStringId());
		assertEquals("audit", "Chromosome_Sex sex <loci=sex <list=[sex]>, sexLinked=true>", this._chromosome_Sex.toString());
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#contains(java.lang.Object)} .
	 */
	@Test
	public void testContains() {
		assertTrue(this._chromosome_Sex.contains(this._chromosome_Sex.getSexLocus()));
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#getIdentifier()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetIdentifier() {
		assertEquals("identifier_a", "nosex", this._chromosome_NoSex.getIdentifier());
		assertEquals("identifier_s", "sex", this._chromosome_Sex.getIdentifier());
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Chromosome_#getLoci()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetLoci() {
		final Collection<Locus<Object>> lociNoSex = this._chromosome_NoSex.getLoci();
		assertEquals("# loci a", 0, lociNoSex.size());
		final Collection<Locus<Boolean>> lociSex = this._chromosome_Sex.getLoci();
		assertEquals("# loci s", 1, lociSex.size());
		for (final Locus<Boolean> locus : lociSex) {
			assertEquals("id", Locus_Sex.SEX, locus.getIdentifier());
		}
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#indexOf(java.lang.Object)} .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testIndexOf() {
		assertEquals("index", 0, this._chromosome_Sex.indexOf(this._chromosome_Sex.getSexLocus()));
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Chromosome_#isEmpty()}.
	 */
	@Test
	public void testIsEmpty() {
		assertTrue(this._chromosome_NoSex.isEmpty());
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Chromosome_#isSexLinked()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testIsSexLinked() {
		assertFalse("a", this._chromosome_NoSex.isSexLinked());
		assertTrue("s", this._chromosome_Sex.isSexLinked());
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Chromosome_#remove(int)}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testRemoveInt() {
		assertNotNull("removed",
				this._chromosome_Sex.remove(this._chromosome_Sex.lastIndexOf(this._chromosome_Sex.getSexLocus())));
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#remove(java.lang.Object)} .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testRemoveObject() {
		assertTrue("removed", this._chromosome_Sex.remove(this._chromosome_Sex.getSexLocus()));
	}

	/**
	 * Tests: {@link SerializerFactory#createSerializer(String)},
	 * {@link SerializerFactory#createDeserializer(String, boolean)},
	 * {@link Serializer#writeObject(Object)}, {@link Serializer#readObject()},
	 * on an Object of type {@link Chromosome_NoSex}.
	 * 
	 * @throws Exception
	 * 
	 *             FIXME this should be working properly
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testSerializeChromosome() throws Exception {
		final List<Locus<Object>> list = new ArrayList<>();
		list.add(new Locus_Triversian<>("junk", this._random));
		this._chromosome_NoSex.setLoci(list);
		final Serializer sin = SerializerFactory.createSerializer("tempFile");
		sin.writeObject(this._chromosome_NoSex);
		sin.close();
		final Serializer sout = SerializerFactory.createDeserializer("tempFile", true);
		if (sout.available() > 0) {
			@SuppressWarnings("unchecked")
			final Chromosome<Object> chromosomeCopy = (Chromosome<Object>) sout.readObject();
			assertTrue("serialization OK", chromosomeCopy.equals(this._chromosome_NoSex));
			assertEquals("available", 0, sout.available());
			final Locus<Object> locusCopy = chromosomeCopy.getLocus(0);
			assertEquals("allele value", "junk", locusCopy.getIdentifier());
			assertTrue("alleles", locusCopy.equals(list.get(0)));
		} else
			fail("file is empty");
		sout.close();

	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.base.Chromosome_#setLoci(java.util.Collection)}
	 * .
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testSetLoci() {
		final List<Locus<Boolean>> listSex = new ArrayList<>();
		final List<Locus<Object>> listNoSex = new ArrayList<>();
		listSex.add(new Locus_Triversian<Boolean>("junk", this._random));
		listNoSex.add(new Locus_Triversian<>("junk", this._random));
		this._chromosome_NoSex.setLoci(listNoSex);
		assertEquals("n", 1, this._chromosome_NoSex.getCount());
		assertEquals("id", "junk", this._chromosome_NoSex.getLocus(0).getIdentifier());
		this._chromosome_Sex.setLoci(listSex);
		assertEquals("n", 2, this._chromosome_Sex.getCount());
		assertEquals("id", Chromosome.SEX, this._chromosome_Sex.getLocus(0).getIdentifier());
		assertEquals("id", "junk", this._chromosome_Sex.getLocus(1).getIdentifier());
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Chromosome_#toArray()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testToArray() {
		final Object[] arrayS = this._chromosome_Sex.toArray();
		assertEquals("array s", 1, arrayS.length);
		assertEquals("array s", this._chromosome_Sex.getSexLocus(), arrayS[0]);
		final Object[] arrayA = this._chromosome_NoSex.toArray();
		assertEquals("array a", 0, arrayA.length);
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testToArrayTArray() {
		@SuppressWarnings("unchecked")
		final Locus<Boolean>[] arrayS = this._chromosome_Sex.toArray(new Locus[] {});
		assertEquals("array s", 1, arrayS.length);
		assertEquals("array s", this._chromosome_Sex.getSexLocus(), arrayS[0]);
		@SuppressWarnings("unchecked")
		final Locus<Object>[] arrayA = this._chromosome_NoSex.toArray((Locus<Object>[]) new Locus<?>[] {});
		assertEquals("array a", 0, arrayA.length);
	}

	/**
	 * Test method for {@link net.sf.darwin.api.base.Chromosome_#toString()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testToString() {
		assertEquals("toString a", "nosex", this._chromosome_NoSex.toStringId());
		assertEquals("toString s", "sex", this._chromosome_Sex.toStringId());
	}

	private Chromosome_NoSex<Object> _chromosome_NoSex;

	private Chromosome_Sex _chromosome_Sex;

	private RandomGenerator _random;

	private Locus<Boolean> _locusSex;

	private Locus<Object> _locusNoSex;

}
