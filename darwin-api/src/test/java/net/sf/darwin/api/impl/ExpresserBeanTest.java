/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExpresserBeanTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.20 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Random;

import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.BeanPot;

/**
 * @author Robin Hillyard
 * 
 */
public class ExpresserBeanTest {

	/**
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception {
		BeanPot.setConfigurationByResource(ExpresserBeanTest.class, "testExpresserEnv.xml"); //$NON-NLS-1$
		BeanPot.setDebug(true);
		BeanPot.configure();
		final BeanPot beanPot = BeanPot.getInstance();
		final Random random = (Random) beanPot.getBean("Random");
		random.setSeed(0);
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		BeanPot.cleanup();
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testExpresser() {
		final Genomic genomic = (Genomic) BeanPot.getInstance().getBean("Genomic"); //$NON-NLS-1$
		final Genes genes = GenomeFactory.makeZygote(genomic);
		final Phenotype phenotype = PhenotypeFactory.makeDawkinsian();
		genomic.express(phenotype, genes);
		final Collection<String> keys = phenotype.getKeys();
		assertEquals("keys", 2, keys.size());
		for (final String key : keys) {
			final Trait trait = phenotype.getTrait(key);
			assertEquals("phenotype", phenotype, trait.getPhenotype());
			assertEquals("index/" + key, (key.equals("sex") ? "F" : "Typica"), trait.getVariantKey());
			final Variant variant = trait.getVariant();
			assertEquals("variant/" + key, (key.equals("sex") ? "F" : "Typica"), variant.getIdentifier());
			final Object value = trait.getValue();
			assertEquals("value/" + key, (key.equals("sex") ? Boolean.TRUE : "Typica"), value);
		}
	}
}
