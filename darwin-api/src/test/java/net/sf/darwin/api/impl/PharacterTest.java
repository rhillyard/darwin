/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TraitFactoryTest.java
 * Created on May 31, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.darwin.api.base.Pharacter_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Variant;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class PharacterTest {

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPharacter1() {
		final Variant variantPi = new Variant_Basic(PI, new Double(Math.PI));
		character.addVariant(variantPi);
		final Collection<Variant> list = character.getVariants();
		assertEquals("variants", 1, list.size());
		try {
			character.addVariant(null);
			fail();
		} catch (final DarwinException e) {
			final String message = e.getLocalizedMessage();
			assertEquals("exception", "addVariant(): null variant", message);
		}
	}

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPharacter2() {
		final Variant variantPi = new Variant_Basic(PI, new Double(Math.PI));
		final Variant variantE = new Variant_Basic(E, new Double(Math.E));
		character.addVariant(variantPi);
		character.addVariant(variantE);
		final Collection<Variant> list = character.getVariants();
		final String key_pi = character.addVariant(variantPi);
		assertEquals("index", PI, key_pi);
		assertEquals("variants", 2, list.size());
		assertEquals("id", "test", character.getIdentifier());
		for (final Variant variant : list) {
			final String string = variant.toString();
			final String identifier = variant.getIdentifier();
			final Object value = variant.getValue();
			assertEquals("audit", "Attribute " + identifier + ": " + value + "", string);
		}
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPharacter3() throws Exception {
		final Variant variantPi = new Variant_Basic(PI, new Double(Math.PI));
		final Variant variantE = new Variant_Basic(E, new Double(Math.E));
		final List<Variant> variants = new ArrayList<Variant>();
		variants.add(variantPi);
		variants.add(variantE);
		assertNotNull(character);
		((Pharacter_) character).setVariants(variants);
		final Collection<Variant> list = character.getVariants();
		final String keyPi = character.addVariant(variantPi);
		assertEquals("index", PI, keyPi);
		assertEquals("variants", 2, list.size());
		assertEquals("id", "test", character.getIdentifier());
		for (final Variant variant : list) {
			final String audit = variant.toString();
			final String identifier = variant.getIdentifier();
			final Object value = variant.getValue();
			assertEquals("audit", "Attribute " + identifier + ": " + value + "", audit);
		}
	}

	/**
	 * 
	 */
	private static final String E = "e"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String PI = "pi"; //$NON-NLS-1$

	private static Pharacter_Fisherian character;

	/**
	 */
	@SuppressWarnings("nls")
	@BeforeClass
	public static void setup() {
		character = new Pharacter_Fisherian("test");
	}

	/**
	 * 
	 */
	public static void tearDown() {
		character.setVariants(new ArrayList<Variant>());
	}

}
