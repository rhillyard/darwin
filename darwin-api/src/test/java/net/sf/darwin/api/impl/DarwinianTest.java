/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: DarwinianTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.85 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.base.MateChoice_Wrightian;
import net.sf.darwin.api.base.MockCensus;
import net.sf.darwin.api.base.MockEnum_WC;
import net.sf.darwin.api.base.MockFitnessEngine;
import net.sf.darwin.api.base.MockRegistry;
import net.sf.darwin.api.base.NumericalTrait;
import net.sf.darwin.api.base.Taxon_;
import net.sf.darwin.api.base.Taxon_Darwinian;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.EnvironmentListener;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.VisualizableListener;
import net.sf.darwin.ui.impl.VisualizableListener_Census;

import org.junit.After;
import org.junit.Test;

import com.rubecula.beanpot.BeanPot;
import com.rubecula.beanpot.StringWriterWriter;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "boxing", "static-method" })
public class DarwinianTest extends TestBase {

	@After
	public void afterTest() {
		BeanPot.cleanup();
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testDarwinian1() throws Exception {
		final String systemId = "Test System";
		final String sTest = "Test";
		final double sootValue = 5;
		final int populationSeed = 10;
		final int idealPopulation = 100;
		final int generations = 2;
		final double infantMortality = Mortality_Reaper.INFANT_MORTALITY_DEFAULT;
		final int $MaxSootDensity = 10;
		final String testGenomicIdentifier = "Test Genomic_Sexual";

		// If you want to be able to log the random number generator, uncomment
		// the following:
		// setRandom(new Random_Standard(new RandomJava()));

		// The "value" of a color is defined as its position in the
		// ApplicationDefinitions.$Colors array.
		final NumericalTrait $WingColorValuer = new NumericalTrait() {
			@Override
			public double getTraitValue(final String trait) {
				return LookupIndex(trait, ApplicationDefinitions.$Colors);
			}
		};

		final Realm realm = new Realm_Wallacian();

		// If you want to turn off caching, uncomment the following:
		// realm.getFitnessCache().setActive(false);
		// realm.getPhenotypeCache().setActive(false);

		final StringWriterWriter printWriter = new StringWriterWriter();

		// An implementation of {@link Census} specifically for the peppered
		// moth.
		final Census censusTaker = new MockCensus(printWriter);

		// A populationListener which knows how to take a census and send it to
		// printWriter
		final VisualizableListener visualizableListener = new VisualizableListener_Census(censusTaker);

		final EnvironmentListener<Number> environmentListener = new EnvironmentListener<Number>() {

			@Override
			public void onEnvironmentChange(final Environment<Number> env) {
				System.out.println("Environment Changed: ");
				for (final String key : env.getEcoSystem().factorKeys()) {
					System.out.println("  key: " + key + " value: " + env.getEcoSystem().getFactor(key));
				}
			}
		};

		// The {@link FitnessEngine} implementation for the PepperedMoth
		// evolution.
		final FitnessEngine fitter = new MockFitnessEngine();

		// A default implementer of the Meiosis class with our random number
		// source.
		final Meiosis meiosis = new Meiosis_Locus(getRandom());

		// Create the genomic for this system.
		final Genomic genomic = new Genomic_Sexual(testGenomicIdentifier, meiosis, getRandom());
		final Chromosome chromosome = new Chromosome_NoSex("A", genomic.getAlphabet());
		genomic.addChromosome(chromosome);

		// Create a locus using our random number source
		final Locus_Triversian locus0 = new Locus_Triversian("unnamed locus", getRandom());
		chromosome.addLocus(locus0);
		final Allele nonMelanism = AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, false);
		locus0.add(nonMelanism, 4);
		locus0.addAllele(AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true));

		// Create the phenome for this system, adding one trait: wing color.
		// XXX consider using Phenome_Gouldian
		final Phenome phenome = new Phenome_Numerical(fitter, $WingColorValuer);
		final Pharacter_Fisherian wingColor = new Pharacter_Fisherian(MockEnum_WC.ID);
		final MockVariant typica = new MockVariant(MockEnum_WC.Typica);
		wingColor.addVariant(typica);
		wingColor.addVariant(new MockVariant(MockEnum_WC.Carbonaria));
		phenome.addCharacter(wingColor);

		// The expression implementation for the peppered moth
		final Expresser expresser = new Expresser_Mendelian(wingColor, MockAllele.MELANISM, MockEnum_WC.Typica.toString());
		genomic.putExpresser(locus0, expresser);

		// Create three eco-factors: one for population, the others for
		// "sootiness"
		// TODO check following - it appears to do integer division. Is that OK?
		final EcoFactor<Number> sootFactorCtl = new MockEcoFactor($MaxSootDensity / 2);
		final EcoFactor<Number> sootFactorExp = new MockEcoFactor(sootValue);

		// Create an environment for the control population to exist in
		final Environment<Number> environmentControl = new MockEnvironment("Test: Control", realm);
		environmentControl.addFactor(sootFactorCtl);
		environmentControl.setIdealPopulation(idealPopulation);

		// Create an environment for the experimental population to exist in
		final Environment environmentExperimental = new MockEnvironment("Test: Exp", realm);
		environmentExperimental.addFactor(sootFactorExp);
		environmentExperimental.setIdealPopulation(idealPopulation);

		// Create the Taxon to test
		final Taxon_ system = new Taxon_Darwinian(systemId, realm, genomic, phenome, censusTaker, infantMortality,
				new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated(), getRandom());

		system.setSeedPopulation(populationSeed);

		// Create the population and add it to the Taxon
		// system.
		final Population population = new Population_Malthusian(sTest);
		system.addPopulation(population); // void result
		final Colony colonyControl = new Colony_Mayrian(population.getIdentifier() + "C", environmentControl, getRandom());
		final Colony colonyExp = new Colony_Mayrian(population.getIdentifier() + "X", environmentExperimental, getRandom());
		population.addColony(colonyControl);
		population.addColony(colonyExp);
		system.addVisualizableListener(visualizableListener);
		final PrintWriter consoleWriter = new PrintWriter(System.out);
		(system).setRegistry(new MockRegistry(consoleWriter));
		environmentExperimental.addListener(environmentListener);

		getRandom().setSeed(0L);

		// seed each population with a number of organisms
		system.seedMembers();

		assertEquals("identifier", systemId, system.getIdentifier());
		final int populations = system.getPopulations().size();

		final Organism organismF = Organism_Sexual.seed(colonyExp);
		final Organism organismM = Organism_Sexual.seed(colonyExp);

		getRandom().setSeed(0L);
		// this is the old value which worked before changing random source
		// double expectedDesirability = 1.5249805417186981;
		// final double expectedDesirability = 0.770539;
		// final double expectedDesirability = 1.7224324360530479;
		// final double expectedDesirability = 0.7342890810031594;
		final double expectedDesirability = 1.3773885361019635;

		assertEquals("chooser", expectedDesirability, system.getChooser().getDesirabilityIndex(organismF, organismM), 0.0001);
		// TODO this isn't much of a test - fix it!
		final Fecundity_Saturated fecundity = (Fecundity_Saturated) system.getFecundity();
		assertEquals("fecundity", 1, fecundity.getFecundity(1));
		assertEquals("genomic", genomic, system.getGenomic());

		assertEquals("mortality", infantMortality, system.getMortality().calculateMortality(0, 1, 0), 0.000001);

		assertEquals("phenome", phenome, system.getPhenome());
		assertEquals("populations", 1, populations);
		assertEquals("colonies", 2, population.getCount());
		assertEquals("exp population", sTest, system.getPopulation(populations - 1).getIdentifier());

		burn(3);
		sootFactorExp.setValue(7.0);

		for (int generation = 0; generation < generations; generation++) {
			printWriter.println("New Generation: " + generation);
			final boolean healthy = system.nextGeneration();
			assertEquals("generation", generation + 1, system.getGeneration());
			printWriter.flush();
			assertTrue("healthy after generation " + generation, healthy);
		}

		setRandom(null);
		consoleWriter.flush();
		printWriter.reset();
		system.doCensus();
		// final Serializer serializer =
		// SerializerFactory.createSerializer("seriazlizer.out");
		// serializer.writeObject(system);
		final String buffer = printWriter.reset();
		final String expected = "[system] Population: Test and population: 2"
				+ NL
				+ "MockCensus for Colony: TestC with soot value: 5.0"
				+ NL
				+ "Generation: 2 in context: system"
				+ NL
				+ "Wing color frequencies: Typica: 20, Carbonaria: 21, "
				+ NL
				+ "41 organisms with mean color: 0.5048780487804878 and mean allele value: 0.25609756097560976 and mean age: 0.8780487804878049"
				+ NL
				+ ""
				+ NL
				+ "MockCensus for Colony: TestX with soot value: "
				+ sootFactorExp.getValue()
				+ ""
				+ NL
				+ "Generation: 2 in context: system"
				+ NL
				+ "Wing color frequencies: Typica: 12, Carbonaria: 27, "
				+ NL
				+ "39 organisms with mean color: 0.5769230769230768 and mean allele value: 0.19230769230769232 and mean age: 0.7948717948717948"
				+ NL + "" + NL + "";
		assertEquals("census", expected, buffer);

		assertEquals("control population", 41, system.getPopulation(0).getColony(0).getCount());
		assertEquals("exprmnt population", 39, system.getPopulation(0).getColony(1).getCount());

		assertEquals("system string", "Taxon_Darwinian Test System", system.toString()); //$NON-NLS-1$//$NON-NLS-2$
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testDarwinian3() throws Exception {
		final String systemId = "Test System";
		final String sTest = "Test";
		final double sootValue = 5;
		final int populationSeed = 10;
		final int idealPopulation = 100;
		final double infantMortality = Mortality_Reaper.INFANT_MORTALITY_DEFAULT;
		final int $MaxSootDensity = 10;
		final String testGenomicIdentifier = "Test Genomic_Sexual";

		// The "value" of a color is defined as its position in the
		// ApplicationDefinitions.$Colors array.
		final NumericalTrait $WingColorValuer = new NumericalTrait() {
			@Override
			public double getTraitValue(final String trait) {
				return LookupIndex(trait, ApplicationDefinitions.$Colors);
			}
		};

		final Realm realm = new Realm_Wallacian();

		final StringWriterWriter printWriter = new StringWriterWriter();

		// An implementation of {@link Census} specifically for the peppered
		// moth.
		final Census censusTaker = new MockCensus(printWriter);

		// A populationListener which knows how to take a census and send it to
		// printWriter
		final VisualizableListener visualizableListener = new VisualizableListener_Census(censusTaker);

		// The {@link FitnessEngine} implementation for the PepperedMoth
		// evolution.
		final FitnessEngine fitter = new MockFitnessEngine();

		// A default implementer of the Meiosis class with our random number
		// source.
		final Meiosis meiosis = new Meiosis_Locus(getRandom());

		// Create the genomic for this system.
		final Genomic_Sexual genomic = new Genomic_Sexual(testGenomicIdentifier, meiosis, getRandom());
		final Chromosome chromosome = new Chromosome_NoSex("no sex", genomic.getAlphabet());
		genomic.addChromosome(chromosome);
		// Create a locus using our random number source
		final Locus_Triversian locus0 = new Locus_Triversian("unnamed locus", getRandom());
		chromosome.addLocus(locus0);
		final Allele nonMelanism = AlleleFactory.makeDominanceAllele(MockAllele.NON_MELANISM, false);
		// Make the non-melanistic allele slightly less probable
		locus0.add(nonMelanism, 3);
		locus0.addAllele(AlleleFactory.makeDominanceAllele(MockAllele.MELANISM, true));

		// Create the phenome for this system, adding one trait: wing color.
		// XXX consider using Phenome_Gouldian
		final Phenome phenome = new Phenome_Numerical(fitter, $WingColorValuer);
		final Pharacter_Fisherian wingColor = new Pharacter_Fisherian(MockEnum_WC.ID);
		final MockVariant typica = new MockVariant(MockEnum_WC.Typica);
		wingColor.addVariant(typica);
		wingColor.addVariant(new MockVariant(MockEnum_WC.Carbonaria));
		phenome.addCharacter(wingColor);

		// The expression implementation for the peppered moth
		final Expresser expresser = new Expresser_Mendelian(wingColor, MockAllele.NON_MELANISM, MockEnum_WC.Typica.toString());
		genomic.putExpresser(locus0, expresser);

		// Create three eco-factors: one for population, the others for
		// "sootiness"
		final EcoFactor sootFactorCtl = new MockEcoFactor($MaxSootDensity / 2);
		final EcoFactor sootFactorExp = new MockEcoFactor(sootValue);

		// Create an environment for the control population to exist in
		final Environment environmentControl = new MockEnvironment("Test: Control", realm);
		environmentControl.addFactor(sootFactorCtl);
		environmentControl.setIdealPopulation(idealPopulation);

		// Create an environment for the experimental population to exist in
		final Environment environmentExperimental = new MockEnvironment("Test: Exp", realm);
		environmentExperimental.addFactor(sootFactorExp);
		environmentExperimental.setIdealPopulation(idealPopulation);

		// Create the Taxon to test
		final Taxon system = new Taxon_Darwinian(systemId, realm, genomic, phenome, censusTaker, infantMortality,
				new MateChoice_Wrightian(getRandom()), new Fecundity_Saturated(), getRandom());
		((Taxon_) system).setSeedPopulation(populationSeed);

		// Create the control population and add it to the Taxon.
		final Population population = new Population_Malthusian(sTest);
		system.addPopulation(population); // void result

		final Colony colonyControl = new Colony_Mayrian(population.getIdentifier() + "C", environmentControl, getRandom());
		final Colony colonyExp = new Colony_Mayrian(population.getIdentifier() + "X", environmentExperimental, getRandom());
		population.addColony(colonyControl);
		population.addColony(colonyExp);

		final Collection<VisualizableListener> listeners = new ArrayList<VisualizableListener>();
		listeners.add(visualizableListener);
		system.setVisualizableListeners(listeners);

		burn(17);

		// seed each population with a number of organisms
		system.seedMembers();

		final String output = printWriter.reset();
		System.out.println("output: " + output);

		final String expected = "MockCensus for Colony: TestC with soot value: 5.0" + NL
				+ "Generation: 0 in context: seed members" + NL + "Wing color frequencies: Typica: 9, Carbonaria: 1, " + NL
				+ "10 organisms with mean color: 0.33999999999999997 and mean allele value: 0.4 and mean age: 0.0" + NL + NL
				+ "MockCensus for Colony: TestX with soot value: 5.0" + NL + "Generation: 0 in context: seed members" + NL
				+ "Wing color frequencies: Typica: 8, Carbonaria: 2, " + NL
				+ "10 organisms with mean color: 0.38 and mean allele value: 0.4 and mean age: 0.0" + NL + NL;

		assertEquals("output", expected, output);
	}

	/**
	 * Method to look up a String (key) from a list of Strings (keys).
	 * 
	 * @param key
	 *            the String to be looked up.
	 * @param keys
	 *            the array of Strings.
	 * @return the index of the first matching key (if exists), else -1 for no
	 *         match.
	 */
	static int LookupIndex(final String key, final String[] keys) {
		for (int i = 0; i < keys.length; i++)
			if (key.equalsIgnoreCase(keys[i]))
				return i;
		return -1;
	}
}
