/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: AlleleTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.core.Allele;
import net.sf.tostring0.AToString;
import net.sf.tostring0.Detail;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class AlleleTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Allele_Dominance#toString()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testAudit() {
		final Allele allele = AlleleFactory.makeDominanceAllele("xxx", true);
		assertEquals("string", "X", ((AToString) allele).toStringId());
		assertEquals("string", "Allele Xxx: true",
				((AToString) allele).toString(new Detail(true, true, true, false, false, null, false, 10, Integer.MAX_VALUE)));
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Allele_Dominance#toString()}.
	 */
	@SuppressWarnings("nls")
	@Test
	public void testToString() {
		final String string = ((AToString) AlleleFactory.makeDominanceAllele("xxx", true)).toStringId();
		assertEquals("string", "X", string);
	}

}
