/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MateChoiceTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.52 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Set;

import net.sf.darwin.api.base.Allele_;
import net.sf.darwin.api.base.Attraction_;
import net.sf.darwin.api.base.MateChoice_Wrightian;
import net.sf.darwin.api.base.MockMateChoice;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.TraitMap;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.Test;

import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.EvaluatorFactory;
import com.rubecula.jexpression.jep.Evaluator_JEP;

/**
 * @author Robin Hillyard
 * 
 */
public class MateChoiceTest extends TestBase {

	@Override
	public void setUp() {
		super.setUp();
		getRandom().setSeed(0);
	}

	@Override
	public void tearDown() {
		super.tearDown();
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.MateChoice#getDesirabilityIndex(Organism, Organism)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetDesirabilityIndex() throws Exception {
		// final Expresser expresser = new Expresser_Mendelian(new
		// Allele_Dominance(0), new Allele_Dominance(1));
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		final Colony colony = population.getColony(0);
		getRandom().setSeed(5L);
		final Organism_Sexual female = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("female", female.isFemale());
		final Organism_Sexual male = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("male", !male.isFemale());
		getRandom().setSeed(0);
		final MateChoice choice = new MateChoice_Wrightian(getRandom());
		assertEquals("desirability", 1.3773885361, choice.getDesirabilityIndex(female, male), 0.00001);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.MateChoice#getDesirabilityIndex(Organism, Organism)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetDesirabilityIndex2() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		final Colony colony = population.getColony(0);
		getRandom().setSeed(5L);
		final Organism_Sexual female = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("female", female.isFemale());
		final Organism_Sexual male = (Organism_Sexual) Organism_Sexual.seed(colony);
		male.age();
		male.setViable(true);
		assertTrue("male", !male.isFemale());
		getRandom().setSeed(0);
		final MateChoice choice = new MateChoice_Wrightian(getRandom());
		// TEST this seems like an awfully high value for the desirability
		// let's check the formulae.
		assertEquals("desirability", 27.547770722, choice.getDesirabilityIndex(female, male), 0.00001);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.core.MateChoice#getDesirabilityIndex(Organism, Organism)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetDesirabilityIndexCustomizable() throws Exception {
		// final Expresser expresser = new Expresser_Mendelian(new
		// Allele_Dominance(0), new Allele_Dominance(1));
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		final Colony colony = population.getColony(0);
		getRandom().setSeed(5L);
		final Organism_Sexual female = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("female", female.isFemale());
		final Organism_Sexual male = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("male", !male.isFemale());
		final Evaluator evaluator = EvaluatorFactory.createEvaluator(Evaluator_JEP.class);
		final MateChoice_Evaluator choice = new MockMateChoice(getRandom(), evaluator, null);
		getRandom().setSeed(0);
		assertEquals("desirability1", 1.3773885361, choice.getDesirabilityIndex(female, male), 0.00001);
		choice.setExpression(0, "(viability * 2^(2*random-1))^2 * (age + 2)");
		choice.setSeed(0);
		assertEquals("desirability2", 3.79439835877, choice.getDesirabilityIndex(female, male), 0.00001);
		choice.setExpression(0, "viability * 2^(2*random-1) * (age + 1)");
		choice.setSeed(0);
		assertEquals("desirability3", 1.3773885361, choice.getDesirabilityIndex(female, male), 0.00001);
	}

	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testGetDesirabilitySexSelection() throws Exception {
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		final Pharacter character = phenome.getCharacter("test character");
		character.addVariant(new Variant_Basic("other"));
		final Set<String> variantKeys = character.getVariantKeys();
		final String[] variants = variantKeys.toArray(new String[0]);
		final Chromosome chromosomeNoSex = genomic.getChromosome(0);
		final Locus locus = chromosomeNoSex.getLocus(0);
		locus.addAllele(new TestAllele("other", Math.PI));
		final Collection<Allele> alleleValues = locus.getAlleleValues();
		final Allele[] alleles = alleleValues.toArray(new Allele[0]);
		final Expresser_Mendelian expresser = new Expresser_Mendelian(character, alleles[0].getIdentifier(), variants[0]);
		// Note that we only need to add the expresser in so that we do not see
		// warnings that there is no expresser for the (non-sex) locus.
		genomic.putExpresser(locus, expresser);
		final Colony colony = population.getColony(0);
		getRandom().setSeed(5L);
		final Organism_Sexual female = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("female", female.isFemale());
		final Organism_Sexual male = (Organism_Sexual) Organism_Sexual.seed(colony);
		assertTrue("male", !male.isFemale());
		final Evaluator evaluator = EvaluatorFactory.createEvaluator(Evaluator_JEP.class);
		final MateChoice_Evaluator choice = new MateChoice_TestType(getRandom(), evaluator, null);
		getRandom().setSeed(0);
		assertEquals("desirability1", 1.3773885361, choice.getDesirabilityIndex(female, male), 0.00001);
		choice.setExpression(0, "(viability * 2^(2*random-1))^2 * (age + 2)");
		choice.setSeed(0);
		assertEquals("desirability2", 3.79439835877, choice.getDesirabilityIndex(female, male), 0.00001);
		choice.setExpression(0, "viability * 2^(2*random-1) * (age + 1)");
		choice.setSeed(0);
		assertEquals("desirability3", 1.3773885361, choice.getDesirabilityIndex(female, male), 0.00001);

	}

	public static class Attraction_Test extends Attraction_ {

		@Override
		public double getAttraction(final TraitMap female, final TraitMap male) {
			return 1;
		}

		@Override
		public boolean isUniform() {
			return false;
		}

	}

	public static class MateChoice_TestType extends MateChoice_Evaluator {

		/**
		 * @param random
		 * @param desirability
		 * @param undesirability
		 * @param attraction
		 * @param evaluatorDesirable
		 * @param evaluatorChoosy
		 */
		public MateChoice_TestType(final RandomGenerator random, final Evaluator evaluatorDesirable,
				final Evaluator evaluatorChoosy) {
			super(random, new Function_Desirable(evaluatorDesirable), new Function_Choosy(evaluatorChoosy), new Attraction_Test());
		}

	}

	static class TestAllele<G> extends Allele_<G> {

		/**
		 * @param key
		 * @param value
		 */
		protected TestAllele(final String key, final G value) {
			super(key, value);
		}

		/**
		 * @see net.sf.darwin.core.Basic#getBases()
		 */
		@Override
		public String getBases() {
			return getValue().toString();
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -2975086316946511290L;

	}
}
