/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MeiosisTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.22 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Meiosis;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class MeiosisTest extends TestBase {

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMeiosisChromosomeCrossover() throws Exception {
		// Create a meiosis implementer where crossover always occurs for each
		// chromosome
		final Meiosis meiosis = new Meiosis_Chromosome(getRandom(), 1);
		final Mutator_Null mutator_Null = new Mutator_Null();
		final Genomic genomic = new Genomic_Diploid("test genomic", meiosis, mutator_Null);
		final Chromosome chromosome = new Chromosome_NoSex("test chromosome", genomic.getAlphabet());
		// XXX do we need Randomizer here? Don't think so!
		final Locus locus1 = new Locus_Triversian("locus 1", new Randomizer(getRandom()));
		// Don't be tempted to replace these constructor calls with references
		// to constant
		locus1.addAllele(AlleleFactory.makeDominanceAllele("x", true));
		locus1.addAllele(AlleleFactory.makeDominanceAllele("x", false));
		chromosome.addLocus(locus1);
		final Locus locus2 = new Locus_Triversian("locus 2", new Randomizer(getRandom()));
		locus2.addAllele(AlleleFactory.makeDominanceAllele("x", true));
		locus2.addAllele(AlleleFactory.makeDominanceAllele("x", false));
		chromosome.addLocus(locus2);
		genomic.addChromosome(chromosome);
		getRandom().setSeed(5);
		final Genome genome = GenomeFactory.makeZygote(genomic);
		System.out.println(genome);
		final Genome gamete1 = genomic.createGamete(genome);
		final int size = gamete1.size();
		assertEquals("genes", 2, size);
		assertEquals("allele10", "x", gamete1.getGene(0).getAlleleKey(0));
		assertEquals("allele11", "x", gamete1.getGene(1).getAlleleKey(0));
		final Genome gamete2 = genomic.createGamete(genome);
		assertEquals("allele20", "X", gamete2.getGene(0).getAlleleKey(0));
		assertEquals("allele21", "X", gamete2.getGene(1).getAlleleKey(0));
		final Genome gamete3 = genomic.createGamete(genome);
		assertEquals("allele30", "X", gamete3.getGene(0).getAlleleKey(0));
		assertEquals("allele31", "X", gamete3.getGene(1).getAlleleKey(0));
		final Genome gamete4 = genomic.createGamete(genome);
		assertEquals("allele40", "X", gamete4.getGene(0).getAlleleKey(0));
		assertEquals("allele41", "X", gamete4.getGene(1).getAlleleKey(0));
		final Genome gamete5 = genomic.createGamete(genome);
		assertEquals("allele50", "X", gamete5.getGene(0).getAlleleKey(0));
		assertEquals("allele51", "X", gamete5.getGene(1).getAlleleKey(0));
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMeiosisChromosomeNoCrossover() throws Exception {
		final Locus locus = new Locus_Triversian("unnamed locus", new Randomizer(getRandom()));
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", true)); // keep
		// this
		// a
		// constructor
		// call
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", false)); // keep
		// this
		// a
		// constructor
		// call
		final Chromosome chromosome = new Chromosome_NoSex("test chromosome", locus.getAlphabet());
		chromosome.addLocus(locus);
		final Meiosis meiosis = new Meiosis_Chromosome(getRandom());
		final Mutator_Null mutator_Null = new Mutator_Null();
		final Genomic genomic = new Genomic_Diploid("test genomic", meiosis, mutator_Null);
		genomic.addChromosome(chromosome);
		getRandom().setSeed(1);
		final Genome genome = GenomeFactory.makeZygote(genomic);
		final Genome gamete = genomic.createGamete(genome);
		final int size = gamete.size();
		assertEquals("genes", 1, size);
		assertEquals("allele1", "x", gamete.getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
	}

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMeiosisLocus1() {
		// XXX do we need Randomizer here? Don't think so!
		final Locus<Boolean> locus = new Locus_Triversian<>("unnamed locus", new Randomizer(getRandom()));
		locus.addAllele(AlleleFactory.makeDominanceAllele("0", true));
		locus.addAllele(AlleleFactory.makeDominanceAllele("1", false));
		final Gene<Boolean> gene = GeneFactory.makeDiploid(locus, "0", "1");
		final Meiosis<Boolean> meiosis = new Meiosis_Locus<>(getRandom());
		assertEquals("allele", "1", meiosis.doMeiosis(gene, -1));
		assertEquals("allele", "1", meiosis.doMeiosis(gene, -1));
		assertEquals("allele", "0", meiosis.doMeiosis(gene, -1));
		assertEquals("allele", "1", meiosis.doMeiosis(gene, -1));
		assertEquals("allele", "1", meiosis.doMeiosis(gene, -1));
		assertEquals("allele", "0", meiosis.doMeiosis(gene, -1));
		assertEquals("allele", "1", meiosis.doMeiosis(gene, -1));
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMeiosisLocus2() throws Exception {
		final Meiosis<Boolean> meiosis = new Meiosis_Locus<>(getRandom());
		final Mutator_Null<Boolean> mutator_Null = new Mutator_Null<>();
		final Genomic<Boolean, Boolean> genomic = new Genomic_Diploid<>("test genomic", meiosis, mutator_Null);
		// XXX do we need Randomizer here? Don't think so!
		final Locus<Boolean> locus = new Locus_Triversian<>("unnamed locus", new Randomizer(getRandom()));
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", true)); // keep
		// as
		// constructor
		locus.addAllele(AlleleFactory.makeDominanceAllele("x", false)); // keep
		// as
		// constructor
		final Chromosome<Boolean> chromosome = new Chromosome_NoSex<>("test chromosome", genomic.getAlphabet());
		chromosome.addLocus(locus);
		genomic.addChromosome(chromosome);
		getRandom().setSeed(1);
		final Genome<Boolean, Boolean> genome = GenomeFactory.makeZygote(genomic);
		final Genome<Boolean, Boolean> gamete = genomic.createGamete(genome);
		final int size = gamete.size();
		assertEquals("genes", 1, size);
		assertEquals("allele1", "x", gamete.getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "x", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
		assertEquals("allele2", "X", genomic.createGamete(genome).getGene(0).getAlleleKey(0));
	}
}
