/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TraitFactoryTest.java
 * Created on May 31, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class TraitFactoryTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.factory.TraitFactory#makeDiscrete(net.sf.darwin.core.Pharacter, String)}
	 * .
	 */
	@SuppressWarnings("nls")
	@Test
	public void testMakeDiscrete() {
		final Trait traitPi = TraitFactory.makeDiscrete(character, "pi");
		assertEquals("variant0", "pi", traitPi.getVariantKey());
		final Variant v0 = traitPi.getCharacter().getVariant(traitPi.getVariantKey());
		assertEquals("variant0", variant0, v0);
		assertEquals("valuePi", Double.valueOf(Math.PI), v0.getValue());
		final Trait traitE = TraitFactory.makeDiscrete(character, "e");
		assertEquals("variant1", "e", traitE.getVariantKey());
		final Variant v1 = traitE.getCharacter().getVariant(traitE.getVariantKey());
		assertEquals("variant1", variant1, v1);
		assertEquals("valueE", Double.valueOf(Math.E), v1.getValue());
	}

	/**
	 * 
	 */
	@SuppressWarnings({ "boxing", "nls" })
	@Test
	public void testMakeVariable() {
		// Normally a variable trait references a character with no variants,
		// but this will do OK for the test.
		final Trait trait = TraitFactory.makeVariable(character, 1);
		final Double value = (Double) trait.getValue();
		assertEquals("value", 1.0, value, 0.0001);
		System.out.println(trait);
	}

	private static Pharacter character;

	private static Variant variant0;

	private static Variant variant1;

	/**
	 */
	@SuppressWarnings("nls")
	@BeforeClass
	public static void setup() {
		character = new Pharacter_Fisherian("test character");
		variant0 = new Variant_Basic("pi", new Double(Math.PI));
		character.addVariant(variant0);
		variant1 = new Variant_Basic("e", new Double(Math.E));
		character.addVariant(variant1);
	}

}
