/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenomeTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Variant;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class PhenomeTest extends TestBase {

	/**
	 */
	@SuppressWarnings("nls")
	@Test
	public void testPhenome() {
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final FitnessEngine function = phenome.getFitnessEngine();
		assertEquals("fitness", fitnessFunction, function);
		final Variant variant = new Variant_Basic("test trait", new Integer(1));
		final Pharacter character = new Pharacter_Fisherian("test");
		character.addVariant(variant);
		phenome.addCharacter(character);
		assertEquals("traits", 1, phenome.size());
		assertEquals("test trait", character, phenome.getCharacter("test"));
	}

}
