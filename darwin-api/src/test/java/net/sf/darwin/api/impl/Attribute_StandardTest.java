/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Attribute_StandardTest.java
 * Created on Feb 24, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import net.sf.darwin.core.Attribute;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class Attribute_StandardTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Attribute_Standard#Attribute_Standard(java.lang.Object, java.lang.Object)}
	 * .
	 */
	@Test
	public void testAttribute_Standard() {
		final String key = "key"; //$NON-NLS-1$
		final String OTHER = "OTHER"; //$NON-NLS-1$
		final String VALUE = "value"; //$NON-NLS-1$
		final Attribute attribute = new Attribute_Standard(key, VALUE);
		assertEquals(key, attribute.getAttribute());
		assertEquals(VALUE, attribute.getValue());
		attribute.setValue(OTHER);
		assertEquals(OTHER, attribute.getValue());
		assertEquals("Attribute " + key + ": " + OTHER + "", attribute.toString()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

}
