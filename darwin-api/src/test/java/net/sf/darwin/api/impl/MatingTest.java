/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MatingTest.java
 * Created on Jan 30, 2007
 * @version $Revision: 1.42 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.sf.darwin.api.util.TestBase;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;

import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class MatingTest extends TestBase {

	/**
	 * @see net.sf.darwin.api.util.TestBase#setUp()
	 */
	@Override
	public void setUp() {
		super.setUp();
		getRandom().setSeed(0);
	}

	/**
	 * @see net.sf.darwin.api.util.TestBase#tearDown()
	 */
	@Override
	public void tearDown() {
		super.tearDown();
	}

	/**
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls" })
	@Test
	public void testMatingOrganismOrganism() throws Exception {
		// final Expresser expresser = new Expresser_Mendelian(new
		// Allele_Dominance(0), new Allele_Dominance(1));
		final Genomic genomic = new Genomic_Sexual("test genomic", new Meiosis_Locus(getRandom()), getRandom());
		// final Genomic genomic = new Genomic_Sexual("test genomic",new
		// Expresser_Mendelian(), new Factory_Standard());
		final FitnessEngine fitnessFunction = new FitnessEngine_Easy();
		final Realm realm = new Realm_Wallacian();
		final Environment environment = new Environment_Muirian(realm);
		final Phenome phenome = new Phenome_Gouldian(fitnessFunction);
		final Population population = createPopulation(genomic, environment, phenome, false);
		// TODO work to do re: Colony
		final Colony colony = population.getColony(0);
		getRandom().setSeed(10L);
		final Organism father = Organism_Sexual.seed(colony);
		final Organism mother = Organism_Sexual.seed(colony);
		// TEST changed the constructor here - it should still work OK
		final Mating mating = new Mating_Haldanian(father.getNuclear(), mother.getNuclear(), getRandom(), genomic);
		// 0 seed produces male as first born
		final Nuclear_Zygote baby = (Nuclear_Zygote) mating.progeny();
		assertTrue("1st born is female", baby.isFemale());
		assertFalse("2nd born is male", ((Nuclear_Zygote) mating.progeny()).isFemale());
		// assertEquals("age",0,baby.getAge());
		assertEquals("genome", 2, baby.getGenome().size());
		// assertEquals("phenotype",1,baby.getPhenotype().size());
		assertEquals("population", colony.getIdentifier(), baby.getColonyId());
		mating.progeny();
		mating.progeny();
		assertTrue("last-born is female", ((Nuclear_Zygote) mating.progeny()).isFemale());
	}

}
