/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Function_ChoosyTest.java
 * Created on Sep 24, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;

import org.apache.commons.math.random.RandomGenerator;
import org.junit.Test;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.matheval.Evaluator_MathEval;
import com.rubecula.jexpression.rpn.Evaluator_RPN_Mutable;

/**
 * @author Robin Hillyard
 * 
 */
public class Function_ChoosyTest {

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Function_Choosy#getMinimumDesirability(double)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("nls")
	@Test
	public void testGetMinimumDesirabilityMathEval() throws Exception {
		final Function_Choosy choosy = new Function_Choosy(new Evaluator_MathEval());
		final RandomGenerator random = new Random_Standard(0);
		final double minimumDesirability = choosy.getMinimumDesirability(random.nextDouble());
		assertEquals("minimumDesirability", 16.0816294413029, minimumDesirability, 0.000001);
	}

	/**
	 * Test method for
	 * {@link net.sf.darwin.api.impl.Function_Choosy#getMinimumDesirability(double)}
	 * .
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "nls", "boxing" })
	@Test
	public void testGetMinimumDesirabilityRPN() throws Exception {
		final Evaluator evaluator = new Evaluator_RPN_Mutable();
		final Function_Choosy choosy = new Function_Choosy(evaluator);
		final RandomGenerator rng = new Random_Standard(0);
		final double r = rng.nextDouble();
		assertEquals("minimumDesirability", 16.0816294413029, choosy.getMinimumDesirability(r), 0.0001);
		evaluator.addSymbol("random", r);
		final CharSequence[] tokens = new CharSequence[] { Concept.e, "$random", "3.8", "*", Concept.pow };
		choosy.setExpression(tokens);
		assertEquals("minimumDesirability", 16.0816294413029, choosy.getMinimumDesirability(r), 0.0001);
	}
}
