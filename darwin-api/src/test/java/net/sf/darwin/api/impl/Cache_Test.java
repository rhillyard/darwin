/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Cache_Test.java
 * Created on Oct 2, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import net.sf.darwin.api.base.Cache_;
import net.sf.tostring0.Identifiable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class Cache_Test {

	/**
	 */
	@Before
	public void setUp() {
		this._cache = new TestCache();
	}

	/**
	 */
	@After
	public void tearDown() {
		this._cache.flush();
		this._cache = null;
	}

	/**
	 */
	@SuppressWarnings({ "boxing", "nls" })
	@Test
	public void testAddGet() {
		assertEquals("count0", 0, this._cache.count());
		this._cache.add(FULHAM, 4, 100.0);
		assertEquals("count1", 1, this._cache.count());
		final Double fulham = this._cache.get(FULHAM, 4);
		assertNotNull(FULHAM + " null", fulham);
		assertEquals(FULHAM + " value", 100.0, fulham, 0.1);
		assertNull(this._cache.get(MAN_UTD, 0));
		this._cache.add(MAN_UTD, 0, 99.0);
		assertEquals("count2", 2, this._cache.count());
		assertEquals(MAN_UTD + " value", 99.0, this._cache.get(MAN_UTD, 0), 0.1);
		final Double fulham2 = this._cache.get(new String(FULHAM), 4);
		assertEquals(FULHAM + " value", 100.0, fulham2, 0.1);
		// here we construct a new String...
		this._cache.flush(new String(FULHAM), 4);
		assertEquals("count1", 1, this._cache.count());
	}

	/**
	 * 
	 */
	private static final String MAN_UTD = "Man Utd."; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String FULHAM = "Fulham"; //$NON-NLS-1$

	private TestCache _cache;

	static class TestCache extends Cache_<TestCacheKey, Double> {

		/**
		 * 
		 */
		public TestCache() {
			super();
		}

		public void add(final String string, final Integer integer, final Double value) {
			add(new TestCacheKey(string, integer), value);
		}

		public boolean cached(final String string, final Integer integer) {
			return cached(new TestCacheKey(string, integer));
		}

		public void flush(final String string, final Integer integer) {
			flush(new TestCacheKey(string, integer));
		}

		public Double get(final String string, final Integer integer) {
			return get(new TestCacheKey(string, integer));
		}

		/**
		 * @return "test cache".
		 * @see net.sf.tostring0.Identifiable#getIdentifier()
		 */
		@Override
		public String getIdentifier() {
			return "test cache"; //$NON-NLS-1$
		}

	}

	static class TestCacheKey implements Identifiable {

		/**
		 * @param string
		 *            XXX
		 * @param integer
		 *            XXX
		 * 
		 */
		public TestCacheKey(final String string, final Integer integer) {
			super();
			this._string = string;
			this._integer = integer;
		}

		/**
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final TestCacheKey other = (TestCacheKey) obj;
			if (this._integer == null) {
				if (other._integer != null)
					return false;
			} else if (!this._integer.equals(other._integer))
				return false;
			if (this._string == null) {
				if (other._string != null)
					return false;
			} else if (!this._string.equals(other._string))
				return false;
			return true;
		}

		/**
		 * @see net.sf.tostring0.Identifiable#getIdentifier()
		 */
		@Override
		@SuppressWarnings("nls")
		public String getIdentifier() {
			return this._string + ": " + this._integer;
		}

		/**
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this._integer == null) ? 0 : this._integer.hashCode());
			result = prime * result + ((this._string == null) ? 0 : this._string.hashCode());
			return result;
		}

		private final String _string;

		private final Integer _integer;

	}
}
