/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: RandomIterableTest.java
 * Created on Dec 4, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.sf.darwin.api.impl.RandomIterable_Standard;
import net.sf.darwin.api.impl.Random_Standard;

import org.apache.commons.collections.iterators.ObjectArrayIterator;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.math.random.RandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Robin Hillyard
 * 
 */
public class RandomIterableTest {

	/**
	 */
	@Before
	public void setUp() {
		this.random = new Random_Standard(0);
	}

	/**
	 */
	@After
	public void tearDown() {
		this.random = null;
	}

	/**
	 * Check that the static value alphabet is as expected.
	 */
	@SuppressWarnings("nls")
	@Test
	public void test0() {
		assertEquals("size", 26, alphabet.size());
		assertEquals(J, J, alphabet.get(9).toString());
	}

	/**
	 * Check that the order is as expected, at least up through first 6 letters.
	 */
	@SuppressWarnings("nls")
	@Test
	public void test1() {
		final RandomIterable_Standard<Character> iterable = new RandomIterable_Standard<Character>(alphabet, this.random);
		assertFalse("sample", iterable.isSample());
		final Iterator<Character> iterator = iterable.iterator();
		assertTrue("non-empty", iterator.hasNext());
		assertEquals("first", "T", iterator.next().toString());
		assertEquals("second", "U", iterator.next().toString());
		assertEquals("third", "Y", iterator.next().toString());
		assertEquals("fourth", "H", iterator.next().toString());
		assertEquals("fifth", "E", iterator.next().toString());
		assertEquals("sixth", "N", iterator.next().toString());
	}

	/**
	 * Check that the order is as expected, at least up through first 6 letters.
	 */
	@SuppressWarnings("nls")
	@Test
	public void test2() {
		final RandomIterable_Standard<Character> iterable = new RandomIterable_Standard<Character>(alphabet, this.random, 6);
		assertTrue("sample", iterable.isSample());
		final Iterator<Character> iterator = iterable.iterator();
		assertTrue("non-empty", iterator.hasNext());
		assertEquals("first", "T", iterator.next().toString());
		assertEquals("second", "U", iterator.next().toString());
		assertEquals("third", "Y", iterator.next().toString());
		assertEquals("fourth", "H", iterator.next().toString());
		assertEquals("fifth", "E", iterator.next().toString());
		assertEquals("sixth", "N", iterator.next().toString());
		assertFalse("exhausted", iterator.hasNext());
	}

	/**
	 * Check that we visit every letter in the alphabet exactly once.
	 */
	@SuppressWarnings("nls")
	@Test
	public void test3() {
		final Collection<Character> checkList = new ArrayList<Character>(alphabet);
		final RandomIterable_Standard<Character> iterable = new RandomIterable_Standard<Character>(alphabet, this.random);
		assertFalse("sample", iterable.isSample());
		final Iterator<Character> iterator = iterable.iterator();
		while (iterator.hasNext())
			assertTrue(checkList.remove(iterator.next()));
		assertEquals("empty", 0, checkList.size());
	}

	/**
	 * Check that we visit every letter in the alphabet exactly once.
	 */
	@SuppressWarnings("nls")
	@Test
	public void test4() {
		final Collection<Character> checkList = new ArrayList<Character>(alphabet);
		final RandomIterable_Standard<Character> iterable = new RandomIterable_Standard<Character>(alphabet, this.random, 6);
		assertTrue("sample", iterable.isSample());
		final Iterable<Character> remainder = iterable.getRemainder();
		final Iterator<Character> iterator = iterable.iterator();
		while (iterator.hasNext())
			assertTrue(checkList.remove(iterator.next()));
		final Iterator<Character> iterator2 = remainder.iterator();
		while (iterator2.hasNext())
			assertTrue(checkList.remove(iterator2.next()));
		assertEquals("empty", 0, checkList.size());
	}

	/**
	 * 
	 */
	private static final String J = "J"; //$NON-NLS-1$

	private RandomGenerator random;

	private static List<Character> alphabet;

	/**
	 * 
	 */
	@BeforeClass
	public static void BeforeClass() {
		final ObjectArrayIterator arrayIterator = new ObjectArrayIterator(
				ArrayUtils.toObject("ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray())); //$NON-NLS-1$
		alphabet = new ArrayList<Character>();
		while (arrayIterator.hasNext())
			alphabet.add((Character) arrayIterator.next());
	}
}
