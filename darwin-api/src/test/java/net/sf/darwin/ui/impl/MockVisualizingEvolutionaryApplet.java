/**
 * Cryptobase Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Module: MockApplet.java
 * Created on Feb 4, 2011
 */

package net.sf.darwin.ui.impl;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import net.sf.darwin.core.Version;
import net.sf.darwin.ui.base.VisualizingEvolutionaryApplet;

import com.rubecula.mocklet.MockAppletStub;
import com.rubecula.mocklet.MockAppletUtilities;

/**
 * @author Robin
 * 
 */
public class MockVisualizingEvolutionaryApplet extends VisualizingEvolutionaryApplet {

	/**
	 * @throws HeadlessException
	 */
	public MockVisualizingEvolutionaryApplet() throws HeadlessException {
		this(new HashMap<String, String>());
	}

	/**
	 * @param parameters
	 *            XXX
	 * @throws HeadlessException
	 */
	public MockVisualizingEvolutionaryApplet(final Map<String, String> parameters) throws HeadlessException {
		super(null);
		setLayout(new FlowLayout());
		setStub(new MockAppletStub(parameters));
	}

	/**
	 * @param key
	 * @return
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public Component getComponent(final String key) {
		return this._componentMap.get(key);
	}

	/**
	 * @see java.applet.Applet#showStatus(java.lang.String)
	 */
	@Override
	public void showStatus(final String msg) {
		System.out.println("MockApplet status: " + msg);
	}

	/**
	 * @see java.applet.Applet#start()
	 */
	@Override
	public void start() {
		this._componentMap = MockAppletUtilities.getComponents(this);
		super.start();
	}

	@Override
	protected Map<String, Component> createOptionsComponentMap() {
		final Map<String, Component> map = new HashMap<String, Component>();
		map.put(LBL_VERSION, new JLabel(Version.DARWIN_VERSION));
		map.put(MockApplet.LBL_SHOW_IDENTIFIERS, new JCheckBox());
		return map;
	}

	// final Map<String, String> _parameters;

	private static final long serialVersionUID = 7694034655895637187L;

	private Map<String, Component> _componentMap;

}
