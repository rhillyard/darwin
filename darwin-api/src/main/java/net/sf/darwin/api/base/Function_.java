/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Function_.java
 * Created on Apr 9, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Function;
import net.sf.darwin.core.FunctionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.jexpression.EvalExpression;
import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.EvaluatorMutable;
import com.rubecula.jexpression.Evaluator_;
import com.rubecula.jexpression.JexpressionException;
import com.rubecula.jexpression.Noop;
import com.rubecula.jexpression.Notation;
import com.rubecula.jexpression.Term;

/**
 * This is the base class for all functions which can be implemented by
 * evaluating an expression via an {@link Evaluator}. The methods operate
 * generally by delegation to the {@link #evaluator} field.
 * 
 * @author Robin Hillyard
 * 
 */
public abstract class Function_ implements Function, EvalExpressionMutable {

	/**
	 * Protected constructor to construct a Function_ with no evaluator.
	 */
	protected Function_() {
		this(null);
	}

	/**
	 * Protected constructor to construct a Function_ with given evaluator.
	 * 
	 * @param evaluator
	 *            the evaluator (expression engine) used to evaluate the
	 *            expression.
	 * 
	 */
	protected Function_(final Evaluator evaluator) {
		super();
		try {
			init(evaluator);
		} catch (final Exception e) {
			throw new DarwinException("Evaluator exception constructing Function_()", e); //$NON-NLS-1$
		}
	}

	/**
	 * @see net.sf.darwin.core.Function#addListener(net.sf.darwin.core.FunctionListener)
	 */
	@Override
	public void addListener(final FunctionListener listener) {
		getListeners().add(listener);
	}

	/**
	 * Delegate to {@link #evaluator}
	 * 
	 * TEST
	 * 
	 * CONSIDER putting in interface
	 * 
	 * @param name
	 * @param value
	 * @return the value actually set
	 */
	public Number addSymbol(final String name, final Number value) {
		if (getEvaluator() != null) {
			try {
				return getEvaluator().addSymbol(name, value);
			} catch (final JexpressionException e) {
				throw new DarwinException(S_CLASS + "addVariable(): exception", e); //$NON-NLS-1$
			}
		}
		throw new DarwinException(S_CLASS + "addVariable()" + S_EVALUATOR_NOT_SET); //$NON-NLS-1$
	}

	/**
	 * WARNING: do not rewrite this method unless you really know what you are
	 * doing!
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Function_ other = (Function_) obj;
		try {
			if (!getStandardFunctionExpression().equals(other.getStandardFunctionExpression()))
				return false;
		} catch (final JexpressionException e) {
			LOG.warn("Function_.equals() exception", e); //$NON-NLS-1$
			return false;
		}
		return true;
	}

	/**
	 * Evaluate the function based on the variables passed in as parameters. *
	 * 
	 * @param parameters
	 * 
	 *            CONSIDER putting in interface
	 * 
	 * @return the value of the expression evaluated by the {@link Evaluator} or
	 *         the value returned from {@link #evaluateByEvaluator(Object...)}.
	 * @throws FunctionException
	 * @see net.sf.darwin.core.FitnessFunction#getFitness(double, double,
	 *      double)
	 */
	public Number evaluate(final Object... parameters) throws FunctionException {
		final int length = parameters.length;
		if (length % 2 == 0 && length > 0) {
			if (isDifferent()) {
				if (getEvaluator() != null)
					return evaluateByEvaluator(parameters);
				throw new FunctionException("FitnessFunction_.evaluate(): expression is different but no evaluator available"); //$NON-NLS-1$
			}
			return Double.valueOf(evaluateByStandardMethod(parameters));
		}
		throw new FunctionException("FitnessFunction_.evaluate(): number of arguments must be positive and even"); //$NON-NLS-1$
	}

	/**
	 * If {@link #getEvaluator()} returns an {@link EvalExpression}, delegate to
	 * it. Else throw new {@link DarwinException};
	 * 
	 * @throws DarwinException
	 * @see com.rubecula.jexpression.EvalExpression#getExpression()
	 */
	@Override
	public String getExpression() {
		final Evaluator eval = getEvaluator();
		if (eval != null)
			return eval.getExpression();
		throw new DarwinException("getExpression: no evaluator"); //$NON-NLS-1$
	}

	/**
	 * If {@link #getEvaluator()} returns an {@link EvalExpression}, delegate to
	 * it. Else throw new {@link DarwinException};
	 * 
	 * @throws DarwinException
	 * @see com.rubecula.jexpression.EvalExpression#getExpressionTerms()
	 */
	@Override
	public Term[] getExpressionTerms() {
		final Evaluator eval = getEvaluator();
		if (eval != null)
			return eval.getExpressionTerms();
		throw new DarwinException("getExpression: no evaluator"); //$NON-NLS-1$
	}

	/**
	 * Delegate to {@link #evaluator}
	 * 
	 * @return the value as an object (or null if the evaluator is not set).
	 * @throws JexpressionException
	 * @see com.rubecula.jexpression.Evaluator#getValue()
	 */
	@Override
	public Number getValue() throws JexpressionException {
		if (getEvaluator() != null)
			return getEvaluator().getValue();
		return null;
	}

	/**
	 * WARNING: do not rewrite this method unless you really know what you are
	 * doing!
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		try {
			result = prime * result + getStandardFunctionExpression().hashCode();
		} catch (final JexpressionException e) {
			LOG.warn("Function_.hashCode() exception", e); //$NON-NLS-1$
		}
		return result;
	}

	/**
	 * CONSIDER putting in interface
	 * 
	 * @return true if the expression has changed from the original expression.
	 */
	public boolean isDifferent() {
		return this.different;
	}

	/**
	 * @return true if the underlying function can be varied by setting a new
	 *         expression in the evaluator. If there is no evaluator, then false
	 *         is returned.
	 */
	@Override
	public boolean isMutable() {
		if (getEvaluator() instanceof EvalExpressionMutable)
			return ((EvalExpressionMutable) getEvaluator()).isMutable();
		return false;
	}

	/**
	 * Conditionally set expression (only if it is different).
	 * 
	 * @see com.rubecula.jexpression.EvalExpressionMutable#setExpression(java.lang.CharSequence[])
	 */
	@Override
	public void setExpression(final CharSequence... tokens) throws JexpressionException {
		if (isMutable()) {
			if (tokens == null || tokens.length == 0)
				throw new JexpressionException("Function_.setExpression(): tokens null or empty"); //$NON-NLS-1$
			if (getEvaluator() == null)
				throw new JexpressionException("Function_.setExpression(): evaluator is null"); //$NON-NLS-1$
			final CharSequence[] standardFunction = getStandardFunctionTokens();
			boolean same = tokens.length == standardFunction.length;
			for (int i = 0; same && i < standardFunction.length && i < tokens.length; i++) {
				same = standardFunction[i].equals(tokens[i]);
			}
			if (!same)
				initExpression(tokens);
		} else
			throw new JexpressionException("Function_.setExpression(): evaluator does not implement HasVariableExpression"); //$NON-NLS-1$

	}

	/**
	 * @param expression
	 * @throws JexpressionException
	 * @see com.rubecula.jexpression.EvalExpressionMutable#setExpression(java.lang.String)
	 */
	@Override
	public void setExpression(final String expression) throws JexpressionException {
		if (isMutable()) {
			if (expression == null || expression.length() == 0)
				throw new JexpressionException("Function_.setExpression(): expression is null or empty"); //$NON-NLS-1$
			if (Evaluator_.isDifferent(expression, getEvaluator(), getStandardFunctionTokens())) {
				// At this point we already know that the evaluator implements
				// EvalExpressionMutable
				final EvalExpressionMutable variableEvaluator = (EvalExpressionMutable) getEvaluator();
				variableEvaluator.setExpression(expression);
				onFunctionChange();
				setDifferent(true);
			}
			// else nothing needs doing
		} else
			throw new JexpressionException("Function_.setExpression(): evaluator does not implement HasVariableExpression"); //$NON-NLS-1$
	}

	/**
	 * @return delegate to {@link #evaluator} and return the result.
	 * @throws FunctionException
	 */
	protected Object getErrorInfo() throws FunctionException {
		if (getEvaluator() != null)
			try {
				return getEvaluator().getErrorInfo();
			} catch (final JexpressionException e) {
				throw new FunctionException(S_CLASS + "getErrorInfo() exception", e); //$NON-NLS-1$
			}
		throw new FunctionException(S_CLASS + "getErrorInfo()" + S_EVALUATOR_NOT_SET); //$NON-NLS-1$
	}

	/**
	 * @return the value of {@link #evaluator}
	 */
	protected Evaluator getEvaluator() {
		return this.evaluator;
	}

	/**
	 * @return {@link #getIdentifier()},<code>this</code> in a new
	 *         {@link HashMap}
	 */
	protected ExpressionMap getExpressionMap() {
		final ExpressionMap result = new ExpressionMap();
		result.addExpression(getIdentifier(), this);
		return result;
	}

	/**
	 * @return the listeners
	 */
	protected Collection<FunctionListener> getListeners() {
		return this._listeners;
	}

	protected void onFunctionChange() {
		for (final FunctionListener functionListener : getListeners()) {
			functionListener.onFunctionChange(this);
		}
	}

	/**
	 * Delegate to {@link #evaluator}
	 * 
	 * TEST
	 * 
	 * @param name
	 * @return the reult of invoking {@link Evaluator#removeSymbol(String)}
	 * @throws FunctionException
	 */
	protected Object removeVariable(final String name) throws FunctionException {
		if (getEvaluator() != null)
			try {
				return getEvaluator().removeSymbol(name);
			} catch (final JexpressionException e) {
				throw new FunctionException(S_CLASS + "removeVariable() exception", e); //$NON-NLS-1$
			}
		throw new FunctionException(S_CLASS + "removeVariable()" + S_EVALUATOR_NOT_SET); //$NON-NLS-1$
	}

	/**
	 * @throws FunctionException
	 * 
	 */
	protected void reset() throws FunctionException {
		if (isMutable()) {
			try {
				initExpression(getStandardFunctionExpression());
			} catch (final JexpressionException e) {
				throw new FunctionException("Function_.reset() exception", e); //$NON-NLS-1$
			}
			setDifferent(false);
		} else
			throw new FunctionException("Function_.reset(): immutable"); //$NON-NLS-1$

	}

	/**
	 * @param evaluator
	 * @throws FunctionException
	 */
	protected void setEvaluator(final Evaluator evaluator) throws FunctionException {
		if (evaluator == null) {
			try {
				initExpression(getStandardFunctionExpression());
			} catch (final JexpressionException e) {
				throw new FunctionException("setEvaluator(): initExpression exception", e); //$NON-NLS-1$
			}
			this.evaluator = evaluator;
			setDifferent(false);
		} else if (!(evaluator instanceof Noop)) {
			this.evaluator = evaluator;
			if (evaluator instanceof EvalExpressionMutable) {
				try {
					((EvalExpressionMutable) evaluator).setExpression(getStandardFunctionTokens());
					reset();
				} catch (final JexpressionException e) {
					throw new FunctionException("setEvaluator(): setExpression exception", e); //$NON-NLS-1$
				}
			}
		}
	}

	/**
	 * @param arguments
	 *            a variable set of Objects which must be passed to the actual
	 *            implemented version of {@link #standardFunction(Object...)}.
	 * 
	 * @return the standard value for this function. This is called when
	 *         {@link #isDifferent()} returns false.
	 * @throws FunctionException
	 *             XXX
	 */
	abstract protected double standardFunction(Object... arguments) throws FunctionException;

	/**
	 * @param notation
	 *            the notation type used by the evaluator.
	 * 
	 * @return the sequence of tokens that corresponds to the
	 *         {@link #standardFunction(Object...)}. This is called by
	 *         {@link #reset()}.
	 */
	abstract protected CharSequence[] standardFunctionTokens(Notation notation);

	/**
	 * @param parameters
	 * @return
	 * @throws FunctionException
	 */
	private Number evaluateByEvaluator(final Object... parameters) throws FunctionException {
		for (int i = 0; i < parameters.length / 2; i++) {
			if (parameters[i * 2] instanceof String) {
				final String varName = (String) parameters[i * 2];
				if (parameters[i * 2 + 1] instanceof Number) {
					addSymbol(varName, (Number) parameters[i * 2 + 1]);
				} else
					throw new FunctionException("FitnessFunction_.evaluate(): even-positioned arguments must implement Number"); //$NON-NLS-1$
			} else
				throw new FunctionException("FitnessFunction_.evaluate(): odd-positioned arguments must be String"); //$NON-NLS-1$
		}
		try {
			final Number result = getValue();
			final Object errorInfo = getErrorInfo();
			if (errorInfo != null)
				LOG.warn("Function_.evaluateByEvaluator: " + errorInfo); //$NON-NLS-1$
			//			LOG.debug("Function_.evaluateByEvaluator: {}", result); //$NON-NLS-1$
			if (LOG.isDebugEnabled())
				LOG.debug("Function_.evaluateByEvaluator: " + result); //$NON-NLS-1$
			return result;
		} catch (final JexpressionException e) {
			throw new FunctionException("Function_.evaluate(): exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @param parameters
	 * @return
	 * @throws FitnessException
	 */
	private double evaluateByStandardMethod(final Object... parameters) throws FunctionException {
		final Object[] values = new Object[parameters.length / 2];
		for (int i = 0; i < parameters.length / 2; i++)
			values[i] = parameters[i * 2 + 1];
		final double result = standardFunction(values);
		//		LOG.trace("Function_.evaluateByStandardMethod(): result: {}", new Double(result)); //$NON-NLS-1$
		if (LOG.isTraceEnabled())
			LOG.trace("Function_.evaluateByStandardMethod(): result: " + new Double(result)); //$NON-NLS-1$
		return result;
	}

	/**
	 * @return the standard expression for this function, represented as a
	 *         String
	 * @throws JexpressionException
	 */
	private String getStandardFunctionExpression() throws JexpressionException {
		return Evaluator_.formExpression(getEvaluator(), getStandardFunctionTokens());
	}

	/**
	 * @return
	 */
	private CharSequence[] getStandardFunctionTokens() {
		return standardFunctionTokens(getEvaluator() != null ? getEvaluator().getNotation() : Notation.Invalid);
	}

	/**
	 * @param eval
	 * @throws JexpressionException
	 */
	private void init(final Evaluator eval) {
		try {
			if (eval instanceof Noop)
				this.evaluator = eval;
			else
				setEvaluator(eval);
			if (eval instanceof EvaluatorMutable)
				reset();
		} catch (final FunctionException e) {
			throw new DarwinException("Function_.init() exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * IMPORTANT NOTE: this method should not be called unless the evaluator is
	 * mutable: it does nothing in such a case.
	 * 
	 * @param tokens
	 * @throws JexpressionException
	 */
	private void initExpression(final CharSequence... tokens) throws JexpressionException {
		final Evaluator eval = getEvaluator();
		if (eval instanceof EvalExpressionMutable) {
			((EvalExpressionMutable) eval).setExpression(tokens);
			setDifferent(true);
		}
		return;
	}

	private void setDifferent(final boolean isDifferent) {
		this.different = isDifferent;
	}

	private boolean different;

	/**
	 * 
	 */
	private static final double FACTOR_NORMAL_DIST_PDF = Math.sqrt(2 * Math.PI);

	/**
	 * 
	 */
	private static final String S_EVALUATOR_NOT_SET = ": evaluator not set"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String S_CLASS = "Function_."; //$NON-NLS-1$

	protected Evaluator evaluator;

	// protected static final Logger LOG =
	// LoggerFactory.getLogger(Function_.class);
	protected static final Log LOG = LogFactory.getLog(Function_.class);

	private final Collection<FunctionListener> _listeners = new ArrayList<>();

	/**
	 * TODO when the 2.0 version of the commons match package comes out, I think
	 * we can use the density() method of the NormalDistributionImpl class.
	 * 
	 * @param value
	 * @param mean
	 * @param stdDev
	 * @return <code>e ^ (-diff * diff / 2 / stdDev / stdDev) / stdDev / FACTOR_NORMAL_DIST_PDF</code>
	 */
	public static double normalDistributionDensityFunction(final double value, final double mean, final double stdDev) {
		final double difference = value - mean;
		return Math.exp(-difference * difference / 2 / stdDev / stdDev) / stdDev / FACTOR_NORMAL_DIST_PDF;
	}

	/**
	 * TODO when the 2.0 version of the commons match package comes out, I think
	 * we can use the density() method of the NormalDistributionImpl class with
	 * the appropriate scaling factor.
	 * 
	 * XXX to be consistent with (unscaled) normal distribution, the stdDev
	 * should be squared inside the exponent of E.
	 * 
	 * @param value
	 * @param mean
	 * @param stdDev
	 * @return <code>e ^ (-diff * diff / 2 /stdDev) where diff = value - mean</code>
	 */
	public static double normalDistributionDensityFunctionScaled(final double value, final double mean, final double stdDev) {
		final double difference = value - mean;
		return Math.exp(-difference * difference / 2 / stdDev);
	}

}
