/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenotypeCache_Standard.java
 * Created on Jul 14, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Cache_;
import net.sf.darwin.core.Basic;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.PhenotypeCache;
import net.sf.tostring0.Identifiable;

/**
 * This cache maintains a set of Phenotypes indexed by Basic/Identifiable pair.
 * Normally, the identifiable object is a population, or at any rate, a
 * {@link Colony}. The purpose is to save time regenerating the phenotype when
 * it is unnecessary.
 * 
 * TODO must ensure that this cache is flushed when variables change.
 * 
 * @author Robin Hillyard
 * 
 */
public class PhenotypeCache_Standard<P> extends Cache_<PhenotypeCacheKey, Phenotype<P>> implements PhenotypeCache<P> {

	/**
	 * @see net.sf.darwin.core.PhenotypeCache#addPhenotype(net.sf.darwin.core.Basic,
	 *      net.sf.tostring0.Identifiable, net.sf.darwin.core.Phenotype)
	 */
	@Override
	public void addPhenotype(final Basic genes, final Identifiable identifiable, final Phenotype<P> phenotype) {
		add(new PhenotypeCacheKey(genes, identifiable), phenotype);
	}

	/**
	 * @see net.sf.darwin.core.PhenotypeCache#flush(net.sf.darwin.core.Basic,
	 *      net.sf.tostring0.Identifiable)
	 */
	@Override
	public void flush(final Basic genes, final Identifiable identifiable) {
		flush(new PhenotypeCacheKey(genes, identifiable));
	}

	/**
	 * @return "phenotype cache"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return "phenotype cache"; //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.PhenotypeCache#getPhenotype(net.sf.darwin.core.Basic,
	 *      net.sf.tostring0.Identifiable)
	 */
	@Override
	public Phenotype<P> getPhenotype(final Basic genes, final Identifiable identifiable) {
		return get(new PhenotypeCacheKey(genes, identifiable));
	}

}
