/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genomic_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.35 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.impl.Mutator_Null;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.ExPhen;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Expression;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Progenitor;
import net.sf.darwin.core.Trait;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class implementing base methods for the {@link Genomic} interface.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            Genomic
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Genomic
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Genomic_<P, G> extends Base implements Genomic<P, G> {

	/**
	 * TEST
	 * 
	 * Constructor for a new Genomic_ instance. This type of Genomic undergoes
	 * no mutations.
	 * 
	 * @param identifier
	 *            the identifier for this Genomic.
	 * @param ploidy
	 *            the ploidy (haploid/diploid/etc).
	 * @param meiosis
	 *            the implementer of {@link Meiosis}.
	 */
	protected Genomic_(final String identifier, final int ploidy, final Meiosis<G> meiosis) {
		this(identifier, ploidy, meiosis, new Mutator_Null<G>());
	}

	/**
	 * Constructor for a new Genomic_ instance.
	 * 
	 * @param identifier
	 *            the identifier for this Genomic.
	 * @param ploidy
	 *            the ploidy (haploid/diploid/etc).
	 * @param meiosis
	 *            the implementer of {@link Meiosis}.
	 * @param mutator
	 *            the implementer of {@link Mutator} which will be applied to
	 *            all new genomes.
	 */
	protected Genomic_(final String identifier, final int ploidy, final Meiosis<G> meiosis, final Mutator<G> mutator) {
		super();
		this._chromosomes = new AuditableList<>(identifier);
		this._ploidy = ploidy;
		this._meiosis = meiosis;
		this._mutator = mutator;
	}

	/**
	 * @see net.sf.darwin.core.Genomic#addChromosome(net.sf.darwin.core.Chromosome)
	 */
	@Override
	public boolean addChromosome(final Chromosome<G> chromosome) {
		addChromosome(getCount(), chromosome);
		return true; // provided that no exception is thrown in add, the result
		// should be true.
	}

	/**
	 * @see net.sf.darwin.core.Genomic#addChromosome(int,
	 *      net.sf.darwin.core.Chromosome)
	 */
	@Override
	public void addChromosome(final int index, final Chromosome<G> chromosome) {
		getChromosomeList().add(index, chromosome);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Replicator#createGamete(java.util.List)
	 */
	@Override
	public Genome<P, G> createGamete(final Genes<G> genes) {
		final Meiosis<G> meiosis = getMeiosis();
		if (meiosis != null) {
			final Genome<P, G> gamete = meiosis.doMeiosis(this, genes, null);
			if (gamete != null) {
				final Genome<P, G> result = mutate(gamete);
				if (LOG.isTraceEnabled())
					LOG.trace("New gamete (after meiosis): " + result); //$NON-NLS-1$
				return result;
			}
			throw new DarwinException("Genomic_.createGamete() logic error");//$NON-NLS-1$
		}
		throw new DarwinException("Genomic_.createGamete(): meiosis is null"); //$NON-NLS-1$
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Genomic_<?, ?> other = (Genomic_<?, ?>) obj;
		if (getMeiosis() == null) {
			if (other._meiosis != null)
				return false;
		} else if (!getMeiosis().equals(other.getMeiosis()))
			return false;
		if (getMutator() == null) {
			if (other._mutator != null)
				return false;
		} else if (!getMutator().equals(other.getMutator()))
			return false;
		if (getPloidy() != other._ploidy)
			return false;
		return true;
	}

	/**
	 * Standard implementation of express for this Genomic. Construct a new
	 * empty phenotype (the result), then for each gene passed in, express it
	 * and add the resulting trait to the result.
	 * 
	 * @param phenotype
	 *            the phenotype to which we will add any traits resulting from
	 *            the expression of the given genes
	 * @param colony
	 *            the population to which our genome belongs, usually ignored
	 *            but referenced if population and/or environment affect gene
	 *            expression.
	 * @param genes
	 *            the genes to express
	 * @see net.sf.darwin.core.Genomic#express(Phenotype, Colony,
	 *      net.sf.darwin.core.Genes)
	 */
	@Override
	public <E> void express(final Phenotype<P> phenotype, final Colony<E, P, G> colony, final Genes<G> genes) {
		if (genes.size() == 0) {
			Expresser<P> expresser = getExpresser(null);
			if (expresser == null)
				expresser = findAnyExpresser();
			doExpression(phenotype, colony, null, GenomeFactory.makeLinear(this, getPloidy()), expresser);
		} else {
			final Map<Locus<G>, Genome<P, G>> geneMap = prepareGeneMap(genes, this, getPloidy());
			for (final Entry<Locus<G>, Genome<P, G>> e : geneMap.entrySet())
				doExpression(phenotype, colony, e.getKey(), e.getValue(), getExpresser(e.getKey()));
		}
	}

	/**
	 * Standard implementation of this method simply calls
	 * {@link #express(Phenotype, Colony, Genes)} with null population.
	 * 
	 * TODO consider checking the cache here (although this implementation is
	 * never called other than by unit tests).
	 * 
	 * @see net.sf.darwin.core.Genomic#express(Phenotype,
	 *      net.sf.darwin.core.Genes)
	 */
	@Override
	public <E> void express(final Phenotype<P> phenotype, final Genes<G> genes) {
		express(phenotype, (Colony<E, P, G>) null, genes);
	}

	/**
	 * Override this method if not using the standard alphabet of bases.
	 * 
	 * @return "CGAT".
	 * @see net.sf.darwin.core.Genomic#getAlphabet()
	 */
	@Override
	public String getAlphabet() {
		return Genomic.STANDARD_ALPHABET;
	}

	/**
	 * @see net.sf.darwin.core.Genomic#getChromosome(int)
	 */
	@Override
	public Chromosome<G> getChromosome(final int index) {
		return getChromosomeList().get(index);
	}

	/**
	 * @see net.sf.darwin.core.Genomic#getChromosomes()
	 * 
	 *      XXX showIdentifer=false has no effect because it uses an either/or
	 *      rule.
	 */
	@Override
	@ToString(child = true)
	public Collection<Chromosome<G>> getChromosomes() {
		return getChromosomeList();
	}

	/**
	 * @return the number of chromosomes in this Genomic.
	 */
	@Override
	public int getCount() {
		return getChromosomeList().size();
	}

	/**
	 * Get the expresser for the specific locus. If the locus is null, then we
	 * return the common expresser. If the locus is not null, then we return the
	 * expresser for that locus. If there is no such expresser, then we return
	 * the expresser for the parent locus, if such exists. If there is still no
	 * expresser for this locus, then we return the common expresser.
	 * 
	 * @param locus
	 *            the locus (or null) for which we want the expresser
	 * @return the appropriate expresser for the given locus else (see above for
	 *         rules).
	 * 
	 * @see net.sf.darwin.core.Genomic#getExpresser(net.sf.darwin.core.Locus)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Expresser<P> getExpresser(final Locus<G> locus) {
		Expresser<P> result = null;
		if (locus != null) {
			if (getExpressers() != null)
				result = getExpressers().get(locus);
			// TODO consider doing this via polymorphism
			if (result == null && locus.getParent() instanceof Locus)
				result = getExpresser((Locus<G>) locus.getParent());
		}
		if (result == null)
			result = getExpresserCommon();
		return result;
	}

	/**
	 * 
	 * @return the mapping for {@link Locus} => {@link Expresser} for this
	 *         Genomic.
	 */
	@Override
	public Map<Locus<G>, Expresser<P>> getExpressers() {
		return this.expressers;
	}

	/**
	 * @return the identifier for the set of chromosomes
	 * @see com.rubecula.darwin.foundation.AuditableList#getIdentifier()
	 * 
	 *      XXX omit=true has no effect because identifiers are not treated like
	 *      normal ToString-annotation objects.
	 */
	@Override
	public String getIdentifier() {
		return getChromosomeList().getIdentifier();
	}

	/**
	 * method to step through each chromosome and get the nth locus relative to
	 * this genom object.
	 * 
	 * Take care: this final method returns even the sex locus.
	 * 
	 * @return the nth locus or null if no such locus
	 * @see net.sf.darwin.core.Genomic#getLocus(int)
	 */
	@Override
	public final Locus<G> getLocus(final int n) {
		int pos = 0;
		final Collection<Chromosome<G>> chromosomes = getChromosomes();
		for (final Chromosome<G> chromosome : chromosomes) {
			final int size = chromosome.getCount();
			if (n - pos < size)
				return chromosome.getLocus(n - pos);
			pos += size;
			if (pos > n)
				return null;
		}
		return null;
	}

	/**
	 * Get the total number of locus by cycling through the chromosomes and
	 * adding one for sex if appropriate
	 * 
	 * @see net.sf.darwin.core.Genomic#getLocusCount()
	 */
	@Override
	public int getLocusCount() {
		final Collection<Chromosome<G>> chromosomes = getChromosomes();
		int size = 0;
		for (final Chromosome<G> chromosome : chromosomes)
			size += chromosome.getCount();
		return size;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Genomic#getMeiosis()
	 */
	@Override
	@ToString
	public Meiosis<G> getMeiosis() {
		return this._meiosis;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Genomic#getMutator()
	 */
	@Override
	@ToString
	public Mutator<G> getMutator() {
		return this._mutator;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Ploidy#getPloidy()
	 */
	@Override
	@ToString
	public int getPloidy() {
		return this._ploidy;
	}

	@Override
	public int getTotalLocusCount() {
		int size = getLocusCount();
		if (this instanceof IsSexual)
			size++;
		return size;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getMeiosis() == null) ? 0 : getMeiosis().hashCode());
		result = prime * result + ((getMutator() == null) ? 0 : getMutator().hashCode());
		result = prime * result + getPloidy();
		return result;
	}

	/**
	 * Non-mutating method to mutate this Genome and return a mutated copy.
	 * 
	 * XXX consider changing the parameter (and result) to {@link Genes}.
	 * 
	 * TODO consider reworking the callers of this method -- the usage is
	 * inconsistent between sexual and asexual situations.
	 * 
	 * @param genome
	 * 
	 * @return either <code>this</code> Genome or a mutated copy.
	 * @see net.sf.darwin.core.Genomic#mutate(net.sf.darwin.core.Genome)
	 */
	@Override
	public Genome<P, G> mutate(final Genome<P, G> genome) {
		final Mutator<G> mutator = getMutator();

		if (mutator.isIdentity())
			return genome;

		final Genome<P, G> result = mutator.mutate(genome);
		// XXX For now, at least, we try to simplify the genome here.
		// Ideally, we should only simplify when we change the environment,
		// because that might attract a few more mutations, but doing it
		// here should be OK.
		mutator.simplify(result, getExpressers());

		return result;
		// TODO simplify
	}

	/**
	 * Normalize the given genome with respect to the reference genome. For
	 * example, if genome and reference are identical to start with, genome will
	 * be modified such that it is now empty. This is particularly useful in
	 * asexual systems.
	 * 
	 * @param genome
	 * @param reference
	 */
	@Override
	public void normalize(final Genome<P, G> genome, final Genome<P, G> reference) {
		getMutator().normalize(genome, reference);
		simplify(genome);
	}

	/**
	 * Register an expresser.
	 * 
	 * @see net.sf.darwin.core.Genomic#putExpresser(net.sf.darwin.core.Locus,
	 *      net.sf.darwin.core.Expresser)
	 */
	@Override
	public Expresser<P> putExpresser(final Locus<G> locus, final Expresser<P> expresser) {
		if (getExpressers() == null)
			this.expressers = new HashMap<>();
		return getExpressers().put(locus, expresser);
	}

	/**
	 * @see net.sf.darwin.core.Genomic#setChromosomes(java.util.Collection)
	 */
	@Override
	public void setChromosomes(final Collection<Chromosome<G>> chromosomes) {
		clearChromosomes();
		getChromosomeList().addAll(chromosomes);
	}

	/**
	 * Set the common expresser (for all loci). Note that the expresser map is
	 * used in preference to this expresser.
	 * 
	 * @param expresser
	 */
	@Override
	public void setExpresser(final Expresser<P> expresser) {
		this.expresserCommon = expresser;
	}

	/**
	 * Used by dependency injection: sets the expresser map. Note that this
	 * expresser map is used in preference to the common expresser.
	 * 
	 * @param expressers
	 */
	public void setExpressers(final Map<Locus<G>, Expresser<P>> expressers) {
		this.expressers.clear();
		this.expressers.putAll(expressers);
	}

	/**
	 * Repeatedly invoke the simplify method of the mutator on the genome until
	 * there are no further changes.
	 * 
	 * @return true if any simplifications have been made to genome.
	 * 
	 * @see net.sf.darwin.core.Genomic#simplify(Genome)
	 */
	@Override
	public boolean simplify(final Genome<P, G> genome) {
		final Mutator<G> mutator = getMutator();
		boolean result = false;
		boolean keepSimplifying;
		do {
			final boolean simplified = mutator.simplify(genome, getExpressers());
			keepSimplifying = simplified;
			result |= simplified;
		} while (keepSimplifying);
		return result;
	}

	/**
	 * 
	 */
	protected void clearChromosomes() {
		getChromosomeList().clear();
	}

	/**
	 * @return the expresserCommon
	 */
	protected Expresser<P> getExpresserCommon() {
		return this.expresserCommon;
	}

	/**
	 * This method is kind of a kluge and only necessary because we have
	 * configured the genome to have just one polygenic expresser rather than
	 * setting the expresser as a common expresser.
	 * 
	 * TODO consider configuring as a common expresser instead
	 * 
	 * @return any expresser we can find
	 */
	private Expresser<P> findAnyExpresser() {
		for (final Locus<G> locus : getExpressers().keySet()) {
			return getExpresser(locus);
		}
		return null;
	}

	/**
	 * @return
	 */
	@ToString(omit = true)
	private AuditableList<Chromosome<G>> getChromosomeList() {
		return this._chromosomes;
	}

	/**
	 * 
	 */
	protected transient Expresser<P> expresserCommon;

	/**
	 * 
	 */
	protected transient Map<Locus<G>, Expresser<P>> expressers = new HashMap<>();

	/**
	 * 
	 */
	protected transient final AuditableList<Chromosome<G>> _chromosomes;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Genomic_.class);

	/**
	 * The implementer of the {@link Meiosis} interface for this Genomic.
	 */
	private final Meiosis<G> _meiosis;

	/**
	 * The "ploidy" is the genetic "form" of the system. It is the number of
	 * alleles that can be simultaneously carried at a particular locus by an
	 * organism. Many organisms (including vertebrates) are diploid, having
	 * chromosomes in matching pairs. So far as I know triploid systems do not
	 * occur in nature. <br>
	 * For a haploid system, the Ode is 1; for a diploid system, the Ode is 2,
	 * etc.
	 */
	private final int _ploidy;

	/**
	 * The implementer of the {@link Mutator} interface for this Genomic.
	 */
	private final Mutator<G> _mutator;

	/**
	 * @param phenotype
	 * @param colony
	 * @param locus
	 *            may be null (only used for logging)
	 * @param genome
	 * @param expresser
	 *            may be null - in which case logging will be generated.
	 */
	private static <E, U, W> void doExpression(final Phenotype<U> phenotype, final Colony<E, U, W> colony, final Locus<W> locus,
			final Genes<W> genome, final Expresser<U> expresser) {
		if (expresser != null)
			processExpressions(phenotype, express(expresser, colony, genome));
		else
			LOG.warn("Genomic_.express(): expresser is null for locus: " + locus); //$NON-NLS-1$
	}

	/**
	 * Convenience method to express a set of genes in the context of this
	 * {@link Genomic} object.
	 * 
	 * XXX consider inlining this method.
	 * 
	 * @param expresser
	 *            must be non-null
	 * @param colony
	 * @param genes
	 * @return the expression of the given genes
	 */
	@SuppressWarnings("unchecked")
	private static <E, U, W> Collection<Expression<U>> express(final Expresser<U> expresser, final Colony<E, U, W> colony,
			final Genes<W> genes) {
		return ((Expresser_<U>) expresser).express(colony, genes.toArray(new Gene[0]));
	}

	/**
	 * @param genes
	 * @param genomic
	 *            XXX
	 * @param ploidy
	 *            XXX
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static <U, W> Map<Locus<W>, Genome<U, W>> prepareGeneMap(final Genes<W> genes, final Genomic<U, W> genomic,
			final int ploidy) {
		final Map<Locus<W>, Genome<U, W>> geneMap = new HashMap<>();
		for (int i = 0; i < genes.size(); i++) {
			final Gene<W> gene = genes.getGene(i);
			Locus<W> locus = gene.getLocus();
			Progenitor parent = locus.getParent();
			// TODO consider doing this via polymorphism
			while (parent instanceof Locus) {
				locus = ((Locus<W>) parent);
				parent = locus.getParent();
			}
			Genome<U, W> genome = geneMap.get(locus);
			if (genome == null) {
				genome = GenomeFactory.makeLinear(genomic, ploidy);
				geneMap.put(locus, genome);
			}
			genome.getGenes().add(gene);
		}
		return geneMap;
	}

	/**
	 * TODO FEATURE ENVY?
	 * 
	 * Add any traits that resulted from expressing the genes into the given
	 * phenotype. We ignore any {@link ExPhen} expressions.
	 * 
	 * @param phenotype
	 * @param expressions
	 */
	private static <U> void processExpressions(final Phenotype<U> phenotype, final Collection<Expression<U>> expressions) {
		for (final Expression<U> expression : expressions) {
			// TODO consider doing this via polymorphism
			if (expression instanceof Trait)
				phenotype.addTrait((Trait<U>) expression);
			else if (expression instanceof ExPhen)
				phenotype.addExtended((ExPhen<U>) expression);
			else
				throw new DarwinException("expression of type: " + expression.getClass() + " was ignored"); //$NON-NLS-1$//$NON-NLS-2$
		}
	}

}