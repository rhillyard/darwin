/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.api.base;

import net.sf.darwin.api.impl.Mortality_Reaper;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Fecundity;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Mortality;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Taxon;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Default implementation of {@link Taxon}.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.2 $
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            taxon
 * @param <G>
 *            the type of the genetic information which is encoded in this taxon
 */
@Lifecycle(permanent = true)
public final class Taxon_Darwinian<E, P, G> extends Taxon_<E, P, G> {

	/**
	 * Constructor to create a Taxon with no populations, and with the default
	 * mortality implementer.
	 * 
	 * @param identifier
	 *            the identifier for this new system.
	 * @param realm
	 *            the realm to which this system belongs.
	 * @param genomic
	 *            the genomic on which all organisms in all populations of this
	 *            new system will be based.
	 * @param phenome
	 *            the phenome which all organisms of all populations of this new
	 *            system will exhibit.
	 * @param censusTaker
	 *            an implementer of {@link Census} for the purpose of censusing
	 *            populations of the new system.
	 * @param infantMortality
	 *            the probability of an infant dying in any of the populations
	 *            of this new system.
	 * @param chooser
	 *            the mate choice implementation.
	 * @param fecundity
	 *            the fecundity implementation.
	 * @param random
	 *            a random number source.
	 */
	public Taxon_Darwinian(final String identifier, final Realm realm, final Genomic<P, G> genomic, final Phenome<P> phenome,
			final Census censusTaker, final double infantMortality, final MateChoice chooser, final Fecundity fecundity,
			final RandomGenerator random) {
		this(identifier, realm, genomic, phenome, censusTaker, new Mortality_Reaper(random, infantMortality), chooser, fecundity);
	}

	/**
	 * Constructor to create a Taxon with no populations.
	 * 
	 * @param identifier
	 *            the identifier for this new system.
	 * @param realm
	 *            the realm to which this system belongs.
	 * @param genomic
	 *            the genomic on which all organisms in all populations of this
	 *            new system will be based.
	 * @param phenome
	 *            the phenome which all organisms of all populations of this new
	 *            system will exhibit.
	 * @param censusTaker
	 *            an implementer of {@link Census} for the purpose of censusing
	 *            populations of the new system.
	 * @param mortality
	 *            the mortality implementation.
	 * @param chooser
	 *            the mate choice implementation.
	 * @param fecundity
	 *            the fecundity implementation.
	 */
	public Taxon_Darwinian(final String identifier, final Realm realm, final Genomic<P, G> genomic, final Phenome<P> phenome,
			final Census censusTaker, final Mortality mortality, final MateChoice chooser, final Fecundity fecundity) {
		super(identifier, realm, genomic, phenome, censusTaker, mortality, chooser, fecundity);
	}

	private static final long serialVersionUID = 3439091197982298319L;

}