/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Lek_Standard.java
 * Created on Dec 4, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import java.util.Collection;

import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Lek;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Sexual;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.math.random.RandomGenerator;

/**
 * Concrete class to model a collection of breeding males from which a female
 * may choose a mate. In our case, the Lek_Standard is an abstract concept
 * rather than a true biological lek, see
 * http://en.wikipedia.org/wiki/Lek_%28mating_arena%29.
 * 
 * TODO filter out females from the collection of organisms.
 * 
 * TODO make this an interface, not a class
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = false)
public class Lek_Standard<E, P, G> extends RandomIterable_Standard<Organism<E, P, G>> implements Lek<E, P, G> {

	/**
	 * @param colony
	 *            XXX
	 * @param random
	 */
	public Lek_Standard(final Colony<E, P, G> colony, final RandomGenerator random) {
		this(colony, random, 1);
	}

	/**
	 * @param colony
	 *            XXX
	 * @param random
	 * @param sampleFraction
	 */
	public Lek_Standard(final Colony<E, P, G> colony, final RandomGenerator random, final double sampleFraction) {
		this(CollectionUtils.select(colony.getOrganisms(), predicate), random, (int) Math.ceil(sampleFraction
				* colony.getOrganisms().size()), colony);
	}

	/**
	 * @param organisms
	 *            the explicit set of organisms to include in the lek
	 * @param random
	 * @param sampleFraction
	 * @param colony
	 *            a reference to the population (not used to determine
	 *            organisms).
	 */
	protected Lek_Standard(final Collection<Organism<E, P, G>> organisms, final RandomGenerator random,
			final double sampleFraction, final Colony<E, P, G> colony) {
		super(organisms, random, (int) Math.ceil(sampleFraction * organisms.size()));
		this._colony = colony;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Lek#getColony()
	 */
	@Override
	public Colony<E, P, G> getColony() {
		return this._colony;
	}

	static final Predicate predicate = new LekPredicate();

	private final Colony<E, P, G> _colony;

	static class LekPredicate implements Predicate {

		/**
		 * @param object
		 * @return
		 */
		@Override
		public boolean evaluate(final Object object) {
			// TODO consider doing this via polymorphism
			if (object instanceof Sexual) {
				return !((Sexual) object).isFemale();
			}
			return false;
		}

	}

}
