/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 * 
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Phenome_;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Phenotype;

/**
 * <p>
 * Default implementation of {@link Phenome}. This concrete class represents the
 * template for the {@link Phenotype} of Organisms within a Taxon. It is the
 * physical counterpart to the Genomic and forms part of a Taxon system.
 * </p>
 * <p>
 * Named in honor of two great men in the field of evolutionary biology:
 * </p>
 * <ul>
 * <li><a href="http://en.wikipedia.org/wiki/Stephen_Jay_Gould">Stephen Jay
 * Gould</a>.</li>
 * <li><a href="http://en.wikipedia.org/wiki/John_Gould">John Gould</a>.</li>
 * </ul>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.3 $
 * 
 */
@Lifecycle(permanent = true)
public final class Phenome_Gouldian<P> extends Phenome_<P> {

	/**
	 * This constructs a new (empty) Phenome belonging to a Taxon.
	 * 
	 * @param fitnessFunction
	 *            the implementer of FitnessEngine which evaluates the fintness
	 *            of a phenome for an environemnt.
	 * 
	 */
	public Phenome_Gouldian(final FitnessEngine fitnessFunction) {
		super(fitnessFunction);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2543242189453313824L;

}
