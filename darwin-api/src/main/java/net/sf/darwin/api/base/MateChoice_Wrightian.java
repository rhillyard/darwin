/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Dec 7, 2003
 *
 */

package net.sf.darwin.api.base;

import net.sf.darwin.api.impl.Attraction_Uniform;
import net.sf.darwin.core.Attraction;
import net.sf.darwin.core.Organism;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Default implementation of MateChoice. In this default implementation, females
 * are not at all <i>choosy</i> - all males appear the same to all female.
 * 
 * However, the desirability of a male (to any female) is based on his age (the
 * experience factor). </p>
 * <p>
 * Named in honor of <a href="http://en.wikipedia.org/wiki/Sewall_Wright">Sewall
 * Wright</a>. There is no intended implication that this kind of mate choice
 * would be particularly favored by F-H-W population genetics.
 * 
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.1 $
 */
public final class MateChoice_Wrightian extends MateChoice_ {

	/**
	 * Secondary constructor.
	 * 
	 * Create a new MateChoice_Wrightian implementation with given random number
	 * generator and with uniform attraction.
	 * 
	 * @param random
	 */
	public MateChoice_Wrightian(final RandomGenerator random) {
		this(random, new Attraction_Uniform());
	}

	/**
	 * Primary constructor.
	 * 
	 * Create a new MateChoice_Wrightian implementation with given random number
	 * generator and with given attraction.
	 * 
	 * @param random
	 * @param attraction
	 */
	public MateChoice_Wrightian(final RandomGenerator random, final Attraction attraction) {
		super(random, attraction);
	}

	/**
	 * This implementation makes all males desirable and therefore a female will
	 * never choose a mate from an alternative population.
	 * 
	 * @see net.sf.darwin.core.MateChoice#getMinimumDesirability()
	 */
	@Override
	public double getMinimumDesirability() {
		return 0;
	}

	/**
	 * @see net.sf.darwin.api.base.MateChoice_#getDesirability(net.sf.darwin.core.Organism)
	 */
	@Override
	protected double getDesirability(final Organism male) {
		// First we use the superclass to determine the desirability based on
		// the male's viability (10 if viable, 1 if not)
		final double viability = super.getDesirability(male);

		// Now we factor in the age of the male - the longer he's been around
		// the more we like him [the more experienced, the better).
		return viability * (male.getAge() + 1);
	}

}
