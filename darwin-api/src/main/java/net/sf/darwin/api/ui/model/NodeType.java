/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: NodeType
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public enum NodeType {
	Substrate, Cell, Organelle, Property, Unelaborated;
}
