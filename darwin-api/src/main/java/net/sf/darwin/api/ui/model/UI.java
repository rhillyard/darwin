package net.sf.darwin.api.ui.model;

import java.io.Writer;

/**
 * @author rhillya
 * 
 */
public interface UI extends Dirty, Boss {

	/**
	 * @param node
	 * @throws UIException
	 */
	public abstract void addNode(Node node) throws UIException;

	public abstract Runnable getListener();

	/**
	 * 
	 */
	public abstract void listenForEvents();

	/**
	 * CONSIDER placing in another interface
	 */
	public abstract void notifySubstrate();

	/**
	 * @param substrate
	 */
	public abstract void setBoss(Boss boss);

	/**
	 * CONSIDER placing in another interface
	 */
	public abstract void setMinimumTime(final long minimumTime);

	/**
	 * CONSIDER placing in another interface
	 */
	public abstract void setWriter(final Writer writer);

	/**
	 * @param message
	 */
	public abstract void showMessage(String message);

	/**
	 * CONSIDER placing in another interface
	 */
	public abstract void wake();

	/**
	 * UI bean id
	 */
	public static final String BEAN_ID = "UI"; //$NON-NLS-1$
}