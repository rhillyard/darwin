/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EnvironmentListener_Writer.java
 * Created on Apr 24, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.impl;

import java.io.PrintWriter;

import net.sf.darwin.api.base.EnvironmentListener_;
import net.sf.darwin.core.Environment;

/**
 * @author Robin Hillyard
 * 
 */
public class EnvironmentListener_Writer<E> extends EnvironmentListener_<E> {

	/**
	 * @param writer
	 *            XXX
	 * 
	 */
	public EnvironmentListener_Writer() {
		this(new PrintWriter(System.out));
	}

	/**
	 * @param writer
	 *            XXX
	 * 
	 */
	public EnvironmentListener_Writer(final PrintWriter writer) {
		super();
		this._writer = writer;
	}

	/**
	 * print a message on the {@link #_writer}.
	 * 
	 * @see net.sf.darwin.core.EnvironmentListener#onEnvironmentChange
	 *      (net.sf.darwin.core.Environment)
	 */
	@Override
	public void onEnvironmentChange(final Environment<E> env) {
		final String identifier = env.getIdentifier();
		this._writer.println("Change in environment " + identifier); //$NON-NLS-1$
	}

	private final PrintWriter _writer;

}
