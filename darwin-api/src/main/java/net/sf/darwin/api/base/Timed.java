/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Timed.java
 * Created on Dec 2, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.base;

import java.util.Calendar;

import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.EvolutionException;

/**
 * @author Robin Hillyard
 * 
 */
public interface Timed {

	/**
	 * @return the rate, i.e. the number of milliseconds between successive
	 *         "ticks" for this {@link Timed} object, typically an
	 *         {@link Evolution}.
	 */
	public abstract int getRate();

	/**
	 * @return real time of this evolution
	 * @throws EvolutionException
	 * @see com.rubecula.darwin.evolution.CalendarEvolution#getTime()
	 */
	public abstract Calendar getTime() throws EvolutionException;

	/**
	 * Change the rate of ticking of this {@link Timed} object.
	 * 
	 * @param rate
	 *            the new rate, in milliseconds.
	 * @throws EvolutionException
	 */
	public void setRate(final int rate) throws EvolutionException;

}