/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Trait_Discrete.java
 * Created on May 29, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Trait_;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;

/**
 * Concrete implementation of discrete {@link Trait}, i.e. a trait that can take
 * one of a finite set of values (called variants). Such variants must be
 * registered (added) to the phenotypic character before they can be referenced.
 * 
 * Consider naming in honor of <a
 * href="http://en.wikipedia.org/wiki/Reginald_C._Punnett">Reginald Punnett</a>.
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            trait
 */
@Lifecycle(permanent = false)
public final class Trait_Discrete<P> extends Trait_<P> {
	/**
	 * Package-scope constructor to create a simple trait for the given
	 * phenotypic character.
	 * 
	 * @param character
	 * @param variant
	 *            the key to the specific variant
	 */
	public Trait_Discrete(final Pharacter<P> character, final String variant) {
		super(character, variant);
	}

	/**
	 * For a discrete trait, we simply return this.
	 * 
	 * @see net.sf.darwin.core.Trait#getKey()
	 */
	@Override
	public Object getKey() {
		return this;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -576158159987933972L;

}
