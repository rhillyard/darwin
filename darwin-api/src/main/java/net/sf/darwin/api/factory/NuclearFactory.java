/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: NuclearFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.13 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.Nuclear_Clone;
import net.sf.darwin.api.impl.Nuclear_Zygote;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Nuclear;
import net.sf.tostring0.ToString;

/**
 * Factory Class to create Nuclear objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
@ToString(showIdentifier = false, showClass = false, allBeanMethods = false)
public final class NuclearFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private NuclearFactory() {
		super();
	}

	private static final String S_FCTRY_MAKE_NUCLEAR = "NuclearFactory.makeNuclear(): "; //$NON-NLS-1$

	/**
	 * @param genome
	 * @param colonyId
	 * @return a new {@link Nuclear_Clone} constructed from the genome and the
	 *         appropriate population identifier
	 */
	public static <U, W> Nuclear<U, W> makeClone(final Genome<U, W> genome, final String colonyId) {
		return new Nuclear_Clone<>(genome, colonyId);
	}

	/**
	 * @param clazz
	 *            the class of Nuclear to instantiate, which must have a
	 *            constructor of form Nuclear(EcoFactor,Object).
	 * @param genome
	 *            the genome
	 * @param popId
	 *            the population identifier
	 * @return a newly constructed object of class <code>clazz</code> and based
	 *         on the genome and population id.
	 * 
	 *         TEST
	 */
	@SuppressWarnings("nls")
	public static <U, W> Nuclear<U, W> makeNuclear(final Class<? extends Nuclear<U, W>> clazz, final Genome<U, W> genome,
			final String popId) {
		try {
			final Constructor<? extends Nuclear<U, W>> constructor = clazz.getConstructor(new Class<?>[] { Genome.class,
					String.class });
			return constructor.newInstance(new Object[] { genome, popId });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_NUCLEAR + "no suitable constructor for " + clazz, e);
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_NUCLEAR + "cannot instantiation " + clazz, e);
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_NUCLEAR + "illegal access to " + clazz, e);
		} catch (final InvocationTargetException e) {
			throw new DarwinException(S_FCTRY_MAKE_NUCLEAR + "constructor for " + clazz + " threw exception",
					e.getTargetException());
		}
	}

	/**
	 * @param genome
	 * @param colonyId
	 * @return a new {@link Nuclear_Zygote} constructed from the genome and
	 *         colonyId
	 */
	public static <U, W> Nuclear<U, W> makeZygote(final Genome<U, W> genome, final String colonyId) {
		return new Nuclear_Zygote<>(genome, colonyId);
	}

}
