/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OrganismCensusContext.java
 * Created on Nov 9, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Allele;
import net.sf.darwin.core.FrequencyMap;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;

/**
 * @author Robin Hillyard
 * 
 *         TODO consider extending AToString
 * 
 */
public class OrganismCensusContext<P, G> {

	/**
	 * @param character
	 * @param alleleFrequencies
	 * @param ageFrequencies
	 * @param sexFrequencies
	 * @param traitFrequencies
	 * 
	 */
	public OrganismCensusContext(final Pharacter<P> character, final FrequencyMap<Allele<G>> alleleFrequencies,
			final NumericFrequencyMap ageFrequencies, final FrequencyMap<String> sexFrequencies,
			final FrequencyMap<Trait<P>> traitFrequencies) {
		super();
		this._character = character;
		this._alleleFrequencies = alleleFrequencies;
		this._ageFrequencies = ageFrequencies;
		this._sexFrequencies = sexFrequencies;
		this._traitFrequencies = traitFrequencies;
	}

	/**
	 * @return the ageFrequencies
	 */
	public NumericFrequencyMap getAgeFrequencies() {
		return this._ageFrequencies;
	}

	/**
	 * @return the alleleFrequencies
	 */
	public FrequencyMap<Allele<G>> getAlleleFrequencies() {
		return this._alleleFrequencies;
	}

	/**
	 * @return the character
	 */
	public Pharacter<P> getCharacter() {
		return this._character;
	}

	/**
	 * @return the sexFrequencies
	 */
	public FrequencyMap<String> getSexFrequencies() {
		return this._sexFrequencies;
	}

	/**
	 * @return the traitFrequencies
	 */
	public FrequencyMap<Trait<P>> getTraitFrequencies() {
		return this._traitFrequencies;
	}

	private final Pharacter<P> _character;

	private final FrequencyMap<Allele<G>> _alleleFrequencies;

	private final NumericFrequencyMap _ageFrequencies;

	private final FrequencyMap<String> _sexFrequencies;

	private final FrequencyMap<Trait<P>> _traitFrequencies;

}