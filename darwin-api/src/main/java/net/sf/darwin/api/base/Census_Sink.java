/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_Sink.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.base;

import java.io.PrintWriter;

import net.sf.darwin.core.Censusible;

/**
 * Base implementation the {@link SinkCensus} interface.
 * 
 * @author Robin Hillyard
 */
public abstract class Census_Sink extends Sink_ implements SinkCensus {

	protected Census_Sink(final PrintWriter printWriter) {
		super();
		this._printWriter = printWriter;
	}

	/**
	 * @see java.lang.Appendable#append(java.lang.CharSequence)
	 */
	@Override
	public PrintWriter append(final CharSequence csq) {
		return getPrintWriter().append(csq);
	}

	/**
	 * @see net.sf.darwin.core.Census#census(net.sf.darwin.core.Censusible, int,
	 *      java.lang.Object)
	 */
	@Override
	public void census(final Censusible individual, final int depth, final Object context) {
		Census_.doCensus(this, individual, depth, context);
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Sink_#close()
	 */
	@Override
	public void close() {
		getPrintWriter().close();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Census_Sink other = (Census_Sink) obj;
		if (getPrintWriter() == null) {
			if (other._printWriter != null)
				return false;
		} else if (getPrintWriter() != (other._printWriter)) // more strict
			return false;
		return true;
	}

	/**
	 * @see com.rubecula.darwin.domain.helper.Sink_#flush()
	 */
	@Override
	public void flush() {
		getPrintWriter().flush();
	}

	/**
	 * @return {@link #_printWriter}
	 * @see net.sf.darwin.api.base.SinkCensus#getPrintWriter()
	 */
	@Override
	public PrintWriter getPrintWriter() {
		return this._printWriter;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getPrintWriter() == null) ? 0 : getPrintWriter().hashCode());
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Census#present(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public void present(final String string, final Object context) {
		if (context != null)
			print("[" + context + "] "); //$NON-NLS-1$//$NON-NLS-2$
		println(string);

	}

	/**
	 * @param s
	 * @see java.io.PrintWriter#print(java.lang.String)
	 */
	@Override
	public void print(final String s) {
		getPrintWriter().print(s);
	}

	/**
	 * 
	 * @see java.io.PrintWriter#println()
	 */
	@Override
	public void println() {
		getPrintWriter().println();
	}

	/**
	 * @param x
	 * @see java.io.PrintWriter#println(java.lang.String)
	 */
	@Override
	public void println(final String x) {
		getPrintWriter().println(x);
	}

	private final PrintWriter _printWriter;

}
