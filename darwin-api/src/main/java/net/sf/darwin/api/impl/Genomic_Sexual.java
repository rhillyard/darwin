/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.IsSexual;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Phenotype;
import net.sf.tostring0.ToString;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Concrete implementation of {@link Genomic} representing the possible genetic
 * information contained in a Taxon. A Genomic_Sexual extends Genomic_ comes
 * with exactly one default chromosome which is sex-linked <b>and</b> no
 * non-sex-linked chromosomes.
 * </p>
 * 
 * XXX consider renaming this.
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.11 $
 */
@Lifecycle(permanent = true)
public final class Genomic_Sexual<P, G> extends Genomic_Diploid<P, G> implements IsSexual {

	// /**
	// * {@inheritDoc}
	// * @see
	// net.sf.darwin.api.base.Genomic_#createGamete(net.sf.darwin.core.Genes)
	// */
	// @Override
	// public Genome createGamete(Genes genes) {
	// // FIXME we need to do something extra for sex genomic
	// return super.createGamete(genes);
	// }

	/**
	 * Constructor for a new Genomic_Sexual instance using the given meiosis
	 * implementation and the given mutator implementation.
	 * 
	 * @param identifier
	 * @param meiosis
	 * @param mutator
	 * @param random
	 *            an implementer of {@link RandomGenerator}.
	 */
	public Genomic_Sexual(final String identifier, final Meiosis<G> meiosis, final Mutator<G> mutator,
			final Genomic<Boolean, Boolean> sexGenomic) {
		super(identifier, meiosis, mutator);
		this.sexGenomic = sexGenomic;
	}

	/**
	 * Constructor for a new Genomic_Sexual instance using the given meiosis
	 * implementation and the given mutator implementation.
	 * 
	 * @param identifier
	 * @param meiosis
	 * @param mutator
	 * @param random
	 *            an implementer of {@link RandomGenerator}.
	 */
	public Genomic_Sexual(final String identifier, final Meiosis<G> meiosis, final Mutator<G> mutator,
			final RandomGenerator random) {
		super(identifier, meiosis, mutator);
		this.sexGenomic = createStandardSexGenomic(identifier, random);
	}

	/**
	 * Constructor for a new Genomic_Sexual instance using the given meiosis
	 * implementation.
	 * 
	 * XXX this appears to be called only by unit tests
	 * 
	 * @param identifier
	 *            the identifier for this Genomic.
	 * @param meiosis
	 *            an implementer of {@link Meiosis}.
	 * @param random
	 *            an implementer of {@link RandomGenerator}.
	 */
	public Genomic_Sexual(final String identifier, final Meiosis<G> meiosis, final RandomGenerator random) {
		this(identifier, meiosis, new Mutator_Null<G>(), random);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.Genomic_#express(net.sf.darwin.core.Phenotype,
	 *      net.sf.darwin.core.Colony, net.sf.darwin.core.Genes)
	 */
	@Override
	public void express(final Phenotype phenotype, final Colony colony, final Genes genes) {
		// FIXME we need to do something extra for sex genomic
		// TODO we need to sort out the types here. The first expression is
		// based on Boolean phenotype and genotype, not P and G
		getSexGenomic().express(phenotype, colony, genes);
		super.express(phenotype, colony, genes);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.Genomic_#express(net.sf.darwin.core.Phenotype,
	 *      net.sf.darwin.core.Genes)
	 */
	@Override
	public void express(final Phenotype phenotype, final Genes genes) {
		// FIXME we need to do something extra for sex genomic
		// TODO we need to sort out the types here. The first expression is
		// based on Boolean phenotype and genotype, not P and G
		getSexGenomic().express(phenotype, genes);
		super.express(phenotype, genes);
	}

	@Override
	public Genomic<Boolean, Boolean> getSexGenomic() {
		return this.sexGenomic;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.Genomic_#mutate(net.sf.darwin.core.Genome)
	 */
	@Override
	public Genome mutate(final Genome genome) {
		// TODO we need to sort out the types here. The first expression is
		// based on Boolean phenotype and genotype, not P and G
		getSexGenomic().mutate(genome);
		return super.mutate(genome);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.Genomic_#normalize(net.sf.darwin.core.Genome,
	 *      net.sf.darwin.core.Genome)
	 */
	@Override
	public void normalize(final Genome genome, final Genome reference) {
		// TODO we need to sort out the types here. The first expression is
		// based on Boolean phenotype and genotype, not P and G
		getSexGenomic().normalize(genome, reference);
		super.normalize(genome, reference);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.Genomic_#simplify(net.sf.darwin.core.Genome)
	 */
	@Override
	public boolean simplify(final Genome genome) {
		// TODO we need to sort out the types here. The first expression is
		// based on Boolean phenotype and genotype, not P and G
		getSexGenomic().simplify(genome);
		return super.simplify(genome);
	}

	/**
	 * Retain only the sex chromosome
	 * 
	 * @see net.sf.darwin.api.base.Genomic_#clearChromosomes()
	 */
	@Override
	protected void clearChromosomes() {
		this._chromosomes.clear();
	}

	@ToString
	private Genomic<Boolean, Boolean> sexGenomic;

	/**
	 * 
	 */
	private static final long serialVersionUID = 8305182888274182993L;

	/**
	 * @param identifier
	 * @param random
	 * @return a new instance of {@link Genomic_Diploid} initialized with a new
	 *         {@link Chromosome_Sex} and an {@link Expresser_Sex}.
	 */
	public static Genomic_Diploid<Boolean, Boolean> createStandardSexGenomic(final String identifier, final RandomGenerator random) {
		// TODO check that we really want no mutation in sex
		final Genomic_Diploid<Boolean, Boolean> result = new Genomic_Diploid<>(
				identifier + "-sex", new Meiosis_Chromosome<Boolean>(random), new Mutator_Null<Boolean>()); //$NON-NLS-1$
		result.addChromosome(new Chromosome_Sex(random, result.getAlphabet()));
		result.setExpresser(new Expresser_Sex());
		return result;
	}

}
