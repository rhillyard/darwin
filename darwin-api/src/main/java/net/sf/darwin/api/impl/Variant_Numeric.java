/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Variant_Numeric.java
 * Created on Nov 5, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Quantifiable;

/**
 * Numerical Variant. The various variants are based on actual {@link Number}
 * values.
 * 
 * @author Robin Hillyard
 * 
 */
public final class Variant_Numeric extends Variant_Basic<Number> implements Quantifiable {

	/**
	 * @param value
	 */
	public Variant_Numeric(final Number value) {
		super(value);
	}

	/**
	 * @param key
	 * @param value
	 */
	public Variant_Numeric(final String key, final Number value) {
		super(key, value);
	}

	/**
	 * @return the value of this variant expressed as a double.
	 * @see net.sf.darwin.api.base.Attribute_#getValue()
	 */
	@Override
	public double doubleValue() {
		return super.getValue().doubleValue();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6713285851074477036L;

}
