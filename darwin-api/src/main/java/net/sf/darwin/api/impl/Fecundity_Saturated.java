/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Dec 7, 2003
 *
 */

package net.sf.darwin.api.impl;

import java.text.MessageFormat;

import net.sf.darwin.api.base.Fecundity_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Fecundity;
import net.sf.darwin.core.Lifecycle;

/**
 * Default implementation of {@link Fecundity}. Fecundity is defined according
 * to population saturation. See the saturation thresholds:
 * {@link #$DefaultSaturationThresholds}.
 * 
 * TODO extend this type with Fecundity_RK which conforms to the RK selection
 * theory model.
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.21 $
 */
@Lifecycle(permanent = true)
public final class Fecundity_Saturated extends Fecundity_ {

	/**
	 * Constructor using default saturation thresholds, viz.
	 * {@link #$DefaultSaturationThresholds}.
	 */
	public Fecundity_Saturated() {
		this($DefaultSaturationThresholds);
	}

	/**
	 * Constructor using default saturation thresholds, viz.
	 * {@link #$DefaultSaturationThresholds}.
	 * 
	 * @param saturationThresholds
	 *            This is the array of saturation thresholds for calculating
	 *            fecundity. The number of possible non-zero fecundity values
	 *            which may result is equal to the number of thresholds in the
	 *            array. The thresholds must be ordered from highest saturation
	 *            to lowest saturation. A saturation value which is higher than
	 *            the 0th element will result in a zero fecundity.
	 */
	public Fecundity_Saturated(final double[] saturationThresholds) {
		super();
		this._saturationThresholds = saturationThresholds;
	}

	/**
	 * Get the fecundity for a population whose saturation is given by
	 * saturation. The fecundity is determined by a lookup table of thresholds
	 * which may be set via {@link #_saturationThresholds} and which defaults to
	 * {@link #$DefaultSaturationThresholds}.
	 * 
	 * @see net.sf.darwin.core.Fecundity#getFecundity(net.sf.darwin.core.Colony)
	 */
	@Override
	public <E, P, G> int getFecundity(final Colony<E, P, G> colony) {
		return getFecundity(colony.getSaturation());
	}

	/**
	 * TODO reduce visibility to default.
	 * 
	 * @param saturation
	 * @return a number greater than 0
	 */
	public int getFecundity(final double saturation) {
		int result = 0;

		final int fecundityMax = getSaturationThresholds().length;
		for (int fecundity = fecundityMax; fecundity > 0; fecundity--) {
			if (saturation < getSaturationThresholds()[fecundity - 1]) {
				result = fecundity;
				break;
			}
			continue;
		}

		logFecundity(saturation, result);

		return result;
	}

	/**
	 * Typically set by bean properties (i.e. reflection, dependency injection).
	 * 
	 * @param thresholds
	 */
	public void setSaturationThresholds(final double[] thresholds) {
		this._saturationThresholds = thresholds;
	}

	/**
	 * TEST
	 * 
	 * @return the array of saturation thresholds as set by
	 *         {@link #setSaturationThresholds(double[])}.
	 */
	protected double[] getSaturationThresholds() {
		return this._saturationThresholds;
	}

	private static final String LOG_MSG_1 = "Fecundity for saturation: {0}: {1}"; //$NON-NLS-1$

	/**
	 * If less than 30% saturated, then fecundity = 4; else if less than 75%
	 * saturated, then fecundity = 3; else if less than 100% saturated, then
	 * fecundity = 2; else if less than 200% saturated, then fecundity = 1; else
	 * fecundity = 0;
	 */
	public static final double[] $DefaultSaturationThresholds = { 2, 1, 0.75, 0.3 };

	/**
	 * The current saturation thresholds.
	 */
	private double[] _saturationThresholds;

	/**
	 * @param saturation
	 * @param result
	 */
	@SuppressWarnings("boxing")
	private static void logFecundity(final double saturation, final int result) {
		if (LOG.isTraceEnabled())
			LOG.trace(MessageFormat.format(LOG_MSG_1, saturation, result));
	}

}
