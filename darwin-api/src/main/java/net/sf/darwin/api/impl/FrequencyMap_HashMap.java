/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FrequencyMap_HashMap.java
 * Created on Nov 6, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.api.base.SinkCensus;
import net.sf.darwin.core.Frequency;
import net.sf.darwin.core.FrequencyMap;

import org.apache.commons.logging.Log;

/**
 * Class to implement a frequency map.
 * 
 * Note that this type is not of the usual type family: there is no interface or
 * abstract type to support this, hence we have public non-bean methods that are
 * not defined in an external interface.
 * 
 * @author Robin Hillyard
 * @param <T>
 *            the class representing the objects whose frequency we wish to map.
 * 
 *            TODO consider extending AToString
 * 
 */
public class FrequencyMap_HashMap<T> implements FrequencyMap<T>, Loggable<T> {

	/**
	 * @param name
	 *            the identifier for this map
	 * 
	 */
	public FrequencyMap_HashMap(final String name) {
		super();
		this._name = name;
		this._map = new HashMap<>();
		this.total = 0;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#add(T)
	 */
	@Override
	public void add(final T key) {
		final Frequency frequency = getFrequency(key);
		frequency.increment();
		this.total++;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#add(T, int)
	 */
	@Override
	public void add(final T key, final int increment) {
		final Frequency frequency = getFrequency(key);
		frequency.add(increment);
		this.total += increment;
	}

	/**
	 * @return true if the total is correct (this is just for testing purposes).
	 */
	public boolean check() {
		int sum = getTotal();
		for (final T key : keySet())
			sum -= get(key);
		return sum == 0;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#clear()
	 */
	@Override
	public void clear() {
		getMap().clear();
		this.total = 0;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#containsKey(T)
	 */
	@Override
	public boolean containsKey(final T key) {
		return getMap().containsKey(key);
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#get(T)
	 */
	@Override
	public int get(final T key) {
		final Frequency frequency = getMap().get(key);
		return frequency != null ? frequency.intValue() : 0;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#getName()
	 */
	@Override
	public String getName() {
		return this._name;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#getTotal()
	 */
	@Override
	public int getTotal() {
		return this.total;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return getMap().isEmpty();
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#keySet()
	 */
	@Override
	public Set<T> keySet() {
		return getMap().keySet();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.impl.Loggable#log(org.apache.commons.logging.Log)
	 */
	@Override
	public void log(final Log log) {
		log.debug("frequencies for " + getName() + ": "); //$NON-NLS-1$//$NON-NLS-2$
		final Set<T> keySet = keySet();
		for (final T key : keySet) {
			final int frequency = get(key);
			log.debug(key.toString() + ":" + frequency); //$NON-NLS-1$
		}
	}

	/**
	 * @param writer
	 */
	public void output(final PrintWriter writer) {
		writer.print("frequencies for " + getName() + ": "); //$NON-NLS-1$//$NON-NLS-2$
		int remain = size();
		final Set<T> keySet = keySet();
		for (final T key : keySet) {
			final int frequency = get(key);
			writer.print(key.toString() + ":" + frequency); //$NON-NLS-1$
			if (--remain == 0)
				writer.println();
			else
				writer.print(", "); //$NON-NLS-1$
		}
	}

	/**
	 * @param sinkCensus
	 */
	public void output(final SinkCensus sinkCensus) {
		output(sinkCensus.getPrintWriter());
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#remove(T)
	 */
	@Override
	public int remove(final T key) {
		final Frequency frequency = getMap().remove(key);
		if (frequency != null) {
			final int value = frequency.intValue();
			this.total -= value;
			return value;
		}
		return 0;
	}

	/**
	 * @see net.sf.darwin.core.FrequencyMap#size()
	 */
	@Override
	public int size() {
		return getMap().size();
	}

	/**
	 * @return the value of {@link #_map}
	 */
	protected Map<T, Frequency> getMap() {
		return this._map;
	}

	/**
	 * @param key
	 * @return the existing (or new) value of frequency
	 */
	private Frequency getFrequency(final T key) {
		Frequency frequency = getMap().get(key);
		if (frequency == null) {
			frequency = new Frequency();
			getMap().put(key, frequency);
		}
		return frequency;
	}

	private final String _name;

	private transient final Map<T, Frequency> _map;

	private transient int total;

}
