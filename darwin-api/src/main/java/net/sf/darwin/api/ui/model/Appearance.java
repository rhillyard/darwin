/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Appearance
 * Created on: Oct 12, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public interface Appearance {

	/**
	 * @return the string to be used to identify the content
	 */
	public abstract String getContentString();

	/**
	 * @return the string to be used as the hover string
	 */
	public abstract String getHoverString();

	/**
	 * @return the name of the image
	 */
	public abstract String getImageName();

}
