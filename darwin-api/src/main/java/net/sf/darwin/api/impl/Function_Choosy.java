/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Function_Choosy.java
 * Created on Apr 20, 2009
 * @version $Revision: 1.24 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.api.base.Function_;
import net.sf.darwin.core.Choosy;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Desirable;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.HasExpressions;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.Notation;

/**
 * This class implements the {@link Choosy#getMinimumDesirability(double)}
 * function and allows dynamic varying of the specific formula, by emplying an
 * {@link Evaluator}.
 * 
 * Concrete implementation of {@link Desirable} which extends {@link Function_}
 * and implements {@link HasExpressions} (thus it can be updated graphically at
 * runtime).
 * 
 * @author Robin Hillyard
 * 
 */
public class Function_Choosy extends Function_ implements Choosy, HasExpressions {

	/**
	 * 
	 */
	public Function_Choosy() {
		super();
	}

	/**
	 * @param evaluator
	 */
	public Function_Choosy(final Evaluator evaluator) {
		super(evaluator);
	}

	/**
	 * @see net.sf.darwin.core.HasExpressions#getExpressions()
	 */
	@Override
	public ExpressionMap getExpressions() {
		return getExpressionMap();
	}

	/**
	 * @return (typically) "Choosiness"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return Messages.getString("Function_Choosy.1"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Choosy#getMinimumDesirability(double)
	 */
	@Override
	@SuppressWarnings("boxing")
	public double getMinimumDesirability(final double random) {
		try {
			final Number value = evaluate(RANDOM, random);
			logDesirability(random, value);
			return value.doubleValue();
		} catch (final FunctionException e) {
			throw new DarwinException("Function_Desirable.getDesirability(): exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @see net.sf.darwin.api.base.Function_#standardFunction(java.lang.Object[])
	 */
	@Override
	protected double standardFunction(final Object... arguments) throws FunctionException {
		// No protection against class cast exceptions
		assert arguments.length == 1 : "Function_Desirable.standardValue(): requires 1 argument"; //$NON-NLS-1$
		return evaluateFixedExpression(((Number) arguments[0]).doubleValue());
	}

	/**
	 * The standard formula is: <code>e^(random*3.8)</code>
	 * 
	 * TEST
	 * 
	 * @see net.sf.darwin.api.base.Function_#standardFunctionTokens(Notation)
	 */
	@Override
	protected CharSequence[] standardFunctionTokens(final Notation notation) {
		switch (notation) {
		case RPN:
			return new CharSequence[] { Concept.e, FitnessFunction.PREFIX_VAR_RPN + RANDOM, S_K, "*", Concept.pow }; //$NON-NLS-1$ 

		default:
			return new CharSequence[] { Concept.e, Concept.pow, "(" + RANDOM + "*" + //$NON-NLS-1$ //$NON-NLS-2$
					S_K + ")" }; //$NON-NLS-1$
		}
	}

	/**
	 * 3.8
	 */
	private static final String S_K = Messages.getString("Function_Choosy.2"); //$NON-NLS-1$

	/**
	 * 3.8
	 */
	private static final double K = Double.parseDouble(S_K);

	/**
	 * random
	 */
	private static final String RANDOM = Messages.getString("Function_Choosy.0"); //$NON-NLS-1$

	private static double evaluateFixedExpression(final double random) {
		final double power = random * K;
		return Math.pow(Math.E, power);
	}

	/**
	 * @param random
	 * @param value
	 */
	private static void logDesirability(final double random, final Number value) {
		LOG.debug("getMinimumDesirability() with random value=" + random + ": result=" + value + ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

}
