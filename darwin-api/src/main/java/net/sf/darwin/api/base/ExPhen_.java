/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExPhen_.java
 * Created on Jun 30, 2009
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExPhen;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Base implementation of an extended phenotype. <br/>
 * CONSIDER removing the EcoFactor part. Do we really need that? <br/>
 * CONSIDER we should make this a two-type generic class based on E and P. This
 * is because field {@link #_factor} should be of type EcoFactor&lt;E&gt;, not
 * EcoFactor&lt;P&gt; <br/>
 * 
 * @author Robin Hillyard
 *
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            Extended Phenotype
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class ExPhen_<P> extends Base implements ExPhen<P> {

	/**
	 * @param factor
	 *            the factor for this extended phenotype.
	 * @param value
	 *            XXX
	 * 
	 */
	protected ExPhen_(final EcoFactor<P> factor, final Object value) {
		super();
		this._factor = factor;
		this._value = value;
	}

	/**
	 * XXX we really must check that the best organism actually has the best
	 * travel time (because of the way fitness is calculated, it's possible that
	 * it actually is worse). We do currently check, but if it is worse, we need
	 * to adjust the parameters in RouteFitness (via configuration) [which,
	 * obviously, we don't do here].
	 * 
	 * @param criterion
	 *            the value which will be used to determine if we should update
	 *            the ecoFactor referenced by this {@link ExPhen} object. The
	 *            <i>higher</i> this value, the more likely the update will
	 *            occur.
	 * 
	 * @see net.sf.darwin.core.ExPhen#applyToEnvironment(Object)
	 */
	@Override
	public boolean applyToEnvironment(final Object criterion) {
		final EcoFactor ecoFactor = getEcoFactor();
		final EcoFactor revisedFactor = getRevisedFactor();
		if (revisedFactor != null && !ecoFactor.equals(revisedFactor)) {
			if (evaluate(criterion)) {
				final boolean updated = ecoFactor.update(revisedFactor);
				if (updated)
					postProcess(ecoFactor, getValue());
				return updated;
			}
			LOG.warn("processExtendedPhenotype: evaluate false: no update: consider adjusting fitness function"); //$NON-NLS-1$
			return false;
		}
		return false;
	}

	/**
	 * @see net.sf.darwin.core.ExPhen#getEcoFactor()
	 */
	@Override
	public EcoFactor<P> getEcoFactor() {
		return this._factor;
	}

	/**
	 * @see net.sf.darwin.core.ExPhen#getValue()
	 */
	@Override
	public Object getValue() {
		return this._value;
	}

	/**
	 * This method is used to determine whether there should be follow-up action
	 * on the processing of this {@link ExPhen} object. By convention, this
	 * method returns true only if derived value is <i>less</i> than criterion.
	 * 
	 * It should be overridden by concrete implementers of {@link ExPhen}.
	 * 
	 * @param criterion
	 *            the higher this value, the more likely the method will return
	 *            true, in general.
	 * @return true always returned by this base method.
	 */
	@SuppressWarnings("static-method")
	protected boolean evaluate(final Object criterion) {
		return true;
	}

	/**
	 * Method to get an updated eco factor for this {@link ExPhen} object which
	 * will be suitable to substitute in the environment as the outcome of
	 * processing this extended phenotype.
	 * 
	 * This method should be overridden by concrete implementers of
	 * {@link ExPhen}.
	 * 
	 * @return null
	 */
	@SuppressWarnings("static-method")
	protected <E> EcoFactor<E> getRevisedFactor() {
		return null;
	}

	/**
	 * Method which is called as a post-processor as the final step of
	 * {@link ExPhen#applyToEnvironment(Object)}. By default, this method does
	 * nothing.
	 * 
	 * Concrete implementations of {@link ExPhen} should consider overriding
	 * this method.
	 * 
	 * @param factor
	 *            the eco factor
	 * @param value
	 */
	protected <E> void postProcess(final EcoFactor<E> factor, final Object value) {
		// do nothing
	}

	@ToString
	private final EcoFactor<P> _factor;

	@ToString
	private final Object _value;

	protected static final Log LOG = LogFactory.getLog(ExPhen_.class);

}
