package net.sf.darwin.api.impl;

import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Stats;

import org.apache.commons.logging.Log;

/**
 * @author Robin Hillyard
 * 
 */
public class MortalityStats implements Stats {

	/**
	 * @param colony
	 */
	public MortalityStats(final Colony colony) {
		this._colony = colony;
	}

	/**
	 * @see net.sf.darwin.core.Stats#logStats(org.apache.commons.logging.Log,
	 *      int)
	 */
	@Override
	public void logStats(final Object logger, final int generation) {
		if (logger instanceof Log) {
			final Log log = (Log) logger;

			if (log.isDebugEnabled()) { // this method gets called a lot
				// so we'll "guard" this line of code
				final String message = getColony().getIdentifier()
						+ (getColony().isIsolated() ? " (isol) " : " ") //$NON-NLS-1$ //$NON-NLS-2$
						+ "(gen " + generation + "): " + this._survivors //$NON-NLS-1$ //$NON-NLS-2$
						+ " survivors with mean age/fitness: " + (this._survivorAge / this._survivors) + "/" + (this._survivorFitness / this._survivors) + //$NON-NLS-1$ //$NON-NLS-2$
						" and " //$NON-NLS-1$
						+ this._expirers
						+ " expirers with mean age/fitness: " + (this._expirerAge / this._expirers) + "/" + (this._expirerFitness / this._expirers); //$NON-NLS-1$//$NON-NLS-2$
				// XXX is this correct?
				log.debug(message);
			}
		} else
			throw new RuntimeException("logger does not implement Log"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Stats#update(boolean, double, int)
	 */
	@Override
	public void update(final boolean mortal, final double fitness, final int age) {
		if (mortal) {
			this._expirers++;
			this._expirerFitness += fitness;
			this._expirerAge += age;
		} else {
			this._survivors++;
			this._survivorFitness += fitness;
			this._survivorAge += age;
		}
	}

	/**
	 * @return the value of {@link #_colony}
	 */
	private Colony getColony() {
		return this._colony;
	}

	/**
	 * 
	 */
	private final Colony _colony;

	private double _survivorFitness = 0;

	private double _expirerFitness = 0;

	private double _survivorAge = 0;

	private double _expirerAge = 0;

	private int _survivors = 0;

	private int _expirers = 0;

}