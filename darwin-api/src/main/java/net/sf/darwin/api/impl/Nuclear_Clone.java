/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Nuclear_Clone.java
 * Created on Feb 21, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Nuclear_;
import net.sf.darwin.api.factory.NuclearFactory;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Nuclear;

/**
 * Concrete implementation of {@link Nuclear} for asexual reproduction.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = false)
public final class Nuclear_Clone<T, V> extends Nuclear_<T, V> {

	/**
	 * Package-scope constructor, invoked by {@link NuclearFactory}. Concrete
	 * implementation of {@link Nuclear} for asexual reproduction.
	 * 
	 * @param genome
	 * @param colonyId
	 */
	public Nuclear_Clone(final Genome<T, V> genome, final String colonyId) {
		super(genome, colonyId);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8367800711842925868L;

}
