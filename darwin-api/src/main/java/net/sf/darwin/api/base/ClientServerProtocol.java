/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: CityServerProtocol
 * Created on: Mar 14, 2012
 */

package net.sf.darwin.api.base;

/**
 * @author rhillya
 * 
 */
public interface ClientServerProtocol {

	/**
	 * @return the serverUp
	 */
	public abstract boolean isAlive();

	/**
	 * @param alive
	 */
	public abstract void setAlive(boolean alive);

	/**
	 * Hello
	 */
	public static final String GREETING_CLIENT = "Hello"; //$NON-NLS-1$

	/**
	 * Hello (Darwin)
	 */
	public static final String GREETING_SERVER = "Hello (Darwin)"; //$NON-NLS-1$

	/**
	 * Quit
	 */
	public static final String QUIT = "Quit"; //$NON-NLS-1$

	/**
	 * Goodbye
	 */
	public static final String GOODBYE = "Goodbye"; //$NON-NLS-1$

	/**
	 * Shutdown
	 */
	public static final String SHUTDOWN = "Shutdown"; //$NON-NLS-1$

}
