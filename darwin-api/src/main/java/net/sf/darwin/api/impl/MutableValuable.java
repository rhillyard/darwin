/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MutableValuable.java
 * Created on Mar 8, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Valuable;
import net.sf.darwin.core.ValueException;
import net.sf.tostring0.ToString;

import org.apache.commons.lang.mutable.MutableDouble;

/**
 * @author Robin Hillyard
 * 
 *         TODO consider using {@link MutableDouble} instead.
 * 
 *         TODO eliminate?
 */
public class MutableValuable implements Valuable {

	/**
	 * @param value
	 *            XXX
	 * 
	 */
	public MutableValuable(final double value) {
		super();
		setValue(value);
	}

	/**
	 * @param value
	 *            XXX
	 * 
	 */
	public MutableValuable(final Number value) {
		super();
		setValue(value);
	}

	/**
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return "" + this.value; //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Valuable#getValue()
	 */
	@Override
	@SuppressWarnings("boxing")
	@ToString(omit = true)
	public Number getValue() throws ValueException {
		return this.value;
	}

	/**
	 * @param factor
	 */
	public void scaleValue(final double factor) {
		setValue(this.value * factor);
	}

	/**
	 * @param factor
	 */
	public void scaleValue(final Number factor) {
		scaleValue(factor.doubleValue());
	}

	/**
	 * Reset the value according to the value of {@link Number#doubleValue()}
	 * 
	 * @param value
	 */
	public void setValue(final double value) {
		this.value = value;
	}

	/**
	 * Reset the value according to the value of {@link Number#doubleValue()}
	 * 
	 * @param value
	 */
	public void setValue(final Number value) {
		setValue(value.doubleValue());
	}

	private double value;

}
