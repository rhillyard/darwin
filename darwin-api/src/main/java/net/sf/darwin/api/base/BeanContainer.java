/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: BeanContainer
 * Created on: Feb 26, 2012
 */

package net.sf.darwin.api.base;

import java.io.PrintWriter;
import java.net.URL;
import java.util.Set;

import net.sf.darwin.core.BeanContainerException;

/**
 * @author rhillya
 * 
 */
public interface BeanContainer {
	// These all need to be matched up elsewhere

	public abstract void cleanup();

	/**
	 * @throws BeanContainerException
	 * 
	 */
	public abstract void configure() throws BeanContainerException;

	public abstract void configureAndRunEvolution(final int maxGenerations, final boolean debug, final String configurationFile,
			final Class<?> configClass, Object[] preConfigData, boolean run) throws BeanContainerException;

	public abstract void doMain(OptionMap<?, Object> optionMap, boolean run);

	public abstract Object getBean(final String key);

	public abstract Set<String> getBeanKeys();

	/**
	 * @return
	 */
	public abstract String getIdentifier();

	/**
	 * @return
	 */
	public abstract PrintWriter getWriter();

	/**
	 * @param name
	 * @param bean
	 * @throws BeanContainerException
	 */
	public abstract void imposeBean(String name, Object bean) throws BeanContainerException;

	/**
	 * 
	 */
	public abstract void runBeans();

	/**
	 * @param beanProperty
	 * @param value
	 * @return
	 * @throws BeanContainerException
	 */
	public abstract Object setBeanProperty(String beanProperty, Object value) throws BeanContainerException;

	public abstract void setClassLoader(final ClassLoader classLoader);

	public abstract void setConfiguration(final String configurationFile) throws BeanContainerException;

	/**
	 * @param class1
	 * @param parameter
	 * @throws BeanContainerException
	 */
	public abstract void setConfigurationByResource(Class<? extends Object> class1, String parameter)
			throws BeanContainerException;

	/**
	 * @param booleanParameter
	 */
	public abstract void setDebug(boolean booleanParameter);

	/**
	 * @param data
	 */
	public abstract void setPostConfigData(Object data);

	/**
	 * @param data
	 */
	public abstract void setPreConfigData(Object[] data);

	public abstract void setValidate();

	/**
	 * @param b
	 * @param url
	 */
	public abstract void setValidate(boolean b, URL url);

	/**
	 * 
	 */
	public abstract void showBeans();

	/**
	 * TODO this and it's fellow must be matched up with the definitions in
	 * Darwin
	 */
	public static final String OPT_GENERATIONS = "gen"; //$NON-NLS-1$

	public static final String OPT_DTD = "dtd"; //$NON-NLS-1$

	public static final String OPT_DEBUG = "d"; //$NON-NLS-1$

	public static final String OPT_VALIDATE = "v"; //$NON-NLS-1$

	public static final String OPT_CONFIG_FILE = "configFile"; //$NON-NLS-1$

	public static final String OPT_CONFIG_CLASS = "configClass"; //$NON-NLS-1$

	public static final String OPT_JAR = "jar"; //$NON-NLS-1$

	public static final String CONFIG_OUT_OF_THE_BOX = "outOfTheBox.xml"; //$NON-NLS-1$

	public static final String BEAN_APPLET = "Applet"; //$NON-NLS-1$

	public static final String OPT_SHOW_BEANS = "showbeans"; //$NON-NLS-1$

}
