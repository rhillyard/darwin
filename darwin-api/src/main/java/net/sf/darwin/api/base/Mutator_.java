/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mutator_.java
 * Created on Feb 7, 2007
 * @version $Revision: 1.22 $
 */

package net.sf.darwin.api.base;

import java.util.Map;

import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mutator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class to define the base methods and fields for instances of
 * {@link Mutator} interface.
 * 
 * @author Robin Hillyard
 */
@SuppressWarnings("serial")
public abstract class Mutator_<V> extends Base implements Mutator<V> {

	/**
	 * Protected constructor for a mutating Mutator.
	 * 
	 */
	protected Mutator_() {
		this(false);
	}

	/**
	 * Protected constructor.
	 * 
	 * @param identity
	 *            true only if the new Mutator does not change anything
	 */
	protected Mutator_(final boolean identity) {
		super();
		this._identity = identity;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Mutator_ other = (Mutator_) obj;
		if (isIdentity() != other.isIdentity())
			return false;
		return true;
	}

	/**
	 * Return the class's simple name (since these are normally singleton
	 * instances we don't really need to distinguish them).
	 * 
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getClass().getSimpleName();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isIdentity() ? 1231 : 1237);
		return result;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Mutator#isIdentity()
	 */
	@Override
	public boolean isIdentity() {
		return this._identity;
	}

	/**
	 * By default, the allele is returned unchanged.
	 * 
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Mutator#mutate(net.sf.darwin.core.Allele)
	 */
	@Override
	public Allele<V> mutate(final Allele<V> allele) {
		return allele;
	}

	/**
	 * Feature Envy: this method should not be here in a Mutator
	 * 
	 * By default, the copy of the genome is returned, such that the individual
	 * genes have been processed using the {@link #mutate(Allele)} method. To be
	 * precise, the result is the result of invoking
	 * {@link Genome#mutate(Mutator)}.
	 * 
	 * This implementation of the method <b>never</b> returns a reference to the
	 * genome, it is always a copy (although <code>result.equals(genome)</code>
	 * may still be true).
	 * 
	 * @see net.sf.darwin.core.Mutator#mutate(net.sf.darwin.core.Genome)
	 */
	@Override
	public <U> Genome<U, V> mutate(final Genome<U, V> genome) {
		return genome.mutate(this);
	}

	/**
	 * @see net.sf.darwin.core.Mutator#normalize(net.sf.darwin.core.Genome,
	 *      net.sf.darwin.core.Genome)
	 */
	@Override
	public <U> void normalize(final Genome<U, V> genome, final Genome<U, V> reference) {
		genome.addGenes(reference.invert());
	}

	/**
	 * Do nothing. In order to do something useful, a class must override this
	 * method.
	 * 
	 * @see net.sf.darwin.core.Mutator#simplify(net.sf.darwin.core.Genome,
	 *      Expresser<U>)
	 */
	@Override
	public <U> boolean simplify(final Genome<U, V> genome, final Map<Locus<V>, Expresser<U>> expresserMap) {
		return false; // do nothing
	}

	/**
	 * Method to test whether two genes are complimentary. The two genes must
	 * belong to the same locus.
	 * 
	 * XXX this doesn't truly make sense genetically (two genes don't generally
	 * belong to the same locus). But it makes some sense in the context of
	 * mutation because genes can be repeated.
	 * 
	 * @param prior
	 * @param gene
	 * @param expresser
	 *            TODO
	 * @return true if the genes are complimentary.
	 * @throws DarwinException
	 *             if the loci are different
	 */
	protected <U> boolean isComplementary(final Gene<V> prior, final Gene<V> gene, final Expresser<U> expresser) {
		final Locus<?> locus = gene.getLocus();
		if (prior.getLocus().equals(locus)) {
			if (expresser != null)
				return expresser.isComplementary(locus, prior.getAlleleKey(0), gene.getAlleleKey(0));
			return false;
		}
		throw new DarwinException("Mutator.isComplementary(): different loci"); //$NON-NLS-1$
	}

	private final boolean _identity;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Mutator_.class);

	/**
	 * @param allele
	 */
	protected static <X> void addToLocusIfUnknown(final Allele<X> allele) {
		final Locus<X> locus = allele.getLocus();
		if (!locus.getAlleleValues().contains(allele))
			locus.addAllele(allele);
	}

}
