/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.api.impl;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.base.Mate_;
import net.sf.darwin.api.base.Organism_;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.MateFactory;
import net.sf.darwin.api.factory.NuclearFactory;
import net.sf.darwin.api.factory.OrganismFactory;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Lek;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Nuclear;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Sexual;
import net.sf.tostring0.ToString;

/**
 * <p>
 * This concrete class represents an individual organism within a Taxon . It
 * contains a Genome from which may be derived a Phenotype in the context of a
 * Genomic.
 * </p>
 * Adds the following property to {@link Organism_}:
 * <dl>
 ** 
 * <dt>sex</dt>
 * <dd>accessible by calling {@link #isFemale()}</dd>;
 * </dl>
 * 
 * <p>
 * Renamed 2007/02/07 from Organism_Default to Organism_Sexual
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.47 $
 */
public class Organism_Sexual<E, P, G> extends Organism_<E, P, G> implements Sexual {

	/**
	 * Package-level constructor (only invoked by OrganismFactory)
	 * 
	 * @param identifier
	 * @param colony
	 * @param nuclear
	 */
	public Organism_Sexual(final String identifier, final Colony<E, P, G> colony, final Nuclear<P, G> nuclear) {
		super(identifier, colony, nuclear);
	}

	/**
	 * Create a {@link Mate_} object based on this {@link Organism} [provided
	 * that this is male] and the given female. If this is female, we return
	 * null.
	 * 
	 * @param chooser
	 * @param female
	 * @return if this is male, then a newly constructed Mate based on this
	 *         organism and this organism's desirability as a mate; otherwise
	 *         null.
	 * @see net.sf.darwin.core.Organism#createMate(net.sf.darwin.core.MateChoice,
	 *      net.sf.darwin.core.Organism)
	 */
	@Override
	public Mate_ createMate(final MateChoice chooser, final Organism<E, P, G> female) {
		if (!isFemale())
			return MateFactory.createMate(this, chooser.getDesirabilityIndex(female, this));
		return null;
	}

	/**
	 * If this object is a female and viable, create a pair bond from either
	 * population or alternativePopulation.
	 * 
	 * @param chooser
	 * @param alternativeLek
	 * 
	 * @return if this is a female, then a new Mating object, otherwise null.
	 * @see net.sf.darwin.core.Organism#createPairBond(net.sf.darwin.core.MateChoice,
	 *      Lek_Standard, Lek_Standard)
	 */
	@Override
	public Mating createPairBond(final MateChoice chooser, final Lek<E, P, G> lek, final Lek<E, P, G> alternativeLek) {
		if (isFemale() && isViable())
			return chooser.createPairBond(this, lek, alternativeLek);
		return null;
	}

	/**
	 * This implementation of the method yields no progeny because we normally
	 * expect sexual organisms to reproduce only sexually. If you need
	 * Dolly-style cloning, then you can override this method.
	 * 
	 * @param howMany
	 *            ignored
	 * @return an empty collection.
	 */
	@Override
	public Collection<Organism<E, P, G>> getAsexualProgeny(final int howMany) {
		return new ArrayList<>();
	}

	/**
	 * @return value of {@link Sexual#isFemale()} applied to the result of
	 *         invoking {@link #getNuclear()}.
	 * @see net.sf.darwin.core.Sexual#isFemale()
	 */
	@Override
	@ToString(omit = true)
	public boolean isFemale() {
		final Nuclear<P, G> nuclear = getNuclear();
		// TODO consider doing this via polymorphism
		if (nuclear instanceof Sexual) {
			return ((Sexual) nuclear).isFemale();
		}
		return false; // XXX this shouldn't occur since sexual organisms should
		// only embody zygotic cells
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8544786196045696110L;

	/**
	 * Class method to create a new Organism_Sexual without parents (for seeding
	 * a population). As a side effect, the resulting Organism is added to the
	 * given population.
	 * 
	 * @param colony
	 *            the population into which the new instance will be placed and
	 *            from which the genomic is taken.
	 * @return an instance of Organism_Sexual
	 */
	public static <E, U, W> Organism<E, U, W> seed(final Colony<E, U, W> colony) {
		final String identifier = colony.createIdentifier();
		final Genome<U, W> genome = GenomeFactory.<U, W> makeZygote(colony.getPopulation().getTaxon().getGenomic());
		final Nuclear<U, W> neonate = NuclearFactory.<U, W> makeZygote(genome, colony.getIdentifier());
		final Organism<E, U, W> organism = OrganismFactory.<E, U, W> makeOrganism(neonate, colony, identifier);
		colony.addOrganism(organism);
		if (LOG.isDebugEnabled())
			LOG.debug("Organism_Sexual.seed(): new organism: " + organism); //$NON-NLS-1$
		return organism;
	}

}
