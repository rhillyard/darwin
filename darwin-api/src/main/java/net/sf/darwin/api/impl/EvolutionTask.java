/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EvolutionTask.java
 * Created on Jul 26, 2009
 * @version $Revision: 1.23 $
 */

package net.sf.darwin.api.impl;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.api.base.Evolution_;
import net.sf.darwin.api.base.Evolver_;
import net.sf.darwin.core.ClockWatcher;
import net.sf.darwin.core.Clocked;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.Evolver;
import net.sf.darwin.core.GenerationListener;
import net.sf.darwin.core.Lifecycle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class represents an evolutionary event involving the various
 * {@link Evolvable} objects passed in through the constructor.
 * 
 * TODO consider extending AToString
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = false)
public class EvolutionTask implements Runnable, Clocked {

	/**
	 * Secondary constructor for a new EvolutionTask (i.e. with the clock
	 * starting at zero).
	 * 
	 * XXX this is not currently used.
	 * 
	 * @param evolver
	 *            XXX
	 * @param evolvables
	 *            the map of evolvables/integers
	 * @param listeners
	 *            the set of listeners
	 * 
	 *            TEST
	 * 
	 */
	public EvolutionTask(final Evolver evolver, final Map<Evolvable, Integer> evolvables,
			final Collection<GenerationListener> listeners) {
		this(evolver, evolvables, listeners, 0L, null);
	}

	/**
	 * Primary constructor for EvolutionTask. This constructor is used directly
	 * if an evolution has been interrupted by, for example, calling stop or
	 * next. An evolution where pause was called should be resumed.
	 * 
	 * @param evolver
	 *            XXX
	 * @param evolvables
	 *            the map of evolvables/integers
	 * @param listeners
	 *            the set of listeners
	 * @param clock
	 *            the initial value of clock (normally 0 for a new task).
	 * @param clockWatcher
	 *            XXX
	 * 
	 */
	public EvolutionTask(final Evolver evolver, final Map<Evolvable, Integer> evolvables,
			final Collection<GenerationListener> listeners, final long clock, final ClockWatcher clockWatcher) {
		super();
		this._evolver = evolver;
		this.clock = clock;
		this._listeners = listeners;
		this._evolvables = evolvables;
		this._clockWatcher = clockWatcher;
	}

	/**
	 * @return {@link #clock}
	 * @see net.sf.darwin.core.Clocked#getClock()
	 */
	@Override
	public synchronized long getClock() {
		return this.clock;
	}

	/**
	 * @return the keys to the set of {@link Evolvable} objects.
	 */
	public synchronized Set<Evolvable> getEvolvableKeys() {
		return getEvolvables().keySet();
	}

	/**
	 * @return {@link #_evolver}
	 */
	public Evolver getEvolver() {
		return this._evolver;
	}

	/**
	 * @return true if we are in {@link #paused} state.
	 */
	public synchronized boolean isPaused() {
		return this.paused;
	}

	/**
	 * If not {@link #isPaused()}, then invoke {@link #tick()}.
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		if (!isPaused()) {
			try {
				final boolean ok = tick();
				if (!ok && LOG.isDebugEnabled())
					LOG.debug("Evolution task finished and canceled"); //$NON-NLS-1$
				if (!ok)
					((Evolution_) getEvolver()).shutdown();
			} catch (final Exception e) {
				final String message = "Evolution exception while running thread " + Thread.currentThread().getName(); //$NON-NLS-1$
				LOG.fatal(message, e);
				System.err.println(message + ": " + e.getLocalizedMessage()); //$NON-NLS-1$
			}
		}
	}

	/**
	 * @param paused
	 */
	public synchronized void setPaused(final boolean paused) {
		this.paused = paused;
	}

	/**
	 * Invoke one tick of this {@link EvolutionTask}'s clock.
	 * 
	 * @return true if this task has more work to do; false if we're done.
	 * @throws EvolutionException
	 */
	public synchronized boolean tick() throws EvolutionException {
		// First we increment the clock
		this.clock++;

		noteTime();

		if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format(LOG_MSG_2, Long.valueOf(getClock())));
		// Next, we process each of the evolvables in no particular order
		for (final Evolvable evolvable : getEvolvableKeys())
			processEvolvable(evolvable);

		// Next, we remove any evolvables that are finished
		final Iterator<Evolvable> iterator = getEvolvableKeys().iterator();
		while (iterator.hasNext()) {
			if (iterator.next().isFinished())
				iterator.remove();
		}

		// Finally, we should check to see if there are any evolvables left.
		if (getEvolvables().size() == 0) {
			notifyListeners(null);
			return false;
		}

		return true;
	}

	/**
	 * @return {@link #_clockWatcher}
	 */
	private ClockWatcher getClockWatcher() {
		return this._clockWatcher;
	}

	/**
	 * @return {@link #_evolvables}
	 */
	private Map<Evolvable, Integer> getEvolvables() {
		return this._evolvables;
	}

	/**
	 * @return {@link #_listeners}
	 */
	private Collection<GenerationListener> getListeners() {
		return this._listeners;
	}

	/**
	 * @throws EvolutionException
	 */
	private void noteTime() throws EvolutionException {
		if (getClockWatcher() != null) {
			if (getEvolver() instanceof Evolution_Calendar)
				getClockWatcher().onTick(((Evolution_Calendar) getEvolver()).getTime());
		}
		if (getEvolver() instanceof Evolution_Calendar)
			LOG.info("Evolutionary time: " + Evolver_.showTime(((Evolution_Calendar) getEvolver()).getTime())); //$NON-NLS-1$
		else
			LOG.info("Evolutionary ticks: " + getEvolver().getClock()); //$NON-NLS-1$

	}

	/**
	 * @param evolvable
	 *            the {@link Evolvable} object to tell our listeners about.
	 * 
	 */
	private void notifyListeners(final Evolvable evolvable) {
		for (final GenerationListener listener : getListeners())
			listener.onGeneration(evolvable);
	}

	/**
	 * Process the evolver. If it is finished, then we remove it from the list.
	 * If an error occurs, then system error.
	 * 
	 * @param evolvable
	 * @throws EvolutionException
	 */
	private void processEvolvable(final Evolvable evolvable) throws EvolutionException {
		final Integer ticks = getEvolvables().get(evolvable);
		if (ticks != null) {
			if (getClock() % ticks.intValue() == 0) {
				if (LOG.isDebugEnabled())
					LOG.debug(MessageFormat.format(LOG_MSG_1, evolvable.getIdentifier(),
							Integer.valueOf(evolvable.getGeneration())));
				if (evolvable.nextGeneration())
					notifyListeners(evolvable);
				else if (!evolvable.isFinished())
					// FIXME determine why we sometimes (rarely) come here:
					// believed to be because of Colony_:686 no mating pairs...
					throw new EvolutionException("Generation failure (population is finished but evolvable is not): " + evolvable); //$NON-NLS-1$
			}
		} else
			throw new EvolutionException("EvolutionTask.processEvolvable() logic error: evolvable  " + evolvable + " not found"); //$NON-NLS-1$//$NON-NLS-2$
	}

	private final Evolver _evolver;

	private final ClockWatcher _clockWatcher;

	/**
	 * 
	 */
	private static final String LOG_MSG_2 = "EvolutionTask.tick: clock now at {0}"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String LOG_MSG_1 = "call nextGeneration for {0} generation {1}"; //$NON-NLS-1$

	protected final static Log LOG = LogFactory.getLog(EvolutionTask.class);

	private transient boolean paused = false;

	private transient long clock;

	/**
	 * A map of Evolvable objects, each with the number of ticks between
	 * generations. The evolutionary process proceeds one tick at a time, but
	 * not every evolvable will undergo a new generation for every tick. For
	 * instance, the 13-year and 17-year cicadas would have tick values of 13
	 * and 17 assuming that the Evolution proceeded one tick per year.
	 */
	private final Map<Evolvable, Integer> _evolvables;

	private final Collection<GenerationListener> _listeners;

}
