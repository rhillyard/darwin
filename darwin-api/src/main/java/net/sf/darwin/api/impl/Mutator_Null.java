/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mutator_Null.java
 * Created on Feb 7, 2007
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Mutator_;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Mutator;

/**
 * This class defines the "null" mutator, ie. an implementation of
 * {@link Mutator} which never changes any alleles.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public final class Mutator_Null<V> extends Mutator_<V> {

	/**
	 * Public constructor.
	 */
	public Mutator_Null() {
		super(true);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8581384192914666253L;

}
