/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Sink_.java
 * Created on Sep 16, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.base;

import java.io.IOException;

import net.sf.darwin.core.Sink;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class Sink_ implements Sink {

	/**
	 * 
	 */
	protected Sink_() {
		super();
	}

	/**
	 * @return result of calling {@link #append(CharSequence)} with parameter
	 *         <code>c + ""</code>.
	 * @see java.lang.Appendable#append(char)
	 */
	@Override
	public Appendable append(final char c) throws IOException {
		return append(c + ""); //$NON-NLS-1$
	}

	/**
	 * @return result of calling {@link #append(CharSequence)} with parameter
	 *         <code>csq.subSequence(start, end)</code>.
	 * @see java.lang.Appendable#append(java.lang.CharSequence, int, int)
	 */
	@Override
	public Appendable append(final CharSequence csq, final int start, final int end) throws IOException {
		return append(csq.subSequence(start, end));
	}

	/**
	 * Do nothing
	 * 
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() throws IOException {
		// Do nothing
	}

	/**
	 * Do nothing
	 * 
	 * @see java.io.Flushable#flush()
	 */
	@Override
	public void flush() throws IOException {
		// Do nothing
	}

	protected final static Log LOG = LogFactory.getLog(Sink_.class);

	/**
	 * This utility method sends a message to the given sink (if non-null)
	 * otherwise it logs it using the info method. In the event that appends it
	 * to the sink, it will additionally log it using the debug method,
	 * providing that info is not enabled.
	 * 
	 * TODO why not just send it to debug?
	 * 
	 * @param message
	 *            the message to output
	 * @param sink
	 *            the sink to which the message will be sent (may be null)
	 * @param log
	 *            the logger of the calling class
	 */
	public static void sinkOrLog(final String message, final Appendable sink, final Log log) {
		try {
			if (sink != null) {
				sink.append(message + Sink.NEWLINE);
				if (!log.isInfoEnabled())
					log.debug(message);
			} else
				log.info(message);
		} catch (final IOException e) {
			LOG.error("append exception", e); //$NON-NLS-1$
		}
	}

}
