/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003,2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_Standard.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.api.base;

import java.io.PrintWriter;

/**
 * 
 * Class to provide a default implementation of {@link Census_Sink}
 * 
 * TODO do we really need this class? No it is obsolete!
 * 
 * @author Robin Hillyard
 */
public class Census_Darwinian extends Census_Sink {

	/**
	 * @param printWriter
	 */
	public Census_Darwinian(final PrintWriter printWriter) {
		super(printWriter);
	}

}