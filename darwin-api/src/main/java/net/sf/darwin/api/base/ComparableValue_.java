/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ComparableValue_.java
 * Created on Jan 4, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.api.util.MathUtil;
import net.sf.darwin.core.ComparableValue;
import net.sf.darwin.core.Valuable;
import net.sf.darwin.core.ValueException;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class ComparableValue_ implements ComparableValue {

	/**
	 * 
	 */
	public ComparableValue_() {
		super();
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final Valuable o) {
		try {
			return compareTo(this, o);
		} catch (final ValueException e) {
			throw new RuntimeException("ComparableValue.comparetTo() value exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @param o1
	 * @param o2
	 * @return -1, 0, or 1 according as o1 is less than, equal, or greater than
	 *         o2
	 * @throws ValueException
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public static int compareTo(final Valuable o1, final Valuable o2) throws ValueException {
		if (o1 == null && o2 == null)
			return 0;
		if (o1 == null)
			return -1;
		if (o2 == null)
			return 1;
		return MathUtil.compare(o1.getValue(), o2.getValue());
	}

}
