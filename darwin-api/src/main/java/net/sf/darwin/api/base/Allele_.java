/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.base;

import java.util.Collection;

import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.Detail;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * This abstract provides base operations for implementations of {@link Allele},
 * which are specific, competing genes which may appear at a {@link Locus}.
 * </p>
 * <p>
 * In general, an allele can have a key and a value, both being objects. If the
 * key is null, then the value, reduced to a String, serves as its key.
 * Normally, known alleles are defined via configuration. But new alleles
 * (mutants) can arise during an evolution. For these, there is an
 * {@link AlleleFactory}.
 * </p>
 * <p>
 * In all cases, however, an Allele is immutable regarding its value. Once
 * constructed, the value cannot be changed. The value of the (parent) locus is
 * set once and once only.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.31 $
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Allele
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
abstract public class Allele_<G> extends Attribute_<String, G> implements Allele<G> {

	/**
	 * Constructor of an Allele_ based on a specific value. The key will be the
	 * value, in the form of a String.
	 * 
	 * XXX this constructor is used only by unit tests.
	 * 
	 * @param value
	 *            the value for the new Allele_, as an Object.
	 * 
	 */
	protected Allele_(final G value) {
		this(value.toString(), value);
	}

	/**
	 * Primary Private constructor of an Allele_ based on a specific value.
	 * 
	 * @param key
	 *            the key for the new Allele_, as an Object.
	 * @param value
	 *            the value for the new Allele_, as an Object.
	 */
	protected Allele_(final String key, final G value) {
		super(key, value);
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present(getLocus().getIdentifier(), context);
		return false;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return null;
	}

	/**
	 * Return the value of {@link #locus}.
	 * 
	 * @see net.sf.darwin.core.Allele#getLocus()
	 */
	@Override
	public Locus<G> getLocus() {
		return this.locus;
	}

	/**
	 * @see net.sf.darwin.core.CacheSignature#getSignature()
	 */
	@Override
	public String getSignature() {
		return getBases();
	}

	/**
	 * Set the value of the locus for this Allele. This is not set in the
	 * constructor.
	 * 
	 * TODO why is it not set in the constructor?
	 * 
	 * @see net.sf.darwin.core.Allele#setLocus(net.sf.darwin.core.Locus)
	 */
	@Override
	public void setLocus(final Locus<G> locus) {
		if (this.locus == null)
			this.locus = locus;
		else if (this.locus != locus)
			LOG.info("Allele.setLocus(): already set"); //$NON-NLS-1$
	}

	/**
	 * Set the number of characters that we should see when we create a string
	 * of this {@link Allele}. By default, the number is 1. We only increase it
	 * to N+1 if there are allele identifiers that share the first N characters.
	 * 
	 * @param stringChars
	 *            the stringChars to set
	 */
	public void setStringChars(final int stringChars) {
		this.stringChars = stringChars;
	}

	/**
	 * @see net.sf.darwin.api.base.Attribute_#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(final Object value) {
		throw new DarwinException("Allele.setValue(): immutable"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.api.base.Attribute_#toString(net.sf.tostring0.Detail)
	 */
	@Override
	public String toString(final Detail detail) {
		if (detail.isShowDetail()) {
			// TODO consider using the normal audit utilities
			return "Allele " + getAttribute() + ": " + getValue(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return getIdentifier().substring(0, getStringChars());
	}

	/**
	 * @return the stringChars
	 */
	protected int getStringChars() {
		return this.stringChars;
	}

	private int stringChars = 1;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Allele_.class);

	private transient Locus<G> locus;

}
