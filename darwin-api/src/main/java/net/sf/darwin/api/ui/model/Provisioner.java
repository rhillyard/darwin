/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Provisioner
 * Created on: Oct 12, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * This is a very general interface which defines a method to provision a type
 * of object.
 * 
 * @author rhillya
 * 
 */
public interface Provisioner<T> {

	/**
	 * @param parameters
	 *            a set of optional parameters which can influence the
	 *            properties of the result
	 * @return a newly provisioned instance of T
	 */
	public abstract T provision(Object... parameters);
}
