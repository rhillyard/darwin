/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003,2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngine_Simple.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FitnessEngine_;
import net.sf.darwin.core.FitnessEngine;

import com.rubecula.jexpression.Evaluator;

/**
 * Default implementation of {@link FitnessEngine} interface. There are no
 * fitnesses registered in this engine after construction. In order for the
 * engine to be used, at least one fitness pair must be registered.
 * 
 * @author Robin Hillyard
 * 
 */
public final class FitnessEngine_Simple extends FitnessEngine_ {

	/**
	 * 
	 */
	public FitnessEngine_Simple() {
		super(null, null);
	}

	/**
	 * @see net.sf.darwin.api.base.FitnessEngine_#registerFitnesses(com.rubecula.jexpression.Evaluator)
	 */
	@Override
	protected void registerFitnesses(final Evaluator evaluator) {
		// no fitnesses to register
	}
}