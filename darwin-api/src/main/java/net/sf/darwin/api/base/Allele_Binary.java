/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Allele_Binary.java
 * Created on Oct 31, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.EitherOr;
import net.sf.tostring0.ToString;

/**
 * Abstract type of allele which has two values, based on a boolean.
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Allele_Binary extends Allele_<Boolean> implements EitherOr {

	/**
	 * @param identifier
	 * @param value
	 */
	public Allele_Binary(final String identifier, final boolean value) {
		this(identifier, value, null);
	}

	/**
	 * @param identifier
	 * @param value
	 * @param bases
	 *            a specific array of bases, or null
	 */
	public Allele_Binary(final String identifier, final boolean value, final String[] bases) {
		super(identifier, Boolean.valueOf(value));
		if (bases != null)
			setBases(bases);
	}

	/**
	 * TODO consider changing the name of this method since it is not a pure
	 * getter. OTOH, it is defined in an interface.
	 * 
	 * @return either {@link #_bases} sub 0 or sub 1, according to which allele
	 *         we are.
	 * @see net.sf.darwin.core.Allele#getBases()
	 */
	@Override
	public String getBases() {
		return this._bases[getValueAsBoolean() ? 0 : 1];
	}

	/**
	 * TODO this is unnecessary
	 * 
	 * @see net.sf.darwin.core.EitherOr#getValueAsBoolean()
	 */
	@Override
	public boolean getValueAsBoolean() {
		// This cast should be OK since this is an immutable type
		return getValue().booleanValue();
	}

	/**
	 * Invoked by reflection.
	 * 
	 * @param bases
	 *            the base strings for the true- and false- valued alleles
	 */
	public void setBases(final String[] bases) {
		this._bases = bases;
	}

	/**
	 * @see net.sf.darwin.api.base.Attribute_#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(final Boolean value) {
		throw new UnsupportedOperationException("Allele_Binary is immutable"); //$NON-NLS-1$
	}

	/**
	 * This should normally be reset by calling {@link #setBases(String[])}.
	 * Clearly these are not real bases.
	 */
	private String[] _bases = new String[] { "T", "F" }; //$NON-NLS-1$ //$NON-NLS-2$

}
