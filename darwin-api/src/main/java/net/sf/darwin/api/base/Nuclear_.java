/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Nuclear_.java
 * Created on Feb 20, 2009
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Nuclear;
import net.sf.tostring0.ToString;

/**
 * TODO need a better name for this Nuclear concept. We are trying to represent
 * something which can reproduce either asexually (e.g. bacteria), or sexually
 * (e.g. animals).
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
@ToString(showIdentifier = false, showClass = false, allBeanMethods = false)
public abstract class Nuclear_<P, G> extends Base implements Nuclear<P, G>, Cloneable {

	/**
	 * Protected Primary Constructor
	 * 
	 * @param genome
	 * @param populationTag
	 */
	protected Nuclear_(final Genome<P, G> genome, final String populationTag) {
		super();
		this._genome = genome;
		this.colonyId = populationTag;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present(getColonyId(), context);
		return true;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		final Nuclear_<P, G> clone = (Nuclear_<P, G>) super.clone();
		@SuppressWarnings("unchecked")
		final Genome_<P, G> genome = (Genome_<P, G>) ((Genome_<P, G>) clone._genome).clone();
		clone._genome = genome;
		return clone;
	}

	/**
	 * @return true if colonyId and genome are equal.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		// TODO consider doing this via polymorphism (or use standard equals
		// method instead)
		if (obj instanceof Nuclear) {
			return getColonyId().equals(((Nuclear<?, ?>) obj).getColonyId())
					&& getGenome().equals(((Nuclear<?, ?>) obj).getGenome());
		}
		return false;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		final Genome<P, G> genome = getGenome();
		final Collection<Censusible> result = new ArrayList<>();
		result.add(genome);
		return result;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Nuclear#getColonyId()
	 */
	@Override
	@ToString
	public String getColonyId() {
		return this.colonyId;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Genetic#getGenome()
	 */
	@Override
	@ToString(detail = 1)
	public Genome<P, G> getGenome() {
		return this._genome;
	}

	/**
	 * @return combination of hash codes for colonyId and genome.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getGenome().hashCode() ^ getColonyId().hashCode();
	}

	/**
	 * XXX consider eliminating this. Is there ever a time when we need to
	 * change the colonyId of a Nuclear object? I don't think so.
	 * 
	 * @see net.sf.darwin.core.Nuclear#setColonyId(java.lang.String)
	 */
	@Override
	public void setColonyId(final String id) {
		this.colonyId = id;
	}

	/**
	 * This should be final, except that clone() needs to set it.
	 */
	private Genome<P, G> _genome;

	/**
	 * This property is not final because there might be some cases where a
	 * cellular object migrates from one colony to another.
	 */
	private String colonyId;
}
