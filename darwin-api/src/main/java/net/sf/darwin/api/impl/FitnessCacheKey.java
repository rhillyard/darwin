/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessCacheKey.java
 * Created on Oct 2, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.TraitMap;
import net.sf.tostring0.Identifiable;

/**
 * This class is used to form the key for the FitnessCache_Standard
 * 
 * CONSIDER remove engine as one of the factors
 * 
 * @author Robin Hillyard
 * 
 */
public class FitnessCacheKey implements Identifiable {

	/**
	 * @param traits
	 *            the traits that will form part of our fitness cache key
	 * @param engine
	 *            the fitness engine that will form part of our fitness cache
	 *            key
	 * @param environment
	 *            the environment
	 * 
	 */
	public FitnessCacheKey(final TraitMap traits, final FitnessEngine engine, final Environment environment) {
		super();
		this._traits = traits;
		this._engine = engine;
		this._environment = environment;
	}

	/**
	 * Use equality for the {@link #_engine} directly but for the
	 * {@link #_traits}, get the signature and check its equality.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final FitnessCacheKey other = (FitnessCacheKey) obj;
		if (this._engine == null) {
			if (other._engine != null)
				return false;
		} else if (!this._engine.getSignature().equals(other._engine.getSignature()))
			return false;
		if (this._environment == null) {
			if (other._environment != null)
				return false;
		} else if (!this._environment.getSignature().equals(other._environment.getSignature()))
			return false;
		if (this._traits == null) {
			if (other._traits != null)
				return false;
		} else if (!this._traits.getSignature().equals(other._traits.getSignature()))
			return false;
		return true;
	}

	/**
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._traits.getSignature() + "+" + this._engine.getSignature() + "+" + this._environment.getSignature(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Use the hash code for the {@link #_engine} directly but for the
	 * {@link #_traits}, get the signature and use its hash code.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._engine == null) ? 0 : this._engine.getSignature().hashCode());
		result = prime * result + ((this._environment == null) ? 0 : this._environment.getSignature().hashCode());
		result = prime * result + ((this._traits == null) ? 0 : this._traits.getSignature().hashCode());
		return result;
	}

	/**
	 * TODO consider to use audit utilities
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getIdentifier();
	}

	private final Environment _environment;

	private final TraitMap _traits;

	private final FitnessEngine _engine;

}
