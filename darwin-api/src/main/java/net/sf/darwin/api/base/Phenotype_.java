/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Phenotype_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.35 $
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.darwin.api.impl.TraitMap_ReadOnly;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.ExPhen;
import net.sf.darwin.core.FrequencyMap;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.TraitMap;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class to define the base methods and fields for an implementation of
 * {@link Phenotype}.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            phenotype
 */
@SuppressWarnings("serial")
public abstract class Phenotype_<P> extends TraitMap_<P> implements Phenotype<P> {

	/**
	 * Constructor to create an identified empty Phenotype.
	 * 
	 * @param id
	 *            the identifier for this Phenotype
	 */
	protected Phenotype_(final String id) {
		super();
		this._id = id;
		this._extendedPhenotypes = new ArrayList<>();
	}

	/**
	 * @see net.sf.darwin.core.Phenotype#addExtended(net.sf.darwin.core.ExPhen)
	 */
	@Override
	public void addExtended(final ExPhen<P> expression) {
		getExtendedPhenotypeList().add(expression);
	}

	/**
	 * Add a trait and set it to "belong" to this {@link Phenotype}.
	 * 
	 * @param key
	 * @param trait
	 * @return the key to the new trait (so it can be found in the trait map).
	 * 
	 * @see net.sf.darwin.core.Phenotype#addTrait(java.lang.String,
	 *      net.sf.darwin.core.Trait)
	 */
	@Override
	public String addTrait(final String key, final Trait<P> trait) {
		put(key, trait);
		reParent(trait);
		return key;
	}

	/**
	 * Add a trait using for its key the id of the character to which this trait
	 * belongs.
	 * 
	 * @see net.sf.darwin.core.Phenotype#addTrait(net.sf.darwin.core.Trait)
	 */
	@Override
	public String addTrait(final Trait<P> trait) {
		if (trait != null && trait.getCharacter() != null)
			return addTrait(trait.getCharacterKey(), trait);
		throw new DarwinException("addTrait(): logic error: trait: " + trait); //$NON-NLS-1$
	}

	/**
	 * @param character
	 * @param traitFrequencies
	 */
	@Override
	public void doCensus(final Pharacter<P> character, final FrequencyMap<Trait<P>> traitFrequencies) {
		for (final String traitKey : getKeys()) {
			final Trait<P> trait = getTrait(traitKey);
			if (trait.getCharacterKey().equals(character.getIdentifier()))
				traitFrequencies.add(trait);
		}
	}

	/**
	 * @see net.sf.darwin.api.base.TraitMap_#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Phenotype_ other = (Phenotype_) obj;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Phenotype#getExtendedPhenotypes()
	 */
	@Override
	public Collection<ExPhen<P>> getExtendedPhenotypes() {
		return getExtendedPhenotypeList();
	}

	/**
	 * @return {@link #_id}.
	 * 
	 * @see net.sf.darwin.api.base.TraitMap_#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._id != null ? "Ph" + this._id : null; //$NON-NLS-1$
	}

	/**
	 * @return the sex-linked traits as a TraitMap
	 * @see net.sf.darwin.core.Phenotype#getSexTraits()
	 */
	@Override
	@ToString(omit = true)
	public TraitMap<P> getSexTraits() {
		final TraitMap_<P> result = new TraitMap_ReadOnly<>();
		for (final String key : getKeys()) {
			final Trait<P> trait = getTrait(key);
			if (trait.getCharacter().isSexLinked())
				result.put(key, trait);
		}
		return result;
	}

	/**
	 * @see net.sf.darwin.api.base.TraitMap_#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		return result;
	}

	/**
	 * @return
	 */
	private Collection<ExPhen<P>> getExtendedPhenotypeList() {
		return this._extendedPhenotypes;
	}

	/**
	 * @param trait
	 */
	private void reParent(final Trait<P> trait) {
		trait.setPhenotype(this);
	}

	/**
	 * the list of extended phenotypes for this phenotype.
	 */
	@ToString()
	public final List<ExPhen<P>> _extendedPhenotypes;

	private final String _id;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Phenotype_.class);

}