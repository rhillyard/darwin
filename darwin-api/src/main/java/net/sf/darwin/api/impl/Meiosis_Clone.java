/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Meiosis_Clone.java
 * Created on Jun 23, 2009
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Meiosis_;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Ploidy;

import org.apache.commons.math.random.RandomGenerator;

/**
 * This is a null-meiosis implementation which is used for haploid genomics
 * (where meiosis doesn't make sense) but where we still need a way of cloning
 * genomes.
 * 
 * @author Robin Hillyard
 * 
 */
public final class Meiosis_Clone<G> extends Meiosis_<G> {

	/**
	 * @param random
	 */
	public Meiosis_Clone(final RandomGenerator random) {
		super(random);
	}

	/**
	 * @see net.sf.darwin.api.base.Meiosis_#doMeiosis(net.sf.darwin.core.Gene,
	 *      int)
	 */
	@Override
	public String doMeiosis(final Gene<G> gene, final int index) {
		return gene.getAlleleKey(0);
	}

	/**
	 * This method essentially clones the existing set of genes.
	 * 
	 * @param genomic
	 * @param genes
	 * @param chromosome
	 * 
	 * @see net.sf.darwin.core.Meiosis#doMeiosis(net.sf.darwin.core.Genomic,
	 *      net.sf.darwin.core.Genes, net.sf.darwin.core.Chromosome)
	 */
	@Override
	public <T> Genome<T, G> doMeiosis(final Genomic<T, G> genomic, final Genes<G> genes, final Chromosome<G> chromosome) {
		final int ploidy = genomic.getPloidy();
		if (ploidy == Ploidy.HAPLOID) {
			return doMeiosisChromosome(genomic, genes, chromosome, -1);
		}
		throw new DarwinException("Meiosis_Clone.doMeiosis(): genomic must be haploid"); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4463345475275611077L;

}
