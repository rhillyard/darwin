/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GeneFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.Gene_Diploid;
import net.sf.darwin.api.impl.Gene_Haploid;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Locus;

/**
 * Factory Class to create Gene objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
public final class GeneFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private GeneFactory() {
		super();
	}

	private static final String S_FCTRY_MAKE_GENE = "GeneFactory.makeGene(): "; //$NON-NLS-1$

	/**
	 * Construct a new {@link Gene_Diploid} with the given allele indexes
	 * 
	 * @param locus
	 *            the locus for the new gene
	 * @param allele1
	 *            the key for the 1st allele
	 * @param allele2
	 *            the key for the 2nd allele
	 * 
	 * @return a newly created diploid gene
	 */
	public static <G> Gene<G> makeDiploid(final Locus<G> locus, final String allele1, final String allele2) {
		final Gene<G> result = new Gene_Diploid<>(allele1, allele2);
		result.setLocus(locus);
		return result;
	}

	/**
	 * TEST
	 * 
	 * @param clazz
	 *            the class of Gene to instantiate, which must have a
	 *            constructor of form Gene(Locus,Object[]).
	 * @param locus
	 *            the locus to which the new gene will belong.
	 * @param parameters
	 *            the parameters to pass to the constructor.
	 * @return a newly constructed {@link Gene}.
	 */
	public static <G> Gene<G> makeGene(final Class<? extends Gene<G>> clazz, final Locus<G> locus, final Object... parameters) {
		try {
			final Constructor<? extends Gene<G>> constructor = clazz
					.getConstructor(new Class<?>[] { Locus.class, Object[].class });
			return constructor.newInstance(new Object[] { locus, parameters });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENE + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENE + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENE + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENE + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * @param locus
	 *            the locus for the new gene
	 * @param allele
	 *            the key to the allele
	 * @return a newly constructed haploid gene
	 */
	public static <G> Gene<G> makeHaploid(final Locus<G> locus, final String allele) {
		final Gene<G> result = new Gene_Haploid<>(allele);
		result.setLocus(locus);
		return result;
	}

}
