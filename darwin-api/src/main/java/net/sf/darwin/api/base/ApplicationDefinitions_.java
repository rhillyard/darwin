/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ApplicationDefinitions_.java
 * Created on Apr 22, 2007
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.base;

/**
 * @author Robin Hillyard
 * 
 *         TODO this class is obsolete but still used in unit tests.
 */
public abstract class ApplicationDefinitions_ {

	/**
	 * The default value for the ideal population value: 50.
	 */
	public static final int $IdealPopulationDefault = 50;

	/**
	 * This defines the default value of the seed population in terms of its
	 * ratio to the ideal population: 5
	 */
	public static int $SeedPopulationRatio = 2;

	/**
	 * This defines the default value of the seed population: 20
	 */
	public static int $SeedPopulationDefault = 20;

	/**
	 * Method to look up a String (key) from a list of Strings (keys).
	 * 
	 * @param key
	 *            the String to be looked up.
	 * @param keys
	 *            the array of Strings.
	 * @return the index of the first matching key (if exists), else -1 for no
	 *         match.
	 */
	public static int LookupIndex(final String key, final String[] keys) {
		for (int i = 0; i < keys.length; i++)
			if (key.equalsIgnoreCase(keys[i]))
				return i;
		return -1;
	}

}
