/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MathUtil.java
 * Created on Jan 4, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.util;

/**
 * @author Robin Hillyard
 * 
 */
public class MathUtil {

	/**
	 * a suitable discriminant for comparing numeric values.
	 */
	public static final double EPSILON = 0.00000001;

	/**
	 * Utility class method to compare two values with a tolerance of
	 * <code>epsilon</code>. If the difference between <code>x</code> and
	 * <code>y</code> is less than <code>epsilon</code>, 0 is returned, else we
	 * return the result of {@link Double#compare(double, double)} for
	 * <code>x</code> and <code>y</code>.
	 * 
	 * @param x
	 * @param y
	 * @return -1, 0 or 1 as x is less than, equal to or greater than y.
	 */
	public static int compare(final double x, final double y) {
		if (java.lang.Math.abs(x - y) < EPSILON)
			return 0;
		return Double.compare(x, y);
	}

	/**
	 * Utility class method to compare two values with a tolerance of
	 * <code>epsilon</code>. If the difference between <code>x</code> and
	 * <code>y</code> is less than <code>epsilon</code>, 0 is returned, else we
	 * return the result of {@link Double#compare(double, double)} for
	 * <code>x</code> and <code>y</code>.
	 * 
	 * @param x
	 * @param y
	 * @param epsilon
	 * @return -1, 0 or 1 as x is less than, equal to or greater than y.
	 */
	public static int compare(final double x, final double y, final double epsilon) {
		if (java.lang.Math.abs(x - y) < epsilon)
			return 0;
		return Double.compare(x, y);
	}

	/**
	 * Utility class method to compare two values with a tolerance of
	 * <code>epsilon</code>. If the difference between <code>x</code> and
	 * <code>y</code> is less than <code>epsilon</code>, 0 is returned, else we
	 * return the result of {@link Double#compare(double, double)} for
	 * <code>x</code> and <code>y</code>.
	 * 
	 * @param x
	 * @param y
	 * @return -1, 0 or 1 as x is less than, equal to or greater than y.
	 */
	public static int compare(final Number x, final Number y) {
		return compare(x.doubleValue(), y.doubleValue(), EPSILON);
	}

	/**
	 * Utility class method to compare two values with a tolerance of
	 * <code>epsilon</code>. If the difference between <code>x</code> and
	 * <code>y</code> is less than <code>epsilon</code>, 0 is returned, else we
	 * return the result of {@link Double#compare(double, double)} for
	 * <code>x</code> and <code>y</code>.
	 * 
	 * @param x
	 * @param y
	 * @param epsilon
	 * @return -1, 0 or 1 as x is less than, equal to or greater than y.
	 */
	public static int compare(final Number x, final Number y, final double epsilon) {
		return compare(x.doubleValue(), y.doubleValue(), epsilon);
	}

}
