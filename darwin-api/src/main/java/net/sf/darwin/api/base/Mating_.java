/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mating_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.31 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.NuclearFactory;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Nuclear;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.random.RandomGenerator;

/**
 * Abstract class to define base methods for implementers of {@link Mating}.
 * 
 * @author Robin Hillyard
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Mating_ extends Base implements Mating {

	/**
	 * Protected primary constructor for a new instance of Mating_.
	 * 
	 * @param parent1
	 *            the father-to-be.
	 * @param parent2
	 *            the mother-to-be.
	 * @param random
	 *            the random number generator to be used
	 * @param genomic
	 *            the genomic of the resulting progeny
	 */
	protected Mating_(final Nuclear parent1, final Nuclear parent2, final RandomGenerator random, final Genomic genomic) {
		this._male = parent1;
		this._female = parent2;
		this._random = random;
		this._genomic = genomic;
		this._colonyId = parent2.getColonyId();
		// if (!_ColonyId.equals(parent1.getColonyId()))
		//			LOG.info("*** Mating of organisms of different colonies ***"); //$NON-NLS-1$
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Mating_ other = (Mating_) obj;
		if (getFemale() == null) {
			if (other.getFemale() != null)
				return false;
		} else if (!getFemale().equals(other.getFemale()))
			return false;
		if (getMale() == null) {
			if (other.getMale() != null)
				return false;
		} else if (!getMale().equals(other.getMale()))
			return false;
		if (getColonyId() == null) {
			if (other.getColonyId() != null)
				return false;
		} else if (!getColonyId().equals(other.getColonyId()))
			return false;
		if (getRandom() == null) {
			if (other.getRandom() != null)
				return false;
		} else if (!getRandom().equals(other.getRandom()))
			return false;
		if (getGenomic() == null) {
			if (other.getGenomic() != null)
				return false;
		} else if (getGenomic() != (other.getGenomic())) // more strict
			return false;
		return true;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getFemale() == null) ? 0 : getFemale().hashCode());
		result = prime * result + ((getMale() == null) ? 0 : getMale().hashCode());
		result = prime * result + ((getColonyId() == null) ? 0 : getColonyId().hashCode());
		result = prime * result + ((getRandom() == null) ? 0 : getRandom().hashCode());
		result = prime * result + ((getGenomic() == null) ? 0 : getGenomic().hashCode());
		return result;
	}

	/**
	 * Each of the parents in this {@link Mating} object produce a gamete. These
	 * gametes are then combined using the class method
	 * {@link NuclearFactory#makeZygote(Genome, String)} and the result becomes
	 * the genome for the progeny. The sex of the progeny is chosen randomly.
	 * Finally, the _Female's birth method is invoked to produce a new
	 * Organism_Sexual which is then returned.
	 * 
	 * @see net.sf.darwin.core.Mating#progeny()
	 */
	@Override
	public Nuclear progeny() {
		final Genome gameteM = getGenomic().createGamete(getMale().getGenome());
		final Genome gameteF = getGenomic().createGamete(getFemale().getGenome());
		final Genome genome = GenomeFactory.makeZygote(gameteM, gameteF);
		final Nuclear zygote = NuclearFactory.makeZygote(genome, getColonyId());
		if (LOG.isTraceEnabled())
			LOG.trace("Mating_.progeny(): " + zygote); //$NON-NLS-1$
		return zygote;
	}

	/**
	 * @return the colonyId
	 */
	protected String getColonyId() {
		return this._colonyId;
	}

	/**
	 * @return the female
	 */
	protected Nuclear getFemale() {
		return this._female;
	}

	/**
	 * @return the genomic
	 */
	protected Genomic getGenomic() {
		return this._genomic;
	}

	/**
	 * @return the male
	 */
	protected Nuclear getMale() {
		return this._male;
	}

	/**
	 * @return the random
	 */
	protected RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Mating_.class);

	private final Genomic _genomic;

	/**
	 * The female mate (mother-to-be).
	 */
	@ToString
	private final Nuclear _female;

	/**
	 * The male mate (father-to-be).
	 */
	@ToString
	private final Nuclear _male;

	/**
	 * The tag of the population to which the mother and father belong.
	 */
	@ToString
	private final String _colonyId;

	/**
	 * The randomizer.
	 */
	private final RandomGenerator _random;

}