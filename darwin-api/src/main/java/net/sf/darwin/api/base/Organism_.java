/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Organism_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.58 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Map;

import net.sf.darwin.api.impl.OrganismCensusContext;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessCache;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Mortality;
import net.sf.darwin.core.Nuclear;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Sexual;
import net.sf.darwin.core.Valuable;
import net.sf.darwin.core.ValueException;
import net.sf.darwin.core.Viability;
import net.sf.darwin.core.Visualizable;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class defining the base methods and fields of an implementation of
 * {@link Organism} interface.
 * 
 * TODO this is a huge type file. Consider splitting up.
 * 
 * @author Robin Hillyard
 * 
 */
@ToString(allBeanMethods = false)
public abstract class Organism_<E, P, G> extends Base implements Organism<E, P, G>, Cloneable {

	/**
	 * Construct a new organism with the following attributes:
	 * 
	 * @param identifier
	 *            a string by which to identify this organism (may be null).
	 * @param colony
	 *            the Population to which this specific organism belongs
	 * @param nuclear
	 *            the Genome to which this organism (and all its relatives)
	 *            adheres.
	 */
	protected Organism_(final String identifier, final Colony<E, P, G> colony, final Nuclear<P, G> nuclear) {
		super();
		this._identifier = identifier;
		this.colony = colony;
		this._nuclear = nuclear;
		this.age = 0;
		this.viable = false;
		this._fitnessCache = colony != null ? colony.getRealm().getFitnessCache() : null;
	}

	/**
	 * 
	 * Age this organism by one year.
	 * 
	 * @see net.sf.darwin.core.Mortal#age()
	 */
	@Override
	public void age() {
		this.age++;
	}

	/**
	 * There are two paths through this method: 1) if context is an instance of
	 * {@link OrganismCensusContext}, then we call
	 * {@link #doCensusInContext(OrganismCensusContext)} and consider ourselves
	 * done (no need for going deeper through children); 2) otherwise, we output
	 * information about this {@link Organism} and return true (OK not yet done
	 * -- need to iterate through children).
	 * 
	 * @see net.sf.darwin.core.Censusible#censusMe(Census, java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		// TODO consider doing this via polymorphism
		if (context instanceof OrganismCensusContext) {
			doCensusInContext((OrganismCensusContext) context);
			return false;
		}
		census.present(this.toString(), context);
		return false;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		final Object clone = super.clone();
		((Nuclear_<P, G>) getNuclear()).clone();
		return clone;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final Valuable o) {
		try {
			return ComparableValue_.compareTo(this, o);
		} catch (final ValueException e) {
			throw new RuntimeException("Organism_.compareTo() value exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Organism_<?, ?, ?> other = (Organism_<?, ?, ?>) obj;
		if (getIdentifier() == null) {
			if (other.getIdentifier() != null)
				return false;
		} else if (!getIdentifier().equals(other.getIdentifier()))
			return false;
		if (getNuclear() == null) {
			if (other.getNuclear() != null)
				return false;
		} else if (!getNuclear().equals(other.getNuclear()))
			return false;
		return true;
	}

	/**
	 * @return {@link #age}.
	 * @see net.sf.darwin.core.Mortal#getAge()
	 */
	@Override
	@ToString
	public int getAge() {
		return this.age;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		// final Collection<Censusible> result = new ArrayList<Censusible>();
		// result.add(getNuclear());
		// result.add(getPhenotype());
		// return result;
		return null;
	}

	/**
	 * @return the value of field {@link #colony}.
	 * @see net.sf.darwin.core.Organism#getColony()
	 */
	// @ToString(parent = true)
	@Override
	public Colony<E, P, G> getColony() {
		return this.colony;
	}

	/**
	 * @param fitnessEngine
	 * @return the fitness of this {@link Organism} in its {@link Environment}.
	 * @throws FitnessException
	 * @see net.sf.darwin.core.Organism#getFitness(net.sf.darwin.core.FitnessEngine)
	 */
	@Override
	public Number getFitness(final FitnessEngine fitnessEngine) throws FitnessException {
		// First we get the environment.
		final Environment<E> environment = getColony().getEnvironment();

		// Next we look up the fitness in the cache...
		Double result = getFitnessCache() != null ? getFitnessCache().getFitness(getPhenotype(), fitnessEngine, environment)
				: null;

		// If it's null, then we must calculate it and add it to the cache.
		if (result == null) {
			result = new Double(fitnessEngine.calculateFitness(getPhenotype(), environment));
			if (getFitnessCache() != null)
				getFitnessCache().addFitness(getPhenotype(), fitnessEngine, environment, result);
		}
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Genetic#getGenome()
	 */
	@Override
	public Genome<P, G> getGenome() {
		return getNuclear().getGenome();
	}

	/**
	 * @return {@link #_identifier}
	 * @see Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @return {@link #_nuclear}, the nuclear object underlying this organism
	 */
	@Override
	@ToString(detail = 1)
	public Nuclear<P, G> getNuclear() {
		return this._nuclear;
	}

	/**
	 * Get the phenotype by expressing the genome in the context of the colony.
	 * If {@link #phenotype} is null, it is set to the result of calling
	 * {@link #getGenome()} and passing its result to
	 * {@link Genome#express(Colony)} with parameter from {@link #getColony()}.
	 * 
	 * @return {@link #phenotype}
	 * @exception DarwinException
	 *                if {@link #getColony()} yields null.
	 * @see net.sf.darwin.core.Organism#getPhenotype()
	 */
	@Override
	@ToString(detail = 1, priority = 2)
	public Phenotype<P> getPhenotype() {
		if (this.phenotype == null) {
			if (getColony() != null) {
				this.phenotype = getGenome().express(getColony());
			} else
				throw new DarwinException(MessageFormat.format(
						"getPhenotype(): organism {0} already dead (null colony)", getIdentifier())); //$NON-NLS-1$
		}
		return this.phenotype;

	}

	/**
	 * Get the fitness for this Organism, either from the cache for the fitness
	 * engine, or by calculation. The higher the value, the fitter is the
	 * organism.
	 * 
	 * @return the fitness of this {@link Organism} by invoking
	 *         {@link #getFitness(FitnessEngine)} and passing it the fitness
	 *         engine for the phenome for the system for the colony.
	 * @throws ValueException
	 * @see net.sf.darwin.core.Valuable#getValue()
	 */
	@Override
	public Number getValue() throws ValueException {
		try {
			return getFitness(getColony().getPopulation().getTaxon().getPhenome().getFitnessEngine());
		} catch (final FitnessException e) {
			throw new ValueException("Organism_.getValue(): fitness exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Individual#getVisualizable()
	 */
	@Override
	public Visualizable getVisualizable() {
		return getColony();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getIdentifier() == null) ? 0 : getIdentifier().hashCode());
		result = prime * result + ((getNuclear() == null) ? 0 : getNuclear().hashCode());
		return result;
	}

	/**
	 * TODO calculate fitness within this method rather than have it passed in.
	 * 
	 * @param fitness
	 *            the fitness of the organism whose mortality we are
	 *            discovering.
	 * @return true if the organism's number is up (i.e. to be eliminated).
	 */
	@Override
	public boolean isMortal(final double fitness) {
		final Mortality mortalityEvaluator = getColony().getPopulation().getTaxon().getMortality();
		final double mortality = mortalityEvaluator.calculateMortality(getAge(), fitness, getColony().getSaturation());
		return mortalityEvaluator.isMarked(mortality);
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Individual#isNew()
	 */
	@Override
	public boolean isNew() {
		return getAge() == 0;
	}

	/**
	 * @see net.sf.darwin.core.Mortal#isViable()
	 */
	@Override
	@ToString
	public boolean isViable() {
		return this.viable && getColony() != null;
	}

	/**
	 * 
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Individual#isVisible()
	 */
	@Override
	public boolean isVisible() {
		return isViable();
	}

	/**
	 * TODO consider adding this to the {@link Organism} interface.
	 * 
	 * @param referenceGenome
	 *            the genome to which this {@link Organism}'s genome will be
	 *            normalized
	 * @param cache
	 *            map of bases -> genome pairs
	 * @param environment
	 *            the environment to which this {@link Organism} belongs.
	 */
	@Override
	public void normalizeGenome(final Genome<P, G> referenceGenome, final Map<String, Genome<P, G>> cache,
			final Environment<E> environment) {
		onEnvironmentChange(environment);
		final Genome<P, G> genome = getGenome();
		final String bases = genome.getBases();
		final Genome<P, G> normalizedGenome = cache.get(bases);
		if (normalizedGenome == null) {
			referenceGenome.getGenomic().normalize(genome, referenceGenome);
			if (LOG.isTraceEnabled())
				LOG.trace(MessageFormat.format("organism genome {0} after normalization: {1}", bases, genome)); //$NON-NLS-1$ 
			cache.put(bases, genome);
		} else {
			genome.getGenes().clear();
			genome.addGenes(normalizedGenome);
		}
		onGenomeChange();
	}

	/**
	 * flush the fitness cache completely. Note that is is not sufficient to
	 * flush just the value for the genome because some genomes may have been
	 * lost from the gene pool, but not flushed from the cache, thus they would
	 * never get flushed and cause the wrong fitness values to be picked up
	 * later when an identical genome comes along which, because of the
	 * environment change, would have a different fitness value.
	 * 
	 * TODO find a way to flush all only once on an environment change and then
	 * in this method we could then revert to simple flushing the cache for the
	 * genome, provided that we remove values from cache when they no longer
	 * exist in the gene pool.
	 * 
	 * @see net.sf.darwin.core.EnvironmentListener#onEnvironmentChange(net.sf.darwin.core.Environment)
	 */
	@Override
	public void onEnvironmentChange(final Environment<E> env) {
		this.phenotype = null;
		getFitnessCache().flush();
		// _fitnessCache.flush(getGenome());
	}

	/**
	 * Reset the owning colony of this organism. This is only ever done
	 * 
	 * @see net.sf.darwin.core.Organism#setColony(Colony)
	 */
	@Override
	public void setColony(final Colony<E, P, G> colony) {
		this.colony = colony;
	}

	/**
	 * @param viability
	 *            the determinant of viability for this {@link Organism}
	 * @return true if {@link #setViable(boolean)} was invoked on this
	 *         {@link Organism}.
	 * @throws FitnessException
	 */
	@Override
	public boolean setViability(final Viability viability) throws FitnessException {
		// try {
		return viability.resetViability(this);
		// } catch (final FitnessException e) {
		//			LOG.warn("setViability fitness exception", e); //$NON-NLS-1$
		// return false;
		// }
	}

	/**
	 * @see net.sf.darwin.core.Mortal#setViable(boolean)
	 */
	@Override
	public void setViable(final boolean viable) {
		this.viable = viable;
	}

	/**
	 * @param f
	 * @return the mortality, defined by calling
	 *         {@link Mortality#calculateMortality(int, double, double)} with
	 *         parameters {@link #age()}, <code>f</code>, and the saturation for
	 *         the colony.
	 */
	double calculateMortality(final double f) {
		if (getColony() != null)
			return getMortality().calculateMortality(getAge(), f, getColony().getSaturation());
		throw new DarwinException("calculateMortality(): already dead"); //$NON-NLS-1$
	}

	/**
	 * Method which must be called whenever the genome of this {@link Organism}
	 * is changed. See to do comment re:
	 * {@link #onEnvironmentChange(Environment)} regarding flushing.
	 */
	void onGenomeChange() {
		this.phenotype = null;
		getFitnessCache().flush();
	}

	/**
	 * @return {@link #_fitnessCache}
	 */
	protected FitnessCache getFitnessCache() {
		return this._fitnessCache;
	}

	/**
	 * Get the mortality for this mortal object (from the system of the colony
	 * to which this organism belongs).
	 * 
	 * @return the mortality of the system of the colony.
	 */
	protected Mortality getMortality() {
		if (getColony() != null)
			return getColony().getPopulation().getTaxon().getMortality();
		throw new DarwinException("getMortality(): already dead"); //$NON-NLS-1$
	}

	/**
	 * @param context
	 */
	private void doCensusInContext(final OrganismCensusContext<P, G> context) {
		if (isViable() || getAge() == 0) {
			// XXX consider doing this via polymorphism
			if (this instanceof Sexual)
				context.getSexFrequencies().add(((Sexual) this).isFemale() ? "F" : "M"); //$NON-NLS-1$//$NON-NLS-2$

			getPhenotype().doCensus(context.getCharacter(), context.getTraitFrequencies());
			getGenome().doCensus(context.getAlleleFrequencies());
			context.getAgeFrequencies().add(Integer.valueOf(getAge()));
		}
	}

	private static final long serialVersionUID = 7901215389244718404L;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Organism_.class);

	/**
	 * The cell (in effect the nuclear DNA) to which this organism conforms.
	 */
	protected final Nuclear<P, G> _nuclear;

	/**
	 * An identifier for this organism.
	 */
	protected final String _identifier;

	/**
	 * The true if this organism is alive (it will only ever be set to false for
	 * a short time, before the organism is buried/cremated/eaten/whatever).
	 */
	private transient boolean viable;

	/**
	 * The current age of this organism (measured in generations).
	 */
	private transient int age;

	/**
	 * The colony to which this organism (currently) belongs.
	 */
	private transient Colony<E, P, G> colony;

	/**
	 * Cache of fitness values.
	 */
	private transient final FitnessCache _fitnessCache;

	private Phenotype<P> phenotype;

}