/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser_Sex.java
 * Created on Jun 8, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Expresser_;
import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Sexual;
import net.sf.darwin.core.Trait;

/**
 * Implementer of {@link Expresser} specifically for sex.
 * 
 * TODO this seems wrong -- we should be extending {@link Expresser_} instead of
 * {@link Expresser_Mendelian}. Consider refactoring here.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public class Expresser_Sex extends Expresser_Mendelian<Boolean> {

	/**
	 */
	public Expresser_Sex() {
		super(Pharacter_Sex.getInstance(), Allele_Sex.S_X, Pharacter_Sex.KEY_F);
	}

	/**
	 * We can express the sex of a (haploid) gamete
	 * 
	 * @see net.sf.darwin.api.impl.Expresser_Mendelian#expressHaploid(net.sf.darwin.core.Gene)
	 */
	@Override
	protected <G> Trait<Boolean> expressHaploid(final Gene<G> gene) {
		final Allele<G> allele = gene.getAllele(0);
		// TODO consider doing this via polymorphism
		if (allele instanceof Sexual) {
			return ((Sexual) allele).isFemale() ? TRAIT_FEMININE : TRAIT_MASCULINE;
		}
		throw new DarwinException("expressHaploid: invalid allele"); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final Trait TRAIT_MASCULINE = TraitFactory.makeDiscrete(Pharacter_Sex.getInstance(), Pharacter_Sex.KEY_M);

	/**
	 * 
	 */
	private static final Trait TRAIT_FEMININE = TraitFactory.makeDiscrete(Pharacter_Sex.getInstance(), Pharacter_Sex.KEY_F);
}
