/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Dirty
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

import java.util.Collection;

/**
 * This models a potentially dirty object such as a user-interface model. It is
 * expected that when setDirty is called with true, some action will
 * <i>eventually</i> be taken to restore cleanliness and reset the dirty value
 * to false.
 * 
 * @author rhillya
 * 
 */
public interface Dirty {

	/**
	 * @return a minimal set of objects that when refreshed will make this
	 *         {@link Dirty} object clean.
	 */
	public abstract Collection<Object> getDirt();

	/**
	 * @return true if this object is actually dirty.
	 */
	public abstract boolean isDirty();

	/**
	 * Set the state of dirtiness.
	 * 
	 * @param dirty
	 *            if true, then an event should be triggered for eventual action
	 *            to restore state.
	 */
	public abstract void setDirty(boolean dirty);

	/**
	 * Set the state of dirtiness.
	 * 
	 * @param dirty
	 *            if true, then an event should be triggered for eventual action
	 *            to restore state.
	 * @param hint
	 *            if dirty is true, then the hint will be examined and, if it is
	 *            non-null, then potentially only a partial cleanup will be
	 *            required. Interpretation of hint is entirely up to this Dirty
	 *            object.
	 */
	public abstract void setDirty(boolean dirty, Object hint);
}
