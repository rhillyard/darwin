/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GeneFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.ExPhen_Castor;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExPhen;

/**
 * Factory Class to create {@link ExPhen} objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
public final class ExPhenFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private ExPhenFactory() {
		super();
	}

	/**
	 * 
	 */
	private static final String S_FCTRY_MAKE_EX_PHEN = "ExPhenFactory.makeExtendedPhenotype(): "; //$NON-NLS-1$

	/**
	 * @param clazz
	 *            the class of ExPhen to instantiate, which must have a
	 *            constructor of form ExPhen(EcoFactor,Object).
	 * @param factor
	 *            the eco factor
	 * @param value
	 *            the value of the extended phenotype
	 * @return a newly constructed {@link ExPhen}.
	 */
	public static ExPhen makeExtendedPhenotype(final Class<? extends ExPhen> clazz, final EcoFactor factor, final Object value) {
		try {
			final Constructor<? extends ExPhen> constructor = clazz
					.getConstructor(new Class<?>[] { EcoFactor.class, Object.class });
			return constructor.newInstance(new Object[] { factor, value });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_EX_PHEN + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_EX_PHEN + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_EX_PHEN + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(
					S_FCTRY_MAKE_EX_PHEN + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Consruct a new ExPhen object, of the default class: {@link ExPhen_Castor}
	 * .
	 * 
	 * TEST
	 * 
	 * @param factor
	 *            the eco factor
	 * @param value
	 *            the value of the extended phenotype
	 * @return a newly constructed {@link ExPhen}.
	 */
	public static ExPhen makeExtendedPhenotype(final EcoFactor factor, final Object value) {
		return new ExPhen_Castor(factor, value);
	}

}
