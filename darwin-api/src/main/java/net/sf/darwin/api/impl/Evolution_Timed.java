/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution_Timed.java
 * Created on Aug 3, 2009
 * @version $Revision: 1.16 $
 */

package net.sf.darwin.api.impl;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.concurrent.Future;

import net.sf.darwin.api.base.Evolution_;
import net.sf.darwin.api.base.Timed;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.GenerationListener;
import net.sf.tostring0.ToString;

/**
 * This abstract class extends the notion of an {@link Evolution_} by adding the
 * properties start, rate for continuously evolving processes [the
 * {@link Evolution_} class can be run in continuous evolution mode too but it
 * doesn't really know about rate and start. Time properties for this class are
 * real time.
 * 
 * This class also allows for the scheduling of arbitrary future events in terms
 * of real time.
 * 
 * This class has a sub-class Evolution_Calendar which allows for evolution to
 * be timed in non-real-time, for example geological time.
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
public abstract class Evolution_Timed extends Evolution_ implements Timed {

	/**
	 * @param run
	 *            XXX
	 * 
	 */
	protected Evolution_Timed(final boolean run) {
		this(Calendar.getInstance(), run);
	}

	/**
	 * 
	 * @param start
	 *            XXX
	 * @param run
	 *            XXX
	 * 
	 */
	protected Evolution_Timed(final Calendar start, final boolean run) {
		super();
		this._start = start;
		this._run = run;
		this.rate = Evolution_.MILLISECS_PER_SECOND;
		this.startAdjustment = 0;
		LOG.info("new Evolution_Timed object instantiated: " + this); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.api.base.Timed#getRate()
	 */
	@Override
	public int getRate() {
		return this.rate;
	}

	/**
	 * Get the current time of this evolution
	 * 
	 * @see net.sf.darwin.api.base.Timed#getTime()
	 */
	@Override
	@ToString(formString = "showTime")
	public Calendar getTime() throws EvolutionException {
		final double clk = getClock() * getRate() + getStartAdjustment();
		final Calendar result = getTime(Calendar.MILLISECOND, clk);
		if (result != null)
			return result;
		throw new EvolutionException("Evolution_Calendar.getTime(): logic error"); //$NON-NLS-1$
	}

	/**
	 * First, stop any currently running tasks. Then start a new repeating task
	 * with parameters from {@link #getRate()} and {@link #isRun()}. Then, if
	 * {@link #isWaitUntilComplete()} returns true, we invoke
	 * {@link #waitUntilComplete(int)} with the value of #_waitPeriod.
	 * 
	 * Thus, if you do not want to wait for the evolution to finish, simply
	 * invoke {@link #setWaitUntilComplete(boolean)} with false before calling
	 * run.
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		startAndWait(getRate(), isRun(), true);
	}

	/**
	 * @param when
	 * @param event
	 */
	public void scheduleEvent(final Calendar when, final Runnable event) {
		final long difference = when.getTimeInMillis() - getStart().getTimeInMillis();
		scheduleEvent(difference / getRate(), event);
	}

	/**
	 * @param ticks
	 *            the number of ticks before the event is run.
	 * @param event
	 *            the event to be run.
	 * 
	 *            NOTE that the number of ticks will not necessarily match the
	 *            ticks of a running evolution because of pauses (which don't
	 *            increment the tick number).
	 * 
	 *            TODO consider making the ticks correspond properly.
	 * 
	 * @see net.sf.darwin.api.base.Evolution_#scheduleEvent(long,
	 *      java.lang.Runnable)
	 */
	@Override
	final public Future<?> scheduleEvent(final long ticks, final Runnable event) {
		return super.scheduleEvent(ticks * getRate(), event);
	}

	/**
	 * @see net.sf.darwin.api.base.Timed#setRate(int)
	 */
	@Override
	public void setRate(final int newRate) throws EvolutionException {
		if (newRate != getRate()) {
			final boolean paused = isPaused();
			if (!paused)
				pause(); // XXX is this necessary?
			// TODO should stop only if already running
			stop();
			waitUntilComplete(GenerationListener.$1_SECOND);
			this.startAdjustment += getClock() * (getRate() - newRate);
			logRate(newRate, getStartAdjustment());
			this.rate = newRate;
			start(newRate, !paused);
		}
	}

	/**
	 * @return {@link #_start}, the moment that the start of evolution
	 *         corresponds to. Typically this is the real time when evolution
	 *         started, but in a simulation, it can be an arbitrary historical
	 *         or future time.
	 */
	protected Calendar getStart() {
		return this._start;
	}

	/**
	 * This gives us the adjustment (in milliseconds) to be applied to the
	 * actual start time, which takes into account any changes in rate along the
	 * way. The formula for current evolutionary time is therefore
	 * {@link #_start} + {@link #startAdjustment} + {@link #getClock()} *
	 * {@link #getRate()}.
	 * 
	 * @return {@link #startAdjustment}
	 */
	protected double getStartAdjustment() {
		return this.startAdjustment;
	}

	/**
	 * @param period
	 * @param quantity
	 * @return if quantity can be represented in terms of period in a Calendar
	 *         object, then a new Calendar object, otherwise null.
	 */
	protected Calendar getTime(final int period, final double quantity) {
		if ((getStart().get(period) + quantity) < Integer.MAX_VALUE) {
			final Calendar result = (Calendar) getStart().clone();
			result.add(period, (int) Math.round(quantity));
			return result;
		}
		return null;
	}

	/**
	 * @return {@link #_run}, true if the evolution is to start immediately
	 */
	protected boolean isRun() {
		return this._run;
	}

	protected int rate;

	/**
	 */
	private double startAdjustment;

	private final boolean _run;

	private final Calendar _start;

	/**
	 * @param newRate
	 * @param startAdjustment
	 *            XXX
	 */
	@SuppressWarnings({ "nls", "boxing" })
	private static void logRate(final int newRate, final double startAdjustment) {
		if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format("setRate(): rate is {0} and start adjustment is {1} seconds", newRate,
					(startAdjustment / Evolution_.MILLISECS_PER_SECOND)));
	}

}
