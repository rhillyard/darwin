/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GeneFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.Allele_Dominance;
import net.sf.darwin.api.impl.Allele_Sex;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.DarwinException;

/**
 * Factory Class to create Allele objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * TODO move into factory package
 * 
 * @author Robin Hillyard
 * 
 */
public final class AlleleFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private AlleleFactory() {
		super();
	}

	/**
	 * These are completely arbitrary and only so that we can see something when
	 * we show the bases of a dominance allele
	 */
	@SuppressWarnings("nls")
	private static final String[] DOMINANCE_BASES = new String[] { "CA", "GT" };

	private static final String S_FCTRY_MAKE_ALLELE = "AlleleFactory.makeAllele(): "; //$NON-NLS-1$

	/**
	 * TEST
	 * 
	 * @param clazz
	 *            the class of Allele to instantiate, which must have a
	 *            constructor of form Allele(Object,Object).
	 * @param key
	 *            the key, ideally a String
	 * @param value
	 *            the value, ideally a V
	 * @return a newly constructed {@link Object}.
	 */
	public static <V> Allele<V> makeAllele(final Class<? extends Allele<V>> clazz, final Object key, final Object value) {
		try {
			final Constructor<? extends Allele<V>> constructor = clazz
					.getConstructor(new Class<?>[] { Object.class, Object.class });
			return constructor.newInstance(new Object[] { key, value });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_ALLELE + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_ALLELE + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_ALLELE + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(
					S_FCTRY_MAKE_ALLELE + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Construct a new {@link Allele_Dominance} with the given allele indexes
	 * 
	 * TEST
	 * 
	 * @param identifier
	 *            the identifier for a pair of dominant/recessive alleles (the
	 *            identifiers should match but don't need to be in any
	 *            particular case).
	 * @param dominant
	 *            true if the allele is to be dominant, else recessive
	 * @return a newly constructed {@link Allele_Dominance}.
	 */
	public static Allele<Boolean> makeDominanceAllele(final String identifier, final boolean dominant) {
		return new Allele_Dominance(identifier, dominant, DOMINANCE_BASES);
	}

	/**
	 * @param male
	 *            XXX
	 * @return a newly constructed {@link Allele_Sex} object.
	 */
	public static Allele<Boolean> makeSexAllele(final boolean male) {
		return new Allele_Sex(male);
	}

}
