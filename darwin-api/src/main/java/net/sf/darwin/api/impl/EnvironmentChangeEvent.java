/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EnvironmentChangeEvent.java
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Evolution;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class defines a {@link Runnable} event which changes the value of an eco
 * factor in an environment. It is normally scheduled by virtue of the
 * {@link Evolution#scheduleEvent(long, Runnable)} method.
 * 
 * TODO consider extending AToString
 * 
 * @author Robin Hillyard
 * 
 */
public class EnvironmentChangeEvent<E> implements Runnable {

	/**
	 * @param environment
	 *            the environment we will be changing
	 * @param factorKey
	 *            the factor key to identify which factor will change
	 * @param factorValue
	 *            the new value for the factor
	 * 
	 */
	public EnvironmentChangeEvent(final Environment<E> environment, final String factorKey, final E factorValue) {
		super();
		this._environment = environment;
		this._factorKey = factorKey;
		this._factorValue = factorValue;
	}

	/**
	 * @return the environment
	 */
	public Environment<E> getEnvironment() {
		return this._environment;
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		LOG.info("scheduled environment change event"); //$NON-NLS-1$
		getEnvironment().update(this._factorKey, this._factorValue);
		LOG.info("environment updated"); //$NON-NLS-1$
	}

	protected final static Log LOG = LogFactory.getLog(EnvironmentChangeEvent.class);

	private final Environment<E> _environment;

	private final String _factorKey;

	private final E _factorValue;
}