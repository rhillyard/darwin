/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 * 
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Variant;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * This abstract class defines the base methods and fields for an implementation
 * of {@link Variant}.
 * </p>
 * 
 * Note that {@link #getIdentifier()} and {@link #getAttribute()} return the
 * same object (although the latter returns it as an Object).
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.19 $
 */
@SuppressWarnings("serial")
public abstract class Variant_<P> extends Attribute_<String, P> implements Variant<P> {

	/**
	 * @param identifier
	 * @param value
	 */
	protected Variant_(final String identifier, final P value) {
		super(identifier, value);
	}

	/**
	 * @return the Phenotypic Character to which this variant belongs.
	 * @see net.sf.darwin.core.Variant#getCharacter()
	 */
	@Override
	public Pharacter<P> getCharacter() {
		return this.character;
	}

	/**
	 * Set the phenotypic character, normally via reflection/configuration.
	 * 
	 * @param character
	 */
	public void setCharacter(final Pharacter<P> character) {
		this.character = character;
	}

	private transient Pharacter<P> character;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Variant_.class);

}