/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenotypeCache_Standard.java
 * Created on Jul 14, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Cache_;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.FitnessCache;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.TraitMap;

/**
 * This cache maintains a set of fitness values indexed by
 * Signature/FitnessEngine pair. Normally, the trait map object is the phenotype
 * of an Organism. The purpose is to save time recalculating the fitness when it
 * is unnecessary.
 * 
 * The signature of a TraitMap is its "cache key".
 * 
 * @author Robin Hillyard
 * 
 */
public class FitnessCache_Standard<P, E> extends Cache_<FitnessCacheKey, Double> implements FitnessCache<P, E> {

	/**
	 * 
	 */
	public FitnessCache_Standard() {
		super();
	}

	/**
	 * @see net.sf.darwin.core.FitnessCache#addFitness(net.sf.darwin.core.TraitMap,
	 *      net.sf.darwin.core.FitnessEngine, net.sf.darwin.core.Environment,
	 *      java.lang.Double)
	 */
	@Override
	public void addFitness(final TraitMap<P> traits, final FitnessEngine<P, E> fitnessEngine, final Environment<E> environment,
			final Double fitness) {
		add(new FitnessCacheKey(traits, fitnessEngine, environment), fitness);
	}

	/**
	 * @see net.sf.darwin.core.FitnessCache#flush(net.sf.darwin.core.TraitMap,
	 *      net.sf.darwin.core.FitnessEngine, net.sf.darwin.core.Environment)
	 */
	@Override
	public void flush(final TraitMap<P> traits, final FitnessEngine<P, E> engine, final Environment<E> environment) {
		flush(new FitnessCacheKey(traits, engine, environment));
	}

	/**
	 * @see net.sf.darwin.core.FitnessCache#getFitness(net.sf.darwin.core.TraitMap,
	 *      net.sf.darwin.core.FitnessEngine, net.sf.darwin.core.Environment)
	 */
	@Override
	public Double getFitness(final TraitMap<P> traits, final FitnessEngine<P, E> fitnessEngine, final Environment<E> environment) {
		return get(new FitnessCacheKey(traits, fitnessEngine, environment));
	}

	/**
	 * @return "fitness cache"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return "fitness cache"; //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.FitnessCache#register(net.sf.darwin.core.FitnessEngine)
	 */
	@Override
	public void register(final FitnessEngine<P, E> fitnessEngine) {
		fitnessEngine.setFitnessCache(this);
	}
}
