/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenotypeCacheKey.java
 * Created on Oct 2, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Basic;
import net.sf.darwin.core.Genes;
import net.sf.tostring0.Identifiable;

/**
 * This class is used to form the key for the PhenotypeCache
 * 
 * @author Robin Hillyard
 * 
 */
public class PhenotypeCacheKey implements Identifiable {

	/**
	 * @param genes
	 *            the genes of the cached object
	 * @param identifiable
	 *            an identifiable object to uniquely determine said genes,
	 *            usually the id of the population to which the genes belong.
	 * 
	 */
	public PhenotypeCacheKey(final Basic genes, final Identifiable identifiable) {
		super();
		this._genes = genes;
		this._identifier = identifiable != null ? identifiable.getIdentifier() : ""; //$NON-NLS-1$
	}

	/**
	 * Standard equals but based on {@link Genes#getSignature()} instead of just
	 * _genes.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PhenotypeCacheKey other = (PhenotypeCacheKey) obj;
		if (this._genes == null) {
			if (other._genes != null)
				return false;
		} else if (!this._genes.getSignature().equals(other._genes.getSignature()))
			return false;
		if (this._identifier == null) {
			if (other._identifier != null)
				return false;
		} else if (!this._identifier.equals(other._identifier))
			return false;
		return true;
	}

	/**
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier + ":" + this._genes.getBases(); //$NON-NLS-1$
	}

	/**
	 * Standard hashCode but based on {@link Genes#getSignature()} instead of
	 * just _genes.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._genes == null) ? 0 : this._genes.getSignature().hashCode());
		result = prime * result + ((this._identifier == null) ? 0 : this._identifier.hashCode());
		return result;
	}

	/**
	 * TODO consider to use audit utilities
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getIdentifier();
	}

	private final Basic _genes;

	private final String _identifier;

}
