/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionSource
 * Created on: Mar 4, 2012
 */

package net.sf.darwin.api.base;

import java.util.Collection;

/**
 * @author rhillya
 * 
 * @param <S>
 *            this is the type in which option values are presented from the
 *            source - some sources simply leave the option as a String, others
 *            attempt to convert into an Object, Number, whatever.
 */
public interface OptionSource<S> {

	/**
	 * @return a collection of Strings which are they keys to the options
	 */
	public abstract Collection<String> getKeys();

	/**
	 * @param key
	 * @return the value for the given key
	 */
	public abstract S getOption(String key);
}
