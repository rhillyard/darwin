/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: Loggable
 * Created on: Feb 27, 2012
 */

package net.sf.darwin.api.impl;

import org.apache.commons.logging.Log;

/**
 * @author rhillya
 * 
 * @param <T>
 */
public interface Loggable<T> {

	/**
	 * @param log
	 */
	public abstract void log(final Log log);

}