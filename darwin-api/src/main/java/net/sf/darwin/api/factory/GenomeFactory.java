/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GenomeFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.base.Genome_;
import net.sf.darwin.api.base.Genomic_;
import net.sf.darwin.api.base.IsSexual;
import net.sf.darwin.api.base.Meiosis_;
import net.sf.darwin.api.impl.Genome_Linear;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Ploidy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Factory Class to create Genome objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * XXX consider renaming this and refactoring some of the methods to return
 * Genotype objects.
 * 
 * @author Robin Hillyard
 * 
 */
public final class GenomeFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private GenomeFactory() {
		super();
	}

	protected static final Log LOG = LogFactory.getLog(GenomeFactory.class);

	private static final String S_FCTRY_MAKE_GENOTYPE = "GenomeFactory.makeGenome(): "; //$NON-NLS-1$

	/**
	 * Factory method to create a haploid genome which conforms to the given
	 * {@link Genomic} and with genes where the alleles have been picked for
	 * each locus according to the {@link Locus#pickAllele()} method.
	 * 
	 * TODO this code is very similar to that in the createGamete method:
	 * {@link Genomic_#createGamete(Genes)}
	 * 
	 * TODO this code is very similar to that in the doMeiosis method of
	 * {@link Meiosis_#doMeiosis(Genomic, Genes, net.sf.darwin.core.Chromosome)}
	 * 
	 * @param genomic
	 *            the genomic which defines the possible gene alleles.
	 * @return the resulting genome.
	 */
	public static <P, G> Genome<P, G> makeGamete(final Genomic<P, G> genomic) {
		final Genome<P, G> gamete = makeLinear(genomic, Ploidy.HAPLOID);
		final int loci = genomic.getLocusCount();
		for (int pos = 0; pos < loci; pos++) {
			final Locus<G> locus = genomic.getLocus(pos);
			gamete.addGene(locus.makeGene(locus.pickAllele()));
			// TODO check if it's OK to impose type W on the Polygenic locus
			Locus<G> polygenic = (Locus<G>) locus.getPolygenic();
			while (polygenic != null) {
				gamete.addGene(polygenic.makeGene(polygenic.pickAllele()));
				polygenic = (Locus<G>) polygenic.getPolygenic();
			}
		}
		if (gamete.isSexual() && gamete instanceof Genome_)
			try {
				final Locus<Boolean> locus = ((IsSexual) gamete.getGenomic()).getSexGenomic().getLocus(0);
				final Gene<Boolean> sexGene = locus.makeGene(locus.pickAllele());
				((Genome_<P, G>) gamete).setSexGene(sexGene);
			} catch (final GeneticsException e) {
				// This should never happen (famous last words)
				throw new RuntimeException("logic error", e); //$NON-NLS-1$
			}

		final Genome<P, G> result = genomic.mutate(gamete);
		if (LOG.isTraceEnabled())
			LOG.trace("GenomeFactory.makeGamete(Genomic): result: " + result); //$NON-NLS-1$
		return result;
	}

	/**
	 * TEST
	 * 
	 * @param clazz
	 *            the class of Genome to instantiate, which must have a
	 *            constructor of form Genome(Genomic,Object[]).
	 * @param genomic
	 *            the genomic
	 * @param parameters
	 *            the parameters for the constructor
	 * @return a newly constructed {@link Genome}.
	 */
	public static <P, G> Genome<P, G> makeGenome(final Class<? extends Genome<P, G>> clazz, final Genomic<P, G> genomic,
			final Object... parameters) {
		try {
			final Constructor<? extends Genome<P, G>> constructor = clazz.getConstructor(new Class<?>[] { Genomic.class,
					Object[].class });
			return constructor.newInstance(new Object[] { genomic, parameters });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENOTYPE + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENOTYPE + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_GENOTYPE + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(
					S_FCTRY_MAKE_GENOTYPE + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Factory method to create an <i>empty</i> genome which conforms to the
	 * given {@link Genomic} and is diploid or haploid according to the
	 * <code>ploidy</code> parameter. Note the will be no genes in the result.
	 * 
	 * TODO consider returning a Genotype_, for example a Genotype_Linear, which
	 * currently does not exist.
	 * 
	 * @param genomic
	 *            the genomic to which the resulting genome will conform.
	 * @param ploidy
	 * @return a newly constructed empty linear genome with the given ploidy.
	 */
	public static <P, G> Genome<P, G> makeLinear(final Genomic<P, G> genomic, final int ploidy) {
		return new Genome_Linear<>(genomic, ploidy);
	}

	/**
	 * @param gameteX
	 * @param gameteY
	 * @return the result of fertilizing gameteX with gameteY
	 */
	public static <P, G> Genome<P, G> makeZygote(final Genome<P, G> gameteX, final Genes<G> gameteY) {
		// XXX For now, we just assume that gameteX and gameteY share the same
		// genomic.
		final Genome<P, G> result = GenomeFactory.makeLinear(gameteX.getGenomic(), Ploidy.DIPLOID);
		result.fertilize(gameteX, gameteY);
		return result;
	}

	/**
	 * Method to create (from "whole cloth") a new diploid Genome, by creating
	 * two gametes (using {@link #makeGamete} and then fertilizing them.
	 * 
	 * XXX check that this name is reasonably appropriate.
	 * 
	 * @param genomic
	 * @return a new diploid genome.
	 */
	public static <P, G> Genome<P, G> makeZygote(final Genomic<P, G> genomic) {
		final Genome<P, G> gameteX = makeGamete(genomic);
		final Genes<G> gameteY = makeGamete(genomic);
		return makeZygote(gameteX, gameteY);
	}

}
