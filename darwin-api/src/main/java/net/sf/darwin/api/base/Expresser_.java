/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser_.java
 * Created on Mar 4, 2009
 * @version $Revision: 1.23 $
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.factory.TraitFactory;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.ExPhen;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Expression;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Base (abstract) implementation of {@link Expresser}.
 * 
 * There is no specific support for Epistasis since the expression of each of
 * the various loci is essentially independent. For further information, see
 * {@link Chromosome_}.
 * 
 * @author Robin Hillyard
 * 
 */
public abstract class Expresser_<P> implements Expresser<P> {

	/**
	 * protected constructor
	 * 
	 * @param character
	 *            the phenotypic character of any resulting traits.
	 * @param type
	 *            ignored
	 */
	protected Expresser_(final Pharacter<P> character, final Object type) {
		super();
		this._character = character;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Expresser_ other = (Expresser_) obj;
		if (this._character == null) {
			if (other._character != null)
				return false;
		} else if (this._character != other._character)
			return false;
		return true;
	}

	/**
	 * Express the gene in the context of the environment, potentially resulting
	 * in several expressions, each of which may be either a {@link Trait} or an
	 * {@link ExPhen}.
	 * 
	 * This is the more general form of expression and typically will invoke the
	 * more specific form -- {@link #express(Gene...)} -- (colony is ignored and
	 * always returns a {@link Trait}).
	 * 
	 * This method supports both polygeny (where several genes act together) and
	 * pleiotropy (where one or more genes results in more than one trait).
	 * 
	 * @param colony
	 *            (which may be null)
	 * @param genes
	 *            one gene or a set of polygenic genes.
	 * 
	 * @return a collection of objects which are either a {@link Trait} or an
	 *         {@link ExPhen}
	 */
	public <E, V> Collection<Expression<P>> express(final Colony<E, P, V> colony,
			@SuppressWarnings("unchecked") final Gene<V>... genes) {
		final Collection<Expression<P>> result = new ArrayList<>();
		result.add(express(genes));
		return result;
	}

	/**
	 * @return {@link #_character}
	 * @see net.sf.darwin.core.Expresser#getCharacter()
	 */
	@Override
	public Pharacter<P> getCharacter() {
		return this._character;
	}

	/**
	 * This implementation of getComplement first checks to make sure that the
	 * locus is binary (i.e. has two alleles) and then simply returns the key
	 * for the other allele.
	 * 
	 * If the locus is not binary, then this method will need to be overridden,
	 * otherwise an exception will be thrown.
	 * 
	 * @see net.sf.darwin.core.Expresser#getComplement(net.sf.darwin.core.Locus,
	 *      java.lang.String)
	 */
	@Override
	public <V> String getComplement(final Locus<V> locus, final String allele) {
		if (locus.getAlleleMap().size() == 2) {
			for (final String key : locus.getAlleleMap().keySet()) {
				if (key.equals(allele))
					continue;
				return key;
			}
			throw new DarwinException("Expresser.getComplement() logic error"); //$NON-NLS-1$
		}
		throw new DarwinException("Expresser.getComplement() needs to be overridden for loci with other than 2 alleles"); //$NON-NLS-1$
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._character == null) ? 0 : this._character.hashCode());
		return result;
	}

	/**
	 * XXX Be careful -- this needs to be overridden for any meaningful result.
	 * 
	 * @see net.sf.darwin.core.Expresser#isComplementary(net.sf.darwin.core.Locus,
	 *      java.lang.String, java.lang.String)
	 */
	@Override
	public <V> boolean isComplementary(final Locus<V> locus, final String allele1, final String allele2) {
		return false;
	}

	/**
	 * TEST
	 * 
	 * @param value
	 *            the key of the desired variant, in the context of the
	 *            character.
	 * @return a trait for this expresser's phenotypic character and the
	 *         designated value.
	 */
	@SuppressWarnings("unchecked")
	protected Trait<Number> createTrait(final double value) {
		// TODO check this cast
		return TraitFactory.makeVariable((Pharacter<Number>) getCharacter(), value);
	}

	/**
	 * @param key
	 *            the key of the desired variant, in the context of the
	 *            character.
	 * @return a trait for this expresser's phenotypic character and the
	 *         designated variant.
	 */
	protected Trait<P> createTrait(final String key) {
		return TraitFactory.makeDiscrete(getCharacter(), key);
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Expresser_.class);

	protected final Pharacter<P> _character;

}
