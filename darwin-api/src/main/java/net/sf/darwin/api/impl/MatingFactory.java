/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MatingFactory.java
 * Created on Nov 4, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Organism;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Factory Class to create Mating objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
public final class MatingFactory {

	/**
	 * 
	 */
	private MatingFactory() {
		super();
	}

	/**
	 * @param genomic
	 * @param female
	 * @param male
	 * @param random
	 * @return a new instance of {@link Mating_Haldanian} based on the given
	 *         male and female and conforming to the given genomic
	 */
	public static Mating makeHaldanian(final Genomic genomic, final Organism female, final Organism male,
			final RandomGenerator random) {
		return new Mating_Haldanian(male.getNuclear(), female.getNuclear(), random, genomic);
	}

}
