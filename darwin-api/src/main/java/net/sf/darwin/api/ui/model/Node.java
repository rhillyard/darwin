/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Node
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

import java.util.Collection;
import java.util.Map;

/**
 * TODO provide javadoc for methods
 * 
 * @author rhillya
 * 
 */
public interface Node extends Visibility {

	public abstract Map<String, Action> getActionMenu();

	public abstract Appearance getAppearance();

	public abstract Collection<Node> getChildren();

	public abstract Collection<Node> getConnexions();

	public abstract NodeType getNodeType();

	public abstract Map<String, Object> getProperties();

	public abstract Nodal getValue();

	public abstract boolean hasChildren();

	public abstract boolean isDrawn();

	public abstract boolean isUpToDate();

	/**
	 * Update the properties of this Node
	 */
	public abstract void refresh();

	/**
	 * This and the following string constants are to be replaced by named
	 * properties of the Node, allowing the properties property to be used
	 * solely for custom attributes.
	 */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	public static final String PROPERTY_COMMENT = "comment"; //$NON-NLS-1$

	public static final String PROPERTY_CHILDREN_VISIBLE = "childrenVisible"; //$NON-NLS-1$

	public static final String PROPERTY_TYPE = "type"; //$NON-NLS-1$

	public static final String PROPERTY_ENVELOPE = "envelope"; //$NON-NLS-1$

	public static final String PROPERTY_ID = "id"; //$NON-NLS-1$

	public static final String PROPERTY_CLASS = "class"; //$NON-NLS-1$
}
