/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 10, 2003
 *
 * @version $Revision: 1.22 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Attribute;
import net.sf.darwin.core.Base;
import net.sf.tostring0.Detail;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

/**
 * <p>
 * Abstract class representing a named attribute, and is the base class for
 * Variant_, Allele_ and EcoFactor_. <br>
 * An attribute has two properties: identifier and value.
 * </p>
 * 
 * @author Robin Hillyard
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
abstract public class Attribute_<K, V> extends Base implements Attribute<K, V>, Identifiable {

	/**
	 * @param identifier
	 * @param value
	 */
	protected Attribute_(final K identifier, final V value) {
		super();
		this._identifier = identifier;
		this.value = value;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Attribute_<?, ?> other = (Attribute_<?, ?>) obj;
		if (this._identifier == null) {
			if (other._identifier != null)
				return false;
		} else if (!this._identifier.equals(other._identifier))
			return false;
		if (getValue() == null) {
			if (other.value != null)
				return false;
		} else if (!getValue().equals(other.value))
			return false;
		return true;
	}

	/**
	 * @return the value of {@link #_identifier}
	 * 
	 * @see net.sf.darwin.core.Attribute#getAttribute()
	 */
	@Override
	@ToString
	public K getAttribute() {
		return this._identifier;
	}

	/**
	 * @return the value of {@link #getAttribute()} as a String.
	 * 
	 * @see Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		if (getAttribute() != null)
			return getAttribute().toString();
		return null;
	}

	/**
	 * @return the value of {@link #value}.
	 * 
	 * @see net.sf.darwin.core.Attribute#getValue()
	 */
	@Override
	@ToString
	public V getValue() {
		return this.value;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._identifier == null) ? 0 : this._identifier.hashCode());
		result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());
		return result;
	}

	/**
	 * set the field {@link #value}.
	 * 
	 * @see net.sf.darwin.core.Attribute#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(final V value) {
		this.value = value;
	}

	/**
	 * @see net.sf.tostring0.AToString#toString(net.sf.tostring0.Detail)
	 */
	@Override
	public String toString(final Detail detail) {
		if (detail.isShowDetail()) {
			final StringBuilder result = new StringBuilder();
			result.append("Attribute "); //$NON-NLS-1$
			result.append(getIdentifier());
			if (getValue() != null) {
				result.append(": "); //$NON-NLS-1$
				result.append(getValue().toString());
				// result.append(ToStringUtils.toStringValue(value, showClass,
				// showIdentifier, showDetail, prefix,
				// recurse, maxElements, maxChars, null));
			}
			return result.toString();
		}
		return getIdentifier();
	}

	/**
	 * This field represents the (fixed) identifier for this object. Typically,
	 * but not always, the identifier is a string.
	 */
	private final K _identifier;

	/**
	 * This field represents the (mutable) value for this object. It may take
	 * any form.
	 */
	private V value;
}
