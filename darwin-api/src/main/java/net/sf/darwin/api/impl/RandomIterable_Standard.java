/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: RandomIterable_Standard.java
 * Created on Dec 4, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.sf.darwin.core.RandomIterable;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Decorate a {@link Collection} as an {@link Iterator} such that the order of
 * visiting the elements in the collection is random.
 * 
 * IMPORTANT NOTE: a RandomIterable_Standard is not <i>backed</i> by the
 * underlying collection. That is to say, if the collection changes, the
 * iterator will not be aware of the change. Removing an element from this
 * iterator does NOT remove the element from the underlying collection.
 * 
 * TODO consider trying to implement it such that it really is backed by the
 * underlying collection.
 * 
 * @author Robin Hillyard
 * @param <E>
 *            the Generic type for this Iterator
 * 
 *            TODO consider extending AToString
 */
public class RandomIterable_Standard<E> implements RandomIterable<E> {

	/**
	 * @param collection
	 *            the collection we want to iterate randomly through.
	 * @param random
	 *            the random number generator to be used (useful if we want to
	 *            seed the random number generator).
	 */
	public RandomIterable_Standard(final Collection<E> collection, final RandomGenerator random) {
		this(collection, random, collection.size());
	}

	/**
	 * @param collection
	 *            the collection we want to iterate through.
	 * @param random
	 *            the random number generator to be used (useful if we want to
	 *            seed the random number generator).
	 * @param sampleSize
	 *            the maximum number of elements to put in the iterator.
	 * 
	 */
	public RandomIterable_Standard(final Collection<E> collection, final RandomGenerator random, final int sampleSize) {
		this._backing = collection;
		this._random = random;
		this._sampleSize = sampleSize;
		setList(new ArrayList<E>());
		reset();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.RandomIterable#getRemainder()
	 */
	@Override
	public Iterable<E> getRemainder() {
		return getList().subList(size(), getList().size());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.RandomIterable#isSample()
	 */
	@Override
	public boolean isSample() {
		return size() < getList().size();
	}

	/**
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return getList().subList(0, size()).iterator();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.RandomIterable#reset()
	 */
	@Override
	public void reset() {
		getList().clear();
		for (final E e : getBacking())
			getList().add(getRandom().nextInt(getList().size() + 1), e);
		final int listsize = getList().size();
		setSize((getSampleSize() <= listsize) ? getSampleSize() : listsize);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.RandomIterable#size()
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * @return the backing
	 */
	private Iterable<E> getBacking() {
		return this._backing;
	}

	/**
	 * @return the list
	 */
	private List<E> getList() {
		return this.list;
	}

	/**
	 * @return the random
	 */
	private RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * @return the sampleSize
	 */
	private int getSampleSize() {
		return this._sampleSize;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	private void setList(final List<E> list) {
		this.list = list;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	private void setSize(final int size) {
		this.size = size;
	}

	/**
	 * read-only reference to the original collection passed in
	 */
	private final Iterable<E> _backing;

	/**
	 * copy of parameter passed in.
	 */
	private final int _sampleSize;

	/**
	 * the random number source
	 */
	private final RandomGenerator _random;

	/**
	 * A complete copy of the collection passed in, but in a random order.
	 */
	private List<E> list;

	private int size;

}
