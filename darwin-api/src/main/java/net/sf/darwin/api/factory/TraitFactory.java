/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GeneFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.Trait_Discrete;
import net.sf.darwin.api.impl.Trait_Variable;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Trait;

/**
 * Factory Class to create Trait objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
public final class TraitFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private TraitFactory() {
		super();
	}

	private static final String S_FCTRY_MAKE_TRAIT = "TraitFactory.makeTrait(): "; //$NON-NLS-1$

	/**
	 * Factory method to create a discrete trait -- that is where the value is
	 * one of a finite set of variants, as opposed to the variable trait as in
	 * the case of {@link #makeVariable(Pharacter, double)}.
	 * 
	 * @param character
	 *            the phenotypic character of which this is a variant.
	 * @param variant
	 *            the key to the variant.
	 * @return a newly constructed trait
	 */
	public static <X> Trait<X> makeDiscrete(final Pharacter<X> character, final String variant) {
		return new Trait_Discrete<>(character, variant);
	}

	/**
	 * TEST
	 * 
	 * @param clazz
	 *            the class of Trait to instantiate, which must have a
	 *            constructor of form Trait(Character,Object).
	 * @param character
	 *            the phenotypic character to which this trait refers.
	 * @param value
	 *            the value.
	 * @return a newly constructed trait
	 */
	public static <X> Trait<X> makeTrait(final Class<? extends Trait<X>> clazz, final Character character, final Object value) {
		try {
			final Constructor<? extends Trait<X>> constructor = clazz.getConstructor(new Class<?>[] { Character.class,
					Object.class });
			return constructor.newInstance(new Object[] { character, value });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_TRAIT + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_TRAIT + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_TRAIT + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(
					S_FCTRY_MAKE_TRAIT + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Factory method to create a variable trait -- that is where the value is
	 * infinitely (or relatively infinitely) variable (as opposed to one of a
	 * discrete set) as in the case of {@link #makeDiscrete(Pharacter, String)}.
	 * 
	 * XXX check why there are two calls to this method. It seems to me there
	 * should be only one.
	 * 
	 * @param character
	 *            the phenotypic character of which this is an instance.
	 * @param value
	 *            the value of the trait
	 * @return a newly constructed trait
	 */
	@SuppressWarnings("boxing")
	public static Trait<Number> makeVariable(final Pharacter<Number> character, final double value) {
		return new Trait_Variable(character, value);
	}

}
