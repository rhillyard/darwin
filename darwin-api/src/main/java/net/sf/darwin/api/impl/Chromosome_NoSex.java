/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Chromosome_NoSex.java
 * Created on Jun 1, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Chromosome_;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;

/**
 * Standard implementation of {@link Chromosome} that is not sex-linked. If you
 * need an implementation of Chromosome which does not fit either this type or
 * {@link Chromosome_Sex}, then you need to subclass {@link Chromosome_}.
 * 
 * This type name reminds me of the play <a
 * href="http://en.wikipedia.org/wiki/No_Sex_Please,_We%27re_British">No Sex
 * Please, We're British</a>
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public final class Chromosome_NoSex<V> extends Chromosome_<V> {

	/**
	 * @param identifier
	 */
	public Chromosome_NoSex(final String identifier) {
		this(identifier, Genomic.STANDARD_ALPHABET);
	}

	/**
	 * @param identifier
	 * @param alphabet
	 *            TODO
	 */
	public Chromosome_NoSex(final String identifier, final String alphabet) {
		super(identifier, alphabet, false);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7164947091307751103L;

}
