/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: StringUtilities.java
 * Created on Nov 1, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.util;

/**
 * @author Robin Hillyard
 * 
 */
public class StringUtilities {

	/**
	 * @param s
	 *            the string
	 * @return s but with the first character in upper case and the rest in
	 *         lower case
	 */
	public static String capitalizeInitial(final String s) {
		return capitalizeInitial(s, true);
	}

	/**
	 * @param s
	 *            the string
	 * @param b
	 *            whether or not to capitalize the initial
	 * @return s but with the first character in upper case (depending on value
	 *         of <code>b</code>) and the rest in lower case
	 */
	public static String capitalizeInitial(final String s, final boolean b) {
		final String initial = s.substring(0, 1);
		return (b ? initial.toUpperCase() : initial.toLowerCase()) + s.substring(1).toLowerCase();
	}
}
