/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009 Rubecula Software, LLC.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Created on Dec 28, 2003
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.NumericalTrait;
import net.sf.darwin.api.base.Phenome_;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Phenome;

/**
 * <p>
 * Concrete class Phenome_Numerical is a simple implementation of
 * {@link Phenome} with the one policy that Traits can be reduced to a numerical
 * value. If there is no appropriate numerical value for a trait, then the
 * {@link Phenome_Gouldian} class should be used, not Phenome_Numerical.
 * </p>
 * 
 * TODO this appears to be obsolete -- it can be replaced by
 * {@link Phenome_Gouldian}
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.16 $
 * @since V_0_0
 */
@Lifecycle(permanent = true)
public class Phenome_Numerical extends Phenome_<Number> implements NumericalTrait {

	/**
	 * Primary Constructor which invokes the super-constructor for, passing in
	 * the fitnessFunction as its argument.
	 * 
	 * @param fitnessFunction
	 *            the {@link FitnessEngine}.
	 * @param numericalTrait
	 *            an implementer of {@link NumericalTrait}.
	 * 
	 */
	public Phenome_Numerical(final FitnessEngine fitnessFunction, final NumericalTrait numericalTrait) {
		super(fitnessFunction);
		this._numericalTrait = numericalTrait;
	}

	/**
	 * Method to reduce the value of a trait to a (double) number.
	 * 
	 * @param trait
	 *            the name of the trait.
	 * @return the "value" of the trait expressed numerically.
	 * @see net.sf.darwin.api.base.NumericalTrait#getTraitValue(java.lang.String)
	 */
	@Override
	public double getTraitValue(final String trait) {
		return this._numericalTrait.getTraitValue(trait);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4746696147069326538L;

	/**
	 * The delegate for the getTraitValue method.
	 */
	final private NumericalTrait _numericalTrait;

}
