/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: NodeMutable
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

import java.util.Collection;

/**
 * @author rhillya
 * 
 */
public interface NodeMutable extends Node {

	/**
	 * @param key
	 * @param value
	 */
	public abstract void addActionMenuItem(final String key, final Action value);

	/**
	 * CONSIDER removing this unused method.
	 * 
	 * @param child
	 * @return
	 * @see java.util.List#add(java.lang.Object)
	 */
	public abstract boolean addChild(Node child);

	/**
	 * CONSIDER removing this unused method.
	 * 
	 * @param key
	 * @param value
	 * 
	 */
	public abstract void addProperty(String key, Object value);

	/**
	 * @param children
	 */
	public abstract void setChildren(Collection<Node> children);

	/**
	 * @param upToDate
	 */
	public abstract void setUpToDate(boolean upToDate);

	/**
	 * @param visible
	 */
	public abstract void setVisible(boolean visible);

}