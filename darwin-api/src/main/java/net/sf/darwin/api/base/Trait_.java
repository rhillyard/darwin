/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Variant_.java
 * Created on May 29, 2009
 * @version $Revision: 1.27 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

/**
 * Base implementation of {@link Trait}.
 * 
 * TODO we should declare the type of value as Object and have the subtypes set
 * values as appropriate (String, Number, etc.).
 * 
 * @author Robin Hillyard
 *
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            {@link Trait}
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Trait_<P> extends Base implements Trait<P> {

	/**
	 * @param character
	 *            the {@link Pharacter} of which this {@link Trait} is an
	 *            instance.
	 * @param variant
	 *            the key of the specific variant for this {@link Trait}
	 */
	protected Trait_(final Pharacter<P> character, final String variant) {
		super();
		this._character = character;
		this._variant = variant;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present(toString(), context);
		return false;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Trait_ other = (Trait_) obj;
		if (getCharacter() == null) {
			if (other.getCharacter() != null)
				return false;
		} else if (!getCharacter().equals(other.getCharacter()))
			return false;
		if (getVariantKey() == null) {
			if (other.getVariantKey() != null)
				return false;
		} else if (!getVariantKey().equals(other.getVariantKey()))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return null;
	}

	/**
	 * @see net.sf.darwin.core.Trait#getCharacter()
	 */
	@Override
	public Pharacter<P> getCharacter() {
		return this._character;
	}

	/**
	 * @return the identifier of this character
	 */
	@Override
	@ToString(omit = true)
	public String getCharacterKey() {
		return getCharacter().getIdentifier();
	}

	/**
	 * @return {@link #getCharacterKey()} ':' {@link #getVariantKey()}
	 * @see Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getCharacterKey() + ":" + getVariantKey(); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Trait#getPhenotype()
	 */
	@Override
	public Phenotype<P> getPhenotype() {
		return this.phenotype;
	}

	/**
	 * @see net.sf.darwin.core.Trait#getValue()
	 */
	@Override
	@ToString
	public P getValue() {
		return getVariant().getValue();
	}

	/**
	 * TODO consider renaming this method.
	 * 
	 * @return the variant for this trait.
	 */
	@Override
	public Variant<P> getVariant() {
		return getCharacter().getVariant(getVariantKey());
	}

	/**
	 * TODO consider renaming this method.
	 * 
	 * @return the value of the {@link #_variant} field.
	 * @see net.sf.darwin.core.Trait#getVariantKey()
	 */
	@Override
	@ToString(omit = true)
	public String getVariantKey() {
		return this._variant;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCharacter() == null) ? 0 : getCharacter().hashCode());
		result = prime * result + ((getVariantKey() == null) ? 0 : getVariantKey().hashCode());
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Trait#setPhenotype(net.sf.darwin.core.Phenotype)
	 */
	@Override
	public void setPhenotype(final Phenotype<P> phenotype) {
		this.phenotype = phenotype;
	}

	private final Pharacter<P> _character;

	private final String _variant;

	private transient Phenotype<P> phenotype;

}
