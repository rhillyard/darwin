/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExpresserFunction.java
 * Created on Jun 10, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Allele;

/**
 * Defines the operation which takes an Allele and generates a Trait. This is
 * useful for non-Mendelian gene expression, for example, when we are expressing
 * a haploid gene.
 * 
 * @author Robin Hillyard
 * 
 */
public interface ExpresserFunction {

	/**
	 * Express the given allele in a non-Mendelian manner. The value returned
	 * might be a string representing the key to a variant, or it might be a
	 * value, for a variable trait.
	 * 
	 * @param allele
	 * @return the result of expressing the given allele (a String or a Double).
	 */
	public abstract Object express(Allele allele);

}
