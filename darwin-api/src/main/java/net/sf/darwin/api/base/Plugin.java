/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: Plugin
 * Created on: Mar 3, 2012
 */

package net.sf.darwin.api.base;

/**
 * This interface is intended to form the basis of a plugin architecture.
 * 
 * @author rhillya
 * 
 */
public interface Plugin {

	// nothing as yet
}