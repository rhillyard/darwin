/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: UIException
 * Created on: Sep 29, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public class UIException extends Exception {

	public UIException(final String message) {
		super(message);
	}

	public UIException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public UIException(final Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = -1416692596266319208L;
}
