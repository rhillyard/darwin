/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Cache_.java
 * Created on Oct 1, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.core.Cache;
import net.sf.darwin.core.CacheSignature;
import net.sf.tostring0.Identifiable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract base class for implementing a {@link Cache}.
 * 
 * TODO consider moving this to the foundation package (along with other
 * {@link Cache}-related classes)
 * 
 * TODO consider having K extend {@link CacheSignature} instead of
 * {@link Identifiable}.
 * 
 * Essentially, we will delegate the real work to {@link HashMap}. Note that it
 * is required that the type K has an appropriate definition of
 * {@link Object#hashCode()} (and of course equals) so that the cache will
 * operate the way you expect.
 * 
 * @author Robin Hillyard
 * @param <K>
 *            the unique key type which will be used for this cache.
 * @param <V>
 *            the value that will be stored for the given key.
 */
public abstract class Cache_<K extends Identifiable, V> implements Cache, Identifiable {

	/**
	 * 
	 */
	protected Cache_() {
		super();
		this._cache = new HashMap<>();
	}

	/**
	 * @return the number of entries in the cache
	 * @see java.util.Map#size()
	 */
	@Override
	public int count() {
		return getCache().size();
	}

	/**
	 * Does nothing if {@link #_cache} is already clear.
	 * 
	 * @see java.util.Map#clear()
	 */
	@Override
	public void flush() {
		if (getCache().size() > 0) {
			getCache().clear();
			if (LOG.isDebugEnabled())
				LOG.debug("cache flushed: " + getIdentifier()); //$NON-NLS-1$
		}
	}

	/**
	 * @return the value of the field {@link #active}.
	 */
	@Override
	public boolean isActive() {
		return this.active;
	}

	/**
	 * @param active
	 */
	@Override
	public void setActive(final boolean active) {
		this.active = active;
		if (!active)
			flush();
	}

	/**
	 * @param key
	 * @param value
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	protected void add(final K key, final V value) {
		if (isActive()) {
			getCache().put(key, value); // void the result
			if (LOG.isTraceEnabled())
				LOG.trace(MessageFormat.format("key {0} added to cache: {1}", key.getIdentifier(), getIdentifier())); //$NON-NLS-1$
		}
	}

	/**
	 * @param key
	 * @return true if the cache already contains the given key
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	protected boolean cached(final K key) {
		if (isActive())
			return getCache().containsKey(key);
		return false;
	}

	/**
	 * @param key
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	protected void flush(final K key) {
		if (isActive())
			getCache().remove(key); // void the result
	}

	/**
	 * @param key
	 * @return the value for this key, if any
	 * @see java.util.Map#get(java.lang.Object)
	 */
	protected V get(final K key) {
		if (isActive()) {
			final V result = getCache().get(key);
			if (LOG.isTraceEnabled())
				LOG.trace(MessageFormat.format("Cache.get(): key={0}, result={1} from cache={2}", key, result, this)); //$NON-NLS-1$
			return result;
		}
		return null;
	}

	/**
	 * @return the value of {@link #_cache}
	 */
	private Map<K, V> getCache() {
		return this._cache;
	}

	private boolean active = true;

	private final Map<K, V> _cache;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Cache_.class);

}
