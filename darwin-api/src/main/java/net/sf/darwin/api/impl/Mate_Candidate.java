/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mate_Candidate.java
 * Created on Jul 17, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Candidate;
import net.sf.darwin.api.base.Mate_;
import net.sf.darwin.core.Mate;
import net.sf.darwin.core.Organism;

/**
 * @author Robin Hillyard
 * 
 */
public class Mate_Candidate extends Mate_ implements Candidate {

	/**
	 * @param male
	 * @param desirability
	 */
	public Mate_Candidate(final Organism male, final double desirability) {
		super(male, desirability);
	}

	/**
	 * @param candidate
	 * @return true if this has been updated
	 * @see net.sf.darwin.api.base.Candidate#updateIfBetter(net.sf.darwin.core.Mate)
	 */
	@Override
	public boolean updateIfBetter(final Mate candidate) {
		if (candidate != null && candidate.getMale() != null && candidate.getDesirability() > getDesirability())
			return update(candidate.getMale(), candidate.getDesirability());
		return false;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5418538723397158376L;
}
