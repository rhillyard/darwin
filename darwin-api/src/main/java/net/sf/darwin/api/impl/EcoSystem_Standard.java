/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoSystem_Standard.java
 * Created on Mar 21, 2009
 * @version $Revision: 1.27 $
 */

package net.sf.darwin.api.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.api.base.EcoFactor_;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.EcoSystem;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.TraitMap;
import net.sf.tostring0.ToString;

/**
 * The type holds a map of eco factors for an environment. Since it's only used
 * by Environment (i.e. this package) most of the methods are of package scope
 * only. It could conceivably be defined as a class within the Environment_
 * class.
 * 
 * Logically parallel class to {@link TraitMap}.
 * 
 * @author Robin Hillyard
 */
@ToString(allBeanMethods = false)
public final class EcoSystem_Standard<E> extends Base implements EcoSystem<E>, Cloneable {

	/**
	 * Default-scoped constructor. This should be invoked only by the
	 * Environment_ class.
	 * 
	 * @param environment
	 *            the environment to which this ecosystem belongs.
	 * 
	 */
	public EcoSystem_Standard(final Environment<E> environment) {
		this(environment, new HashMap<String, EcoFactor<E>>());
	}

	/**
	 * Default-scoped constructor. This should be invoked only by the
	 * Environment_ class. In this case, we clone the factors of the given
	 * ecoSystem and use them, along with the given environment to construct a
	 * new EcoSystem_Standard.
	 * 
	 * @param environment
	 *            the environment to which this ecosystem belongs.
	 * @param ecoSystem
	 * @throws CloneNotSupportedException
	 * 
	 */
	@SuppressWarnings("unchecked")
	public EcoSystem_Standard(final Environment<E> environment, final EcoSystem<E> ecoSystem) throws CloneNotSupportedException {
		this(environment, (Map<String, EcoFactor<E>>) ((HashMap<String, EcoFactor<E>>) ((EcoSystem_Standard<E>) ecoSystem)
				.getFactors()).clone());
		for (final String key : factorKeys())
			getFactors().put(key, (EcoFactor<E>) ((EcoFactor_<E>) getFactor(key)).clone());
	}

	/**
	 * Default-scoped constructor. This should be invoked only by the
	 * Environment_ class.
	 * 
	 * @param environment
	 *            the environment to which this ecosystem belongs.
	 * @param factors
	 * 
	 */
	public EcoSystem_Standard(final Environment<E> environment, final Map<String, EcoFactor<E>> factors) {
		this._environment = environment;
		this._ecoFactors = factors;
	}

	/**
	 * Add or replace an ecofactor in this {@link EcoSystem_Standard}. Not the
	 * favored method of adding a factor to an EcoSytem. See
	 * {@link #setEcoFactors(Collection)}.
	 * 
	 * @param factor
	 * @return true if successful TODO return description not accurate but
	 *         should be.
	 */
	@Override
	public Object add(final EcoFactor<E> factor) {
		factor.setEnvironment(this._environment);
		return this.getFactors().put(factor.getIdentifier(), factor);
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		final EcoSystem_Standard<E> clone = (EcoSystem_Standard<E>) super.clone();
		clone.setEcoFactors((Collection<EcoFactor<E>>) ((HashMap<String, EcoFactor<E>>) clone.getFactors()).clone());
		return clone;
	}

	/**
	 * @return the set of keys
	 */
	@Override
	public Set<String> factorKeys() {
		return getFactors().keySet();
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Countable#getCount()
	 */
	@Override
	public int getCount() {
		return this.getFactors().size();
	}

	/**
	 * @param name
	 * @return the double value
	 * @throws DarwinException
	 */
	@Override
	public double getDoubleValue(final String name) {
		final EcoFactor<E> factor = getFactor(name);
		// TODO consider doing this via polymorphism
		if (factor instanceof Quantifiable) {
			return ((Quantifiable) factor).doubleValue();
		}
		throw new DarwinException("Environment does not have numeric eco factor: " + name); //$NON-NLS-1$
	}

	/**
	 * @param key
	 * @return the factor, if any
	 */
	@Override
	public EcoFactor<E> getFactor(final String key) {
		return getFactors().get(key);
	}

	/**
	 * Add all of the ecoFactors to this eco system, using each factor's
	 * identifier for the key in the map.
	 * 
	 * @param ecoFactors
	 *            a collection of ecoFactor objects.
	 */
	@Override
	public void setEcoFactors(final Collection<EcoFactor<E>> ecoFactors) {
		clear();
		final Map<String, EcoFactor<E>> map = new HashMap<>();
		for (final EcoFactor<E> ecoFactor : ecoFactors) {
			ecoFactor.setEnvironment(this._environment);
			map.put(ecoFactor.getIdentifier(), ecoFactor);
		}
		putAll(map);
	}

	/**
	 * @return a collection of values.
	 */
	@Override
	public Collection<EcoFactor<E>> values() {
		return getFactors().values();
	}

	private void clear() {
		getFactors().clear();
	}

	/**
	 * @return
	 */
	private Map<String, EcoFactor<E>> getFactors() {
		return this._ecoFactors;
	}

	/**
	 * Method which delegates to {@link #getFactors()}.
	 * 
	 * @param map
	 * @see Map#putAll(Map)
	 */
	private void putAll(final Map<? extends String, ? extends EcoFactor<E>> map) {
		getFactors().putAll(map);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1763125062043242229L;

	/**
	 * 
	 */
	private final Environment<E> _environment;

	/**
	 * 
	 */
	@ToString(detail = 1)
	private transient final Map<String, EcoFactor<E>> _ecoFactors;

}