/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Fecundity_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Fecundity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Base implementation of the {@link Fecundity} interface. This class provides
 * no methods, fields or constructors.
 * 
 * @author Robin Hillyard
 */
public abstract class Fecundity_ implements Fecundity {

	/**
	 * Protected Primary Constructor
	 */
	protected Fecundity_() {
		super();
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Fecundity_.class);

}
