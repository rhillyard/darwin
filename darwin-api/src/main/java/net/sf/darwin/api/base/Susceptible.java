/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Susceptible.java
 * Created on Jan 20, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.api.impl.Population_Managed;
import net.sf.darwin.core.Best;
import net.sf.darwin.core.ComparableValue;

/**
 * This interface defines a method which makes an Environment susceptible to
 * change due to the extended phenotypes of organisms. This is particularly
 * useful for splitting-type Populations, e.g. {@link Population_Managed}
 * objects, which tend to be be based on asexual reproduction.
 * 
 * @author Robin Hillyard
 * 
 */
public interface Susceptible {

	/**
	 * This implementation attempts to update the given environment from the
	 * given source. If an update occurs, we reset the bestForEnvironment value,
	 * and log the new route and its corresponding travel time.
	 * 
	 * @param best
	 *            the best object that should be reset if there is an update in
	 *            this method.
	 * @param source
	 *            the source of the update
	 * 
	 * @return true if the environment was updated
	 */
	public abstract boolean updateFromSource(final Best<? extends ComparableValue> best, final Object source);

}