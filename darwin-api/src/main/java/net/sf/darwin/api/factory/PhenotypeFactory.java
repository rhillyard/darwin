/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PhenotypeFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.Phenotype_Dawkinsian;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Phenotype;

/**
 * Factory Class to create Phenotype objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * TODO fix up the other factory classes so that it works like this one.
 * 
 * TODO figure out a way to reference Phenotype_Dawkinsian directly without
 * violating any architectural issues.
 * 
 * @author Robin Hillyard
 * 
 */
public final class PhenotypeFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private PhenotypeFactory() {
		super();
	}

	private static final String PHENOTYPE_DAWKINSIAN = "net.sf.darwin.api.impl.Phenotype_Dawkinsian"; //$NON-NLS-1$

	private static final String S_FCTRY_MAKE_PHEN = "PhenotypeFactor.makePhenotype(): "; //$NON-NLS-1$

	private static transient int sequence = 0;

	/**
	 * This is only for the use of unit tests.
	 * 
	 * TODO by using pattern matching, avoid having to use this.
	 * 
	 * @return the sequence
	 */
	@Deprecated
	public static int getNextSequence() {
		return sequence;
	}

	/**
	 * @return a newly created {@link Phenotype_Dawkinsian} object.
	 */
	@SuppressWarnings("unchecked")
	public static <P> Phenotype<P> makeDawkinsian() {
		final String id = "" + (sequence++); //$NON-NLS-1$
		return makePhenotype(PHENOTYPE_DAWKINSIAN, new Class[] { String.class }, new Object[] { id });
		//		return new Phenotype_Dawkinsian(id); //$NON-NLS-1$
	}

	/**
	 * @param clazz
	 *            the class of Phenotype to instantiate, which must have a
	 *            constructor of form Phenotype(EcoFactor,Object).
	 * @param parameterClasses
	 *            XXX
	 * @param parameterValues
	 *            XXX
	 * @return a newly constructed {@link Phenotype}.
	 */
	public static <P> Phenotype<P> makePhenotype(final Class<? extends Phenotype<P>> clazz,
			final Class<? extends Object>[] parameterClasses, final Object[] parameterValues) {
		try {
			final Constructor<? extends Phenotype<P>> constructor = clazz.getConstructor(parameterClasses);
			return constructor.newInstance(parameterValues);
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_PHEN + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_PHEN + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_PHEN + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(S_FCTRY_MAKE_PHEN + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * @param classname
	 *            the fully qualified name of the Phenotype class requied.
	 * @param parameterClasses
	 *            XXX
	 * @param parameterValues
	 *            the parameters for the constructor
	 * @return a newly constructed {@link Phenotype}.
	 */
	public static <P> Phenotype<P> makePhenotype(final String classname, final Class<? extends Object>[] parameterClasses,
			final Object[] parameterValues) {
		try {
			return makePhenotype((Class<? extends Phenotype<P>>) Class.forName(classname), parameterClasses, parameterValues);
		} catch (final ClassNotFoundException e) {
			throw new DarwinException(S_FCTRY_MAKE_PHEN + "class not found", e); //$NON-NLS-1$
		}
	}

}
