package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;

import net.sf.darwin.api.impl.Population_Managed;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Best;
import net.sf.darwin.core.ComparableValue;
import net.sf.darwin.core.ProcessBest;
import net.sf.darwin.core.Valuable;
import net.sf.darwin.core.ValueException;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class to manage the best fit of something.
 * 
 * @param <T>
 *            the base type for the best fit
 * 
 */
@SuppressWarnings("serial")
public abstract class Best_<T extends ComparableValue> extends Base implements Best<T> {
	/**
	 * Construct the Best_ object based on the identifier <code>id</code>, and
	 * get the best value for the given <code>candidates</code>, using the
	 * <code>processor</code> if not-null.
	 * 
	 * @param id
	 * @param candidates
	 * @param processor
	 * @throws ValueException
	 */
	protected Best_(final String id, final Collection<T> candidates, final ProcessBest<T> processor) throws ValueException {
		this(id, null, processor, 0);
		update(candidates);
	}

	/**
	 * @param id
	 *            the description of this "best" object
	 * @param convergence
	 *            XXX
	 * 
	 */
	protected Best_(final String id, final int convergence) {
		this(id, null, convergence);
	}

	/**
	 * @param id
	 *            the description of this "best" object
	 * @param processor
	 * 
	 */
	protected Best_(final String id, final ProcessBest<T> processor) {
		this(id, null, processor, 0);
	}

	/**
	 * @param id
	 *            the description of this "best" object
	 * @param processor
	 * @param convergence
	 *            XXX
	 * 
	 */
	protected Best_(final String id, final ProcessBest<T> processor, final int convergence) {
		this(id, null, processor, convergence);
	}

	/**
	 * @param id
	 *            the description of this "best" object
	 * @param best
	 * @param processor
	 *            the object which will process each new best object when an
	 *            update occurs.
	 * @param convergence
	 *            XXX
	 */
	protected Best_(final String id, final T best, final ProcessBest<T> processor, final int convergence) {
		super();
		this._id = id;
		this._processor = processor;
		this.convergence = convergence;
		setObject(best);
	}

	/**
	 * @return {@link #_id}
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._id;
	}

	/**
	 * @see net.sf.darwin.core.Best#getObject()
	 * 
	 *      XXX note that we must include this method explicitly because its
	 *      generic return type means it doesn't match as a bean method.
	 */
	@Override
	@ToString
	public T getObject() {
		return this.object;
	}

	/**
	 * @return {@link #_processor}
	 */
	@Override
	public ProcessBest<T> getProcessor() {
		return this._processor;
	}

	/**
	 * @throws ValueException
	 *             which will in practice never be thrown by this method (since
	 *             we do not crown invalid candidates as best).
	 * @see net.sf.darwin.core.Best#getValue()
	 */
	@Override
	public Number getValue() throws ValueException {
		final T best = getObject();
		if (best != null)
			return best.getValue();
		return null;
	}

	/**
	 * @see net.sf.darwin.core.Best#reset()
	 */
	@Override
	public void reset() {
		setObject(null);
	}

	/**
	 * This method loops through each element of candidates and calls
	 * {@link #updateInternal(ComparableValue, boolean)} for each candidate. On
	 * an update, and provided that value of {@link #_processor} is not null, we
	 * invoke {@link #_processor} with the new best object and best value.
	 * 
	 * Note that we do not really need to deal with convergence here, because we
	 * are processing an entire collection. It doesn't therefore matter much
	 * when the processor is actually triggered because it will be done
	 * internally while this method is in play. However, in fact, convergence is
	 * dealt with appropriately (assuming that {@link #convergence} is greater
	 * than zero. If it's zero, then the whole notion of convergence is moot.
	 * 
	 * @param candidates
	 * @return true if we actually did update the best value (i.e. candidates
	 *         was not empty).
	 * @throws ValueException
	 */
	@Override
	public boolean update(final Collection<T> candidates) throws ValueException {
		boolean result = false;
		final Iterator<T> iterator = candidates.iterator();
		while (iterator.hasNext()) {
			final T candidate = iterator.next();
			if (updateInternal(candidate, !iterator.hasNext()))
				result = true;
		}
		return result;
	}

	/**
	 * @param candidate
	 *            XXX
	 * @param override
	 *            if true, then we consider convergence to be true even if the
	 *            count has not been reached.
	 * @return true if we have accepted and processed the new candidate
	 * @throws ValueException
	 */
	@Override
	public boolean update(final T candidate, final boolean override) throws ValueException {
		return updateInternal(candidate, override);
	}

	/**
	 * @return {@link #convergence}
	 */
	protected int getConvergence() {
		return this.convergence;
	}

	protected abstract boolean isUnique(T candidate);

	/**
	 * @param convergence
	 *            the convergence to set
	 */
	protected void setConvergence(final int convergence) {
		this.convergence = convergence;
	}

	/**
	 * Sets the object AND resets the count value.
	 * 
	 * @param object
	 *            the object to set
	 */
	protected void setObject(final T object) {
		this.object = object;
		this.count = 0;
	}

	/**
	 * As a side effect, logging may occur.
	 * 
	 * @return true if count is equal to the convergence value
	 */
	private boolean isConvergent() {
		final boolean result = this.count == getConvergence(); // XXX was >=
		logConvergent(getConvergence(), result, this.count);
		return result;
	}

	/**
	 * XXX reduce the McCabe Cyclomatic Complexity measure.
	 * 
	 * @param candidate
	 * @param override
	 *            I neglected to describe this adequately while writing this
	 *            method. Shame! However, I note that this is generally true
	 *            when we get to the last of a collection of objects and false
	 *            while we're still working through the collection.
	 * 
	 *            TODO check the following result
	 * @return see table below where unique is true if the Best_#isUnique(T)
	 *         method of the implementing class applied to the candidate returns
	 *         true:
	 *         <table>
	 *         <tr>
	 *         <th></th>
	 *         <th align="center">unique</th>
	 *         <th align="center">not unique</th>
	 *         </tr>
	 *         <tr>
	 *         <th>more fit</th>
	 *         <td align="center">T (update)</td>
	 *         <td align="center">T (update + warning)</td>
	 *         </tr>
	 *         <tr>
	 *         <th>same</th>
	 *         <td align="center">F (increment count)</td>
	 *         <td align="center">F (info msg)</td>
	 *         </tr>
	 *         <tr>
	 *         <th>less fit</th>
	 *         <td align="center">F</td>
	 *         <td align="center">F</td>
	 *         </tr>
	 *         </table>
	 * @throws ValueException
	 */
	private boolean updateInternal(final T candidate, final boolean override) throws ValueException {
		// First test to see if the candidate value is better than the incumbent
		final int cf = ComparableValue_.compareTo(candidate, getObject());
		// final int cf = candidate.compareTo(getObject());
		if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat
					.format("update(): best in {0} candidate {1} with value: {2}: is {3} than {4}", getIdentifier(), candidate.getIdentifier(), candidate.getValue(), (cf > 0 ? "more fit" : (cf < 0 ? "less fit" : "equally fit")), getValue())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ 
		if (cf < 0)
			return false;
		boolean trigger = false;
		// Note that we only need to calculate unique here because it's used to
		// generate a warning, otherwise we could wait until (if) needed.
		final boolean unique = isUnique(candidate);
		if (cf > 0) {
			LOG.info(MessageFormat
					.format("update(): best in {0} done for {1} with value: {2}", getIdentifier(), candidate.getIdentifier(), candidate.getValue())); //$NON-NLS-1$ 
			if (!unique)
				LOG.warn("new best value does not come with unique candidate in " + getIdentifier()); //$NON-NLS-1$
			setObject(candidate);
			// XXX consider adding "|| override" to the following logic
			// expression
			trigger = (getConvergence() == 0);
		} else if (!unique) {
			++this.count;
			trigger = getConvergence() > 0 && (override || isConvergent());
		} else if (LOG.isTraceEnabled())
			LOG.trace("update: unique but equally good candidate: " + candidate); //$NON-NLS-1$

		if (trigger && getProcessor() != null)
			getProcessor().onUpdate(candidate);

		return trigger;
	}

	/**
	 * This is the number of equivalent calls to
	 * {@link #updateInternal(Valuable, boolean)} that we pass over before we
	 * trigger the processor and return true. Essentially, this is final, but we
	 * have to allow it so be updated because there is a
	 * {@link Population_Managed#setConvergentGenerations(int)} method which we
	 * need to accommodate. If this value is less than or equal to zero, the
	 * first time we get a better value, we trigger the processor and return
	 * true.
	 */
	private int convergence = 0;

	/**
	 * This is the processor which will be invoked after trigger becomes true in
	 * {@link #updateInternal(ComparableValue, boolean)}. If this field is null,
	 * then no processor will be invoked.
	 */
	final ProcessBest<T> _processor;

	/**
	 * the identifier which describes what this is the best of.
	 */
	private final String _id;

	/**
	 * the number of times that we have been presented with an equivalent best
	 * object, since the last actual update.
	 */
	private int count = 0;

	/**
	 * the current best object
	 */
	private T object;

	protected static final Log LOG = LogFactory.getLog(Best_.class);

	/**
	 * @param generations
	 * @param convergent
	 *            XXX
	 * @param count
	 *            XXX
	 */
	@SuppressWarnings({ "nls", "boxing" })
	private static void logConvergent(final int generations, final boolean convergent, final int count) {
		if (convergent)
			LOG.info(MessageFormat.format("convergent: count={0}, generations={1}", count, generations));
		else if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format("not convergent: count={0}, generations={1}", count, generations));
	}

}