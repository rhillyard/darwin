/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionMap
 * Created on: Mar 4, 2012
 */

package net.sf.darwin.api.base;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @author rhillya
 * 
 * @param <S>
 *            the type which the source getter gets
 * @param <T>
 *            the type which is put into the option map
 */
public class OptionMap<S, T> extends HashMap<String, T> {

	/**
	 * 
	 */
	public OptionMap() {
		this((OptionSource<S>) null);
	}

	/**
	 * @param defaultMap
	 * @param optionSource
	 *            XXX
	 */
	public OptionMap(final Map<String, ? extends T> defaultMap) {
		this(defaultMap, null);
	}

	/**
	 * @param defaultMap
	 * @param optionSource
	 *            XXX
	 */
	public OptionMap(final Map<String, ? extends T> defaultMap, final OptionSource<S> optionSource) {
		super(defaultMap);
		this._optionSource = optionSource;
		if (this._optionSource != null)
			update(this._optionSource);
	}

	/**
	 * 
	 */
	public OptionMap(final OptionSource<S> optionSource) {
		super();
		this._optionSource = optionSource;
		if (this._optionSource != null)
			update(this._optionSource);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.util.HashMap#get(java.lang.Object)
	 */
	@SuppressWarnings("nls")
	@Override
	public T get(final Object key) {
		final T result = super.get(key);
		if (result != null)
			return result;
		throw new RuntimeException(key + " has not been not set");
	}

	/**
	 * @param key
	 * @return either true or false (the latter is the default if the option
	 *         specified by key is not defined)
	 */
	@SuppressWarnings("nls")
	public boolean getBoolean(final String key) {
		final T result = super.get(key);
		if (result != null) {
			if (result instanceof Boolean)
				return ((Boolean) result).booleanValue();
			throw new RuntimeException(key + " is not a Boolean");
		}
		return false;
	}

	@SuppressWarnings("nls")
	public double getDouble(final String key) {
		final Number number = getNumber(key);
		if (number != null)
			return number.doubleValue();
		throw new RuntimeException("option " + key + " is not set");
	}

	@SuppressWarnings("nls")
	public int getInt(final String key) {
		final Number number = getNumber(key);
		if (number != null)
			return number.intValue();
		throw new RuntimeException("option " + key + " is not set");
	}

	@SuppressWarnings("nls")
	public long getLong(final String key) {
		final Number number = getNumber(key);
		if (number != null)
			return number.longValue();
		throw new RuntimeException("option " + key + " is not set");
	}

	@SuppressWarnings("nls")
	public Number getNumber(final String key) {
		final T result = super.get(key);
		if (result != null) {
			if (result instanceof Number)
				return (Number) result;
			throw new RuntimeException("option " + key + " is not a Number");
		}
		return null;
	}

	/**
	 * @param key
	 * @return the value of key <i>as a String</i>. Note that this will not
	 *         necessarily be the same as the original characters in the source.
	 */
	public String getString(final String key) {
		final T result = super.get(key);
		if (result != null)
			return result.toString();
		return null;
	}

	@SuppressWarnings("nls")
	public URL getUrl(final String key) {
		final T result = super.get(key);
		if (result != null) {
			if (result instanceof URL)
				return (URL) result;
			throw new RuntimeException(key + " has is not an URL");
		}
		return null;
	}

	/**
	 * This method is usually not called explicitly (it is normally called by
	 * the constructor when a source is provided). It can be used to refresh the
	 * options from a (new or updated) source of options.
	 * 
	 * @param source
	 * @return true if anything changed in the map
	 */
	@SuppressWarnings("nls")
	public boolean update(final OptionSource<S> source) {
		if (source == null)
			throw new RuntimeException("option source is null");
		boolean changed = false;
		for (final String key : source.getKeys()) {
			final S sourceValue = source.getOption(key);
			if (sourceValue != null) {
				final T mapValue = transform(key, sourceValue);
				final T previous = put(mapKey(key), mapValue);
				changed |= (previous == null || !previous.equals(mapValue));
			}
		}
		return changed;
	}

	/**
	 * This method should be overridden if the domain of option keys is not the
	 * same as the domain of source keys.
	 * 
	 * @param sourceKey
	 *            the source key
	 * @return the option key
	 * 
	 *         By default, the source key is returned as the option key
	 */
	@SuppressWarnings("static-method")
	protected String mapKey(final String sourceKey) {
		return sourceKey;
	}

	/**
	 * This method should normally be overridden.
	 * 
	 * @param sourceKey
	 *            XXX
	 * @param value
	 * 
	 * @return the value
	 */
	@SuppressWarnings({ "unchecked" })
	protected T transform(final String sourceKey, final S value) {
		return (T) value;
	}

	private static final long serialVersionUID = 3897740961245175797L;

	private final OptionSource<S> _optionSource;

}
