/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Allele_Sex.java
 * Created on Jun 15, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Allele_Binary;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Sexual;
import net.sf.tostring0.ToString;

/**
 * Unlike {@link Allele_Dominance}, which uses upper and lower case to denote
 * dominance, the two different alleles of the {@link Allele_Sex} type are "Y"
 * and "X" for male and female, respectively.
 * 
 * @author Robin Hillyard
 * 
 */
public final class Allele_Sex extends Allele_Binary implements Sexual {

	/**
	 * @param isY
	 */
	public Allele_Sex(final boolean isY) {
		super(isY ? S_Y : S_X, isY, BASES);
	}

	/**
	 * @see net.sf.darwin.core.Sexual#isFemale()
	 */
	@Override
	@ToString(omit = true)
	public boolean isFemale() {
		return !getValueAsBoolean();
	}

	@SuppressWarnings("nls")
	private static final String[] BASES = new String[] { "Y", "X" };

	/**
	 * 
	 */
	private static final long serialVersionUID = 4542969025924082120L;

	/**
	 * Allele_Sex(true)
	 */
	public static final Allele<Boolean> Y = AlleleFactory.makeSexAllele(true);

	/**
	 * Allele_Sex(false)
	 */
	public static final Allele<Boolean> X = AlleleFactory.makeSexAllele(false);

	/**
	 * X
	 */
	public static final String S_X = "X"; //$NON-NLS-1$

	/**
	 * Y
	 */
	public static final String S_Y = "Y"; //$NON-NLS-1$

	/**
	 * sex
	 */
	public static final String SEX = "sex"; //$NON-NLS-1$

}
