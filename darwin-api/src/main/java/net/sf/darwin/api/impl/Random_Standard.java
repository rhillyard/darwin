/**
 * RUBECULA COMMON UTILITIES.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Random_Standard.java
 * Created: 2003
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import org.apache.commons.math.random.JDKRandomGenerator;
import org.apache.commons.math.random.RandomAdaptor;
import org.apache.commons.math.random.RandomGenerator;

/**
 * This final class is a default implementation of Random which utilizes the
 * {@link JDKRandomGenerator} class as its random number generator. There are
 * three constructors for different situations. All of the methods simply
 * delegate to the random number generator.
 * 
 * @author Robin Hillyard
 * 
 */
final public class Random_Standard extends RandomAdaptor {

	/**
	 * Secondary constructor for a new Random_Standard, seeded according to
	 * current date/time.
	 */
	public Random_Standard() {
		this(System.currentTimeMillis());
	}

	/**
	 * Secondary constructor for a new Random_Standard, seeded by the parameter
	 * <code>seed</code>.
	 * 
	 * If you want to turn Random logging on easily for all (or almost all) unit
	 * tests and applications, simply replace {@link JDKRandomGenerator} here
	 * with {@link RandomLogged} and ensure that the logging configuration file
	 * includes com.rubecula.util.random at debug level.
	 * 
	 * @param seed
	 */
	public Random_Standard(final long seed) {
		this(new JDKRandomGenerator());
		setSeed(seed);
	}

	/**
	 * @param randomGenerator
	 */
	public Random_Standard(final RandomGenerator randomGenerator) {
		super(randomGenerator);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3091195482059096887L;

}
