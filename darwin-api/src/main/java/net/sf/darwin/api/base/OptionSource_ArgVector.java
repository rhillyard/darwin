/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionSource_ArgVector
 * Created on: Mar 4, 2012
 */

package net.sf.darwin.api.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.rubecula.beanpot.ArgVector;
import com.rubecula.beanpot.argvector.ArgVectorProcessor;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_Map;

/**
 * @author rhillya
 * 
 */
public class OptionSource_ArgVector<X> implements OptionSource<X> {

	public OptionSource_ArgVector(final ArgVector args, final ArgVectorProcessor_Map<X> argProcessor) {
		super();
		this._args = args;
		this._argProcessor = argProcessor;
		init();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.OptionSource#getKeys()
	 */
	@Override
	public Collection<String> getKeys() {
		return this._map.keySet();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.OptionSource#getOption(java.lang.String)
	 */
	@Override
	public X getOption(final String key) {
		return this._map.get(key);
	}

	/**
	 * 
	 */
	private void init() {
		this._map = new HashMap<>();
		this._args.process(this._argProcessor, this._map);

	}

	private final ArgVector _args;

	private final ArgVectorProcessor<Map<String, X>> _argProcessor;

	private Map<String, X> _map;

}
