/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mortality_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.24 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Mortality;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.random.RandomGenerator;

/**
 * Abstract class which defines the base methods and fields needed to implement
 * the {@link Mortality} interface.
 * 
 * @author Robin Hillyard
 */
@SuppressWarnings("serial")
public abstract class Mortality_ extends Base implements Mortality {

	/**
	 * Protected primary constructor.
	 * 
	 * @param random
	 *            a random number generator
	 */
	protected Mortality_(final RandomGenerator random) {
		super();
		this._random = random;
	}

	/**
	 * Method to calculate the mortality for an organism as 1 less the simple
	 * value of {@link #calculateViability(int, double, double)}, given the age,
	 * fitness and saturation. The viability factor (VF) = (1-IM) if age=0 where
	 * IM is the infant mortality (20%, by default) otherwise, VF =
	 * (1-AF)^(age+1) where AF is the actuarial factor (10%, by default). The
	 * sustainabilityIndex (SI) = 1 if saturation less than 1; else SI = (1 -
	 * saturation)^2.
	 * 
	 * @see net.sf.darwin.core.Mortality#calculateMortality(int,double,double)
	 */
	@Override
	public double calculateMortality(final int age, final double fitness, final double saturation) {

		final double result = 1 - calculateViability(age, fitness, saturation);

		if (LOG.isTraceEnabled()) // this method gets called a lot so we'll
			// "guard" this line of code
			LOG.trace("calculate mortality: age=" + age + ", fitness=" + fitness + ", saturation=" + saturation + ": mortality is " + result); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Mortality_ other = (Mortality_) obj;
		if (getRandom() == null) {
			if (other.getRandom() != null)
				return false;
		} else if (!getRandom().equals(other.getRandom()))
			return false;
		return true;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getRandom() == null) ? 0 : getRandom().hashCode());
		return result;
	}

	/**
	 * @param mortality
	 *            the probability that an organism will die before the next
	 *            generation (a value between 0 and 1).
	 * @return true if that organism is actually marked for death (i.e. if the
	 *         mortality value exceeds the current bias, perturbed by a random
	 *         variation)
	 */
	@Override
	public boolean isMarked(final double mortality) {
		final double threshold = getBias() + getRandom().nextGaussian() * GAUSSIAN_ST_DEV;
		final boolean marked = mortality >= threshold;
		logMortality(mortality, threshold, marked);
		return marked;
	}

	/**
	 * Set the value of bias. Typically set by dependency injection.
	 * 
	 * @param bias
	 *            a value between 0 and 1. If 0 then most organisms will
	 *            probably die, while if 1 then few organisms are likely to die.
	 * 
	 */
	public void setBias(final double bias) {
		this.bias = bias;
	}

	/**
	 * Subclasses where viability depends on age must override this method.
	 * 
	 * TEST
	 * 
	 * @param age
	 * @return 1.0, that is to say mortality depends only on fitness and
	 *         saturation -- not age.
	 */
	@SuppressWarnings("static-method")
	protected double calculateAgeViabilityFactor(final int age) {
		return 1.0;
	}

	/**
	 * @param age
	 * @param fitness
	 * @param saturation
	 * @return the viability, between 0 and 1
	 */
	protected double calculateViability(final int age, final double fitness, final double saturation) {
		return Math.exp(-saturation / fitness) * calculateAgeViabilityFactor(age);
	}

	protected double getBias() {
		return this.bias;
	}

	/**
	 * @return the random number generator
	 */
	protected RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * 
	 */
	private static final double GAUSSIAN_ST_DEV = 0.25;

	/**
	 * the random number generator.
	 */
	private final RandomGenerator _random;

	/**
	 * the current bias. Normally, this should remain at 0.5
	 */
	private transient double bias = 0.5;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Mortality_.class);

	/**
	 * @param mortality
	 * @param threshold
	 * @param marked
	 */
	@SuppressWarnings("boxing")
	private static void logMortality(final double mortality, final double threshold, final boolean marked) {
		if (LOG.isTraceEnabled())
			LOG.trace(MessageFormat.format("Mortality: {0}, threshold: {1}: is marked: {2}", mortality, threshold, marked)); //$NON-NLS-1$
	}

}