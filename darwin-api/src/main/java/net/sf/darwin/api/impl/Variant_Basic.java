/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Variant_Basic.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Variant_;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Variant;

/**
 * A trivial implementation of the {@link Variant} interface. This can be used
 * as a concrete type, or extended.
 * 
 * @author Robin Hillyard
 */
@Lifecycle(permanent = true)
public class Variant_Basic<V> extends Variant_<V> {

	/**
	 * @param identifier
	 * @param value
	 */
	public Variant_Basic(final String identifier, final V value) {
		super(identifier, value);
	}

	/**
	 * @param value
	 */
	public Variant_Basic(final V value) {
		this(value.toString(), value);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6281440360265186376L;

}
