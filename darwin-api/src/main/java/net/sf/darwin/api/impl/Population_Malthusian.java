/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Population_;

/**
 * <p>
 * This concrete class is the default implementation of {@link Population}. It
 * provides no additional functionality over {@link Population_}, other than a
 * public constructor.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.18 $
 */
public final class Population_Malthusian<E, P, G> extends Population_<E, P, G> {

	/**
	 * Public constructor for Population_Malthusian where:
	 * 
	 * @param identifier
	 */
	public Population_Malthusian(final String identifier) {
		super(identifier);
	}

	/**
	 * This simply returns null, such that there is no result.
	 * 
	 * @see net.sf.darwin.api.base.Population_#determineResult()
	 */
	@Override
	protected Object determineResult() {
		return null;
	}

	private static final long serialVersionUID = 7883544999208533004L;

}
