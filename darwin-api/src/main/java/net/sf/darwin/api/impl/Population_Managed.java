/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Population_Managed.java
 * Created on Jan 3, 2010
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import net.sf.darwin.api.base.Managed;
import net.sf.darwin.api.base.Organism_;
import net.sf.darwin.api.base.Population_;
import net.sf.darwin.api.base.Sink_;
import net.sf.darwin.api.base.Susceptible;
import net.sf.darwin.core.Best;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.ExPhen;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Generational;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.ProcessBest;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.ValueException;

/**
 * This abstract class is designed to be a base class for populations whose
 * evolution is "managed". That's to say that we, the programmer, like to play
 * God. For an example of a managed evolution in the real world, see the <a
 * href= "http://en.wikipedia.org/wiki/E._coli_long-term_evolution_experiment">
 * Lenski experiment</a>.
 * 
 * Such managed populations tend to have the following properties:
 * <ul>
 * <li>the concept of a "best" organism;</li>
 * <li>the notion of "convergence" so that we can tell when a population has
 * settled with an appropriately stable best organism;</li>
 * <li>a continuously updated environment which can take any one of several
 * related forms, each of which is the environment for one of the colonies;</li>
 * <li>the ability to spawn daughter colonies, each of which takes as its
 * environment one of the forms of environment mentioned above;</li>
 * <li>a willingness for a colony to commit mass suicide, a la Jonesville, once
 * the best organism is found from one of the other populations;</li>
 * <li>such populations do not undergo completely natural selection - instead
 * they are "managed": in particular, there is the concept of a pardon (or
 * reprieve) for the "best" organisms in a population;</li>
 * <li>typically, such a population is based on asexual reproduction because
 * only with asexual reproduction are we likely to want to interfere with normal
 * natural selection.</li>
 * </ul>
 * An example of such a population would be the population for the traveling
 * salesman problem. The details of how this works are a little different from
 * the Lenski experiment. In the latter experiment there are 12 colonies, but
 * after their initial founding from a single colony, they are kept strictly
 * apart (for 22 years so far). In the traveling salesman problem, after each
 * new client is added and convergence reached, only the "best" colony is
 * retained, and then a number of new colonies are created from the best
 * (founder) colony.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            population
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            population
 */
public abstract class Population_Managed<E, P, G> extends Population_<E, P, G> implements Managed<E, P, G> {

	/**
	 * @param identifier
	 * @param updateSourceProperty
	 *            the name of the property in this population's {@link Taxon}
	 *            that will provide the data for updating the environment when
	 *            necessary.
	 */
	protected Population_Managed(final String identifier, final String updateSourceProperty) {
		super(identifier);
		this.bestInEnvironment = new Best_Organism<>(getIdentifier(), -1, new ProcessBestInEnvironment(updateSourceProperty), 0);
	}

	/**
	 * XXX check that we really still want this stuff with pardons.
	 * 
	 * @see net.sf.darwin.api.base.Population_#midGenerationProcessing()
	 */
	@Override
	public void midGenerationProcessing() {
		super.midGenerationProcessing();
		// TODO check that this is right. Why don't we just use best.getValue()?
		double fittest = ((Number) getTaxon().getPhenome().getData()).doubleValue();
		// XXX is this OK? we want to get the fittest organisms and if necessary
		// grant a reprieve to
		// at least 5 of them.
		final int pardonsPerColony = 5;
		for (final Colony<E, P, G> colony : getColonies()) {
			try {
				fittest = colony.grantPardons(fittest, pardonsPerColony);
			} catch (final FitnessException e) {
				LOG.warn("fitness exception granting pardons: ", e); //$NON-NLS-1$
			}
		}
		// finally make sure that the best organism is going to survive
		final Organism<E, P, G> bestOrganism = getBestInEnvironment().getObject();
		if (bestOrganism != null)
			bestOrganism.setViable(true);
	}

	/**
	 * First, we invoke the super-method, i.e.
	 * {@link Population_#postGenerationCleanup()} and then we invoke
	 * {@link #findAndProcessBestFit()}. If the result of this call is false, we
	 * log a warning.
	 * 
	 * @see net.sf.darwin.api.base.Population_#postGenerationCleanup()
	 */
	@Override
	public void postGenerationCleanup() {
		super.postGenerationCleanup();
		if (!findAndProcessBestFit())
			LOG.warn("generation " + getGeneration() + " did not finish with a final best organism update - probably because the evolution terminated early (for example maximum generation reached)"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * First, we invoke the super-method, i.e.
	 * {@link Population_#preGenerationPreparation()} and then we invoke
	 * {@link Phenome#setData(Object)} with a zero value.
	 * 
	 * TODO check that this setData mechanism is still needed.
	 * 
	 * @see net.sf.darwin.api.base.Population_#preGenerationPreparation()
	 */
	@Override
	public void preGenerationPreparation() {
		super.preGenerationPreparation();
		// set the "fittest" value to 0
		getTaxon().getPhenome().setData(new Double(0));
	}

	/**
	 * First we get the seed population property from the {@link Taxon} system
	 * to which we belong by invoking {@link Taxon#getSeedPopulation()} and
	 * recording the result as <code>number</code>. Then we call
	 * {@link #seedOrganism(Colony, int)} <code>number</code> times. If the
	 * population has changed (it should have), we invoke
	 * {@link Colony#populationChanged(Object)} with "seed members".
	 * 
	 * @see net.sf.darwin.api.base.Population_#seedMembers()
	 * 
	 *      TODO need to do more work here - perhaps seed all colonies (but
	 *      normally there will be exactly one colony when we are seeding a
	 *      {@link Population_Managed}).
	 * 
	 *      TODO also, we use a different mechanism for seeding here than in the
	 *      Colony#seedMembers() method (which is what the super-method uses)
	 */
	@Override
	public void seedMembers() {

		final int number = getTaxon().getSeedPopulation();
		boolean populationChanged = false;
		final Colony<E, P, G> colony = getColony(0);
		for (int i = 0; i < number; i++) {
			final Organism<E, P, G> organism = seedOrganism(colony, i);
			if (organism != null)
				populationChanged = true;
		}

		if (populationChanged)
			colony.populationChanged("seed members"); //$NON-NLS-1$
	}

	/**
	 * @param convergentGenerations
	 */
	public void setConvergentGenerations(final int convergentGenerations) {
		this.convergentGenerations = convergentGenerations;
		getBestInEnvironment().setConvergence(convergentGenerations);
	}

	/**
	 * @param maxColonies
	 *            the maxColonies to set. Note that this is not a hard maximum
	 *            but an approximate maximum.
	 */
	public void setMaxColonies(final int maxColonies) {
		this.maxColonies = maxColonies;
	}

	/**
	 * This is a critical part of the evolutionary process for a
	 * {@link Population_Managed} and is called by the
	 * {@link ProcessBestInEnvironment#onUpdate(Organism)}, which in turn is
	 * invoked when there is a new best organism by
	 * {@link Best_Organism#update(Organism, boolean)}.
	 * 
	 * First, we determine the Colony to which the given <code>organism</code>
	 * belongs and name this <code>founderColony</code>. Next, we invoke the
	 * euphemistically titled {@link #decimatePoorerColonies(Colony)} for
	 * <code>founderColony</code> (i.e. we get rid of all the other colonies). A
	 * kind of ethnic cleansing. Next, we invoke
	 * {@link Susceptible#updateFromSource(Best, Object)} on the
	 * <code>founderColony</code>'s environment, and assuming that all is OK, we
	 * invoke {@link #makeDaughterColonies(Colony, Environment)} which creates a
	 * set of (new) daughter colonies, which have environments which are
	 * variations of the <code>founderColony</code>'s environment.
	 * 
	 * @param founderColony
	 *            the "best" colony which is the only colony to remain and which
	 *            will be the founder of the new daughter colonies.
	 * 
	 * @param source
	 *            the source from which the environment will be updated when
	 *            appropriate.
	 * 
	 * @return true if the environment was actually updated and at least one new
	 *         colony was created.
	 * 
	 * @see net.sf.darwin.api.base.Managed#updateEnvironmentAndColonies(Colony,
	 *      java.lang.Object)
	 */
	@Override
	public boolean updateEnvironmentAndColonies(final Colony<E, P, G> founderColony, final Object source) {
		decimatePoorerColonies(founderColony);
		final Environment<E> founderEnvironment = founderColony.getEnvironment();
		if (founderEnvironment instanceof Susceptible) {
			if (((Susceptible) founderEnvironment).updateFromSource(getBestInEnvironment(), source))
				return makeDaughterColonies(founderColony, founderEnvironment) > 0;
				return false;
		}
		return false;
	}

	/**
	 * Do some logging regarding this population. Then we invoke the
	 * super-method, {@link Population_#wrapUp(String)}.
	 * 
	 * @see net.sf.darwin.api.base.Population_#wrapUp(String)
	 */
	@SuppressWarnings("nls")
	@Override
	public void wrapUp(final String cause) {
		try {
			final Sink sink = getSink();
			Sink_.sinkOrLog("Wrapping population: " + getIdentifier() + " after " + getGeneration() //$NON-NLS-1$
					+ " generations because: " + cause, sink, LOG);
			Sink_.sinkOrLog("Best fitness attained: " + getBestInEnvironment().getValue(), sink, LOG);
			for (final Colony<E, P, G> colony : getColonies()) {
				Sink_.sinkOrLog(showEnvironment1(colony.getEnvironment()), sink, LOG);
				Sink_.sinkOrLog(showEnvironment2(colony.getEnvironment()), sink, LOG);
			}
			sink.flush();
		} catch (final IOException e) {
			LOG.error("I/O exception in wrapup", e);
		} catch (final ValueException e) {
			// In practice, this will never occur.
			LOG.error("Value exception in wrapup", e);
		}

		// Remember to invoke the super method
		super.wrapUp(cause);
	}

	/**
	 * Generally speaking, we invoke {@link #processBestFit(ProcessBest)} with
	 * an appropriate processor.
	 * 
	 * See, for example, processBestFit(ProcessBest) method in the class
	 * Population_TS.
	 * 
	 * @return true if all well
	 */
	protected boolean findAndProcessBestFit() {
		final Phenome<P> phenome = getTaxon().getPhenome();
		// TODO check that the following is OK
		for (@SuppressWarnings("unused")
		final Colony<E, P, G> colony : getColonies())
			phenome.getFitnessEngine().setFitnessAdjustment(phenome.getCharacterKeys(), Double.valueOf(1));
		return true;
	}

	/**
	 * @return {@link #bestInEnvironment}
	 */
	protected Best_Organism<E, P, G> getBestInEnvironment() {
		return this.bestInEnvironment;
	}

	/**
	 * @return {@link #convergentGenerations}.
	 */
	protected int getConvergentGenerations() {
		return this.convergentGenerations;
	}

	/**
	 * @return {@link #maxColonies}. Note that this is not a hard maximum but an
	 *         approximate maximum.
	 */
	protected int getMaxColonies() {
		return this.maxColonies;
	}

	/**
	 * @param env
	 *            XXX
	 * @return true if the environment is stable, i.e. has not changed.
	 */
	protected abstract boolean isEnvironmentStable(Environment<E> env);

	/**
	 * 
	 * @param organism
	 * @param phenotype
	 * @throws ValueException
	 *             an exception that could be thrown when getting the value.
	 *             However, in practice, this exception should never be thrown
	 *             once an organism as been crowned as best.
	 */
	protected abstract void logBestFitness(final Organism<E, P, G> organism, final Phenotype<P> phenotype) throws ValueException;

	protected abstract void logBestOrganism(Colony<E, P, G> colony);

	/**
	 * @param founderColony
	 * @param founderEnvironment
	 * @return true if all is well
	 */
	protected abstract int makeDaughterColonies(final Colony<E, P, G> founderColony, final Environment<E> founderEnvironment);

	/**
	 * @param founderColony
	 * @param size
	 *            the number of possible variations of environment from which to
	 *            make new colonies.
	 * @param addition
	 *            the object which is newly added and has to be moved into
	 *            different positions for each new colony.
	 * @return the number of daughter colonies actually created
	 */
	protected int makeDaughterColonies(final Colony<E, P, G> founderColony, final int size, final Object addition) {
		LOG.info("makeDaughterColonies (before): total population: " + getTotal()); //$NON-NLS-1$
		final int daughters = size <= getMaxColonies() ? size : getMaxColonies();
		final int odds = (size + daughters - 1) / daughters;
		// Give us plenty of space for daughter identifiers because max colonies
		// is only an approximate bound.
		founderColony.setNDaughters(daughters + getMaxColonies());
		int result = 0;
		try {
			for (int i = 0; i < size - 1; i++) {
				if (daughters == size || founderColony.spin(odds)) {
					addColony(makeDaughterColony(founderColony, addition, i));
					result++;
				}
			}
			thinColonies(getCount());
			LOG.info("makeDaughterColonies (after): total population: " + getTotal()); //$NON-NLS-1$
			return result;
		} catch (final CloneNotSupportedException e) {
			throw new DarwinException("logic error: colony not cloneable", e); //$NON-NLS-1$
		} catch (final FitnessException e) {
			throw new DarwinException("logic error: colony not cloneable", e); //$NON-NLS-1$
		}
	}

	/**
	 * clone the founderColony to create a new colony with the newClient
	 * appropriately positioned in the new colony's environment.
	 * 
	 * @param founderColony
	 * @param addition
	 * @param pos
	 * @return the new colony.
	 * @throws CloneNotSupportedException
	 */
	protected abstract Colony<E, P, G> makeDaughterColony(final Colony<E, P, G> founderColony, final Object addition,
			final int pos) throws CloneNotSupportedException;

	/**
	 * This method processes each of the {@link Organism}s in the population by
	 * normalizing its genome in reference to the "best" genome. The actual work
	 * is performed by
	 * {@link Organism_#normalizeGenome(Genome, Map, Environment)} where the
	 * parameters are the bestGenome, a cache, and the environment for this
	 * population.
	 * 
	 * TODO this is slow
	 */
	protected void normalizeGenomes() {
		final Organism<E, P, G> organism = getBestInEnvironment().getObject();
		organism.getColony().normalizeGenomes(organism);
	}

	/**
	 * TODO this needs to be rewritten.
	 * 
	 * This method is called by {@link #findAndProcessBestFit()} which in turn
	 * is called by {@link #postGenerationCleanup()}. Here is the sequence:
	 * <ol>
	 * <li>call {@link #findBestOrganism(ProcessBest)} with
	 * <code>bestOfGenerationProcessor</code> as the parameter;</li>
	 * <li>the result is then passed into
	 * {@link Best#update(net.sf.tostring0.Identifiable, boolean)} on the object
	 * returned from {@link #getBestInEnvironment()}, along with
	 * {@link Taxon#isLastGeneration()}.</li>
	 * </ol>
	 * </li> </ol>
	 * 
	 * @param bestOfGenerationProcessor
	 *            the object that will process organisms as they replace the
	 *            current best organism for this generation.
	 * 
	 * @return true if we updated #best.
	 * @throws ValueException
	 */
	protected boolean processBestFit(final ProcessBest<Organism<E, P, G>> bestOfGenerationProcessor) throws ValueException {
		// First, we find the best organism of all the organisms currently in
		// this population
		final Best_Organism<E, P, G> bestOrganismOfGeneration = findBestOrganism(bestOfGenerationProcessor);

		// Next we take the best organism found and, if appropriate, update the
		// best for the current environment
		return getBestInEnvironment().update(bestOrganismOfGeneration.getObject(), getTaxon().isLastGeneration());
	}

	/**
	 * Typical behavior for this method is to loop through the extended
	 * phenotypes provided and for each invoke
	 * {@link ExPhen#applyToEnvironment(Object)}, passing in the
	 * <code>criterion</code>. If the result is true for any extended phenotype,
	 * the returned result will be true.
	 * 
	 * @param exphens
	 * @param criterion
	 * @return true if the environment of an extended phenotype has been updated
	 */
	protected abstract boolean processExtendedPhenotypes(final Collection<ExPhen<P>> exphens, final Object criterion);

	/**
	 * @param colony
	 *            XXX
	 * @param i
	 *            application-specific parameter
	 * @return a newly created organism based on the integer i
	 */
	protected abstract Organism<E, P, G> seedOrganism(Colony<E, P, G> colony, int i);

	/**
	 * @param environment
	 *            XXX
	 * @return a String for the first descriptive message about the environment
	 */
	protected abstract String showEnvironment1(Environment<E> environment);

	/**
	 * @param environment
	 *            XXX
	 * @return a String for the second descriptive message about the environment
	 */
	protected abstract String showEnvironment2(Environment<E> environment);

	/**
	 * Reduce the population of each colony of this population by the
	 * thinFactor.
	 * 
	 * @param thinFactor
	 *            the factor by which each colony's population will be thinned.
	 * @throws FitnessException
	 */
	protected void thinColonies(final int thinFactor) throws FitnessException {
		for (final Colony<E, P, G> colony : getColonies()) {
			colony.thin(thinFactor);
		}
	}

	/**
	 * This method annihilates the non-best colonies for this population.
	 * 
	 * @param bestColony
	 */
	private <F, U, W> void decimatePoorerColonies(final Colony<F, U, W> bestColony) {
		final Iterator<Colony<E, P, G>> iterator = getColonyList().listIterator();
		while (iterator.hasNext()) {
			if (!iterator.next().equals(bestColony))
				iterator.remove();
		}
	}

	/**
	 * Get the best fit for all of the organisms belonging to this population at
	 * a given time, normally, at the close of the processing of a generation,
	 * i.e. it is called indirectly from
	 * {@link Generational#postGenerationCleanup()}.
	 * 
	 * For each colony in this populatiion, get the best organism, invoking the
	 * method {@link ProcessBest#onUpdate(net.sf.tostring0.Identifiable)} of
	 * processor for each new best organism.
	 * 
	 * 
	 * TODO for now we just run through all the colonies. More work to do.
	 * 
	 * @param processor
	 *            an object to process each new best organism
	 * 
	 * @return a {@link Best_Organism} object which has munged through all of
	 *         the organisms of this population and processed each new best
	 *         organism using the processor.
	 * @throws ValueException
	 */
	private Best_Organism<E, P, G> findBestOrganism(final ProcessBest<Organism<E, P, G>> processor) throws ValueException {
		final Collection<Colony<E, P, G>> colonies = getColonies();
		if (colonies.isEmpty())
			throw new DarwinException("population " + getIdentifier() + " " + "has no colonies"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		final Best_Organism<E, P, G> result = new Best_Organism<>(getIdentifier(), getGeneration());
		for (final Colony<E, P, G> colony : colonies) {
			final Best_Organism<E, P, G> bestInColony = new Best_Organism<>(colony.getIdentifier(), getGeneration(),
					colony.getOrganisms(), processor);
			result.update(bestInColony.getObject(), false);
		}
		return result;
	}

	private static final long serialVersionUID = 5812115202733907787L;

	/**
	 * The best (i.e. fittest) Organism since the last change of environment
	 * (and normalization) occurred.
	 */
	private final transient Best_Organism<E, P, G> bestInEnvironment;

	protected transient int convergentGenerations = 10;

	private int maxColonies = 10;

	/**
	 * Type which provides a callback method which is called whenever a new best
	 * organism in the environment is found. This event only occurs at the end
	 * of a generation. Typically, there will be a convergence factor, which
	 * means that we don't jump on the very first generation in which there is
	 * an improvement, but rather we wait until things have settled a bit.
	 */
	public final class ProcessBestInEnvironment implements ProcessBest<Organism<E, P, G>> {
		ProcessBestInEnvironment(final String sourceProperty) {
			super();
			this._sourceProperty = sourceProperty;
		}

		/**
		 * Callback method (see above).
		 * 
		 * @see net.sf.darwin.core.ProcessBest#onUpdate(net.sf.tostring0.Identifiable)
		 */
		@Override
		public void onUpdate(final Organism<E, P, G> object) {
			final Organism<E, P, G> organism = object;
			final Phenotype<P> phenotype = organism.getPhenotype();
			if (phenotype != null) {
				if (processExtendedPhenotypes(phenotype.getExtendedPhenotypes(), getCriterion())) {
					try {
						logBestFitness(organism, phenotype);
					} catch (final ValueException e) {
						LOG.info("processBestFit(): unexpected exception" + e.getLocalizedMessage()); //$NON-NLS-1$
					}
					normalizeGenomes();
				} else
					LOG.info(MessageFormat.format("processBestFit(): no update occurred for {0} and {1}", //$NON-NLS-1$
							organism, getCriterion()));
				final Colony<E, P, G> colony = organism.getColony();
				final boolean updated = updateEnvironmentAndColonies(colony, getTaxon().getProperty(this._sourceProperty));

				if (!updated && isEnvironmentStable(colony.getEnvironment()))
					wrapUp("environment is stable"); //$NON-NLS-1$
				return;
			}
			throw new DarwinException("processBestFit(): organism already dead: " + organism); //$NON-NLS-1$
		}

		/**
		 * @param criterion
		 *            the criterion to set
		 */
		public void setCriterion(final Object criterion) {
			this.criterion = criterion;
		}

		/**
		 * @return {@link #criterion}.
		 */
		protected Object getCriterion() {
			return this.criterion;
		}

		private final String _sourceProperty;

		/**
		 * 
		 */
		private Object criterion;
	}

}
