/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Genomic_;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Ploidy;

/**
 * <p>
 * Concrete implementation of {@link Genomic} representing the possible genetic
 * information contained in a Taxon. A Genomic_Diploid contains exactly one
 * chromosome which is sex-linked.<br>
 * Note that there is a sub-type, {@link Genomic_Sexual}, which should normally
 * be used rather than this type.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.5 $
 */
@Lifecycle(permanent = true)
public class Genomic_Diploid<T, V> extends Genomic_<T, V> {

	/**
	 * TEST
	 * 
	 * Constructor for a new Genomic_Diploid instance using the given meiosis
	 * implementation and no mutation.
	 * 
	 * @param identifier
	 *            the identifier for this Genomic.
	 * @param meiosis
	 *            an implementer of {@link Meiosis}.
	 */
	public Genomic_Diploid(final String identifier, final Meiosis<V> meiosis) {
		super(identifier, Ploidy.DIPLOID, meiosis);
	}

	/**
	 * Constructor for a new Genomic_Diploid instance using the given meiosis
	 * implementation and the given mutator implementation.
	 * 
	 * @param identifier
	 * @param meiosis
	 * @param mutator
	 */
	public Genomic_Diploid(final String identifier, final Meiosis<V> meiosis, final Mutator<V> mutator) {
		super(identifier, Ploidy.DIPLOID, meiosis, mutator);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8305182888274182993L;

}
