/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Genomic_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.30 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;

import net.sf.darwin.api.impl.Chromosome_Sex;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class implementing base methods for the {@link Chromosome}
 * interface.
 * 
 * There is no specific support for Epistasis since the ordering of the loci set
 * that is returned by {@link #getLoci()} is essentially random. If you need to
 * allow certain genes to suppress the expression of other genes, then it would
 * probably be best to extend this class.
 * 
 * TODO consider extending AuditableList instead of Base
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Chromosome_<V> extends Base implements Chromosome<V> {

	/**
	 * @param identifier
	 *            the identifier for this chromosome.
	 * @param alphabet
	 *            TODO
	 * @param sexLinked
	 *            true if this chromosome is sex-linked
	 * 
	 */
	protected Chromosome_(final String identifier, final String alphabet, final boolean sexLinked) {
		super();
		this.alphabet = alphabet;
		this._sexLinked = sexLinked;
		this._loci = new AuditableList<>(identifier);
	}

	/**
	 * add locus and set the locus to point to this Chromosome as its owner.
	 * Note: do not use this to add a polygenic loci, except for the first of
	 * each group.
	 * 
	 * @param index
	 * @param locus
	 * 
	 * @throws GeneticsException
	 * 
	 * @see net.sf.darwin.core.Chromosome#addLocus(int,
	 *      net.sf.darwin.core.Locus)
	 */
	@Override
	public void addLocus(final int index, final Locus<V> locus) throws GeneticsException {
		add(index, locus);
		locus.setParent(this);
	}

	/**
	 * Method to add a Locus at the end of this Genomic.
	 * 
	 * Generally, we prefer to use {@link #setLoci(Collection)} instead.
	 * 
	 * @see #addLocus(int, Locus)
	 * @param locus
	 *            a locus to be inserted into this Genomic.
	 * @return true, always.
	 * @throws GeneticsException
	 */
	@Override
	public synchronized boolean addLocus(final Locus<V> locus) throws GeneticsException {
		addLocus(getCount(), locus);
		return true; // provided that no exception is thrown in add, the result
		// should be true.
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Chromosome_ other = (Chromosome_) obj;
		if (getLociList() == null) {
			if (other.getLociList() != null)
				return false;
		} else if (!getLoci().equals(other.getLoci()))
			return false;
		if (isSexLinked() != other.isSexLinked())
			return false;
		return true;
	}

	/**
	 * @return the alphabet
	 */
	@Override
	public String getAlphabet() {
		return this.alphabet;
	}

	/**
	 * @return the number of loci in this chromosome.
	 * @see net.sf.darwin.core.Chromosome#getCount()
	 */
	@Override
	public int getCount() {
		return getLociList().size();
	}

	/**
	 * @return result of invoking {@link Identifiable#getIdentifier()} on
	 *         {@link #getLociList()}.
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getLociList().getIdentifier();
	}

	/**
	 * @return the loci as a collection of Locus objects.
	 */
	@Override
	@ToString(detail = 1)
	// treat loci as a detail rather than as a child
	public Collection<Locus<V>> getLoci() {
		return getLociList();
	}

	/**
	 * @return the indexth locus.
	 * 
	 * @see net.sf.darwin.core.Chromosome#getLocus(int)
	 */
	@Override
	public synchronized Locus<V> getLocus(final int index) {
		return getLociList().get(index);
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getLociList() == null) ? 0 : getLociList().hashCode());
		result = prime * result + (isSexLinked() ? 1231 : 1237);
		return result;
	}

	/**
	 * @return value of field {@link #_sexLinked}.
	 * 
	 * @see net.sf.darwin.core.Chromosome#isSexLinked()
	 */
	@Override
	@ToString
	public boolean isSexLinked() {
		return this._sexLinked;
	}

	/**
	 * This is now the preferred method to set the loci. Any prior loci will be
	 * forgotten. Note: do not include any polygenic loci, except for the first
	 * of each group.
	 * 
	 * @param loci
	 */
	@Override
	public void setLoci(final Collection<Locus<V>> loci) {
		clear();
		addAll(loci);
		for (final Locus<V> locus : loci)
			locus.setParent(this);
	}

	protected void add(final int index, final Locus<V> locus) throws GeneticsException {
		final boolean present = contains(locus);
		if (!present)
			getLociList().add(index, locus);
		else
			throw new GeneticsException("Chromosome_.add(): locus already present: " + locus); //$NON-NLS-1$
	}

	protected boolean add(final Locus<V> o) {
		return getLociList().add(o);
	}

	protected boolean addAll(final Collection<? extends Locus<V>> loci) {
		return getLociList().addAll(loci);
	}

	protected void clear() {
		getLociList().clear();
	}

	protected boolean contains(final Object locus) {
		return getLociList().contains(locus);
	}

	protected int indexOf(final Object locus) {
		return getLociList().indexOf(locus);
	}

	protected boolean isEmpty() {
		return getLociList().isEmpty();
	}

	protected int lastIndexOf(final Object locus) {
		return getLociList().lastIndexOf(locus);
	}

	/**
	 * Actually remove a locus, even if it is the sex locus (unless this method
	 * is overridden by {@link Chromosome_Sex}.
	 * 
	 * @param index
	 * @return the element previously at the specified position.
	 */
	protected Locus<V> remove(final int index) {
		return getLociList().remove(index);
	}

	/**
	 * Actually remove a locus, even if it is the sex locus (unless this method
	 * is overridden by {@link Chromosome_Sex}.
	 * 
	 * @param locus
	 * @return true if the list contained the object.
	 */
	protected boolean remove(final Object locus) {
		return getLociList().remove(locus);
	}

	protected boolean retainAll(final Collection<?> c) {
		return getLociList().retainAll(c);
	}

	protected Object[] toArray() {
		return getLociList().toArray();
	}

	protected Locus<V>[] toArray(final Locus<V>[] a) {
		return getLociList().toArray(a);
	}

	/**
	 * @return
	 */
	@ToString(omit = true)
	private AuditableList<Locus<V>> getLociList() {
		return this._loci;
	}

	private final String alphabet;

	private final boolean _sexLinked;

	private final AuditableList<Locus<V>> _loci;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Chromosome_.class);

}