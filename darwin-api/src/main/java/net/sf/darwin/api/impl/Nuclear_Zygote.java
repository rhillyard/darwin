/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Nuclear_Zygote.java
 * Created on Feb 20, 2009
 * @version $Revision: 1.20 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Nuclear_;
import net.sf.darwin.api.factory.NuclearFactory;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Nuclear;
import net.sf.darwin.core.Sexual;
import net.sf.tostring0.ToString;

/**
 * Concrete implementation of {@link Nuclear} for sexual reproduction.
 * 
 * @author Robin Hillyard
 * 
 */
@ToString(showIdentifier = false, showClass = false, allBeanMethods = false)
@Lifecycle(permanent = false)
public final class Nuclear_Zygote<T, V> extends Nuclear_<T, V> implements Sexual {

	/**
	 * Package-scope constructor, invoked by {@link NuclearFactory}. Concrete
	 * implementation of {@link Nuclear} for sexual reproduction.
	 * 
	 * @param genome
	 * @param colonyId
	 */
	public Nuclear_Zygote(final Genome<T, V> genome, final String colonyId) {
		super(genome, colonyId);
		try {
			// XXX check if this is OK
			setFemale(genome.getSex());
		} catch (final GeneticsException e) {
			throw new DarwinException("Nuclear_Zygote constructor exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @return true if female.
	 */
	@Override
	@ToString
	public boolean isFemale() {
		return this.female;
	}

	/**
	 * Reset the cached version of sex (derived by expressing the sex gene).
	 * This should only be invoked when you really know what you're doing, for
	 * example, you want to simulate the ability of certain fish, or amphibians
	 * which can change sex during their lifetime.
	 * 
	 * @param female
	 */
	void setFemale(final boolean female) {
		this.female = female;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8816179971904984999L;

	/**
	 * This is essentially a cached version of {@link Genome#getSex()} applied
	 * to the result of {@link #getGenome()} during the construction of the
	 * object.
	 */
	private transient boolean female;

}
