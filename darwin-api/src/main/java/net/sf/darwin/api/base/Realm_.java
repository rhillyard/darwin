/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Realm_.java
 * Created on Aug 7, 2009
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;

import net.sf.darwin.api.impl.FitnessCache_Standard;
import net.sf.darwin.api.impl.PhenotypeCache_Standard;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Version;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Base implementation of {@link Realm}.
 * 
 * @author Robin Hillyard
 * 
 */
public abstract class Realm_ implements Realm {

	/**
	 * Protected constructor.
	 */
	protected Realm_() {
		this(new PhenotypeCache_Standard(), new FitnessCache_Standard());
	}

	/**
	 * Protected constructor.
	 * 
	 * @param phenotypeCache
	 * @param fitnessCache
	 */
	protected Realm_(final PhenotypeCache_Standard phenotypeCache, final FitnessCache_Standard fitnessCache) {
		super();
		this._phenotypeCache = phenotypeCache;
		this._fitnessCache = fitnessCache;
		LOG.info(MessageFormat.format("Darwin {0} initializing", Version.DARWIN_VERSION)); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Realm#flushCaches()
	 */
	@Override
	public void flushCaches() {
		getFitnessCache().flush();
		getPhenotypeCache().flush();
	}

	/**
	 * @see net.sf.darwin.core.Realm#getFitnessCache()
	 */
	@Override
	public FitnessCache_Standard getFitnessCache() {
		return this._fitnessCache;
	}

	/**
	 * @see net.sf.darwin.core.HasPhenotypeCache#getPhenotypeCache()
	 */
	@Override
	public PhenotypeCache_Standard getPhenotypeCache() {
		return this._phenotypeCache;
	}

	/**
	 * @param active
	 */
	public void setFitnessCacheActive(final boolean active) {
		getFitnessCache().setActive(active);
	}

	/**
	 * @param active
	 */
	public void setPhenotypeCacheActive(final boolean active) {
		getPhenotypeCache().setActive(active);
	}

	private final PhenotypeCache_Standard _phenotypeCache;

	private final FitnessCache_Standard _fitnessCache;

	protected static final Log LOG = LogFactory.getLog(Realm_.class);

}
