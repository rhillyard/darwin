/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Organism_Asexual.java
 * 
 */

package net.sf.darwin.api.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.base.Mate_;
import net.sf.darwin.api.base.Organism_;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.NuclearFactory;
import net.sf.darwin.api.factory.OrganismFactory;
import net.sf.darwin.core.Asexual;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lek;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Nuclear;
import net.sf.darwin.core.Organism;

/**
 * <p>
 * This concrete class represents an individual organism within a Taxon . It
 * contains a Genome from which may be derived a Phenotype in the context of a
 * Genomic.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.47 $
 */
public class Organism_Asexual<E, P, G> extends Organism_<E, P, G> implements Asexual<E, P, G> {

	/**
	 * Package-level constructor (only invoked by OrganismFactory)
	 * 
	 * @param identifier
	 * @param colony
	 * @param progeny
	 */
	public Organism_Asexual(final String identifier, final Colony<E, P, G> colony, final Nuclear<P, G> progeny) {
		super(identifier, colony, progeny);
	}

	/**
	 * Unused constructor required only for type hierarchy purposes
	 */
	@Deprecated
	protected Organism_Asexual() {
		this(null, null, null);
		throw new DarwinException("unsupported constructor"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return null always
	 * @see net.sf.darwin.core.Organism#createMate(net.sf.darwin.core.MateChoice,
	 *      net.sf.darwin.core.Organism)
	 */
	@Override
	public Mate_ createMate(final MateChoice chooser, final Organism<E, P, G> female) {
		return null;
	}

	/**
	 * @return null always
	 * @see net.sf.darwin.core.Organism#createPairBond(net.sf.darwin.core.MateChoice,
	 *      Lek_Standard, Lek_Standard)
	 */
	@Override
	public Mating createPairBond(final MateChoice chooser, final Lek<E, P, G> lek, final Lek<E, P, G> alternativeLek) {
		return null;
	}

	/**
	 * @param howMany
	 * @return a collection of progeny from the given organism.
	 */
	@Override
	public Collection<Organism<E, P, G>> getAsexualProgeny(final int howMany) {
		final Colony<E, P, G> colony = getColony();
		final Genomic<P, G> genomic = colony.getPopulation().getTaxon().getGenomic();
		final Collection<Organism<E, P, G>> result = new ArrayList<>();
		for (int gravis = 0; gravis < howMany; gravis++) {
			final Organism<E, P, G> neonate = ((Asexual<E, P, G>) this).reproduce(colony.createIdentifier(), genomic);
			result.add(neonate);
			if (LOG.isTraceEnabled())
				LOG.trace("New progeny for population (asexual): " + colony.getIdentifier() + ":" + neonate.getIdentifier() + " and genome: " + neonate.getGenome()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Asexual#reproduce(java.lang.String,
	 *      net.sf.darwin.core.Genomic)
	 */
	@Override
	public Organism<E, P, G> reproduce(final String identifier, final Genomic<P, G> genomic) {
		if (getColony() != null)
			return reproduce(genomic, getGenome(), getColony());
		throw new DarwinException("Organism_Asexual.reproduce(): is dead"); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7867362438672119164L;

	/**
	 * @param identifier
	 * @param colony
	 * @param genome
	 * @return a new asexual organism which is the result of calling
	 *         {@link NuclearFactory#makeClone(Genome, String)} and passing its
	 *         result to
	 *         {@link OrganismFactory#makeOrganism(Nuclear, Colony, String)} .
	 */
	public static <E, U, W> Organism<E, U, W> makeProgeny(final String identifier, final Colony<E, U, W> colony,
			final Genome<U, W> genome) {
		final Nuclear<U, W> progeny = NuclearFactory.makeClone(genome, colony.getIdentifier());
		return OrganismFactory.makeOrganism(progeny, colony, identifier);
	}

	/**
	 * Class method to create a new Organism without parents (for seeding a
	 * population).
	 * 
	 * @param colony
	 *            the population to be seeded.
	 * @return a new organism created out of "whole cloth".
	 */
	public static <E, U, W> Organism<E, U, W> seed(final Colony<E, U, W> colony) {
		final Genome<U, W> genome = GenomeFactory.makeGamete(colony.getPopulation().getTaxon().getGenomic());
		final Organism<E, U, W> organism = Organism_Asexual.makeProgeny(colony.createIdentifier(), colony, genome);
		colony.addOrganism(organism);
		if (LOG.isDebugEnabled())
			LOG.debug("Organism_Asexual.seed(): new organism: " + organism); //$NON-NLS-1$
		return organism;
	}

	/**
	 * @param genome
	 * @param mutant
	 */
	private static <U, W> void logMutant(final Genome<U, W> genome, final Genome<U, W> mutant) {
		if (LOG.isTraceEnabled() && !mutant.equals(genome))
			LOG.trace(MessageFormat.format("Organism_Asexual.reproduce: {0} mutated into {1}", genome, mutant)); //$NON-NLS-1$
	}

	private static <E, U, W> Organism<E, U, W> reproduce(final Genomic<U, W> genomic, final Genome<U, W> genome,
			final Colony<E, U, W> colony) {
		final Mutator<W> mutator = genomic.getMutator();
		final Genome<U, W> mutant = mutator.mutate(genome);
		// TODO simplify
		// XXX For now, at least, we try to simplify the genome here.
		// Ideally, we should only simplify when we change the environment,
		// because that might attract a few more mutations, but doing it
		// here should be OK.
		mutator.simplify(mutant, genomic.getExpressers());

		logMutant(genome, mutant);

		return Organism_Asexual.makeProgeny(colony.createIdentifier(), colony, mutant);
	}

}
