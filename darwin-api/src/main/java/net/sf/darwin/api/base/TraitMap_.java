/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TraitMap_.java
 * Created on Feb 6, 2007
 * @version $Revision: 1.23 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.TraitMap;
import net.sf.tostring0.ToString;

/**
 * Abstract class to define base methods and fields for a String-keyed map of
 * Variant objects.
 * 
 * TODO whereas in practice the keys are the character keys, and whereas we use
 * a phenotype cache which is based on {@link TraitMap}, we should change the
 * initialization of the {@link #getTraitMap()} field to be a {@link TreeMap},
 * which will thus give us the traits, ordered by character key.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed in this
 *            trait map
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
abstract public class TraitMap_<P> extends Base implements TraitMap<P> {

	/**
	 * 
	 */
	protected TraitMap_() {
		super();
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present("sig: " + getSignature(), context); //$NON-NLS-1$
		return true;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object o) {
		return getTraitMap().equals(o);
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return getTraits();
	}

	/**
	 * @return the signature for this TraitMap
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getSignature();
	}

	/**
	 * @return the set of trait keys
	 */
	@Override
	public Set<String> getKeys() {
		return getTraitMap().keySet();
	}

	/**
	 * @return the traits as a String made up of character ":" variant "," ...
	 *         variant. That is to say something which uniquely describes this
	 *         TraitMap and can be used for determine a cache key.
	 * @see net.sf.darwin.core.TraitMap#getSignature()
	 */
	@Override
	@ToString(omit = true)
	public String getSignature() {
		final StringBuilder result = new StringBuilder();
		for (final String key : getTraitMap().keySet())
			result.append(key + ":" + getTraitMap().get(key).getValue() + " "); //$NON-NLS-1$//$NON-NLS-2$
		return result.toString();
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.TraitMap#getTrait(java.lang.String)
	 */
	@Override
	public Trait<P> getTrait(final String character) {
		return getTraitMap().get(character);
	}

	/**
	 * TEST
	 * 
	 * @return the set of traits.
	 */
	@Override
	@ToString
	public Collection<Trait<P>> getTraits() {
		return getTraitMap().values();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getTraitMap().hashCode();
	}

	/**
	 * @return the number of traits in this TraitMap
	 * 
	 * @see net.sf.darwin.core.TraitMap#size()
	 */
	@Override
	public int size() {
		return getTraitMap().size();
	}

	/**
	 * @param key
	 *            the character key for this trait
	 * @param value
	 *            the trait
	 * @throws DarwinException
	 *             if the key is already present. This should never happen but
	 *             if your application really thinks this is OK, then you can
	 *             catch the exception. But you'll still only have one value for
	 *             the trait.
	 */
	protected void put(final String key, final Trait<P> value) throws DarwinException {
		if (getTraitMap().put(key, value) != null)
			throw new DarwinException("TraitMap_.put(): logic error: key already present in traits: " + key); //$NON-NLS-1$
	}

	/**
	 * @return
	 */
	private Map<String, Trait<P>> getTraitMap() {
		return this.traits;
	}

	private final Map<String, Trait<P>> traits = new TreeMap<>();

}
