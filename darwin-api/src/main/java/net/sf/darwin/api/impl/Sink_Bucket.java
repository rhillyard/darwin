/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Sink_Bucket.java
 * Created on Sep 16, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.api.impl;

import java.io.IOException;

import net.sf.darwin.api.base.Sink_;

/**
 * This is the bit bucket. Stuff appended here just goes up to the great bit
 * bucket in the sky.
 * 
 * @author Robin Hillyard
 * 
 */
final public class Sink_Bucket extends Sink_ {

	/**
	 * 
	 */
	public Sink_Bucket() {
		super();
	}

	/**
	 * Do nothing
	 * 
	 * @see java.lang.Appendable#append(java.lang.CharSequence)
	 */
	@Override
	public Appendable append(final CharSequence csq) throws IOException {
		// Do nothing
		return this;
	}

}
