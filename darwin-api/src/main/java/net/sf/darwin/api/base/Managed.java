/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Managed.java
 * Created on Jan 20, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Population;

/**
 * Interface to define the operations on a {@link Population} where after
 * stabilizing on a best (fittest) solution, we update the environment, kill off
 * the colonies other than the one containing the best organism, create daughter
 * colonies, each of which has a differently modified environment based on the
 * original environment.
 * 
 * Typically, but not required, a Managed population also can influence its
 * environment(s) via the extended phenotypes of organisms, much like a beaver
 * does with its dams (or we humans do with just about everything).
 * 
 * @author Robin Hillyard
 * 
 */
public interface Managed<E, P, G> {

	/**
	 * This method is responsible for updating the environment (from the given
	 * source) for the given colony in such a way that variations of the update
	 * are applied to a number of new daughter colonies. Additionally, any other
	 * colonies belonging to this population are eliminated.
	 * 
	 * @param founderColony
	 *            XXX
	 * @param source
	 *            the source from which the environment will be updated when
	 *            appropriate.
	 * 
	 * @return true if the environment was actually updated and at least one new
	 *         colony was created.
	 */
	public abstract boolean updateEnvironmentAndColonies(Colony<E, P, G> founderColony, Object source);

}