/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Boss
 * Created on: Oct 24, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * This is a marker interface which marks its implementer as being the object in
 * charge of another object. Used in conjunction with {@link Notifiable}.
 * 
 * @author rhillya
 * 
 */
public interface Boss {
	// This interface has no method signatures
}
