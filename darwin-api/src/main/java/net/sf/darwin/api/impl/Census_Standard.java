/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003,2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_Standard.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.api.impl;

import java.io.PrintWriter;

import net.sf.darwin.api.base.Census_Sink;
import net.sf.darwin.core.Censusible;

/**
 * Class to provide a default implementation of {@link Census_Sink}
 * 
 * @author Robin Hillyard
 */
public final class Census_Standard extends Census_Sink {

	/**
	 * @param printWriter
	 */
	public Census_Standard(final PrintWriter printWriter) {
		super(printWriter);
	}

	/**
	 * output individual to print writer.
	 * 
	 * @see net.sf.darwin.core.Census#census(Censusible, int, Object)
	 */
	@Override
	public void census(final Censusible individual, final int depth, final Object context) {
		super.census(individual, depth, context);
		//		getPrintWriter().println("Census: " //$NON-NLS-1$
		//				+ individual.toString() + " in context: " + context); //$NON-NLS-1$
	}

}