/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Runnable_Safe
 * Created on: Oct 17, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public abstract class Runnable_Safe implements Runnable {

	/**
	 * 
	 */
	public Runnable_Safe() {
		super();
	}

	/**
	 * At some later stage, we might go back and check to see if there was a
	 * throwable left, but bear in mind that the running of this class may be
	 * done asynchronously.
	 * 
	 * @return the throwable if one was generated, else null. CONSIDER
	 *         generalization
	 * 
	 */
	public UIException getThrowable() {
		return this.throwable;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			runSafely();
		} catch (final UIException e) {
			System.err.println("UI exception thrown running safely: " + e); //$NON-NLS-1$
			setThrowable(e);
		} catch (final Throwable e) {
			System.err.println("unexpected exception thrown running safely: "); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	protected abstract void runSafely() throws UIException;

	/**
	 * @param throwable
	 *            the throwable to set
	 */
	private void setThrowable(final UIException throwable) {
		this.throwable = throwable;
	}

	private UIException throwable = null;

}
