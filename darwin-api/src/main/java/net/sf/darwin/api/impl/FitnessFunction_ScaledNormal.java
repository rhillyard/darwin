/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_Normal.java
 * Created on Apr 6, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FitnessFunction_;
import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.core.Lifecycle;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.Notation;

/**
 * This class implements an approximate normal distribution but it is scaled so
 * that the value at the mean is unity rather than the area under the entire
 * curve being unit.
 * 
 * To be honest, I'm not sure whether this has a sound mathematical basis for
 * use in evolutionary computations but I'm using it for now.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public class FitnessFunction_ScaledNormal extends FitnessFunction_ {

	/**
	 */
	public FitnessFunction_ScaledNormal() {
		this(null);
	}

	/**
	 * @param evaluator
	 */
	public FitnessFunction_ScaledNormal(final Evaluator evaluator) {
		super(evaluator);
	}

	/**
	 * @return "Fitness function: scaled normal distribution"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return "Fitness function: scaled normal distribution"; //$NON-NLS-1$
	}

	/**
	 * @param arguments
	 *            these arguments must be, in order,
	 *            <ol>
	 *            <li>Number: value</li>
	 *            <li>Number: mean</li>
	 *            <li>Number: standard deviation</li>
	 *            </ol>
	 * <br>
	 * 
	 *            Note that we do not protect against cast exceptions.
	 * 
	 * @return result of invoking
	 *         {@link #normalDistributionDensityFunctionScaled(double, double, double)}
	 *         .
	 * @see net.sf.darwin.api.base.darwin.foundation.Function_#standardFunction(Object[])
	 */
	@Override
	protected double standardFunction(final Object... arguments) throws FunctionException {
		// No protection against class cast exceptions
		final String methodPrefix = "FitnessFunction_ScaledNormal.standardValue(): "; //$NON-NLS-1$
		if (arguments.length == 3)
			return normalDistributionDensityFunctionScaled(((Number) arguments[0]).doubleValue(),
					((Number) arguments[1]).doubleValue(), ((Number) arguments[2]).doubleValue());
		throw new FunctionException(methodPrefix + "requires 3 arguments"); //$NON-NLS-1$)
	}

	/**
	 * 
	 * @see net.sf.darwin.api.base.darwin.foundation.Function_#standardFunctionTokens(Notation)
	 */
	@Override
	protected CharSequence[] standardFunctionTokens(final Notation notation) {
		switch (notation) {
		case RPN:
			return standardFunctionTokensRPN();
		default:
			return standardFunctionTokensAlgebraic();
		}
	}

	/**
	 * @return <code>e^(-(value-target)^2/2/shapeFactor)</code> as a sequence of
	 *         tokens.
	 */
	@SuppressWarnings("nls")
	private static CharSequence[] standardFunctionTokensAlgebraic() {
		return new CharSequence[] { Concept.e, Concept.pow, "(-(" + VAR_VALUE + "-" + VAR_TARGET + ")", Concept.pow,
				"2/2/" + VAR_SHAPE_FACTOR + ")" }; //$NON-NLS-1$//$NON-NLS-2$
	}

	/**
	 * @return <code>E $shapeFactor 2 * / $value $target - + 2 ^ - * ^</code> as
	 *         a sequence of tokens.
	 */
	@SuppressWarnings("nls")
	private static CharSequence[] standardFunctionTokensRPN() {
		return new CharSequence[] {
				Concept.e,
				PREFIX_VAR_RPN + VAR_SHAPE_FACTOR + " 2 * / " + PREFIX_VAR_RPN + VAR_VALUE + " " + PREFIX_VAR_RPN + VAR_TARGET
				+ " - + 2", Concept.pow, "- *", Concept.pow };
	}

}
