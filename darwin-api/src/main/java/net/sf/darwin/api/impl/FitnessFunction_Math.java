/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_Math.java
 * Created on Apr 6, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.FitnessFunction;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.NormalDistribution;
import org.apache.commons.math.distribution.NormalDistributionImpl;

/**
 * TODO This type is obsolete (never referenced) and appears to be in the wrong
 * place.
 * 
 * This type has an explicit dependency on the Apache Commons MathUtil Project.
 * 
 * @author Robin Hillyard
 * 
 */
public class FitnessFunction_Math implements FitnessFunction {

	/**
	 * 
	 */
	@Deprecated
	public FitnessFunction_Math() {
		super();
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.FitnessFunction#getFitness(double, double,
	 *      double)
	 */
	@Override
	public double getFitness(final double value, final double target, final double factor) {
		final NormalDistribution normal = new NormalDistributionImpl(target, factor);
		try {
			// XXX note that the documentation says that it's easy to get
			// the probability density function (PDF) from the cumulative
			// distribution function (CDF) but it doesn't say how.
			// Therefore, I'm using a very simplistic method.
			final double probability0 = normal.cumulativeProbability(value);
			final double probability1 = normal.cumulativeProbability(value + DELTA);
			return (probability1 - probability0) / DELTA;
		} catch (final MathException e) {
			throw new DarwinException("FitnessFunction_Math.getFitness(): exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @return "FitnessFunction_Math"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return "FitnessFunction_Math"; //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final double DELTA = 0.0001;

}
