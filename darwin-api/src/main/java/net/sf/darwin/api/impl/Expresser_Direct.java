/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser_Direct.java
 * Created on Jun 10, 2009
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.api.impl;

import java.util.Map;

import net.sf.darwin.api.base.Expresser_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Trait;

/**
 * This expresser is suitable for haploid genomes. There is a simple one-to-one
 * correspondence between alleles and traits (not dominance, recession, etc.).
 * If you are looking for an expresser for the situation where there is but one
 * allele, with differing values, then consider using the
 * {@link Expresser_Function} class.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            expresser
 */
public final class Expresser_Direct<P> extends Expresser_<P> {

	/**
	 * @param character
	 * @param mapping
	 *            a map of allele keys to variant keys
	 * @param type
	 *            TODO
	 */
	public Expresser_Direct(final Pharacter<P> character, final Map<String, String> mapping, final Object type) {
		super(character, type);
		this._mapping = mapping;
	}

	/**
	 * Express a gene by getting the allele's value and generating a new trait
	 * with value derived from the allele's value.
	 * 
	 * @param genes
	 *            a variable number of Gene objects.
	 * @return a trait which is the expression of the given <code>genes</code>
	 * 
	 * @see net.sf.darwin.core.Expresser#express(Colony,
	 *      net.sf.darwin.core.Gene[])
	 */
	@Override
	public <G> Trait<P> express(final Gene<G>... genes) {
		if (genes.length == 1) {
			final Gene<G> gene = genes[0];
			if (gene.getPloidy() == Ploidy.HAPLOID) {
				try {
					return createTrait(this._mapping.get(gene.getAlleleKey(0)));
				} catch (final ArrayIndexOutOfBoundsException e) {
					throw new DarwinException(S_METHOD_EXPRESS + "index problem for gene: " + gene, e); //$NON-NLS-1$
				}
			}
			throw new DarwinException(S_METHOD_EXPRESS + "invalid gene (not diploid)"); //$NON-NLS-1$
		}
		throw new DarwinException(S_METHOD_EXPRESS + "expects only one gene parameter"); //$NON-NLS-1$
	}

	private final Map<String, String> _mapping;

	private static final String S_METHOD_EXPRESS = "Expresser_Direct.express(): "; //$NON-NLS-1$

}
