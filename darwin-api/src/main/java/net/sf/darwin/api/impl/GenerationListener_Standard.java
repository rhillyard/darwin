package net.sf.darwin.api.impl;

import java.io.PrintWriter;

import net.sf.darwin.api.base.GenerationListener_;
import net.sf.darwin.core.Evolvable;

import com.rubecula.beanpot.BeanPot;

/**
 * @author Robin Hillyard
 * 
 */
public final class GenerationListener_Standard extends GenerationListener_ {

	/**
	 * 
	 */
	public GenerationListener_Standard() {
		this(BeanPot.getInstance().getWriter());
	}

	/**
	 * @param writer
	 * 
	 */
	public GenerationListener_Standard(final PrintWriter writer) {
		super(writer);
	}

	/**
	 * @see net.sf.darwin.core.GenerationListener#onGeneration(net.sf.darwin.core.Evolvable)
	 */
	@Override
	public void onGeneration(final Evolvable evolvable) {
		if (evolvable != null)
			this._writer.println("Evolvable " + evolvable.getIdentifier() + ": generation=" //$NON-NLS-1$ //$NON-NLS-2$
					+ evolvable.getGeneration());
		else
			this._writer.println("Evolution is finished"); //$NON-NLS-1$
	}
}