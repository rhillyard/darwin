/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_.java
 * Created on Nov 9, 2009
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;

import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.DarwinException;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class Census_ implements Census {

	/**
	 * 
	 */
	protected Census_() {
		super();
	}

	/**
	 * @see net.sf.darwin.core.Census#census(net.sf.darwin.core.Censusible, int,
	 *      java.lang.Object)
	 */
	@Override
	public void census(final Censusible censusible, final int depth, final Object context) {
		doCensus(this, censusible, depth, context);
	}

	/**
	 * @param censuser
	 *            the implementer of {@link Census}
	 * @param censusible
	 * @param depth
	 * @param context
	 */
	protected static void doCensus(final Census censuser, final Censusible censusible, final int depth, final Object context) {
		final boolean goDeeper = censusible.censusMe(censuser, context);
		if (goDeeper && depth > 0) {
			final Collection<? extends Censusible> children = censusible.getCensusibleChildren();
			if (children != null) {
				for (final Censusible child : children)
					censuser.census(child, depth - 1, context);
			} else
				throw new DarwinException("Census.doCensus(): null children list but depth is needed"); //$NON-NLS-1$
		}
	}

}
