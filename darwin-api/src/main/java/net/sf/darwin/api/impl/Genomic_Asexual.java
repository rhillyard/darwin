/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Renamed (was Genomic_Diploid).
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import java.util.Collection;

import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mutator;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Concrete implementation of {@link Genomic} representing the possible genetic
 * information contained in a Taxon. A Genomic_Asexual contains exactly one
 * non-sex-linked chromosome, which in turn may contain any number of gene Loci.
 * If you need a multi-chromosome haploid genome, then extend Genomic_Haploid.
 * </p>
 * 
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.14 $
 */
@Lifecycle(permanent = true)
public final class Genomic_Asexual<T, V> extends Genomic_Haploid<T, V> implements Chromosome<V> {

	/**
	 * @param identifier
	 */
	public Genomic_Asexual(final String identifier) {
		this(identifier, null, new Mutator_Null<V>());
	}

	/**
	 * @param identifier
	 * @param random
	 *            random number source
	 * @param mutator
	 */
	public Genomic_Asexual(final String identifier, final RandomGenerator random, final Mutator<V> mutator) {
		super(identifier, random, mutator);
		this._chromosome = new Chromosome_NoSex<>("", getAlphabet()); //$NON-NLS-1$
		addChromosome(getChromosome());
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Chromosome#addLocus(int,
	 *      net.sf.darwin.core.Locus)
	 */
	@Override
	public void addLocus(final int index, final Locus<V> locus) throws GeneticsException {
		getChromosome().addLocus(index, locus);
	}

	/**
	 * @see net.sf.darwin.core.Chromosome#addLocus(net.sf.darwin.core.Locus)
	 */
	@Override
	public boolean addLocus(final Locus<V> locus) throws GeneticsException {
		return getChromosome().addLocus(locus);
	}

	/**
	 * @return {@link Identifiable#getIdentifier()} invoked on
	 *         {@link #_chromosome}.
	 * @see Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getChromosome().getIdentifier();
	}

	/**
	 * @see net.sf.darwin.core.Chromosome#getLoci()
	 */
	@Override
	public Collection<Locus<V>> getLoci() {
		return getChromosome().getLoci();
	}

	/**
	 * @see net.sf.darwin.core.SexLinked#isSexLinked()
	 */
	@Override
	public boolean isSexLinked() {
		return getChromosome().isSexLinked();
	}

	/**
	 * @see net.sf.darwin.core.Chromosome#setLoci(java.util.Collection)
	 * 
	 * 
	 *      TEST
	 */
	@Override
	public void setLoci(final Collection<Locus<V>> loci) {
		getChromosome().setLoci(loci);
	}

	/**
	 * @return this._chromosome
	 */
	@ToString(omit = true)
	protected Chromosome<V> getChromosome() {
		return this._chromosome;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8305182888274182993L;

	private final Chromosome<V> _chromosome;

}
