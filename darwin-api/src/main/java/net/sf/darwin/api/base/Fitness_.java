/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Fitness_.java
 * Created on Apr 1, 2009
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;

import net.sf.darwin.api.impl.FitnessFunction_ScaledNormal;
import net.sf.darwin.api.util.MathUtil;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Fitness;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.Function;
import net.sf.darwin.core.FunctionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.jexpression.EvalExpression;
import com.rubecula.jexpression.Term;

/**
 * This type represents the fitness of a Trait/EcoFactor combination.
 * 
 * @author Robin Hillyard
 * 
 */
public abstract class Fitness_ implements Fitness, EvalExpression {

	/**
	 */
	protected Fitness_() {
		this(new FitnessFunction_ScaledNormal(), null);
	}

	/**
	 * @param fitnessFunction
	 * @param functionListener
	 *            a listener to function changes
	 */
	protected Fitness_(final FitnessFunction fitnessFunction, final FunctionListener functionListener) {
		super();
		this._fitnessFunction = fitnessFunction;
		// TODO consider doing this via polymorphism
		if (functionListener != null && fitnessFunction instanceof Function) {
			((Function) fitnessFunction).addListener(functionListener);
		}
	}

	/**
	 * Compare fitness 1 with fitness 2 (fitter values are higher values).
	 * 
	 * @param fitness1
	 * @param fitness2
	 * @return -1 if fitness1 is greater than fitness2, etc.
	 */
	@Override
	public int compare(final double fitness1, final double fitness2) {
		return MathUtil.compare(fitness1, fitness2, getTolerance());
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (this._fitnessFunction == null) {
			if (((Fitness_) obj)._fitnessFunction != null)
				return false;
		} else if (!this._fitnessFunction.equals(((Fitness_) obj)._fitnessFunction))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Fitness#getEnvironmentFactor()
	 */
	@Override
	public Number getEnvironmentFactor() {
		return null;
	}

	/**
	 * TEST
	 * 
	 */
	/**
	 * @see com.rubecula.jexpression.EvalExpression#getExpression()
	 */
	@Override
	public String getExpression() {
		if (getFitnessFunction() instanceof EvalExpression)
			return ((EvalExpression) getFitnessFunction()).getExpression();
		return null;
	}

	/**
	 * @see com.rubecula.jexpression.EvalExpression#getExpressionTerms()
	 */
	@Override
	public Term[] getExpressionTerms() {
		if (getFitnessFunction() instanceof EvalExpression)
			return ((EvalExpression) getFitnessFunction()).getExpressionTerms();
		return new Term[0]; // XXX changed from returning null.
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getFitnessFunction() == null) ? 0 : getFitnessFunction().hashCode());
		return result;
	}

	/**
	 * By default, this method returns null.
	 * 
	 * @see net.sf.darwin.core.Fitness#resetAdjustment(java.lang.Number,
	 *      java.lang.String)
	 */
	@Override
	public void resetAdjustment(final Number factor, final String trait) {
		// FIXME need to ensure that fitness cache is cleared.
		if (factor == null && trait == null)
			throw new DarwinException("unimplemented method"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Fitness#resetEnvironmentFactor(String,
	 *      net.sf.darwin.core.EcoFactor)
	 */
	@Override
	public void resetEnvironmentFactor(final String trait, final EcoFactor factor) {
		// XXX do nothing
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getFitnessFunction().getIdentifier();
	}

	/**
	 * Concrete sub-classes must implement this method to indicate how sharp the
	 * fitness curve should be with respect to the possible values of the
	 * variate.
	 * 
	 * @param key
	 *            the key which determines which bandwidth to get (trait or
	 *            ecofactor).
	 * 
	 * @return a measure of the bandwidth of variate. In a normal distribution,
	 *         bandwidth is identical with standard deviation.
	 * 
	 *         Note that the bandwidth should be in the same domain as the
	 *         <i>scaled</i> values.
	 */
	protected abstract double bandwidth(String key);

	/**
	 * Calculate and return the value of the probability density function
	 * corresponding to the variate value <code>t * sT</code> of a continuous
	 * distribution with mean: <code>f * sF</code> and standard deviation: the
	 * value of {@link #bandwidth(String)}. The actual continuous distribution
	 * used is defined the the {@link #_fitnessFunction}.
	 * 
	 * Implementers of Fitness may override this method if they need something
	 * different.
	 * 
	 * XXX consider renaming some of the parameters (trait -> variant)
	 * 
	 * @param t
	 *            the trait value
	 * @param f
	 *            the eco factor value
	 * @param keyT
	 *            the trait key (identifier) used to determine which scale
	 *            factor to use, using {@link #scaleTrait(String)}
	 * @param keyF
	 *            the eco factor key (identifier) used to determine which scale
	 *            factor to use, using {@link #scaleFactor(String)}
	 * @return the value of
	 *         {@link FitnessFunction#getFitness(double, double, double)} where
	 *         the mean is <code>f * sF</code> and the value is
	 *         <code>t * sT</code> and the standardDeviation is given by
	 *         {@link #bandwidth(String)} and where sT is the value returned
	 *         from the method {@link #scaleTrait(String)} and sF is the value
	 *         returned from the method {@link #scaleFactor(String)}.
	 * @throws FitnessException
	 */
	protected double calculateFitness(final double t, final double f, final String keyT, final String keyF)
			throws FitnessException {
		final double result = getFitnessFunction().getFitness(t * scaleTrait(keyT), f * scaleFactor(keyF), bandwidth(keyF));
		// LOG.debug(LOG_MSG_1, new Object[] { new Double(t), new Double(f), new
		// Double(result) });
		if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format(LOG_MSG_1, new Double(t), new Double(f), new Double(result)));
		return result;
	}

	/**
	 * @return {@link #_fitnessFunction}
	 */
	protected FitnessFunction getFitnessFunction() {
		return this._fitnessFunction;
	}

	/**
	 * @return the tolerance
	 */
	protected double getTolerance() {
		return this.tolerance;
	}

	/**
	 * @param key
	 *            the key (identifier) of the ecoFactor
	 * @return a scale factor to be applied (to the eco factor value) before the
	 *         difference is calculated between the trait as a value and the eco
	 *         factor as a value. By value returned by this base method is 1.0.
	 *         Specific fitness implementations must override if the value is to
	 *         be different.
	 */
	@SuppressWarnings("static-method")
	protected double scaleFactor(final String key) {
		return 1.0;
	}

	/**
	 * @param key
	 *            the key (identifier) of the trait
	 * @return a scale factor to be applied (to the trait value) before the
	 *         difference is calculated between the trait as a value and the eco
	 *         factor as a value. By value returned by this base method is 1.0.
	 *         Specific fitness implementations must override if the value is to
	 *         be different.
	 */
	@SuppressWarnings("static-method")
	protected double scaleTrait(final String key) {
		return 1.0;
	}

	/**
	 * TODO consider making this public
	 * 
	 * @param tolerance
	 *            the tolerance to set
	 */
	protected void setTolerance(final double tolerance) {
		this.tolerance = tolerance;
	}

	/**
	 */
	//	private static final String LOG_MSG_1 = "fitness for trait value: {} and eco factor value: {} = {}"; //$NON-NLS-1$
	private static final String LOG_MSG_1 = "fitness for trait value: {0} and eco factor value: {1} = {2}"; //$NON-NLS-1$

	private double tolerance = 0.00000001;

	/**
	 * The logger for this class.
	 */
	// protected static final Logger LOG =
	// LoggerFactory.getLogger(Fitness_.class);
	protected static final Log LOG = LogFactory.getLog(Fitness_.class);

	private final FitnessFunction _fitnessFunction;

}
