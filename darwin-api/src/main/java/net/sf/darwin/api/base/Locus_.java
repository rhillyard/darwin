/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Locus_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.41 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Progenitor;
import net.sf.darwin.core.SexLinked;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class defining the base methods and fields for an implementer of
 * {@link Locus}.
 * 
 * @author Robin Hillyard
 * @param <G>
 *            the type of the genetic information which is encoded in this Locus
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Locus_<G> extends Base implements Locus<G>, SexLinked {

	/**
	 * Protected method to construct a plain vanilla implementer of
	 * {@link Locus}.
	 * 
	 * @param identifier
	 *            the identifier for this locus.
	 */
	protected Locus_(final String identifier) {
		super();
		this._identifier = identifier;
	}

	/**
	 * Incrementally add the given <code>allele</code> to this {@link Locus}.
	 * The frequency value for the allele will be 1.
	 * 
	 * This is an alternative to the preferred method of setting the alleles:
	 * {@link #setAlleles(Map)}.
	 * 
	 * @param allele
	 *            an allele which is possible at this locus.
	 * @return the result of invoking {@link #add(Allele, int)} with allele and
	 *         1 as the parameters.
	 * @see net.sf.darwin.core.Locus#addAllele(net.sf.darwin.core.Allele)
	 */
	@Override
	public synchronized String addAllele(final Allele<G> allele) {
		final String result = add(allele, 1);
		allele.setLocus(this);
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Locus_ other = (Locus_) obj;
		if (getIdentifier() == null) {
			if (other._identifier != null)
				return false;
		} else if (!getIdentifier().equals(other._identifier))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Locus#getAllele(java.lang.String)
	 */
	@Override
	public Allele<G> getAllele(final String key) {
		return getAlleleMap().get(key);
	}

	/**
	 * @return {@link #_alleleMap}.
	 * @see net.sf.darwin.core.Locus#getAlleleMap()
	 */
	@Override
	@ToString(child = true, priority = 2)
	public Map<String, Allele<G>> getAlleleMap() {
		return this._alleleMap;
	}

	/**
	 * TODO reduce to default scope.
	 * 
	 * @return a map of alleles and their frequencies Note that the returned
	 *         values is essentially a join of two maps, {@link #_alleleMap} and
	 *         {@link #_frequencies}, and so updating the result is just
	 *         updating a temporary copy.
	 */
	@ToString(omit = true)
	public Map<Allele<G>, Integer> getAlleles() {
		final Map<String, Allele<G>> alleleMap = getAlleleMap();
		final Map<Allele<G>, Integer> result = new HashMap<>();
		for (final String key : alleleMap.keySet())
			result.put(alleleMap.get(key), getFrequencies().get(key));
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Locus#getAlleleValues()
	 */
	@Override
	public Collection<Allele<G>> getAlleleValues() {
		return getAlleleMap().values();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Locus#getAlphabet()
	 */
	@Override
	public String getAlphabet() {
		return getChromosome().getAlphabet();
	}

	/**
	 * @return the value of the field {@link #parent}.
	 * @see net.sf.darwin.core.Locus#getChromosome()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Chromosome<G> getChromosome() {
		Progenitor result = this.parent;
		// TODO consider doing this via polymorphism
		while (result instanceof Locus)
			result = ((Locus<G>) result).getParent();
		if (result instanceof Chromosome)
			return (Chromosome<G>) result;
		throw new DarwinException("getChromosome(): logic error: locus does not belong to a chromosome"); //$NON-NLS-1$
	}

	/**
	 * @return the _identifier
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @param allele
	 * @return result of invoking {@link List#indexOf(Object)} on the #list with
	 *         the parameter allele. Will return -1 if the allele is not found.
	 */
	@Override
	public String getKey(final Allele<G> allele) {
		if (allele != null) {
			if (getAlleleMap().containsKey(allele.getIdentifier()))
				return allele.getIdentifier();
			return null;
		}
		throw new DarwinException("Locus_.getKey(): null allele"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Locus#getParent()
	 */
	@Override
	@ToString(parent = true)
	public Progenitor getParent() {
		return this.parent;
	}

	/**
	 * @return null if there is no polygenic locus to which this locus is
	 *         linked, i.e. this locus is either non-polygenic or it is at the
	 *         end of a polygenic chain.
	 * @see net.sf.darwin.core.Locus#getPolygenic()
	 */
	@Override
	@ToString
	public Locus<?> getPolygenic() {
		return this.polygenic;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getIdentifier() == null) ? 0 : getIdentifier().hashCode());
		return result;
	}

	/**
	 * Method to determine if the given allele is legal at this Locus.
	 * 
	 * @param allele
	 *            the allele to be tested
	 * @return true if this allele is defined for (known at) this locus and if
	 *         its frequency is greater than zero.
	 * @see net.sf.darwin.core.Locus#isLegal(java.lang.String)
	 */
	@Override
	public boolean isLegal(final String allele) {
		if (getFrequencies().containsKey(allele)) {
			final Integer frequency = getFrequencies().get(allele);
			return frequency.intValue() > 0;
		}

		// TEST
		return false;
	}

	/**
	 * @see net.sf.darwin.core.SexLinked#isSexLinked()
	 */
	@Override
	public boolean isSexLinked() {
		return getChromosome().isSexLinked();
	}

	/**
	 * Make a gene with the alleles specified by the variable number of allele
	 * indexes.
	 * 
	 * @param alleles
	 *            variable number of alleles indexes.
	 * @return the new Gene.
	 * @see net.sf.darwin.core.Locus#makeGene(java.lang.String[])
	 */
	@Override
	public Gene<G> makeGene(final String... alleles) {
		if (alleles.length == 2)
			return GeneFactory.makeDiploid(this, alleles[0], alleles[1]);
		else if (alleles.length == 1)
			return GeneFactory.makeHaploid(this, alleles[0]);
		else
			throw new DarwinException("makeGene(): ploidy unsupported: " + alleles.length); //$NON-NLS-1$
	}

	/**
	 * @param allele
	 * @param frequency
	 * @return the index of the new allele
	 */
	public String putAllele(final Allele<G> allele, final int frequency) {
		final String key = allele.getIdentifier();
		if (getAlleleMap().put(key, allele) == null) {
			getFrequencies().put(key, Integer.valueOf(frequency));
			allele.setLocus(this);
			onChange();
			return key;
		}
		if (getFrequencies().get(key).intValue() == frequency)
			return key;
		throw new DarwinException("Locus_.putAllele(): attempt to change frequency of existing allele"); //$NON-NLS-1$
	}

	/**
	 * Preferred method of setting up the allele/frequency map (rather than
	 * using add()).
	 * 
	 * @param map
	 */
	public void setAlleles(final Map<? extends Allele<G>, Integer> map) {
		getFrequencies().clear();
		getAlleleMap().clear();
		// TODO consider using entrySet().
		for (final Allele<G> allele : map.keySet()) {
			add(allele, map.get(allele).intValue());
		}
	}

	/**
	 * Set the parent for this locus.
	 * 
	 * @param parent
	 *            either the preceding locus if this locus is polygenic, or the
	 *            chromosome that this locus belongs to.
	 * @see net.sf.darwin.core.Locus#setParent(net.sf.darwin.core.Progenitor)
	 */
	@Override
	public void setParent(final Progenitor parent) {
		this.parent = parent;
	}

	/**
	 * Bean method to set the next locus in a chain of polygenic loci.
	 * 
	 * @param locus
	 */
	public void setPolygenic(final Locus<?> locus) {
		this.polygenic = locus;
		locus.setParent(this);
	}

	/**
	 * @return the number of alleles in this Locus
	 */
	@Override
	public int size() {
		return getAlleleMap().size();
	}

	/**
	 * @return the frequencies
	 */
	@ToString
	protected Map<String, Integer> getFrequencies() {
		return this._frequencies;
	}

	/**
	 * This method is invoked whenever the set of alleles or their frequencies
	 * changes
	 */
	protected void onChange() {
		// do nothing
	}

	/**
	 * Link to a polygenic locus, if any.
	 */
	private Locus<?> polygenic = null;

	/**
	 * frequency map.
	 */
	private final Map<String, Integer> _frequencies = new HashMap<>();

	/**
	 * Map of this locus' alleles, keyed by identifier.
	 */
	private final Map<String, Allele<G>> _alleleMap = new HashMap<>();

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Locus_.class);

	private Progenitor parent;

	private final String _identifier;

}