/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Visibility
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public interface Visibility {

	/**
	 * @return true if we should be drawing this object in the UI
	 * 
	 *         CONSIDER refactoring such that the object generates a "model" to
	 *         be drawn and if that model is null, then we don't draw it.
	 */
	public abstract boolean isVisible();

}