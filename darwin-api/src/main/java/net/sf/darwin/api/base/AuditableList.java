/**
 * RUBECULA COMMON UTILITIES.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 21, 2003
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import net.sf.darwin.core.Auditable;
import net.sf.darwin.core.Base;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

/**
 * <p>
 * This class is an auditable extension of Vector.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.3 $
 * @param <E>
 */
@ToString(allBeanMethods = false)
public class AuditableList<E extends Auditable> extends Base implements List<E>, Identifiable {

	/**
	 * @param identifier
	 * 
	 */
	public AuditableList(final String identifier) {
		this(identifier, new ArrayList<E>());
	}

	/**
	 * @param identifier
	 * @param list
	 * 
	 */
	public AuditableList(final String identifier, final List<E> list) {
		super();
		this._identifier = identifier;
		this._list = list;
	}

	/**
	 * @param o
	 * @return result of calling {@link List#add(Object)} on {@link #_list}.
	 * @see java.util.List#add(java.lang.Object)
	 */
	@Override
	public boolean add(final E o) {
		return getList().add(o);
	}

	/**
	 * @param index
	 * @param element
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	@Override
	public void add(final int index, final E element) {
		if (element != null)
			this._list.add(index, element);
		else
			throw new RuntimeException("AuditableList.add(): null element"); //$NON-NLS-1$
	}

	/**
	 * @param c
	 * @return result of calling {@link List#addAll(Collection)} on
	 *         {@link #_list}.
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return this._list.addAll(c);
	}

	/**
	 * @param index
	 * @param c
	 * @return result of calling {@link List#addAll(int, Collection)} on
	 *         {@link #_list}.
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		return this._list.addAll(index, c);
	}

	/**
	 * 
	 * @see java.util.List#clear()
	 */
	@Override
	public void clear() {
		getList().clear();
	}

	/**
	 * @param o
	 * @return result of calling {@link List#contains(Object)} on {@link #_list}
	 *         .
	 * @see java.util.List#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(final Object o) {
		return getList().contains(o);
	}

	/**
	 * @param c
	 * @return result of calling {@link List#containsAll(Collection)} on
	 *         {@link #_list}.
	 * @see java.util.List#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(final Collection<?> c) {
		return getList().containsAll(c);
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (getIdentifier() == null) {
			if (((Identifiable) obj).getIdentifier() != null)
				return false;
		} else if (!getIdentifier().equals(((Identifiable) obj).getIdentifier()))
			return false;
		if (this._list == null) {
			if (((AuditableList) obj)._list != null)
				return false;
		} else if (!this._list.equals(((AuditableList) obj)._list))
			return false;
		return true;
	}

	/**
	 * @param index
	 * @return result of calling {@link List#get(int)} on {@link #_list}.
	 * @see java.util.List#get(int)
	 */
	@Override
	public E get(final int index) {
		return this._list.get(index);
	}

	/**
	 * @return {@link #_identifier}
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getIdentifier() == null) ? 0 : getIdentifier().hashCode());
		result = prime * result + ((getList() == null) ? 0 : getList().hashCode());
		return result;
	}

	/**
	 * @param o
	 * @return result of calling {@link List#indexOf(Object)} on {@link #_list}.
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	@Override
	public int indexOf(final Object o) {
		return this._list.indexOf(o);
	}

	/**
	 * @return result of calling {@link List#isEmpty()} on {@link #_list}.
	 * @see java.util.List#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return getList().isEmpty();
	}

	/**
	 * @return result of calling {@link List#iterator()} on {@link #_list}.
	 * @see java.util.List#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return getList().iterator();
	}

	/**
	 * @param o
	 * @return result of calling {@link List#lastIndexOf(Object)} on
	 *         {@link #_list}.
	 * @see java.util.List#lastIndexOf(java.lang.Object)
	 */
	@Override
	public int lastIndexOf(final Object o) {
		return this._list.lastIndexOf(o);
	}

	/**
	 * @return result of calling {@link List#listIterator()} on {@link #_list}.
	 * @see java.util.List#listIterator()
	 */
	@Override
	public ListIterator<E> listIterator() {
		return this._list.listIterator();
	}

	/**
	 * @param index
	 * @return result of calling {@link List#listIterator(int)} on
	 *         {@link #_list}.
	 * @see java.util.List#listIterator(int)
	 */
	@Override
	public ListIterator<E> listIterator(final int index) {
		return this._list.listIterator(index);
	}

	/**
	 * @param index
	 * @return result of calling {@link List#remove(int)} on {@link #_list}.
	 * @see java.util.List#remove(int)
	 */
	@Override
	public E remove(final int index) {
		return this._list.remove(index);
	}

	/**
	 * @param o
	 * @return result of calling {@link List#remove(Object)} on {@link #_list}.
	 * @see java.util.List#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(final Object o) {
		return this._list.remove(o);
	}

	/**
	 * @param c
	 * @return result of calling {@link List#removeAll(Collection)} on
	 *         {@link #_list}.
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(final Collection<?> c) {
		return this._list.removeAll(c);
	}

	/**
	 * @param c
	 * @return result of calling {@link List#retainAll(Collection)} on
	 *         {@link #_list}.
	 * @see java.util.List#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(final Collection<?> c) {
		return this._list.retainAll(c);
	}

	/**
	 * @param index
	 * @param element
	 * @return result of calling {@link List#set(int, Object)} on {@link #_list}
	 *         .
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	@Override
	public E set(final int index, final E element) {
		return this._list.set(index, element);
	}

	/**
	 * @return result of calling {@link List#size()} on {@link #_list}.
	 * @see java.util.List#size()
	 */
	@Override
	public int size() {
		return getList().size();
	}

	/**
	 * @param fromIndex
	 * @param toIndex
	 * @return result of calling {@link List#subList(int, int)} on
	 *         {@link #_list}.
	 * @see java.util.List#subList(int, int)
	 */
	@Override
	public List<E> subList(final int fromIndex, final int toIndex) {
		return this._list.subList(fromIndex, toIndex);
	}

	/**
	 * @return result of calling {@link List#toArray()} on {@link #_list}.
	 * @see java.util.List#toArray()
	 */
	@Override
	public Object[] toArray() {
		return getList().toArray();
	}

	/**
	 * @param <T>
	 * @param a
	 * @return result of calling {@link List#toArray(Object[])} on
	 *         {@link #_list}.
	 * @see java.util.List#toArray(T[])
	 */
	@Override
	public <T> T[] toArray(final T[] a) {
		return getList().toArray(a);
	}

	/**
	 * @return {@link #_list} as a Collection of E objects
	 */
	@ToString(detail = 1)
	protected List<E> getList() {
		return this._list;
	}

	/**
	 * Used only by {@link Object#clone()}. TODO If we implement that method in
	 * this class, we can make this private.
	 * 
	 * @param list
	 *            the list to set
	 */
	protected void setList(final List<E> list) {
		this._list = list;
	}

	/**
	 * The identifier by which this AuditableVector is known.
	 */
	final private String _identifier;

	private List<E> _list;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6148934358685676281L;

}
