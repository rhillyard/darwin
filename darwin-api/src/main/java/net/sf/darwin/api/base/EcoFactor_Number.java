/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 10, 2003
 *
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Visualizable;
import net.sf.tostring0.ToString;

/**
 * <p>
 * This abstract implementer of {@link EcoFactor} represents an environmental
 * factor with a double value.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.12 $
 */
@SuppressWarnings("serial")
public abstract class EcoFactor_Number extends EcoFactor_<Number> implements Quantifiable {

	/**
	 * Construct an EcoFactor_Number from an identifier string and a value.
	 * 
	 * @param identifier
	 * @param value
	 */
	public EcoFactor_Number(final String identifier, final Number value) {
		super(identifier, value);
	}

	/**
	 * @see com.rubecula.darwin.domain.environment.EcoFactor_#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		final EcoFactor_Number clone = (EcoFactor_Number) super.clone();
		if (clone.getValue() instanceof Integer)
			clone.setValue(Integer.valueOf(((Integer) clone.getValue()).intValue()));
		else if (clone.getValue() instanceof Double)
			clone.setValue(Double.valueOf(((Double) clone.getValue()).doubleValue()));
		else
			throw new CloneNotSupportedException("EcoFactor_Number: doesn't support " + clone.getValue().getClass()); //$NON-NLS-1$
		return clone;
	}

	/**
	 * @return the value of this eco factor as a double.
	 * 
	 * @see com.rubecula.darwin.domain.helper.Quantifiable#doubleValue()
	 */
	@Override
	public double doubleValue() {
		// This cast should be safe since all values must be Number because
		// there is only one constructor.
		return getValue().doubleValue();
	}

	/**
	 * @see com.rubecula.darwin.domain.environment.EcoFactor_#getVisualizable()
	 */
	@Override
	public Visualizable getVisualizable() {
		return null;
	}

	/**
	 * @see net.sf.darwin.core.Individual#isNew()
	 */
	@Override
	@ToString(omit = true)
	public boolean isNew() {
		return false;
	}

	/**
	 * @see net.sf.darwin.core.Individual#isVisible()
	 */
	@Override
	@ToString(omit = true)
	public boolean isVisible() {
		return false;
	}

	/**
	 * @param other
	 * @return true if this {@link EcoFactor} has been updated
	 */
	@Override
	public boolean update(final EcoFactor<Number> other) {
		return false;
	}

}
