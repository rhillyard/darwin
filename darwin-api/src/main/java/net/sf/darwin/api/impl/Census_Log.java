/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_Log.java
 * Created on Nov 9, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Census_;

import org.apache.commons.logging.Log;

/**
 * @author Robin Hillyard
 * 
 */
public final class Census_Log extends Census_ {

	/**
	 * @param logger
	 *            a logger to output the census information.
	 * @param debug
	 *            if true then use debug method to output census information,
	 *            otherwise we use trace method for the output.
	 * 
	 */
	public Census_Log(final Log logger, final boolean debug) {
		super();
		this._logger = logger;
		this._debug = debug;
	}

	/**
	 * @param string
	 * @see net.sf.darwin.core.Census#present(java.lang.String, Object)
	 */
	@Override
	public void present(final String string, final Object context) {
		String message = ""; //$NON-NLS-1$
		if (context != null)
			message += "[" + context + "] "; //$NON-NLS-1$ //$NON-NLS-2$
		message += string;
		if (this._debug && this._logger.isDebugEnabled())
			this._logger.debug(message);
		else if (this._logger.isTraceEnabled())
			this._logger.trace(message);
	}

	private final Log _logger;

	private final boolean _debug;

}
