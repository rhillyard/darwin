/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: NumericFrequencyMap.java
 * Created on Nov 6, 2009
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.HasMean;

/**
 * This is a {@link FrequencyMap_HashMap} where the keys each have an intrinsic
 * value, yielded by invoking {@link Number#doubleValue()}.
 * 
 * @author Robin Hillyard
 * 
 */
public class NumericFrequencyMap extends FrequencyMap_HashMap<Number> implements HasMean {

	/**
	 * @param name
	 */
	public NumericFrequencyMap(final String name) {
		super(name);
	}

	/**
	 * @return the weighted mean of the values in the map.
	 */
	@Override
	public double mean() {
		double result = 0;
		for (final Number key : keySet())
			result += (get(key) * key.doubleValue());
		return result / getTotal();
	}
}
