/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution_.java
 * Created on Jul 31, 2009
 * @version $Revision: 1.39 $
 */

package net.sf.darwin.api.base;

import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.security.AccessControlException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import net.sf.darwin.api.impl.EvolutionTask;
import net.sf.darwin.core.ClockWatcher;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.GenerationListener;

/**
 * TODO this class (and its subclasses) almost certainly belongs only in the
 * platform and not here in the API. Need to check though.
 * 
 * This class is the base implementation of the Evolution_ interface. It is what
 * drives the evolution of a set of {@link Evolvable} objects. That is to say
 * that it drives the running of a genetic algorithm, or genetic program.
 * 
 * It can run in automatic mode (initiated by a call to
 * {@link #start(int, boolean)}, or in manual mode (steps, each invoked by
 * calling {@link #next()}.
 * 
 * Although not a singleton in the strict sense, there will normally only be one
 * of these objects.
 * 
 * FIXME application does not terminate cleanly when the rate of evolution is
 * too fast (the evolution terminates appropriately but a task is left hanging
 * so that the JVM doesn't release the executable).
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
abstract public class Evolution_ extends Evolver_ implements Evolution {

	/**
	 * 
	 */
	protected Evolution_() {
		super();
		this._eventExecutor = Executors.newScheduledThreadPool(2);
		this._eventFutures = new ArrayList<>();
		this._threadFactory = new EvolutionThreadFactory();
	}

	/**
	 * @return the number of ticks since this Generator was started, or null if
	 *         this evolution has not been started.
	 */
	@Override
	public long getClock() {
		if (getEvolutionTask() != null)
			return getEvolutionTask().getClock();
		return super.getClock();
	}

	/**
	 * @throws EvolutionException
	 * @see net.sf.darwin.core.Evolution#isActive(boolean)
	 */
	@Override
	public boolean isActive(final boolean ignoreEvents) throws EvolutionException {
		final boolean evolutionActive = getEvolutionExecutor() != null && getEvolutionTask() != null
				&& getEvolvableKeys().size() > 0;
				if (evolutionActive) {
					final boolean active = checkEvolutionActive();
					if (!active)
						return false;
				}
				if (ignoreEvents || !evolutionActive)
					return evolutionActive;
				boolean eventsScheduled = false;
				for (final Future<?> future : getEventFutures()) {
					if (!future.isDone()) {
						eventsScheduled = true;
						break;
					}
				}
				return evolutionActive && eventsScheduled;
	}

	/**
	 * @return true if {@link EvolutionTask#isPaused()} returns true for the
	 *         {@link #evolutionTask} (if {@link #evolutionTask} is null then
	 *         return true.
	 * 
	 *         XXX is this OK?
	 * 
	 * @see net.sf.darwin.api.impl.EvolutionTask#isPaused()
	 */
	@Override
	public boolean isPaused() {
		return getEvolutionTask() == null || getEvolutionTask().isPaused();
	}

	/**
	 * Create a new {@link EvolutionTask} and run it. This method has works
	 * quite differently in the two different modes (manual versus auto). When
	 * this {@link Evolution} is in auto mode, we is intended for manual
	 * stepping of the evolutionary process.
	 * 
	 * @return true if there are evolvables still to evolve.
	 * @throws EvolutionException
	 * @see net.sf.darwin.core.Evolution#next()
	 */
	@Override
	public boolean next() throws EvolutionException {
		LOG.debug("Evolution.next()"); //$NON-NLS-1$
		if (isActive(true))
			return getEvolutionTask().tick();
		return super.next();
	}

	/**
	 * TEST
	 * 
	 * @throws EvolutionException
	 * 
	 * @see net.sf.darwin.core.Evolution#pause()
	 */
	@Override
	public void pause() throws EvolutionException {
		if (!getEvolutionTask().isPaused())
			getEvolutionTask().setPaused(true);
		else
			throw new EvolutionException("Evolution_.pause(): already paused"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.api.base.Evolver_#postConfigure(java.lang.Object)
	 */
	@Override
	public void postConfigure(final Object data) {
		if (data != null) {
			if (data instanceof Integer)
				postConfigure(((Integer) data).intValue());
			else
				LOG.warn("Evolution_.postConfigure(): data is not Integer: " + data.getClass()); //$NON-NLS-1$
		} else
			LOG.info("Evolution_.postConfigure(): data is null"); //$NON-NLS-1$
	}

	/**
	 * Method to pre-configure the Evolution object when it is used as part of
	 * an application, as opposed to being in an applet.
	 * 
	 * @see net.sf.darwin.api.base.Evolver_#preConfigure(java.lang.Object)
	 */
	@Override
	public void preConfigure(final Object data) {
		if (data != null) {
			if (data.getClass().isArray()) {
				final Object[] array = (Object[]) data;
				if (array.length == 2)
					if (array[0] instanceof GenerationListener)
						if (array[1] instanceof ClockWatcher)
							preConfigure((GenerationListener) array[0], (ClockWatcher) array[1]);
						else
							LOG.warn("Evolution_.preConfigure(): data[2] is not a ClockWatcher: " + array[1].getClass()); //$NON-NLS-1$
					else
						LOG.warn("Evolution_.preConfigure(): data[1] is not a GenerationListener: " + array[0].getClass()); //$NON-NLS-1$
				else
					LOG.warn("Evolution_.preConfigure(): data array length is not 2: " + array.length); //$NON-NLS-1$
			} else
				LOG.warn("Evolution_.preConfigure(): data is not an array: " + data.getClass()); //$NON-NLS-1$
		} else
			LOG.info("Evolution_.preConfigure(): data is null"); //$NON-NLS-1$
	}

	/**
	 * TEST
	 * 
	 * @throws EvolutionException
	 * 
	 * @see net.sf.darwin.core.Evolution#resume()
	 */
	@Override
	public void resume() throws EvolutionException {
		if (getEvolutionTask().isPaused())
			getEvolutionTask().setPaused(false);
		else
			throw new EvolutionException("Evolution_.resume(): not paused"); //$NON-NLS-1$
	}

	/**
	 * By default, the run method for an Evolution_ object starts the evolution
	 * going at the rate of one tick per second.
	 * 
	 * First, start a new repeating task with parameters
	 * {@link #MILLISECS_PER_SECOND} and true. Then, if
	 * {@link #isWaitUntilComplete()} returns true, we invoke
	 * {@link #waitUntilComplete(int)} with the value of #_waitPeriod.
	 * 
	 * Thus, if you do not want to wait for the evolution to finish, simply
	 * invoke {@link #setWaitUntilComplete(boolean)} with false before calling
	 * run.
	 * 
	 * NOTE: this method may be overridden by sub-classes.
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		startAndWait(MILLISECS_PER_SECOND, true, false);
	}

	/**
	 * 
	 * @return the future which is returned by the scheduler for the event.
	 * @see net.sf.darwin.core.Evolution#scheduleEvent(long, java.lang.Runnable)
	 */
	@Override
	public Future<?> scheduleEvent(final long millisecs, final Runnable event) {
		final Future<?> future = getEventExecutor().schedule(event, millisecs, TimeUnit.MILLISECONDS);
		getEventFutures().add(future);
		return future;
	}

	/**
	 * Called by reflection (during configuration).
	 * 
	 * @param ui
	 */
	public void setUi(final Object ui) {
		this.ui = ui;
		// TODO consider doing this via polymorphism
		if (ui instanceof Evolutionary)
			((Evolutionary) ui).setEvolution(this);
		setWaitUntilComplete(false);
	}

	/**
	 * Called by reflection (during configuration).
	 * 
	 * @param waitUntilComplete
	 *            the waitUntilComplete to set
	 */
	public void setWaitUntilComplete(final boolean waitUntilComplete) {
		this.waitUntilComplete = waitUntilComplete;
	}

	/**
	 * A terminal shut-down of the Evolution process (including both evolution
	 * and scheduled events). Subsequent calls to this Evolution object will not
	 * succeed. Compare {@link #pause()} and {@link #stop()}.
	 * 
	 * @return the result of calling
	 *         {@link ScheduledExecutorService#awaitTermination(long, TimeUnit)}
	 *         which, roughly speaking, will be true if everything shut down in
	 *         an orderly fashion.
	 * @throws EvolutionException
	 * @see net.sf.darwin.core.Evolution#stop()
	 */
	@Override
	public boolean shutdown() throws EvolutionException {
		boolean result = stop();
		if (result)
			result = stopExecutor(getEventExecutor(), 10);
		// scheduledEvolutionTask.cancel(true);
		return result;
	}

	/**
	 * XXX the following comment seems totally wrong.
	 * 
	 * A temporary stop of the Evolution process (scheduled events are not
	 * affected).
	 * 
	 * @return the result of calling
	 *         {@link #stopExecutor(ScheduledExecutorService, int)} which,
	 *         roughly speaking, will be true if everything shut down in an
	 *         orderly fashion.
	 * @throws EvolutionException
	 * @see net.sf.darwin.core.Evolution#stop()
	 */
	@Override
	public boolean stop() throws EvolutionException {
		if (stoppable()) {
			if (getScheduledEvolutionTask() != null && !getScheduledEvolutionTask().isDone()) {
				final boolean cancel = getScheduledEvolutionTask().cancel(true);
				LOG.info("evolution task canceled: " + (cancel ? "OK" : "(maybe already canceled?)")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			if (getEvolutionExecutor() != null) {
				try {
					boolean result = stopExecutor(getEvolutionExecutor(), 1);
					if (getEvolutionExecutor() != null)
						for (final Runnable runnable : getEvolutionExecutor().shutdownNow()) {
							result = false;
							LOG.warn("Task did not complete: " + runnable.toString()); //$NON-NLS-1$
						}
					setEvolutionExecutor(null);
					if (LOG.isDebugEnabled())
						LOG.debug("Evolution executor shut down with result: " + result); //$NON-NLS-1$
					return result;
				} catch (final SecurityException e) {
					throw new EvolutionException("Unable to stop evolution because: ", e); //$NON-NLS-1$
				}
			}
			return true;
		}
		throw new EvolutionException("Evolution is not stoppable"); //$NON-NLS-1$
	}

	/**
	 * @return true if we have permission to stop this {@link Evolution}.
	 */
	@Override
	public boolean stoppable() {
		final String target = "stopThread"; //$NON-NLS-1$
		try {
			final SecurityManager securityManager = System.getSecurityManager();
			if (securityManager != null) {
				securityManager.checkPermission(new RuntimePermission(target));
				if (LOG.isDebugEnabled())
					LOG.debug("permission given for " + target); //$NON-NLS-1$
			}
		} catch (final Exception e) {
			LOG.warn("permission denied for " + target, e); //$NON-NLS-1$
			return false;
		}
		return true;
	}

	/**
	 * Wait for this {@link Evolution} to become inactive, then return.
	 * 
	 * @param delay
	 *            delay between tests (in milliseconds)
	 * 
	 * @return true if the evolution is complete.
	 * @throws EvolutionException
	 */
	@Override
	public boolean waitUntilComplete(final int delay) throws EvolutionException {
		// check that evolution's active, ignoring any scheduled events.
		while (isActive(true)) {
			try {
				Thread.sleep(delay);
			} catch (final InterruptedException e) {
				return false;
			}
		}
		if (isActive(false)) {
			final boolean ok = cancelEvents();
			if (!ok)
				LOG.warn("problem canceling events"); //$NON-NLS-1$
			return ok;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Evolution#writeEvolvables(java.io.Writer)
	 */
	@Override
	public void writeEvolvables(final Writer writer) {
		// TODO do nothing
	}

	/**
	 * TEST
	 * 
	 * @return true if the event was properly canceled.
	 */
	protected boolean cancelEvents() {
		boolean result = true;
		for (final Future<?> future : getEventFutures()) {
			if (!future.isDone()) {
				if (!future.cancel(false)) {
					result = false;
					LOG.error("event could not be canceled: " + future.toString()); //$NON-NLS-1$
				}
			}
		}
		return result;

	}

	@Override
	protected void finalize() throws Throwable {
		if (!shutdown())
			System.err.println("Evolution failed to shut down correctly"); //$NON-NLS-1$
		super.finalize();
	}

	protected ScheduledExecutorService getEventExecutor() {
		return this._eventExecutor;
	}

	/**
	 * @return {@link #_eventFutures}, as a collection of {@link Future}<?>
	 *         objects
	 */
	protected Collection<Future<?>> getEventFutures() {
		return this._eventFutures;
	}

	/**
	 * @return {@link #evolutionTask}
	 */
	protected EvolutionTask getEvolutionTask() {
		return this.evolutionTask;
	}

	/**
	 * TEST
	 * 
	 * @return {@link #ui}, the UI for this object, as defined by
	 *         {@link #setUi(Object)}.
	 */
	protected Object getUi() {
		return this.ui;
	}

	/**
	 * @return {@link #waitUntilComplete}
	 */
	protected boolean isWaitUntilComplete() {
		return this.waitUntilComplete;
	}

	/**
	 * set the value of {@link #evolutionTask}
	 * 
	 * @param evolutionTask
	 *            the evolutionTask to set
	 */
	protected void setEvolutionTask(final EvolutionTask evolutionTask) {
		this.evolutionTask = evolutionTask;
	}

	/**
	 * This method starts a new evolution with generations potentially occurring
	 * at given rate (in milliseconds) [a particular {@link Evolvable} object
	 * might not undergo a new generation for every "tick" of this evolutionary
	 * clock.]
	 * 
	 * The result of calling
	 * {@link ScheduledExecutorService#scheduleAtFixedRate(Runnable, long, long, TimeUnit)}
	 * results in a new thread being started with the name formed from
	 * {@link EvolutionThreadFactory#namePrefix}, space and a sequence number.
	 * 
	 * @param rate
	 *            the rate of ticking, in milliseconds
	 * @param run
	 *            whether to start in the run state (or otherwise in the paused
	 *            state).
	 */
	protected void start(final int rate, final boolean run) {
		logStart(rate, run);
		setEvolutionTask(new EvolutionTask(this, getEvolvables(), getListeners(), getClock(), getClockWatcher()));
		getEvolutionTask().setPaused(!run);
		final ScheduledExecutorService executor = Executors.newScheduledThreadPool(2, getThreadFactory());
		setScheduledEvolutionTask(executor.scheduleAtFixedRate(getEvolutionTask(), rate, rate, TimeUnit.MILLISECONDS));
		setEvolutionExecutor(executor);
	}

	/**
	 * @param rate
	 * @param run
	 * @param stopFirst
	 *            XXX
	 */
	protected void startAndWait(final int rate, final boolean run, final boolean stopFirst) {
		if (stopFirst) {
			try {
				stop();
			} catch (final EvolutionException e) {
				LOG.warn("Evolution_.startAndWait(): exception thrown during stop()", e); //$NON-NLS-1$
			}
		}

		start(rate, run);

		if (isWaitUntilComplete())
			try {
				waitUntilComplete(_waitPeriod);
			} catch (final EvolutionException e) {
				final String message = "Evolution_.startAndWait() caught evolution exception"; //$NON-NLS-1$
				LOG.error(message + ": " + e.getLocalizedMessage()); //$NON-NLS-1$
				throw new RuntimeException(message, e);
			}
	}

	/**
	 * Shutdown the given executor and wait for the tasks to terminate
	 * 
	 * @param executor
	 * @param timeoutSeconds
	 *            the number of seconds to wait for tasks to shutdown.
	 * @return the result of calling
	 *         {@link ScheduledExecutorService#awaitTermination(long, TimeUnit)}
	 * @throws EvolutionException
	 */
	@SuppressWarnings("static-method")
	protected boolean stopExecutor(final ScheduledExecutorService executor, final int timeoutSeconds) throws EvolutionException {
		if (!executor.isShutdown()) {
			try {
				executor.shutdown();
			} catch (final AccessControlException e1) {
				final String message = "Evolution_.stopExecutor() cannot stop thread " //$NON-NLS-1$
						+ Thread.currentThread().getName();
				LOG.error(message, e1);
				throw new EvolutionException(message, e1);
			}
			try {
				return executor.awaitTermination(timeoutSeconds, TimeUnit.SECONDS);
			} catch (final InterruptedException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return
	 * @throws EvolutionException
	 * 
	 */
	private boolean checkEvolutionActive() throws EvolutionException {
		try {
			final Object object = getScheduledEvolutionTask().get(10, TimeUnit.MILLISECONDS);
			LOG.info("scheduled evolution task complete and returned: " + object); //$NON-NLS-1$
			return false;
		} catch (final InterruptedException e) {
			// we ignore this and move on
			return true;
		} catch (final CancellationException e) {
			// we ignore this and move on
			return false;
		} catch (final ExecutionException e) {
			throw new EvolutionException("Evolution_.isActive() caught evolution task exception", e.getCause()); //$NON-NLS-1$
		} catch (final TimeoutException e) {
			// we ignore this and move on
			return true;
		}
	}

	/**
	 * @return {@link #evolutionExecutor}
	 */
	private ScheduledExecutorService getEvolutionExecutor() {
		return this.evolutionExecutor;
	}

	/**
	 * Method to get the evolution task, that is to say the evolutionary clock
	 * that ticks at a specified rate and on whose ticks, evolvables may undergo
	 * new generations.
	 * 
	 * @return {@link #scheduledEvolutionTask}
	 */
	private ScheduledFuture<?> getScheduledEvolutionTask() {
		return this.scheduledEvolutionTask;
	}

	/**
	 * @return the threadFactory
	 */
	private EvolutionThreadFactory getThreadFactory() {
		return this._threadFactory;
	}

	/**
	 * @param maxGenerations
	 */
	private void postConfigure(final int maxGenerations) {
		init();
		if (maxGenerations > 0) {
			final Set<Evolvable> evolvables = getEvolvableKeys();
			for (final Evolvable evolvable : evolvables) {
				((Evolvable_) evolvable).setMaxGenerations(maxGenerations);
			}
		}
	}

	/**
	 * @param generationListener
	 *            an appropriate generation listener
	 * @param clockWatcher
	 *            an appropriate clock watcher
	 */
	private void preConfigure(final GenerationListener generationListener, final ClockWatcher clockWatcher) {
		addListener(generationListener);
		setClockWatcher(clockWatcher);
	}

	/**
	 * @param evolutionExecutor
	 *            the evolutionExecutor to set
	 */
	private void setEvolutionExecutor(final ScheduledExecutorService evolutionExecutor) {
		this.evolutionExecutor = evolutionExecutor;
	}

	/**
	 * Set the evolution task (a repeating task at a predetermined rate and
	 * starting time).
	 * 
	 * @param scheduledEvolutionTask
	 *            the scheduledEvolutionTask to set
	 */
	private void setScheduledEvolutionTask(final ScheduledFuture<?> scheduledEvolutionTask) {
		this.scheduledEvolutionTask = scheduledEvolutionTask;
	}

	/**
	 * The evolution task which is run on a periodic basis by the
	 * #evolutionExecutor.
	 */
	private EvolutionTask evolutionTask;

	/**
	 * The event executor service. This is fixed at construction time.
	 */
	private final ScheduledExecutorService _eventExecutor;

	/**
	 * The evolution executor service. This is set when a new evolution is
	 * started.
	 */
	private transient ScheduledExecutorService evolutionExecutor;

	private transient Object ui;

	/**
	 * A list of futures which correspond to explicitly invoked evolutionary
	 * events (not the regular evolution process).
	 */
	private final Collection<Future<?>> _eventFutures;

	/**
	 * 1000
	 */
	public static final int MILLISECS_PER_SECOND = 1000;

	/**
	 * The evolution task (a repeating task at a predetermined rate and starting
	 * time).
	 */
	private ScheduledFuture<?> scheduledEvolutionTask;

	private final EvolutionThreadFactory _threadFactory;

	/**
	 * This field controls whether or not we wait for the evolution to complete
	 * when running this Evolution bean. When an Evolution is linked to an
	 * Applet, this value gets set to false. Otherwise it is true unless
	 * explicitly set as a property in the configuration file.
	 */
	private boolean waitUntilComplete = true;

	/**
	 * Period between checks to see if the run method has completed (in
	 * milliseconds). Can be set by subclasses if desired.
	 */
	protected static int _waitPeriod = 50;

	/**
	 * Inner class to construct new Threads for the ExecutorService. The first
	 * thread will be called Evolution pool-thread-0, the second EvolutionThread
	 * 1 and so on.
	 * 
	 * It's a pity that we can't simply extend the
	 * Executors$DefaultThreadFactory class.
	 */
	static class EvolutionThreadFactory implements ThreadFactory {
		EvolutionThreadFactory() {
			final SecurityManager s = System.getSecurityManager();
			this.group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
			this.namePrefix = POOL_NAME + poolNumber.getAndIncrement() + S_DASH + S_THREAD;
		}

		@Override
		public Thread newThread(final Runnable r) {
			final Thread thread = new Thread(this.group, r, this.namePrefix + this.threadNumber.getAndIncrement(), 0);
			if (thread.isDaemon())
				thread.setDaemon(false);
			if (thread.getPriority() != Thread.NORM_PRIORITY)
				thread.setPriority(Thread.NORM_PRIORITY);
			// TODO do we actually need to set the uncaught exception handler?
			final UncaughtExceptionHandler catcher = new UncaughtExceptionHandler() {

				@Override
				public void uncaughtException(final Thread t, final Throwable e) {
					final String message = "exception in thread: " + t.getName(); //$NON-NLS-1$
					System.err.println("Evolution_: " + message + ": " + e.getLocalizedMessage()); //$NON-NLS-1$//$NON-NLS-2$
					LOG.warn(message, e);
				}
			};
			thread.setUncaughtExceptionHandler(catcher);
			return thread;
		}

		/**
		 * t
		 */
		private static final String S_THREAD = "t"; //$NON-NLS-1$

		/**
		 * Evol p
		 */
		private static final String POOL_NAME = "Evol p"; //$NON-NLS-1$

		/**
		 * -
		 */
		private static final String S_DASH = "-"; //$NON-NLS-1$

		static final AtomicInteger poolNumber = new AtomicInteger(1);

		final ThreadGroup group;

		final AtomicInteger threadNumber = new AtomicInteger(0);

		final String namePrefix;
	}

	/**
	 * Method to create a String which appropriately represents the accuracy of
	 * the elapsed time.
	 * 
	 * @param millisecs
	 * @return a String which shows the elapsed time and the accuracy of same.
	 */
	public static String getElapsedTime(final long millisecs) {
		return (millisecs / _waitPeriod) / 1000. * _waitPeriod + " seconds (accurate to " + _waitPeriod + " mSec)"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @param rate
	 * @param run
	 */
	@SuppressWarnings("boxing")
	private static void logStart(final int rate, final boolean run) {
		LOG.info(MessageFormat.format("Evolution_.start(): rate={0}ms, run={1}", rate, run)); //$NON-NLS-1$
	}
}
