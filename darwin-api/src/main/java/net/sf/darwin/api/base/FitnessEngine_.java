/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngine_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.14 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.Fitness;
import net.sf.darwin.core.FitnessCache;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Function;
import net.sf.darwin.core.FunctionListener;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.TraitMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.jexpression.Evaluator;

/**
 * Defines the base methods and fields for an implementation of
 * {@link FitnessEngine} interface. Contains no fields, methods or constructors.
 * 
 * @author Robin Hillyard
 */
public abstract class FitnessEngine_<P, E> implements FitnessEngine<P, E>, FunctionListener {

	/**
	 * No-evaluator form of constructor.
	 */
	protected FitnessEngine_() {
		this(null, null);
	}

	/**
	 * Protected Primary Constructor
	 * 
	 * @param evaluator
	 *            the expression evaluator (or null)
	 * @param realm
	 *            the realm this fitness engine operates in.
	 */
	protected FitnessEngine_(final Evaluator evaluator, final Realm realm) {
		super();
		this._realm = realm;
		this._fitnesses = new HashMap<>();
		registerFitnesses(evaluator);
	}

	/**
	 * The base implementation of this method (which should normally be
	 * appropriate for all usage) simply goes through all possible pairings of
	 * trait/ecofactor (the traits belonging to the phenotype and the ecofactors
	 * belonging to the environment) and calculates the weighted product of all
	 * fitnesses. Weights must be positive. Normally, weights will be unity in
	 * which case the resulting fitness is simply the product of all relevant
	 * factors.
	 * 
	 * TODO consider narrowing phenotype type to {@link TraitMap}.
	 * 
	 * @throws FitnessException
	 * 
	 * @see net.sf.darwin.core.FitnessEngine#calculateFitness(net.sf.darwin.core.Phenotype,
	 *      net.sf.darwin.core.Environment)
	 */
	@Override
	public double calculateFitness(final Phenotype<P> phenotype, final Environment<E> environment) throws FitnessException {
		if (countFitnesses() == 0)
			throw new DarwinException("calculateFitness(): fitness engine of " + getClass() //$NON-NLS-1$
					+ " has no registered fitnesses"); //$NON-NLS-1$ 

		double combinedFitness = 1.0;
		int totalWeight = 0;

		for (final Object traitKey : phenotype.getKeys()) {
			final Trait trait = phenotype.getTrait((String) traitKey);
			for (final String factorKey : environment.getEcoSystem().factorKeys()) {
				final EcoFactor factor = environment.getEcoSystem().getFactor(factorKey);
				for (final Fitness fitness : getFitnessValues()) {
					final int weight = fitness.getWeight(trait.getCharacterKey(), factor);
					if (weight != 0) {
						final double f = fitness.getFitness(trait, factor);
						final double weightedFitness = weight == 1 ? f : Math.pow(f, weight);
						totalWeight += weight;
						combinedFitness *= weightedFitness;
					}
				}
			}
		}

		if (totalWeight > 0) {
			// XXX for now, we do not normalize by total weight - in fact total
			// weight is irrelevant
			// final double result = totalWeight == 1 ? combinedFitness :
			// MathUtil.pow(combinedFitness,
			// 1.0 / totalWeight);
			final double result = combinedFitness;
			if (!phenotype.equals(getLookedAtPhenotype())) {
				// LOG.debug(LOG_MSG_1, new Object[] { phenotype, environment,
				// new Double(result) });
				if (LOG.isDebugEnabled())
					LOG.debug(MessageFormat.format(LOG_MSG_1, new Object[] { phenotype, environment, new Double(result) }));
				setLookedAtPhenotype(phenotype);
			}
			return result;
		}

		throw new DarwinException("calculateFitness(): cannot measure fitness for: " + phenotype + " in " + environment); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final FitnessEngine_ other = (FitnessEngine_) obj;
		if (this._realm == null) {
			if (other._realm != null)
				return false;
		} else if (!this._realm.equals(other._realm))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.HasExpressions#getExpressions()
	 */
	@Override
	public ExpressionMap getExpressions() {
		final ExpressionMap result = new ExpressionMap();
		for (final Fitness fitness : getFitnessValues()) {
			// TODO consider doing this via polymorphism
			if (fitness instanceof HasExpressions) {
				result.putAll(((HasExpressions) fitness).getExpressions());
			}
		}
		return result;
	}

	/**
	 * @see net.sf.darwin.core.CacheSignature#getSignature()
	 */
	@Override
	public String getSignature() {
		final StringBuilder result = new StringBuilder();
		for (final Fitness fitness : getFitnessValues())
			result.append(fitness);
		return result.toString();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._realm == null) ? 0 : this._realm.hashCode());
		return result;
	}

	/**
	 * The base implementation of this method (which should normally be
	 * appropriate for all usage) simply goes through all possible pairings of
	 * trait/ecofactor (from the given traits and the ecofactors belonging to
	 * the environment) and gets the appropriate setup. If the setup is
	 * non-null, we invoke it with the object that is passed in to this method.
	 * 
	 * @param environment
	 * 
	 * @see net.sf.darwin.core.FitnessEngine#onEnvironmentChange(java.util.Collection,
	 *      net.sf.darwin.core.Environment)
	 */
	@Override
	public void onEnvironmentChange(final Collection<String> characterKeys, final Environment<E> environment) {
		for (final String traitKey : characterKeys) {
			for (final String factorKey : environment.getEcoSystem().factorKeys()) {
				final EcoFactor factor = environment.getEcoSystem().getFactor(factorKey);
				for (final Fitness fitness : getFitnessValues())
					fitness.resetEnvironmentFactor(traitKey, factor);
			}
		}
	}

	/**
	 * @see net.sf.darwin.core.FunctionListener#onFunctionChange(net.sf.darwin.core.Function)
	 */
	@Override
	public void onFunctionChange(final Function function) {
		if (this.fitnessCache != null)
			this.fitnessCache.flush();
	}

	/**
	 * @see net.sf.darwin.core.FitnessEngine#putFitness(java.lang.String,
	 *      net.sf.darwin.core.Fitness)
	 */
	@Override
	public Fitness putFitness(final String key, final Fitness fitness) {
		return this.getFitnesses().put(key, fitness);
	}

	/**
	 * The base implementation of this method (which should normally be
	 * appropriate for all usage) simply goes through all possible pairings of
	 * trait/ecofactor (from the given traits and the ecofactors belonging to
	 * the environment) and gets the appropriate setup. If the setup is
	 * non-null, we invoke it with the object that is passed in to this method.
	 * 
	 * @param characterKeys
	 * @param adjustment
	 *            the object to be passed in to the setup method.
	 * @see net.sf.darwin.core.FitnessEngine#calculateFitness(net.sf.darwin.core.Phenotype,
	 *      net.sf.darwin.core.Environment)
	 */
	@Override
	public void setFitnessAdjustment(final Collection<String> characterKeys, final Number adjustment) {
		for (final String traitKey : characterKeys)
			for (final Fitness fitness : getFitnessValues())
				fitness.resetAdjustment(adjustment, traitKey);
	}

	/**
	 * @see net.sf.darwin.core.FitnessEngine#setFitnessCache(com.rubecula.darwin.domain.helper.FitnessCache_Standard)
	 */
	@Override
	public void setFitnessCache(final FitnessCache fitnessCache) {
		this.fitnessCache = fitnessCache;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = super.toString();
		result += " with " + countFitnesses() + " fitnesses"; //$NON-NLS-1$//$NON-NLS-2$
		return result;
	}

	/**
	 * @see net.sf.darwin.core.FitnessEngine#unregisterFitness(java.lang.String)
	 */
	@Override
	public Fitness unregisterFitness(final String key) {
		return getFitnesses().remove(key);
	}

	protected int countFitnesses() {
		return getFitnesses().size();
	}

	protected Set<String> fitnessKeys() {
		return getFitnesses().keySet();
	}

	protected Fitness getFitness(final String key) {
		return getFitnesses().get(key);
	}

	protected Collection<Fitness<P, E>> getFitnessValues() {
		return getFitnesses().values();
	}

	/**
	 * @return the realm
	 */
	protected Realm getRealm() {
		return this._realm;
	}

	protected abstract void registerFitnesses(Evaluator evaluator);

	/**
	 * @return the fitnesses
	 */
	private Map<String, Fitness<P, E>> getFitnesses() {
		return this._fitnesses;
	}

	/**
	 * @return the value of {@link #lookedAtPhenotype}
	 */
	private Object getLookedAtPhenotype() {
		return this.lookedAtPhenotype;
	}

	/**
	 * @param lookedAtPhenotype
	 *            the lookedAtPhenotype to set
	 */
	private void setLookedAtPhenotype(final Object lookedAtPhenotype) {
		this.lookedAtPhenotype = lookedAtPhenotype;
	}

	//	private static final String LOG_MSG_1 = "fitness for phenotype: {}, environment: {} = {}"; //$NON-NLS-1$
	private static final String LOG_MSG_1 = "fitness for phenotype: {0}, environment: {1} = {2}"; //$NON-NLS-1$

	private final Realm _realm;

	private FitnessCache fitnessCache;

	private transient Object lookedAtPhenotype;

	/**
	 * The logger for this class.
	 */
	// protected static final Logger LOG =
	// LoggerFactory.getLogger(FitnessEngine_.class);
	protected static final Log LOG = LogFactory.getLog(FitnessEngine_.class);

	private final transient Map<String, Fitness<P, E>> _fitnesses;

}
