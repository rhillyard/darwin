/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: TimeUtilities
 * Created on: Mar 2, 2012
 */

package net.sf.darwin.api.util;

/**
 * @author rhillya
 * 
 */
public final class TimeUtilities {

	/**
	 * Period between checks to see if the run method has completed (in
	 * milliseconds). Can be set by subclasses if desired.
	 */
	public static int WaitPeriod = 50;

	/**
	 * Method to create a String which appropriately represents the accuracy of
	 * the elapsed time.
	 * 
	 * @param millisecs
	 * @return a String which shows the elapsed time and the accuracy of same.
	 */
	public static String getElapsedTime(final long millisecs) {
		return (millisecs / WaitPeriod) / 1000. * WaitPeriod + " seconds (accurate to " + WaitPeriod + " mSec)"; //$NON-NLS-1$ //$NON-NLS-2$
	}

}
