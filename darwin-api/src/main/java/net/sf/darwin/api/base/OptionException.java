/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionException
 * Created on: Mar 6, 2012
 */

package net.sf.darwin.api.base;

/**
 * @author rhillya
 * 
 */
public class OptionException extends Exception {

	/**
	 * @param message
	 */
	public OptionException(final String message) {
		super(message);
	}

	/**
	 * @param message
	 */
	@SuppressWarnings("nls")
	public OptionException(final String key, final String message) {
		super("Option " + key + " not available because: " + message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public OptionException(final String message, final Throwable cause) {
		super(message, cause);
		// XXX Auto-generated constructor stub
	}

	private static final long serialVersionUID = -3121222821951366157L;

}
