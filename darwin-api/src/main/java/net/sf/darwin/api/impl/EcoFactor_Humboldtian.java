/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoFactor_Humboldtian.java
 * Created on Nov 4, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.EcoFactor_Number;

/**
 * @author Robin Hillyard
 * 
 */
final public class EcoFactor_Humboldtian extends EcoFactor_Number {

	/**
	 * If this constructor is used, the value must be set as a property.
	 * 
	 * @param identifier
	 */
	public EcoFactor_Humboldtian(final String identifier) {
		this(identifier, null);
	}

	/**
	 * @param identifier
	 * @param value
	 */
	public EcoFactor_Humboldtian(final String identifier, final Number value) {
		super(identifier, value);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6654360018561487042L;

}
