/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Attraction_Uniform.java
 * Created on Jun 18, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Attraction_;
import net.sf.darwin.core.Attraction;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.TraitMap;

/**
 * Default implementation of {@link Attraction} such that all attractions are
 * unity (i.e. don't affect the mating choice).
 * 
 * If you want to implement true sexual selection, you need to extend the
 * {@link Attraction_} type.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public final class Attraction_Uniform extends Attraction_ {

	/**
	 * 
	 */
	public Attraction_Uniform() {
		super();
	}

	/**
	 * Default method for getAttraction. All matches are considered equally
	 * attractive.
	 * 
	 * @return 1.0
	 * 
	 * @see net.sf.darwin.core.Attraction#getAttraction(TraitMap, TraitMap)
	 */
	@Override
	public <P> double getAttraction(final TraitMap<P> female, final TraitMap<P> male) {
		return 1.0;
	}

	/**
	 * @see net.sf.darwin.core.Attraction#isUniform()
	 */
	@Override
	public boolean isUniform() {
		return true;
	}

}
