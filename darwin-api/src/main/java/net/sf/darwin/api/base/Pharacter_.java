/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Pharacter_.java
 * Created on May 29, 2009
 * @version $Revision: 1.21 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Variant;
import net.sf.tostring0.ToString;

/**
 * Base implementation of {@link Pharacter} (a Phenotypic Character). This class
 * is the environmental analog to {@link Locus_}.
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
public abstract class Pharacter_<P> extends Base implements Pharacter<P> {

	/**
	 * Protected abstract constructor.
	 * 
	 * @param identifier
	 *            the identifier for this character
	 * @param sexLinked
	 *            XXX
	 */
	protected Pharacter_(final String identifier, final boolean sexLinked) {
		super();
		this._identifier = identifier;
		this._sexLinked = sexLinked;
		this._variants = new HashMap<>();
	}

	/**
	 * Add the variant to this Pharacter, set its character to be this; then
	 * return its key.
	 * 
	 * @return the variant's key.
	 * 
	 * @see net.sf.darwin.core.Pharacter#addVariant(net.sf.darwin.core.Variant)
	 */
	@Override
	public String addVariant(final Variant<P> variant) {
		if (variant != null) {
			final String key = variant.getIdentifier();
			if (!getVariantMap().containsKey(key)) {
				final Variant<P> added = getVariantMap().put(key, variant);
				if (added == null) {
					// TODO consider doing this via polymorphism
					if (variant instanceof Variant_) {
						((Variant_<P>) variant).setCharacter(this);
						return key;
					}
					throw new DarwinException("addVariant(): logic error: wrong type"); //$NON-NLS-1$
				}
				throw new DarwinException("addVariant(): logic error: not added"); //$NON-NLS-1$
			}
			return key;
		}
		throw new DarwinException("addVariant(): null variant"); //$NON-NLS-1$
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Pharacter_ other = (Pharacter_) obj;
		if (getIdentifier() == null) {
			if (other.getIdentifier() != null)
				return false;
		} else if (!getIdentifier().equals(other.getIdentifier()))
			return false;
		if (getVariantMap() == null) {
			if (other.getVariantMap() != null)
				return false;
		} else if (!getVariantMap().equals(other.getVariantMap()))
			return false;
		return true;
	}

	/**
	 * Get the identifier for this phenotypic character, for example
	 * "Eye Color".
	 * 
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * if {@link #getVariantMap()} contains an element whose key is the
	 * variant's id, then return the key else null.
	 * 
	 * @see net.sf.darwin.core.Pharacter#getKey(net.sf.darwin.core.Variant)
	 */
	@Override
	public String getKey(final Variant<P> variant) {
		final String key = variant.getIdentifier();
		return getVariantMap().containsValue(variant) && variant.equals(getVariantMap().get(key)) ? key : null;
	}

	/**
	 * Method to get the specific variant referred to by the key.
	 * 
	 * @param key
	 *            a string which identifies the variant from among those which
	 *            are legal for this character. If the key is null, then we
	 *            return null as the result. This is the case when a trait is
	 *            variable and there is no registered variants.
	 * @see net.sf.darwin.core.Pharacter#getVariant(java.lang.String)
	 */
	@Override
	public Variant<P> getVariant(final String key) {
		if (key != null)
			return getVariantMap().get(key);
		return null;
	}

	/**
	 * @see net.sf.darwin.core.Pharacter#getVariantKeys()
	 */
	@Override
	public Set<String> getVariantKeys() {
		return getVariantMap().keySet();
	}

	/**
	 * @see net.sf.darwin.core.Pharacter#getVariants()
	 */
	@Override
	public Collection<Variant<P>> getVariants() {
		return getVariantMap().values();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getIdentifier() == null) ? 0 : getIdentifier().hashCode());
		result = prime * result + ((getVariantMap() == null) ? 0 : getVariantMap().hashCode());
		return result;
	}

	/**
	 * @see net.sf.darwin.core.SexLinked#isSexLinked()
	 */
	@Override
	public boolean isSexLinked() {
		return this._sexLinked;
	}

	/**
	 * Clear the list of variants and add in all those from the given list. For
	 * each of the variants, set its character to this.
	 * 
	 * @param variants
	 *            a collection of variants which will make up the variants for
	 *            this phenotypic character.
	 */
	public void setVariants(final Collection<? extends Variant<P>> variants) {
		getVariantMap().clear();
		for (final Variant<P> variant : variants) {
			final String identifier = variant.getIdentifier();
			getVariantMap().put(identifier, variant);
			// TODO consider doing this via polymorphism
			if (variant instanceof Variant_) {
				((Variant_<P>) variant).setCharacter(this);
			}
		}
	}

	/**
	 * @return
	 */
	private Map<String, Variant<P>> getVariantMap() {
		return this._variants;
	}

	private final boolean _sexLinked;

	private final String _identifier;

	@ToString(child = true)
	private final Map<String, Variant<P>> _variants;
}
