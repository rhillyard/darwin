/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003,2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Census_Detail.java
 * Created on Nov 5, 2009
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.api.impl;

import java.io.PrintWriter;

import net.sf.darwin.api.base.Census_Darwinian;
import net.sf.darwin.api.base.Colony_;
import net.sf.darwin.api.base.SinkCensus;
import net.sf.darwin.core.Censusible;

/**
 * Implementation of {@link SinkCensus} specifically which tries to do sensible
 * things for general types of phenotypes and genotypes. This implementer
 * reports on the mean allelic and traitic values, as well as identifying the
 * numbers of each type of allele.
 * 
 * TODO Consider having more intelligence here regarding the level of detail to
 * show.
 * 
 * @author Robin Hillyard
 * 
 */
public final class Census_Detail extends Census_Darwinian {

	/**
	 * @param printWriter
	 */
	public Census_Detail(final PrintWriter printWriter) {
		super(printWriter);
	}

	/**
	 * 
	 * @see com.rubecula.darwin.examples.pepperedmoth.Census_Darwinian#census(Censusible,
	 *      int, Object)
	 */
	@Override
	public void census(final Censusible censusible, final int depth, final Object context) {
		// TODO consider doing this via polymorphism
		if (censusible instanceof Colony_)
			((Colony_) censusible).doCensusDetail(this, context);
		else
			super.census(censusible, depth, context);
	}

}