/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Function_Desirable.java
 * Created on Apr 19, 2009
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.api.base.Function_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Desirable;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.HasExpressions;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.Notation;

/**
 * Concrete implementation of {@link Desirable} which extends {@link Function_}
 * and implements {@link HasExpressions} (thus it can be updated graphically at
 * runtime).
 * 
 * @author Robin Hillyard
 * 
 */
public class Function_Desirable extends Function_ implements Desirable, HasExpressions {

	/**
	 * 
	 */
	public Function_Desirable() {
		super();
	}

	/**
	 * @param evaluator
	 */
	public Function_Desirable(final Evaluator evaluator) {
		super(evaluator);
	}

	/**
	 * @see net.sf.darwin.core.Desirable#getDesirability(boolean, double, int)
	 */
	@Override
	@SuppressWarnings("boxing")
	public double getDesirability(final boolean viability, final double random, final int age) {
		try {
			// Calculate the viability factor as 10 for a viable male and 1 for
			// non-viable male (such are already marked for death).
			final int viabilityFactor = viability ? 10 : 1;
			return evaluate(VIABILITY, viabilityFactor, RANDOM, random, AGE, age).doubleValue();
		} catch (final FunctionException e) {
			throw new DarwinException("Function_Desirable.getDesirability(): exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @see net.sf.darwin.core.HasExpressions#getExpressions()
	 * @see net.sf.darwin.core.HasExpressions#getExpressions()
	 */
	@Override
	public ExpressionMap getExpressions() {
		return getExpressionMap();
	}

	/**
	 * @return (typically) "Desirability"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return Messages.getString("Function_Desirable.3"); //$NON-NLS-1$
	}

	/**
	 * @param arguments
	 *            these arguments must be, in order,
	 *            <ol>
	 *            <li>Integer: viabilityFactor (10 for viable, 1 for non-viable)
	 *            </li>
	 *            <li>Number: random</li>
	 *            <li>Integer: age</li>
	 *            </ol>
	 * <br>
	 * 
	 *            Note that the arguments are auto-boxed for passing into
	 *            {@link #evaluateFixedExpression(int, double, int)}. Note also
	 *            that we do not protect against cast exceptions.
	 * @return the result of invoking
	 *         {@link #evaluateFixedExpression(int, double, int)}.
	 * @see net.sf.darwin.api.base.Function_#standardFunction(double[])
	 */
	@SuppressWarnings("boxing")
	@Override
	protected double standardFunction(final Object... arguments) throws FunctionException {
		// No protection against class cast exceptions
		assert arguments.length == 3 : "Function_Desirable.standardValue(): requires 3 arguments"; //$NON-NLS-1$

		return evaluateFixedExpression((Integer) arguments[0], ((Number) arguments[1]).doubleValue(), (Integer) arguments[2]);
	}

	/**
	 * 
	 * @see net.sf.darwin.api.base.Function_#standardFunctionTokens(Notation)
	 */
	@Override
	protected CharSequence[] standardFunctionTokens(final Notation notation) {
		switch (notation) {
		case RPN:
			return standardFunctionTokensRPN();
		default:
			return standardFunctionTokensAlgebraic();
		}
	}

	/**
	 * 1
	 */
	private static final String S_K4 = Messages.getString("Function_Desirable.7"); //$NON-NLS-1$

	/**
	 * 1
	 */
	private static final int K4 = Integer.parseInt(S_K4);

	/**
	 * 1
	 */
	private static final String S_K3 = Messages.getString("Function_Desirable.6"); //$NON-NLS-1$

	/**
	 * 1
	 */
	private static final int K3 = Integer.parseInt(S_K3);

	/**
	 * 2
	 */
	private static final String S_K2 = Messages.getString("Function_Desirable.5"); //$NON-NLS-1$

	/**
	 * 2
	 */
	private static final int K2 = Integer.parseInt(S_K2);

	/**
	 * 2
	 */
	private static final String S_K1 = Messages.getString("Function_Desirable.4"); //$NON-NLS-1$

	/**
	 * 2
	 */
	private static final int K1 = Integer.parseInt(S_K1);

	/**
	 * -
	 */
	private static final String S_MINUS = "-"; //$NON-NLS-1$

	/**
	 * +
	 */
	private static final String S_PLUS = "+"; //$NON-NLS-1$

	/**
	 * *
	 */
	private static final String S_TIMES = "*"; //$NON-NLS-1$

	/**
	 * )
	 */
	private static final String S_PAR_CLSE = ")"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final String S_PAR_OPEN = "("; //$NON-NLS-1$

	/**
	 * age
	 */
	private static final String AGE = Messages.getString("Function_Desirable.0"); //$NON-NLS-1$

	/**
	 * random
	 */
	private static final String RANDOM = Messages.getString("Function_Desirable.1"); //$NON-NLS-1$

	/**
	 * viability
	 */
	private static final String VIABILITY = Messages.getString("Function_Desirable.2"); //$NON-NLS-1$

	private static double evaluateFixedExpression(final int viability, final double random, final int age) {
		final double f = Math.pow(K1, K2 * random - K3);

		// Now we factor in the age of the male - the longer he's survived the
		// more we like him because he must have good genes
		final int g = age + K4;

		// Now we return the product of all three terms
		return f * viability * g;
	}

	/**
	 * @return The standard expression is of the form:
	 * 
	 *         <pre>
	 * viability * S_K1 &circ; (2 * random - 1) * (age + 1)
	 * </pre>
	 */
	private static CharSequence[] standardFunctionTokensAlgebraic() {
		return new CharSequence[] {
				VIABILITY + S_TIMES + S_K1,
				Concept.pow,
				S_PAR_OPEN + S_K2 + S_TIMES + RANDOM + S_MINUS + S_K3 + S_PAR_CLSE + S_TIMES + S_PAR_OPEN + AGE + S_PLUS + S_K4
				+ S_PAR_CLSE };
	}

	/**
	 * @return The standard expression is of the form:
	 * 
	 *         <pre>
	 *         K1 random K2 * K3 - + &circ; $age K4 + * $viability *
	 * </pre>
	 */
	private static CharSequence[] standardFunctionTokensRPN() {
		return new CharSequence[] { S_K1, FitnessFunction.PREFIX_VAR_RPN + RANDOM, S_K2, S_TIMES, S_K3, S_MINUS, S_PLUS,
				Concept.pow, FitnessFunction.PREFIX_VAR_RPN + AGE, S_K4, S_PLUS, S_TIMES,
				FitnessFunction.PREFIX_VAR_RPN + VIABILITY, S_TIMES };
	}

}
