/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Locus_Sex.java
 * Created on Jun 8, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Locus_Random;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Implementer of {@link Locus} specifically for sex.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public final class Locus_Sex extends Locus_Random<Boolean> {

	/**
	 * @param random
	 */
	public Locus_Sex(final RandomGenerator random) {
		super(SEX, random);
		init();
	}

	/**
	 * 
	 */
	private void init() {
		add(Allele_Sex.Y, 1);
		add(Allele_Sex.X, 1);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2921389849002462481L;

	/**
	 * 
	 */
	public static final String SEX = "sex"; //$NON-NLS-1$

	/**
	 * 
	 */
	public static final int INDEX_FEMALE = 1;

}
