/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 * 
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Phenotype_;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.TraitMap;

/**
 * <p>
 * Default implementation of {@link Phenotype}. This concrete class provides a
 * default phenotype for an organism. The phenotype can be evaluated for
 * fitness. A {@link Phenotype_} extends of a {@link TraitMap}. The set of
 * traits is defined by the system's Phenome.
 * </p>
 * <p>
 * Named in honor of Richard Dawkins
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.10 $
 */
@Lifecycle(permanent = false)
public final class Phenotype_Dawkinsian<P> extends Phenotype_<P> {

	/**
	 * Package-scope constructor (normally instances are created by calling
	 * {@link PhenotypeFactory#makeDawkinsian()}.
	 * 
	 * TODO restrict scope to package.
	 * 
	 * @param id
	 */
	public Phenotype_Dawkinsian(final String id) {
		super(id);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6661660658895323401L;

}