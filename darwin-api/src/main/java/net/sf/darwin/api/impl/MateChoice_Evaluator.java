/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MateChoice_Evaluator.java
 * Created on Feb 26, 2009
 * @version $Revision: 1.32 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.api.base.MateChoice_;
import net.sf.darwin.core.Attraction;
import net.sf.darwin.core.Choosy;
import net.sf.darwin.core.Desirable;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.core.Organism;

import org.apache.commons.math.random.RandomGenerator;

import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.JexpressionException;

/**
 * @author Robin Hillyard
 * 
 *         TODO this class has some refactoring to do in terms of the public
 *         methods - add them to the interface or what?
 */
public class MateChoice_Evaluator extends MateChoice_ implements HasExpressions {

	/**
	 * Public secondary constructor -- with no evaluator.
	 * 
	 * @param random
	 *            a random number generator
	 */
	public MateChoice_Evaluator(final RandomGenerator random) {
		this(random, (Evaluator) null, null);
	}

	/**
	 * @param random
	 * @param desirability
	 * @param undesirability
	 * @param attraction
	 *            the implementer of sexual selection.
	 * 
	 */
	public MateChoice_Evaluator(final RandomGenerator random, final Desirable desirability, final Choosy undesirability,
			final Attraction attraction) {
		super(random, attraction);
		this._desirabilityEvaluator = desirability;
		this._desirabilityMinEvaluator = undesirability;
	}

	/**
	 * Public constructor.
	 * 
	 * @param random
	 *            a random number generator
	 * @param evaluatorDesirable
	 *            the expression evaluator to be used for the desirability
	 *            formula (or null)
	 * @param evaluatorChoosy
	 *            the expression evaluator to be used for the
	 *            minimum-desirability formula (or null)
	 */
	public MateChoice_Evaluator(final RandomGenerator random, final Evaluator evaluatorDesirable, final Evaluator evaluatorChoosy) {
		this(random, new Function_Desirable(evaluatorDesirable), new Function_Choosy(evaluatorChoosy));
	}

	/**
	 * Public constructor with uniform attraction.
	 * 
	 * @param random
	 *            a random number generator
	 * @param functionDesirable
	 *            the expression evaluator to be used for the desirability
	 *            formula (or null)
	 * @param functionChoosy
	 *            the expression evaluator to be used for the
	 *            minimum-desirability formula (or null)
	 */
	public MateChoice_Evaluator(final RandomGenerator random, final Function_Desirable functionDesirable,
			final Function_Choosy functionChoosy) {
		this(random, functionDesirable, functionChoosy, new Attraction_Uniform());
	}

	/**
	 * TEST
	 * 
	 * TODO consider eliminating this method.
	 */
	public void enableMateChoiceChanges() {
		setEvaluator(0);
		setEvaluator(1);
	}

	/**
	 * XXX Need explanation.
	 * 
	 * @see net.sf.darwin.core.HasExpressions#getExpressions()
	 */
	@Override
	public ExpressionMap getExpressions() {
		final ExpressionMap map = new ExpressionMap();
		// TODO consider doing this via polymorphism
		if (getDesirabilityEvaluator() instanceof HasExpressions) {
			map.putAll(((HasExpressions) getDesirabilityEvaluator()).getExpressions());
		}
		if (getDesirabilityMinEvaluator() instanceof HasExpressions) {
			map.putAll(((HasExpressions) getDesirabilityMinEvaluator()).getExpressions());
		}
		return map;
	}

	/**
	 * By default, this implementation returns a value E^(3.8 * r) where r is a
	 * random number between 0 and 1. The higher the value returned, the more
	 * likely a female will find no mate (and hence look outside the population)
	 * 
	 * @see net.sf.darwin.api.base.MateChoice_#getMinimumDesirability()
	 */
	@Override
	public double getMinimumDesirability() {
		return getDesirabilityMinEvaluator().getMinimumDesirability(getRandom().nextDouble());
	}

	/**
	 * @param index
	 * 
	 * @param expression
	 * 
	 *            TEST
	 * 
	 *            TODO this should have package visibility (but need to fix a
	 *            test class first).
	 */
	public void setEvaluator(final int index, final String expression) {
		// Do nothing
	}

	/**
	 * @param index
	 * 
	 * @param expression
	 * 
	 *            TODO this should have package visibility (but need to fix a
	 *            test class first).
	 * @throws FunctionException
	 */
	public void setExpression(final int index, final String expression) throws FunctionException {
		try {
			if (index == 0)
				getDesirabilityEvaluator().setExpression(expression);
			else
				getDesirabilityMinEvaluator().setExpression(expression);
		} catch (final JexpressionException e) {
			throw new FunctionException("MateChoice_Evaluator.setExpression() exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * 
	 * TODO this should have package visibility (but need to fix a test class
	 * first). ??
	 * 
	 * @param seed
	 * 
	 * @see net.sf.darwin.api.base.MateChoice_#getRandom()
	 * 
	 */
	public void setSeed(final long seed) {
		getRandom().setSeed(seed);
	}

	/**
	 * TODO consider eliminating this method.
	 * 
	 * @param index
	 * 
	 * @return the current expression for the evaluator
	 *         {@link #_desirabilityEvaluator}
	 * 
	 *         TODO this should have package visibility (but need to fix a test
	 *         class first).
	 */
	String getExpression(final int index) {
		if (index == 0)
			return getDesirabilityEvaluator().getExpression();

		// TEST
		return getDesirabilityMinEvaluator().getExpression();
	}

	/**
	 * Set the evaluator with the default expression.
	 * 
	 * TEST
	 * 
	 * @param index
	 * 
	 */
	void setEvaluator(@SuppressWarnings("unused") final int index) {
		// Do nothing
	}

	/**
	 * This implementation returns a value which is: <code>d * g * h</code>
	 * where d is the value of the desirability index defined by the superclass
	 * (see {@link MateChoice_#getDesirabilityIndex(Organism, Organism)} ; and g
	 * is one more than the age of the candidate male;
	 * 
	 * TODO consider a different mechanism here: instead of passing in a random
	 * number to the desirability formula, we should divide the random space
	 * into several bands (or tranches), using {@link Randomizer}, and then
	 * cache the various desirability values for age/viability/tranche.
	 * 
	 * @see net.sf.darwin.core.MateChoice#getDesirabilityIndex(Organism,
	 *      Organism)
	 * @see net.sf.darwin.api.base.MateChoice_#getDesirabilityIndex(net.sf.darwin.core.Organism,
	 *      net.sf.darwin.core.Organism)
	 * @see net.sf.darwin.api.base.MateChoice_#getDesirability(net.sf.darwin.core.Organism)
	 */
	@Override
	protected <E, P, G> double getDesirability(final Organism<E, P, G> male) {
		return getDesirabilityEvaluator().getDesirability(male.isViable(), getRandom().nextDouble(), male.getAge());
	}

	/**
	 * @return the value of the field {@link #_desirabilityEvaluator}
	 */
	protected Desirable getDesirabilityEvaluator() {
		return this._desirabilityEvaluator;
	}

	/**
	 * @return the value of field {@link #_desirabilityMinEvaluator}
	 */
	protected Choosy getDesirabilityMinEvaluator() {
		return this._desirabilityMinEvaluator;
	}

	protected final Desirable _desirabilityEvaluator;

	protected final Choosy _desirabilityMinEvaluator;

}
