/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Dec 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Meiosis_;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Meiosis;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Default implementation for the {@link Meiosis} interface. Its behavior is to
 * choose alleles purely randomly. Formerly known as Meiosis_Random.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.16 $
 */
@Lifecycle(permanent = true)
public final class Meiosis_Locus<G> extends Meiosis_<G> {

	/**
	 * Public primary constructor for an implementer of {@link Meiosis} with
	 * given random number source.
	 * 
	 * @param random
	 *            a source of random numbers.
	 */
	public Meiosis_Locus(final RandomGenerator random) {
		super(random);
	}

	/**
	 * This method does meiosis for a set of genes. Any genes not belonging to
	 * chromosome are ignored. Otherwise, each gene takes as its allele the
	 * allele determined invoking {@link Meiosis#doMeiosis(Gene, int)}.
	 * 
	 * @param genomic
	 * 
	 * @param genes
	 *            the genes to process
	 * @param chromosome
	 *            the chromosome of interest (or null, if all are of interest).
	 * 
	 * @see net.sf.darwin.core.Meiosis#doMeiosis(Genomic,
	 *      net.sf.darwin.core.Genes, net.sf.darwin.core.Chromosome)
	 */
	@Override
	public <T> Genome<T, G> doMeiosis(final Genomic<T, G> genomic, final Genes<G> genes, final Chromosome<G> chromosome) {
		return doMeiosisChromosome(genomic, genes, chromosome, -1);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3449660344257696265L;
}
