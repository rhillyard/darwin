/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Mutator;
import net.sf.tostring0.AToString;
import net.sf.tostring0.Identifiable;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * This abstract class represents a particular gene at a locus of a genome. In a
 * diploid system there will be two genes at a locus. In most systems there is
 * one. A gene can take as its "value" an Allele which is valid at the given
 * locus.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.37 $
 * 
 * @param <G>
 *            the type of the genetic information which is encoded in this Gene
 */
@SuppressWarnings("serial")
@ToString(showParent = true, allBeanMethods = false)
public abstract class Gene_<G> extends Base implements Gene<G>, Cloneable, Identifiable {

	/**
	 * Primary protected constructor to create a Gene_ which appears at a
	 * particular locus. The array alleles of size ploidy is initialized with no
	 * elements specified.
	 * 
	 * @param ploidy
	 *            the number of alleles at any given gene locus
	 */
	protected Gene_(final int ploidy) {
		this.setAlleles(new String[ploidy]);
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present(getLocus().getIdentifier(), context);
		return true;
	}

	/**
	 * XXX
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		try {
			@SuppressWarnings("unchecked")
			final Gene_<G> clone = (Gene_<G>) super.clone();
			// Nothing else to do -- no mutable or non-primitive fields to
			// deep-copy.
			return clone;
		} catch (final CloneNotSupportedException e) {
			e.printStackTrace(System.err); // this will not happen since the
			// superclass supports Cloneable
			return null;
		}
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Gene_<?> other = (Gene_<?>) obj;
		if (!Arrays.equals(getAlleles(), other.getAlleles()))
			return false;
		return true;
	}

	/**
	 * @param index
	 * @return the indexth allele
	 */
	@Override
	public Allele<G> getAllele(final int index) {
		if (getLocus() != null)
			return getLocus().getAllele(getAlleleKey(index));
		throw new DarwinException("Gene_.getAllele(): locus not set"); //$NON-NLS-1$
	}

	/**
	 * @return the number of alleles for the locus of this gene
	 */
	@Override
	public int getAlleleCount() {
		return getLocus().size();
	}

	/**
	 * @see net.sf.darwin.core.Gene#getAlleleKey(int)
	 */
	@Override
	public String getAlleleKey(final int index) {
		if (index >= 0 && index < getAlleles().length)
			return getAlleles()[index];

		throw new DarwinException("Locus.getAllele: index out of range: " + index); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Basic#getBases()
	 */
	@Override
	public String getBases() {
		String result = ""; //$NON-NLS-1$
		final int ploidy = getPloidy();
		for (int i = 0; i < ploidy; i++)
			result += getAllele(i).getBases(); // TODO consider a fix
		if (ploidy > 1)
			result += "-"; //$NON-NLS-1$
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		final Collection<Censusible> result = new ArrayList<>();
		for (int i = 0; i < getAlleles().length; i++)
			result.add(getAllele(i));
		return result;
	}

	/**
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		String result = getLocus().getIdentifier() + "="; //$NON-NLS-1$
		for (int i = 0; i < getAlleles().length; i++)
			result += ((AToString) getAllele(i)).toStringId(); // TODO consider
		// a fix
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Gene#getLocus()
	 */
	@Override
	@ToString(parent = true, priority = 2)
	public Locus<G> getLocus() {
		return this.locus;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Ploidy#getPloidy()
	 */
	@Override
	public int getPloidy() {
		return getAlleles().length;
	}

	/**
	 * @see net.sf.darwin.core.CacheSignature#getSignature()
	 */
	@Override
	public String getSignature() {
		return getBases();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(getAlleles());
		return result;
	}

	/**
	 * Method to get a mutated copy of a Gene.
	 * 
	 * @param mutator
	 * 
	 * 
	 * @return a mutated copy (which may be identical if no mutations have
	 *         occurred)
	 * @see net.sf.darwin.core.Gene#mutate(net.sf.darwin.core.Mutator)
	 */
	@Override
	public Gene<G> mutate(final Mutator<G> mutator) {
		// TODO take care of sex gene also!!
		if (mutator.isIdentity())
			return this;
		@SuppressWarnings("unchecked")
		final Gene_<G> result = (Gene_<G>) clone();
		result.setAlleles(new String[result.getPloidy()]);
		final int ploidy = getPloidy();
		for (int i = 0; i < ploidy; i++) {
			final Allele<G> allele = getAllele(i);
			final Allele<G> mutant = mutator.mutate(allele);
			if (mutant != allele) {
				if (getLocus() != null) {
					String index = getLocus().getKey(mutant);
					if (index == null) {
						index = getLocus().addAllele(mutant);
						logMutation(allele, mutant);
					}
					result.getAlleles()[i] = index; // we bypass the setAllele
					// method
				} else
					throw new DarwinException("mutate(): locus not set"); //$NON-NLS-1$
			} else
				result.getAlleles()[i] = allele.getIdentifier(); // we bypass
			// the
			// setAllele method
		}
		return result;
	}

	/**
	 * XXX
	 * 
	 * It's the responsibility of the caller to ensure that the indicated allele
	 * is valid for the locus to which this gene will belong.
	 * 
	 * @see net.sf.darwin.core.Gene#setAllele(int, String)
	 */
	@Override
	public void setAllele(final int index, final String allele) {
		if (index >= 0 && index < getAlleles().length)
			if (getAlleles()[index] == null) {
				getAlleles()[index] = allele;
			} else
				throw new DarwinException("Locus.setAllele: already set: " + index); //$NON-NLS-1$
		else
			throw new DarwinException("Locus.setAllele: index out of range: " + index); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.Gene#setLocus(net.sf.darwin.core.Locus)
	 */
	@Override
	public void setLocus(final Locus<G> locus) {
		this.locus = locus;
	}

	/**
	 * @return the alleles
	 */
	protected String[] getAlleles() {
		return this.alleles;
	}

	/**
	 * @param alleles
	 *            the alleles to set
	 */
	protected void setAlleles(final String[] alleles) {
		this.alleles = alleles;
	}

	private static final String LOG_MSG_1 = "Gene_.mutate: {0} mutated into {1}"; //$NON-NLS-1$

	private transient Locus<G> locus;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Gene_.class);

	/**
	 * Private array of alleles which make up this gene locus. In theory, at
	 * least, the ordering of the alleles within the array is immaterial. The
	 * size of the array is the "ploidy" of the gene. Ideally, this would be a
	 * private field, but that's not compatible with the way we do mutation,
	 * using clone().
	 * 
	 * NOTE that the ToString annotation does not mark this field as "child"
	 * because we want to see the alleles always.
	 */
	@ToString(detail = 0)
	private String[] alleles;

	/**
	 * @param allele
	 * @param mutant
	 */
	private static <X> void logMutation(final Allele<X> allele, final Allele<X> mutant) {
		if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format(LOG_MSG_1, allele, mutant));
	}
}
