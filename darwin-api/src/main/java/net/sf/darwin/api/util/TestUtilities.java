/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: TestUtilities
 * Created on: Aug 18, 2011
 */

package net.sf.darwin.api.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This class provides a set of utilities for invoking private methods FOR
 * TESTING PURPOSES ONLY!
 * 
 * @author rhillya
 * 
 */
public final class TestUtilities {

	private static final String GET = "get"; //$NON-NLS-1$

	private static final String SET = "set"; //$NON-NLS-1$

	/**
	 * @param target
	 * @param name
	 * @return
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Object invokePrivateGetter(final Object target, final String name) throws NoSuchMethodException,
	IllegalAccessException, InvocationTargetException {
		return invokePrivateMethod(target, GET + initialize(name));
	}

	/**
	 * @param clazz
	 *            the exact class in which we expect to find the method.
	 * @param object
	 *            the object on which to invoke the method (may be null for a
	 *            static, or "class", method)
	 * @param methodName
	 * @param parameterTypes
	 * @param parameterValues
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static Object invokePrivateMethod(final Class<?> clazz, final Object object, final String methodName,
			final Class<?>[] parameterTypes, final Object[] parameterValues) throws SecurityException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		final Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
		if (method != null) {
			method.setAccessible(true);
			return method.invoke(object, parameterValues);
		}
		throw new RuntimeException("unknown method " + methodName + " for class " + clazz); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @param clazz
	 *            the exact class in which we expect to find the static, i.e.
	 *            "class", method.
	 * @param methodName
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Object invokePrivateMethod(final Class<?> clazz, final String methodName) throws SecurityException,
	NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		return invokePrivateMethod(clazz, methodName, new Class<?>[] {}, new Object[] {});
	}

	/**
	 * @param clazz
	 *            the exact class in which we expect to find the static, i.e.
	 *            "class", method.
	 * @param methodName
	 * @param parameterType
	 * @param parameterValue
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Object invokePrivateMethod(final Class<?> clazz, final String methodName, final Class<?> parameterType,
			final Object parameterValue) throws SecurityException, NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		return invokePrivateMethod(clazz, methodName, new Class<?>[] { parameterType }, new Object[] { parameterValue });
	}

	/**
	 * @param clazz
	 *            the exact class in which we expect to find the static, i.e.
	 *            "class", method.
	 * @param methodName
	 * @param parameterTypes
	 * @param parameterValues
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static Object invokePrivateMethod(final Class<?> clazz, final String methodName, final Class<?>[] parameterTypes,
			final Object[] parameterValues) throws SecurityException, IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		return invokePrivateMethod(clazz, null, methodName, parameterTypes, parameterValues);
	}

	/**
	 * @param object
	 *            the object on which to invoke the method (must be non-null)
	 * @param methodName
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Object invokePrivateMethod(final Object object, final String methodName) throws SecurityException,
	NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		return invokePrivateMethod(object, methodName, new Class<?>[] {}, new Object[] {});
	}

	/**
	 * @param object
	 *            the object on which to invoke the method (must be non-null)
	 * @param methodName
	 * @param parameterType
	 * @param parameterValue
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Object invokePrivateMethod(final Object object, final String methodName, final Class<?> parameterType,
			final Object parameterValue) throws SecurityException, NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		return invokePrivateMethod(object, methodName, new Class<?>[] { parameterType }, new Object[] { parameterValue });
	}

	/**
	 * @param object
	 *            the object on which to invoke the method (must be non-null)
	 * @param methodName
	 * @param parameterTypes
	 * @param parameterValues
	 * @return the value returned by the method
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static Object invokePrivateMethod(final Object object, final String methodName, final Class<?>[] parameterTypes,
			final Object[] parameterValues) throws SecurityException, IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		Class<? extends Object> clazz = object.getClass();
		while (clazz != null) {
			try {
				return invokePrivateMethod(clazz, object, methodName, parameterTypes, parameterValues);
			} catch (final NoSuchMethodException e) {
				if (clazz == Object.class)
					throw e;
				clazz = clazz.getSuperclass();
			}
		}
		throw new RuntimeException("unknown method " + methodName + " for object " + object); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @param target
	 * @param name
	 * @return
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static void invokePrivateSetter(final Object target, final String name, final Object value)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		invokePrivateMethod(target, SET + initialize(name), Object.class, value);
	}

	/**
	 * @param name
	 * @return
	 */
	private static String initialize(final String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

}
