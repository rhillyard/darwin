/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 23, 2003
 *
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.base;

import java.io.PrintWriter;

import net.sf.darwin.core.Census;
import net.sf.darwin.core.Sink;

/**
 * <p>
 * This application interface is used to define how an individual object should
 * be processed for a census.
 * </p>
 * 
 * @author Robin Hillyard
 */
public interface SinkCensus extends Sink, Census {

	/**
	 * TODO consider eliminating this method.
	 * 
	 * @return the print writer for this SinkCensus
	 */
	public PrintWriter getPrintWriter();

	/**
	 * @param s
	 * @see java.io.PrintWriter#print(java.lang.String)
	 */
	public void print(final String s);

	/**
	 * 
	 * @see java.io.PrintWriter#println()
	 */
	public void println();

	/**
	 * @param x
	 * @see java.io.PrintWriter#println(java.lang.String)
	 */
	public void println(final String x);

}
