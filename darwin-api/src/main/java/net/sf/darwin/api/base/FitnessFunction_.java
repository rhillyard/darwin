/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_.java
 * Created on Apr 8, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;

import com.rubecula.jexpression.Evaluator;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class FitnessFunction_ extends Function_ implements FitnessFunction {

	/**
	 * 
	 * TEST
	 */
	protected FitnessFunction_() {
		this(null);
	}

	/**
	 * @param evaluator
	 *            the evaluator (expression engine) used to evaluate the
	 *            expression.
	 * 
	 */
	protected FitnessFunction_(final Evaluator evaluator) {
		super(evaluator);
	}

	/**
	 * Get the fitness based on the <code>value</code> (usually from one of the
	 * traits of a phenotype), the <code>target</code> (usually a value from an
	 * eco factor in the environment), and the shape <code>factor</code>
	 * (usually determined by the application).
	 * 
	 * @param value
	 *            the variate value.
	 * @param target
	 *            usually the mean value of a distribution, if such a concept is
	 *            meaningful.
	 * @param factor
	 *            usually the standard deviation of a distribution, if such a
	 *            concept is meaningful.
	 * @return the value of the expression evaluated by the {@link Evaluator} or
	 *         the value returned from {@link #evaluate(Object...)} with
	 *         parameters: VAR_VALUE, value, VAR_TARGET, target,
	 *         VAR_SHAPE_FACTOR, factor.
	 * @throws FitnessException
	 * 
	 * @see com.rubecula.darwin.domain.helper.FitnessFunction#getFitness(double,
	 *      double, double)
	 */
	@Override
	@SuppressWarnings("boxing")
	public double getFitness(final double value, final double target, final double factor) throws FitnessException {
		try {
			final Number result = evaluate(VAR_VALUE, value, VAR_TARGET, target, VAR_SHAPE_FACTOR, factor);
			// LOG.debug(LOG_MSG_1, new Object[] { value, target, factor, result
			// });
			if (LOG.isDebugEnabled())
				LOG.debug(MessageFormat.format(LOG_MSG_1, value, target, factor, result));
			return result.doubleValue();
		} catch (final FunctionException e) {
			if (e.getCause() instanceof FitnessException)
				throw ((FitnessException) e.getCause());
			throw new DarwinException("FitnessFunction_.getFitness(): function exception", e.getCause()); //$NON-NLS-1$
		}
	}

	//	private static final String LOG_MSG_1 = "fitness for: {} ({}, {}) = {}"; //$NON-NLS-1$
	private static final String LOG_MSG_1 = "fitness for: {0} ({1}, {2}) = {3}"; //$NON-NLS-1$

}
