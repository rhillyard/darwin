/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser_Direct.java
 * Created on Jun 10, 2009
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.ExpresserFunction;
import net.sf.darwin.api.base.Expresser_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Trait;

/**
 * This expresser is suitable for haploid genomes. There is a functional
 * correspondence between allele values and trait values.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            expresser
 */
public final class Expresser_Function<P> extends Expresser_<P> {

	/**
	 * TEST
	 * 
	 * @param character
	 * @param function
	 *            the function which takes an allele and generates a trait
	 *            value.
	 * @param type
	 *            TODO
	 */
	public Expresser_Function(final Pharacter<P> character, final ExpresserFunction function, final Object type) {
		super(character, type);
		this._function = function;
	}

	/**
	 * TEST
	 * 
	 * Express a gene by getting the allele's value and generating a new trait
	 * with value derived from the allele's value.
	 * 
	 * @param genes
	 *            a variable number of Gene objects.
	 * @return a trait which is the expression of the given <code>genes</code>
	 * 
	 * @see net.sf.darwin.core.Expresser#express(Colony,
	 *      net.sf.darwin.core.Gene[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <G> Trait<P> express(final Gene<G>... genes) {
		if (genes.length == 1) {
			final Gene<G> gene = genes[0];
			if (gene.getPloidy() == Ploidy.HAPLOID) {
				// TEST this all must be checked out and tested.
				final Object value = this._function.express(gene.getAllele(0));
				if (value instanceof String)
					return createTrait((String) value);
				else if (value instanceof Number)
					// TODO check this cast!
					return (Trait<P>) createTrait(((Number) value).doubleValue());
				throw new DarwinException(S_METHOD_EXPRESS + "expressed value cannot does not correspond to a trait: " + value); //$NON-NLS-1$				
			}
			throw new DarwinException(S_METHOD_EXPRESS + "invalid gene (not diploid)"); //$NON-NLS-1$
		}
		throw new DarwinException(S_METHOD_EXPRESS + "expects only one gene parameter"); //$NON-NLS-1$
	}

	private final ExpresserFunction _function;

	private static final String S_METHOD_EXPRESS = "Expresser_Function.express(): "; //$NON-NLS-1$

}
