/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 7, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Environment_;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Realm;

/**
 * <p>
 * This class provides a default implementation of the {@link Environment}
 * interface.
 * </p>
 * 
 * Named in honor of <a href="http://en.wikipedia.org/wiki/John_Muir">John
 * Muir</a>.
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.4 $
 */
@Lifecycle(permanent = true)
public final class Environment_Muirian<E> extends Environment_<E> {

	/**
	 * TODO consider removing this
	 * 
	 * @param realm
	 * 
	 * 
	 */
	public Environment_Muirian(final Realm realm) {
		super(realm);
	}

	/**
	 * @param identifier
	 * @param realm
	 *            TEST
	 */
	public Environment_Muirian(final String identifier, final Realm realm) {
		super(identifier, realm);
	}

	/**
	 * TODO consider removing this TEST
	 * 
	 * @param identifier
	 * @param realm
	 * @param idealPopulation
	 */
	public Environment_Muirian(final String identifier, final Realm realm, final long idealPopulation) {
		super(identifier, realm, idealPopulation);
	}

	/**
	 * @see net.sf.darwin.core.Environment#init()
	 */
	@Override
	public void init() {
		// do nothing
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8365199085011159615L;
}
