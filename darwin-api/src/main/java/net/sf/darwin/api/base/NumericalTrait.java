/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Dec 28, 2003
 */

package net.sf.darwin.api.base;

/**
 * <p>
 * Defines how to get a numerical value for a trait. This interface is
 * implemented by instances of {@link Trait_}.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.1 $
 * 
 *          TODO this interface seems wrong. Consider refactoring it or
 *          preferably remove it or merging it with Variant.
 */
public interface NumericalTrait {

	/**
	 * Method to yield a numerical value for a trait identified by a string.
	 * 
	 * @param trait
	 *            the identifier for the trait.
	 * @return a numerical value for the given trait.
	 */
	public abstract double getTraitValue(String trait);
}
