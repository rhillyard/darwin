/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: GenerationListener_.java
 * Created on Feb 26, 2010
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.base;

import java.io.PrintWriter;

import net.sf.darwin.core.GenerationListener;

import com.rubecula.beanpot.BeanPot;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class GenerationListener_ implements GenerationListener {

	/**
	 * 
	 */
	protected GenerationListener_() {
		this(BeanPot.getInstance().getWriter());
	}

	/**
	 * @param writer
	 * 
	 */
	protected GenerationListener_(final PrintWriter writer) {
		super();
		this._writer = writer;
	}

	protected PrintWriter _writer;

}
