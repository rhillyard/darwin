/**
 * Cryptobase Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Module: EvolutionService.java
 * Created on Apr 7, 2012
 */

package net.sf.darwin.api.base;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;

import net.sf.darwin.core.GenerationListener;
import net.sf.darwin.core.VisualizableListener;
import net.sf.darwin.ui.base.RepaintListener;
import net.sf.darwin.ui.base.VisualizationModel;

/**
 * @author Robin
 * 
 */
public interface EvolutionService extends Remote {

	/**
	 * @param handle
	 * @param listener
	 *            XXX
	 * @throws RemoteException
	 */
	public abstract void addGenerationListener(String handle, GenerationListener listener) throws RemoteException;

	/**
	 * @param handle
	 * @param listener
	 *            XXX
	 * @throws RemoteException
	 */
	public abstract void addVisualizingListener(String handle, VisualizableListener listener) throws RemoteException;

	/**
	 * @throws RemoteException
	 */
	public abstract void close() throws RemoteException;

	/**
	 * @return an {@link String} object for this service.
	 * @throws RemoteException
	 */
	public abstract String getEvolutionHandle() throws RemoteException;

	/**
	 * @return a collection of {@link String} objects for this service.
	 * @throws RemoteException
	 */
	public abstract Collection<String> getEvolutionHandles() throws RemoteException;

	/**
	 * CONSIDER do we actually need this?
	 * 
	 * @param handle
	 * @return the actual model for this handle
	 * @throws RemoteException
	 */
	public abstract VisualizationModel getVisualizationModel(final String handle) throws RemoteException;

	/**
	 * @return an {@link String} object for this service.
	 * @throws RemoteException
	 */
	public abstract String getVisualizationModelHandle() throws RemoteException;

	/**
	 * @return a collection of {@link String} objects for this service.
	 * @throws RemoteException
	 */
	public abstract Collection<String> getVisualizationModelHandles() throws RemoteException;

	/**
	 * @param handle
	 * @throws RemoteException
	 */
	public abstract void runEvolution(String handle) throws RemoteException;

	/**
	 * @param handle
	 * @param listener
	 * @throws RemoteException
	 */
	public abstract void setRepaintListener(final String handle, RepaintListener listener) throws RemoteException;
}
