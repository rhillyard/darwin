/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Phenome_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.31 $
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.FitnessEngine;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class defining base methods and fields for implementations of
 * {@link Phenome} interface.
 * 
 * @author Robin Hillyard
 * 
 *         XXX consider extending StringHash instead.
 */
@SuppressWarnings("serial")
public abstract class Phenome_<P> extends Base implements Phenome<P> {

	/**
	 * This constructs a new (empty) Phenome belonging to a Taxon.
	 * 
	 * @param fitnessFunction
	 *            the implementer of FitnessEngine which evaluates the fintness
	 *            of a phenome for an environemnt.
	 * 
	 */
	protected Phenome_(final FitnessEngine fitnessFunction) {
		super();
		this._fitnessEngine = fitnessFunction;
		this._characters = new ArrayList<>();
	}

	/**
	 * @see net.sf.darwin.core.Phenome#addCharacter(net.sf.darwin.core.Pharacter)
	 */
	@Override
	public int addCharacter(final Pharacter<P> character) {
		final int result = getCharacterList().size();
		if (getCharacterList().add(character)) {
			// character.setPhenome(this);
			return result;
		}
		throw new DarwinException("Phenome_.addCharacter(): character not unique"); //$NON-NLS-1$
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Phenome_<?> other = (Phenome_<?>) obj;
		if (getFitnessEngine() == null) {
			if (other.getFitnessEngine() != null)
				return false;
		} else if (!getFitnessEngine().equals(other.getFitnessEngine()))
			return false;
		return true;
	}

	/**
	 * Get the character whose identifier matches <code>id</code>.
	 * 
	 * @see net.sf.darwin.core.Phenome#getCharacter(java.lang.String)
	 */
	@Override
	public Pharacter<P> getCharacter(final String id) {
		for (final Pharacter<P> character : getCharacterList()) {
			if (character.getIdentifier().equals(id))
				return character;
		}
		return null;
	}

	/**
	 * @return the set of character keys for this Phenome
	 */
	@Override
	public Collection<String> getCharacterKeys() {
		final TreeSet<String> result = new TreeSet<>();
		for (final Pharacter<P> pharacter : getCharacters())
			result.add(pharacter.getIdentifier());
		return result;
	}

	/**
	 * @return the characters
	 */
	@Override
	public Collection<Pharacter<P>> getCharacters() {
		return getCharacterList();
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Phenome#getData()
	 */
	@Override
	public Object getData() {
		return this.applicationData;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Phenome#getFitnessEngine()
	 */
	@Override
	public FitnessEngine getFitnessEngine() {
		return this._fitnessEngine;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getFitnessEngine() == null) ? 0 : getFitnessEngine().hashCode());
		return result;
	}

	/**
	 * Set the phenotypic characters, usually by reflection/dependeny injection.
	 * 
	 * @param characters
	 */
	public void setCharacters(final Collection<? extends Pharacter<P>> characters) {
		getCharacterList().clear();
		getCharacterList().addAll(characters);
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Phenome#setData(java.lang.Object)
	 */
	@Override
	public void setData(final Object data) {
		this.applicationData = data;
	}

	/**
	 * @see net.sf.darwin.core.Phenome#size()
	 */
	@Override
	public int size() {
		return getCharacterList().size();
	}

	/**
	 * @return
	 */
	private List<Pharacter<P>> getCharacterList() {
		return this._characters;
	}

	// TODO consider making this non-transient because the characters don't
	// really change.
	private transient final List<Pharacter<P>> _characters;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Phenome_.class);

	/**
	 * The fitness function which yields a fitness and weight for a particular
	 * Variant, given a particular EcoFactor.
	 */
	private final FitnessEngine _fitnessEngine;

	/**
	 * Application-specific data
	 */
	private transient Object applicationData;

}