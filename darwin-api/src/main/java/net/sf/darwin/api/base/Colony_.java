/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Colony_.java
 * Created on Jan 5, 2010
 * @version $Revision: 1.13 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.darwin.api.factory.OrganismFactory;
import net.sf.darwin.api.impl.FrequencyMap_HashMap;
import net.sf.darwin.api.impl.Loggable;
import net.sf.darwin.api.impl.MortalityStats;
import net.sf.darwin.api.impl.NumericFrequencyMap;
import net.sf.darwin.api.impl.OrganismCensusContext;
import net.sf.darwin.api.impl.Organism_Asexual;
import net.sf.darwin.api.impl.Organism_Sexual;
import net.sf.darwin.api.impl.Viability_Fitness;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.EnvironmentListener;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FrequencyMap;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.HasMean;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Lek;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Nuclear;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Stats;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Viability;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.core.VisualizableListener;
import net.sf.tostring0.AToString;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.random.RandomGenerator;

/**
 * @author Robin Hillyard
 * 
 *         TODO this is a huge type file. Consider splitting it up somehow.
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            colony
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            colony
 */
public abstract class Colony_<E, P, G> extends Base implements Colony<E, P, G>, EnvironmentListener<E> {

	/**
	 * @param identifier
	 *            the name of this colony
	 * @param environment
	 *            the environment in which this colony exists
	 * @param random
	 *            a random number source used by the colony for reproduction
	 */
	protected Colony_(final String identifier, final Environment<E> environment, final RandomGenerator random) {
		this._identifier = identifier;
		this._index = -1;
		this._environment = environment;
		this._random = random;
		this._organisms = new ArrayList<>();
		// FIXME why is the following field declared static?
		this._traitFrequencyMaps = new HashMap<>();
		init();
	}

	/**
	 * Note that this method does NOT invoke the populationChanged() method. The
	 * caller MUST call that method once a set of additions has been made.
	 * 
	 * @see net.sf.darwin.core.Colony#addOrganism(net.sf.darwin.core.Organism)
	 */
	@Override
	public synchronized boolean addOrganism(final Organism<E, P, G> organism) {
		return getOrganismList().add(organism);
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		String result = "Colony" + S_COLON + getIdentifier();//$NON-NLS-1$
		result += " and count: " + getCount(); //$NON-NLS-1$
		census.present(result, context);
		return true;
	}

	/**
	 * <p>
	 * Method to clean this colony up after processing the current generation.
	 * Called by {@link Population_#nextGeneration()}. Some individuals will be
	 * culled (according to the mortality implementation, etc.).
	 * </p>
	 * 
	 * <p>
	 * Normally, this method will not be overridden by sub-classes because it
	 * contains critical logic. If you want to do additional logic, you would
	 * normally override one or more of the following methods:
	 * <ul>
	 * <li>{@link #removeDeadOrganisms()}</li>
	 * <li>{@link #populationChanged(Object)}</li>
	 * </ul>
	 * </p>
	 * <p>
	 * Here is the exact sequence of events for this method:
	 * <ol>
	 * <li>increment {@link #sequence}</li>
	 * <li>call {@link #removeDeadOrganisms()}</li>
	 * <li>call {@link #populationChanged(Object)}</li>
	 * </ol>
	 * </p>
	 * 
	 * @see net.sf.darwin.core.Evolvable#nextGeneration()
	 */
	@Override
	final public void cleanupGeneration() {
		if (removeDeadOrganisms())
			populationChanged("post-generation"); //$NON-NLS-1$
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		final Colony_<E, P, G> result = (Colony_<E, P, G>) super.clone();
		result._environment = (Environment_<E>) ((Environment_<E>) this._environment).clone();
		result._identifier = getDaughterId();
		result._organisms = (List<Organism<E, P, G>>) ((ArrayList<Organism<E, P, G>>) getOrganismList()).clone();
		final int size = result.getOrganismList().size();
		for (int i = 0; i < size; i++) {
			final Organism<E, P, G> organism = result.getOrganismList().get(i);
			final Organism_<E, P, G> clone = (Organism_<E, P, G>) ((Organism_<E, P, G>) organism).clone();
			result.getOrganismList().set(i, clone);
			clone.setColony(result);
			clone.getNuclear().setColonyId(result._identifier);
		}
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Colony#createIdentifier()
	 */
	@Override
	public String createIdentifier() {
		return this.sequence + "-" + getRandom().nextInt(10000); //$NON-NLS-1$
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Evolvable#cullMembers()
	 */
	@Override
	public void cullMembers() {
		getOrganismList().clear();
		populationChanged("after cull"); //$NON-NLS-1$
	}

	/**
	 * CONSIDER looks like feature envy
	 * 
	 * @param census
	 *            the census object
	 * @param context
	 * @return true if we should continue censusing the children of this object
	 */
	public boolean doCensusDetail(final Census census, final Object context) {
		final String identifier = getIdentifier();
		census.present("Census for colony: " + identifier, null); //$NON-NLS-1$
		census.present("Generation: " + getPopulation().getTaxon().getGeneration() + " in context: " + context, null); //$NON-NLS-1$ //$NON-NLS-2$			
		getEnvironment().censusMe(census, context);
		census.present((isIsolated() ? " isolated" : ""), null); //$NON-NLS-1$ //$NON-NLS-2$
		// TODO try to do this without having to use Census_Sink
		if (census instanceof Census_Sink)
			outputFrequencies((Census_Sink) census);
		census.present("", null); //$NON-NLS-1$
		return false;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	@ToString(omit = true)
	public Collection<? extends Censusible> getCensusibleChildren() {
		final Collection<Censusible> result = new ArrayList<>();
		result.add(getEnvironment());
		for (final Organism<E, P, G> organism : getOrganisms())
			result.add(organism);
		return result;
	}

	/**
	 * @return the number of organisms in this population.
	 * @see java.util.Collection#size()
	 */
	@Override
	@ToString(name = "population")
	public int getCount() {
		return getOrganismList().size();
	}

	/**
	 * @return {@link #_environment}
	 * @see net.sf.darwin.core.Colony#getEnvironment()
	 */
	@Override
	@ToString(detail = 1)
	public Environment<E> getEnvironment() {
		return this._environment;
	}

	/**
	 * @return {@link #_identifier}
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @see net.sf.darwin.core.Visualizable#getIndividuals()
	 */
	@Override
	@SuppressWarnings("unchecked")
	@ToString(omit = true)
	public Collection<Individual> getIndividuals() {
		final Collection<? extends Individual> organisms = getOrganisms();
		return (Collection<Individual>) organisms;
	}

	/**
	 * @return an {@link Iterator} of {@link Organism} objects.
	 * @see net.sf.darwin.core.Colony#getOrganisms()
	 */
	@Override
	public Collection<Organism<E, P, G>> getOrganisms() {
		return getOrganismList();
	}

	/**
	 * @return {@link #_population}, the population to which this {@link Colony}
	 *         belongs.
	 * @see net.sf.darwin.core.Colony#getPopulation()
	 */
	@Override
	@ToString(parent = true)
	public Population<E, P, G> getPopulation() {
		return this._population;
	}

	/**
	 * TODO consider eliminating this method
	 * 
	 * @return the result of {@link Environment#getRealm()} applied to the
	 *         result of {@link #getEnvironment()}.
	 * 
	 * @see net.sf.darwin.core.Visualizable#getRealm()
	 */
	@Override
	@ToString(omit = true)
	public Realm getRealm() {
		return getEnvironment().getRealm();
	}

	/**
	 * Get the current saturation for this colony. Technically, this is the
	 * value of N/K in the Verhulst equation of population dynamics. In general,
	 * this value will depend on this colony, its environment and the
	 * populations of any other colonies sharing the same environment.
	 * 
	 * This implementation ignores any other colonies which share the colony's
	 * environment.
	 * 
	 * @return a number between 0 and 1 (although it is possible to have a
	 *         super-saturated environment where the saturation is greater than
	 *         1.
	 * 
	 * @see net.sf.darwin.core.Colony#getSaturation()
	 */
	@Override
	public double getSaturation() {
		final int population = getCount();
		final long idealPopulation = getEnvironment().getIdealPopulation();
		if (idealPopulation != 0)
			return 1.0 * population / idealPopulation;

		return 1.0;
	}

	/**
	 * @see net.sf.darwin.core.Visualizable#getSink()
	 */
	@Override
	@ToString(omit = true)
	public Sink getSink() {
		final Census censusTaker = getPopulation().getTaxon().getCensusTaker();
		// TODO consider doing this via polymorphism
		if (censusTaker instanceof Sink) {
			final Sink sink = (Sink) censusTaker;
			return sink;
		}
		return null;
	}

	/**
	 * @param fittestOriginal
	 *            the starting value for fitness
	 * @param pardonsPerColony
	 * @return the fittest value that we have now
	 * @throws FitnessException
	 * @see net.sf.darwin.core.Colony#grantPardons(double, int)
	 */
	@Override
	public double grantPardons(final double fittestOriginal, final int pardonsPerColony) throws FitnessException {
		double fittest = fittestOriginal;
		for (final Organism<E, P, G> organism : getOrganisms()) {
			int pardons = pardonsPerColony;
			final double fitness = organism.getFitness(getPopulation().getTaxon().getPhenome().getFitnessEngine()).doubleValue();
			// final double fitnessOld =
			// organism.getPhenotype().evaluateFitness(getEnvironment(),
			// getSystem().getPhenome().getFitnessFunction());
			if (pardons > 0 && fitness >= fittest) {
				fittest = fitness;
				if (!organism.isViable()) {
					organism.setViable(true);
					pardons--;
					if (Population_.LOG.isTraceEnabled())
						Population_.LOG.trace("Granting clemency to: " + ((AToString) organism).toStringId()); //$NON-NLS-1$
				}
			}

		}
		return fittest;
	}

	/**
	 * @return {@link #isolated}, the value of the isolated flag. True if
	 *         females from this population cannot find mates in other
	 *         populations.
	 * @see net.sf.darwin.core.Insular#isIsolated()
	 */
	@Override
	@ToString
	public boolean isIsolated() {
		return this.isolated;
	}

	/**
	 * @param viability
	 *            the viability which will determine whether an organism is
	 *            viable or not.
	 * @throws FitnessException
	 * @see net.sf.darwin.core.Colony#markDeadOrganisms(net.sf.darwin.core.Viability)
	 */
	@Override
	public synchronized void markDeadOrganisms(final Viability viability) throws FitnessException {
		for (final Organism<E, P, G> organism : getOrganisms())
			organism.setViability(viability);
		final Stats stats = viability.getStats();
		if (stats != null)
			stats.logStats(LOG, getPopulation().getTaxon().getGeneration());
	}

	/**
	 * @param bestOrganism
	 * @see net.sf.darwin.core.Colony#normalizeGenomes(net.sf.darwin.core.Organism)
	 */
	@Override
	public void normalizeGenomes(final Organism<E, P, G> bestOrganism) {
		final Genome<P, G> bestGenome = bestOrganism.getGenome();

		// Set up a cache of base-string -> normalized genome pairs
		final Map<String, Genome<P, G>> cache = new HashMap<>();

		// First we loop through all of the organisms, except the best organism
		// (which we want to stay intact).
		for (final Organism<E, P, G> organism : getOrganisms()) {
			if (organism != bestOrganism)
				organism.normalizeGenome(bestGenome, cache, getEnvironment());
		}

		// Finally, we go back and normalize the best organism, which should
		// result in an empty genome.
		bestOrganism.normalizeGenome(bestGenome, cache, getEnvironment());

		// Check that best genome is empty
		final int size = bestGenome.size();
		if (size != 0)
			Population_.LOG.warn("normalized best genome should be empty"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.core.EnvironmentListener#onEnvironmentChange(net.sf.darwin.core.Environment)
	 */
	@Override
	public void onEnvironmentChange(final Environment<E> env) {

		visualizableChanged(env, "environment change"); //$NON-NLS-1$
		// XXX note that ideally we would only flush those elements of the cache
		// relating to this Colony
		env.getRealm().getPhenotypeCache().flush();
		final Population<E, P, G> population = getPopulation();
		if (population != null) {
			final Phenome<P> phenome = population.getTaxon().getPhenome();
			phenome.getFitnessEngine().onEnvironmentChange(phenome.getCharacterKeys(), getEnvironment());
		}
	}

	/**
	 * Method to respond to the fact that the population of Organisms in this
	 * Colony have changed.
	 * 
	 * @param context
	 *            something that characterizes what caused the population change
	 * 
	 * @see net.sf.darwin.core.Colony#populationChanged(java.lang.Object)
	 */
	@Override
	public void populationChanged(final Object context) {
		visualizableChanged(this, context);
	}

	/**
	 * @see net.sf.darwin.core.Evolvable#seedMembers()
	 */
	@Override
	public void seedMembers() {

		final int population = getPopulation().getTaxon().getSeedPopulation();
		boolean populationChanged = false;

		for (int i = 0; i < population; i++) {
			final Organism<E, P, G> organism = createOrganism();
			if (organism != null)
				populationChanged = true;
		}

		if (populationChanged) {
			populationChanged("seed members"); //$NON-NLS-1$
			final Stats stats = new MortalityStats(this);
			try {
				markDeadOrganisms(new Viability_Fitness(this, LOG, stats, true));
			} catch (final FitnessException e) {
				LOG.warn("seedMembers(): fitness exception", e); //$NON-NLS-1$
			}
		}
	}

	/**
	 * set {@link #_index} to the value of
	 * 
	 * @param index
	 * @see net.sf.darwin.core.Colony#setIndex(int)
	 */
	@Override
	public void setIndex(final int index) {
		this._index = index;
	}

	/**
	 * @param isolated
	 *            the isolated to set
	 * @see net.sf.darwin.core.Colony#setIsolated(boolean)
	 */
	@Override
	public void setIsolated(final boolean isolated) {
		this.isolated = isolated;
	}

	/**
	 * set {@link #nDaughters} to
	 * 
	 * @param size
	 *            and set {@link #daughterSequence} to zero.
	 * @see net.sf.darwin.core.Colony#setNDaughters(int)
	 */
	@Override
	public void setNDaughters(final int size) {
		this.nDaughters = size;
		this.daughterSequence = 0;
	}

	/**
	 * set {@link #_population} to the value of
	 * 
	 * @param population
	 *            the population to set
	 * @see net.sf.darwin.core.Colony#setPopulation(net.sf.darwin.core.Population)
	 */
	@Override
	public void setPopulation(final Population<E, P, G> population) {
		this._population = population;
	}

	/**
	 * <p>
	 * Method to set up this colony for the current generation. Called by
	 * {@link Population_#nextGeneration()}. Some individuals will be culled
	 * (according to the mortality implementation, etc.); while each mating pair
	 * will produce some progeny.
	 * </p>
	 * 
	 * <p>
	 * Normally, this method will not be overridden by sub-classes because it
	 * contains critical logic. If you want to do additional logic, you would
	 * normally override one or more of the following methods:
	 * <ul>
	 * <li>{@link #markDeadOrganisms(Viability)}</li>
	 * <li>{@link #neonatalCare()}</li>
	 * <li>{@link #getProgenyAsexual()}</li>
	 * <li>{@link #getProgenySexual()}</li>
	 * <li>{@link #populationChanged(Object)}</li>
	 * </ul>
	 * </p>
	 * <p>
	 * Here is the exact sequence of events for this method (each step depends
	 * on the success of the previous step):
	 * <ol>
	 * <li>increment {@link #sequence}</li>
	 * <li>call {@link #ageOrganisms()}</li>
	 * <li>call {@link #markDeadOrganisms(Viability)} with false parameter</li>
	 * <li>call {@link #getProgenySexual()} or {@link #getProgenyAsexual()}
	 * according to the whether the system's genome is diploid or haploid.</li>
	 * <li>call {@link #neonatalCare()}</li>
	 * <li>call {@link #populationChanged(Object)}</li>
	 * </ol>
	 * </p>
	 * 
	 * @return true if everything worked as expected.
	 * 
	 * @see net.sf.darwin.core.Evolvable#nextGeneration()
	 */
	@Override
	final public boolean setupGeneration() {
		boolean ok = true;
		ageOrganisms();

		final Taxon system = getPopulation().getTaxon();
		final Phenome phenome = system.getPhenome();

		final Stats stats = new MortalityStats(this);
		Number adjustmentFactor = Double.valueOf(1);
		int count = 10;
		while (true) {
			phenome.getFitnessEngine().setFitnessAdjustment(phenome.getCharacterKeys(), adjustmentFactor);
			try {
				markDeadOrganisms(new Viability_Fitness(this, LOG, stats, false));
				break;
			} catch (final FitnessException e) {
				if (count-- == 0)
					break;
				adjustmentFactor = getFitnessAdjustment(e);
			}
		}

		final int ploidy = system.getGenomic().getPloidy();

		if (ploidy == Ploidy.DIPLOID)
			ok = getProgenySexual();

		else if (ploidy == Ploidy.HAPLOID)
			getProgenyAsexual();

		else
			throw new DarwinException("Colony.setupGeneration(): " + ploidy + "-systems not supported"); //$NON-NLS-1$ //$NON-NLS-2$

		try {
			neonatalCare();
			if (ok)
				populationChanged("mid-generation"); //$NON-NLS-1$
			return ok;
		} catch (final FitnessException e) {
			throw new DarwinException("Colony.setupGeneration(): " + ploidy + "-systems not supported"); //$NON-NLS-1$ //$NON-NLS-2$
		}

	}

	/**
	 * @param n
	 *            the odds against the result being true.
	 * @return true if the next random number in range 0..n-1 turns out to be a
	 *         zero.
	 * 
	 * @see net.sf.darwin.core.Colony#spin(int)
	 */
	@Override
	public boolean spin(final int n) {
		final int r = getRandom().nextInt(n); // XXX inline this
		return r == 0;
	}

	/**
	 * @param thinFactor
	 * @throws FitnessException
	 * @see net.sf.darwin.core.Colony#thin(int)
	 */
	@Override
	public void thin(final int thinFactor) throws FitnessException {
		markDeadOrganisms(new Viability_(null) {

			@Override
			public <F, U, W> boolean resetViability(final Organism<F, U, W> organism) {
				organism.setViable(spin(thinFactor));
				return true;
			}

		});
		cleanupGeneration();
	}

	/**
	 * This method ages all current organisms.
	 */
	protected void ageOrganisms() {
		for (final Organism<E, P, G> organism : getOrganisms()) {
			organism.age();
		}
	}

	/**
	 * @return a suitable name for a daughter colony.
	 */
	protected abstract String getDaughterId();

	/**
	 * @return the current value of {@link #daughterSequence} which is then
	 *         incremented
	 */
	protected int getDaughterSequence() {
		return this.daughterSequence++;
	}

	/**
	 * @return {@link #_index}, the index
	 */
	protected int getIndex() {
		return this._index;
	}

	/**
	 * @return {@link #nDaughters}, the number of daughter colonies
	 */
	protected int getNDaughters() {
		return this.nDaughters;
	}

	protected synchronized void getProgenyAsexual() {
		final int fecundity = getPopulation().getTaxon().getFecundity().getFecundity(this);
		final Collection<Organism<E, P, G>> progeny = new ArrayList<>();
		for (final Organism<E, P, G> organism : getOrganisms()) {
			// since every organism is potentially reproducing
			// (unlike in Sexual organisms where individuals have to mate),
			// we allow only about half of the organisms to reproduce.
			if (getRandom().nextInt(2) == 0)
				progeny.addAll(organism.getAsexualProgeny(fecundity));
		}
		for (final Organism<E, P, G> organism : progeny) {
			addOrganism(organism);
		}
	}

	/**
	 * @return true if the colony is healthy or if the generation is less than 3
	 */
	protected boolean getProgenySexual() {
		final Collection<Mating> pairs = findPairs(alternativeColony());

		final boolean healthy = pairs.size() > 0;

		if (healthy) {
			if (LOG.isDebugEnabled())
				LOG.debug("number of mating pairs: " + pairs.size()); //$NON-NLS-1$
			final int fecundity = getPopulation().getTaxon().getFecundity().getFecundity(this);
			final Collection<Organism<E, P, G>> neonates = new ArrayList<>();
			for (final Mating mating : pairs) {
				neonates.addAll(addProgeny(mating, fecundity));
			}
			// The following code is something of an alternative to the registry
			// system
			if (LOG.isDebugEnabled())
				logNeonates(LOG, neonates);

		} else
			LOG.warn("Population.nextGeneration(): no mating pairs for " + getIdentifier() + " with " + getCount() + " organisms"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		return healthy || getPopulation().getTaxon().getGeneration() < 3;
	}

	/**
	 * @return the RNG as set by the consructor.
	 */
	protected RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * Convenience method. If the system has not yet been set, an empty list is
	 * returned.
	 * 
	 * TODO not sure why the population or system would be null when this is
	 * called.
	 * 
	 * @return the collection of VisualizableListener objects
	 */
	@ToString(omit = true)
	protected Collection<VisualizableListener> getVisualizableListeners() {
		if (getPopulation() != null && getPopulation().getTaxon() != null)
			return getPopulation().getTaxon().getVisualizableListeners();
		return new ArrayList<>();
	}

	/**
	 * Register all births with the Registry (if any); Mark dead organisms (with
	 * true parameter).
	 * 
	 * @throws FitnessException
	 */
	protected void neonatalCare() throws FitnessException {
		final Collection<Organism<E, P, G>> neonates = new ArrayList<>();
		for (final Organism<E, P, G> organism : getOrganisms()) {
			if (organism.getAge() == 0)
				neonates.add(organism);
		}
		getPopulation().getTaxon().registerBirths(this, neonates);
		markDeadOrganisms(new Viability_Fitness(this, LOG, new MortalityStats(this), true));
	}

	/**
	 * @return true if we killed anyone off
	 */
	protected synchronized boolean removeDeadOrganisms() {
		int obituaries = 0;
		for (final Iterator<Organism<E, P, G>> iter = getOrganisms().iterator(); iter.hasNext();) {
			final Organism<E, P, G> organism = iter.next();
			if (!organism.isViable()) {
				// First we "bury" the organism
				organism.setColony(null);
				// Then we remove it from our list of organisms.
				iter.remove();
				obituaries++;
			}
		}
		return obituaries > 0;
	}

	/**
	 * This method should be called whenever this population (or an environment)
	 * has changed, for example after seeding, or getting the next generation.
	 * 
	 * @param source
	 *            the source of the change (either this population or its
	 *            environment).
	 * @param context
	 *            the context of the change
	 */
	protected void visualizableChanged(final Visualizable source, final Object context) {
		for (final VisualizableListener listener : getVisualizableListeners()) {
			listener.onChange(source, context);
		}
		// TODO consider doing this via polymorphism
		if (source instanceof Environment) {
			for (final Organism<E, P, G> organism : getOrganisms()) {
				organism.onEnvironmentChange((Environment<E>) source);
				organism.getGenome().environmentChanged(this);
			}
			LOG.info(MessageFormat.format("Organisms of {0} updated due to change in {1}", getIdentifier(), source)); //$NON-NLS-1$
		}
	}

	/**
	 * Create a new organism from the factory and add it to this population.
	 * 
	 * @param mating
	 * @param fecundity
	 * @return
	 */
	private Collection<Organism<E, P, G>> addProgeny(final Mating mating, final int fecundity) {
		final Collection<Organism<E, P, G>> result = new ArrayList<>();
		for (int gravis = 0; gravis < fecundity; gravis++) {
			final Nuclear<P, G> neonate = mating.progeny();
			final Organism<E, P, G> organism = OrganismFactory.makeOrganism(neonate, this, createIdentifier());
			addOrganism(organism);
			result.add(organism);
			// organism.getAge(); // XXX why do we do this? Seems like it has no
			// // effect.
			if (LOG.isTraceEnabled())
				LOG.trace("New progeny for population (sexual): " + getIdentifier() + S_COLON + neonate.toString() + " and genome: " + neonate.getGenome()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return result;
	}

	/**
	 * Get the "other" colony, based on the index of this colony.
	 * 
	 * TODO let's get rid of this indexing idea.
	 * 
	 * @return the other colony (or null if this colony is isolated)
	 */
	private Colony<E, P, G> alternativeColony() {
		if (!isIsolated()) {
			final int colonies = getPopulation().getColonies().size();
			if (colonies != 2)
				LOG.info("Only one population for sexual reproduction in non-isolated population"); //$NON-NLS-1$
			final int otherPopIndex = colonies - 1 - getIndex();
			Colony<E, P, G> result = null;
			if (otherPopIndex >= 0 && otherPopIndex != getIndex())
				result = getPopulation().getColony(otherPopIndex);
			return result;
		}
		return null;
	}

	/**
	 * Method to create a new organism by seeding.
	 * 
	 * @return
	 */
	private Organism<E, P, G> createOrganism() {
		if (getPopulation().getTaxon().getGenomic().getPloidy() == Ploidy.DIPLOID) {
			return Organism_Sexual.seed(this);
		}

		return Organism_Asexual.seed(this);
	}

	/**
	 * Method to enumerate a set of mating pairs from this Colony, with the
	 * possibility of finding mates from the <code>alternativeColony</code>.
	 * 
	 * @param alternativeColony
	 *            either an alternative colony from which to find a mate,
	 *            otherwise null.
	 * 
	 * @return a Vector of Mating pairs.
	 */
	private synchronized Collection<Mating> findPairs(final Colony<E, P, G> alternativeColony) {
		final MateChoice chooser = getPopulation().getTaxon().getChooser();
		return chooser.findPairs(getOrganisms(), chooser.getLek(this), getAlternativeLek(chooser, alternativeColony));
	}

	/**
	 * Return an alternative lek provided that this colony is not isolated, and
	 * that the given <code>colony</code> is not null or equal to this colony.
	 * 
	 * @param chooser
	 *            the chooser.
	 * @param colony
	 *            the colony from which we draw the lek (may be null or the same
	 *            as this): in either case we return null
	 * @return either a valid alternative lek (or null).
	 */
	private Lek<E, P, G> getAlternativeLek(final MateChoice chooser, final Colony<E, P, G> colony) {
		if (colony == null || this.equals(colony) || isIsolated())
			return null;
		return chooser.getLek(colony);
	}

	/**
	 * @return
	 */
	private List<Organism<E, P, G>> getOrganismList() {
		return this._organisms;
	}

	/**
	 * 
	 */
	private void init() {
		getEnvironment().addListener(this);
	}

	/**
	 * Private static method to output the wing color frequencies for census
	 * purposes.
	 * 
	 * @param sinkCensus
	 *            the censuser
	 */
	private void outputFrequencies(final SinkCensus sinkCensus) {

		final FrequencyMap_HashMap<Allele<G>> alleleFrequencies = new FrequencyMap_HashMap<>("asexual alleles"); //$NON-NLS-1$
		final NumericFrequencyMap ageFrequencies = new NumericFrequencyMap("ages"); //$NON-NLS-1$
		final FrequencyMap_HashMap<String> sexFrequencies = new FrequencyMap_HashMap<>("sex"); //$NON-NLS-1$

		for (final Pharacter<P> character : getPopulation().getTaxon().getPhenome().getCharacters())
			updateFrequencies(character, sinkCensus, alleleFrequencies, ageFrequencies, sexFrequencies);

		sinkCensus.println("total: " + ageFrequencies.getTotal()); //$NON-NLS-1$
		sexFrequencies.output(sinkCensus);
		ageFrequencies.output(sinkCensus);
		alleleFrequencies.output(sinkCensus);
		sinkCensus.println(" mean age: " + ageFrequencies.mean()); //$NON-NLS-1$
		for (final String key : this._traitFrequencyMaps.keySet())
			mapToSink(this._traitFrequencyMaps.get(key), sinkCensus, key);
	}

	/**
	 * @param character
	 * @param sinkCensus
	 * @param alleleFrequencies
	 * @param ageFrequencies
	 * @param sexFrequencies
	 */
	@SuppressWarnings("rawtypes")
	private void updateFrequencies(final Pharacter<P> character, final SinkCensus sinkCensus,
			final FrequencyMap<Allele<G>> alleleFrequencies, final NumericFrequencyMap ageFrequencies,
			final FrequencyMap<String> sexFrequencies) {
		// This (traitFrequencies) will not work where the traits are
		// instances of Trait_Variable.
		// In such a case, you must use TraitFrequencyMap instead.
		// But for a generic census, this should be OK.
		final FrequencyMap<Trait<P>> traitFrequencies = new FrequencyMap_HashMap<>(character.getIdentifier());
		this._traitFrequencyMaps.put(character.getIdentifier(), traitFrequencies);
		final OrganismCensusContext context = new OrganismCensusContext(character, alleleFrequencies, ageFrequencies,
				sexFrequencies, traitFrequencies);
		synchronized (this) {
			for (final Organism organism : getOrganisms()) {
				organism.censusMe(sinkCensus, context);
			}
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * TODO consider putting this in a separate class, with daughterSequence
	 */
	private int nDaughters;

	private int daughterSequence;

	private Population<E, P, G> _population;

	/**
	 * The environment in which this population exists. This should be final but
	 * we need to set it in clone().
	 */
	protected Environment<E> _environment;

	/**
	 * The string by which this population may be identified. This should be
	 * final but is needed to be non-final by the clone() method.
	 */
	protected String _identifier;

	/**
	 * A random number generator for this population.
	 */
	protected final RandomGenerator _random;

	/**
	 * collection of organisms in this population. Note that this is non-final
	 * only for the sake of the clone() method.
	 */
	protected transient List<Organism<E, P, G>> _organisms;

	/**
	 * A sequence number for creating identifiers.
	 */
	protected transient int sequence = 0;

	/**
	 * If true, then members of <code>this</code> population cannot find mates
	 * outside <code>this</code> population. This is used, for example, for a
	 * control "reservoir" population that is supposed to simulate the outside
	 * world.
	 */
	private transient boolean isolated = false;

	/**
	 * TODO consider removing this the index of this colony within its
	 * population.
	 */
	public transient int _index;

	/**
	 * The logger for this class.
	 */
	public static final Log LOG = LogFactory.getLog(Colony_.class);

	/**
	 */
	public static final String S_COLON = ": "; //$NON-NLS-1$

	private final Map<String, FrequencyMap<Trait<P>>> _traitFrequencyMaps;

	/**
	 * @param e
	 * @return
	 */
	private static Number getFitnessAdjustment(final FitnessException e) {
		switch (e.getProblem()) {
		case PoissonTargetExceedsValue:
			return e.getDiscrepancy();
		default:
			throw new DarwinException("setupGeneration: fitness exception cannot be handled", e); //$NON-NLS-1$;
		}
	}

	/**
	 * @param log2
	 * @param neonates
	 */
	@SuppressWarnings("unchecked")
	private static <F, U, W> void logNeonates(final Log log, final Collection<Organism<F, U, W>> neonates) {
		final FrequencyMap<String> mapGenotype = new FrequencyMap_HashMap<>("newborn genotypes"); //$NON-NLS-1$
		final FrequencyMap<String> mapPhenotype = new FrequencyMap_HashMap<>("newborn phenotypes"); //$NON-NLS-1$
		for (final Organism<F, U, W> organism : neonates) {
			mapGenotype.add(organism.getGenome().toString());
			mapPhenotype.add(organism.getPhenotype().getSignature());
		}
		// TODO protect these casts
		((Loggable<String>) mapGenotype).log(log);
		((Loggable<String>) mapPhenotype).log(log);
	}

	/**
	 * @param map
	 * @param sinkCensus
	 * @param traitKey
	 */
	@SuppressWarnings("unchecked")
	private static void mapToSink(final FrequencyMap<? extends Object> map, final SinkCensus sinkCensus, final String traitKey) {
		// TODO need to check the generic type of this...
		((FrequencyMap_HashMap<String>) map).output(sinkCensus);
		// TODO consider doing this via polymorphism
		if (map instanceof HasMean)
			sinkCensus.println("mean trait value for " + traitKey + " = " + ((HasMean) map).mean()); //$NON-NLS-1$ //$NON-NLS-2$
	}

}
