/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003-2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import java.util.Map;

import net.sf.darwin.api.base.Locus_Random;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.ToString;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * This final concrete implementation of {@link Locus} provides a simple scheme
 * for picking an allele based on a Random number source supplied at constructor
 * time. This is a sub-class of {@link Locus_Random}.
 * </p>
 * 
 * Named in honor of <a href="http://en.wikipedia.org/wiki/Trivers">Robert T.
 * Trivers.</a>.
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.9 $
 * 
 * @param <G>
 *            the type of the genetic information which is encoded in this Locus
 */
@Lifecycle(permanent = true)
public final class Locus_Triversian<G> extends Locus_Random<G> {

	/**
	 * Construct a new instance of Locus_Triversian using a specific given
	 * randomizer.
	 * 
	 * XXX it seems that it's OK, at least in testing, for random to be null.
	 * However, we shouldn't allow that, probably.
	 * 
	 * @param identifier
	 *            the identifier for this locus.
	 * @param random
	 *            an instance of {@link Randomizer}.
	 */
	public Locus_Triversian(final String identifier, final RandomGenerator random) {
		super(identifier, random);
	}

	/**
	 * @see net.sf.darwin.api.base.Locus_#getAlleles()
	 */
	@ToString(omit = true)
	@Override
	public Map<Allele<G>, Integer> getAlleles() {
		return super.getAlleles();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6740645895492374913L;

}
