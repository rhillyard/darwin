/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Genomic_;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Ploidy;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Concrete implementation of {@link Genomic} representing the possible genetic
 * information contained in a Taxon. A Genomic_Haploid may contain any number of
 * gene Loci. A Genomic_Haploid is essentially a template for a Genome. Both
 * structures are approximately parallel. <br>
 * Note that there is a sub-type, {@link Genomic_Asexual}, which should normally
 * be used rather than this type.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.9 $
 */
@Lifecycle(permanent = true)
public class Genomic_Haploid<T, V> extends Genomic_<T, V> {

	/**
	 * Primary Constructor for a new Genomic_Haploid instance using no meiosis
	 * implementation and the given mutator implementation.
	 * 
	 * @param identifier
	 * @param random
	 *            a random number source
	 * @param mutator
	 */
	public Genomic_Haploid(final String identifier, final RandomGenerator random, final Mutator<V> mutator) {
		super(identifier, Ploidy.HAPLOID, new Meiosis_Clone<V>(random), mutator);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8305182888274182993L;

}
