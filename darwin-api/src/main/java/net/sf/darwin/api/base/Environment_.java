/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Environment_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.43 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.impl.EcoSystem_Standard;
import net.sf.darwin.api.impl.Sink_Bucket;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.EcoSystem;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.EnvironmentListener;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Sink;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class defining base methods which implement {@link Environment}
 * interface.
 * 
 * @author Robin Hillyard
 */
@ToString(allBeanMethods = false)
public abstract class Environment_<E> extends Base implements Environment<E> {

	/**
	 * Secondary constructor of an empty unidentified environment with default
	 * initial value for ideal population (no eco factors).
	 * 
	 * @param realm
	 * 
	 */
	protected Environment_(final Realm realm) {
		this(Environment.S_ID_UNIDENTIFIED, realm);
	}

	/**
	 * Secondary constructor of an empty identified environment with default
	 * initial value for ideal population (no eco factors).
	 * 
	 * @param identifier
	 * @param realm
	 * 
	 */
	protected Environment_(final String identifier, final Realm realm) {
		this(identifier, realm, Environment.IDEAL_POPULATION_DEFAULT);
	}

	/**
	 * Primary constructor an empty identified environment with given ideal
	 * population (no eco factors).
	 * 
	 * @param identifier
	 *            the identifier for this Environment (for logging).
	 * @param realm
	 * @param idealPopulation
	 *            the initial value of ideal population
	 * 
	 */
	protected Environment_(final String identifier, final Realm realm, final long idealPopulation) {
		super();
		this._identifier = identifier;
		this._realm = realm;
		this._listeners = new ArrayList<>();
		this.idealPopulation = idealPopulation;
		LOG.debug(MessageFormat.format(LOG_MSG_2, getIdentifier()));
	}

	/**
	 * Not the favored method of adding an {@link EcoFactor} to an
	 * {@link Environment}. Normally, we use {@link #setEcoFactors(Collection)}.
	 * 
	 * @see net.sf.darwin.core.Environment#addFactor(net.sf.darwin.core.EcoFactor)
	 */
	@Override
	public Object addFactor(final EcoFactor<E> factor) {
		final Object result = getEcoSystem().add(factor);
		if (LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format(LOG_MSG_1, getIdentifier(), factor));
		environmentChanged();
		return result;
	}

	/**
	 * @param listener
	 *            an object which is interested in changes to the environment.
	 * @return true if the listener was successfully added.
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean addListener(final EnvironmentListener<E> listener) {
		return getListeners().add(listener);
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		return true;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		final Environment<E> result = (Environment_<E>) super.clone();
		result.setEcoSystem(new EcoSystem_Standard<>(result, result.getEcoSystem()));
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Environment_<?> other = (Environment_<?>) obj;
		if (this._realm == null) {
			if (other._realm != null)
				return false;
		} else if (!this._realm.equals(other._realm))
			return false;
		if (this._identifier == null) {
			if (other._identifier != null)
				return false;
		} else if (!this._identifier.equals(other._identifier))
			return false;
		return true;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Environment#fireEnvironmentChanged()
	 */
	@Override
	public void fireEnvironmentChanged() {
		environmentChanged();
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return getEcoSystem().values();
	}

	/**
	 * @see net.sf.darwin.core.Countable#getCount()
	 */
	@Override
	public int getCount() {
		return getEcoSystem().values().size();
	}

	/**
	 * 
	 * @see net.sf.darwin.core.Environment#getEcoFactorValue(java.lang.String)
	 */
	@Override
	public double getEcoFactorValue(final String name) {
		return getEcoSystem().getDoubleValue(name);
	}

	/**
	 * @return the value of {@link #_ecoSystem}
	 */
	@Override
	public EcoSystem<E> getEcoSystem() {
		return this._ecoSystem;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Environment#getIdealPopulation()
	 */
	@Override
	public long getIdealPopulation() {
		return this.idealPopulation;
	}

	/**
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @see net.sf.darwin.core.Visualizable#getIndividuals()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Individual> getIndividuals() {
		final Collection<? extends Individual> ecoFactors = getEcoSystem().values();
		return (Collection<Individual>) ecoFactors;
	}

	/**
	 * @return {@link #_listeners}, i.e. the current set of environment
	 *         listeners.
	 */
	@Override
	public Collection<EnvironmentListener<E>> getListeners() {
		return this._listeners;
	}

	/**
	 * @return the realm in which this environment acts.
	 * @see net.sf.darwin.core.Visualizable#getRealm()
	 */
	@Override
	public Realm getRealm() {
		return this._realm;
	}

	/**
	 * @see net.sf.darwin.core.CacheSignature#getSignature()
	 */
	@Override
	public String getSignature() {
		final StringBuilder result = new StringBuilder(getIdentifier());
		final EcoSystem<?> ecoSystem = getEcoSystem();
		for (final EcoFactor<?> ecoFactor : ecoSystem.values())
			result.append(ecoFactor);
		return result.toString();
	}

	/**
	 * @see net.sf.darwin.core.Visualizable#getSink()
	 */
	@Override
	public Sink getSink() {
		return this.sink;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._realm == null) ? 0 : this._realm.hashCode());
		result = prime * result + ((this._identifier == null) ? 0 : this._identifier.hashCode());
		return result;
	}

	/**
	 * Set the ecoFactors for this environment to the collection provided. Any
	 * prior ecoFactors are forgotten.
	 * 
	 * @param ecoFactors
	 */
	public void setEcoFactors(final Collection<EcoFactor<E>> ecoFactors) {
		final EcoSystem<E> ecoSystem = getEcoSystem();
		ecoSystem.setEcoFactors(ecoFactors);
		environmentChanged();
	}

	/**
	 * @param ecoSystem
	 *            the ecoSystem to set
	 */
	@Override
	public void setEcoSystem(final EcoSystem<E> ecoSystem) {
		this._ecoSystem = ecoSystem;
	}

	/**
	 * @see net.sf.darwin.core.Environment#setIdealPopulation(long)
	 */
	@Override
	public void setIdealPopulation(final long idealPopulation) {
		this.idealPopulation = idealPopulation;
	}

	/**
	 * Add the given set of environment listeners according to the collection
	 * provided. Any prior listeners will remain.
	 * 
	 * @param listeners
	 */
	@Override
	public void setListeners(final Collection<EnvironmentListener<E>> listeners) {
		getListeners().addAll(listeners);
	}

	/**
	 * @param sink
	 */
	public void setSink(final Sink sink) {
		this.sink = sink;
	}

	/**
	 * Find and set the eco factor to have the factor value, then, if
	 * successful, invoke {@link #fireEnvironmentChanged()}.
	 * 
	 * @param factorKey
	 * @param factorValue
	 * @return true if the environment was updated.
	 */
	@Override
	public boolean update(final String factorKey, final E factorValue) {
		boolean updated = false;

		final EcoSystem<E> ecoSystem = getEcoSystem();
		for (final EcoFactor<E> ecoFactor : ecoSystem.values()) {
			if (factorKey.equals(ecoFactor.getAttribute())) {
				ecoFactor.setValue(factorValue);
				updated = true;
				if (updated && LOG.isDebugEnabled())
					LOG.debug(MessageFormat.format(LOG_MSG_3, factorKey, factorValue));
			}
		}
		if (updated)
			fireEnvironmentChanged();
		return updated;
	}

	/**
	 * This method should be called whenever the environment has changed.
	 */
	protected void environmentChanged() {
		// We cycle through the registered listeners, including any populations
		// that inhabit this environment
		for (final EnvironmentListener<E> listener : getListeners()) {
			listener.onEnvironmentChange(this);
		}
	}

	/**
	 * ecoFactor {0} updated with value: {1}
	 */
	private static final String LOG_MSG_3 = "ecoFactor {0} updated with value: {1}"; //$NON-NLS-1$

	/**
	 * Environment: {0} created.
	 */
	private static final String LOG_MSG_2 = "Environment: {0} created."; //$NON-NLS-1$

	/**
	 * Environment: {0} has new factor: {1}
	 */
	private static final String LOG_MSG_1 = "Environment: {0} has new factor: {1}"; //$NON-NLS-1$

	private Sink sink = new Sink_Bucket();

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Environment_.class);

	/**
	 * This is the ideal number of organisms for this environment, i.e. the
	 * number that the environment can support. If more than one population
	 * exists in this environment, each will compete -- the ideal population
	 * refers to the <i>total</i> of all populations.
	 * 
	 * The default value is {@link Environment#IDEAL_POPULATION_DEFAULT}.
	 */
	protected transient long idealPopulation = Environment.IDEAL_POPULATION_DEFAULT;

	/**
	 * 
	 */
	private static final long serialVersionUID = -206198373889855587L;

	/**
	 * The set of EcoFactors for this environment. This field should be final,
	 * but we need it to be non-final for the sole purpose of the
	 * {@link #clone()} method.
	 * 
	 * TODO remove the specific concrete class name by using a factory class.
	 */
	@ToString(detail = 1)
	private transient EcoSystem<E> _ecoSystem = new EcoSystem_Standard<>(this);

	/**
	 * The factory for this environment.
	 */
	private final Realm _realm;

	/**
	 * Population Listener list
	 */
	@ToString(child = true)
	private final transient Collection<EnvironmentListener<E>> _listeners;

	/**
	 * The identifier of this Environment
	 */
	private final String _identifier;

}