/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TraitFrequencyMap.java
 * Created on Nov 6, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.HasMean;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Trait;

/**
 * Note that the objects that go into this frequency map may be variable and
 * therefore we call {@link Trait#getKey()} to determine how to uniquely
 * identify the trait for purposes of the frequency map.
 * 
 * If you are sure that your traits are discrete you can use
 * {@link FrequencyMap_HashMap} based on {@link Trait}.
 * 
 * You should not use this class for discrete traits.
 * 
 * @author Robin Hillyard
 * 
 */
public class TraitFrequencyMap<T> extends FrequencyMap_HashMap<Trait<T>> implements HasMean {

	/**
	 * @param name
	 */
	public TraitFrequencyMap(final String name) {
		super(name);
	}

	/**
	 * @param trait
	 * @see net.sf.darwin.api.impl.FrequencyMap_HashMap#add(java.lang.Object)
	 */
	@Override
	public void add(final Trait<T> trait) {
		super.add(trait);
	}

	/**
	 * @param trait
	 * @param increment
	 * @see net.sf.darwin.api.impl.FrequencyMap_HashMap#add(java.lang.Object,
	 *      int)
	 */
	@Override
	public void add(final Trait<T> trait, final int increment) {
		super.add(trait, increment);
	}

	/**
	 * @param trait
	 * @return true if the map contains the value of trait as a key.
	 * @see net.sf.darwin.api.impl.FrequencyMap_HashMap#containsKey(java.lang.Object)
	 */
	public boolean containsTrait(final Trait<T> trait) {
		return super.containsKey(trait);
	}

	/**
	 * @param trait
	 * @return the frequency value of trait.
	 * @see net.sf.darwin.api.impl.FrequencyMap_HashMap#get(java.lang.Object)
	 */
	@Override
	public int get(final Trait<T> trait) {
		return super.get(trait);
	}

	/**
	 * @return the weighted mean of the values in the map.
	 */
	@Override
	public double mean() {
		double result = 0;
		for (final Trait<T> key : keySet()) {
			// CONSIDER putting this back but I can't see how it could be right
			// if (key instanceof Number) {
			// result += (get(key)* ((Number) key).doubleValue());
			// // TODO consider doing this via polymorphism
			// } else
			if (key instanceof Quantifiable) {
				result += (get(key) * ((Quantifiable) key).doubleValue());
			} else
				throw new DarwinException("Undefined mean for frequency map"); //$NON-NLS-1$
		}
		return result / getTotal();
	}

	/**
	 * @param trait
	 * @return the (former) frequency of the value of trait.
	 * @see net.sf.darwin.api.impl.FrequencyMap_HashMap#remove(java.lang.Object)
	 */
	@Override
	public int remove(final Trait<T> trait) {
		return super.remove(trait);
	}

}
