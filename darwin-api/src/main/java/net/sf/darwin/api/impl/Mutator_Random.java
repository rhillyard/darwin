/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mutator_Random.java
 * Created on Apr 11, 2009
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Mutator_;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Chancy;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;

import org.apache.commons.math.random.RandomGenerator;

/**
 * This type of mutator uses a random number generator to determine whether or
 * not to mutate. It implements the Chancy interface.
 * 
 * All new instances of this type construct new {@link Randomizer} instances.
 * 
 * Despite having subtypes and not being final, this type is a legitimate
 * concrete type which is actually used (by the Mendels Peas example).
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public class Mutator_Random<V> extends Mutator_<V> implements Chancy {

	/**
	 * @param random
	 */
	public Mutator_Random(final RandomGenerator random) {
		super(false);
		this._random = new Randomizer(random);
	}

	/**
	 * @param random
	 * @param odds
	 */
	public Mutator_Random(final RandomGenerator random, final int odds) {
		super(false);
		final Randomizer randomizer = new Randomizer(random);
		randomizer.setOdds(odds);
		this._random = randomizer;
	}

	/**
	 * TEST
	 * 
	 * @param random
	 * @param bands
	 */
	public Mutator_Random(final RandomGenerator random, final int[] bands) {
		super(false);
		this._random = new Randomizer(random, bands);
	}

	/**
	 * @see net.sf.darwin.api.base.Mutator_#mutate(net.sf.darwin.core.Allele)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Allele<V> mutate(final Allele<V> allele) {
		if (getRandom().nextIndex() == 1) {
			final Locus<V> locus = allele.getLocus();
			return locus.getAlleleValues().toArray(new Allele[] {})[getRandom().nextInt(locus.size())];
		}
		return allele;
	}

	/**
	 * @param bands
	 */
	public void setBands(final int[] bands) {
		getRandom().setBands(bands);
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Chancy#setOdds(int)
	 */
	@Override
	public void setOdds(final int odds) {
		getRandom().setOdds(odds);
	}

	protected Randomizer getRandom() {
		return this._random;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8077305712908689069L;

	protected final Randomizer _random;

}
