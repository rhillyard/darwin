/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Nodal
 * Created on: Oct 3, 2011
 */

package net.sf.darwin.api.ui.model;

import java.util.Collection;

/**
 * @author rhillya
 * 
 */
public interface Nodal extends Provisioner<Collection<Node>>, Dirty {

	/**
	 * @return the Node that represents this object in the hierarchy
	 */
	public abstract Node getNode();

}
