/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: CalendarEvolution.java
 * Created on Aug 3, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.base;

import java.util.Calendar;

import net.sf.darwin.core.EvolutionException;

/**
 * @author Robin Hillyard
 * 
 */
public interface CalendarEvolution {

	/**
	 * @return the current time for this evolution, based on the number of ticks
	 *         and the speed of each tick.
	 * @throws EvolutionException
	 */
	public abstract Calendar getTime() throws EvolutionException;

	/**
	 * @param when
	 * @param event
	 */
	public abstract void scheduleEvent(final Calendar when, final Runnable event);

}