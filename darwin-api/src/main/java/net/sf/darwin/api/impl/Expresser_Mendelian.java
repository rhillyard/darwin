/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Expresser_Mendelian.java
 * Created on Apr 15, 2009
 * @version $Revision: 1.17 $
 */

package net.sf.darwin.api.impl;

import java.util.Collection;

import net.sf.darwin.api.base.Expresser_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Trait;

/**
 * This class implements {@link Expresser} and models a simple Mendelian gene
 * expression. By simple, we mean that there is a <b>one-to-one</b>
 * correspondence between genes and their resulting traits. Genes and traits may
 * be dominant or recessive.
 * 
 * For this simple implementation of {@link Expresser}, there must be exactly
 * two alleles in the locus of any gene to be expressed AND there must be
 * exactly two variants in the character. Furthermore, the genes must be passed
 * in to the express() method, one at a time.
 * 
 * For more complex expression models, it is necessary to extend
 * {@link Expresser_}, although it is also possible to extend this class.
 * 
 * This type is named, obviously, for <a
 * href="http://en.wikipedia.org/wiki/Mendel">Gregor Mendel</a>, the father of
 * genetics.
 * 
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            expresser
 */
@Lifecycle(permanent = true)
public class Expresser_Mendelian<P> extends Expresser_<P> {

	/**
	 * @param character
	 *            the phenotypic character
	 * @param domAllele
	 *            the key of the dominant allele
	 * @param domVariant
	 *            the key of the dominant variant
	 * 
	 */
	public Expresser_Mendelian(final Pharacter<P> character, final String domAllele, final String domVariant) {
		super(character, Boolean.TRUE);
		this._dominantAllele = domAllele;
		this._dominantVariant = domVariant;
		if (domVariant == null || domAllele == null)
			throw new DarwinException("Expresser_Mendelian: domAllele or domVariant null"); //$NON-NLS-1$
	}

	/**
	 * Express (one) gene passed in [Note this class cannot handle calls to this
	 * method with multiple genes].
	 * 
	 * @param genes
	 *            the gene locus which we are trying to express.
	 * @return the expressed trait
	 * 
	 * @see net.sf.darwin.core.Expresser#express(Colony, Gene[])
	 */
	@Override
	public <G> Trait<P> express(final Gene<G>... genes) {
		if (genes.length == 1) {
			final Gene<G> gene = genes[0];
			if (gene.getPloidy() == Ploidy.DIPLOID)
				return expressDiploid(gene);
			else if (gene.getPloidy() == Ploidy.HAPLOID)
				return expressHaploid(gene);
			else
				throw new DarwinException(S_METHOD_EXPRESS + "invalid gene ploidy: " + gene.getPloidy()); //$NON-NLS-1$
		}
		throw new DarwinException(S_METHOD_EXPRESS + "expects only one gene parameter"); //$NON-NLS-1$
	}

	/**
	 * @param gene
	 * @return the trait expressed by the gene.
	 */
	protected <G> Trait<P> expressDiploid(final Gene<G> gene) {
		if (this._character.getVariants().size() != 2)
			throw new DarwinException(S_METHOD_EXPRESS + "this class handles only characters with exactly two variants"); //$NON-NLS-1$

		if (this._recessiveVariant == null) {
			final Collection<String> keys = this._character.getVariantKeys();
			for (final String key : keys) {
				if (!key.equals(this._dominantVariant))
					this._recessiveVariant = key;
			}
			assert this._recessiveVariant != null : "Logic error in Expresser_Mendelian"; //$NON-NLS-1$
		}

		final int nAlleles = gene.getAlleleCount();

		if (nAlleles == 2)
			return createTrait(getVariant(gene.getAlleleKey(0), gene.getAlleleKey(1), this._dominantAllele,
					this._dominantVariant, this._recessiveVariant));
		throw new DarwinException(S_METHOD_EXPRESS
				+ "this simple type does not handle loci with unique or multiple alleles: " + nAlleles); //$NON-NLS-1$
	}

	/**
	 * @param gene
	 *            XXX
	 * @return if all is OK, then we return a trait but this particular
	 *         implementation always throws...
	 * @exception DarwinException
	 * 
	 *                TEST
	 */
	protected <G> Trait<P> expressHaploid(final Gene<G> gene) {
		throw new DarwinException(S_METHOD_EXPRESS + "unsupported gene (haploid)"); //$NON-NLS-1$
	}

	/**
	 * TODO check this out -- non-standard code.
	 */
	private String _recessiveVariant;

	private static final String S_METHOD_EXPRESS = "Expresser_Mendelian.express(): "; //$NON-NLS-1$

	protected final String _dominantAllele;

	protected final String _dominantVariant;

	/**
	 * This is the heart of the simple Mendelian gene expression. The two
	 * alleles' keys (a and b) are passed in [order is irrelevant]. The variant
	 * index returned is determined as follows:<br>
	 * if either a or b is the dominantAllele, then return the dominant variant;
	 * else if both a and b are the recessiveAllele, then return the recessive
	 * variant; otherwise there is a logic problem.
	 * 
	 * @param a
	 * @param b
	 * @param dominantAllele
	 * @param dominantVariant
	 * @param recessiveVariant
	 * @return the variant index as described above.
	 */
	private static String getVariant(final String a, final String b, final String dominantAllele, final String dominantVariant,
			final String recessiveVariant) {
		if (a.equals(dominantAllele) || b.equals(dominantAllele))
			return dominantVariant;
		return recessiveVariant;
	}

}
