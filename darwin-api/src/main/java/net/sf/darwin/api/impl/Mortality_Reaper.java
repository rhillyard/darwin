/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 * Created on Dec 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Mortality_;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Mortality;
import net.sf.tostring0.ToString;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * This default implementation of {@link Mortality} provides a basic and
 * reasonable calculation of mortality. The infant mortality can be specified
 * through the constructor (it defaults to 20%).
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.24 $
 */
@ToString(allBeanMethods = false)
@Lifecycle(permanent = true)
public final class Mortality_Reaper extends Mortality_ {

	/**
	 * Secondary constructor using <code>random</code> for random numbers and
	 * invoking the constructor
	 * {@link #Mortality_Reaper(RandomGenerator, double)}.
	 * 
	 * @param random
	 * 
	 */
	public Mortality_Reaper(final RandomGenerator random) {
		this(random, INFANT_MORTALITY_DEFAULT);
	}

	/**
	 * Secondary constructor using <code>random</code> for random numbers and
	 * invoking the constructor
	 * {@link #Mortality_Reaper(RandomGenerator, Number, Number)}.
	 * 
	 * @param random
	 * @param infantMortality
	 * 
	 */
	public Mortality_Reaper(final RandomGenerator random, final double infantMortality) {
		this(random, new Double(infantMortality), new Double(ACTUARIAL_FACTOR_DEFAULT));
	}

	/**
	 * Primary constructor.
	 * 
	 * @param random
	 *            the random number generator to use
	 * @param infantMortality
	 *            this is the probability of dying in the first year.
	 * @param actuarialFactor
	 *            this is the factor by which the <i>probability</i> of dying
	 *            increases annually.
	 * 
	 * 
	 */
	public Mortality_Reaper(final RandomGenerator random, final Number infantMortality, final Number actuarialFactor) {
		super(random);
		this._infantMortality = infantMortality.doubleValue();
		this._actuarialFactor = actuarialFactor.doubleValue();
	}

	/**
	 * 
	 * @param age
	 * @return if age==0 then <code>1 - infantMortality</code><br>
	 *         else if age==1 then <code>1</code> <br>
	 *         else <code>e ^ ((1-age) * actuarialFactor</code>
	 */
	@Override
	protected double calculateAgeViabilityFactor(final int age) {
		return age == 0 ? (1 - this._infantMortality) : Math.exp((1 - age) * this._actuarialFactor);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2007415710891833706L;

	/**
	 * 0.2
	 */
	public static final double INFANT_MORTALITY_DEFAULT = 0.2;

	/**
	 * 0.1
	 */
	private static final double ACTUARIAL_FACTOR_DEFAULT = 0.1;

	@ToString
	private final double _actuarialFactor;

	@ToString
	private final double _infantMortality;

}
