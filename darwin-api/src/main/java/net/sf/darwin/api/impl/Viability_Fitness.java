package net.sf.darwin.api.impl;

import java.text.MessageFormat;

import net.sf.darwin.api.base.Colony_;
import net.sf.darwin.api.base.Viability_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Stats;
import net.sf.darwin.core.Viability;

import org.apache.commons.logging.Log;

/**
 * This class implements the {@link Viability} interface for the purpose of
 * cycling through the Organisms of a Colony and determining which to kill off.
 * 
 * TODO need to parameterize with E, P, G ?
 * 
 * @author Robin Hillyard
 */
public final class Viability_Fitness extends Viability_ {
	/**
	 * @param colony
	 *            XXX
	 * @param log
	 *            XXX
	 * @param stats
	 *            XXX
	 * @param neonatesOnly
	 *            XXX
	 */
	public Viability_Fitness(final Colony colony, final Log log, final Stats stats, final boolean neonatesOnly) {
		super(stats);
		this._colony = colony;
		this._log = log;
		this._neonatesOnly = neonatesOnly;
	}

	@Override
	public boolean resetViability(final Organism organism) throws FitnessException {

		if (isNeonatesOnly() && organism.getAge() > 0)
			return false;

		final Number fitness = organism.getFitness(getColony().getPopulation().getTaxon().getPhenome().getFitnessEngine());

		double fit = 0.;

		if (fitness != null)
			fit = fitness.doubleValue();
		else
			getLog().warn("fitness is null for organism " + organism); //$NON-NLS-1$

		final boolean mortal = organism.isMortal(fit);

		// if not viable, prepare it for removal
		organism.setViable(!mortal);

		if (getStats() != null)
			getStats().update(mortal, fit, organism.getAge());

		// this method gets called a lot so we guard this block of
		// code
		if (getLog().isTraceEnabled()) {
			final String genome = organism.getGenome().toString();
			final String survivor = organism.isViable() ? "survivor" : "non-survivor"; //$NON-NLS-1$ //$NON-NLS-2$
			final String message = MessageFormat.format("{0}{1}{2} with fitness: {3} and genome: {4}", survivor, Colony_.S_COLON, //$NON-NLS-1$
					getColony().getIdentifier(), fitness, genome);
			getLog().trace(message);
		}
		return true;
	}

	/**
	 * @return the colony
	 */
	private Colony getColony() {
		return this._colony;
	}

	/**
	 * @return the log
	 */
	private Log getLog() {
		return this._log;
	}

	/**
	 * @return the neonatesOnly
	 */
	private boolean isNeonatesOnly() {
		return this._neonatesOnly;
	}

	/**
	 * 
	 */
	private final Colony _colony;

	private final Log _log;

	private final boolean _neonatesOnly;
}