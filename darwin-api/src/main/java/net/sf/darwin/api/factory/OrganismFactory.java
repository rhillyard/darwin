/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OrganismFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.api.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sf.darwin.api.impl.Nuclear_Zygote;
import net.sf.darwin.api.impl.Organism_Asexual;
import net.sf.darwin.api.impl.Organism_Sexual;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Nuclear;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Population;

/**
 * Factory Class to create Organism objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
public final class OrganismFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private OrganismFactory() {
		super();
	}

	private static final String S_FCTRY_MAKE_ORGANISM = "OrganismFactory.makeOrganism(): "; //$NON-NLS-1$

	/**
	 * @param clazz
	 *            the class of Organism to instantiate, which must have a
	 *            constructor of form Organism(Nuclear,Population,String).
	 * @param nuclear
	 *            the genetic information for the new organism
	 * @param population
	 *            the population to which the new organism will belong
	 * @param identifier
	 *            an identifier for the new organism
	 * @return a newly constructed {@link Organism}.
	 * 
	 *         TEST
	 */
	public static <E, P, G> Organism<E, P, G> makeOrganism(final Class<? extends Organism<E, P, G>> clazz,
			final Nuclear<P, G> nuclear, final Population<E, P, G> population, final String identifier) {
		try {
			final Constructor<? extends Organism<E, P, G>> constructor = clazz.getConstructor(new Class<?>[] { Nuclear.class,
					Population.class, String.class });
			return constructor.newInstance(new Object[] { nuclear, population, identifier });
		} catch (final NoSuchMethodException e) {
			throw new DarwinException(S_FCTRY_MAKE_ORGANISM + "no suitable constructor for " + clazz, e); //$NON-NLS-1$
		} catch (final InstantiationException e) {
			throw new DarwinException(S_FCTRY_MAKE_ORGANISM + "cannot instantiation " + clazz, e); //$NON-NLS-1$
		} catch (final IllegalAccessException e) {
			throw new DarwinException(S_FCTRY_MAKE_ORGANISM + "illegal access to " + clazz, e); //$NON-NLS-1$
		} catch (final InvocationTargetException e) {
			throw new DarwinException(
					S_FCTRY_MAKE_ORGANISM + "constructor for " + clazz + " threw exception", e.getTargetException()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Factory method for a new Organism.
	 * 
	 * @param nuclear
	 *            the genetic information for the new organism
	 * @param colony
	 *            the population to which the new organism will belong
	 * @param identifier
	 *            an identifier for the new organism
	 * @return either an {@link Organism_Sexual} or an {@link Organism_Asexual},
	 *         depending on whether nuclear is a {@link Nuclear_Zygote} or not.
	 */
	public static <E, U, W> Organism<E, U, W> makeOrganism(final Nuclear<U, W> nuclear, final Colony<E, U, W> colony,
			final String identifier) {
		// TODO consider doing this via polymorphism
		if (nuclear instanceof Nuclear_Zygote)
			return new Organism_Sexual<>(identifier, colony, nuclear);
		return new Organism_Asexual<>(identifier, colony, nuclear);
	}

}
