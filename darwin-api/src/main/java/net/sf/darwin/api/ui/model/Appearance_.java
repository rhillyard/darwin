/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Appearance_
 * Created on: Dec 20, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public abstract class Appearance_ implements Appearance {

	/**
	 * 
	 */
	public Appearance_() {
		super();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Appearance#getContentString()
	 */
	@Override
	public String getContentString() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Appearance#getHoverString()
	 */
	@Override
	public String getHoverString() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Appearance#getImageName()
	 */
	@Override
	public String getImageName() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	@SuppressWarnings("nls")
	public String toString() {
		return "Appearance [contentString=" + this.getContentString() + ", imageName=" + this.getImageName() + ", hoverString="
				+ this.getHoverString() + "]";
	}

}
