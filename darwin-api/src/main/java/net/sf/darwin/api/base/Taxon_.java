/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Taxon_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.impl.PhenotypeCache_Standard;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Fecundity;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Mortality;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenome;
import net.sf.darwin.core.PhenotypeCache;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Registry;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.Theological;
import net.sf.darwin.core.VisualizableListener;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class implementing Taxon interface.
 * 
 * @author Robin Hillyard
 * 
 */
@ToString(allBeanMethods = false)
public abstract class Taxon_<E, P, G> extends Evolvable_ implements Taxon<E, P, G> {

	/**
	 * Constructor to create a Taxon with no populations.
	 * 
	 * @param identifier
	 *            the identifier for this new system.
	 * @param realm
	 *            the realm in which this system can be found.
	 * @param genomic
	 *            the genomic object on which the genomes of all organisms in
	 *            all populations of this new system will be based.
	 * @param phenome
	 *            the phenome which all organisms of all populations of this new
	 *            system will exhibit.
	 * @param censusTaker
	 *            an implementer of {@link Census} for the purpose of censusing
	 *            populations of the new system.
	 * @param mortality
	 *            an implementer of {@link Mortality} for the purpose of
	 *            calculating mortalities of organisms in populations of this
	 *            new system.
	 * @param chooser
	 *            the mate chooser.
	 * @param fecundity
	 *            the fecundity determiner.
	 */
	protected Taxon_(final String identifier, final Realm realm, final Genomic<P, G> genomic, final Phenome<P> phenome,
			final Census censusTaker, final Mortality mortality, final MateChoice chooser, final Fecundity fecundity) {
		super(identifier);
		this._realm = realm;
		this._populations = new AuditableList<>(identifier);
		this._genomic = genomic;
		this._phenome = phenome;
		this._censusTaker = censusTaker;
		this._mortality = mortality;
		this._chooser = chooser;
		this._fecundity = fecundity;
		this._visualizableListeners = new ArrayList<>();
		this._properties = new HashMap<>();
	}

	/**
	 * Add the given population to this Taxon. There are two side-effects: this
	 * is passed to the population's setSystem() method; and the index of the
	 * population in the list of _populations is passed to the population's
	 * setIndex() method.
	 * 
	 * @see net.sf.darwin.core.Taxon#addPopulation(net.sf.darwin.core.Population)
	 */
	@Override
	public synchronized int addPopulation(final Population<E, P, G> population) {
		population.setTaxon(this);
		final boolean ok = populations().add(population);
		final int indexOf = populations().indexOf(population);
		population.setIndex(indexOf);
		return ok ? indexOf : -1;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Taxon#addVisualizableListener(net.sf.darwin.core.VisualizableListener)
	 */
	@Override
	public void addVisualizableListener(final VisualizableListener listener) {
		getVisualizableListeners().add(listener);
	}

	/**
	 * 
	 * @param census
	 * @param prefix
	 *            for error message only
	 * @see net.sf.darwin.core.Censusible#censusMe(Census, java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object prefix) {
		return true;
	}

	/**
	 * For each population, call {@link Population#cullMembers()}, passing value
	 * of organisms.
	 * 
	 * @see net.sf.darwin.core.Taxon#seedMembers()
	 */
	@Override
	public void cullMembers() {
		for (final Theological evolvable : getPopulations()) {
			evolvable.cullMembers();
		}
	}

	/**
	 * XXX this is currently only ever used by tests.
	 * 
	 * @throws IOException
	 * 
	 * @see net.sf.darwin.core.Taxon#doCensus()
	 */
	@Override
	public void doCensus() throws IOException {
		final Census censusTaker = getCensusTaker();
		censusTaker.census(this, 2, "system"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.api.base.Evolvable_#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Taxon_<?, ?, ?> other = (Taxon_<?, ?, ?>) obj;
		if (this._censusTaker == null) {
			if (other._censusTaker != null)
				return false;
		} else if (!this._censusTaker.equals(other._censusTaker))
			return false;
		if (this._chooser == null) {
			if (other._chooser != null)
				return false;
		} else if (!this._chooser.equals(other._chooser))
			return false;
		if (this._fecundity == null) {
			if (other._fecundity != null)
				return false;
		} else if (!this._fecundity.equals(other._fecundity))
			return false;
		if (this._genomic == null) {
			if (other._genomic != null)
				return false;
		} else if (!this._genomic.equals(other._genomic))
			return false;
		if (this._mortality == null) {
			if (other._mortality != null)
				return false;
		} else if (!this._mortality.equals(other._mortality))
			return false;
		if (this._phenome == null) {
			if (other._phenome != null)
				return false;
		} else if (!this._phenome.equals(other._phenome))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return getPopulations();
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Taxon#getCensusTaker()
	 */
	@Override
	public Census getCensusTaker() {
		return this._censusTaker;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Taxon#getChooser()
	 */
	@Override
	public MateChoice getChooser() {
		return this._chooser;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Taxon#getFecundity()
	 */
	@Override
	public Fecundity getFecundity() {
		return this._fecundity;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Taxon#getGenomic()
	 */
	@Override
	public Genomic<P, G> getGenomic() {
		return this._genomic;
	}

	/**
	 * @see net.sf.darwin.api.base.Evolvable_#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return populations().getIdentifier();
	}

	/**
	 * @return {@link #_mortality}
	 * @see net.sf.darwin.core.Taxon#getMortality()
	 */
	@Override
	public Mortality getMortality() {
		return this._mortality;
	}

	/**
	 * XXX
	 * 
	 * @see net.sf.darwin.core.Taxon#getPhenome()
	 */
	@Override
	public Phenome<P> getPhenome() {
		return this._phenome;
	}

	/**
	 * @see net.sf.darwin.core.HasPhenotypeCache#getPhenotypeCache()
	 */
	@Override
	public PhenotypeCache<P> getPhenotypeCache() {
		return this._phenotypeCache;
	}

	/**
	 * TODO this method should be eliminated.
	 * 
	 * @param index
	 * @return the indexth population among those which have organisms of this
	 *         taxon.
	 * 
	 */
	public synchronized Population<E, P, G> getPopulation(final int index) {
		return populations().get(index);
	}

	/**
	 * TODO this method should get the apporpriate populations from this taxon's
	 * Realm.
	 * 
	 * @return a collection of populations which have organisms of this taxon.
	 */
	public synchronized Collection<Population<E, P, G>> getPopulations() {
		return populations();
	}

	/**
	 * Bean method.
	 * 
	 * @return {@link #_properties}, the map of the arbitrary
	 *         application-specific properties of this system.
	 */
	public Map<String, Object> getProperties() {
		return this._properties;
	}

	/**
	 * Method to get one of the arbitrary application-specific properties of
	 * this system.
	 * 
	 * @param key
	 *            the key to the property.
	 * @return the value of the property.
	 * @see net.sf.darwin.core.Taxon#getProperty(java.lang.Object)
	 */
	@Override
	public Object getProperty(final Object key) {
		return getProperties().get(key);
	}

	/**
	 * @see net.sf.darwin.core.Taxon#getRealm()
	 */
	@Override
	public Realm getRealm() {
		return this._realm;
	}

	/**
	 * @return seedPopulation
	 */
	@Override
	public int getSeedPopulation() {
		return this.seedPopulation;

	}

	/**
	 * @return {@link #_visualizableListeners}
	 * @see net.sf.darwin.core.Taxon#getVisualizableListeners()
	 */
	@Override
	public Collection<VisualizableListener> getVisualizableListeners() {
		return this._visualizableListeners;
	}

	/**
	 * @see net.sf.darwin.api.base.Evolvable_#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._censusTaker == null) ? 0 : this._censusTaker.hashCode());
		result = prime * result + ((this._chooser == null) ? 0 : this._chooser.hashCode());
		result = prime * result + ((this._fecundity == null) ? 0 : this._fecundity.hashCode());
		result = prime * result + ((this._genomic == null) ? 0 : this._genomic.hashCode());
		result = prime * result + ((this._mortality == null) ? 0 : this._mortality.hashCode());
		result = prime * result + ((this._phenome == null) ? 0 : this._phenome.hashCode());
		return result;
	}

	/**
	 * First we check the super-method. If that determines that we are finished,
	 * then we return true; Otherwise, if any population is not finished we
	 * return false; Finally (if all populations are finished), we return true;
	 * 
	 * @see net.sf.darwin.api.base.Evolvable_#isFinished()
	 */
	@Override
	public boolean isFinished() {
		final boolean finished = super.isFinished();
		if (finished)
			return true;
		for (final Population<E, P, G> population : getPopulations()) {
			if (!population.isFinished())
				return false;
		}
		return true;
	}

	/**
	 * TODO consider combining this with {@link #isFinished()}.
	 * 
	 * @see net.sf.darwin.core.Taxon#isLastGeneration()
	 */
	@Override
	public boolean isLastGeneration() {
		return getGeneration() == getMaxGenerations() - 1;
	}

	/**
	 * For each population in this system, call nextGeneration().
	 * 
	 * @return true if all the populations in this system are healthy.
	 * @see net.sf.darwin.core.Evolvable#nextGeneration()
	 */
	@SuppressWarnings("nls")
	@Override
	public boolean nextGeneration() {
		super.nextGeneration(); // TODO consider retaining result
		final boolean finished = isFinished();
		if (finished)
			LOG.info("Taxon.nextGeneration(): finished"); //$NON-NLS-1$ 

		boolean result = !finished;

		final Census censusTaker = getCensusTaker();
		for (final Population<E, P, G> population : getPopulations()) {
			if (population.isFinished())
				continue;

			if (!result) {
				population.wrapUp("finished"); //$NON-NLS-1$
				continue;
			}

			try {
				final boolean ok = population.nextGeneration();
				if (population.isFinished())
					continue;
				censusTaker.present(population.getIdentifier() + ": " + (ok ? "OK" : "not OK"), "Generation " + getGeneration()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				result = ok;
			} catch (final DarwinException e) {
				System.err.println("Taxon.nextGeneration: exception: " + e.getLocalizedMessage());
				LOG.error("Taxon.nextGeneration: exception: ", e); //$NON-NLS-1$
				break;
			}
		}

		try {
			// TODO consider doing this via polymorphism
			if (censusTaker instanceof Sink)
				((Sink) censusTaker).flush();
		} catch (final IOException e) {
			LOG.warn("Taxon.nextGeneration: problem with flush", e); //$NON-NLS-1$
		}

		return result;
	}

	/**
	 * @see net.sf.darwin.core.Registry#registerBirths(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerBirths(final Colony<E, P, G> colony, final Collection<Organism<E, P, G>> neonates) {
		if (getRegistry() != null)
			getRegistry().registerBirths(colony, neonates);
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Registry#registerDeaths(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerDeaths(final Colony<E, P, G> colony, final Collection<Organism<E, P, G>> deaths) {
		if (getRegistry() != null)
			getRegistry().registerDeaths(colony, deaths);
	}

	/**
	 * @see net.sf.darwin.core.Registry#registerMarriages(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerMarriages(final Colony<E, P, G> colony, final Collection<Mating> marriages) {
		if (getRegistry() != null)
			getRegistry().registerMarriages(colony, marriages);
	}

	/**
	 * For each population, call {@link Population#seedMembers()}, passing value
	 * of organisms.
	 * 
	 * @see net.sf.darwin.core.Taxon#seedMembers()
	 */
	@Override
	public void seedMembers() {
		for (final Theological evolvable : getPopulations()) {
			evolvable.seedMembers();
		}
	}

	/**
	 * This is the preferred way to set populations, rather than using the
	 * {@link #addPopulation(Population)} method. Any previous populations are
	 * lost after invoking this method.
	 * 
	 * @param populations
	 *            a Collection of Population objects.
	 */
	@Override
	public void setPopulations(final Collection<Population<E, P, G>> populations) {
		populations().clear();
		populations().addAll(populations);
		for (final Population<E, P, G> population : populations) {
			population.setTaxon(this);
		}
	}

	/**
	 * Bean method.
	 * 
	 * @param properties
	 *            the map of the arbitrary application-specific properties of
	 *            this system.
	 */
	public void setProperties(final Map<String, Object> properties) {
		this._properties = properties;
	}

	/**
	 * method to set one of the arbitrary application-specific properties of
	 * this system.
	 * 
	 * @param key
	 * @param value
	 * @return the result of invoking {@link Map#put(Object, Object)}.
	 * 
	 * @see net.sf.darwin.core.Taxon#setProperty(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public Object setProperty(final String key, final Object value) {
		return getProperties().put(key, value);
	}

	/**
	 * Invoked via bean operations (e.g dependency injection)
	 * 
	 * @param registry
	 */
	public void setRegistry(final Registry<E, P, G> registry) {
		this.registry = registry;
	}

	/**
	 * @param seedPopulation
	 */
	public void setSeedPopulation(final int seedPopulation) {
		this.seedPopulation = seedPopulation;
	}

	/**
	 * This is called, but not addListener()
	 * 
	 * @see net.sf.darwin.core.Taxon#setVisualizableListeners(java.util.Collection)
	 */
	@Override
	public void setVisualizableListeners(final Collection<VisualizableListener> visualizableListeners) {
		getVisualizableListeners().clear();
		getVisualizableListeners().addAll(visualizableListeners);
	}

	/**
	 * @return {@link #registry}
	 */
	private Registry<E, P, G> getRegistry() {
		return this.registry;
	}

	/**
	 * @return the populations
	 */
	private AuditableList<Population<E, P, G>> populations() {
		return this._populations;
	}

	private static final long serialVersionUID = -1497792460828682148L;

	private final Realm _realm;

	/**
	 * This is an arbitrary default. It's expected that all systems will set a
	 * default seed population value.
	 */
	private transient int seedPopulation = 25;

	/**
	 * Collection of population listeners for all of the populations belonging
	 * to this system.
	 * 
	 */
	private transient final Collection<VisualizableListener> _visualizableListeners;

	/**
	 * A set of populations, possibly geographically or ecologically separated,
	 * which form this system.
	 */
	private transient final AuditableList<Population<E, P, G>> _populations;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Taxon_.class);

	/**
	 * The census taker which implements the Census in order to make a census of
	 * this Taxon.
	 */
	protected final transient Census _censusTaker;

	/**
	 * The Genomic of this Taxon (fixed).
	 */
	protected final Genomic<P, G> _genomic;

	/**
	 * The "Grim Reaper" which implements the Mortality to determine when
	 * organisms in this system will die.
	 */
	protected final Mortality _mortality;

	/**
	 * The cache for saving phenotypes which have already been expressed.
	 */
	private transient final PhenotypeCache<P> _phenotypeCache = new PhenotypeCache_Standard<>();

	/**
	 * The Chooser which implements the MateChoice to determine how desirable a
	 * male organism appears to a female organism.
	 */
	private final MateChoice _chooser;

	/**
	 * The implementation of {@link Fecundity}.
	 */
	private final Fecundity _fecundity;

	/**
	 * The Phenome of this Taxon (fixed).
	 */
	protected final Phenome<P> _phenome;

	private transient Registry<E, P, G> registry;

	/**
	 * A set of arbitrary application-specific properties for this system.
	 */
	private transient Map<String, Object> _properties;

}
