/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Allele_Dominance.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Allele_Binary;
import net.sf.darwin.api.factory.AlleleFactory;
import net.sf.darwin.api.util.StringUtilities;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Dominance;
import net.sf.darwin.core.Lifecycle;
import net.sf.tostring0.ToString;

/**
 * A simple implementation of {@link Allele} which models two competing alleles:
 * one dominant and one recessive.
 * 
 * The base pairs that define each allele are set as a property.
 * 
 * @author Robin Hillyard
 * 
 */
@ToString(allBeanMethods = false)
@Lifecycle(permanent = true)
public final class Allele_Dominance extends Allele_Binary implements Dominance {

	/**
	 * Primary constructor for Allele_Dominance. The identifier is forced into a
	 * suitable form by calling
	 * {@link StringUtilities#capitalizeInitial(String, boolean)} such that the
	 * initial letter is either capitalized (if dominant) or not. The other
	 * letters are uncapitalized.
	 * 
	 * Normally, such alleles are constructed using
	 * {@link AlleleFactory#makeDominanceAllele(String, boolean)} method.
	 * 
	 * @param identifier
	 *            the identifier for this pair of alleles (case and number of
	 *            characters are unconstrained).
	 * @param dominant
	 *            any value.
	 */
	public Allele_Dominance(final String identifier, final boolean dominant) {
		this(identifier, dominant, null);
	}

	/**
	 * CONSIDER whether this is a proper modeling of dominance. In biology,
	 * dominance is a consequence of gene <i>expression</i>, not the gene
	 * itself. <br/>
	 * Primary constructor for Allele_Dominance. The identifier is forced into a
	 * suitable form by calling
	 * {@link StringUtilities#capitalizeInitial(String, boolean)} such that the
	 * initial letter is either capitalized (if dominant) or not. The other
	 * letters are uncapitalized.
	 * 
	 * Normally, such alleles are constructed using
	 * {@link AlleleFactory#makeDominanceAllele(String, boolean)} method.
	 * 
	 * @param identifier
	 *            the identifier for this pair of alleles (case and number of
	 *            characters are unconstrained).
	 * @param dominant
	 *            true for the dominant allele, false for the recessive allele.
	 * @param bases
	 *            explicit bases for this type of allele (or null).
	 */
	public Allele_Dominance(final String identifier, final boolean dominant, final String[] bases) {
		super(StringUtilities.capitalizeInitial(identifier, dominant), dominant, bases);
	}

	/**
	 * @see net.sf.darwin.core.Dominance#isDominant()
	 */
	@Override
	public boolean isDominant() {
		return getValueAsBoolean();
	}

	private static final long serialVersionUID = -6935097817258924298L;

}
