/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.darwin.core.Allele;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.FrequencyMap;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Genotype;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * This abstract class provides base definitions for implementers of
 * {@link Genome}, the genetic information contained in an Organism belonging to
 * a Taxon. A Genome may contain any number of Genes, though in practice the
 * number is determined by the Genomic which defines this Genome. The structure
 * of Genome is approximately parallel to that of {@link Genomic} .
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.50 $
 * 
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Genotype
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Genotype
 */
@SuppressWarnings("serial")
@ToString(detail = 1, allBeanMethods = false)
public abstract class Genotype_<P, G> extends AuditableList<Gene<G>> implements Genotype<P, G>, Cloneable {

	/**
	 * Constructor which creates a Genome instance with the given
	 * <code>"ploidy"</code>.
	 * 
	 * @param ploidy
	 *            the ploidy (haploid, diploid) of the genome.
	 */
	protected Genotype_(final int ploidy) {
		super(null);
		this._ploidy = ploidy;
	}

	/**
	 * @param gene
	 *            the gene to add
	 * @return true.
	 * @see net.sf.darwin.api.base.AuditableList#add(net.sf.tostring0.IToString)
	 */
	@Override
	public synchronized boolean addGene(final Gene<G> gene) {
		addGene(size(), gene);
		return true;
	}

	/**
	 * @param locus
	 * @param gene
	 * 
	 * @see net.sf.darwin.core.Genome#addGene(int, net.sf.darwin.core.Gene)
	 */
	@Override
	public void addGene(final int locus, final Gene<G> gene) {
		super.add(locus, gene);
	}

	/**
	 * Iteratively call {@link #addGene(Gene)} for each of the genes provided.
	 * 
	 * @param genes
	 * 
	 * @see net.sf.darwin.core.Genome#addGenes(net.sf.darwin.core.Genes)
	 */
	@Override
	public void addGenes(final Genes<G> genes) {
		for (int i = 0; i < genes.size(); i++) {
			addGene(genes.getGene(i));
		}
	}

	/**
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present(getIdentifier(), context);
		return true;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		try {
			final Object clone = super.clone();
			if (getList() instanceof ArrayList)
				// XXX check this out
				setList(((ArrayList<Gene<G>>) ((ArrayList<Gene<G>>) getList()).clone()));

			// XXX try this instead...
			// ((Genome_)clone)._list = new ArrayList<Gene>(_list);

			// take
			// care:
			// unchecked
			// cast?
			// Nothing else to do -- no mutable or non-primitive fields to
			// deep-copy.
			return clone;
		} catch (final CloneNotSupportedException e) {
			// This cannot happen provided that type implements Cloneable
			return null;
		}
	}

	/**
	 * @param alleleFrequencies
	 */
	@Override
	public void doCensus(final FrequencyMap<Allele<G>> alleleFrequencies) {
		for (int i = 0; i < size(); i++) {
			final Gene<G> gene = getGene(i);
			final Locus<?> locus = gene.getLocus();
			final String identifier = locus.getIdentifier();
			// TODO there should be a better way to do this.
			if (!identifier.equals(Locus.ID_SEX)) {
				alleleFrequencies.add(gene.getAllele(0));
				alleleFrequencies.add(gene.getAllele(1));
			}
		}
	}

	/**
	 * Determine equality based on the value of {@link #getBases()}. Ignore the
	 * super-class.
	 * 
	 * @see net.sf.darwin.api.base.AuditableList#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		final Genes<?> other = (Genes<?>) obj;
		// Note that we use equals here, not ==
		if (!getBases().equals(other.getBases()))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Basic#getBases()
	 */
	@Override
	@ToString(omit = true)
	public String getBases() {
		final StringBuilder result = new StringBuilder();
		for (final Gene<G> gene : getList())
			result.append(gene.getBases());
		final Gene<Boolean> sexGene = getSexGene();
		if (sexGene != null)
			result.append(sexGene.getBases());
		return result.toString();
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		final Collection<Censusible> result = new ArrayList<>();
		for (final Gene<G> gene : getList())
			result.add(gene);
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Genes#getGene(int)
	 */
	@Override
	public synchronized Gene<G> getGene(final int locus) {
		if (locus >= 0 && locus < size())
			return get(locus);
		else if (locus == size())
			return null; // special case for sex gene
		throw new DarwinException("Geontype_.getGene(int): gene locus out of bounds: " + locus); //$NON-NLS-1$
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Genes#getGenes()
	 */
	@Override
	@ToString(detail = 1, name = "genes")
	public List<Gene<G>> getGenes() {
		return super.getList();
	}

	/**
	 * @return the signature for this TraitMap
	 * @see net.sf.darwin.api.base.AuditableList#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getSignature();
	}

	/**
	 * The "ploidy" of this Genome. It may not match that of the Genomic to
	 * which the genes belong.
	 * 
	 * @see net.sf.darwin.core.Ploidy#getPloidy()
	 */
	@Override
	@ToString
	public int getPloidy() {
		return this._ploidy;
	}

	/**
	 * Currently, this method is always overridden but we leave it here for
	 * completeness.
	 * 
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Genes#getSexGene()
	 */
	@Override
	public Gene<Boolean> getSexGene() {
		return null;
	}

	/**
	 * @see net.sf.darwin.core.CacheSignature#getSignature()
	 */
	@Override
	@ToString(omit = true)
	public String getSignature() {
		return getBases();
	}

	/**
	 * return the hashcode of the bases. We ignore the super method.
	 * 
	 * @see net.sf.darwin.api.base.AuditableList#hashCode()
	 */
	@Override
	public int hashCode() {
		return getBases().hashCode();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.AuditableList#toArray(java.lang.Object[])
	 */
	@Override
	public <X> X[] toArray(final X[] a) {
		return getList().toArray(a);
	}

	/**
	 * TODO eliminate: use getGenes instead
	 * 
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.AuditableList#getList()
	 */
	@Override
	protected List<Gene<G>> getList() {
		return super.getList();
	}

	/**
	 * The "ploidy" (or "oid") is the genetic "form" of the this genome. It is
	 * the number of alleles that are carried at any locus by this genome. Note
	 * that an organism has its ploidy determined by its genome. However,
	 * gametes of that organism are always haploid. <br>
	 * For a haploid system, the Ode is 1; for a diploid system, the Ode is 2,
	 * etc.
	 */
	protected final int _ploidy;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Genotype_.class);

}