/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Best_Organism.java
 * Created on Dec 15, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.api.impl;

import java.util.Collection;

import net.sf.darwin.api.base.Best_;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.ProcessBest;
import net.sf.darwin.core.ValueException;

/**
 * This class keeps track of the best organism, where best is defined as having
 * the greatest fitness.
 * 
 * @author Robin Hillyard
 * 
 */
public final class Best_Organism<E, T, V> extends Best_<Organism<E, T, V>> {

	/**
	 * @param identifier
	 *            the id of the group which "owns" the best organism.
	 * @param generation
	 *            XXX
	 * 
	 */
	public Best_Organism(final String identifier, final int generation) {
		this(identifier, generation, 0);
	}

	/**
	 * @param identifier
	 *            the id of the group which "owns" the best organism.
	 * @param generation
	 *            the current generation (ignored if less than 0)
	 * @param candidates
	 * @param processor
	 * @throws ValueException
	 */
	public Best_Organism(final String identifier, final int generation, final Collection<Organism<E, T, V>> candidates,
			final ProcessBest<Organism<E, T, V>> processor) throws ValueException {
		super("organism from population " + identifier + (generation >= 0 ? " @ gen " + generation : ""), candidates, processor); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * @param identifier
	 *            the id of the group which "owns" the best organism.
	 * @param generation
	 *            XXX
	 * @param convergence
	 *            XXX
	 * 
	 */
	public Best_Organism(final String identifier, final int generation, final int convergence) {
		this(identifier, null, generation, convergence);
	}

	/**
	 * @param identifier
	 *            the id of the group which "owns" the best organism.
	 * @param generation
	 *            the current generation (ignored if less than 0)
	 * @param processor
	 * @param convergence
	 *            XXX
	 */
	public Best_Organism(final String identifier, final int generation, final ProcessBest<Organism<E, T, V>> processor,
			final int convergence) {
		this("organism from " + identifier + (generation >= 0 ? " @ gen " + generation : ""), null, processor, convergence); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * @param identifier
	 *            the id of the group which "owns" the best organism.
	 * @param best
	 * @param generation
	 *            the current generation (ignored if less than 0)
	 * @param convergence
	 *            XXX
	 */
	public Best_Organism(final String identifier, final Organism<E, T, V> best, final int generation, final int convergence) {
		super("organism from population " + identifier + (generation >= 0 ? " @ gen " + generation : ""), best, null, convergence); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * @param identifier
	 *            XXX
	 * @param best
	 * @param processor
	 * @param convergence
	 *            XXX
	 */
	public Best_Organism(final String identifier, final Organism<E, T, V> best, final ProcessBest<Organism<E, T, V>> processor,
			final int convergence) {
		super(identifier, best, processor, convergence);
	}

	/**
	 * @see com.rubecula.darwin.foundation.Best_#setConvergence(int)
	 */
	@Override
	public void setConvergence(final int convergence) {
		super.setConvergence(convergence);
	}

	/**
	 * @return the complement of the equals method applied to the genome of the
	 *         candidate with parameter: the genome of the best object.
	 * @see com.rubecula.darwin.foundation.Best_#isUnique(com.rubecula.darwin.foundation.ComparableValue)
	 */
	@Override
	protected boolean isUnique(final Organism<E, T, V> candidate) {
		final Organism<E, T, V> previousObject = getObject();
		if (previousObject == null)
			return true;
		return !candidate.getGenome().equals(previousObject.getGenome());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4223597206022060979L;

}
