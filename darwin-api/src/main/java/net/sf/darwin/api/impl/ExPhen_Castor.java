/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ExPhen_Castor.java
 * Created on Jun 30, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.ExPhen_;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExPhen;

/**
 * The default implementation of {@link ExPhen}, named in honor of the nature's
 * engineer, the Beaver (genus Castor) which creates some of the largest
 * extended phenotypes found in the animal kingdom, that is to say the lakes
 * formed by Beaver dams (we humans create even bigger ones, like the Great Wall
 * of China).
 * 
 * @author Robin Hillyard
 * 
 */
public final class ExPhen_Castor extends ExPhen_ {

	/**
	 * TEST
	 * 
	 * @param factor
	 * @param value
	 */
	public ExPhen_Castor(final EcoFactor factor, final Object value) {
		super(factor, value);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6977245471842798764L;

}
