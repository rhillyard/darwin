/**
 * Cryptobase Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Module: EvolutionHandle.java
 * Created on Apr 8, 2012
 */

package net.sf.darwin.api.base;

import java.io.Serializable;

/**
 * @author Robin
 * 
 */
public class EvolutionHandle implements Serializable {

	/**
	 * @param identifier
	 *            XXX
	 * 
	 */
	public EvolutionHandle(final String identifier) {
		super();
		this._identifier = identifier;
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EvolutionHandle <identifier=" + this._identifier + ">";
	}

	private static final long serialVersionUID = 2495784603371809606L;

	private final String _identifier;

}
