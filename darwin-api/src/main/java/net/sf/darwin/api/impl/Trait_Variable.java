/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Trait_Variable.java
 * Created on Jul 2, 2009
 * @version $Revision: 1.20 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Trait_;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Trait;

/**
 * Concrete implementation of {@link Trait} which is based on a variable value.
 * 
 * TODO we should not have our own {@link #_value} field here. We should be
 * using the superclass value field.
 * 
 * @author Robin Hillyard
 * 
 */
final public class Trait_Variable extends Trait_<Number> implements Quantifiable {

	/**
	 * @param character
	 * @param value
	 *            a double
	 */
	public Trait_Variable(final Pharacter<Number> character, final Number value) {
		super(character, value.toString());
		this._value = value;
	}

	/**
	 * @see net.sf.darwin.core.Quantifiable#doubleValue()
	 */
	@Override
	public double doubleValue() {
		return this._value.doubleValue();
	}

	/**
	 * @see net.sf.darwin.api.base.Trait_#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getCharacter().getIdentifier();
	}

	/**
	 * @return the value of this trait - sufficient to distinguish trait from
	 *         others.
	 * @see net.sf.darwin.api.base.Trait_#getValue()
	 */
	@Override
	public Object getKey() {
		return this._value;
	}

	/**
	 * @see net.sf.darwin.api.base.Trait_#getValue()
	 */
	@Override
	public Number getValue() {
		return this._value;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8801549710256211071L;

	private final Number _value;

}
