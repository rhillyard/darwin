/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Meiosis_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.25 $
 */
package net.sf.darwin.api.base;

import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.impl.Genome_Linear;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Ploidy;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.random.RandomGenerator;

/**
 * Abstract class which defines the base operations for an implementation of
 * {@link Meiosis}.
 * 
 * @param <G>
 *            the type of the genetic information which processed by this
 *            Meiosis implementer
 * 
 * @author Robin Hillyard
 */
@SuppressWarnings("serial")
@ToString(allBeanMethods = false)
abstract public class Meiosis_<G> extends Base implements Meiosis<G> {

	/**
	 * Protected constructor for an implementer of {@link Meiosis}
	 * 
	 * @param random
	 *            a random number source.
	 */
	protected Meiosis_(final RandomGenerator random) {
		super();
		this._random = random;
	}

	/**
	 * Choose an allele at random for the given locus.
	 * 
	 * @param gene
	 *            the gene on which we are doing the meiosis.
	 * @param index
	 *            if index >=0 then we use it, otherwise we generate a random
	 *            number between 0 and the gene's ploidy less one.
	 * 
	 * @return the key of the allele chosen.
	 */
	@Override
	public String doMeiosis(final Gene<G> gene, final int index) {
		return doMeiosis(gene, index, getRandom());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Meiosis#doMeiosis(net.sf.darwin.core.Genomic,
	 *      java.util.List, net.sf.darwin.core.Chromosome)
	 */
	@Override
	public <P> Genome<P, G> doMeiosis(final Genomic<P, G> genomic, final Genes<G> genes, final Chromosome<G> chromosome) {
		return doMeiosisChromosome(genomic, genes, chromosome, -1);
	}

	/**
	 * Return the class's simple name (since these are normally singleton
	 * instances we don't really need to distinguish them).
	 * 
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return getClass().getSimpleName();
	}

	/**
	 * TODO replicate this logic for crossover
	 * 
	 * @param genomic
	 * @param genes
	 * @param chromosome
	 * @param index
	 *            the index to use to choose the allele for a given gene (if < 0
	 *            then we use a random number)
	 * @return a newly constructed Genome
	 */
	protected <P> Genome<P, G> doMeiosisChromosome(final Genomic<P, G> genomic, final Genes<G> genes,
			final Chromosome<G> chromosome, final int index) {
		final Genome<P, G> result = new Genome_Linear<>(genomic, Ploidy.HAPLOID);
		for (int i = 0; i < genes.size(); i++) {
			final Gene<G> gene = genes.getGene(i);
			final Locus<G> locus = gene.getLocus();
			if (chromosome == null || locus.getChromosome().equals(chromosome))
				result.addGene(GeneFactory.makeHaploid(locus, doMeiosis(gene, index)));
		}
		if (result.isSexual() && result instanceof Genome_)
			try {
				final Locus<Boolean> locus = ((IsSexual) result.getGenomic()).getSexGenomic().getLocus(0);
				final Gene<Boolean> sexGene = GeneFactory.makeHaploid(locus, doMeiosis(genes.getSexGene(), -1, getRandom()));
				((Genome_<P, G>) result).setSexGene(sexGene);
			} catch (final GeneticsException e) {
				// This should never happen (famous last words)
				throw new RuntimeException("logic error", e); //$NON-NLS-1$
			}
		return result;
	}

	/**
	 * @return the random number generator
	 */
	@ToString
	protected RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Meiosis_.class);

	/**
	 * Random number source.
	 */
	final private RandomGenerator _random;

	/**
	 * @param gene
	 * @param index
	 * @param random
	 * @return
	 */
	private static <X> String doMeiosis(final Gene<X> gene, final int index, final RandomGenerator random) {
		final int ploidy = gene.getPloidy();
		final int which = index >= 0 ? index : random.nextInt(ploidy);
		return getAllele(gene, which);
	}

	/**
	 * @param gene
	 * @param which
	 * @return
	 */
	private static <X> String getAllele(final Gene<X> gene, final int which) {
		final String alleleKey = gene.getAlleleKey(which);
		if (LOG.isTraceEnabled())
			LOG.trace("Meiosis.getAllele(): " + alleleKey); //$NON-NLS-1$
		return alleleKey;
	}

}