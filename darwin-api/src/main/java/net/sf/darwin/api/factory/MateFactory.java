/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MateFactory.java
 * Created on Jul 17, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.factory;

import net.sf.darwin.api.base.Mate_;
import net.sf.darwin.api.impl.Mate_Candidate;
import net.sf.darwin.core.Mate;
import net.sf.darwin.core.Organism;

/**
 * @author Robin Hillyard
 * 
 */
public final class MateFactory {

	/**
	 * 
	 */
	public MateFactory() {
		super();
	}

	/**
	 * Factory method to create a new {@link Mate_} object based on the given
	 * <code>male</code> Organism and his <code>desirability</code>.
	 * 
	 * @param male
	 *            the male organism who will be our mate.
	 * @param desirability
	 *            the mate's desirability.
	 * @return a newly constructed Mate object based on the given male and
	 *         desirability
	 */
	public static Mate_ createMate(final Organism male, final double desirability) {
		return new Mate_Candidate(male, desirability);
	}

	/**
	 * @param minimumDesirability
	 *            the minimum acceptable desirability for a real {@link Mate_}.
	 * @return a null Mate used only for comparison purposes (every real
	 *         candidate {@link Mate_} with better than the minimum desirability
	 *         will be better than this one.
	 */
	public static Mate createNemo(final double minimumDesirability) {
		return createMate(null, minimumDesirability);
	}

}
