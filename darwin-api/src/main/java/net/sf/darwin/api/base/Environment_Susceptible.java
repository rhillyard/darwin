/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Environment_Susceptible.java
 * Created on Jan 20, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Best;
import net.sf.darwin.core.ComparableValue;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Realm;

/**
 * Abstract class for an {@link Environment} which is {@link Susceptible} to
 * extended-phenotype-invoked changes.
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
abstract public class Environment_Susceptible<E> extends Environment_<E> implements Susceptible {

	/**
	 * @param realm
	 */
	public Environment_Susceptible(final Realm realm) {
		super(realm);
	}

	/**
	 * @param identifier
	 * @param realm
	 */
	public Environment_Susceptible(final String identifier, final Realm realm) {
		super(identifier, realm);
	}

	/**
	 * @param identifier
	 * @param realm
	 * @param idealPopulation
	 */
	public Environment_Susceptible(final String identifier, final Realm realm, final long idealPopulation) {
		super(identifier, realm, idealPopulation);
	}

	/**
	 * @see com.rubecula.darwin.domain.environment.Susceptible#updateFromSource(com.rubecula.darwin.foundation.Best,
	 *      java.lang.Object)
	 */
	@Override
	public boolean updateFromSource(final Best<? extends ComparableValue> best, final Object source) {
		if (updateFromSource(source)) {
			best.reset();
			logUpdate();
			return true;
		}
		return false;
	}

	/**
	 * Method to log the fact that we caused an update
	 */
	protected abstract void logUpdate();

	/**
	 * @param source
	 * @return true if this Environment was updated
	 */
	protected abstract boolean updateFromSource(final Object source);

}
