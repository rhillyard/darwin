/**
 * RUBECULA COMMON UTILITIES.
 * Copyright (C) 2003, 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Randomizer.java
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.api.impl;

import java.io.Serializable;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Title: Randomizer
 * </p>
 * <p>
 * Description: this class extends {@link RandomGenerator} to play a kind of
 * roulette wheel game. A set of relative frequencies is defined for each
 * "band". The result of calling nextIndex() is to return an index into the
 * band. Obviously, the higher the frequency of the band, the more likely the
 * index is going to point to that particular band. <br>
 * For the roulette wheel game, there would be 37 (38 in the USA) bands defined
 * with equal frequency. Unless of course you were setting up a crooked roulette
 * wheel, in which case you would increase (slightly, presumably) the frequency
 * of your favorite band.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company: Rubecula Software, LLC
 * </p>
 * 
 * @author Robin Hillyard
 * @version 1.0 TODO consider extending AToString
 */

public class Randomizer implements RandomGenerator, Serializable {

	/**
	 * Secondary constructor for randomizer with an empty array of band widths.
	 * The Random Number Generator used will be an instance of
	 * {@link Random_Standard} with no specified seed. The RNG will not be
	 * reseeded after each call.
	 * 
	 * @deprecated
	 */
	@Deprecated
	public Randomizer() {
		this(new int[] {});
	}

	/**
	 * Secondary constructor for randomizer with an array of band widths. The
	 * Random Number Generator used will be an instance of
	 * {@link Random_Standard} with no specified seed. The RNG will not be
	 * reseeded after each call.
	 * 
	 * @param bands
	 *            An array of longs which give the relative frequencies desired
	 *            for each band.
	 * 
	 * @deprecated
	 */
	@Deprecated
	public Randomizer(final int[] bands) {
		this(bands, false);
	}

	/**
	 * Secondary constructor for randomizer with an array of band widths. The
	 * Random Number Generator used will be an instance of
	 * {@link Random_Standard} with no specified seed.
	 * 
	 * @param bands
	 *            An array of longs which give the relative frequencies desired
	 *            for each band.
	 * @param reseed
	 *            true if the randomizer is to be reseeded before every
	 *            randomizing call. This is good when the calls are made
	 *            according to e.g. clicks in a user interface or updates from a
	 *            web service. Otherwise, the numbers follow a pseudo-random
	 *            number generator sequence.
	 * 
	 * @deprecated
	 */
	@Deprecated
	public Randomizer(final int[] bands, final boolean reseed) {
		this(new Random_Standard(), bands, reseed);
	}

	/**
	 * Secondary constructor for randomizer with an array of band widths. The
	 * Random Number Generator used will be an instance of
	 * {@link Random_Standard} with the specified seed. The RNG will not be
	 * reseeded after each call.
	 * 
	 * @param bands
	 *            An array of longs which give the relative frequencies desired
	 *            for each band.
	 * @param seed
	 *            the seed for the first random number.
	 * 
	 * @deprecated
	 */
	@Deprecated
	public Randomizer(final int[] bands, final long seed) {
		this(new Random_Standard(seed), bands, false);
		System.err.println("Randomizer(int[],long) is deprecated"); //$NON-NLS-1$
	}

	/**
	 * Secondary constructor for randomizer with an empty array of band widths.
	 * The Random Number Generator used will be an instance of
	 * {@link Random_Standard} with the specified seed. The RNG will not be
	 * reseeded after each call.
	 * 
	 * @param seed
	 *            the seed for the first random number.
	 * 
	 * @deprecated
	 */
	@Deprecated
	public Randomizer(final long seed) {
		this(new int[] {}, seed);
	}

	/**
	 * Construct a randomizer with an empty array of band widths, and using the
	 * specified random number generator. The RNG will not be reseeded after
	 * each call.
	 * 
	 * @param random
	 *            a random number generator.
	 */
	public Randomizer(final RandomGenerator random) {
		this(random, new int[] {});
	}

	/**
	 * Construct a randomizer with an array of band widths, and using the
	 * specified random number generator. The RNG will not be reseeded after
	 * each call.
	 * 
	 * @param random
	 * 
	 * @param bands
	 *            An array of longs which give the relative frequencies desired
	 *            for each band.
	 */
	public Randomizer(final RandomGenerator random, final int[] bands) {
		this(random, bands, false);
	}

	/**
	 * Primary constructor.
	 * 
	 * @param random
	 *            a random number generator.
	 * @param bands
	 * @param reseed
	 *            if true, the RNG will be reseeded after each call (using the
	 *            system clock).
	 */
	public Randomizer(final RandomGenerator random, final int[] bands, final boolean reseed) {
		super();
		this._random = random;
		setBands(bands);
		this._Reseed = reseed;
	}

	/**
	 * @return the array field {@link #_Bands}
	 */
	public int[] getBands() {
		return this._Bands;
	}

	/**
	 * @return the field {@link #_Total}
	 */
	public int getTotal() {
		return this._Total;
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextBoolean()
	 */
	@Override
	public boolean nextBoolean() {
		return getRandom().nextBoolean();
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextBytes(byte[])
	 */
	@Override
	public void nextBytes(final byte[] bytes) {
		getRandom().nextBytes(bytes);
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextDouble()
	 */
	@Override
	public double nextDouble() {
		return getRandom().nextDouble();
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextFloat()
	 */
	@Override
	public float nextFloat() {
		return getRandom().nextFloat();
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextGaussian()
	 */
	@Override
	public double nextGaussian() {
		return getRandom().nextGaussian();
	}

	/**
	 * The behavior of this method depends on whether any bands were defined for
	 * this randomizer. If bands were defined, the result is an index into the
	 * bands. If bands were not defined, the result is a number uniformly
	 * distributed between 0 and {@link Integer#MAX_VALUE}. In both cases, the
	 * superclass method {@link RandomGenerator#nextInt(int)} is called.
	 * 
	 * @return the "index" as defined above.
	 */
	public int nextIndex() {
		if (isReseed())
			setSeed(System.currentTimeMillis());

		if (getTotal() > 0) {
			int randomizer = nextInt(getTotal());
			for (int i = 0; i < getBands().length; i++) {
				randomizer -= getBands()[i];
				if (randomizer < 0) {
					return i;
				}
			}
			return -1; // logic error
		}

		return nextInt(Integer.MAX_VALUE);
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextInt()
	 */
	@Override
	public int nextInt() {
		return getRandom().nextInt();
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextInt(int)
	 */
	@Override
	public int nextInt(final int n) {
		return getRandom().nextInt(n);
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#nextLong()
	 */
	@Override
	public long nextLong() {
		return getRandom().nextLong();
	}

	/**
	 * @param bands
	 */
	public void setBands(final int[] bands) {
		this._Bands = bands;
		int total = 0;
		for (int i = 0; i < getBands().length; i++) {
			total += getBands()[i];
		}
		setTotal(total);
	}

	/**
	 * @param odds
	 *            the odds.
	 */
	public void setOdds(final int odds) {
		if (odds > 0)
			setBands(new int[] { odds, 1 });
		else
			setBands(new int[] { 1 });
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.apache.commons.math.random.RandomGenerator#setSeed(int)
	 */
	@Override
	public void setSeed(final int seed) {
		getRandom().setSeed(seed);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.apache.commons.math.random.RandomGenerator#setSeed(int[])
	 */
	@Override
	public void setSeed(final int[] seed) {
		getRandom().setSeed(seed);
	}

	/**
	 * @see org.apache.commons.math.random.RandomGenerator#setSeed(long)
	 */
	@Override
	public void setSeed(final long seed) {
		getRandom().setSeed(seed);
	}

	/**
	 * @return {@link #_random}
	 */
	private RandomGenerator getRandom() {
		return this._random;
	}

	/**
	 * @return the reseed
	 */
	private boolean isReseed() {
		return this._Reseed;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	private void setTotal(final int total) {
		this._Total = total;
	}

	/**
	 * An array of relative frequencies. The result of calling nextIndex() will
	 * be an index into this array, that is to say it will be between 0 and
	 * _Bands.length-1 (inclusive).
	 */
	private int _Bands[];

	/**
	 * The total frequencies of all the bands.
	 */
	private int _Total;

	/**
	 * determines whether the RNG should be reseeded before each call. defaults
	 * to false.
	 */
	private final boolean _Reseed;

	/**
	 * 
	 */
	private static final long serialVersionUID = 5663338561378233818L;

	private final RandomGenerator _random;

}