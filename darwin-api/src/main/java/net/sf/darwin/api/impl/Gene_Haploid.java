/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Gene_Haploid.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.17 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Gene_;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Ploidy;

/**
 * Concrete class to model a haploid gene (locus).
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = false)
public class Gene_Haploid<V> extends Gene_<V> {

	/**
	 * Package-scope constructor to create a haploid gene with allele in slot 0.
	 * 
	 * Applications should use {@link GeneFactory#makeHaploid(Locus, String)}
	 * rather than use this constructor directly.
	 * 
	 * @param index
	 * 
	 */
	public Gene_Haploid(final String index) {
		super(Ploidy.HAPLOID);
		setAllele(0, index);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3752242250732039157L;

}
