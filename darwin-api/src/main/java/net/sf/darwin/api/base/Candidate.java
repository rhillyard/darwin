/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Candidate.java
 * Created on Jul 17, 2010
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Mate;

/**
 * @author Robin Hillyard
 * 
 */
public interface Candidate {

	/**
	 * Update this object according to candidate male and desirability,
	 * providing that the candidate desirability is greater than the current
	 * value (and that the candidate male is not null).
	 * 
	 * @param candidate
	 *            the candidate Mate (may be null in which case nothing will
	 *            happen)
	 * 
	 * @return true if this was updated to equal the candidate.
	 */
	public abstract boolean updateIfBetter(final Mate candidate);

}