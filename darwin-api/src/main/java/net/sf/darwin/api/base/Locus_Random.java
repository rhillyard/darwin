/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Locus_Random.java
 * Created on Mar 3, 2007
 * @version $Revision: 1.26 $
 */

package net.sf.darwin.api.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.api.impl.Randomizer;
import net.sf.darwin.core.Allele;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Default implementation of {@link Locus} which uses a random number source to
 * pick alleles during seeding.
 * 
 * @author Robin Hillyard
 * 
 * @param <G>
 *            the type of the genetic information which is encoded in this Locus
 */
@Lifecycle(permanent = true)
public abstract class Locus_Random<G> extends Locus_<G> {

	/**
	 * @param identifier
	 * @param random
	 */
	public Locus_Random(final String identifier, final RandomGenerator random) {
		super(identifier);
		this._random = new Randomizer(random);
	}

	/**
	 * This is used only when a mutation occurs. Normally the legal alleles at
	 * the start of an evolution are set using {@link #setAlleles(Map)}.
	 * 
	 * @return the result of invoking {@link #putAllele(Allele, int)} with
	 *         allele and frequency as the parameters.
	 * 
	 *         TODO eliminating this and using putAllele directly.
	 * 
	 * @see net.sf.darwin.core.Locus#add(net.sf.darwin.core.Allele, int)
	 */
	@Override
	public synchronized String add(final Allele<G> allele, final int frequency) {
		final String result = putAllele(allele, frequency);
		//
		// // TODO consider using onChange() which is more general than this
		// // and doesn't build on the existing data, but instead starts fresh.
		// final int[] bands = _random.getBands();
		// final int length = bands.length;
		// final int[] bandsNew = new int[length + 1];
		// System.arraycopy(bands, 0, bandsNew, 0, length);
		// bandsNew[length] = frequency;
		// _random.setBands(bandsNew);
		// _index.add(allele.getIdentifier());
		return result;
	}

	/**
	 * Randomly choose a particular allele from the possible alleles at this
	 * locus.
	 * 
	 * @return the key to the allele.
	 * 
	 *         XXX this should probably be combined with the Seed stuff.
	 * 
	 * @see net.sf.darwin.core.Locus#pickAllele()
	 */
	@Override
	public String pickAllele() {
		// XXX use Range class here
		final int total = getRandom().getTotal();
		if (total > 0) {
			final int index = getRandom().nextIndex();
			if (index < getIndex().size())
				return getIndex().get(index);

			throw new DarwinException("index is out of range"); //$NON-NLS-1$
		}
		throw new DarwinException("locus randomizer has no bands (probably locus has no alleles)"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.api.base.Locus_#setAlleles(Map)
	 */
	@Override
	public void setAlleles(final Map<? extends Allele<G>, Integer> map) {
		super.setAlleles(map);
		onChange();
	}

	/**
	 * This does essentially the same thing as {@link #add(Allele, int)},
	 * although the follow-up work on the {@link #_random} is slightly
	 * different, plus the other method returns a result which should normally
	 * be null.
	 * 
	 * @param key
	 *            the allele key
	 * @param frequency
	 */
	void setFrequency(final String key, final int frequency) {
		if (getAlleleMap().get(key) != null) {
			getFrequencies().put(key, Integer.valueOf(frequency));
			onChange();
		} else
			throw new DarwinException("no such allele key: " + key); //$NON-NLS-1$
	}

	/**
	 * @return the index
	 */
	protected List<String> getIndex() {
		return this._index;
	}

	/**
	 * @return the random
	 */
	protected Randomizer getRandom() {
		return this._random;
	}

	/**
	 * 
	 */
	@Override
	protected void onChange() {
		getIndex().clear();
		final int[] bands = new int[getFrequencies().size()];
		final Set<String> keySet = getAlleleMap().keySet();
		int bandIndex = 0;
		for (final String allele : keySet) {
			bands[bandIndex++] = getFrequencies().get(allele).intValue();
			getIndex().add(allele);
		}
		getRandom().setBands(bands);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8044950193860066223L;

	private final List<String> _index = new ArrayList<>();

	private final Randomizer _random;

}
