/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Node_
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author rhillya
 * 
 */
public abstract class Node_ implements NodeMutable {

	/**
	 * @param type
	 * 
	 * @param value
	 * 
	 * @param appearance
	 * 
	 * @param childrenProvisioner
	 * 
	 * 
	 */
	public Node_(final NodeType type, final Nodal value, final Appearance appearance,
			final Provisioner<Collection<Node>> childrenProvisioner, final Map<String, Object> properties) {
		this._type = type;
		this._value = value;
		this._appearance = appearance;
		this._properties = properties;
		this._childrenProvisioner = childrenProvisioner;
		this._actionMenu = new HashMap<String, Action>();
	}

	/**
	 * @param key
	 * @param value
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void addActionMenuItem(final String key, final Action value) {
		this._actionMenu.put(key, value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.NodeMutable#addChild(net.sf.darwin.api.ui.model.Node)
	 */
	@Override
	public boolean addChild(final Node child) {
		return this._children.add(child);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.NodeMutable#addProperty(java.lang.String,
	 *      Object)
	 */
	@Override
	public void addProperty(final String key, final Object value) {
		this._properties.put(key, value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Node#getActionMenu()
	 */
	@Override
	public Map<String, Action> getActionMenu() {
		return this._actionMenu;
	}

	/**
	 * @return the appearance
	 */
	@Override
	public Appearance getAppearance() {
		return this._appearance;
	}

	/**
	 * @return
	 * @see net.sf.darwin.api.ui.model.Node#getChildren()
	 */
	@Override
	public Collection<Node> getChildren() {
		final Collection<Node> result = this._children;
		if (result != null)
			return result;
		setChildren(this._childrenProvisioner.provision());
		return this._children;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Node#getNodeType()
	 */
	@Override
	public NodeType getNodeType() {
		return this._type;
	}

	/**
	 * @return the properties
	 */
	@Override
	public Map<String, Object> getProperties() {
		return this._properties;
	}

	/**
	 * @return the value
	 */
	@Override
	public Nodal getValue() {
		return this._value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Node#hasChildren()
	 */
	@Override
	public boolean hasChildren() {
		// FIXME we should change the logic here because null value
		// may simply mean that the children haven't been elaborated yet
		if (getChildren() == null)
			return false;
		return !this.getChildren().isEmpty();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.ui.model.Node#isDrawn()
	 */
	@Override
	public boolean isDrawn() {
		return this.drawn;
	}

	/**
	 * @return the upToDate
	 */
	@Override
	public boolean isUpToDate() {
		return !getValue().isDirty();
	}

	/**
	 * @return the visible
	 */
	@Override
	public boolean isVisible() {
		return this.visible;
	}

	@Override
	public void refresh() {
		switch (this._type) {
		case Unelaborated:
		case Property:
			break;

		default:
			// Everything else can be taken care of simply by
			// marking not up to date and resetting the children
			setUpToDate(false);
			this._children = null;
			break;
		}
	}

	/**
	 * @param children
	 *            the children to set
	 */
	@Override
	public void setChildren(final Collection<Node> children) {
		this._children = children;
	}

	/**
	 * @param drawn
	 *            the drawn to set
	 */
	public void setDrawn(final boolean drawn) {
		this.drawn = drawn;
	}

	/**
	 * @param upToDate
	 *            the upToDate to set
	 */
	@Override
	public void setUpToDate(final boolean upToDate) {
		final Dirty value = getValue();
		if (value != null)
			value.setDirty(!upToDate);
		// TODO we should set everything up to date if value is null
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	@Override
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@SuppressWarnings("nls")
	@Override
	public String toString() {
		return "Node [type=" + this._type + ", value=" + this._value + "]";
	}

	private boolean drawn = false;

	private final Appearance _appearance;

	private final Provisioner<Collection<Node>> _childrenProvisioner;

	private Collection<Node> _children = null;

	private final NodeType _type;

	private final Nodal _value;

	private final Map<String, Object> _properties;

	private boolean visible = true;

	private final Map<String, Action> _actionMenu;

}
