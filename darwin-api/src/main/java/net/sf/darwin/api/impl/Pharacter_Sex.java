/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Pharacter_Sex.java
 * Created on Jun 8, 2009
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Pharacter_;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Pharacter;
import net.sf.darwin.core.Variant;

/**
 * Implementer of {@link Pharacter} (Phenotypic Character) specifically for sex.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true, singleton = true)
public final class Pharacter_Sex extends Pharacter_<Boolean> {

	/**
	 * Private constructor
	 */
	private Pharacter_Sex() {
		super(Pharacter.SEX, true);
		init();
	}

	/**
	 * 
	 */
	private void init() {
		final Variant<Boolean> variantMale = new Variant_Basic<>(KEY_M, Boolean.FALSE);
		final Variant<Boolean> variantFemale = new Variant_Basic<>(KEY_F, Boolean.TRUE);
		addVariant(variantMale);
		addVariant(variantFemale);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3305539201745054740L;

	/**
	 * F
	 */
	public static final String KEY_F = "F"; //$NON-NLS-1$

	/**
	 * M
	 */
	public static final String KEY_M = "M"; //$NON-NLS-1$

	/**
	 * the singleton instance
	 */
	public static Pharacter_Sex instance;

	/**
	 * 1
	 */
	public static final int INDEX_FEMALE = 1;

	/**
	 * @return the singleton instance
	 */
	public static Pharacter_Sex getInstance() {
		if (instance == null)
			instance = new Pharacter_Sex();
		return instance;
	}

}
