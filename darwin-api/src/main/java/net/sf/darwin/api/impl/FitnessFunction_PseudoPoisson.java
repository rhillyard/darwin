/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_PseudoPoisson.java
 * Created on Sep 14, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FitnessFunction_;
import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessProblem;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.Notation;

/**
 * @author Robin Hillyard
 * 
 */
public class FitnessFunction_PseudoPoisson extends FitnessFunction_ {

	/**
	 * 
	 */
	public FitnessFunction_PseudoPoisson() {
		this(null);
	}

	/**
	 * @param evaluator
	 */
	public FitnessFunction_PseudoPoisson(final Evaluator evaluator) {
		super(evaluator);
	}

	/**
	 * @return {@link #ID_CLASS}
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return ID_CLASS;
	}

	/**
	 * @see net.sf.darwin.api.base.Function_#standardFunction(java.lang.Object[])
	 */
	@Override
	protected double standardFunction(final Object... arguments) throws FunctionException {
		// No protection against class cast exceptions
		final String methodPrefix = "FitnessFunction_PseudoPoisson.standardValue(): "; //$NON-NLS-1$
		if (arguments.length == 3) {
			final double value = ((Number) arguments[0]).doubleValue();
			final double target = ((Number) arguments[1]).doubleValue();
			final double shapeFactor = ((Number) arguments[2]).doubleValue();
			try {
				return calculatePseudoPoissonValue(value, target, shapeFactor);
			} catch (final FitnessException e) {
				throw new FunctionException(methodPrefix + "fitness exception", e); //$NON-NLS-1$
			}
		}
		throw new FunctionException(methodPrefix + "requires 3 arguments"); //$NON-NLS-1$)

	}

	/**
	 * 
	 * @see net.sf.darwin.api.base.Function_#standardFunctionTokens(Notation)
	 */
	@Override
	protected CharSequence[] standardFunctionTokens(final Notation notation) {
		switch (notation) {
		case RPN:
			return standardFunctionTokensRPN();
		default:
			return standardFunctionTokensAlgebraic();
		}
	}

	/**
	 * @param value
	 * @param target
	 * @param shapeFactor
	 * @return
	 * @throws FitnessException
	 */
	@SuppressWarnings("static-method")
	private double calculatePseudoPoissonValue(final double value, final double target, final double shapeFactor)
			throws FitnessException {
		if (value == target)
			return 1.0;
		if (value < target) {
			final double adjustment = target == 0.0 ? Double.NaN : value / target;
			throw new FitnessException(
					"PoissonTargetExceedsValue", new Double(adjustment), FitnessProblem.PoissonTargetExceedsValue); //$NON-NLS-1$
		}
		final double x = shapeFactor / (value - target);
		return 1 - Math.exp(-x);
	}

	/**
	 * 
	 */
	private static final String ID_CLASS = "Fitness function: pseudo-Poisson distribution"; //$NON-NLS-1$

	/**
	 * TODO this sequence does not work properly when value < target.
	 * 
	 * @return <code>1-e^(-shapeFactor/(value-target))</code>
	 */
	@SuppressWarnings("nls")
	private static CharSequence[] standardFunctionTokensAlgebraic() {
		return new CharSequence[] { "1-", Concept.e, Concept.pow,
				"(-" + VAR_SHAPE_FACTOR + "/(" + VAR_VALUE + "-" + VAR_TARGET + "))" }; //$NON-NLS-1$
	}

	/**
	 * TODO this sequence does not work properly when value < target.
	 * 
	 * @return <code>1 e $shapeFactor $value $target - + / * - ^ - +</code>
	 * 
	 */
	@SuppressWarnings("nls")
	private static CharSequence[] standardFunctionTokensRPN() {
		return new CharSequence[] {
				Concept.e,
				PREFIX_VAR_RPN + VAR_VALUE + " " + PREFIX_VAR_RPN + VAR_TARGET + " - + / " + PREFIX_VAR_RPN + VAR_SHAPE_FACTOR
				+ " * -", Concept.pow, "- 1 +" };
	}

}
