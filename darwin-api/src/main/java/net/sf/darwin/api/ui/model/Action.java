/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: Action
 * Created on: Oct 4, 2011
 */

package net.sf.darwin.api.ui.model;

/**
 * @author rhillya
 * 
 */
public interface Action {

	/**
	 * Perform the action on the node.
	 * 
	 * @param node
	 *            the Node on which the action is to be taken
	 * @param menuItem
	 *            this is provided for convenience: it is the name of the action
	 *            in the menu
	 */
	public void performAction(Node node, String menuItem);

}
