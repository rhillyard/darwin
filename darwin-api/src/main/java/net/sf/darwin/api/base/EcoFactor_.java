/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EcoFactor_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;

import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Visualizable;
import net.sf.tostring0.Detail;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class providing base functionality for implementers of
 * {@link EcoFactor}.
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <E>
 *            the type of the environmental information which is expressed in
 *            this {@link EcoFactor}
 */
@ToString(allBeanMethods = false)
public abstract class EcoFactor_<E> extends Attribute_<String, E> implements EcoFactor<E>, Cloneable {

	/**
	 * Protected constructor.
	 * 
	 * @param identifier
	 * @param value
	 */
	protected EcoFactor_(final String identifier, final E value) {
		super(identifier, value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.Censusible#censusMe(net.sf.darwin.core.Census,
	 *      java.lang.Object)
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		census.present("  " + getIdentifier() + "   with eco factor " + getIdentifier() //$NON-NLS-1$ //$NON-NLS-2$
				+ " and value: " + getValue(), null); //$NON-NLS-1$
		return false;
	}

	/**
	 * Note that if an EcoFactor's value (or identifier) is mutable, i.e. is not
	 * a primitive (or String) and is dynamically changeable, it probably needs
	 * deep-copying. Therefore sub-classes should probably override this method.
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return null;
	}

	/**
	 * @see net.sf.darwin.core.EcoFactor#getEnvironment()
	 */
	@Override
	public Environment<E> getEnvironment() {
		return this.environment;
	}

	/**
	 * @see net.sf.darwin.core.Individual#getVisualizable()
	 */
	@Override
	public Visualizable getVisualizable() {
		return getEnvironment();
	}

	/**
	 * @see net.sf.darwin.core.EcoFactor#setEnvironment(net.sf.darwin.core.Environment)
	 */
	@Override
	public void setEnvironment(final Environment<E> environment) {
		this.environment = environment;
	}

	/**
	 * @see net.sf.darwin.api.base.Attribute_#toString(net.sf.tostring0.Detail)
	 */
	@Override
	public String toString(final Detail detail) {
		if (detail.isShowDetail()) {
			return "Factor " + getAttribute() + ": " + getValue(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return getValue().toString();
	}

	private static final long serialVersionUID = -3236428721885904472L;

	/**
	 * the environment to which this eco-factor belongs.
	 */
	@ToString(parent = true)
	private Environment<E> environment;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(EcoFactor_.class);

}
