/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessFunction_Normal.java
 * Created on Apr 6, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.FitnessFunction_;
import net.sf.darwin.api.base.FunctionException;
import net.sf.darwin.core.Lifecycle;

import com.rubecula.jexpression.Concept;
import com.rubecula.jexpression.Evaluator;
import com.rubecula.jexpression.Notation;

/**
 * This class implements an approximate normal distribution. An alternative
 * implementation requires the use of the Apache commons math library.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public class FitnessFunction_Normal extends FitnessFunction_ {

	/**
	 */
	public FitnessFunction_Normal() {
		this(null);
	}

	/**
	 * @param evaluator
	 */
	public FitnessFunction_Normal(final Evaluator evaluator) {
		super(evaluator);
	}

	/**
	 * @return "Fitness function: normal distribution"
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return "Fitness function: normal distribution"; //$NON-NLS-1$
	}

	/**
	 * @param arguments
	 *            these arguments must be, in order,
	 *            <ol>
	 *            <li>Double: value</li>
	 *            <li>Double: mean</li>
	 *            <li>Double: standard deviation</li>
	 *            </ol>
	 * <br>
	 * 
	 *            Note that we do not protect against cast exceptions.
	 * 
	 * @return result of invoking
	 *         {@link #normalDistributionDensityFunction(double, double, double)}
	 *         .
	 * @see net.sf.darwin.api.base.Function_#standardFunction(Object[])
	 */
	@Override
	protected double standardFunction(final Object... arguments) throws FunctionException {
		// No protection against class cast exceptions
		final String methodPrefix = "FitnessFunction_Normal.standardValue(): "; //$NON-NLS-1$
		if (arguments.length == 3)
			return normalDistributionDensityFunction(((Number) arguments[0]).doubleValue(),
					((Number) arguments[1]).doubleValue(), ((Number) arguments[2]).doubleValue());
		throw new FunctionException(methodPrefix + "requires 3 arguments"); //$NON-NLS-1$)
	}

	/**
	 * @see net.sf.darwin.api.base.Function_#standardFunctionTokens(Notation)
	 */
	@Override
	protected CharSequence[] standardFunctionTokens(final Notation notation) {
		switch (notation) {
		case RPN:
			return standardFunctionTokensRPN();
		default:
			return standardFunctionTokensAlgebraic();
		}
	}

	/**
	 * 
	 */
	private static final String S_TIMES = "*"; //$NON-NLS-1$

	/**
	 * @return <code>e^(-(value-target)^2/2/shapeFactor/shapeFactor)/shapeFactor/sqrt(2*pi)</code>
	 */
	@SuppressWarnings("nls")
	private static CharSequence[] standardFunctionTokensAlgebraic() {
		return new CharSequence[] { Concept.e, Concept.pow, "(-(" + VAR_VALUE + "-" + VAR_TARGET + ")", Concept.pow,
				"2/2/" + VAR_SHAPE_FACTOR, Concept.pow, "2)/" + VAR_SHAPE_FACTOR + "/sqrt(2" + S_TIMES, Concept.pi, ")" };
	}

	/**
	 * XXX reference shape factor only once, by using the "copy" operator.
	 * 
	 * @return <code>E $shapeFactor 2 ^ 2 * / $value $target + " - + 2 ^ * ^ $shapeFactor pi 2 * sqrt * / *</code>
	 *         as a sequence of tokens.
	 */
	@SuppressWarnings("nls")
	private static CharSequence[] standardFunctionTokensRPN() {
		return new CharSequence[] { Concept.e, PREFIX_VAR_RPN + VAR_SHAPE_FACTOR + " 2", Concept.pow,
				"2 " + S_TIMES + " / " + PREFIX_VAR_RPN + VAR_VALUE + " " + PREFIX_VAR_RPN + VAR_TARGET + " - + 2", Concept.pow,
				S_TIMES, Concept.pow, PREFIX_VAR_RPN + VAR_SHAPE_FACTOR, Concept.pi, "2 " + S_TIMES, Concept.sqrt,
				S_TIMES + " / " + S_TIMES };
	}

}
