/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Registry_.java
 * Created on Apr 11, 2009
 * @version $Revision: 1.20 $
 */

package net.sf.darwin.api.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Registry;

/**
 * @author Robin Hillyard
 * 
 * @param <P>
 *            the type of the phenotypic information which is expressed by
 *            members of this registry
 * @param <G>
 *            the type of the genetic information which is encoded by members of
 *            this registry
 */
public abstract class Registry_<E, P, G> implements Registry<E, P, G> {

	/**
	 * Protected constructor
	 */
	protected Registry_() {
		super();
	}

	/**
	 * Do nothing
	 * 
	 * @see net.sf.darwin.core.Registry#registerBirths(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerBirths(final Colony<E, P, G> colony, final Collection<Organism<E, P, G>> neonates) {
		// By default, we do nothing at all
	}

	/**
	 * Do nothing
	 * 
	 * @see net.sf.darwin.core.Registry#registerDeaths(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerDeaths(final Colony<E, P, G> colony, final Collection<Organism<E, P, G>> deaths) {
		// By default, we do nothing at all
	}

	/**
	 * Do nothing.
	 * 
	 * @see net.sf.darwin.core.Registry#registerMarriages(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerMarriages(final Colony<E, P, G> colony, final Collection<Mating> marriages) {
		// By default, we do nothing at all
	}

	/**
	 * TODO consider using FrequencyMap_HashMap instead
	 * 
	 * @param organisms
	 * @return the frequency map for the Organism's phenotypes
	 */
	protected Map<Object, Integer> getOrganismFrequencyMap(final Collection<Organism<E, P, G>> organisms) {
		final Map<Object, Integer> map = new HashMap<>();
		for (final Organism<E, P, G> organism : organisms) {
			final Phenotype<P> phenotype = organism.getPhenotype();
			if (phenotype != null) {
				for (final Object key : phenotype.getKeys()) {
					// TODO check this cast
					increment(map, (String) key);
				}
			} else
				throw new DarwinException("getOrganismFrequencyMap(): organism already dead: " + organism); //$NON-NLS-1$
		}
		return map;
	}

	/**
	 * @param map
	 * @param key
	 */
	private static void increment(final Map<Object, Integer> map, final String key) {
		int y = 0;
		final Integer x = map.get(key);
		if (x != null)
			y = x.intValue();
		map.put(key, Integer.valueOf(y + 1));
	}
}
