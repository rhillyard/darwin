/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionSource_Applet
 * Created on: Mar 4, 2012
 */

package net.sf.darwin.api.base;

import java.applet.Applet;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author rhillya
 * 
 */
public class OptionSource_Applet implements OptionSource<String> {

	public OptionSource_Applet(final Applet applet) {
		super();
		this._applet = applet;
		this._parameterKeys = new ArrayList<>();
		this._parameterInfo = this._applet.getParameterInfo();
		if (this._parameterInfo == null)
			throw new RuntimeException("cannot use applet as an option source because it does not provide parameter info"); //$NON-NLS-1$
		for (final String[] parameter : this._parameterInfo)
			this._parameterKeys.add(parameter[0]);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.OptionSource#getKeys()
	 */
	@Override
	public Collection<String> getKeys() {
		return this._parameterKeys;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.OptionSource#getOption(java.lang.String)
	 */
	@Override
	public String getOption(final String key) {
		return this._applet.getParameter(key);
	}

	private final Applet _applet;

	private final Collection<String> _parameterKeys;

	private final String[][] _parameterInfo;

}
