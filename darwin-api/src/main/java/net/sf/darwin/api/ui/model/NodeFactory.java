/**
 * Substrate Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Organization: Advanced Computations Lab
 * Module: NodeFactory
 * Created on: Sep 30, 2011
 */

package net.sf.darwin.api.ui.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author rhillya
 * 
 */
public final class NodeFactory {

	/**
	 * @param cell
	 * @param properties
	 * @return
	 */
	public static Node createCellNode(final Nodal cell, final Map<String, Object> properties) {
		return createNode(NodeType.Cell, cell, new Appearance_() {

			@Override
			public String getContentString() {
				// TODO get this directly but note that this allows properties
				// to change dynamically
				return properties.get(Node.PROPERTY_TYPE) + ": " + properties.get(Node.PROPERTY_ID); //$NON-NLS-1$
			}

			@Override
			public String getHoverString() {
				// TODO get this directly but note that this allows properties
				// to change dynamically
				// TODO check cast
				return properties.get(Node.PROPERTY_ENVELOPE).toString();
			}

			@Override
			public String getImageName() {
				// TODO get this directly but note that this allows properties
				// to change dynamically
				return properties.get(Node.PROPERTY_TYPE).toString().toLowerCase();
			}
		}, cell, properties);
	}

	/**
	 * @param organelle
	 * @param properties
	 * @return
	 */
	public static Node createOrganelleNode(final Nodal organelle, final Map<String, Object> properties) {
		return createNode(NodeType.Organelle, organelle, new Appearance_() {

			@Override
			public String getContentString() {
				// TODO get this directly but note that this allows properties
				// to change dynamically
				return properties.get(Node.PROPERTY_CLASS) + ": " + properties.get(Node.PROPERTY_ID); //$NON-NLS-1$
			}
		}, organelle, properties);
	}

	/**
	 * @return
	 */
	public static Node createRootNode() {
		return createNode(NodeType.Unelaborated, null, new Appearance_() {
			// no method definitions
		}, new Provisioner<Collection<Node>>() {

			@Override
			public Collection<Node> provision(final Object... parameters) {
				return new ArrayList<Node>();
			}
		}, new HashMap<String, Object>());
	}

	/**
	 * @param substrate
	 * @param properties
	 * @return
	 */
	public static Node createSubstrateNode(final Nodal substrate, final Map<String, Object> properties) {
		return createNode(NodeType.Substrate, substrate, new Appearance_() {

			@Override
			public String getContentString() {
				// TODO get this directly but note that this allows properties
				// to change dynamically
				return (String) properties.get(Node.PROPERTY_NAME);
			}

			@Override
			public String getHoverString() {
				// TODO get this directly but note that this allows properties
				// to change dynamically
				return "" + properties.get(Node.PROPERTY_COMMENT); //$NON-NLS-1$
			}

		}, substrate, properties);
	}

	/**
	 * 
	 * @param type
	 * @param object
	 *            a Dirty object
	 * 
	 *            TODO change to Dirty.
	 * 
	 * @param appearance
	 * 
	 * @param childrenProvisioner
	 * 
	 * @param properties
	 * @return
	 */
	private static Node createNode(final NodeType type, final Nodal object, final Appearance appearance,
			final Provisioner<Collection<Node>> childrenProvisioner, final Map<String, Object> properties) {
		return new Node_(type, object, appearance, childrenProvisioner, properties) {

			@Override
			public Collection<Node> getConnexions() {
				switch (type) {
				default:
					return new ArrayList<Node>();
				}
			}
		};
	}

}
