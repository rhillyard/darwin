/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Dec 5, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Meiosis_;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Ploidy;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * Implementation for the {@link Meiosis} interface where genes on chromosomes
 * tend to stick together (depending on the value of crossoverProbability).
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.22 $
 */
@Lifecycle(permanent = true)
public final class Meiosis_Chromosome<G> extends Meiosis_<G> {

	/**
	 * Public primary constructor for an implementer of {@link Meiosis} with
	 * given random number source and no crossover.
	 * 
	 * @param random
	 *            a source of random numbers.
	 */
	public Meiosis_Chromosome(final RandomGenerator random) {
		this(random, 0.0);
	}

	/**
	 * Public primary constructor for an implementer of {@link Meiosis} with
	 * given random number source.
	 * 
	 * @param random
	 *            a source of random numbers.
	 * @param crossoverProbability
	 *            defaults to 0 (no crossover - whole chromosomes stay
	 *            together). A value of 1 means that crossover will always occur
	 *            (at a randomly chosen gene locus).
	 */
	public Meiosis_Chromosome(final RandomGenerator random, final double crossoverProbability) {
		super(random);
		this._crossoverProbability = crossoverProbability;
	}

	/**
	 * This method does meiosis for a set of genes. Any genes not belonging to
	 * chromosome are ignored. Otherwise, each gene takes as its allele the
	 * allele determined by index, a randomly generated integer which is the
	 * same for all genes of the chromosome.
	 * 
	 * @param genomic
	 * 
	 * @param genes
	 *            the genes to process
	 * @param chromosome
	 *            the chromosome of interest.
	 * @return a genome which is the result of doing meiosis at the gene or
	 *         chromosome level
	 * 
	 * @see net.sf.darwin.core.Meiosis#doMeiosis(net.sf.darwin.core.Genomic,
	 *      net.sf.darwin.core.Genes, net.sf.darwin.core.Chromosome)
	 */
	@Override
	public <P> Genome<P, G> doMeiosis(final Genomic<P, G> genomic, final Genes<G> genes, final Chromosome<G> chromosome) {
		if (chromosome != null) {
			if (getCrossoverProbability() <= 0 || getCrossoverProbability() < getRandom().nextDouble())
				return doMeiosisChromosome(genomic, genes, chromosome, getRandom().nextInt(genomic.getPloidy()));

			return doMeiosisCrossover(genomic, genes, chromosome);
		}

		final Genome<P, G> result = GenomeFactory.makeLinear(genomic, Ploidy.HAPLOID);
		for (final Chromosome<G> c : genomic.getChromosomes()) {
			final Genes<G> genome = doMeiosis(genomic, genes, c);
			result.addGenes(genome);
		}
		return result;
	}

	/**
	 * @return the crossover probability which was set in the constructor
	 *         (between 0 -- no crossover -- and 1 -- always crossover).
	 */
	protected double getCrossoverProbability() {
		return this._crossoverProbability;
	}

	/**
	 * In this method, which is essentially similar to
	 * {@link Meiosis_#doMeiosisChromosome(Genomic, Genes, Chromosome, int)}, we
	 * first find a crossover point. Genes before the crossover point will use a
	 * common index for the allele, based on a random pick. After the crossover
	 * point, genes will use the other allele (normally the ploidy will be 2).
	 * 
	 * @param genomic
	 * @param genes
	 * @param chromosome
	 * @return
	 */
	private <P> Genome<P, G> doMeiosisCrossover(final Genomic<P, G> genomic, final Genes<G> genes, final Chromosome<G> chromosome) {
		final RandomGenerator random = getRandom();
		final int crossover = random.nextInt(genes.size());
		final int ploidy = genomic.getPloidy();
		int index = random.nextInt(ploidy);
		final Genome<P, G> result = new Genome_Linear<>(genomic, Ploidy.HAPLOID);
		for (int i = 0; i < genes.size(); i++) {
			if (i >= crossover) {
				index = ploidy - index - 1;
				if (LOG.isTraceEnabled())
					LOG.trace("Meiosis.doMeiosisCrossover(): " + crossover); //$NON-NLS-1$
			}
			final Gene<G> gene = genes.getGene(i);
			final Locus<G> locus = gene.getLocus();
			if (chromosome == null || locus.getChromosome().equals(chromosome)) {
				result.addGene(GeneFactory.makeHaploid(locus, doMeiosis(gene, index)));
			}
		}
		return result;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 983416199361743429L;

	private final double _crossoverProbability;
}
