/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Gene_Diploid.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.17 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Gene_;
import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Ploidy;

/**
 * Concrete class to model a diploid gene (locus).
 * 
 * @author Robin Hillyard
 * 
 *
 * @param <G>
 *            the type of the genetic information which is encoded in this Gene
 */
@Lifecycle(permanent = false)
public class Gene_Diploid<G> extends Gene_<G> {

	/**
	 * Public constructor to create a diploid gene with allele0 in slot 0 and
	 * allele1 in slot 1.
	 * 
	 * Applications should use
	 * {@link GeneFactory#makeDiploid(Locus, String, String)} rather than use
	 * this constructor directly.
	 * 
	 * @param allele1
	 *            the key to the first allele
	 * @param allele2
	 *            the key to the second allele
	 */
	public Gene_Diploid(final String allele1, final String allele2) {
		super(Ploidy.DIPLOID);
		setAllele(0, allele1);
		setAllele(1, allele2);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8352674392023629880L;

}
