/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 20, 2003
 *
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Mating_;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Nuclear;

import org.apache.commons.math.random.RandomGenerator;

/**
 * <p>
 * This is the default implementation class for the {@link Mating} interface
 * </p>
 * <p>
 * This class is named in honor of <a
 * href="http://en.wikipedia.org/wiki/J._B._S._Haldane">J. B. S. Haldane</a>
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.3 $
 */
@Lifecycle(permanent = true)
public final class Mating_Haldanian extends Mating_ {

	/**
	 * Primary constructor.
	 * 
	 * @param parent1
	 * @param parent2
	 * @param random
	 * @param genomic
	 */
	public Mating_Haldanian(final Nuclear parent1, final Nuclear parent2, final RandomGenerator random, final Genomic genomic) {
		super(parent1, parent2, random, genomic);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6440874008803318588L;

}
