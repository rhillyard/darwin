/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Chromosome_Sex.java
 * Created on Jun 10, 2009
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.api.impl;

import java.util.ArrayList;
import java.util.Collection;

import net.sf.darwin.api.base.Chromosome_;
import net.sf.darwin.core.Chromosome;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Locus;
import net.sf.tostring0.ToString;

import org.apache.commons.math.random.RandomGenerator;

/**
 * This type implements the {@link Chromosome} interface with a sex-linked
 * chromosome.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public final class Chromosome_Sex extends Chromosome_<Boolean> {

	/**
	 * @param identifier
	 */
	public Chromosome_Sex(final RandomGenerator random) {
		this(random, Genomic.STANDARD_ALPHABET);
	}

	/**
	 * @param random
	 *            XXX
	 * @param alphabet
	 *            TODO
	 * @param genomic
	 *            TODO
	 */
	public Chromosome_Sex(final RandomGenerator random, final String alphabet) {
		super(Chromosome.SEX, alphabet, true);
		final Locus<Boolean> locus = new Locus_Sex(random);
		this._locus = locus;
		try {
			addLocus(locus);
		} catch (final GeneticsException e) {
			throw new DarwinException("Chromosome_Sex() exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @return the sex locus.
	 * 
	 *         XXX consider making this method part of an interface
	 */
	@ToString(omit = true)
	public Locus<Boolean> getSexLocus() {
		return this._locus;
	}

	/**
	 * Clear all but the sex locus.
	 * 
	 * @see com.rubecula.darwin.domain.genetics.Chromosome_#clear()
	 */
	@Override
	protected void clear() {
		final Collection<Locus<Boolean>> toKeep = new ArrayList<>();
		toKeep.add(getSexLocus());
		retainAll(toKeep);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1464670876390051541L;

	/**
	 * Convenient way of remembering the sex locus (in case the order of loci
	 * changes).
	 */
	private final Locus<Boolean> _locus;

}
