/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: Serializer
 * Created on: Mar 14, 2012
 */

package net.sf.darwin.api.base;

import java.io.Writer;
import java.util.List;

/**
 * @author rhillya
 * 
 */
public interface Serializer {

	public abstract String deepSerialize(final Object target, final StringBuilder out);

	public abstract void deepSerialize(final Object target, final Writer out);

	public abstract String serialize(Object target);

	public abstract void serialize(Object target, Writer writer);

	public abstract void setExcludes(final List<String> arg0);

	public abstract void setIncludes(final List<String> arg0);
}
