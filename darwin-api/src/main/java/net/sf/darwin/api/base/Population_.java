/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Population_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.85 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.darwin.api.impl.Sink_Log;
import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.Realm;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.core.Visualizable;
import net.sf.tostring0.Detail;
import net.sf.tostring0.ToString;
import net.sf.tostring0.ToStringUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract class defining the base methods and fields for an instance of the
 * {@link Population} interface.
 * 
 * @author Robin Hillyard
 * 
 * @param <E>
 *            the type of the information in the environment which is inhabited
 *            by this population
 * @param <P>
 *            the type of the phenotypic information which is expressed by this
 *            population
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            population
 */
@ToString(allBeanMethods = false)
public abstract class Population_<E, P, G> extends Evolvable_ implements Population<E, P, G>, Visualizable {

	/**
	 * (Protected) constructor for Population_ where:
	 * 
	 * @param identifier
	 *            is the string which will identify this population;
	 */
	protected Population_(final String identifier) {
		super(identifier);
		this._index = -1;
		this._colonies = new ArrayList<>();
		// TODO do this properly
		this._sink = new Sink_Log();
		init();
	}

	/**
	 * @param colony
	 * @return result of calling
	 * @see java.util.List#add(java.lang.Object) on {@link #getColonyList()}.
	 */
	@Override
	public synchronized int addColony(final Colony<E, P, G> colony) {
		colony.setPopulation(this);
		final boolean add = getColonyList().add(colony);
		final int indexOf = getColonyList().indexOf(colony);
		colony.setIndex(indexOf);
		return add ? indexOf : -1;
	}

	/**
	 * TODO perhaps census the colonies too? or is that taken care of with
	 * children mechanism?
	 * 
	 * @param census
	 *            the census object
	 * @param context
	 * @return true if we should continue censusing the children of this object
	 */
	@Override
	public boolean censusMe(final Census census, final Object context) {
		String result = "Population" + S_COLON + getIdentifier();//$NON-NLS-1$
		result += " and population: " + getCount(); //$NON-NLS-1$
		census.present(result, context);
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Evolvable#cullMembers()
	 */
	@Override
	public void cullMembers() {
		for (final Colony<E, P, G> colony : getColonies())
			colony.cullMembers();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Population_<?, ?, ?> other = (Population_<?, ?, ?>) obj;
		if (getColonyList() == null) {
			if (other.getColonyList() != null)
				return false;
		} else if (!getColonyList().equals(other.getColonyList()))
			return false;
		return true;
	}

	/**
	 * @see net.sf.darwin.core.Censusible#getCensusibleChildren()
	 */
	@Override
	public Collection<? extends Censusible> getCensusibleChildren() {
		return getColonies();
	}

	/**
	 * @return the colonies
	 */
	@Override
	public Collection<Colony<E, P, G>> getColonies() {
		return getColonyList();
	}

	/**
	 * @see net.sf.darwin.core.Population#getColony(int)
	 */
	@Override
	public synchronized Colony<E, P, G> getColony(final int index) {
		if (index < 0)
			throw new RuntimeException("logic error: index may not be negative"); //$NON-NLS-1$
		if (index < getColonyList().size())
			return getColonyList().get(index);
		throw new DarwinException("population " + getIdentifier() + //$NON-NLS-1$
				" has insufficient colonies for index " + index); //$NON-NLS-1$
	}

	/**
	 * @return the number of Colonies in this population.
	 * @see net.sf.darwin.core.Countable#getCount()
	 */
	@Override
	public int getCount() {
		return getColonies().size();
	}

	/**
	 * @see net.sf.darwin.core.Evolvable#getGeneration()
	 */
	@Override
	public int getGeneration() {
		return getSequence();
	}

	/**
	 * TODO is this ever actually called? Maybe we don't need this method for
	 * Population.
	 * 
	 * @see net.sf.darwin.core.Visualizable#getIndividuals()
	 */
	@Override
	public Collection<Individual> getIndividuals() {
		final Collection<Individual> result = new ArrayList<>();
		for (final Colony<E, P, G> colony : getColonies())
			result.addAll(colony.getIndividuals());
		return result;
	}

	/**
	 * @see net.sf.darwin.core.Visualizable#getRealm()
	 */
	@Override
	public Realm getRealm() {
		if (getTaxon() != null)
			return getTaxon().getRealm();
		return null;
	}

	/**
	 * TODO consider eliminating this and using only the generation from
	 * super-type
	 * 
	 * @return {@link #sequence}
	 */
	@Override
	public int getSequence() {
		return this.sequence;
	}

	/**
	 * @return {@link #_sink}
	 * @see net.sf.darwin.core.Visualizable#getSink()
	 */
	@Override
	public Sink getSink() {
		return this._sink;
	}

	/**
	 * @return {@link #taxon}
	 * @see net.sf.darwin.core.Population#getTaxon()
	 */
	@Override
	public Taxon<E, P, G> getTaxon() {
		return this.taxon;
	}

	/**
	 * @return the total number of organisms in all colonies of this population
	 */
	@Override
	public int getTotal() {
		int result = 0;
		for (final Colony<E, P, G> colony : getColonies())
			result += colony.getCount();
		return result;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getColonyList() == null) ? 0 : getColonyList().hashCode());
		return result;
	}

	/**
	 * This implementation simply returns the value of the finished field.
	 * 
	 * @return {@link #finished}
	 * @see net.sf.darwin.core.Evolvable#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return this.finished;
	}

	/**
	 * <p>
	 * Method to get a new generation of this population. Some individuals will
	 * be culled (according to the mortality implementation, etc.); while each
	 * mating pair will produce some progeny.
	 * </p>
	 * 
	 * <p>
	 * Normally, this method will not be overridden by sub-classes because it
	 * contains critical logic. If you want to do additional logic, you would
	 * normally override one or more of the following methods:
	 * <ul>
	 * <li>{@link #preGenerationPreparation()}</li>
	 * <li>{@link #midGenerationProcessing()}</li>
	 * <li>{@link #postGenerationCleanup()}</li>
	 * <li>{@link #isFinished()}</li>
	 * </ul>
	 * </p>
	 * <p>
	 * Here is the exact sequence of events for this method:
	 * <ol>
	 * <li>increment {@link #sequence}</li>
	 * <li>call {@link #preGenerationPreparation()}</li>
	 * <li>call {@link Colony#setupGeneration()} (once for each colony)</li>
	 * <li>call {@link #midGenerationProcessing()}</li>
	 * <li>call {@link Colony#cleanupGeneration() (once for each colony)}</li>
	 * <li>call {@link #postGenerationCleanup()}</li>
	 * <li>call {@link #isFinished()}</li>
	 * </ol>
	 * </p>
	 * 
	 * @return a value which is formed from anding together the results of
	 *         several of this method and with the complement of the value
	 *         returned by {@link #isFinished()}
	 * @see net.sf.darwin.core.Evolvable#nextGeneration()
	 */
	@Override
	final public boolean nextGeneration() {

		this.sequence++;

		boolean ok = true;

		preGenerationPreparation();

		for (final Colony<E, P, G> colony : getColonies())
			ok &= colony.setupGeneration();

		midGenerationProcessing();

		if (ok) {
			for (final Colony<E, P, G> colony : getColonies())
				colony.cleanupGeneration();
		}

		postGenerationCleanup();

		if (!ok)
			LOG.warn(MessageFormat.format("Population: {0}: not healthy", getIdentifier())); //$NON-NLS-1$

		final boolean done = isFinished();

		if (done)
			LOG.info(MessageFormat.format("Population: {0}: is finished", getIdentifier())); //$NON-NLS-1$

		return ok && !done;
	}

	/**
	 * @see net.sf.darwin.core.Evolvable#seedMembers()
	 */
	@Override
	public void seedMembers() {
		if (getColonies().isEmpty())
			throw new DarwinException("population " + getIdentifier() + " " + "has no colonies"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		for (final Colony<E, P, G> colony : getColonies())
			colony.seedMembers();
	}

	/**
	 * Update the value of {@link #_index}
	 * 
	 * @param index
	 *            the index to set
	 */
	@Override
	public void setIndex(final int index) {
		this._index = index;
	}

	/**
	 * Set the taxon to which this population belongs. This is normally set when
	 * the population is added to the taxon.
	 * 
	 * @param taxon
	 */
	@Override
	public void setTaxon(final Taxon<E, P, G> taxon) {
		this.taxon = taxon;
	}

	/**
	 * @see net.sf.tostring0.AToString#toString(boolean, boolean, boolean,
	 *      boolean, boolean, java.lang.String, boolean, int, int)
	 */
	@Override
	public String toString(final Detail detail) {
		if (detail.isShowDetail()) {
			String className = getClass().toString();
			final int lastPeriod = className.lastIndexOf('.');
			if (lastPeriod >= 0)
				className = className.substring(lastPeriod + 1);
			final String id = getIdentifier();
			return className + S_COLON + (id != null ? id : "unidentified"); //$NON-NLS-1$
		}
		return ToStringUtils.toString(this, ToStringUtils.createDefaultInternalDetail(detail));
	}

	/**
	 * Do nothing. If you want to do something when the evolution is finished,
	 * override this method.
	 * 
	 * @see net.sf.darwin.core.Population#wrapUp(String)
	 */
	@Override
	public void wrapUp(final String cause) {
		final Object result = determineResult();
		setResult(result);
		this.finished = true;
	}

	/**
	 * @return XXX
	 * 
	 */
	protected abstract Object determineResult();

	/**
	 * @return {@link #_colonies}
	 */
	protected List<Colony<E, P, G>> getColonyList() {
		return this._colonies;
	}

	/**
	 * 
	 */
	protected void midGenerationProcessing() {
		if (LOG.isDebugEnabled())
			LOG.debug("mid-generation processing: generation " + getGeneration()); //$NON-NLS-1$
		return;
	}

	/**
	 * 
	 */
	protected void postGenerationCleanup() {
		if (LOG.isDebugEnabled())
			LOG.debug("post-generation processing: generation " + getGeneration()); //$NON-NLS-1$
		return;
	}

	/**
	 * 
	 */
	protected void preGenerationPreparation() {
		if (LOG.isDebugEnabled())
			LOG.debug("pre-generation processing: generation " + getGeneration()); //$NON-NLS-1$
		return;
	}

	/**
	 * 
	 */
	private void init() {
		// do nothing
	}

	private static final long serialVersionUID = -4250662778602587498L;

	private final Sink _sink;

	/**
	 * A list of colonies.
	 * 
	 * TODO make it a map
	 */
	public final List<Colony<E, P, G>> _colonies;

	private transient int sequence;

	/**
	 * The logger for this class.
	 */
	public static final Log LOG = LogFactory.getLog(Population_.class);

	/**
	 * 
	 */
	public static final String S_COLON = ": "; //$NON-NLS-1$

	/**
	 * the index of this population within its taxon. TODO check to see if
	 * obsolete
	 */
	public transient int _index;

	/**
	 * The Taxon to which this population exists. This is customarily set when
	 * the population is added to the taxon.
	 */
	private transient Taxon<E, P, G> taxon;

	/**
	 * Set to true by {@link #wrapUp(String)}
	 */
	private transient boolean finished = false;

}