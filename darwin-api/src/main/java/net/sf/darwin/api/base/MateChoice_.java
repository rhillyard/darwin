/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MateChoice_.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.33 $
 */

package net.sf.darwin.api.base;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Vector;

import net.sf.darwin.api.factory.MateFactory;
import net.sf.darwin.api.impl.Attraction_Uniform;
import net.sf.darwin.api.impl.Lek_Standard;
import net.sf.darwin.api.impl.MatingFactory;
import net.sf.darwin.api.impl.Mating_Haldanian;
import net.sf.darwin.core.Attraction;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Lek;
import net.sf.darwin.core.Mate;
import net.sf.darwin.core.MateChoice;
import net.sf.darwin.core.Mating;
import net.sf.darwin.core.Organism;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.random.RandomGenerator;

/**
 * Abstract class defining the base methods of MateChoice.
 * 
 * @author Robin Hillyard
 */
@ToString(allBeanMethods = false)
public abstract class MateChoice_ implements MateChoice {

	/**
	 * protected constructor.
	 * 
	 * @param random
	 *            random number source.
	 * @param attraction
	 *            sexual selection evaluator (if null then Attraction_Uniform is
	 *            employed).
	 */
	protected MateChoice_(final RandomGenerator random, final Attraction attraction) {
		super();
		this.random = random;
		this._attraction = (attraction == null ? new Attraction_Uniform() : attraction);
	}

	/**
	 * Choose the best (male) mate for the given <code>female</code>, from among
	 * all of the organisms at the <code>lek</code>.
	 * 
	 * Typically, the lek is only a sample of available breeding males. If no
	 * mate can be found amongst the sample, we look at the remainder, in case
	 * we can find a mate among those. This is primarily an optimization.
	 * 
	 * @param female
	 * @param lek
	 *            a collection of organisms from which to choose a (male) mate
	 *            for the given female, (note that, currently, not all of the
	 *            organisms are male, some are competing females).
	 * @return an appropriate male (or null if none live up to expectations).
	 */
	@Override
	public <E, P, G> Organism<E, P, G> chooseMate(final Organism<E, P, G> female, final Lek<E, P, G> lek) {
		final Mate best = MateFactory.createNemo(getMinimumDesirability());
		if (best instanceof Candidate) {
			final Candidate currentBest = (Candidate) best;

			for (final Organism<E, P, G> candidate : lek) {
				currentBest.updateIfBetter(candidate.createMate(this, female));
			}

			if (best.getMale() == null && lek.isSample()) {
				for (final Organism<E, P, G> candidate : lek.getRemainder())
					currentBest.updateIfBetter(candidate.createMate(this, female));
			}

		}

		logMateChoice(female, best, getMinimumDesirability());

		return best.getMale();
	}

	/**
	 * First, we call {@link MateChoice#pairUp(Organism, Lek_Standard)} for the
	 * <code>female</code>, <code>lek</code> and the appropriate genomic. If
	 * this is not-null, we return it. Otherwise, we invoke
	 * {@link Colony#isIsolated()} and IF the result is false, AND if the
	 * alternative lek is not null and its colony is not the same as this
	 * colony, THEN we invoke {@link MateChoice#pairUp(Organism, Lek_Standard)}
	 * for the <code>female</code>, <code>alternativeLek</code> and the
	 * appropriate genomic. We return the result.
	 * 
	 * This method should only be called if the female organism is in fact
	 * female.
	 * 
	 * @param female
	 *            the organism for whom we require a mate.
	 * @param lek
	 *            the primary lek from which the female chooses a mate
	 * @param alternativeLek
	 *            the alternative lek from which the female chooses a mate (may
	 *            be null)
	 * @return a newly constructed {@link Mating} object, or null.
	 * @see net.sf.darwin.core.MateChoice#createPairBond(net.sf.darwin.core.Organism,
	 *      com.rubecula.darwin.domain.helper.Lek_Standard,
	 *      com.rubecula.darwin.domain.helper.Lek_Standard)
	 */
	@Override
	public <E, P, G> Mating createPairBond(final Organism<E, P, G> female, final Lek<E, P, G> lek,
			final Lek<E, P, G> alternativeLek) {
		Mating result = pairUp(female, lek);
		if (result == null) {
			if (alternativeLek != null) {
				result = pairUp(female, alternativeLek);
				if (result != null)
					LOG.info("exotic pair bond created: " + result); //$NON-NLS-1$
			}
			// else
			// LOG
			//						.warn("createPairBond: alternative population is null (typically, there is a single population in a sexual system and it is not isolated)."); //$NON-NLS-1$
		}
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final MateChoice_ other = (MateChoice_) obj;
		if (this.random == null) {
			if (other.random != null)
				return false;
		} else if (!this.random.equals(other.random))
			return false;
		return true;
	}

	/**
	 * Method to enumerate a set of mating pairs: females from the given set of
	 * organisms and males from either the <code>lek</code> or the
	 * <code>alternativeLek</code> .
	 * 
	 * Unlike the rest of the Darwin package, there IS an asymmetry here between
	 * the sexes. Every female organism is involved in exactly one mating per
	 * generation. However, males are chosen randomly and can therefore be
	 * involved in any number of matings in a given generation.
	 * 
	 * @param organisms
	 *            a collection of organisms from which we will take females and
	 *            pair them up.
	 * @param lek
	 * @param alternativeLek
	 * 
	 * @return a Vector of Mating objects.
	 */
	@Override
	public <E, P, G> Collection<Mating> findPairs(final Collection<Organism<E, P, G>> organisms, final Lek<E, P, G> lek,
			final Lek<E, P, G> alternativeLek) {
		final Collection<Mating> result = new Vector<>();
		for (final Organism<E, P, G> organism : organisms) {
			final Mating pairBond = organism.createPairBond(this, lek, alternativeLek);
			if (pairBond != null)
				result.add(pairBond);
		}
		return result;
	}

	/**
	 * Method to determine the desirability of a given male in the eyes, etc. of
	 * a given female.
	 * 
	 * @param female
	 *            the choosy female.
	 * @param male
	 *            the poor schmuck male.
	 * @return ((maleViability*9)+1) * 2^(2*random-1)
	 * @see net.sf.darwin.core.MateChoice#getDesirabilityIndex(net.sf.darwin.core.Organism,
	 *      net.sf.darwin.core.Organism)
	 */
	@Override
	public <E, P, G> double getDesirabilityIndex(final Organism<E, P, G> female, final Organism<E, P, G> male) {
		return getDesirability(male) * getAttraction(female, male);
	}

	/**
	 * @see net.sf.darwin.core.MateChoice#getLek(Colony)
	 */
	@Override
	public <E, P, G> Lek<E, P, G> getLek(final Colony<E, P, G> colony) {
		if (colony != null)
			return new Lek_Standard<>(colony, getRandom(), getSampleFraction());
		return null;
	}

	/**
	 * @return the random
	 */
	@Override
	@ToString
	public RandomGenerator getRandom() {
		return this.random;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.random == null) ? 0 : this.random.hashCode());
		return result;
	}

	/**
	 * @param female
	 * @param lek
	 *            a collection of organisms from which to choose a (male) mate
	 *            for the given female, (note that not all of the organisms are
	 *            male, some are competing females).
	 * @return a newly constructed {@link Mating_Haldanian} if a suitable male
	 *         was found, else null.
	 * 
	 *         TODO use {@link MatingFactory}
	 * 
	 * @see net.sf.darwin.core.MateChoice#pairUp(net.sf.darwin.core.Organism,
	 *      Lek_Standard)
	 */
	@Override
	public <E, P, G> Mating pairUp(final Organism<E, P, G> female, final Lek<E, P, G> lek) {
		final Organism<E, P, G> male = chooseMate(female, lek);
		if (male != null) {
			final Genomic<P, G> genomic = female.getColony().getPopulation().getTaxon().getGenomic();
			if (genomic.equals(male.getColony().getPopulation().getTaxon().getGenomic()))
				return MatingFactory.makeHaldanian(genomic, female, male, getRandom());
			// This exception should never be thrown but it doesn't hurt to
			// check
			throw new DarwinException("MateChoice_.pairUp(): incompatible genomics"); //$NON-NLS-1$
		}

		return null;
	}

	/**
	 * Reset the current random number generator.
	 * 
	 * TEST
	 * 
	 * @param random
	 *            random number generator.
	 * 
	 * @see net.sf.darwin.core.MateChoice#setRandom(org.apache.commons.math.random.RandomGenerator)
	 */
	@Override
	public void setRandom(final RandomGenerator random) {
		this.random = random;
	}

	/**
	 * @param sampleFraction
	 *            the sampleFraction to set
	 */
	public void setSampleFraction(final double sampleFraction) {
		this.sampleFraction = sampleFraction;
	}

	/**
	 * @param female
	 * @param male
	 * @return the value returned from applying
	 *         {@link #getAttraction(Organism, Organism)} to the result of
	 *         {@link #getAttractionEvaluator()}.
	 */
	protected <E, P, G> double getAttraction(final Organism<E, P, G> female, final Organism<E, P, G> male) {
		final Attraction attractionEvaluator = getAttractionEvaluator();
		// XXX Note that even if non-uniform, we still will
		// be getting non-sex parts of the phenotype unnecessarily here.
		double attraction = 1;
		if (!attractionEvaluator.isUniform())
			attraction = attractionEvaluator.getAttraction(female.getPhenotype().getSexTraits(), male.getPhenotype()
					.getSexTraits());
		return attraction;
	}

	@ToString
	protected Attraction getAttractionEvaluator() {
		return this._attraction;
	}

	/**
	 * @param male
	 * @return the desirability of this male based on viability, and a random
	 *         number <code>r</code>: <code>d = v * 2^(2r-1)
	 * </code> where v is 10 if viable, 1 otherwise.
	 */
	protected <E, P, G> double getDesirability(final Organism<E, P, G> male) {
		// formula is: ((maleViability*9)+1) * 2^(2*random-1)
		final int v = male.isViable() ? 10 : 1;
		final double r = getRandom().nextDouble();
		final double f = Math.pow(2, 2 * r - 1); // this will be in the range
		// 0.5 through 2
		final double desirability = f * v;
		if (LOG.isTraceEnabled())
			LOG.trace("MateChoice_.getDesirabilityIndex(): " + desirability); //$NON-NLS-1$
		return desirability;
	}

	/**
	 * @return the sampleFraction
	 */
	protected double getSampleFraction() {
		return this.sampleFraction;
	}

	/**
	 * This was formerly final, but now for dependency injection we need to
	 * allow it to be set.
	 */
	private RandomGenerator random;

	private double sampleFraction = 1;

	protected final Attraction _attraction;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(MateChoice_.class);

	/**
	 * @param female
	 * @param best
	 * @param minimumDesirability
	 *            XXX
	 */
	@SuppressWarnings("boxing")
	private static void logMateChoice(final Organism<?, ?, ?> female, final Mate best, final double minimumDesirability) {
		if (Population_.LOG.isDebugEnabled()) {
			Population_.LOG.debug(MessageFormat.format("mate choice: for {0} is: {1}", female, best.getMale())); //$NON-NLS-1$
			Population_.LOG.debug(MessageFormat.format(
					"mate choice: min des={0}, actual={1}", minimumDesirability, best.getDesirability())); //$NON-NLS-1$
		}
	}

}