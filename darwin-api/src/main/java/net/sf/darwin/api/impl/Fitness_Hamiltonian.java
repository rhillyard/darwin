/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Fitness_Hamiltonian.java
 * Created on Nov 4, 2009
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.api.impl;

import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.base.Fitness_;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Fitness;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.FunctionListener;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;

/**
 * This is a standard implementation of {@link Fitness} well suited to
 * configuration via XML and dependency injection.
 * 
 * It is however very basic and is not normally used where a fitness function is
 * complex, for example in the peppered moth application.
 * 
 * In order for this implementation to work properly, the weights must be set by
 * calling the method {@link #putWeighting(String, EcoFactor)} with the
 * appropriate pair of values: a String which corresponds to the key
 * (identifier) of a phenotypic character; and an EcoFactor against which traits
 * of the character will be measured for fitness. <br>
 * This concrete class is named in honor of <a
 * href="http://en.wikipedia.org/wiki/W.D._Hamilton">W. D. Hamilton</a>,
 * although I don't claim that the specifics of the class's behavior have
 * anything to do with inclusive fitness.
 * 
 * @author Robin Hillyard
 * 
 */
public final class Fitness_Hamiltonian extends Fitness_ {

	/**
	 * 
	 */
	public Fitness_Hamiltonian() {
		super();
		this._weightings = new HashMap<String, EcoFactor>();
	}

	/**
	 * @param fitnessFunction
	 * @param functionListener
	 */
	public Fitness_Hamiltonian(final FitnessFunction fitnessFunction, final FunctionListener functionListener) {
		super(fitnessFunction, functionListener);
		this._weightings = new HashMap<String, EcoFactor>();
	}

	/**
	 * @throws FitnessException
	 * @see net.sf.darwin.core.Fitness#getFitness(net.sf.darwin.core.Trait,
	 *      net.sf.darwin.core.EcoFactor)
	 */
	@Override
	public double getFitness(final Trait trait, final EcoFactor factor) throws FitnessException {
		final Variant variant = trait.getVariant();
		// TODO consider doing this via polymorphism
		if (variant instanceof Quantifiable) {
			final double variantValue = ((Quantifiable) variant).doubleValue();
			if (factor instanceof Quantifiable) {
				final double factorValue = ((Quantifiable) factor).doubleValue();
				return calculateFitness(variantValue, factorValue, trait.getVariantKey(), factor.getIdentifier());
			}
			return 0;
		}
		return 0;
	}

	/**
	 * @see net.sf.darwin.core.Fitness#getWeight(String,
	 *      net.sf.darwin.core.EcoFactor)
	 */
	@Override
	public int getWeight(final String character, final EcoFactor factor) {
		if (getWeightings().get(character) == factor)
			return 1;
		return 0;
	}

	/**
	 * @param character
	 *            the key (identifier) for the character whose traits will be
	 *            measured for fitness against factor.
	 * @param factor
	 * @return the previous value corresponding to this key.
	 * @see java.util.HashMap#put(java.lang.Object, java.lang.Object)
	 */
	public EcoFactor putWeighting(final String character, final EcoFactor factor) {
		return getWeightings().put(character, factor);
	}

	/**
	 * @param bandwidth
	 *            the bandwidth to set
	 */
	public void setBandwidth(final double bandwidth) {
		this.bandwidth = bandwidth;
	}

	/**
	 * @return the value of {@link #bandwidth}.
	 * @see net.sf.darwin.api.base.Fitness_#bandwidth(java.lang.String)
	 */
	@Override
	protected double bandwidth(final String key) {
		return this.bandwidth;
	}

	/**
	 * @return the weightings
	 */
	private Map<String, EcoFactor> getWeightings() {
		return this._weightings;
	}

	/**
	 * by default 0.25
	 */
	private double bandwidth = 0.25;

	private final transient Map<String, EcoFactor> _weightings;

}
