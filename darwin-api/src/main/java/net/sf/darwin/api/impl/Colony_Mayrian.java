/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Colony_Mayrian.java
 * Created on Jan 6, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.api.impl;

import net.sf.darwin.api.base.Colony_;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Default implementation of a Colony.
 * 
 * Named in honor of <a href="http://en.wikipedia.org/wiki/Ernst_W._Mayr">Ernst
 * Mayr</a>.
 * 
 * @author Robin Hillyard
 * 
 */
final public class Colony_Mayrian<E, P, G> extends Colony_<E, P, G> {

	/**
	 * @param identifier
	 * @param environment
	 * @param random
	 */
	public Colony_Mayrian(final String identifier, final Environment<E> environment, final RandomGenerator random) {
		super(identifier, environment, random);
	}

	/**
	 * @return a suitable name for a daughter colony.
	 */
	@Override
	protected String getDaughterId() {
		String identifier = getIdentifier();
		if (identifier.startsWith("founding ")) //$NON-NLS-1$
			identifier = identifier.substring(9);
		final int daughters = getNDaughters();
		String result = ""; //$NON-NLS-1$
		int maxDaughters = 1;
		int s = getDaughterSequence();
		if (s < daughters) {
			while (maxDaughters < daughters) {
				maxDaughters *= ALPHABET.length();
				final int r = s % maxDaughters;
				result = ALPHABET.charAt(r) + result;
				s += -r;
			}
			return identifier + "-" + result; //$NON-NLS-1$
		}
		// If this exception is thrown, we need to set a greater value for
		// setNDaughters()
		throw new DarwinException("too many daughters for colony: " + this); //$NON-NLS-1$
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8421211902273325429L;

	/**
	 * 
	 */
	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_$"; //$NON-NLS-1$

}
