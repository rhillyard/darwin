/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolvable_.java
 * Created on Jul 22, 2009
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Evolvable;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class Evolvable_ extends Base implements Evolvable {

	/**
	 * Primary protected constructor.
	 * 
	 * @param identifier
	 */
	protected Evolvable_(final String identifier) {
		this.maxGenerations = Integer.MAX_VALUE;
		this._identifier = identifier;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Evolvable_ other = (Evolvable_) obj;
		if (getIdentifier() == null) {
			if (other.getIdentifier() != null)
				return false;
		} else if (!getIdentifier().equals(other.getIdentifier()))
			return false;
		return true;
	}

	/**
	 * @return {@link #generation}
	 * @see net.sf.darwin.core.Evolvable#getGeneration()
	 */
	@Override
	public int getGeneration() {
		return this.generation;
	}

	/**
	 * @return {@link #_identifier}
	 * @see net.sf.tostring0.Identifiable#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @return {@link #result}
	 */
	@Override
	public Object getResult() {
		return this.result;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 1;
		hash = prime * hash + ((getIdentifier() == null) ? 0 : getIdentifier().hashCode());
		return hash;
	}

	/**
	 * @return true if generation is &gt;= to {@link #maxGenerations}.
	 * 
	 *         For a more complex condition, an evolvable could override this
	 *         definition.
	 * 
	 * @see net.sf.darwin.core.Evolvable#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return getGeneration() >= getMaxGenerations();
	}

	/**
	 * increment {@link #generation} and
	 * 
	 * @return true
	 * @see net.sf.darwin.core.Evolvable#nextGeneration()
	 */
	@Override
	public boolean nextGeneration() {
		this.generation++;
		return true;
	}

	/**
	 * set {@link #maxGenerations} to
	 * 
	 * @param maxGenerations
	 */
	public void setMaxGenerations(final int maxGenerations) {
		this.maxGenerations = maxGenerations;
	}

	/**
	 * @return {@link #maxGenerations}
	 */
	protected int getMaxGenerations() {
		return this.maxGenerations;
	}

	/**
	 * set {@link #result} to
	 * 
	 * @param result
	 *            the result to set
	 */
	protected void setResult(final Object result) {
		this.result = result;
	}

	private static final long serialVersionUID = -1139837308253470588L;

	private final String _identifier;

	/**
	 * defaults to: Integer.MAX_VALUE, i.e. 2147483647.
	 */
	private transient int maxGenerations;

	/**
	 * starts at: 0
	 */
	private transient int generation = 0;

	private transient Object result;

}
