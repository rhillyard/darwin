/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Mate.java
 * Created on Nov 4, 2009 (formerly was Mate.java)
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.api.base;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Mate;
import net.sf.darwin.core.Organism;
import net.sf.tostring0.ToString;

/**
 * Class to model a potential (male) mate. Objects of this class are used to
 * choose the best male as a mate for a female.
 * 
 * @author Robin Hillyard
 * 
 */
@ToString(allBeanMethods = false)
public class Mate_ extends Base implements Mate {

	/**
	 * Package-scoped constructor (we discourage outsiders invoking the
	 * constructor directly).
	 * 
	 * @param male
	 *            XXX
	 * @param desirability
	 *            XXX
	 * 
	 */
	protected Mate_(final Organism male, final double desirability) {
		super();
		this.update(male, desirability);
	}

	/**
	 * @see net.sf.darwin.core.Mate#getDesirability()
	 */
	@Override
	@ToString
	public double getDesirability() {
		return this._desirability;
	}

	/**
	 * @see net.sf.darwin.core.Mate#getMale()
	 */
	@Override
	@ToString
	public Organism getMale() {
		return this._male;
	}

	/**
	 * @param male
	 * @param desirability
	 * @return true
	 */
	protected boolean update(final Organism male, final double desirability) {
		this._male = male;
		this._desirability = desirability;
		return true;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1135064346350961466L;

	private Organism _male;

	private double _desirability;

}
