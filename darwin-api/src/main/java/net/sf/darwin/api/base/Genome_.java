/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Created on Oct 5, 2003
 *
 */

package net.sf.darwin.api.base;

import net.sf.darwin.api.factory.GeneFactory;
import net.sf.darwin.api.factory.GenomeFactory;
import net.sf.darwin.api.factory.PhenotypeFactory;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Expresser;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.GeneticsException;
import net.sf.darwin.core.Genome;
import net.sf.darwin.core.Genomic;
import net.sf.darwin.core.Genotype;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Meiosis;
import net.sf.darwin.core.Mutator;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.PhenotypeCache;
import net.sf.darwin.core.Ploidy;
import net.sf.darwin.core.Visualizable;
import net.sf.tostring0.ToString;

/**
 * <p>
 * This abstract class provides base definitions for implementers of
 * {@link Genome}, the genetic information contained in an Organism belonging to
 * a Taxon. A Genome may contain any number of Genes, though in practice the
 * number is determined by the Genomic which defines this Genome. The structure
 * of Genome is approximately parallel to that of {@link Genomic}. <br>
 * TODO Genome_ extends Genotype_ and many of the uses of Genome_ could actually
 * be narrowed to use Genotype_ instead, especially if fertilization and
 * mutation were done at the Genotype level.
 * </p>
 * 
 * @author Robin Hillyard
 * @version $Revision: 1.53 $
 * 
 * @param <P>
 *            the type of the phenotypic information which is encoded in this
 *            Genotype
 * @param <G>
 *            the type of the genetic information which is encoded in this
 *            Genotype
 */
@SuppressWarnings("serial")
@ToString(detail = 1, allBeanMethods = false)
public abstract class Genome_<P, G> extends Genotype_<P, G> implements Genome<P, G> {

	/**
	 * @param genomic
	 * @param ploidy
	 */
	protected Genome_(final Genomic<P, G> genomic, final int ploidy) {
		super(ploidy);
		this._genomic = genomic;
		final int gPloidy = genomic.getPloidy();
		initSex();
		if (ploidy > gPloidy || ploidy < 1)
			throw new DarwinException("Genome_: constructor: ploidy value " + ploidy + " exceeds genomic ploidy: " //$NON-NLS-1$ //$NON-NLS-2$
					+ gPloidy + " or is non-positive"); //$NON-NLS-1$
	}

	/**
	 * Flush the cached phenotype for this genome/population combination. This
	 * must be called for any significant population change, or change to the
	 * population's environment. A significant change is one that would result
	 * in a different phenotype being returned.
	 * 
	 * @param visualizable
	 *            the population which has changed.
	 * 
	 * @see net.sf.darwin.core.Genome#environmentChanged(Visualizable)
	 */
	@Override
	public void environmentChanged(final Visualizable visualizable) {
		visualizable.getRealm().getPhenotypeCache().flush(this, visualizable);
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Genome_<?, ?> other = (Genome_<?, ?>) obj;
		if (getGenomic() == null) {
			if (other._genomic != null)
				return false;
		} else if (!getGenomic().equals(other._genomic))
			return false;
		return true;
	}

	/**
	 * Method to create a new phenotype from a collection of genes by expressing
	 * each and every gene, one at a time, using <code>this</code>
	 * {@link Expresser}.
	 * 
	 * This method may be overwritten by implementers of {@link Expresser} but
	 * that would be unusual. An example of why you might want to overwrite it
	 * would be to allow the expression of several genes together or to express
	 * more than one trait from any one gene.
	 * 
	 * The phenotype for any particular population is cached. If the population
	 * changes in some significant way, for example a property of its
	 * environment changes, the method environmentChanged() should be called.
	 * 
	 * @param colony
	 *            even though generally ignored, it must be a valid population,
	 *            with an environment that in turns has an environment factory.
	 * 
	 * @return a Dawkinsian Phenotype filled with the expressed traits.
	 * @see Expresser#express(Colony, Gene[])
	 */
	@Override
	public <E> Phenotype<P> express(final Colony<E, P, G> colony) {
		final PhenotypeCache<P> phenotypeCache = colony != null ? colony.getRealm().getPhenotypeCache() : null;
		Phenotype<P> phenotype = phenotypeCache != null ? phenotypeCache.getPhenotype(this, colony) : null;

		if (phenotype == null) {
			phenotype = PhenotypeFactory.makeDawkinsian();
			getGenomic().express(phenotype, colony, this);
			if (phenotype.size() == 0)
				LOG.warn("phenotype is empty"); //$NON-NLS-1$
			if (phenotypeCache != null)
				phenotypeCache.addPhenotype(this, colony, phenotype);
		}
		return phenotype;
	}

	/**
	 * CONSIDER making this part of {@link Meiosis} because really it is Meiosis
	 * stage 2.
	 * 
	 * Method to fuse this haploid genome with <code>other</code> genome into a
	 * new diploid genome. In theory, at least, there is no implied ordering of
	 * the two gametes. The resulting genome should express as a phenotype
	 * regardless of the ordering of the arguments x and y. At meiosis, the
	 * ordering of the parents x, y is semi-significant - but the effect should
	 * be masked by the seeding of the random number generator for a mating.
	 * 
	 * This and the other gamete must conform to the same Genomic, otherwise, no
	 * fertilization can take place.
	 * 
	 * TODO in theory, we could allow this and two other Genes parameters to
	 * combine into a triploid result.
	 * 
	 * XXX consider pulling this up into Genotype_.
	 * 
	 * @param a
	 *            the gamete to be fused with this gamete
	 * @return the resulting genome.
	 * 
	 */
	@Override
	public void fertilize(final Genes<G> a, final Genes<G> b) {
		if (getPloidy() == Ploidy.DIPLOID && a.getPloidy() == b.getPloidy() && a.getPloidy() == 1) {
			if (a.size() == b.size()) {
				final Genomic<P, G> genomic = getGenomic();
				for (int pos = 0; pos < a.size(); pos++) {
					final Gene<G> geneA = a.getGene(pos);
					final Gene<G> geneB = b.getGene(pos);
					final Locus<G> locus = geneA.getLocus();
					assert locus == geneB.getLocus() : "Genome_.makeGene(); loci are different"; //$NON-NLS-1$
					addGene(makeGene(locus, geneA, geneB));
				}
				if (genomic instanceof IsSexual) {
					final Gene<Boolean> sexGeneA = a.getSexGene();
					final Gene<Boolean> sexGeneB = b.getSexGene();
					if (sexGeneA != null && sexGeneB != null)
						try {
							setSexGene(makeGene(sexGeneA.getLocus(), sexGeneA, sexGeneB));
						} catch (final GeneticsException e) {
							// Shouldn't happen
							throw new RuntimeException("logic error", e); //$NON-NLS-1$
						}
					else
						throw new RuntimeException("logic error: cannot fertilize with asexual genes"); //$NON-NLS-1$
				}
			} else
				throw new DarwinException("Genome_.addGenes(): gametes have different lengths"); //$NON-NLS-1$
		} else
			throw new DarwinException("Genome_.fertilize(): incompatible ploidys"); //$NON-NLS-1$		
	}

	/**
	 * @return the gene whose locus has identifier which matches <code>id</code>
	 *         (ignoring case).
	 * @see net.sf.darwin.core.Genome#getGene(java.lang.String)
	 */
	@Override
	public Gene getGene(final String id) {
		for (final Gene gene : getList()) {
			final Locus locus = gene.getLocus();
			if (locus != null) {
				if (locus.getIdentifier().equalsIgnoreCase(id))
					return gene;
			} else
				throw new DarwinException("getGene(String): locus not set"); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * @return the genomic for this genome
	 * 
	 */
	@Override
	public Genomic<P, G> getGenomic() {
		return this._genomic;
	}

	/**
	 * @return the sex of this genome, expressed as a boolean (female is true if
	 *         I recall).
	 * @throws GeneticsException
	 *             if this Genome does not contain the sex gene, or it is
	 *             improperly formed
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean getSex() throws GeneticsException {
		// TODO consider doing this via polymorphism
		if (getGenomic() instanceof IsSexual) {
			final Genomic<Boolean, Boolean> sexualGenomic = ((IsSexual) getGenomic()).getSexGenomic();
			final Locus<Boolean> locusSex = sexualGenomic.getChromosome(0).getLocus(0);
			final Expresser<Boolean> expresser = sexualGenomic.getExpresser(locusSex);
			if (expresser != null)
				return expresser.express(getSexGene()).getValue().booleanValue();
			throw new DarwinException(METHOD_GET_SEX + ": no expresser set for sex locus"); //$NON-NLS-1$
		}
		throw new DarwinException(METHOD_GET_SEX + ": genomic is not sex-linked"); //$NON-NLS-1$
	}

	@Override
	public Gene<Boolean> getSexGene() {
		return this.sexGene;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getGenomic() == null) ? 0 : getGenomic().hashCode());
		return result;
	}

	/**
	 * This implementation of invert() assumes that the genome is haploid. The
	 * resulting genes object is a set of genes of the same length as this
	 * genome, but result(i) = complement(gene(N-1-i)) where N is the number of
	 * genes.
	 * 
	 * If you need a more complex inversion process, or for some reason, you are
	 * inverting non-haploid genomes, you will need to override this.
	 * 
	 * @return the inverted genome, as a Genes object.
	 * 
	 * @see net.sf.darwin.core.Genome#invert()
	 */
	@Override
	public Genes invert() {
		final Genome result = GenomeFactory.makeLinear(getGenomic(), getPloidy());
		final int which = 0; // assume that this genome is haploid.
		for (int i = which; i < size(); i++) {
			final Gene gene = getGene(i);
			final Locus locus = gene.getLocus();
			final Expresser expresser = getGenomic().getExpresser(locus);
			if (expresser != null) {
				result.getGenes()
				.add(0, GeneFactory.makeHaploid(locus, expresser.getComplement(locus, gene.getAlleleKey(which))));
			} else
				throw new DarwinException("Genome.invert(): no expresser for locus"); //$NON-NLS-1$
		}
		return result;
	}

	/**
	 * @return
	 */
	@Override
	public boolean isSexual() {
		return this._genomic instanceof IsSexual;
	}

	/**
	 * @param mutator
	 *            the mutator
	 * @return a distinct copy of <code>this</code> {@link Genome}, which may or
	 *         may not be mutated.
	 * 
	 * @see net.sf.darwin.core.Genome#mutate(net.sf.darwin.core.Mutator)
	 */
	@Override
	public Genome<P, G> mutate(final Mutator<G> mutator) {
		@SuppressWarnings("unchecked")
		final Genome<P, G> result = (Genome<P, G>) clone();

		if (result != null) {
			if (mutator.isIdentity())
				return result;

			for (int i = 0; i < result.size(); i++) {
				result.getGenes().set(i, result.getGenes().get(i).mutate(mutator));
			}
			return result;
		}
		throw new DarwinException("mutate(): clone failure"); //$NON-NLS-1$
	}

	public void setSexGene(final Gene<Boolean> gene) throws GeneticsException {
		if (isSexual())
			if (gene != null)
				this.sexGene = gene;
			else
				throw new GeneticsException("sex gene given is null"); //$NON-NLS-1$				
		else
			throw new GeneticsException("Genome is asexual"); //$NON-NLS-1$
	}

	/**
	 * @param genotype
	 * @param other
	 * @param pos
	 */
	private void addGeneAtPosition(final Genotype<P, G> genotype, final Genes<G> other, final int pos) {
		final Gene<G> geneX = getGene(pos);
		final Gene<G> geneY = other.getGene(pos);
		assert geneY.getPloidy() == Ploidy.HAPLOID : "other gamete is not haploid"; //$NON-NLS-1$
		final Locus<G> locus = geneX.getLocus();
		addGeneAtLocus(genotype, other, pos, geneX, geneY, locus);
	}

	/**
	 * @param genotype
	 * @param other
	 */
	@SuppressWarnings("unused")
	private void addGenes(final Genotype<P, G> genotype, final Genes<G> other) {
		if (size() == other.size()) {
			for (int pos = 0; pos < size(); pos++)
				addGeneAtPosition(genotype, other, pos);
		} else
			throw new DarwinException("Genome_.addGenes(): gametes have different lengths"); //$NON-NLS-1$
	}

	/**
	 */
	private void initSex() {
		if (isSexual()) {
			final Locus<Boolean> locus = ((IsSexual) this._genomic).getSexGenomic().getLocus(0);
			assert locus != null : "logic error: sex locus is null"; //$NON-NLS-1$
			//			this.sexGene=locus.makeGene("X","Y"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	private final Genomic<P, G> _genomic;

	/**
	 * 
	 */
	private static final String METHOD_GET_SEX = "Genome_.getSex()"; //$NON-NLS-1$

	@ToString
	private Gene<Boolean> sexGene;

	/**
	 * @param genotype
	 * @param other
	 * @param pos
	 * @param geneX
	 * @param geneY
	 * @param locus
	 */
	private static <U, W> void addGeneAtLocus(final Genotype<U, W> genotype, final Genes<W> other, final int pos,
			final Gene<W> geneX, final Gene<W> geneY, final Locus<W> locus) {
		if (locus != null)
			genotype.addGene(makeGene(other, pos, geneX, geneY, locus));
		else
			throw new DarwinException("Genome_.addGeneAtLocus(): gene locus not set"); //$NON-NLS-1$
	}

	/**
	 * @param other
	 * @param pos
	 * @param geneX
	 * @param geneY
	 * @param locus
	 * @return
	 */
	private static <W> Gene<W> makeGene(final Genes<W> other, final int pos, final Gene<W> geneX, final Gene<W> geneY,
			final Locus<W> locus) {
		assert locus == geneY.getLocus() : "Genome_.makeGene(); loci are different"; //$NON-NLS-1$
		return makeGene(locus, geneX, geneY);
	}

	/**
	 * CONSIDER Moving this to Locus_
	 * 
	 * @param locus
	 * @param geneX
	 * @param geneY
	 * @return
	 */
	private static <W> Gene<W> makeGene(final Locus<W> locus, final Gene<W> geneX, final Gene<W> geneY) {
		final String alleleX = geneX.getAlleleKey(0);
		final String alleleY = geneY.getAlleleKey(0);
		return locus.makeGene(alleleX, alleleY);
	}

	/**
	 * @param result
	 * @param other
	 */
	private static <U, W> void setSexGene(final Genome<U, W> result, final Genes<W> other) {
		if (other.getSexGene() != null)
			try {
				((Genome_<U, W>) result).setSexGene(other.getSexGene());
			} catch (final GeneticsException e) {
				throw new RuntimeException("logic error", e); //$NON-NLS-1$
			}
	}

}