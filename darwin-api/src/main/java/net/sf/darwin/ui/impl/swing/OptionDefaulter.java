/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionDefaulter
 * Created on: Mar 3, 2012
 */

package net.sf.darwin.ui.impl.swing;

import java.util.Map;

/**
 * TODO this interface is obsolete and should be eliminated
 * 
 * @author rhillya
 * 
 */
public interface OptionDefaulter {

	/**
	 * @param configurationClass
	 * @return a Map of option defaults
	 */
	public abstract Map<String, Object> createDefaultOptionMap(final String configurationClass);

}