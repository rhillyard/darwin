/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Painter.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.ui.base;

import java.awt.Graphics;

/**
 * This interface defines the methods which allow painting of specific
 * visualization model items. These methods are called the {@link Visualizer_}
 * wants to visualize the model(s).
 * 
 * @author Robin Hillyard
 */
public interface Painter {

	/**
	 * @return the identify
	 */
	public boolean isIdentify();

	/**
	 * Method to paint a representation of the organisms described by the
	 * {@link VisualizationModel} instance. This method is called by
	 * {@link Visualizer_#paintComponent(Graphics)} method, before any other
	 * painting is done.
	 * 
	 * @param g
	 *            a graphics context, typically a buffered image.
	 * @param width
	 *            the width of the area in which we may draw (0..width are legal
	 *            for x coordinates).
	 * @param height
	 *            the height of the area in which we may draw (0..height are
	 *            legal for y coordinates).
	 */
	public abstract void paintBase(Graphics g, int width, int height);

	/**
	 * Method to paint a representation of an organism. This method is called
	 * (indirectly) by {@link Visualizer_#paintComponent(Graphics)} method, once
	 * for each individual.
	 * 
	 * @param g
	 *            a graphics context, typically a buffered image.
	 * @param avatar
	 *            the individual from the population model to be painted.
	 */
	public abstract void paintIndividual(Graphics g, Avatar avatar);

	/**
	 * Method to paint visualization model <code>which</code>. This method is
	 * called (indirectly) by {@link Visualizer_#paintComponent(Graphics)}
	 * method, once for each model.
	 * 
	 * @param visualizationModel
	 * 
	 * @param g
	 * @param width
	 * @param height
	 */
	public abstract void paintVisualization(VisualizationModel visualizationModel, Graphics g, int width, int height);

	/**
	 * @param identify
	 *            the identify to set
	 */
	public void setIdentify(boolean identify);

	/**
	 * backgroundColor
	 */
	public static final String BACKGROUND_COLOR = "backgroundColor"; //$NON-NLS-1$
}
