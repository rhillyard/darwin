/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Title.java
 * Created on Aug 6, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.ui.impl.swing;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Robin Hillyard
 * 
 */
public class Title extends JPanel {

	/**
	 * @param opaque
	 * @param banner
	 *            the string to use for the fixed part of the title
	 * 
	 */
	public Title(final boolean opaque, final String banner) {
		this._label = new JLabel("Unknown Application"); //$NON-NLS-1$
		init(opaque, banner);
	}

	/**
	 * @param text
	 */
	public void setText(final String text) {
		getLabel().setText(text);
	}

	/**
	 * @see javax.swing.JComponent#setToolTipText(java.lang.String)
	 */
	@Override
	public void setToolTipText(final String text) {
		getLabel().setToolTipText(text);
	}

	/**
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return getLabel().getText();
	}

	/**
	 * @return {@link #_label}
	 */
	private JLabel getLabel() {
		return this._label;
	}

	/**
	 * @param opaque
	 * @param banner
	 */
	private void init(final boolean opaque, final String banner) {
		final JLabel title = new JLabel(banner);
		add(title);
		add(getLabel());
		setOpaque(opaque);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5314770301699190867L;

	private final JLabel _label;

}
