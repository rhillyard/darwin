/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EvolutionaryApplet.java
 * Created on Aug 5, 2009
 * @version $Revision: 1.38 $
 */

package net.sf.darwin.ui.impl.swing;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.Evolutionary;
import net.sf.darwin.api.base.OptionMap;
import net.sf.darwin.api.base.OptionSource;
import net.sf.darwin.api.base.OptionSource_Applet;
import net.sf.darwin.api.base.Timed;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.GenerationListener;
import net.sf.darwin.core.Taxon;
import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.VisualizationException;
import net.sf.darwin.ui.impl.ControlPanel_Default;
import net.sf.darwin.ui.impl.Messages;
import net.sf.darwin.ui.impl.VisualizerTabs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * This subclass of {@link JApplet} understands how to interact with an
 * {@link Evolution}, by virtue of implementing {@link GenerationListener} and
 * {@link ControlAction}.
 * </p>
 * 
 * <p>
 * It expects that the Evolution object has the property "ui" set to this applet
 * in the bean configuration file (otherwise, the field {@link #evolution} will
 * not be set when it needs to be).
 * </p>
 * 
 * <p>
 * The {@link #init()} method is marked as final (see comments) and thus all
 * preparatory UI work must be done in the {@link #setup()} method
 * implementations.
 * </p>
 * <p>
 * 
 * This applet has optional censusing capability.
 * </p>
 * 
 * <p>
 * This applet controls a Taxon evolutionary computation system. Essentially,
 * this applet defines a panel with border layout (N,E,S,W, center). The E/W
 * panels are unused. The N panel contains a title and the S panel contains a
 * control panel. The center panel delegates its functionality to an instance of
 * the {@link VisualizerTabs} class.
 * </p>
 * 
 * @author Robin Hillyard
 * 
 */
abstract public class EvolutionaryApplet extends JApplet implements GenerationListener, ControlAction, Evolutionary,
OptionDefaulter, OptionSource<String> {

	/**
	 * Instantiate the applet, retrieving the title for the applet from the
	 * message.properties file (property Applet_Darwinian.0)
	 * 
	 * @param beanContainer
	 *            XXX
	 * @throws HeadlessException
	 */
	protected EvolutionaryApplet(final BeanContainer beanContainer) throws HeadlessException {
		super();
		this._beanContainer = beanContainer;
		this._title = new Title(true, Messages.getString("Applet_Darwinian.0") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
		this._optionSource = new OptionSource_Applet(this);
		this._optionMap = new OptionMap_Applet(createDefaultOptionMap(this.getClass().getCanonicalName()), this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.ui.impl.swing.OptionDefaulter#createDefaultOptionMap(java.lang.String)
	 */
	@Override
	public Map<String, Object> createDefaultOptionMap(final String configurationClass) {
		final Map<String, Object> result = new HashMap<String, Object>();
		// result.put(BeanContainer.OPT_CONFIG_CLASS, configurationClass);
		result.put(BeanContainer.OPT_GENERATIONS, Integer.valueOf(0));
		return result;
	}

	/**
	 * shut down the evolution.
	 * 
	 * TODO we should also break down all of the user interface components.
	 * 
	 * @see java.applet.Applet#destroy()
	 */
	@Override
	public void destroy() {
		LOG.info("Destroying " + getTitle() + " applet"); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			if (getEvolution() != null && getEvolution().stoppable()) {
				final boolean ok = getEvolution().shutdown();
				if (LOG.isDebugEnabled())
					LOG.debug("destroy(): applet shut down evolution with result: " + ok); //$NON-NLS-1$
				else if (!ok)
					LOG.info("destroy(): applet shut down evolution but not cleanly."); //$NON-NLS-1$
			} else
				LOG.warn("destroy(): cannot shut down evolution"); //$NON-NLS-1$
		} catch (final EvolutionException e) {
			final String message = "Exception shutting down evolution";//$NON-NLS-1$
			// XXX should not need to warn.
			LOG.warn(message, e);
			throw new RuntimeException(message, e);
		}
		super.destroy();
	}

	/**
	 * @return {@link #evolution}
	 * @see com.rubecula.darwin.evolution.Evolutionary#getEvolution()
	 */
	@Override
	public Evolution getEvolution() {
		return this.evolution;
	}

	/**
	 * @return
	 * @see net.sf.darwin.api.base.OptionSource#getKeys()
	 */
	@Override
	public Collection<String> getKeys() {
		return this._optionSource.getKeys();
	}

	/**
	 * @param key
	 * @return
	 * @see net.sf.darwin.api.base.OptionSource#getOption(java.lang.String)
	 */
	@Override
	public String getOption(final String key) {
		return this._optionSource.getOption(key);
	}

	/**
	 * @param parameterName
	 * @param defaultValue
	 * @return a String which is either the parameter value specified in the
	 *         applet parameter list, or if that is missing, then the default
	 *         value.
	 */
	public String getParameter(final String parameterName, final String defaultValue) {
		final String value = getParameter(parameterName);
		return value != null ? value : defaultValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.applet.Applet#getParameterInfo()
	 */
	@Override
	public String[][] getParameterInfo() {
		return PARAMETER_INFO;
	}

	// /**
	// * {@inheritDoc}
	// *
	// * @see net.sf.darwin.api.base.OptionGetter#getOptions(java.util.Map)
	// */
	// @SuppressWarnings("boxing")
	// @Override
	// public OptionMap<String, Object> getOptions(final Map<String, Object>
	// defaults) {
	// try {
	// final OptionMap<String, Object> result = new OptionMap<String,
	// Object>(defaults, new OptionSource_Applet(this));
	// // FIXME should not need the rest of this method now.
	// final String optKey = BeanContainer.OPT_VALIDATE;
	// final String paramKey = APPLET_PARAM_VALIDATE;
	// updateOptionValue(result, optKey, paramKey);
	// result.put(BeanContainer.OPT_DTD, getParameter(APPLET_PARAM_DTD,
	// (String) defaults.get(BeanContainer.OPT_DTD)));
	// result.put(BeanContainer.OPT_CONFIG_FILE,
	// getParameter(APPLET_PARAM_BEANS, (String)
	// defaults.get(BeanContainer.OPT_CONFIG_FILE)));
	// result.put(BeanContainer.OPT_DEBUG,
	// getBooleanParameter(APPLET_PARAM_DEBUG, (Boolean)
	// defaults.get(BeanContainer.OPT_DEBUG)));
	// result.put(BeanContainer.OPT_SHOW_BEANS,
	// getBooleanParameter(APPLET_PARAM_SHOW_BEANS, (Boolean)
	// defaults.get(BeanContainer.OPT_SHOW_BEANS)));
	// return result;
	// } catch (final Exception e) {
	//			throw new RuntimeException("problem setting up applet options", e); //$NON-NLS-1$
	// }
	// }

	// /**
	// *
	// */
	// @SuppressWarnings("boxing")
	// protected Map<String, Object> getOptionsMap() {
	// final Map<String, Object> optionsMap = new HashMap<String, Object>();
	// final boolean validate = getBooleanParameter(APPLET_PARAM_VALIDATE,
	// false);
	// optionsMap.put(BeanContainer.OPT_VALIDATE, validate);
	// if (validate) {
	// final String dtd = getParameter(APPLET_PARAM_DTD);
	// if (dtd != null)
	// this._beanContainer.setValidate(true, new URL(dtd));
	// }
	// // set the filename for bean container configuration.
	// this._beanContainer.setConfigurationByResource(getClass(),
	// getParameter(APPLET_PARAM_BEANS, getDefaultBeansFilename()));
	//
	// // set the debug flag according to the applet parameter "debug"
	// this._beanContainer.setDebug(getBooleanParameter(APPLET_PARAM_DEBUG,
	// false));
	//
	// // this._beanContainer.doMain(this._beanContainer, new
	// // ArrayList<String>());
	//
	// // impose this as an external bean in the container.
	// this._beanContainer.imposeBean(BeanContainer.BEAN_APPLET, this);
	//
	// // Now we configure the bean container.
	// this._beanContainer.configure();
	//
	// if (getBooleanParameter(APPLET_PARAM_SHOW_BEANS, false))
	// this._beanContainer.showBeans();
	//
	// }

	/**
	 * <p>
	 * Final implementation of init(). This is final in order to avoid the
	 * inevitable confusion caused by overriding this method. All of the user
	 * interface work that would normally appear in the {@link #init()} method
	 * is actually defined in the {@link #setup()} method which is called from
	 * within this method (after the call to the super-method and the beans
	 * configuration).
	 * </p>
	 * 
	 * <p>
	 * Here is the sequence:
	 * <ul>
	 * <li>invoke super.init();</li>
	 * <li>invoke
	 * {@link BeanContainer#setConfigurationByResource(Class, String)};</li>
	 * <li>invoke {@link BeanContainer#imposeBean(String, Object)};</li>
	 * <li>invoke {@link BeanContainer#configure()};</li>
	 * <li>if appropriate, invoke {@link BeanContainer#showBeans()};</li>
	 * <li>invoke {@link #setup()};</li>
	 * <li>invoke {@link BeanContainer#runBeans()}.</li>
	 * </ul>
	 * </p>
	 * 
	 * <p>
	 * The beans used by this EvolutionaryApplet are instantiated and configured
	 * here (after the super.init() method is invoked). The reference to this
	 * {@link Applet} is typically dependency-injected into at least some of the
	 * beans. Note that the configuration includes all pre- and
	 * post-configuration processing.
	 * </p>
	 * 
	 * <p>
	 * The filename of the beans definition is provided by the "beans" applet
	 * parameter. If the "beans" parameter is unset, then the filename defaults
	 * to {@link #getDefaultBeansFilename()}. Assuming that the filename is a
	 * relative name, beans file should reside in the resources directory, under
	 * the package of the concrete class being instantiated (not typically in
	 * the package that this class resides in).
	 * </p>
	 * 
	 * @see java.applet.Applet#init()
	 */
	@Override
	public final void init() {
		// First invoke the super-method.
		super.init();

		this._optionMap.update(this._optionSource);

		try {
			// impose this as an external bean in the container.
			this._beanContainer.imposeBean(BeanContainer.BEAN_APPLET, this);

			// Now we configure the bean container.
			this._beanContainer.doMain(this._optionMap, false);

			// invoke setup.
			setup();

			// run the beans in the bean container.
			this._beanContainer.runBeans();

			LOG.info(getTitle() + " applet initialized"); //$NON-NLS-1$ 
		} catch (final Exception e) {
			throw new RuntimeException("EvolutionaryApplet.init(): exception setting up bean container", e); //$NON-NLS-1$
		}
	}

	/**
	 * @see net.sf.darwin.core.GenerationListener#onGeneration(net.sf.darwin.core.Evolvable)
	 */
	@Override
	public void onGeneration(final Evolvable evolvable) {
		if (evolvable != null)
			showStatus(evolvable.getIdentifier() + ": " + GENERATION + evolvable.getGeneration()); //$NON-NLS-1$
		else {
			showStatus("Evolution Complete"); //$NON-NLS-1$
		}
	}

	/**
	 * @param title
	 * 
	 * @see javax.swing.JLabel#setText(java.lang.String)
	 */
	public void setAppletTitle(final String title) {
		getTitle().setText(title);
	}

	/**
	 * @param controlPanel
	 *            the controlPanel to set
	 */
	public void setControlPanel(final ControlPanel_ controlPanel) {
		this.controlPanel = controlPanel;
	}

	/**
	 * @see com.rubecula.darwin.evolution.Evolutionary#setEvolution(net.sf.darwin.core.darwin.evolution.Evolution)
	 */
	@Override
	public void setEvolution(final Evolution evolution) {
		this.evolution = evolution;
	}

	/**
	 * Update the system according to a user-interface property change. If the
	 * property relates to the evolution process itself, we invoke
	 * {@link #changeEvolution(String, Object)}. Otherwise, we invoke
	 * {@link #updateModel(String, Object)} and then {@link #repaint()}.
	 * 
	 * @see net.sf.darwin.ui.base.ControlAction#setProperty(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public void setProperty(final String name, final Object value) throws VisualizationException {
		LOG.info("property update: " + name + "=" + value); //$NON-NLS-1$ //$NON-NLS-2$
		if (name.equalsIgnoreCase(ControlAction.TIME_DELAY))
			changeEvolution(name, value);
		else {
			updateModel(name, value);
			repaint();
		}
	}

	/**
	 * @see net.sf.darwin.ui.base.ControlAction#setState(java.lang.String)
	 */
	@Override
	public void setState(final String state) throws VisualizationException {
		try {
			if (state.equalsIgnoreCase(ControlAction.NEXT)) {
				getEvolution().next();
				doCensus();
			} else if (state.equalsIgnoreCase(ControlAction.START))
				getEvolution().resume();
			else if (state.equalsIgnoreCase(ControlAction.STOP)) {
				getEvolution().pause();
				doCensus();
			} else if (state.equalsIgnoreCase(ControlAction.QUIT)) {
				setEvolution(null);
				this.stop();
				this.destroy();
			}
		} catch (final EvolutionException e) {
			throw new VisualizationException("setState(): " + state + " exception", e); //$NON-NLS-1$//$NON-NLS-2$
		}
	}

	/**
	 * @see net.sf.darwin.ui.base.ControlAction#settable(java.lang.String)
	 */
	@Override
	public boolean settable(final String propertyName) {
		if (getEvolution() != null && propertyName.equalsIgnoreCase(ControlAction.TIME_DELAY))
			return getEvolution().stoppable();
		return false;
	}

	/**
	 * Initialize the evolution.
	 * 
	 * @see java.applet.Applet#start()
	 */
	@Override
	public void start() {
		super.start();
		if (getEvolution() != null) {
			getEvolution().init();
			LOG.info(getTitle() + " applet started"); //$NON-NLS-1$
		} else
			LOG.warn("start(): evolution is unset"); //$NON-NLS-1$
	}

	/**
	 * Cleanup the evolution
	 * 
	 * @see java.applet.Applet#stop()
	 */
	@Override
	public void stop() {
		super.stop();
		if (getEvolution() != null) {
			getEvolution().cleanup();
			LOG.info(getTitle() + " applet stopped"); //$NON-NLS-1$
		}
	}

	/**
	 * This is required for double-buffering to work correctly.
	 * 
	 * @see java.awt.Container#update(java.awt.Graphics)
	 */
	@Override
	public void update(final Graphics graphics) {
		paint(graphics);
	}

	/**
	 * 
	 */
	void doCensus() {
		if (getEvolution() != null)
			for (final Evolvable evolvable : getEvolution().getEvolvableKeys()) {
				// TODO consider doing this via polymorphism
				if (evolvable instanceof Taxon) {
					final Taxon taxon = (Taxon) evolvable;
					taxon.censusMe(taxon.getCensusTaker(), getName());
				}
				// else do nothing
			}
	}

	/**
	 * @param controlAction
	 *            the object to implement the control actions
	 * @return a newly created {@link ControlPanel_Default} object.
	 * 
	 *         TODO consider using factory class.
	 */
	@SuppressWarnings("static-method")
	protected JPanel createControlPanel(final ControlAction controlAction) {
		return new ControlPanel_Default(controlAction);
	}

	/**
	 * @return a new empty {@link JLabel}
	 */
	@SuppressWarnings("static-method")
	protected JLabel createEastPanel() {
		return new JLabel(EMPTY);
	}

	/**
	 * @param opaque
	 *            true if this panel should be opaque
	 * @return a the {@link #_title} with opaque property set accordingly.
	 */
	protected JPanel createNorthPanel(final boolean opaque) {
		getTitle().setOpaque(opaque);
		return getTitle();
	}

	/**
	 * Needs to be overridden by extenders.
	 * 
	 * @return the newly fabricated options component map
	 */
	protected abstract Map<String, Component> createOptionsComponentMap();

	/**
	 * @return {@link #getControlPanel()}.
	 * 
	 */
	protected JPanel createSouthPanel() {
		return getControlPanel();
	}

	/**
	 * @return a new empty {@link JLabel}
	 */
	@SuppressWarnings("static-method")
	protected JLabel createWestPanel() {
		return new JLabel(EMPTY);
	}

	/**
	 * @return the beanContainer
	 */
	protected BeanContainer getBeanContainer() {
		return this._beanContainer;
	}

	/**
	 * @param parameterName
	 * @param defaultValue
	 * @return the value of the applet parameter specified or the default value.
	 * 
	 * @see #getParameter(String, String)
	 */
	protected boolean getBooleanParameter(final String parameterName, final boolean defaultValue) {
		return Boolean.valueOf(getParameter(parameterName, Boolean.valueOf(defaultValue).toString())).booleanValue();
	}

	/**
	 * @return the controlPanel
	 */
	protected JPanel getControlPanel() {
		return this.controlPanel;
	}

	/**
	 * TODO eliminate
	 * 
	 * Subclasses should override this as appropriate.
	 * 
	 * @return {@link #FILENAME_BEANS_XML}.
	 */
	@SuppressWarnings("static-method")
	protected String getDefaultBeansFilename() {
		return FILENAME_BEANS_XML;
	}

	/**
	 * @return {@link #_title}
	 */
	protected Title getTitle() {
		return this._title;
	}

	/**
	 * Method which is called by invoked by {@link #init()}, after invoking the
	 * super-method.
	 */
	abstract protected void setup();

	/**
	 * 
	 */
	protected void setupLayout() {
		setLayout(new BorderLayout());
		add(createNorthPanel(true), BorderLayout.NORTH);
		add(createWestPanel(), BorderLayout.WEST);
		add(createEastPanel(), BorderLayout.EAST);
		add(createSouthPanel(), BorderLayout.SOUTH);
	}

	protected abstract void updateModel(final String name, final Object value) throws VisualizationException;

	/**
	 * @param name
	 * @param value
	 * @throws VisualizationException
	 */
	private void changeEvolution(final String name, final Object value) throws VisualizationException {
		final int rate = (int) Math.round(RATE_DETERMINANT / ((Number) value).doubleValue());
		try {
			// TODO consider doing this via polymorphism
			if (getEvolution() instanceof Timed)
				((Timed) getEvolution()).setRate(rate);
			else
				throw new VisualizationException("setProperty(): " + name + " evolution is does not support variable rate"); //$NON-NLS-1$//$NON-NLS-2$
		} catch (final EvolutionException e) {
			throw new VisualizationException("setProperty(): " + name + " exception while shutting down evolution", e); //$NON-NLS-1$//$NON-NLS-2$
		}
	}

	private static final String APPLET_PARAM_GEN = "generations"; //$NON-NLS-1$

	private final BeanContainer _beanContainer;

	/**
	 * The number of milliseconds in one minute
	 */
	private static final int RATE_DETERMINANT = 60000;

	/**
	 * validate parameter name (defaults to false).
	 */
	private static final String APPLET_PARAM_VALIDATE = "validate"; //$NON-NLS-1$

	/**
	 * the parameter to specify a specific URL for validation.
	 */
	private static final String APPLET_PARAM_DTD = "dtd"; //$NON-NLS-1$

	/**
	 * debug
	 */
	private static final String APPLET_PARAM_DEBUG = "debug"; //$NON-NLS-1$

	private static final String APPLET_PARAM_SHOW_BEANS = "showBeans"; //$NON-NLS-1$

	private static final String APPLET_PARAM_BEANS = "beans"; //$NON-NLS-1$

	private static final long serialVersionUID = 6251221911709691733L;

	private static final String FILENAME_BEANS_XML = "beans.xml"; //$NON-NLS-1$

	/**
	 * Generation
	 */
	static final String GENERATION = Messages.getString("EvolutionaryApplet.0"); //$NON-NLS-1$

	private static final String EMPTY = ""; //$NON-NLS-1$

	/**
	 * TODO need to fix this. This URL is good for development:
	 * <code>file:../config/pepperedMoth.properties</code> But to deploy as an
	 * applet you will have to set the parameter {@link #PROPERTIES_URL} as an
	 * applet (embed) parameter in the HTML.
	 */
	protected static final String DEFAULT_PROPERTIES_URL = "pepperedMoth.properties"; //$NON-NLS-1$

	protected static final String PROPERTIES_URL = "PropertiesUrl"; //$NON-NLS-1$

	protected Evolution evolution;

	/**
	 * This is the control panel which allows the user to adjust settings.
	 */
	protected JPanel controlPanel;

	protected final transient Title _title;

	/**
	 * TitleApplication
	 */
	public static final String P_TITLE_APPLICATION = "TitleApplication"; //$NON-NLS-1$

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(EvolutionaryApplet.class);

	/**
	 * enableSlider
	 */
	protected static final String APPLET_PARAM_ENABLE_SLIDER = "enableSlider"; //$NON-NLS-1$

	private static final String APPLET_PARAM_JAR = "jar"; //$NON-NLS-1$

	/**
	 * 
	 */
	@SuppressWarnings("nls")
	private static final String[][] PARAMETER_INFO = new String[][] { { "width", "Integer", "display width" },
		{ "height", "Integer", "display height" },
		{ APPLET_PARAM_GEN, "Integer", "the maximum number of generations", BeanContainer.OPT_GENERATIONS },
		{ APPLET_PARAM_ENABLE_SLIDER, "Boolean", "enable slide option" },
		{ APPLET_PARAM_SHOW_BEANS, "Boolean", "enable showing of beans", BeanContainer.OPT_SHOW_BEANS },
		{ APPLET_PARAM_VALIDATE, "Boolean", "enable validation", BeanContainer.OPT_VALIDATE },
		{ APPLET_PARAM_DEBUG, "Boolean", "enable debug", BeanContainer.OPT_DEBUG },
		{ APPLET_PARAM_DTD, "java.net.URL", "DTD URL", BeanContainer.OPT_DTD },
		{ APPLET_PARAM_JAR, "java.net.URL", "DTD URL" },
		{ APPLET_PARAM_BEANS, "String", "configuration file", BeanContainer.OPT_CONFIG_FILE } };

	/**
	 * Version
	 */
	protected static final String LBL_VERSION = "Version"; //$NON-NLS-1$

	private final OptionSource<String> _optionSource;

	private final OptionMap<String, Object> _optionMap;

}
