/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ControlPanel_Default.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.ui.impl;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.BoxLayout;

import net.sf.darwin.api.impl.Messages;
import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.impl.swing.ControlPanel_;
import net.sf.darwin.ui.impl.swing.LabeledComponent;
import net.sf.darwin.ui.impl.swing.PropertySlider_Exponential;

/**
 * A trivial extension of {@link ControlPanel_}.
 * 
 * @author Robin Hillyard
 */
public final class ControlPanel_Default extends ControlPanel_ {

	/**
	 * Public constructor to instantiate a ControlPanel_Default.
	 * 
	 * @param controlAction
	 *            an implementation of {@link ControlAction}.
	 */
	public ControlPanel_Default(final ControlAction controlAction) {
		super(controlAction, Boolean.TRUE);

		// TODO don't really think these should be added to the default control
		// panel
		final Collection<Component> list = new ArrayList<Component>();
		list.add(new LabeledComponent(
				Messages.getString("ControlPanel_Default.0"), new PropertySlider_Exponential(controlAction, 3, Messages.getString("ControlPanel_Default.1"), 6, ControlAction.TIME_DELAY), BoxLayout.Y_AXIS)); //$NON-NLS-1$ //$NON-NLS-2$
		// list.add(new LabeledComponent("(Exp) Soot density)",new
		// PropertySlider_Normal(controlAction, 3, "", 5, ControlAction.SOOT,
		// ApplicationDefinitions.$InitSootDensity), BoxLayout.Y_AXIS));
		addComponents(list);
	}

	private static final long serialVersionUID = 7232522528491834919L;

}
