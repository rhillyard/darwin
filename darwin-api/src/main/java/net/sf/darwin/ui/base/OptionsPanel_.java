/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ControlPanel_.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.21 $
 */

package net.sf.darwin.ui.base;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.ui.impl.swing.LabeledComponent;
import net.sf.darwin.ui.impl.swing.TextField;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.JexpressionException;

/**
 * Defines base methods for the Options Panel for the Darwin display package.
 * 
 * @author Robin Hillyard
 */
public abstract class OptionsPanel_ extends JPanel implements OptionsPanel {

	/**
	 * @param controlAction
	 */
	protected OptionsPanel_(final ControlAction controlAction) {
		super();
		this._controlAction = controlAction;
		this._dependentBeanProperties = new HashMap<String, String>();
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(final ActionEvent actionEvent) {
		final String actionCommand = actionEvent.getActionCommand();
		final Object source = actionEvent.getSource();
		if (source instanceof TextField && ((TextField) source).getListener() != null) {
			try {
				(((TextField) source).getListener()).onTextChange(TextMessageable.SUBJECT_EXPRESSION,
						(((TextField) source).getText()));
			} catch (final VisualizationException e) {
				throw new DarwinException("OptionsPanel_.actionPerformed(): on text change exception", e); //$NON-NLS-1$
			}
		} else if (source instanceof JTextField) {
			try {
				getControlAction().setProperty(actionCommand, ((JTextField) source).getText());
			} catch (final VisualizationException e) {
				throw new DarwinException("OptionsPanel_.actionPerformed(): set property exception", e); //$NON-NLS-1$
			}
		} else if (source instanceof JCheckBox) {
			try {
				getControlAction().setState(((JCheckBox) source).isSelected() ? actionCommand : "!" + actionCommand); //$NON-NLS-1$
			} catch (final VisualizationException e) {
				throw new DarwinException("OptionsPanel_.actionPerformed(): set state exception", e); //$NON-NLS-1$
			}
		} else
			throw new DarwinException("OptionsPanel_.actionPerformed(): source not supported: " + source.getClass()); //$NON-NLS-1$
	}

	/**
	 * XXX consider splitting this into two methods: one to do the labeling, one
	 * do deal with the action aspects.
	 * 
	 * @param label
	 *            this is used for both labeling the component and as the action
	 *            command
	 * @param component
	 */
	@Override
	public void addActionableComponentWithLabel(final String label, final Component component) {
		// This is really bad! Those guys at Sun just don't understand about
		// interfaces!
		if (component instanceof JTextField) {
			final JTextField actionableComponent = (JTextField) component;
			actionableComponent.setActionCommand(label);
			actionableComponent.addActionListener(this);
		} else if (component instanceof JCheckBox) {
			final JCheckBox actionableComponent = (JCheckBox) component;
			actionableComponent.setActionCommand(label);
			actionableComponent.addActionListener(this);
		}
		if (!getLabels().contains(label)) {
			add(new LabeledComponent(label, component, BoxLayout.Y_AXIS));
			getLabels().add(label);
		} else
			LOG.warn("OptionsPanel_.addActionableComponentWithLabel(): already contains label " + label); //$NON-NLS-1$
	}

	/**
	 * @return {@link #_dependentBeanProperties}, i.e. the propertyMap
	 */
	@Override
	public Map<String, String> getDependentBeanProperties() {
		return this._dependentBeanProperties;
	}

	/**
	 * @param componentMap
	 */
	@Override
	public void setComponentMap(final Map<String, Component> componentMap, final Collection<? extends HasExpressions> beans) {
		for (final Entry<String, Component> e : componentMap.entrySet())
			addActionableComponentWithLabel(e.getKey(), e.getValue());
		// for (final String key : componentMap.keySet()) {
		// addActionableComponentWithLabel(key, componentMap.get(key));
		// }
		addFormulaFields(beans);
	}

	/**
	 * @param propertyMap
	 *            the propertyMap to set
	 */
	public void setDependentBeanProperties(final Map<String, String> propertyMap) {
		this._dependentBeanProperties = propertyMap;
	}

	/**
	 * @param formulaBeans
	 *            XXX
	 */
	void addFormulaFields(final Collection<? extends HasExpressions> formulaBeans) {
		for (final HasExpressions bean : formulaBeans)
			addFormulaFields(bean.getExpressions());
	}

	/**
	 * @param key
	 * @param evaluationExpression
	 */
	private void addFormulaField(final String key, final EvalExpressionMutable evaluationExpression) {
		final String expression = evaluationExpression.getExpression();
		if (expression == null || expression.length() == 0)
			LOG.warn("evaluator " + key + " has no expression"); //$NON-NLS-1$ //$NON-NLS-2$
		final Component expressionField = new TextField(expression, new TextMessageable() {
			@Override
			public void onTextChange(final String subject, final String text) throws VisualizationException {
				if (subject.equals(TextMessageable.SUBJECT_EXPRESSION)) {
					try {
						evaluationExpression.setExpression(text);
					} catch (final JexpressionException e) {
						throw new VisualizationException("TextMessage.onTextChange() exception: ", e); //$NON-NLS-1$
					}
					LOG.info("set expression: " + key + " = " + text); //$NON-NLS-1$ //$NON-NLS-2$
				} else
					throw new VisualizationException("TextMessage.onTextChange() unexpected subject: " + subject); //$NON-NLS-1$
			}
		});
		expressionField.setEnabled(evaluationExpression.isMutable());
		addActionableComponentWithLabel(key, expressionField);
	}

	/**
	 * @param optionsComponents
	 * @param expressions
	 */
	private void addFormulaFields(final ExpressionMap expressions) {
		for (final String key : expressions.keySet()) {
			final EvalExpressionMutable expression = expressions.get(key);
			if (expression != null)
				addFormulaField(key, expression);
		}
	}

	/**
	 * @return {@link #_controlAction}
	 */
	private ControlAction getControlAction() {
		return this._controlAction;
	}

	/**
	 * @return {@link #_labels}, i.e. the labels
	 */
	private Collection<String> getLabels() {
		return this._labels;
	}

	private final Collection<String> _labels = new ArrayList<String>();

	private final ControlAction _controlAction;

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(OptionsPanel_.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -7640894251631031934L;

	private Map<String, String> _dependentBeanProperties;

}
