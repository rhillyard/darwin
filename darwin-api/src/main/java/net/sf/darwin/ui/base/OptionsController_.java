/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OptionsController_.java
 * Created on Oct 6, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.ui.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract base class for options controllers.
 * 
 * @author Robin Hillyard
 * 
 */
public abstract class OptionsController_ implements ControlAction {

	/**
	 * @param interacter
	 *            the applet which interacts with the evolution.
	 * 
	 */
	protected OptionsController_(final Interacter interacter) {
		super();
		this._interacter = interacter;
	}

	/**
	 * @see net.sf.darwin.ui.base.ControlAction#settable(java.lang.String)
	 */
	@Override
	public boolean settable(final String propertyName) {
		return getInteracter().settable(propertyName);
	}

	/**
	 * @return {@link #_interacter}, i.e. the painter
	 */
	protected Interacter getInteracter() {
		return this._interacter;
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(OptionsController_.class);

	/**
	 * The user interface, typically a the {@link VisualizingEvolutionaryApplet}
	 * .
	 */
	private final Interacter _interacter;

}
