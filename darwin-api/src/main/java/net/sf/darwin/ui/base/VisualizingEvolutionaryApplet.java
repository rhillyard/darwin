/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizingEvolutionaryApplet.java
 * Created on Aug 5, 2009
 * @version $Revision: 1.25 $
 */

package net.sf.darwin.ui.base;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.swing.Icon;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.ui.impl.VisualizerTabs;
import net.sf.darwin.ui.impl.swing.EvolutionaryApplet;

/**
 * FIXME move me into UI module.
 * 
 * This subclass of EvolutionaryApplet additionally knows how to paint the
 * changes to the visualization model.
 * 
 * TODO fix the issue where we add (for Applet) (for each visualization model)
 * <member
 * membership="visualizationModelListener">!VisualizationModelXXX</member> and
 * (for each visualization model) <member
 * membership="visualizationModel">!Applet</member>
 * 
 * @author Robin Hillyard
 * 
 */
public abstract class VisualizingEvolutionaryApplet extends EvolutionaryApplet implements Interacter {

	/**
	 * @param beanContainer
	 *            XXX
	 * @throws HeadlessException
	 */
	protected VisualizingEvolutionaryApplet(final BeanContainer beanContainer) throws HeadlessException {
		super(beanContainer);
	}

	/**
	 * Method to add a visualization model to this applet. Note that the
	 * visualization models are kept in a list until the visualizer tabs have
	 * been set up. At that point, the method {@link #addVisualizers()} is
	 * invoked.
	 * 
	 * As a side-effect, we set the component property of the visualizationModel
	 * to be <code>this</code>.
	 * 
	 * @param visualizationModel
	 */
	public void addVisualizationModel(final VisualizationModel visualizationModel) {
		getVisualizationModels().add(visualizationModel);
		visualizationModel.setRepaintListener(new RepaintListener_Component(this));
	}

	/**
	 * FIXME here we should make an RMI call to get the expression beans from
	 * the Darwin platform.
	 * 
	 * @return a list of interactive functions
	 */
	public Collection<HasExpressions> getInteractiveFunctions() {
		final Collection<HasExpressions> result = new ArrayList<HasExpressions>();
		for (final Object bean : getBeans())
			if (bean instanceof HasExpressions)
				result.add((HasExpressions) bean);
		return result;
	}

	/**
	 * @see net.sf.darwin.ui.base.Paints#getPainter()
	 */
	@Override
	public Painter getPainter() {
		return getVisualizerTabs().getPainter();
	}

	/**
	 * @param optionsPanel
	 *            the optionsPanel to set
	 */
	public void setOptionsPanel(final OptionsPanel optionsPanel) {
		this._optionsPanel = optionsPanel;
	}

	/**
	 * @param visualizationFactory
	 *            the visualizationFactory to set
	 */
	public void setVisualizationFactory(final VisualizationFactory visualizationFactory) {
		this.visualizationFactory = visualizationFactory;
	}

	/**
	 * @param visualizerTabs
	 *            the visualizer tabs
	 * 
	 *            XXX consider defining this public in interface
	 */
	public void setVisualizerTabs(final VisualizerTabs visualizerTabs) {
		this.visualizerTabs = visualizerTabs;
	}

	/**
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 * 
	 *            XXX consider defining this public in interface
	 * @throws VisualizationException
	 */
	protected void addVisualizer(final String key, final VisualizationModel visualizationModel) throws VisualizationException {
		addVisualizer(key, visualizationModel, null);
	}

	/**
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 * @param icon
	 * @param tip
	 * 
	 *            XXX consider defining this public in interface
	 * @throws VisualizationException
	 */
	protected void addVisualizer(final String key, final VisualizationModel visualizationModel, final Icon icon, final String tip)
			throws VisualizationException {
		final VisualizerTabs tabs = getVisualizerTabs();
		if (tabs != null)
			tabs.addVisualizer(key, visualizationModel, icon, tip);
		else
			throw new DarwinException("Applet_Darwinian.addVisualizer(...): visualizer tabs not set up"); //$NON-NLS-1$
	}

	/**
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 * @param tip
	 * 
	 *            XXX consider defining this public in interface
	 * @throws VisualizationException
	 */
	protected void addVisualizer(final String key, final VisualizationModel visualizationModel, final String tip)
			throws VisualizationException {
		addVisualizer(key, visualizationModel, null, tip);
	}

	/**
	 * Method to add visualizers to this applet's visualizer tabs. The default
	 * implementation is to iterate through the list of visualization models in
	 * {@link #_visualizationModels} and for each individual visualization
	 * model, to take it's title as it's key and add a visualizer based on that
	 * key and that model. It's important to note that visualizers are looked up
	 * by events which note a change in a Visualizable, by keying on the
	 * visualizable's identifier. Therefore, it is expected that the title of
	 * the visualizable model corresponds to the identifier of the visualizable.
	 * 
	 * TODO consider that we may not have a 1:1 correspondence between
	 * visualizers and visualizable models.
	 * 
	 * May to be overridden by extenders.
	 * 
	 * @throws VisualizationException
	 */
	protected void addVisualizers() throws VisualizationException {
		for (final VisualizationModel visualizationModel : getVisualizationModels()) {
			addVisualizer(visualizationModel.getTitle(), visualizationModel);
		}
		getVisualizationModels().clear();
	}

	/**
	 * @see net.sf.darwin.ui.impl.swing.EvolutionaryApplet#createOptionsComponentMap()
	 */
	@Override
	protected Map<String, Component> createOptionsComponentMap() {
		// XXX Auto-generated method stub
		return null;
	}

	/**
	 * @return the optionsPanel
	 */
	protected OptionsPanel getOptionsPanel() {
		return this._optionsPanel;
	}

	/**
	 * @return the current value of visualizationFactory.
	 * 
	 */
	protected VisualizationFactory getVisualizationFactory() {
		return this.visualizationFactory;

	}

	/**
	 * XXX consider defining this public in interface
	 * 
	 * @return the visualizerTabs
	 */
	protected VisualizerTabs getVisualizerTabs() {
		return this.visualizerTabs;
	}

	/**
	 * @see net.sf.darwin.ui.impl.swing.EvolutionaryApplet#setup()
	 */
	@Override
	protected void setup() {
		setupLayout();
		setAppletTitle(getBeanContainer().getIdentifier());
		final Collection<HasExpressions> expressionBeans = getInteractiveFunctions();
		getOptionsPanel().setComponentMap(createOptionsComponentMap(), expressionBeans);
		add(getVisualizerTabs(), BorderLayout.CENTER);
		add(getControlPanel(), BorderLayout.SOUTH);
		setVisualizationDimensions(getVisualizationFactory());
		try {
			addVisualizers();
		} catch (final VisualizationException e) {
			LOG.warn("setup(): exception thrown", e); //$NON-NLS-1$
		}
	}

	/**
	 * As always when a user-interface action causes a change to the model which
	 * backs a widget, two things must happen: we must fire a change event so
	 * that other parts of the system know that the property has changed.
	 * Finally, we must repaint the user-interface in case something visual has
	 * changed. This step, however, is not the responsibility of this method (it
	 * is invoked by the method which calls this method).
	 * 
	 * @param name
	 *            the user-interface property that has changed.
	 * @param value
	 *            the new value.
	 * @throws VisualizationException
	 */
	@Override
	protected void updateModel(final String name, final Object value) throws VisualizationException {
		// Get any bean property dependence on this property.
		// beanProperty should be of the form: beanIdentifier.propertyName
		final String beanProperty = getOptionsPanel().getDependentBeanProperties().get(name);
		if (beanProperty != null)
			try {
				final Object bean = getBeanContainer().setBeanProperty(beanProperty, value);
				LOG.info("Updated bean property: " + beanProperty + " to " + value); //$NON-NLS-1$//$NON-NLS-2$
				// TODO consider other types of object too
				// TODO consider doing this via polymorphism
				if (bean instanceof Environment)
					((Environment) bean).fireEnvironmentChanged();
				else if (bean instanceof EcoFactor)
					((EcoFactor) bean).getEnvironment().fireEnvironmentChanged();
			} catch (final Exception e) {
				throw new VisualizationException("setProperty exception for bean property: " + beanProperty, e); //$NON-NLS-1$
			}
	}

	/**
	 * @return
	 */
	private Collection<Object> getBeans() {
		final Collection<Object> beans = new ArrayList<Object>();
		for (final String key : getBeanContainer().getBeanKeys())
			beans.add(getBeanContainer().getBean(key));
		return beans;
	}

	/**
	 * @return {@link #_visualizationModels}
	 */
	private Collection<VisualizationModel> getVisualizationModels() {
		return this._visualizationModels;
	}

	/**
	 * @param vizFact
	 */
	private void setVisualizationDimensions(final VisualizationFactory vizFact) {
		vizFact.setHeight(getHeight());
		vizFact.setWidth(getWidth());
	}

	private VisualizationFactory visualizationFactory;

	private final Collection<VisualizationModel> _visualizationModels = new ArrayList<VisualizationModel>();

	protected Environment _platformEnvironment;

	/**
	 * 
	 */
	private static final long serialVersionUID = 9009721436111004458L;

	/**
	 * the visualizer tabs
	 * 
	 * XXX improve this description
	 */
	private VisualizerTabs visualizerTabs;

	private OptionsPanel _optionsPanel;

}
