/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: OptionMap_Applet
 * Created on: Mar 5, 2012
 */

package net.sf.darwin.ui.impl.swing;

import java.applet.Applet;
import java.util.Map;

import net.sf.darwin.api.base.OptionMap;

import com.rubecula.beanpot.BeanPotException;
import com.rubecula.beanpot.BeanUtilities;

/**
 * @author rhillya
 * 
 */
public final class OptionMap_Applet extends OptionMap<String, Object> {
	/**
	 * @param defaultMap
	 * @param applet
	 *            XXX
	 */
	public OptionMap_Applet(final Map<String, ? extends Object> defaultMap, final Applet applet) {
		super(defaultMap);
		this._applet = applet;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.OptionMap#mapKey(java.lang.String)
	 */
	@Override
	protected String mapKey(final String sourceKey) {
		for (final String[] parameter : this._applet.getParameterInfo()) {
			if (parameter[0].equals(sourceKey)) {
				if (parameter.length > 3)
					return parameter[3];
				return super.mapKey(sourceKey);
			}
		}
		return super.mapKey(sourceKey);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.OptionMap#transform(String, java.lang.Object)
	 */
	@Override
	protected Object transform(final String sourceKey, final String value) {
		for (final String[] parameter : this._applet.getParameterInfo()) {
			if (parameter[0].equals(sourceKey)) {
				try {
					// FIXME remove dependence on BeanPot
					return BeanUtilities.getValueFromString(parameter[1], value, ClassLoader.getSystemClassLoader());
				} catch (final BeanPotException e) {
					throw new RuntimeException("transformation problem", e); //$NON-NLS-1$
				}
			}
		}
		return super.transform(sourceKey, value);
	}

	private static final long serialVersionUID = -6653054463855076261L;

	private final Applet _applet;

	// /**
	// * @param clazz
	// * @return
	// * @throws ClassNotFoundException
	// */
	// private static Class<?> getTransformClass(final String clazz) throws
	// ClassNotFoundException {
	// try {
	// return Class.forName(clazz);
	// } catch (final ClassNotFoundException e) {
	//			return Class.forName("java.lang" + clazz); //$NON-NLS-1$
	// }
	// }
}