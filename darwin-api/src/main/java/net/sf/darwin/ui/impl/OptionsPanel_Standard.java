/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OptionsPanel_Standard.java
 * Created on Feb 20, 2007
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.ui.impl;

import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.OptionsPanel_;

/**
 * Implementation of {@link OptionsPanel_} for the Peppered Moth.
 * 
 * @author Robin Hillyard
 */
public class OptionsPanel_Standard extends OptionsPanel_ {

	/**
	 * Public constructor. Note that the key in the map will be used both as
	 * visual labels and as action commands
	 * 
	 * @param controlAction
	 */
	public OptionsPanel_Standard(final ControlAction controlAction) {
		super(controlAction);
	}

	private static final long serialVersionUID = 8563053733795431122L;
}
