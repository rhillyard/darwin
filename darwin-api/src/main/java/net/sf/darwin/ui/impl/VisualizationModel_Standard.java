/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizationModel_Standard.java
 * Created on Feb 4, 2007
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.ui.impl;

import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.VisualizationModel_;

/**
 * Standard implementation of Visualization Model.
 * 
 * @author Robin Hillyard
 */
public class VisualizationModel_Standard extends VisualizationModel_ {

	/**
	 * Public constructor to create a visualization model for the given
	 * population.
	 * 
	 * @param visualizable
	 */
	public VisualizationModel_Standard(final Visualizable visualizable) {
		super(visualizable.getIdentifier());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8955856990261050186L;

}
