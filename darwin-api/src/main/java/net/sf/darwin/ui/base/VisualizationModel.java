/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizationModel.java
 * Created on Feb 4, 2007
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.ui.base;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import net.sf.darwin.core.Countable;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Visualizable;

/**
 * Defines a model to which population delegates its graphical rendering.
 * 
 * @author Robin Hillyard
 * 
 */
public interface VisualizationModel extends Countable, Serializable {

	/**
	 * Mutator method to add an individual to this population (the index for
	 * this new individual will be the current value of {@link #getCount()}.
	 * 
	 * @param avatar
	 *            an individual which is to be added to the population.
	 */
	public abstract void addAvatar(Avatar avatar);

	/**
	 * Clean the model of dead wood, so to speak.
	 * 
	 * @return the number of individuals culled.
	 */
	public abstract int clean();

	/**
	 * Method to remove all individuals from the model.
	 * 
	 * Consider eliminating.
	 */
	public abstract void clear();

	/**
	 * @param individual
	 * @return an Avatar based on the given individual.
	 */
	public abstract Avatar findAvatar(Individual individual);

	/**
	 * Method to notify all of the listeners that the population has changed. In
	 * general, should be called after any calls to {@link #addAvatar(Avatar)}
	 * or {@link #removeIndividual(Avatar)}.
	 */
	public abstract void fireModelChange();

	/**
	 * @return a map which specifies various attributes of the visualization
	 *         model, derived typically from the environment.
	 */
	public abstract Map<String, Object> getAttributes();

	/**
	 * @return the current collection of individual objects.
	 */
	public abstract Collection<Avatar> getIndividuals();

	/**
	 * @return get the title for this visualization model.
	 */
	public abstract String getTitle();

	/**
	 * @return true if this visualization model is visible.
	 */
	public abstract boolean isVisible();

	/**
	 * Method to set (or reset) an attribute.
	 * 
	 * @param attrName
	 * @param attrValue
	 * 
	 */
	public abstract void putAttribute(String attrName, Object attrValue);

	/**
	 * Mutator method to remove an individual from this population (NOTE: the
	 * indices for some of the other individual members of the population will
	 * be affected).
	 * 
	 * @param avatar
	 *            an individual which is to be removed from the population.
	 * @return true if the population model has changed (result would be false
	 *         if individual was not a member of this population model).
	 */
	public abstract boolean removeIndividual(Avatar avatar);

	/**
	 * Method to initialize several attributes at once.
	 * 
	 * @param attributes
	 */
	public abstract void setAttributes(Map<String, Object> attributes);

	/**
	 * @param component
	 *            the component to set
	 */
	public void setRepaintListener(RepaintListener component);

	/**
	 * @param visible
	 *            the visible to set
	 */
	public abstract void setVisible(boolean visible);

	/**
	 * @param visualizable
	 * @param visualizationFactory
	 */
	public abstract void visualize(Visualizable visualizable, VisualizationFactory visualizationFactory);

}
