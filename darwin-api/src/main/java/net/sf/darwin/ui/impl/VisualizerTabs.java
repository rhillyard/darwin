/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizerTabs.java
 * Created on Feb 20, 2007
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.ui.impl;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.Painter;
import net.sf.darwin.ui.base.VisualizationException;
import net.sf.darwin.ui.base.VisualizationFactory;
import net.sf.darwin.ui.base.VisualizationModel;
import net.sf.darwin.ui.base.Visualizer;

/**
 * A set of tabbed panels which will be used to visualize different populations,
 * as well as showing the secondary control panel.
 * 
 * @author Robin Hillyard
 */
public class VisualizerTabs extends JTabbedPane {

	/**
	 * Secondary constructor.
	 * 
	 * @param painter
	 */
	public VisualizerTabs(final Painter painter) {
		this(painter, SwingConstants.TOP);
	}

	/**
	 * Secondary constructor.
	 * 
	 * @param painter
	 * @param tabPlacement
	 */
	public VisualizerTabs(final Painter painter, final int tabPlacement) {
		this(painter, tabPlacement, JTabbedPane.WRAP_TAB_LAYOUT);
	}

	/**
	 * Primary constructor of a VisualizerTabs instance.
	 * 
	 * @param painter
	 * @param tabPlacement
	 * @param tabLayoutPolicy
	 */
	public VisualizerTabs(final Painter painter, final int tabPlacement, final int tabLayoutPolicy) {
		super(tabPlacement, tabLayoutPolicy);
		this._painter = painter;
	}

	/**
	 * Method to add a tabbed pane supporting a visualizer at the end of the
	 * list of tabs.
	 * 
	 * @param key
	 *            XXX
	 * @param visualizationModel
	 *            the visualization model which will back the new visualizer in
	 *            the next tab.
	 * @param icon
	 *            an icon to show in the tab (may be null).
	 * @param tip
	 *            a tooltip for the tab (may be null).
	 * 
	 *            XXX Define in an interface
	 * @throws VisualizationException
	 * 
	 */
	public void addVisualizer(final String key, final VisualizationModel visualizationModel, final Icon icon, final String tip)
			throws VisualizationException {
		final Visualizer visualizer = new Visualizer_Standard(visualizationModel, getPainter());
		final String title = visualizationModel.getTitle();
		final int index = getTabCount();
		addTab(title, icon, visualizer.asComponent(), tip);
		final Integer previous = getVisualizerMap().put(key, Integer.valueOf(index));
		if (previous == null)
			setSelectedIndex(index);
		else
			throw new VisualizationException("non-unique visualizer key"); //$NON-NLS-1$
	}

	/**
	 * @return the painter
	 */
	public Painter getPainter() {
		return this._painter;
	}

	/**
	 * @param key
	 *            the key which identifies this visualizer.
	 * @return the visualizer corresponding to population (or null if there is
	 *         no such visualizer).
	 * 
	 *         TODO Define in an interface
	 * 
	 * @see Map#get(Object)
	 * @see #_visualizerMap
	 * @see JTabbedPane#getComponentAt(int)
	 * @see Integer#intValue()
	 */
	public Visualizer getVisualizer(final String key) {
		final Integer index = getVisualizerMap().get(key);
		if (index != null)
			return (Visualizer) getComponentAt(index.intValue());
		return null;
	}

	/**
	 * It's currently not possible to specify a specific ordering of the tabs.
	 * 
	 * TODO allow an ordering.
	 * 
	 * TODO Define in an interface
	 * 
	 * @param map
	 *            of String:Component pairs
	 */
	public void setTabs(final Map<String, Component> map) {
		// TODO consider using entrySet()
		for (final String key : map.keySet()) {
			addTab(key, map.get(key));
		}
	}

	/**
	 * TODO figure out whether this should be public/static whatever
	 * 
	 * @param visualizable
	 * @param visualizer
	 * @param visualizationFactory
	 *            the visualizer factory with which to make avatars
	 */
	@SuppressWarnings("static-method")
	public void visualize(final Visualizable visualizable, final Visualizer visualizer,
			final VisualizationFactory visualizationFactory) {
		visualizer.getModel().visualize(visualizable, visualizationFactory);
	}

	/**
	 * @return the visualizerMap
	 */
	private Map<String, Integer> getVisualizerMap() {
		return this._visualizerMap;
	}

	private static final long serialVersionUID = 3359983832189645969L;

	final private Map<String, Integer> _visualizerMap = new HashMap<String, Integer>();

	final private Painter _painter;
}
