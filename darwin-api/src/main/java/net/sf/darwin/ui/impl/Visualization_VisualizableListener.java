/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualization_VisualizableListener.java
 * Created on Feb 19, 2007
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.ui.impl;

import java.text.MessageFormat;

import net.sf.darwin.core.Visualizable;
import net.sf.darwin.core.VisualizableListener;
import net.sf.darwin.ui.base.VisualizationFactory;
import net.sf.darwin.ui.base.Visualization_;
import net.sf.darwin.ui.base.Visualizer;

/**
 * Implementer of {@link VisualizableListener} which updates a visualization
 * model.
 * 
 * @author Robin Hillyard
 */
public class Visualization_VisualizableListener extends Visualization_ implements VisualizableListener {

	/**
	 * Primary constructor.
	 * 
	 * @param factory
	 */
	public Visualization_VisualizableListener(final VisualizationFactory factory) {
		super();
		this._factory = factory;
	}

	/**
	 * This method loops through all the organisms in population <code>p</code>
	 * and, for each one, and, if it is not viable [either just born or newly
	 * dead], either removes the corresponding individual, or (providing that
	 * the age is 0) adds a newly created individual.
	 * 
	 * @see net.sf.darwin.core.VisualizableListener#onChange(Visualizable,
	 *      Object)
	 * @see VisualizerTabs#getVisualizer(String)
	 * @see Visualizer#getModel()
	 */
	@Override
	public void onChange(final Visualizable source, final Object context) {
		final VisualizerTabs tabs = getVisualizerTabs();
		if (tabs != null) {
			if (source != null) {
				final Visualizer visualizer = tabs.getVisualizer(source.getIdentifier());
				if (visualizer != null)
					tabs.visualize(source, visualizer, getFactory());
			}
		} else
			LOG.info(MessageFormat.format("onChange(): no visualizer tabs to show {0} in context{1}", source, context)); //$NON-NLS-1$
		// throw new DarwinException(
		//					"VisualizableListener.populationChanged(): visualizer tabs have not been set"); //$NON-NLS-1$
	}

	/**
	 * @return {@link #_factory}
	 */
	private VisualizationFactory getFactory() {
		return this._factory;
	}

	private final VisualizationFactory _factory;
}