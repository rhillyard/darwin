/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualization_.java
 * Created on Aug 6, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.ui.base;

import net.sf.darwin.ui.impl.VisualizerTabs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class Visualization_ implements Visualization {

	/**
	 * 
	 */
	protected Visualization_() {
		super();
	}

	/**
	 * @see com.rubecula.darwin.visualization.swing.Visualization#getVisualizerTabs()
	 */
	@Override
	public VisualizerTabs getVisualizerTabs() {
		return this.visualizerTabs;
	}

	/**
	 * @see com.rubecula.darwin.visualization.swing.Visualization#setVisualizerTabs(net.sf.darwin.ui.impl.VisualizerTabs)
	 */
	@Override
	public void setVisualizerTabs(final VisualizerTabs visualizerTabs) {
		this.visualizerTabs = visualizerTabs;
	}

	/**
	 * TODO use audit utilities
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getCanonicalName() + " with tabs: " + getVisualizerTabs(); //$NON-NLS-1$
	}

	private VisualizerTabs visualizerTabs;

	protected static final Log LOG = LogFactory.getLog(Visualization_.class);

}
