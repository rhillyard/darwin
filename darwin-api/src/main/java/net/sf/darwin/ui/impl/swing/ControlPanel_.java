/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ControlPanel_.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.ui.impl.swing;

import java.awt.Component;
import java.text.MessageFormat;
import java.util.Collection;

import javax.swing.JPanel;

import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.ControlPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Defines base methods for the ControlPanel for the Darwin display package.
 * 
 * @author Robin Hillyard
 */
public abstract class ControlPanel_ extends JPanel implements ControlPanel {

	/**
	 * Constructor which sets up a pair of flip/flop buttons for start/stop.
	 * 
	 * @param controlAction
	 *            the implementer of {@link ControlAction} which is to be called
	 *            whenever one of the buttons is pushed.
	 * @param startRunning
	 *            if true then the experiment will start immediately
	 */
	protected ControlPanel_(final ControlAction controlAction, final Boolean startRunning) {
		super();
		// _controlAction = controlAction;
		final JPanel buttonPanel = new JPanel();
		add(buttonPanel);
		final FlipFlopButtons flipFlopButtons = new FlipFlopButtons(controlAction, buttonPanel);
		flipFlopButtons.addButton(ControlAction.START, !startRunning.booleanValue());
		flipFlopButtons.addButton(ControlAction.STOP, startRunning.booleanValue());
		// new JButton(ControlAction.NEXT);
		final ActionButton next = new ActionButton(controlAction, ControlAction.NEXT);
		buttonPanel.add(next);
		// The following button doesn't properly quit so let's not use it
		// add(new ActionButton(controlAction,ControlAction.QUIT));
	}

	/**
	 * @see com.rubecula.darwin.visualization.swing.ControlPanel#enableComponent(java.lang.String,
	 *      boolean)
	 */
	@Override
	public void enableComponent(final String name, final boolean enabled) {
		for (int i = 0; i < getComponents().length; i++) {
			final Component component = getComponents()[i];
			// TODO consider doing this via polymorphism
			if (component instanceof LabeledComponent) {
				if (name.equalsIgnoreCase(((LabeledComponent) component).getName())) {
					component.setEnabled(enabled);
					logComponent(name, enabled);
				}
			}
		}
	}

	protected void addComponents(final Collection<Component> components) {
		for (final Component component : components) {
			add(component);
		}
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(ControlPanel_.class);

	private static final long serialVersionUID = -7427246195791784266L;

	/**
	 * @param name
	 * @param enabled
	 */
	@SuppressWarnings("boxing")
	private static void logComponent(final String name, final boolean enabled) {
		LOG.info(MessageFormat.format("enableComponent: {0}: {1}", name, enabled)); //$NON-NLS-1$
	}

}
