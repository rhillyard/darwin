/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MockVisualizableListener.java
 * Created on Feb 19, 2007
 * @version $Revision: 1.16 $
 */

package net.sf.darwin.ui.impl;

import java.io.IOException;

import net.sf.darwin.core.Census;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Sink;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.VisualizableListener_;

/**
 * Implementation of VisualizableListener which knows how to take a census of
 * the (changed) population.
 * 
 * @author Robin Hillyard
 */
public final class VisualizableListener_Census extends VisualizableListener_ {

	/**
	 * Public constructor to create a MockVisualizableListener with given
	 * implementations of (print) writer and (census) taker.
	 * 
	 * @param censusTaker
	 */
	public VisualizableListener_Census(final Census censusTaker) {
		this._taker = censusTaker;
	}

	/**
	 * Override the abstract method to deal with a change in the population. In
	 * particular, the method {@link Census#census(Censusible, int, Object)} is
	 * called on {@link #_taker}; and then the taker is flushed.
	 * 
	 * @see net.sf.darwin.core.VisualizableListener#onChange(Visualizable,
	 *      Object)
	 */
	@Override
	public void onChange(final Visualizable p, final Object context) {
		getTaker().census(p, 2, context);
		try {
			// TODO consider doing this via polymorphism
			if (getTaker() instanceof Sink) {
				((Sink) getTaker()).flush();
			}
		} catch (final IOException e) {
			throw new RuntimeException("I/O exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * TODO consider using toString0 utilities
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MockVisualizableListener with taker: " + getTaker(); //$NON-NLS-1$
	}

	/**
	 * @return the taker
	 */
	private Census getTaker() {
		return this._taker;
	}

	private final Census _taker;
}