/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualizer_.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.ui.base;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.text.MessageFormat;

import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * An opaque Swing component (a JPanel) which defines an area for visualizing
 * the evolution in progress. In order to display anything, an implementer of
 * {@link Painter} must be provided by the application. <br>
 * The graphics of this component are double-buffered.
 * 
 * @author Robin Hillyard
 */
public abstract class Visualizer_ extends JPanel implements Visualizer {

	/**
	 * Construct a Visualizer_ by invoking {@link JPanel#JPanel()} and setting
	 * opaque to be true.
	 * 
	 * @param model
	 *            the visualization model which will be visualized by this
	 *            visualizer.
	 * 
	 * @param painter
	 *            the painter which will actual paint the model in this
	 *            visualizer.
	 * 
	 */
	protected Visualizer_(final VisualizationModel model, final Painter painter) {
		super();
		this._model = model;
		this._painter = painter;
		setOpaque(true);
	}

	/**
	 * @see com.rubecula.darwin.visualization.Visualizer#asComponent()
	 */
	@Override
	public Component asComponent() {
		return this;
	}

	/**
	 * @see com.rubecula.darwin.visualization.Visualizer#getModel()
	 */
	@Override
	public VisualizationModel getModel() {
		return this._model;
	}

	/**
	 * @see com.rubecula.darwin.visualization.Visualizer#getPainter()
	 */
	@Override
	public Painter getPainter() {
		return this._painter;
	}

	/**
	 * @return the bufferContext
	 */
	protected Graphics getBufferContext() {
		return this.bufferContext;
	}

	/**
	 * @return the bufferImage
	 */
	protected Image getBufferImage() {
		return this.bufferImage;
	}

	/**
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(final Graphics graphics) {
		if (getPainter() != null) {
			// XXX these are only necessary if the size/shape of this component
			// has changed
			setBufferImage(createImage(getWidth(), getHeight()));
			setBufferContext(getBufferImage().getGraphics());

			getPainter().paintBase(getBufferContext(), getSize().width, getSize().width);
			paintVisualizationModel();
			graphics.drawImage(getBufferImage(), 0, 0, this);
		}
	}

	/**
	 * @param bufferContext
	 *            the bufferContext to set
	 */
	protected void setBufferContext(final Graphics bufferContext) {
		this.bufferContext = bufferContext;
	}

	/**
	 * @param bufferImage
	 *            the bufferImage to set
	 */
	protected void setBufferImage(final Image bufferImage) {
		this.bufferImage = bufferImage;
	}

	/**
	 * Method to paint the specified visualization model. This is done by first
	 * calling
	 * {@link Painter#paintVisualization(VisualizationModel, Graphics, int, int)}
	 * and then, for each individual, calling
	 * {@link Painter#paintIndividual(Graphics, Avatar)}.
	 */
	private void paintVisualizationModel() {
		final VisualizationModel model = getModel();
		synchronized (model) {
			getPainter().paintVisualization(model, getBufferContext(), getSize().width, getSize().width);
			logClean(model.clean());
			for (final Avatar avatar : model.getIndividuals()) {
				getPainter().paintIndividual(getBufferContext(), avatar);
			}
		}
	}

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Visualizer_.class);

	private static final long serialVersionUID = 6450213012532902988L;

	/**
	 * The visualization model which is supported by this Visualizer.
	 */
	private final VisualizationModel _model;

	/**
	 * The implementation of {@link Painter} which will render the population
	 * graphically.
	 */
	private final Painter _painter;

	/**
	 * The graphics context for the buffer
	 */
	private Graphics bufferContext;

	/**
	 * The image for double buffering
	 */
	private Image bufferImage;

	/**
	 * @param clean
	 */
	@SuppressWarnings("boxing")
	private static void logClean(final int clean) {
		if (clean > 0 && LOG.isDebugEnabled())
			LOG.debug(MessageFormat.format("Visualizer: culled {0} individuals", clean)); //$NON-NLS-1$
	}

}
