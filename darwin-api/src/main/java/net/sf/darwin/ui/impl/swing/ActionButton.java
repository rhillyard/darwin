/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FlipFlopButtons.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.ui.impl.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.VisualizationException;

/**
 * Class which manages a set of JButtons (usually of cardinality two), belonging
 * to a container and, when pressed, invoking calls to an implementer of
 * {@link ControlAction}. When a button is pressed, two things will happen:
 * <ol>
 * <li>the ControlAction implementer will be called with
 * {@link ControlAction#setState(String)} where the parameter is the action
 * command for the button;</li>
 * <li>every button's enabled state will be flipped.</li>
 * </ol>
 * 
 * @author Robin Hillyard
 * 
 */
public class ActionButton extends JButton implements ActionListener {

	/**
	 * @param controlAction
	 * @param command
	 */
	public ActionButton(final ControlAction controlAction, final String command) {
		super(command);
		this._controlAction = controlAction;
		setEnabled(true);
		addActionListener(this);
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(final ActionEvent event) {
		try {
			getControlAction().setState(event.getActionCommand());
		} catch (final VisualizationException e) {
			throw new DarwinException("set state exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * @return {@link #_controlAction}
	 */
	private ControlAction getControlAction() {
		return this._controlAction;
	}

	private static final long serialVersionUID = 7041683752500501612L;

	private final ControlAction _controlAction;
}
