/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: PropertySlider_Exponential.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.ui.impl.swing;

import java.awt.Component;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.VisualizationException;

/**
 * This extension of {@link JSlider} sets up a slider which controls a property
 * of the application. The values of the slider or exponential (or logarithmic,
 * depending on your point of view).
 * 
 * @author Robin Hillyard
 * 
 */
public class PropertySlider_Exponential extends JSlider implements ChangeListener {

	/**
	 * Constructor to set up an instance of PropertySlider_Exponential. The
	 * class extends JSlider and will behave thus. This object will respond to
	 * slider position changes and calculate the appropriate property change.
	 * 
	 * @param controlAction
	 *            the implementer of ControlAction which will be invoked to set
	 *            the property value, as determined by propertyName, and passed
	 *            as a Double.
	 * @param majorTicks
	 *            the number of major ticks that we will see on the slider.
	 * @param tag
	 *            the tag which will accompany the value for each major tick.
	 * @param scale
	 *            the scale, that is to say, the value which will be shown at
	 *            the left-hand tick mark.
	 * @param propertyName
	 *            the name of the property to be set
	 */
	public PropertySlider_Exponential(final ControlAction controlAction, final int majorTicks, final String tag, final int scale,
			final String propertyName) {
		super(SwingConstants.HORIZONTAL, 0, MULTIPLIER * (majorTicks - 1), MULTIPLIER * (majorTicks - 1) / 2);
		this._controlAction = controlAction;
		this._scale = scale;
		this._propertyName = propertyName;
		init(majorTicks, tag, scale);
	}

	/**
	 * The value for generations-per-minute is calculated from the slider value,
	 * and passed (as a Double) into the
	 * {@link ControlAction#setProperty(String, Object)} method for the provided
	 * implementation of ControlAction.
	 * 
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(final ChangeEvent e) {
		final JSlider source = (JSlider) e.getSource();
		if (!source.getValueIsAdjusting()) {
			final double value = getScale() * Math.pow(LOGBASE, 1.0 * source.getValue() / MULTIPLIER);
			try {
				getControlAction().setProperty(getPropertyName(), new Double(value));
			} catch (final VisualizationException e1) {
				throw new DarwinException("PropertySlider_Exponential.stateChanged() exception", e1); //$NON-NLS-1$
			}
		}
	}

	/**
	 * @return {@link #_controlAction}
	 */
	private ControlAction getControlAction() {
		return this._controlAction;
	}

	/**
	 * @return {@link #_propertyName}
	 */
	private String getPropertyName() {
		return this._propertyName;
	}

	/**
	 * @return {@link #_scale}
	 */
	private double getScale() {
		return this._scale;
	}

	/**
	 * @param majorTicks
	 * @param tag
	 * @param scale
	 */
	private void init(final int majorTicks, final String tag, final int scale) {
		addChangeListener(this);
		setMajorTickSpacing(MULTIPLIER);
		setMinorTickSpacing(1);
		setPaintTicks(true);
		setPaintLabels(true);
		final Dictionary<Integer, Component> labelTable = new Hashtable<Integer, Component>();
		for (int i = 0; i < majorTicks; i++) {
			final String label = Math.round(scale * Math.pow(LOGBASE, i)) + SP + tag;
			labelTable.put(Integer.valueOf(i * MULTIPLIER), new JLabel(label));
		}
		setLabelTable(labelTable);
		setEnabled(getControlAction().settable(getPropertyName()));
	}

	private static final int LOGBASE = 10;

	private static final String SP = " "; //$NON-NLS-1$

	private static final int MULTIPLIER = 10; // this controls the number of

	// gradations in the slider,
	// it's effect is purely visual

	private static final long serialVersionUID = 8274787085916315200L;

	private final ControlAction _controlAction;

	private final double _scale;

	private final String _propertyName;
}
