/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: LabeledComponent.java
 * Created on Feb 14, 2007
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.ui.impl.swing;

import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * General utility (not really specific to the Darwin package).
 * 
 * @author Robin Hillyard
 */
public class LabeledComponent extends JPanel {

	/**
	 * Construct a LabledComponent with the given text for the label, the given
	 * component, and in the appropriate orientation (from {@link BoxLayout}.
	 * 
	 * @param labelText
	 * @param component
	 * @param orientation
	 */
	public LabeledComponent(final String labelText, final Component component, final int orientation) {
		super();
		setLayout(new BoxLayout(this, orientation));
		final JLabel label = new JLabel(labelText);
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		label.setAlignmentY(Component.CENTER_ALIGNMENT);
		add(label); // component 0
		add(component); // component 1
		setName(labelText); // TEST
	}

	/**
	 * @see javax.swing.JComponent#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(final boolean enabled) {
		// TODO consider enabling all of the components, not just the 1th.
		getComponent(1).setEnabled(enabled);
		super.setEnabled(enabled);
	}

	private static final long serialVersionUID = -8756796076669106688L;

}
