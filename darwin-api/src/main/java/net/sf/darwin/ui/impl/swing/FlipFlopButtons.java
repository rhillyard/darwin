/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FlipFlopButtons.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.ui.impl.swing;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.ui.base.ControlAction;
import net.sf.darwin.ui.base.VisualizationException;

/**
 * Class which manages a set of JButtons (usually of cardinality two), belonging
 * to a container and, when pressed, invoking calls to an implementer of
 * {@link ControlAction}. When a button is pressed, two things will happen:
 * <ol>
 * <li>the ControlAction implementer will be called with
 * {@link ControlAction#setState(String)} where the parameter is the action
 * command for the button;</li>
 * <li>every button's enabled state will be flipped.</li>
 * </ol>
 * 
 * @author Robin Hillyard
 * 
 */
public class FlipFlopButtons implements ActionListener {

	/**
	 * @param controlAction
	 * @param container
	 */
	public FlipFlopButtons(final ControlAction controlAction, final Container container) {
		super();
		this._controlAction = controlAction;
		this._container = container;
		this._buttons = new ArrayList<JButton>();
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(final ActionEvent event) {
		try {
			getControlAction().setState(event.getActionCommand());
			for (final JButton button : getButtons()) {
				button.setEnabled(!button.isEnabled());
			}
			// Need also to turn off the "Next" button - but it isn't part of
			// this set of flip/flops
		} catch (final VisualizationException e) {
			throw new DarwinException("setState exception", e); //$NON-NLS-1$
		}
	}

	/**
	 * Mutating method to create a new {@link JButton} and:
	 * <ol>
	 * <li>add it to this object's {@link Container}</li>
	 * <li>set its enabled state (from the given parameter)</li>
	 * <li>add <code>this</code> as an action listener to it</li>
	 * <li>add it to the list of buttons</li>
	 * </ol>
	 * 
	 * @param command
	 *            used to define both the label and the action command for a the
	 *            new button.
	 * @param enabled
	 *            the initial value of the enabled property for the new button.
	 * @return the result of calling {@link Collection#add(Object)} on the
	 *         buttons list with parameter the new button.
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	public boolean addButton(final String command, final boolean enabled) {
		final JButton button = new JButton(command);
		getContainer().add(button);
		button.setEnabled(enabled);
		button.addActionListener(this);
		return getButtons().add(button);
	}

	/**
	 * @return {@link #_buttons}
	 */
	private Collection<JButton> getButtons() {
		return this._buttons;
	}

	/**
	 * @return {@link #_container}
	 */
	private Container getContainer() {
		return this._container;
	}

	/**
	 * @return {@link #_controlAction}
	 */
	private ControlAction getControlAction() {
		return this._controlAction;
	}

	private final Collection<JButton> _buttons;

	private final ControlAction _controlAction;

	private final Container _container;

}
