/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TextMessageable.java
 * Created on Apr 16, 2009
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.ui.base;

/**
 * @author Robin Hillyard
 * 
 */
public interface TextMessageable {

	/**
	 * Callback method to notify this object that text has been changed in
	 * another object. That other object is indicated, very loosely, by the
	 * subject parameter. XXX consider having the call back specify the actual
	 * changed object.
	 * 
	 * @param subject
	 *            the type of change that has occurred in the text
	 * @param text
	 *            the new version of the text.
	 * @throws VisualizationException
	 *             XXX
	 */
	public abstract void onTextChange(String subject, String text) throws VisualizationException;

	/**
	 * 
	 */
	public static final String SUBJECT_EXPRESSION = "expression"; //$NON-NLS-1$

}
