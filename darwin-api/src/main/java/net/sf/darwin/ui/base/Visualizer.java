/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualizer.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.ui.base;

import java.awt.Component;

/**
 * Defines the public operations required for a Visualizer.
 * 
 * @author Robin Hillyard
 */
public interface Visualizer {

	/**
	 * @return this object as a Component.
	 */
	public abstract Component asComponent();

	/**
	 * @return the model
	 */
	public abstract VisualizationModel getModel();

	/**
	 * Get the implementation of {@link Painter} for this system.
	 * 
	 * @return the painter defined for this {@link Visualizer}
	 */
	public abstract Painter getPainter();

}