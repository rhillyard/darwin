/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Painter_.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.11 $
 */

package net.sf.darwin.ui.base;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Map;

import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Mortal;
import net.sf.darwin.core.Sexual;

/**
 * Base methods and operations for implementers of {@link Painter}.
 * 
 * Sub-classes of this type have life-span: permanent.
 * 
 * @author Robin Hillyard
 */
public abstract class Painter_ implements Painter {

	/**
	 * Protected base constructor for implementers of {@link Painter}
	 */
	protected Painter_() {
		super();
	}

	/**
	 * @return the identify
	 */
	@Override
	public boolean isIdentify() {
		return this.identify;
	}

	/**
	 * Base method: simply clears the rectangle defined by {0, 0, width, height}
	 * to the (previously defined) background color. /* (non-Javadoc)
	 * 
	 * @see net.sf.darwin.ui.base.Painter#paintBase(java.awt.Graphics, int, int)
	 */
	@Override
	public void paintBase(final Graphics g, final int width, final int height) {
		g.clearRect(0, 0, width, height);
	}

	/**
	 * @see net.sf.darwin.ui.base.Painter#paintVisualization(com.rubecula.darwin.visualization.VisualizationModel,
	 *      java.awt.Graphics, int, int)
	 */
	@Override
	public void paintVisualization(final VisualizationModel visualizationModel, final Graphics g, final int width,
			final int height) {
		if (visualizationModel.isVisible()) {
			final Map<String, Object> attributes = visualizationModel.getAttributes();
			final Color color = (Color) attributes.get(BACKGROUND_COLOR);
			if (color != null) {
				g.setColor(color);
				g.fillRect(0, 0, width, height);
			} else
				throw new DarwinException("Painter_.paintVisualization(): backgroundColor attribute undefined"); //$NON-NLS-1$
		}
	}

	/**
	 * @param identify
	 *            the identify to set
	 */
	@Override
	public void setIdentify(final boolean identify) {
		this.identify = identify;
	}

	@SuppressWarnings("static-method")
	protected void paintIdentification(final Graphics g, final Avatar avatar, final Point location) {
		g.setColor(Color.red);
		final Individual individual = avatar.getIndividual();
		String label = individual.getIdentifier() + ": "; //$NON-NLS-1$
		// TODO consider doing this via polymorphism
		if (individual instanceof Mortal)
			label += ((Mortal) individual).getAge();
		// TODO consider doing this via polymorphism
		if (individual instanceof Sexual)
			label += (((Sexual) individual).isFemale() ? "F" : "M"); //$NON-NLS-1$//$NON-NLS-2$
		if (!individual.isVisible())
			label += "**"; //$NON-NLS-1$
		g.drawChars(label.toCharArray(), 0, label.length(), location.x - 5, location.y + 5);
	}

	private boolean identify;

}
