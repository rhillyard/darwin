/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Avatar.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.5 $
 */

package net.sf.darwin.ui.base;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;

import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Visualizable;

/**
 * Defines the methods required in order to be able to draw an individual member
 * of a {@link Visualizable} community.
 * 
 * @author Robin Hillyard
 */
public interface Avatar extends Serializable {

	/**
	 * @return a color suitable for representing this individual.
	 */
	public abstract Color getColor();

	/**
	 * @return the organism
	 */
	public abstract Individual getIndividual();

	/**
	 * @return a point at which this individual should be presented.
	 */
	public abstract Point getLocation();

	/**
	 * @return the (relative) size of this individual
	 */
	public abstract double getSize();
}
