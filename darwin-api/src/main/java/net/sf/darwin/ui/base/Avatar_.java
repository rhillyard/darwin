/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Avatar_.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.ui.base;

import java.awt.Color;
import java.awt.Point;
import java.text.NumberFormat;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Individual;
import net.sf.tostring0.ToString;

/**
 * Abstract base class for implementers of {@link Avatar}.
 * 
 * @author Robin Hillyard
 */
@ToString(allBeanMethods = false)
public abstract class Avatar_ extends Base implements Avatar {

	/**
	 * Protected constructor which creates a new instance of Avatar at the point
	 * [0,0] and with color black.
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 */
	protected Avatar_(final Individual individual) {
		this(individual, Color.black);
	}

	/**
	 * Protected constructor which creates a new instance of Avatar at the point
	 * [0,0] and with given <code>color</code>.
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param color
	 *            the color for the new Avatar.
	 */
	protected Avatar_(final Individual individual, final Color color) {
		this(individual, new Point(0, 0), color);
	}

	/**
	 * Protected constructor which creates a new instance of Avatar at the given
	 * <code>point</code> and with color black.
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param location
	 *            the location for the new Avatar.
	 * 
	 *            TEST
	 * 
	 */
	protected Avatar_(final Individual individual, final Point location) {
		this(individual, location, Color.black);
	}

	/**
	 * Protected constructor which creates a new instance of Avatar at the given
	 * <code>point</code> and with given <code>color</code>.
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param location
	 *            the location for the new Avatar.
	 * @param color
	 *            the color for the new Avatar.
	 */
	protected Avatar_(final Individual individual, final Point location, final Color color) {
		super();
		this._individual = individual;
		// XXX check that these two calls are OK.
		moveTo(location);
		setColor(color);
	}

	/**
	 * This is purely for debugging/logging purposes.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		// TODO consider using standard form of equals
		if (obj instanceof Avatar_) {
			return getIndividual().equals(((Avatar_) obj).getIndividual());
		}
		return false;
	}

	/**
	 * @return {@link #color}
	 * 
	 *         TEST
	 * 
	 */
	@Override
	@ToString(formString = "showColor")
	public Color getColor() {
		return this.color;
	}

	/**
	 * @return {@link #_individual}
	 * @see net.sf.darwin.ui.base.Avatar#getIndividual()
	 */
	@Override
	public Individual getIndividual() {
		return this._individual;
	}

	/**
	 * @return {@link #location}
	 * 
	 *         TEST
	 * 
	 */
	@Override
	@ToString(formString = "showLocation")
	public Point getLocation() {
		return this.location;
	}

	/**
	 * By default, we simply return 1.
	 * 
	 * @see net.sf.darwin.ui.base.Avatar#getSize()
	 */
	@Override
	@ToString(formString = "showSize")
	public double getSize() {
		return 1.0;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getIndividual().hashCode();
	}

	/**
	 * Not a public method unless Moveable is implemented.
	 * 
	 * @param point
	 * 
	 */
	protected void moveTo(final Point point) {
		this.location = point;
	}

	/**
	 * Not a public method unless Colorable is implemented.
	 * 
	 * @param color
	 */
	protected void setColor(final Color color) {
		this.color = color;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3347709500966312012L;

	/**
	 * 
	 */
	static public final NumberFormat numberFormatter = NumberFormat.getNumberInstance();

	{
		numberFormatter.setMaximumFractionDigits(2);
	}

	private final Individual _individual;

	/**
	 * Not final in case a subclass wants to implement Moveable.
	 */
	private Point location;

	/**
	 * Not final in case a subclass wants to implement Colorable.
	 */
	private Color color;

	/**
	 * @param color
	 * @return a String of the form {r:g:b} where r, g and b are the integers
	 *         representing the red, green and blue values.
	 */
	public static String showColor(final Color color) {
		return "{" + color.getRed() + ":" + color.getGreen() + ":" + color.getBlue() + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	/**
	 * @param location
	 * @return a String representing the location
	 */
	public static String showLocation(final Point location) {
		return "[" + location.x + "," + location.y + "]"; //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
	}

	/**
	 * @param size
	 * @return a String representing the size
	 */
	public static String showSize(final Double size) {
		return numberFormatter.format(size);
	}

}
