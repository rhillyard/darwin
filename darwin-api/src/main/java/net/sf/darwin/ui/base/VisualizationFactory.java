/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizationFactory.java
 * Created on Feb 19, 2007
 * @version $Revision: 1.12 $
 */

package net.sf.darwin.ui.base;

import java.awt.Color;
import java.awt.Point;
import java.util.Map;

import net.sf.darwin.core.Individual;

/**
 * Defines the methods required to construct objects necessary for visualization
 * of a particular application.
 * 
 * @author Robin Hillyard
 */
public interface VisualizationFactory {

	/**
	 * @return the height of the space available for visualization.
	 * 
	 *         XXX not strictly necessary.
	 */
	public int getHeight();

	/**
	 * @return the width of the space available for visualization.
	 * 
	 *         XXX not strictly necessary.
	 */
	public int getWidth();

	/**
	 * Method to create a new {@link Avatar}, based on an organism.
	 * 
	 * @param individual
	 *            the individual to be represented by the {@link Avatar}.
	 * @param properties
	 *            the properties of the visualization model
	 * @return a newly constructed {@link Avatar}.
	 */
	public abstract Avatar makeAvatar(Individual individual, Map<String, Object> properties);

	/**
	 * Method to determine a suitable color for visualizing the organism
	 * 
	 * @param individual
	 * @return the appropriate color
	 */
	public abstract Color makeColor(Individual individual);

	/**
	 * Method to determine a suitable location for visualizing the organism
	 * 
	 * @param individual
	 * @return the appropriate location
	 */
	public abstract Point makeLocation(Individual individual);

	/**
	 * set the height of the space available for visualization.
	 * 
	 * @param height
	 */
	public void setHeight(int height);

	/**
	 * the width of the space available for visualization.
	 * 
	 * @param width
	 */
	public void setWidth(int width);
}
