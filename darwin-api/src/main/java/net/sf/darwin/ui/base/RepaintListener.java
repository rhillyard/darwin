/**
 * Cryptobase Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Module: Area.java
 * Created on Apr 8, 2012
 */

package net.sf.darwin.ui.base;

import java.io.Serializable;

/**
 * This interface defines the concept of something that can be repainted.
 * 
 * @author Robin
 * 
 */
public interface RepaintListener extends Serializable {

	/**
	 * perform the appropriate repaint for this object.
	 */
	public abstract void doRepaint();

}
