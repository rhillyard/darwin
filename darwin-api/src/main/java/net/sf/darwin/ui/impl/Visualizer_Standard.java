/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Visualizer_Standard.java
 * Created on Feb 20, 2007
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.ui.impl;

import net.sf.darwin.ui.base.Painter;
import net.sf.darwin.ui.base.VisualizationModel;
import net.sf.darwin.ui.base.Visualizer_;

/**
 * Default implementation of Visualizer.
 * 
 * @author Robin Hillyard
 */
public final class Visualizer_Standard extends Visualizer_ {

	/**
	 * Public constructor.
	 * 
	 * @param model
	 *            the model to support the visualization.
	 * @param painter
	 *            the painter which will actually draw the visualization.
	 */
	public Visualizer_Standard(final VisualizationModel model, final Painter painter) {
		super(model, painter);
	}

	private static final long serialVersionUID = -5816820084008980235L;

}
