/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: OptionsPanel.java
 * Created on Mar 23, 2009
 * @version $Revision: 1.9 $
 */

package net.sf.darwin.ui.base;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Map;

import net.sf.darwin.core.HasExpressions;

/**
 * @author Robin Hillyard
 * 
 */
public interface OptionsPanel extends ActionListener {

	/**
	 * @param label
	 *            this is used for both labeling the component and as the action
	 *            command
	 * @param component
	 */
	public abstract void addActionableComponentWithLabel(String label, Component component);

	/**
	 * @return the map of dependent bean properties
	 */
	public Map<String, String> getDependentBeanProperties();

	/**
	 * @param componentMap
	 * @param formulaBeans
	 *            XXX
	 */
	public abstract void setComponentMap(Map<String, Component> componentMap, Collection<? extends HasExpressions> formulaBeans);

}