/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ControlAction.java
 * Created on Feb 12, 2007
 * @version $Revision: 1.13 $
 */

package net.sf.darwin.ui.base;

/**
 * Defines two callback methods: setState (which takes one String parameter);
 * and setProperty (which takes a String name and a value);
 * 
 * There are two pre-defined states "stop" and "start".
 * 
 * @author Robin Hillyard
 */
public interface ControlAction {

	/**
	 * Method to set a property with given <code>name</code> and
	 * <code>value</code>.
	 * 
	 * @param name
	 * @param value
	 * @throws VisualizationException
	 *             XXX
	 */
	public abstract void setProperty(String name, Object value) throws VisualizationException;

	/**
	 * Method to set a <code>state</code>.
	 * 
	 * @param state
	 * @throws VisualizationException
	 *             XXX
	 */
	public abstract void setState(String state) throws VisualizationException;

	/**
	 * @param propertyName
	 * @return true if the property named is settable
	 */
	public abstract boolean settable(String propertyName);

	/**
	 * 
	 */
	public static final String STOP = Messages.getString("ControlAction.0"); //$NON-NLS-1$

	/**
	 * 
	 */
	public static final String START = Messages.getString("ControlAction.1"); //$NON-NLS-1$

	/**
	 * 
	 */
	public static final String TIME_DELAY = Messages.getString("ControlAction.2"); //$NON-NLS-1$

	/**
	 * 
	 */
	public static final String QUIT = Messages.getString("ControlAction.4"); //$NON-NLS-1$

	/**
	 * next
	 * 
	 * TODO get this from Messages
	 */
	public static final String NEXT = "next"; //$NON-NLS-1$
}
