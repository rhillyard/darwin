package net.sf.darwin.ui.base;

import java.awt.Component;

/**
 * FIXME move this and any other AWT or Swing related classes to the UI module.
 * 
 * @author Robin
 * 
 */
public final class RepaintListener_Component implements RepaintListener {
	/**
	 * @param component
	 *            XXX
	 * 
	 */
	public RepaintListener_Component(final Component component) {
		super();
		this._component = component;
	}

	/**
	 * @see net.sf.darwin.ui.base.RepaintListener#doRepaint()
	 */
	@Override
	public void doRepaint() {
		System.out.println("doRepaint() called");
		_component.repaint();
	}

	private static final long serialVersionUID = 2006730724690187625L;

	private transient final Component _component;
}