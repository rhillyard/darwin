/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TextField.java
 * Created on Apr 16, 2009
 * @version $Revision: 1.6 $
 */

package net.sf.darwin.ui.impl.swing;

import javax.swing.JTextField;

import net.sf.darwin.ui.base.TextMessageable;

/**
 * @author Robin Hillyard
 * 
 */
public class TextField extends JTextField {

	/**
	 * @param text
	 *            the initial value for the text
	 * @param listener
	 *            an object which is interested to hear about changes the user
	 *            makes to this text field
	 */
	public TextField(final String text, final TextMessageable listener) {
		super(text);
		this._listener = listener;
	}

	/**
	 * @return {@link #_listener}, the listener as set in the constructor
	 */
	public TextMessageable getListener() {
		return this._listener;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7725551678782395138L;

	private final TextMessageable _listener;

}
