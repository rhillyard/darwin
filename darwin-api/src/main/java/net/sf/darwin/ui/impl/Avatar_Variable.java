/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Avatar_Variable.java
 * Created on Jun 5, 2009
 * @version $Revision: 1.10 $
 */

package net.sf.darwin.ui.impl;

import java.awt.Color;
import java.awt.Point;

import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.core.Mortal;
import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.base.Avatar_;
import net.sf.tostring0.ToString;

/**
 * TODO This implementer of {@link Avatar} is somewhat application-specific for
 * the Peppered Moth. We should really update the color and size of the
 * individual objects as we go, rather than override the getColor() and
 * getSize() methods.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = false)
public class Avatar_Variable extends Avatar_ {

	/**
	 * Default scope constructor (to be called from AvatarFactory).
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param location
	 * @param color
	 */
	public Avatar_Variable(final Individual individual, final Point location, final Color color) {
		super(individual, location, color);
	}

	/**
	 * @see com.rubecula.darwin.visualization.Avatar_#getColor()
	 */
	@Override
	@ToString(formString = "showColor")
	public Color getColor() {
		final Color result = super.getColor();
		if (!getIndividual().isVisible()) {
			return new Color(result.getBlue(), result.getBlue() / 2, 0);
		}
		return result;
	}

	/**
	 * @return (age + 1) / 2
	 * @see net.sf.darwin.ui.base.Avatar#getSize()
	 */
	@Override
	@ToString(formString = "showSize")
	public double getSize() {
		final Individual individual = getIndividual();
		// TODO consider doing this via polymorphism
		if (individual instanceof Mortal)
			return (((Mortal) individual).getAge() + 2) * 0.2;
		return 1.0;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5119559251924394210L;

}
