/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: AvatarFactory.java
 * Created on May 27, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.ui.factory;

import java.awt.Color;
import java.awt.Point;

import net.sf.darwin.core.Individual;
import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.impl.Avatar_Variable;

/**
 * Factory Class to create {@link Avatar} objects.
 * 
 * Applications can't change this class (it is final) but they can override the
 * methods which call these methods.
 * 
 * @author Robin Hillyard
 * 
 */
public final class AvatarFactory {

	/**
	 * Private constructor for Factory Class
	 */
	private AvatarFactory() {
		super();
	}

	/**
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param location
	 * @param color
	 * @return a newly constructed {@link Avatar_Variable}.
	 */
	public static Avatar makeAvatar(final Individual individual, final Point location, final Color color) {
		return new Avatar_Variable(individual, location, color);
	}

}
