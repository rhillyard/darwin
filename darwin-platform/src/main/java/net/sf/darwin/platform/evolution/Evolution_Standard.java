/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution_Standard.java
 * Created on Dec 3, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.platform.evolution;

import net.sf.darwin.core.Lifecycle;

/**
 * This concrete class extends the notion of an {@link Evolution_}. Time
 * properties for this class are real time. The start is always the time of the
 * constructor call.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
public final class Evolution_Standard extends Evolution_Timed {

	/**
	 * Construct an Evolution_Standard with the given value of the run
	 * parameter.
	 * 
	 * @param run
	 */
	public Evolution_Standard(final boolean run) {
		super(run);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4016057109898597187L;

}
