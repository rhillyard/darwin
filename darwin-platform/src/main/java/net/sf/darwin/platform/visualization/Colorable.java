/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Colorable.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.8 $
 */

package net.sf.darwin.platform.visualization;

import java.awt.Color;

/**
 * Defines method to change the color of a model object.
 * 
 * TODO this is a little weird. Probably should eliminate it.
 * 
 * @author Robin Hillyard
 */
public interface Colorable {

	/**
	 * Mutating method to set the color to that given.
	 * 
	 * @param color
	 *            the new color.
	 */
	public abstract void setColor(Color color);
}
