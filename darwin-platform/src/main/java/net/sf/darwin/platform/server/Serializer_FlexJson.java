/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: Serializer_FlexJson
 * Created on: Mar 14, 2012
 */

package net.sf.darwin.platform.server;

/**
 * TODO OBSOLETE
 * 
 * @author rhillya
 * 
 */
public class Serializer_FlexJson { // implements Serializer {
	//
	// private final JSONSerializer _serializer;
	//
	// /**
	// * @param target
	// * @param out
	// * @see flexjson.JSONSerializer#serialize(java.lang.Object,
	// java.io.Writer)
	// */
	// @Override
	// public void serialize(final Object target, final Writer out) {
	// this._serializer.serialize(target, out);
	// }
	//
	// /**
	// * @param target
	// * @return
	// * @see flexjson.JSONSerializer#serialize(java.lang.Object)
	// */
	// @Override
	// public String serialize(final Object target) {
	// return this._serializer.serialize(target);
	// }
	//
	// /**
	// * @param target
	// * @param out
	// * @return
	// * @see flexjson.JSONSerializer#deepSerialize(java.lang.Object,
	// * java.lang.StringBuilder)
	// */
	// @Override
	// public String deepSerialize(final Object target, final StringBuilder out)
	// {
	// return this._serializer.deepSerialize(target, out);
	// }
	//
	// /**
	// * @param target
	// * @param out
	// * @see flexjson.JSONSerializer#deepSerialize(java.lang.Object,
	// * java.io.Writer)
	// */
	// @Override
	// public void deepSerialize(final Object target, final Writer out) {
	// this._serializer.deepSerialize(target, out);
	// }
	//
	// /**
	// * @param arg0
	// * @see flexjson.JSONSerializer#setExcludes(java.util.List)
	// */
	// @Override
	// public void setExcludes(final List<String> arg0) {
	// this._serializer.setExcludes(arg0);
	// }
	//
	// /**
	// * @param arg0
	// * @see flexjson.JSONSerializer#setIncludes(java.util.List)
	// */
	// @Override
	// public void setIncludes(final List<String> arg0) {
	// this._serializer.setIncludes(arg0);
	// }
	//
	// /**
	// * @return
	// * @see java.lang.Object#toString()
	// */
	// @Override
	// public String toString() {
	//		return "Serializer using " + this._serializer.getClass(); //$NON-NLS-1$
	// }
	//
	// public Serializer_FlexJson() {
	// super();
	// this._serializer = new JSONSerializer();
	// }

}
