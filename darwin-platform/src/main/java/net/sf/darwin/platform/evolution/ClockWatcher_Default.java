package net.sf.darwin.platform.evolution;

import java.io.PrintWriter;
import java.util.Calendar;

/**
 * @author Robin Hillyard
 * 
 */
final public class ClockWatcher_Default extends ClockWatcher_ {

	/**
	 * 
	 */
	public ClockWatcher_Default() {
		super();
	}

	/**
	 * @param writer
	 */
	public ClockWatcher_Default(final PrintWriter writer) {
		super(writer);
	}

	/**
	 * @see net.sf.darwin.core.ClockWatcher#onTick(java.util.Calendar)
	 */
	@Override
	public void onTick(final Calendar time) {
		Evolver_.showTime(time, getWriter());
	}
}