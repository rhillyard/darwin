/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: EvolutionServer
 * Created on: Mar 13, 2012
 */

package net.sf.darwin.platform.server;

import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.GenerationListener;

/**
 * TODO OBSOLETE
 * 
 * @author rhillya
 * 
 */
public class EvolutionServer implements GenerationListener, Runnable {

	public EvolutionServer() {
		super();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.core.GenerationListener#onGeneration(net.sf.darwin.core.Evolvable)
	 */
	@Override
	public void onGeneration(final Evolvable evolvable) {
		// XXX Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// XXX Auto-generated method stub

	}

}
