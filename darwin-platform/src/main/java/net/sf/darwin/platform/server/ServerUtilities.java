/**
 * Cryptobase Project.
 * Copyright (C) 2010  UnitedHealth Group
 *
 * Module: ServerUtilities.java
 * Created on Jan 6, 2011
 */

package net.sf.darwin.platform.server;

import java.io.IOException;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;

/**
 * TODO OBSOLETE
 * 
 * @author rhillya
 * 
 */
public class ServerUtilities {
	private static final String SSL_DH_ANON_WITH_RC4_128_MD5 = "SSL_DH_anon_WITH_RC4_128_MD5"; //$NON-NLS-1$

	// /**
	// * @param server
	// * the server name
	// * @param port
	// * on which to open the socket
	// * @return a newly constructed SSLSocket
	// * @throws IOException
	// * @throws UnknownHostException
	// */
	// public static Socket openSSLSocket(final String server, final int port)
	// throws IOException, UnknownHostException {
	// final Socket socket = SocketFactory.createSocket(server, port);
	// ((SSLSocket) socket).setEnabledCipherSuites(EnabledCipherSuites);
	// return socket;
	// }

	private static SocketFactory SocketFactory = SSLSocketFactory.getDefault();

	private static ServerSocketFactory ServerSocketFactory = SSLServerSocketFactory.getDefault();

	/**
	 * Use an anonymous cipher suite so that a KeyManager or TrustManager is not
	 * needed, well for now anyway.
	 * 
	 * TODO implement the key stuff properly.
	 * 
	 * NOTE: this assumes that the cipher suite is known. A check should be done
	 * first.
	 * 
	 * With thanks to Woozle Wuzzle, see
	 * http://www.realityinteractive.com/rgrzywinski/archives/000082.html
	 * 
	 */
	public static String[] EnabledCipherSuites = { SSL_DH_ANON_WITH_RC4_128_MD5 };

	/**
	 * @param port
	 *            the port on which to listen
	 * @return a newly constructed SSLServerSocket
	 * @throws IOException
	 */
	public static ServerSocket openSSLServerSocket(final int port) throws IOException {
		final ServerSocket serverSocket = ServerSocketFactory.createServerSocket(port);
		((SSLServerSocket) serverSocket).setEnabledCipherSuites(EnabledCipherSuites);
		return serverSocket;
	}

}
