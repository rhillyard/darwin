/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Avatar_ColorLocation.java
 * Created on Feb 9, 2007
 * @version $Revision: 1.7 $
 */

package net.sf.darwin.platform.visualization;

import java.awt.Color;
import java.awt.Point;

import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Lifecycle;
import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.base.Avatar_;

/**
 * A trivial implementation of {@link Avatar} by extending {@link Avatar_} with
 * no further methods or fields.
 * 
 * @author Robin Hillyard
 */
@Lifecycle(permanent = false)
public final class Avatar_ColorLocation extends Avatar_ {

	/**
	 * Public constructor which invokes {@link Avatar_#Avatar_(Individual)}
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 */
	Avatar_ColorLocation(final Individual individual) {
		super(individual);
	}

	/**
	 * Public constructor which invokes
	 * {@link Avatar_#Avatar_(Individual, Color)}
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param color
	 */
	Avatar_ColorLocation(final Individual individual, final Color color) {
		super(individual, color);
	}

	/**
	 * Public constructor which invokes
	 * {@link Avatar_#Avatar_(Individual, Point)}
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param location
	 */
	Avatar_ColorLocation(final Individual individual, final Point location) {
		super(individual, location);
	}

	/**
	 * Public constructor which invokes
	 * {@link Avatar_#Avatar_(Individual, Point, Color)}
	 * 
	 * @param individual
	 *            the individual which is modeled by this avatar.
	 * @param location
	 * @param color
	 */
	Avatar_ColorLocation(final Individual individual, final Point location, final Color color) {
		super(individual, location, color);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7057711024401488993L;

}
