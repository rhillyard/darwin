/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: TimeUnit.java
 * Created on Feb 26, 2010
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.platform.evolution;

/**
 * @author Robin Hillyard
 * 
 */
public enum TimeUnit {
	/**
	 * 
	 */
	Second(1), /**
	 * 
	 */
	Minute(60), /**
	 * 
	 */
	Hour(60 * Minute.seconds()), /**
	 * 
	 */
	Day(24 * Hour.seconds()), /**
	 * 
	 */
	Year(365.256363004 * Day.seconds()),
	/**
	 * 
	 */
	MilliSecond(0.001 * Second.seconds()), /**
	 * 
	 */
	MicroSecond(0.001 * MilliSecond.seconds());

	TimeUnit(final double f) {
		this._f = f;
	}

	double seconds() {
		return this._f;
	}

	private double _f;
}
