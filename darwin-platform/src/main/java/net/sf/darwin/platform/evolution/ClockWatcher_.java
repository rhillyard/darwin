/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: ClockWatcher_.java
 * Created on Feb 26, 2010
 * @version $Revision: 1.3 $
 */

package net.sf.darwin.platform.evolution;

import java.io.PrintWriter;

import net.sf.darwin.core.ClockWatcher;

import com.rubecula.beanpot.BeanPot;

/**
 * @author Robin Hillyard
 * 
 */
public abstract class ClockWatcher_ implements ClockWatcher {
	/**
	 * 
	 */
	protected ClockWatcher_() {
		this(BeanPot.getInstance().getWriter());
	}

	/**
	 * @param writer
	 *            XXX
	 * 
	 */
	protected ClockWatcher_(final PrintWriter writer) {
		super();
		this._writer = writer;
	}

	/**
	 * @return the writer
	 */
	protected PrintWriter getWriter() {
		return this._writer;
	}

	private final PrintWriter _writer;

}
