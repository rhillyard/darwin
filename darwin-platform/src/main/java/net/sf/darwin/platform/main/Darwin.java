/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Darwin.java
 * Created on Oct 4, 2009
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.platform.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.OptionMap;
import net.sf.darwin.api.base.OptionSource_ArgVector;
import net.sf.darwin.ui.impl.swing.OptionDefaulter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.beanpot.ArgVector;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_List;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_ObjectMap;

/**
 * Class to start up a Darwin application (not an applet).
 * 
 * <p>
 * The syntax of the command line parameters is:
 * 
 * <pre>
 * Darwin[-d][-gN][configurationfilename]
 * </pre>
 * 
 * The configuration filename should be specified as a full path name in the
 * following form: <code>/C:/.../../file.xml</code> (yes, you can use the
 * forward slashes on Windows). If you do not start the filename with a "/"
 * character, the system will look for the configuration file in the
 * <code>com.rubecula.darwin.startup</code> package under the
 * <code>/src/main/resoure/</code> source folder. This is useful for running
 * demo programs provided by Darwin contributors.
 * 
 * Currently, there are only two such demos, called outOfTheBox.xml (the default
 * demo if you don't specify a configuration file at all) and
 * travelingSalesman.xml.
 * 
 * There are two other optional parameters:
 * </p>
 * <p>
 * <dl>
 * <dt>-d</dt>
 * <dd>(which sets debug mode)</dd>
 * <dt>-gN</dt>
 * <dd>where N is the maximum number of generations you'd like to run (by
 * default it will run forever, or until some other condition terminates it).</dd>
 * </dl>
 * </p>
 * 
 * <p>
 * Note that this class is designed for use with the BeanPot as the bean
 * container. However, by changing the class {@link BeanContainer}, and making
 * some other relatively minor changes in this class, it should be possible to
 * use Spring or some other bean container instead.
 * 
 * </p>
 * 
 * TODO use typeof where appropriate
 * 
 * @author Robin Hillyard
 * 
 */
public class Darwin implements OptionDefaulter {

	/**
	 * @param argVector
	 *            XXX
	 * 
	 */
	public Darwin(final ArgVector argVector) {
		super();
		this._argVector = argVector;
		this._beanContainer = new BeanContainer_BeanPot();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.ui.impl.swing.OptionDefaulter#createDefaultOptionMap(java.lang.String)
	 */
	@Override
	public Map<String, Object> createDefaultOptionMap(final String configurationClass) {
		final HashMap<String, Object> result = new HashMap<String, Object>();
		result.put(OPT_CONFIG_FILE, CONFIG_OUT_OF_THE_BOX);
		result.put(OPT_GENERATIONS, Long.valueOf(0));
		result.put(OPT_CONFIG_CLASS, configurationClass);
		return result;
	}

	// /**
	// * @param optionDefaults
	// * XXX
	// * @return
	// */
	// @Override
	// public OptionMap<?, Object> getOptions(final Map<String, Object>
	// optionDefaults) {
	// final OptionSource_ArgVector<Object> optionSource_ArgVector = new
	// OptionSource_ArgVector<Object>(this._argVector,
	// new ArgVectorProcessor_ObjectMap());
	// final OptionMap<?, Object> result = new OptionMap<Object,
	// Object>(optionDefaults, optionSource_ArgVector);
	// this._argVector.process(new ArgVectorProcessor_ObjectMap(),
	// optionDefaults);
	// final Collection<String> ignored =
	// getRemainingArguments(this._argVector);
	// if (ignored.size() > 0)
	//			System.err.println("Ignoring the following command-line arguments: "); //$NON-NLS-1$
	// for (final String string : ignored)
	//			System.err.println("    " + string); //$NON-NLS-1$
	// return result;
	// }

	private final ArgVector _argVector;

	public static final String OPT_DEBUG = "d"; //$NON-NLS-1$

	public static final String OPT_VALIDATE = "v"; //$NON-NLS-1$

	public static final String OPT_GENERATIONS = "gen"; //$NON-NLS-1$

	public static final String OPT_CONFIG_FILE = "configFile"; //$NON-NLS-1$

	public static final String OPT_CONFIG_CLASS = "configClass"; //$NON-NLS-1$

	public static final Object OPT_JAR = "jar"; //$NON-NLS-1$

	/**
	 * 
	 */
	protected static final String CONFIG_OUT_OF_THE_BOX = "outOfTheBox.xml"; //$NON-NLS-1$

	protected static final Log LOG = LogFactory.getLog(Darwin.class);

	private final BeanContainer _beanContainer;

	//	private static final String SYNTAX = "Syntax: Darwin [-d] [-g N] [-v [dtdUrl]] [configurationFile]"; //$NON-NLS-1$

	/**
	 * Darwin's own class loader.
	 */
	public static class DarwinClassLoader extends ClassLoader {

		/**
		 * @param logIt
		 *            XXX
		 * 
		 */
		public DarwinClassLoader(final boolean logIt) {
			super();
			this._logIt = logIt;
		}

		/**
		 * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
		 */
		@Override
		protected synchronized Class<?> loadClass(final String name, final boolean resolve) throws ClassNotFoundException {
			// TODO this does not work as expected: even after a class has been
			// loaded, the findLoadedClass method returns null, presumably
			// because it is in the parent class loader that the class has
			// actually been loaded previously.
			if (this._logIt && ((Class<?>) findLoadedClass(name) == null))
				System.out.println("Darwin loading class " + name); //$NON-NLS-1$				
			return super.loadClass(name, resolve);
		}

		private final boolean _logIt;

	}

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		LOG.info("Darwin application begun using args:"); //$NON-NLS-1$
		for (final String arg : args)
			LOG.info(arg);
		final ArgVector argVector = new ArgVector(args);
		final Darwin darwin = new Darwin(argVector);
		final OptionMap<Object, Object> optionMap = new OptionMap<Object, Object>(darwin.createDefaultOptionMap(Darwin.class
				.getCanonicalName()), new OptionSource_ArgVector<Object>(argVector, new ArgVectorProcessor_ObjectMap()));
		darwin._beanContainer.doMain(optionMap, true);
	}

	/**
	 * @param argVector
	 * @return
	 */
	private static ArrayList<String> getRemainingArguments(final ArgVector argVector) {
		final ArrayList<String> ignored = new ArrayList<String>();
		argVector.process(new ArgVectorProcessor_List(), ignored);
		return ignored;
	}

}
