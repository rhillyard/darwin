/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: VisualizationModel_.java
 * Created on Feb 4, 2007
 * @version $Revision: 1.32 $
 */

package net.sf.darwin.platform.visualization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.darwin.core.Base;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.base.RepaintListener;
import net.sf.darwin.ui.base.VisualizationFactory;
import net.sf.darwin.ui.base.VisualizationModel;
import net.sf.tostring0.ToString;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Base implementation of fields/methods for a {@link VisualizationModel}. In
 * particular, this class maintains the following properties:
 * <ul>
 * <li>{@link #_title} (a String representing the title of this model</li>
 * <li>{@link #component} (a component which should be repainted if this model
 * changes)</li>
 * <li>{@link #_avatars} (a list of {@link Avatar} objects</li>
 * <li>{@link #_attributes} (a list of {@link Map} objects</li>
 * <li>{@link #visible} (a boolean representing the current visibility of this
 * model</li>
 * </ul>
 * 
 * Subclasses should have permanent lifespan.
 * 
 * @author Robin Hillyard
 */
public abstract class VisualizationModel_ extends Base implements VisualizationModel {

	/**
	 * Protected constructor which creates new instances for the
	 * {@link #_avatars} and {@link #_attributes} properties. It also sets the
	 * {@link #_title} as given by <code>title</code> and initializes
	 * {@link #visible} to true.
	 * 
	 * @param title
	 *            the title for this model.
	 */
	protected VisualizationModel_(final String title) {
		super();
		// we use Vector because it should be synchronized
		// _avatars = new Vector<Avatar>();
		// we use Hashtable because it should be synchronized
		this._avatars = new Hashtable<Individual, Avatar>();
		this._attributes = new HashMap<String, Object>();
		this._title = title;
		this.visible = true;
	}

	/**
	 * Get the current count, then add the individual. Return the original count
	 * as the new index.
	 * 
	 * @see VisualizationModel#addAvatar(Avatar)
	 */
	@Override
	public synchronized void addAvatar(final Avatar avatar) {
		// final int index = getCount();
		getAvatarMap().put(avatar.getIndividual(), avatar);
		// if (_avatars.add(individual))
		// return index;
	}

	/**
	 * Loop through all the individuals and remove any which are dead and
	 * buried.
	 * 
	 * @return the number remaining in the list.
	 * 
	 * @see net.sf.darwin.ui.base.VisualizationModel#clean()
	 */
	@Override
	public int clean() {
		final List<Avatar> list = new ArrayList<Avatar>();
		for (final Avatar avatar : getIndividuals()) {
			if (avatar.getIndividual().getVisualizable() == null)
				list.add(avatar);
		}
		for (final Avatar avatar : list) {
			removeIndividual(avatar);
		}
		return list.size();
	}

	/**
	 * @see java.util.ArrayList#clear()
	 */
	@Override
	public void clear() {
		getAvatarMap().clear();
	}

	/**
	 * @see net.sf.darwin.ui.base.VisualizationModel#findAvatar(net.sf.darwin.core.Individual)
	 */
	@Override
	public Avatar findAvatar(final Individual individual) {
		return getAvatarMap().get(individual);
	}

	/**
	 * Notify the listeners that the model has changed. This is always invoked
	 * by some external agency which has made some calls to
	 * {@link #addAvatar(Avatar)} or {@link #removeIndividual(Avatar)}.
	 * 
	 * @see net.sf.darwin.ui.base.VisualizationModel#fireModelChange ()
	 */
	@Override
	public void fireModelChange() {
		if (LOG.isDebugEnabled())
			LOG.debug("VisualizationModel changed: " + getCount() + " individuals " + (getComponent() != null ? "and will repaint" : "and will not repaint")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		if (getComponent() != null)
			getComponent().doRepaint();
	}

	/**
	 * @return {@link #_attributes}
	 * @see net.sf.darwin.ui.base.VisualizationModel#getAttributes()
	 */
	@Override
	public Map<String, Object> getAttributes() {
		return this._attributes;
	}

	/**
	 * @return result of invoking {@link Collection#size()} on the result of
	 *         {@link #getAvatarMap()}.
	 * @see net.sf.darwin.core.Countable#getCount()
	 */
	@Override
	public int getCount() {
		return getAvatarMap().size();
	}

	/**
	 * @return result of invoking {@link Map#values()} on the result of
	 *         {@link #getAvatarMap()}.
	 * @see net.sf.darwin.ui.base.VisualizationModel#getIndividuals()
	 */
	@Override
	public Collection<Avatar> getIndividuals() {
		return getAvatarMap().values();
	}

	/**
	 * @return {@link #_title}
	 * @see net.sf.darwin.ui.base.VisualizationModel#getTitle()
	 */
	@Override
	public String getTitle() {
		return this._title;
	}

	/**
	 * @return {@link #visible}
	 * @see net.sf.darwin.ui.base.VisualizationModel#isVisible()
	 */
	@Override
	public boolean isVisible() {
		return this.visible;
	}

	/**
	 * @see net.sf.darwin.ui.base.VisualizationModel#putAttribute(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public void putAttribute(final String attrName, final Object attrValue) {
		getAttributes().put(attrName, attrValue);
	}

	/**
	 * Remove from the {@link #_avatars} map that avatar which is indexed by the
	 * individual referenced by the parameter <code>avatar</code>.
	 * 
	 * TODO consider passing in Individual instead of Avatar.
	 * 
	 * XXX consider eliminating synchronized since it should be handled by the
	 * _avatars object
	 * 
	 * @see VisualizationModel#removeIndividual(Avatar)
	 */
	@Override
	public synchronized boolean removeIndividual(final Avatar avatar) {
		return getAvatarMap().remove(avatar.getIndividual()) != null;
		// return _avatars.remove(individual);
	}

	/**
	 * 
	 * Getter/setter normally invoked by reflection.
	 * 
	 * @see net.sf.darwin.ui.base.VisualizationModel#setAttributes(java.util.Map)
	 */
	@Override
	public void setAttributes(final Map<String, Object> attributes) {
		for (final Entry<String, Object> e : attributes.entrySet())
			putAttribute(e.getKey(), e.getValue());
	}

	/**
	 * @param component
	 *            the component to set
	 */
	@Override
	public void setRepaintListener(final RepaintListener component) {
		this.component = component;
	}

	/**
	 * 
	 * Getter/setter normally invoked by reflection.
	 * 
	 * XXX check that. Maybe we can eliminate.
	 * 
	 * @see net.sf.darwin.ui.base.VisualizationModel#setVisible(boolean)
	 */
	@Override
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}

	/**
	 * @param visualizable
	 * @param visualizationFactory
	 */
	@Override
	public void visualize(final Visualizable visualizable, final VisualizationFactory visualizationFactory) {
		int netChange = 0;
		synchronized (this) {
			for (final Individual individual : visualizable.getIndividuals()) {
				netChange += VisualizationModel_.updateModel(this, individual, visualizationFactory);
			}
		}
		if (LOG.isDebugEnabled())
			LOG.debug("updated visualization model: net change = " + netChange + " (model for " + visualizable.getIdentifier() + ")"); //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
		fireModelChange();
	}

	/**
	 * @return {@link #component}
	 */
	protected RepaintListener getComponent() {
		return this.component;
	}

	/**
	 * @return {@link #_avatars}
	 */
	private Map<Individual, Avatar> getAvatarMap() {
		return this._avatars;
	}

	private static final long serialVersionUID = -7750649953557151136L;

	/**
	 * this is the component which will be repainted when there is a change to
	 * the model
	 */
	private transient RepaintListener component;

	private final String _title;

	private final Map<String, Object> _attributes;

	private boolean visible;

	@ToString(maxElements = 5)
	final private Map<Individual, Avatar> _avatars;

	protected static final Log LOG = LogFactory.getLog(VisualizationModel_.class);

	/**
	 * XXX consider moving this to VisualizationModel (but beware architecture
	 * constraints).
	 * 
	 * @param model
	 * @param individual
	 * @param factory
	 *            VisualizationFactory to create avatars
	 * @return net change in number of individuals
	 */
	public static int updateModel(final VisualizationModel model, final Individual individual, final VisualizationFactory factory) {
		int netChange = 0;
		final boolean visible = individual.isVisible();
		final Avatar avatar = model.findAvatar(individual);
		final boolean traceEnabled = LOG.isTraceEnabled();
		if (traceEnabled) {
			LOG.trace("updateModel: individual: " + individual + ", with avatar: " + avatar); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (avatar == null) {
			if (visible) {
				if (individual.isNew()) {
					model.addAvatar(factory.makeAvatar(individual, null));
					netChange++;
				}
			}
		} else if (!visible) {
			model.removeIndividual(avatar);
			netChange--;
		}

		return netChange;
	}
}
