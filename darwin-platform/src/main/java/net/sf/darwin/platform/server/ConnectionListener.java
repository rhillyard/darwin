/**
 * Cryptobase Project.
 * Copyright (C) 2010  UnitedHealth Group
 *
 * Module: ConnectionListener.java
 * Created on Dec 9, 2010
 */

package net.sf.darwin.platform.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;

import net.sf.darwin.api.base.ClientServerProtocol;
import net.sf.darwin.core.Evolution;

import org.apache.commons.math.random.RandomAdaptor;

import com.rubecula.beanpot.Configurable;

/**
 * TODO OBSOLETE
 * 
 * @author rhillya
 * 
 */
public class ConnectionListener implements Runnable, Configurable {

	/**
	 * @param evolution
	 *            XXX
	 * @param port
	 *            the port number on which we will listen for connections
	 * 
	 */
	public ConnectionListener(final Evolution evolution, final int port) {
		super();
		this._evolution = evolution;
		setPort(port);
	}

	/**
	 * Output should occur only when in debug mode
	 * 
	 * @param prefix
	 * @param string
	 */
	public void debug(final String prefix, final String string) {
		System.out.println(prefix + string);
	}

	/**
	 * @return the random
	 */
	public RandomAdaptor getRandom() {
		return this.random;
	}

	/**
	 * @return the listening
	 */
	public synchronized boolean isListening() {
		return this.listening;
	}

	/**
	 * @return the shutdown
	 */
	public synchronized boolean isShutdown() {
		return this.shutdown;
	}

	/**
	 * @see com.rubecula.beanpot.Configurable#postConfigure(java.lang.Object)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void postConfigure(final Object postConfigMap) {
		if (postConfigMap instanceof Map<?, ?>)
			((Map<String, Object>) postConfigMap).put("listener", this); //$NON-NLS-1$
	}

	/**
	 * @see com.rubecula.beanpot.Configurable#preConfigure(java.lang.Object)
	 */
	@Override
	public void preConfigure(final Object preConfigMap) {
		// @SuppressWarnings("unchecked")
		//		final Boolean ssl = (Boolean) ((Map<String, Object>) preConfigMap).get("ssl"); //$NON-NLS-1$
		// if (ssl.booleanValue()) {
		// setPort(getPort() + 1);
		// this.serverSocketFactory = new CryptobaseSSLServerSocketFactory();
		// }
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		ServerSocket serverSocket = null;
		try {
			String[] supportedCipherSuites = new String[0];
			serverSocket = this.serverSocketFactory.createServerSocket(getPort());
			if (serverSocket instanceof SSLServerSocket)
				supportedCipherSuites = ((SSLServerSocket) serverSocket).getSupportedCipherSuites();
			System.out.println("Listening for connections on port: " + getPort()); //$NON-NLS-1$
			if (supportedCipherSuites.length > 0)
				System.out.println("... for SSL-secured connections"); //$NON-NLS-1$
			final Collection<Socket> sockets = new ArrayList<Socket>();
			setListening(true);
			while (!isShutdown()) {
				final Socket clientSocket = serverSocket.accept();
				sockets.add(clientSocket);
				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							System.out.println("New connection accepted on " + clientSocket); //$NON-NLS-1$
							if (!registerClient(clientSocket)) {
								System.out.println("Server shutting down"); //$NON-NLS-1$
								setShutdown(true);
								// TODO come up with a more elegant way of
								// shutting down
								System.exit(0);
							}
						} catch (final Exception e) {
							// CONSIDER rewording this message, since most of
							// the time it's not really a listener problem, per
							// se
							System.err.println("Listener problem"); //$NON-NLS-1$
							e.printStackTrace(System.err);
						}
					}
				}).start();
			}
			setListening(false);
			int open = 0;
			for (final Socket socket : sockets) {
				if (!socket.isClosed()) {
					socket.close();
					open++;
				}
			}
			System.out.println("Closed " + open + " sockets"); //$NON-NLS-1$ //$NON-NLS-2$
		} catch (final Exception e) {
			System.err.println("Listener problem"); //$NON-NLS-1$
			e.printStackTrace(System.err);
		} finally {
			if (serverSocket != null)
				try {
					serverSocket.close();
					System.out.println("Server has shut down"); //$NON-NLS-1$
				} catch (final IOException e) {
					System.err.println("Problem closing server socket"); //$NON-NLS-1$
					e.printStackTrace(System.err);
				}
		}
	}

	/**
	 * @param rndm
	 *            the random to set
	 */
	public void setRandom(final RandomAdaptor rndm) {
		this.random = rndm;
	}

	/**
	 * @param shdn
	 *            the shutdown to set
	 */
	public synchronized void setShutdown(final boolean shdn) {
		this.shutdown = shdn;
	}

	/**
	 * @param clientSocket
	 * @return
	 * @throws IOException
	 */
	protected boolean registerClient(final Socket clientSocket) throws IOException {
		boolean ok = true;
		final PrintWriter socketOut = new PrintWriter(clientSocket.getOutputStream(), true);
		final BufferedReader socketIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		final PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
		final String greeting = new String(ClientServerProtocol.GREETING_SERVER);
		socketOut.println(greeting.toString());
		debug("sent: ", greeting); //$NON-NLS-1$

		String inputLine;
		while ((inputLine = socketIn.readLine()) != null) {
			debug("received: ", inputLine); //$NON-NLS-1$
			if (inputLine.equals(ClientServerProtocol.GREETING_CLIENT))
				this._evolution.writeEvolvables(writer);
			if (inputLine.equals(ClientServerProtocol.QUIT)) {
				socketOut.println(ClientServerProtocol.GOODBYE);
				break;
			}
			if (inputLine.equals(ClientServerProtocol.SHUTDOWN)) {
				socketOut.println(ClientServerProtocol.GOODBYE);
				ok = false;
				break;
			}
		}
		socketOut.close();
		socketIn.close();
		clientSocket.close();
		System.out.println("socket " + clientSocket + //$NON-NLS-1$
				" shut down"); //$NON-NLS-1$
		return ok;
	}

	/**
	 * @return the port
	 */
	private int getPort() {
		return this._port;
	}

	/**
	 * @param lstng
	 *            the listening to set
	 */
	private synchronized void setListening(final boolean lstng) {
		this.listening = lstng;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	private void setPort(final int port) {
		this._port = port;
	}

	private final Evolution _evolution;

	private boolean listening = false;

	private boolean shutdown = false;

	private RandomAdaptor random;

	private int _port;

	private final ServerSocketFactory serverSocketFactory = ServerSocketFactory.getDefault();
}
