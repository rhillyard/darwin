/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Darwin.java
 * Created on Oct 4, 2009
 * @version $Revision: 1.18 $
 */

package net.sf.darwin.platform.main;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.EvolutionService;
import net.sf.darwin.api.base.OptionMap;
import net.sf.darwin.api.base.OptionSource_ArgVector;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.GenerationListener;
import net.sf.darwin.core.VisualizableListener;
import net.sf.darwin.ui.base.RepaintListener;
import net.sf.darwin.ui.base.VisualizationModel;
import net.sf.darwin.ui.impl.swing.OptionDefaulter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cojen.dirmi.Environment;

import com.rubecula.beanpot.ArgVector;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_List;
import com.rubecula.beanpot.argvector.ArgVectorProcessor_ObjectMap;

/**
 * Class to start up a Darwin application (not an applet).
 * 
 * <p>
 * The syntax of the command line parameters is:
 * 
 * <pre>
 * Darwin[-d][-gN][configurationfilename]
 * </pre>
 * 
 * The configuration filename should be specified as a full path name in the
 * following form: <code>/C:/.../../file.xml</code> (yes, you can use the
 * forward slashes on Windows). If you do not start the filename with a "/"
 * character, the system will look for the configuration file in the
 * <code>com.rubecula.darwin.startup</code> package under the
 * <code>/src/main/resoure/</code> source folder. This is useful for running
 * demo programs provided by Darwin contributors.
 * 
 * Currently, there are only two such demos, called outOfTheBox.xml (the default
 * demo if you don't specify a configuration file at all) and
 * travelingSalesman.xml.
 * 
 * There are two other optional parameters:
 * </p>
 * <p>
 * <dl>
 * <dt>-d</dt>
 * <dd>(which sets debug mode)</dd>
 * <dt>-gN</dt>
 * <dd>where N is the maximum number of generations you'd like to run (by
 * default it will run forever, or until some other condition terminates it).</dd>
 * </dl>
 * </p>
 * 
 * <p>
 * Note that this class is designed for use with the BeanPot as the bean
 * container. However, by changing the class {@link BeanContainer}, and making
 * some other relatively minor changes in this class, it should be possible to
 * use Spring or some other bean container instead.
 * 
 * </p>
 * 
 * TODO use typeof where appropriate
 * 
 * FIXME remove OptionDefault dependence
 * 
 * @author Robin Hillyard
 * 
 */
public class DarwinPlatform implements OptionDefaulter, EvolutionService {

	/**
	 * @param argVector
	 *            XXX
	 * 
	 */
	public DarwinPlatform(final ArgVector argVector) {
		super();
		this._argVector = argVector;
		this._beanContainer = new BeanContainer_BeanPot();
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#addGenerationListener(java.lang.String,
	 *      net.sf.darwin.core.GenerationListener)
	 */
	@Override
	public void addGenerationListener(final String handle, final GenerationListener listener) throws RemoteException {
		System.out.println("addGenerationListener() invoked remotely");
		final Evolution evolution = _evolutionHandleMap.get(handle);
		if (evolution != null)
			evolution.addListener(listener);
		else
			throw new RemoteException("evolution handle is not known");
	}

	// /**
	// * @param optionDefaults
	// * XXX
	// * @return
	// */
	// @Override
	// public OptionMap<?, Object> getOptions(final Map<String, Object>
	// optionDefaults) {
	// final OptionSource_ArgVector<Object> optionSource_ArgVector = new
	// OptionSource_ArgVector<Object>(this._argVector,
	// new ArgVectorProcessor_ObjectMap());
	// final OptionMap<?, Object> result = new OptionMap<Object,
	// Object>(optionDefaults, optionSource_ArgVector);
	// this._argVector.process(new ArgVectorProcessor_ObjectMap(),
	// optionDefaults);
	// final Collection<String> ignored =
	// getRemainingArguments(this._argVector);
	// if (ignored.size() > 0)
	//			System.err.println("Ignoring the following command-line arguments: "); //$NON-NLS-1$
	// for (final String string : ignored)
	//			System.err.println("    " + string); //$NON-NLS-1$
	// return result;
	// }

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#addVisualizingListener(java.lang.String,
	 *      net.sf.darwin.core.VisualizableListener)
	 */
	@Override
	public void addVisualizingListener(final String handle, final VisualizableListener listener) throws RemoteException {
		System.out.println("addVisualizingListener() invoked remotely");
		final Evolution evolution = _evolutionHandleMap.get(handle);
		if (evolution != null)
			evolution.addVisualizableListener(listener);
		else
			throw new RemoteException("evolution handle is not known");
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#close()
	 */
	@Override
	public void close() throws RemoteException {
		System.out.println("close() invoked remotely");
		_beanContainer.cleanup();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.ui.impl.swing.OptionDefaulter#createDefaultOptionMap(java.lang.String)
	 */
	@Override
	public Map<String, Object> createDefaultOptionMap(final String configurationClass) {
		final HashMap<String, Object> result = new HashMap<String, Object>();
		result.put(OPT_CONFIG_FILE, CONFIG_OUT_OF_THE_BOX);
		result.put(OPT_GENERATIONS, Long.valueOf(0));
		result.put(OPT_CONFIG_CLASS, configurationClass);
		return result;
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#getEvolutionHandle()
	 */
	@Override
	public String getEvolutionHandle() throws RemoteException {
		System.out.println("getEvolutionHandle() invoked remotely");
		try {
			final Set<String> keySet = _evolutionHandleMap.keySet();
			if (keySet.size() != 1)
				throw new RemoteException("non-unique evolution handle");
			return keySet.toArray(new String[] {})[0];
		} catch (final Exception e) {
			throw new RemoteException("cannot get handle map key set", e);
		}
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#getEvolutionHandles()
	 */
	@Override
	public Collection<String> getEvolutionHandles() throws RemoteException {
		System.out.println("getEvolutionHandles() invoked remotely");
		final List<String> list = new ArrayList<String>();
		for (final String evolutionHandle : _evolutionHandleMap.keySet())
			list.add(evolutionHandle);
		return list;
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#getVisualizationModel(java.lang.String)
	 */
	@Override
	public VisualizationModel getVisualizationModel(final String handle) throws RemoteException {
		System.out.println("runEvolution() invoked remotely");
		final VisualizationModel result = _visualizationHandleMap.get(handle);
		if (result != null)
			return result;
		throw new RemoteException("visualization model handle is not known");
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#getVisualizationModelHandle()
	 */
	@Override
	public String getVisualizationModelHandle() throws RemoteException {
		System.out.println("getVisualizationModelHandle() invoked remotely");
		try {
			final Set<String> keySet = _visualizationHandleMap.keySet();
			if (keySet.size() != 1)
				throw new RemoteException("non-unique evolution handle");
			return keySet.toArray(new String[] {})[0];
		} catch (final Exception e) {
			throw new RemoteException("cannot get handle map key set", e);
		}
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#getVisualizationModelHandles()
	 */
	@Override
	public Collection<String> getVisualizationModelHandles() throws RemoteException {
		System.out.println("getVisualizationModelHandles() invoked remotely");
		final List<String> list = new ArrayList<String>();
		for (final String evolutionHandle : _visualizationHandleMap.keySet())
			list.add(evolutionHandle);
		return list;
	}

	/**
	 * @see net.sf.darwin.api.base.EvolutionService#runEvolution(java.lang.String)
	 */
	@Override
	public void runEvolution(final String handle) throws RemoteException {
		System.out.println("runEvolution() invoked remotely");
		final Evolution evolution = _evolutionHandleMap.get(handle);
		if (evolution != null)
			evolution.run();
		else
			throw new RemoteException("evolution handle is not known");
	}

	/**
	 * @throws RemoteException
	 * @see net.sf.darwin.api.base.EvolutionService#setRepaintListener(java.lang.String,
	 *      net.sf.darwin.ui.base.RepaintListener)
	 */
	@Override
	public void setRepaintListener(final String handle, final RepaintListener listener) throws RemoteException {
		System.out.println("setRepaintListener() invoked remotely");
		final VisualizationModel model = _visualizationHandleMap.get(handle);
		if (model != null)
			model.setRepaintListener(listener);
		else
			throw new RemoteException("visualization model handle is not known");
	}

	/**
	 * 
	 */
	private void initializeEvolutionHandleMap() {
		for (final String beanKey : _beanContainer.getBeanKeys()) {
			final Object bean = _beanContainer.getBean(beanKey);
			if (bean instanceof Evolution) {
				final Evolution evolution = (Evolution) bean;
				_evolutionHandleMap.put("#" + evolution.hashCode(), evolution);
			}
		}
	}

	/**
	 * 
	 */
	private void initializeVisualizationHandleMap() {
		for (final String beanKey : _beanContainer.getBeanKeys()) {
			final Object bean = _beanContainer.getBean(beanKey);
			if (bean instanceof VisualizationModel) {
				final VisualizationModel model = (VisualizationModel) bean;
				_visualizationHandleMap.put("#" + model.hashCode(), model);
			}
		}
	}

	//	private static final String SYNTAX = "Syntax: Darwin [-d] [-g N] [-v [dtdUrl]] [configurationFile]"; //$NON-NLS-1$

	private final ArgVector _argVector;

	public static final String OPT_DEBUG = "d"; //$NON-NLS-1$

	public static final String OPT_VALIDATE = "v"; //$NON-NLS-1$

	public static final String OPT_GENERATIONS = "gen"; //$NON-NLS-1$

	public static final String OPT_CONFIG_FILE = "configFile"; //$NON-NLS-1$

	public static final String OPT_CONFIG_CLASS = "configClass"; //$NON-NLS-1$

	public static final Object OPT_JAR = "jar"; //$NON-NLS-1$

	/**
	 * 
	 */
	protected static final String CONFIG_OUT_OF_THE_BOX = "outOfTheBox.xml"; //$NON-NLS-1$

	protected static final Log LOG = LogFactory.getLog(DarwinPlatform.class);

	private final BeanContainer _beanContainer;

	private final Map<String, Evolution> _evolutionHandleMap = new HashMap<String, Evolution>();

	private final Map<String, VisualizationModel> _visualizationHandleMap = new HashMap<String, VisualizationModel>();

	/**
	 * Darwin's own class loader.
	 */
	public static class DarwinClassLoader extends ClassLoader {

		/**
		 * @param logIt
		 *            XXX
		 * 
		 */
		public DarwinClassLoader(final boolean logIt) {
			super();
			this._logIt = logIt;
		}

		/**
		 * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
		 */
		@Override
		protected synchronized Class<?> loadClass(final String name, final boolean resolve) throws ClassNotFoundException {
			// TODO this does not work as expected: even after a class has been
			// loaded, the findLoadedClass method returns null, presumably
			// because it is in the parent class loader that the class has
			// actually been loaded previously.
			if (this._logIt && ((Class<?>) findLoadedClass(name) == null))
				System.out.println("Darwin loading class " + name); //$NON-NLS-1$				
			return super.loadClass(name, resolve);
		}

		private final boolean _logIt;

	}

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		LOG.info("Darwin application begun using args:"); //$NON-NLS-1$
		for (final String arg : args)
			LOG.info(arg);
		final ArgVector argVector = new ArgVector(args);
		final DarwinPlatform platform = new DarwinPlatform(argVector);
		final OptionMap<Object, Object> optionMap = new OptionMap<Object, Object>(
				platform.createDefaultOptionMap(DarwinPlatform.class.getCanonicalName()), new OptionSource_ArgVector<Object>(
						argVector, new ArgVectorProcessor_ObjectMap()));
		platform._beanContainer.doMain(optionMap, false);
		platform.initializeEvolutionHandleMap();
		platform.initializeVisualizationHandleMap();
		final Environment env = new Environment();
		try {
			// TODO make port a variable from args.
			final int port = 9001;
			env.newSessionAcceptor(port).acceptAll(platform);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param argVector
	 * @return
	 */
	private static ArrayList<String> getRemainingArguments(final ArgVector argVector) {
		final ArrayList<String> ignored = new ArrayList<String>();
		argVector.process(new ArgVectorProcessor_List(), ignored);
		return ignored;
	}
}
