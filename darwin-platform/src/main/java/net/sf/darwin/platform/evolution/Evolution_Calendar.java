/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution_Calendar.java
 * Created on Aug 2, 2009
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.platform.evolution;

import java.util.Calendar;

import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Lifecycle;
import net.sf.tostring0.ToString;

/**
 * Concrete implementation of {@link Evolution} which is based on
 * {@link Calendar} i.e. where ticks using {@link #getClock()} can be translated
 * into a point in time.
 * 
 * @author Robin Hillyard
 * 
 */
@Lifecycle(permanent = true)
final public class Evolution_Calendar extends Evolution_Timed implements CalendarEvolution {

	/**
	 * TEST
	 * 
	 * Secondary constructor for Evolution_Calendar - start is now, factor of
	 * 1000 (i.e. one generation per second). Evolution starts immediately.
	 */
	public Evolution_Calendar() {
		this(Calendar.getInstance(), true);
	}

	/**
	 * Secondary constructor for Evolution_Calendar - start is now, factor of
	 * 1000 (i.e. one generation per second).
	 * 
	 * @param run
	 *            true if the evolution starts immediately
	 */
	public Evolution_Calendar(final boolean run) {
		this(Calendar.getInstance(), run);
	}

	/**
	 * Secondary constructor for Evolution_Calendar with factor of 1000 (i.e.
	 * one tick per second).
	 * 
	 * @param start
	 *            the calendar value when this solar generator starts.
	 * @param run
	 *            true if the evolution starts immediately
	 */
	public Evolution_Calendar(final Calendar start, final boolean run) {
		super(start, run);
		this._factor = 1;
	}

	/**
	 * @param start
	 *            the calendar value when this calendric evolution starts, for
	 *            example, now or some date in the dim distant past (in which
	 *            case start will probably need to be based on a historical, ie.
	 *            non-Gregorian, calendar).
	 * @param factor
	 *            the multiplier that turns milliseconds into evolutionary (or
	 *            geological) time.
	 * @param run
	 *            true if the evolution starts immediately
	 */
	public Evolution_Calendar(final Calendar start, final double factor, final boolean run) {
		super(start, run);
		this._factor = factor;
	}

	/**
	 * @param start
	 *            the calendar value when this calendric evolution starts, for
	 *            example, now or some date in the dim distant past (in which
	 *            case start will probably need to be based on a historical, ie.
	 *            non-Gregorian, calendar).
	 * @param factor
	 *            the multiplier that turns milliseconds into evolutionary (or
	 *            geological) time.
	 * @param run
	 *            true if the evolution starts immediately
	 */
	public Evolution_Calendar(final Calendar start, final TimeUnit factor, final boolean run) {
		this(start, factor.seconds(), run);
	}

	/**
	 * Secondary constructor for Evolution_Calendar with start now (real time).
	 * 
	 * @param factor
	 *            the multiplier that turns ticks into evolutionary (or
	 *            geological) time.
	 * @param run
	 *            true if the evolution starts immediately
	 */
	public Evolution_Calendar(final double factor, final boolean run) {
		this(Calendar.getInstance(), factor, run);
	}

	/**
	 * Secondary constructor for Evolution_Calendar with start now (real time).
	 * 
	 * @param factor
	 *            the multiplier that turns ticks into evolutionary (or
	 *            geological) time.
	 * @param run
	 *            true if the evolution starts immediately
	 */
	public Evolution_Calendar(final TimeUnit factor, final boolean run) {
		this(Calendar.getInstance(), factor, run);
	}

	/**
	 * @see net.sf.darwin.platform.evolution.Evolution_Timed#getTime()
	 */
	@Override
	@ToString(formString = "showTime")
	public Calendar getTime() throws EvolutionException {
		final double millisecs = (getClock() * getRate() + getStartAdjustment()) * getFactor();
		Calendar result = getTime(Calendar.MILLISECOND, millisecs);
		if (result != null)
			return result;
		result = getTime(Calendar.SECOND, millisecs / Evolution_.MILLISECS_PER_SECOND);
		if (result != null)
			return result;
		result = getTime(Calendar.MINUTE, millisecs / Evolution_.MILLISECS_PER_SECOND / 60);
		if (result != null)
			return result;
		result = getTime(Calendar.HOUR, millisecs / Evolution_.MILLISECS_PER_SECOND / 3600);
		if (result != null)
			return result;
		result = getTime(Calendar.DAY_OF_MONTH, millisecs / Evolution_.MILLISECS_PER_SECOND / 3600 / 24);
		if (result != null)
			return result;
		throw new EvolutionException("Evolution_Calendar.getTime(): logic error"); //$NON-NLS-1$
	}

	/**
	 * @see net.sf.darwin.platform.evolution.Evolution_Timed#scheduleEvent(java.util.Calendar,
	 *      java.lang.Runnable)
	 */
	@Override
	public void scheduleEvent(final Calendar when, final Runnable event) {
		// TODO Translate from Calendar time to real time then schedule the
		// event.
		super.scheduleEvent(when, event);
	}

	/**
	 * @return the factor
	 */
	private double getFactor() {
		return this._factor;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1651247336489166420L;

	/**
	 * The
	 */
	private final double _factor;
}
