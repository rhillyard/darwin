/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolver_.java
 * Created on Dec 2, 2009
 * 
 * This module was split off from Evolution_ in release 2.1.10.
 * 
 * @version $Revision: 1.13 $
 */

package net.sf.darwin.platform.evolution;

import java.applet.Applet;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.darwin.api.base.Taxon_;
import net.sf.darwin.core.Base;
import net.sf.darwin.core.ClockWatcher;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.EvolutionException;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.Evolver;
import net.sf.darwin.core.GenerationListener;
import net.sf.darwin.core.Population;
import net.sf.darwin.core.VisualizableListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rubecula.beanpot.Configurable;

/**
 * This is the base abstract class for the Evolver interface.
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("serial")
public abstract class Evolver_ extends Base implements Evolver, Configurable {

	/**
	 * 
	 */
	protected Evolver_() {
		super();
		this.clock = 0L;
		this._evolvables = new HashMap<>();
	}

	/**
	 * TEST
	 * 
	 * @param listener
	 * 
	 * @see net.sf.darwin.core.Evolution#addEvolvable(net.sf.darwin.core.Evolvable,
	 *      int)
	 */
	@Override
	public void addEvolvable(final Evolvable listener) {
		addEvolvable(listener, 1);
	}

	/**
	 * @see net.sf.darwin.core.Evolver#addEvolvable(net.sf.darwin.core.Evolvable,
	 *      int)
	 */
	@Override
	public void addEvolvable(final Evolvable evolvable, final int ticks) {
		putEvolvable(evolvable, Integer.valueOf(ticks));
	}

	/**
	 * CONSIDER renaming as addGenerationListener()
	 * 
	 * @see net.sf.darwin.core.Evolver#addListener(net.sf.darwin.core.GenerationListener)
	 */
	@Override
	public boolean addListener(final GenerationListener listener) {
		return getListeners().add(listener);
	}

	/**
	 * This is invoked by reflection during configuration.
	 * 
	 * @param listener
	 * @return true if the addition was successful
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean addVisualizableListener(final VisualizableListener listener) {
		return getVisualizableListeners().add(listener);
	}

	/**
	 * TEST
	 * 
	 * @see net.sf.darwin.core.Evolution#cleanup()
	 */
	@Override
	public void cleanup() {
		final Set<Evolvable> evolvables = getEvolvableKeys();
		for (final Evolvable evolvable : evolvables) {
			evolvable.cullMembers();
		}
	}

	/**
	 * @return {@link #clock}, the number of ticks since this Generator was
	 *         started, or null if this evolution has not been started.
	 */
	@Override
	public long getClock() {
		return this.clock;
	}

	/**
	 * @return {@link #clockWatcher}
	 */
	@Override
	public ClockWatcher getClockWatcher() {
		return this.clockWatcher;
	}

	/**
	 * @return value of {@link #getEvolvables()} passed to {@link Map#keySet()}.
	 * @see net.sf.darwin.core.Evolver#getEvolvableKeys()
	 */
	@Override
	public Set<Evolvable> getEvolvableKeys() {
		return getEvolvables().keySet();
	}

	/**
	 * This method initializes the Evolution so that it is ready to start the
	 * first generation.
	 * 
	 * Work done here is performed after any user interface initialization has
	 * been done, for example it is called by {@link Applet#start()}.
	 * 
	 * The complementary method is #cleanup().
	 * 
	 * @see net.sf.darwin.core.Evolution#init()
	 */
	@Override
	public void init() {
		initializeEnvironments();
		setupVisualizableListeners();
		seedEvolvables();
	}

	/**
	 * Create a new {@link EvolutionTask} and run it. This method has works
	 * quite differently in the two different modes (manual versus auto). When
	 * this {@link Evolution} is in auto mode, we is intended for manual
	 * stepping of the evolutionary process.
	 * 
	 * @return true if there are evolvables still to evolve.
	 * @throws EvolutionException
	 * @see net.sf.darwin.core.Evolution#next()
	 */
	@Override
	public boolean next() throws EvolutionException {
		EvolutionTask evolutionTask = new EvolutionTask(this, getEvolvables(), getListeners(), getClock(), getClockWatcher());
		evolutionTask.run();
		this.clock = evolutionTask.getClock();
		evolutionTask = null;
		return getEvolvables().size() > 0;
	}

	/**
	 * Do nothing - override if you need something doing
	 * 
	 * @see com.rubecula.beanpot.Configurable#postConfigure(java.lang.Object)
	 */
	@Override
	public void postConfigure(final Object data) {
		// do nothing here
	}

	/**
	 * Do nothing - override if you need something doing
	 * 
	 * @see com.rubecula.beanpot.Configurable#preConfigure(java.lang.Object)
	 */
	@Override
	public void preConfigure(final Object data) {
		// do nothing here
	}

	/**
	 * This is invoked by reflection during configuration.
	 * 
	 * @param evolvable
	 * @param ticks
	 * @return the value previously associated with evolvable.
	 */
	public Integer putEvolvable(final Evolvable evolvable, final Integer ticks) {
		return getEvolvables().put(evolvable, ticks);
	}

	/**
	 * @see net.sf.darwin.core.Evolver#removeEvolvable(net.sf.darwin.core.Evolvable)
	 */
	@Override
	public void removeEvolvable(final Evolvable evolvable) {
		getEvolvables().remove(evolvable);
	}

	/**
	 */
	@Override
	public void seedEvolvables() {
		for (final Evolvable evolvable : getEvolvableKeys())
			evolvable.seedMembers();
	}

	/**
	 * @param clockWatcher
	 *            the clockWatcher to set
	 */
	@Override
	public void setClockWatcher(final ClockWatcher clockWatcher) {
		this.clockWatcher = clockWatcher;
	}

	/**
	 * Bean-type method to define the evolvables.
	 * 
	 * This is invoked by reflection during configuration.
	 * 
	 * @param map
	 *            A map of Evolvable objects, each with the number of ticks
	 *            between generations. The evolutionary process proceeds one
	 *            tick at a time, but not every evolvable will undergo a new
	 *            generation for every tick. For instance, the 13-year and
	 *            17-year cicadas would have tick values of 13 and 17 assuming
	 *            that the Evolution proceeded one tick per year.
	 */
	public void setEvolvables(final Map<? extends Evolvable, ? extends Integer> map) {
		getEvolvables().clear();
		getEvolvables().putAll(map);
	}

	/**
	 * This is invoked by reflection during configuration.
	 * 
	 * @param listeners
	 */
	public void setListeners(final Collection<? extends GenerationListener> listeners) {
		getListeners().clear();
		getListeners().addAll(listeners);
	}

	/**
	 * Method to clear and reset the visualizable listeners.
	 * 
	 * This is invoked by reflection during configuration.
	 * 
	 * @param visualizableListeners
	 */
	public void setVisualizableListeners(final Collection<VisualizableListener> visualizableListeners) {
		this.visualizableListeners.clear();
		this.visualizableListeners.addAll(visualizableListeners);
	}

	/**
	 * @return {@link #_evolvables}, the map of the evolvables.
	 */
	protected Map<Evolvable, Integer> getEvolvables() {
		return this._evolvables;
	}

	/**
	 * @return {@link #_listeners}, the list of listeners
	 */
	protected Collection<GenerationListener> getListeners() {
		return this._listeners;
	}

	/**
	 * @return {@link #visualizableListeners}
	 */
	protected Collection<VisualizableListener> getVisualizableListeners() {
		return this.visualizableListeners;
	}

	/**
	 * 
	 */
	private void initializeEnvironments() {
		for (final Evolvable evolvable : getEvolvableKeys()) {
			// TODO consider using polymorphism here
			if (evolvable instanceof Taxon_) {
				final Collection<Population> populations = ((Taxon_) evolvable).getPopulations();
				for (final Population population : populations) {
					final Collection<Colony> colonies = population.getColonies();
					for (final Colony colony : colonies) {
						colony.getEnvironment().init();
					}
				}
			}
		}
	}

	/**
	 * For each evolvable, if it is an instance of {@link Taxon_}, pass
	 * {@link #visualizableListeners} to the
	 * {@link Taxon_#setVisualizableListeners(Collection)} method.
	 * 
	 * NOTE that the reason we do this in this method is that the visualizable
	 * listeners are not yet set up properly until after the user interface is
	 * properly established.
	 * 
	 * @see net.sf.darwin.core.Evolution#init()
	 */
	private void setupVisualizableListeners() {
		if (getVisualizableListeners() != null) {
			for (final Evolvable evolvable : getEvolvableKeys()) {
				// TODO consider using polymorphism here
				if (evolvable instanceof Taxon_) {
					((Taxon_) evolvable).setVisualizableListeners(getVisualizableListeners());
				}
			}
		}
	}

	private transient final Collection<VisualizableListener> visualizableListeners = new ArrayList<>();

	private transient long clock;

	private transient final Collection<GenerationListener> _listeners = new ArrayList<>();

	/**
	 * The logger for this class.
	 */
	protected static final Log LOG = LogFactory.getLog(Evolver_.class);

	/**
	 * A map of Evolvable objects, each with the number of ticks between
	 * generations. The evolutionary process proceeds one tick at a time, but
	 * not every evolvable will undergo a new generation for every tick. For
	 * instance, the 13-year and 17-year cicadas would have tick values of 13
	 * and 17 assuming that the Evolution proceeded one tick per year.
	 */
	private transient final Map<Evolvable, Integer> _evolvables;

	private transient ClockWatcher clockWatcher;

	/**
	 * @param time
	 * @return a usable String to represent the time
	 */
	public static String showTime(final Calendar time) {
		return DateFormat.getDateTimeInstance().format(time.getTime());
	}

	/**
	 * @param time
	 * @param writer
	 *            the writer to show the time on
	 */
	public static void showTime(final Calendar time, final PrintWriter writer) {
		writer.println("Evolutionary time: " + showTime(time)); //$NON-NLS-1$
	}

}
