/**
 * Intelligent Data Fabric Project.
 * Copyright (C) 2012  UnitedHealth Group Information Technology
 *
 * Organization: Advanced Computations Lab
 * Module: BeanContainer_BeanPot
 * Created on: Feb 27, 2012
 */

package net.sf.darwin.platform.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Set;

import net.sf.darwin.api.base.BeanContainer;
import net.sf.darwin.api.base.OptionMap;
import net.sf.darwin.api.impl.GenerationListener_Standard;
import net.sf.darwin.api.util.TimeUtilities;
import net.sf.darwin.core.BeanContainerException;
import net.sf.darwin.platform.evolution.ClockWatcher_Default;

import com.rubecula.beanpot.BeanPot;
import com.rubecula.beanpot.BeanPotException;

/**
 * @author rhillya
 * 
 *         FIXME properly match the static nature of the BeanPot with the
 *         instance nature of BeanContainer. That probably means re-writing the
 *         BeanPot.
 */
@SuppressWarnings({ "static-access", "nls" })
public class BeanContainer_BeanPot implements BeanContainer {

	public BeanContainer_BeanPot() {
		super();
		this._beanPot = BeanPot.getInstance();
	}

	/**
	 * 
	 */
	@Override
	public void cleanup() {
		BeanPot.cleanup();
	}

	/**
	 * @throws BeanPotException
	 */
	@Override
	public void configure() throws BeanContainerException {
		try {
			BeanPot.configure();
		} catch (final BeanPotException e) {
			throw new BeanContainerException("configuration problem", e);
		}
	}

	/**
	 * TODO move this - it's much too specific
	 * 
	 * @param maxGenerations
	 * @param debug
	 * @param configurationFile
	 * @param configClass
	 *            XXX
	 * @throws BeanContainerException
	 */
	@Override
	public void configureAndRunEvolution(final int maxGenerations, final boolean debug, final String configurationFile,
			final Class<?> configClass, final Object[] preConfigData, final boolean run) throws BeanContainerException {
		final ClassLoader classLoader = BeanPot.getClassLoader();
		setPreConfigData(preConfigData);
		setPostConfigData(Integer.valueOf(maxGenerations));
		final URL resource = classLoader.getResource(configurationFile);
		if (resource != null)
			try {
				BeanPot.getBeanPot().setConfigurationByResource(resource);
			} catch (final BeanPotException e) {
				throw new BeanContainerException("problem setting configuration by resource", e);
			}
		else if (configClass == null || configurationFile.startsWith("/")) //$NON-NLS-1$
			setConfiguration(configurationFile);
		else
			setConfigurationByResource(configClass, configurationFile);
		setDebug(debug);
		configure();

		if (debug)
			showBeans();

		if (run) {
			final long startTime = System.currentTimeMillis();
			runBeans();
			System.out
					.println("Real time elapsed for evolution: " + TimeUtilities.getElapsedTime(System.currentTimeMillis() - startTime)); //$NON-NLS-1$
		}
	}

	/**
	 * @param ignored
	 */
	@Override
	public void doMain(final OptionMap<?, Object> optionMap, final boolean run) {
		final boolean debug = optionMap.getBoolean(OPT_DEBUG);
		final String configurationFile = optionMap.getString(OPT_CONFIG_FILE);
		final int maxGenerations = optionMap.getInt(OPT_GENERATIONS);
		if (optionMap.getBoolean(OPT_VALIDATE)) {
			setValidate();
			final URL url = optionMap.getUrl(OPT_DTD);
			if (url != null)
				setValidate(true, url);
		}
		final URL url = optionMap.getUrl(OPT_JAR);
		if (url != null) {
			InputStream inputStream = null;
			try {
				inputStream = url.openStream();
				BeanPot.setClassLoader(new URL[] { url });
				System.out.println("Added JAR file to classpath: " + url); //$NON-NLS-1$
				final String string = optionMap.getString(OPT_CONFIG_FILE);
				if (string == null) {

				}
			} catch (final IOException e) {
				System.err.println("Cannot open jar file: " + e.getLocalizedMessage()); //$NON-NLS-1$
			} finally {
				try {
					if (inputStream != null)
						inputStream.close();
				} catch (final IOException e) {
					// OK to eat this exception
				}
			}
		}
		Class<?> configClass = null;
		final String configClassName = optionMap.getString(OPT_CONFIG_CLASS);
		if (configClassName != null)
			try {
				// TODO don't think this is really right - we should be looking
				// in the jar file
				final ClassLoader classLoader = BeanPot.getClassLoader();
				configClass = Class.forName(optionMap.getString(OPT_CONFIG_CLASS), true, classLoader);
			} catch (final ClassNotFoundException e) {
				System.err.println("Cannot find configuration class: " + e.getLocalizedMessage()); //$NON-NLS-1$
			}

		System.out.println("Darwin application starting with configurationFile: " + configurationFile); //$NON-NLS-1$

		try {
			final Object[] preConfigData = new Object[] { new GenerationListener_Standard(), new ClockWatcher_Default() };
			configureAndRunEvolution(maxGenerations, debug, configurationFile, configClass, preConfigData, run);
		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			if (run)
				cleanup();
		}
	}

	/**
	 * @param key
	 * @return the bean accessed with key <code>key</code>.
	 */
	@Override
	public Object getBean(final String key) {
		return this._beanPot.getBean(key);
	}

	/**
	 * TEST
	 * 
	 * @return all the keys for the beans.
	 */
	@Override
	public Set<String> getBeanKeys() {
		return BeanPot.getBeanKeys();
	}

	/**
	 * @return the identifier of the bean pot
	 */
	@Override
	public String getIdentifier() {
		return BeanPot.getIdentifier();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.BeanContainer#getWriter()
	 */
	@Override
	public PrintWriter getWriter() {
		return this._beanPot.getWriter();
	}

	/**
	 * @param key
	 * @param bean
	 * @throws BeanContainerException
	 * @throws BeanPotException
	 */
	@Override
	public void imposeBean(final String key, final Object bean) throws BeanContainerException {
		try {
			BeanPot.imposeBean(key, bean);
		} catch (final BeanPotException e) {
			throw new BeanContainerException("problem imposing bean " + key, e);
		}
	}

	/**
	 * TEST
	 * 
	 */
	@Override
	public void runBeans() {
		this._beanPot.run();
	}

	/**
	 * @param beanProperty
	 * @param value
	 * @return the bean which has been updated.
	 * @throws BeanContainerException
	 * @throws BeanPotException
	 */
	@Override
	public Object setBeanProperty(final String beanProperty, final Object value) throws BeanContainerException {
		try {
			return BeanPot.setBeanProperty(beanProperty, value);
		} catch (final BeanPotException e) {
			throw new BeanContainerException("problem setting bean property " + beanProperty, e);
		}
	}

	/**
	 * @param classLoader
	 */
	@Override
	public void setClassLoader(final ClassLoader classLoader) {
		BeanPot.setClassLoader(classLoader);
	}

	/**
	 * Invoke {@link BeanPot#setConfiguration(String)} with the one parameter
	 * given.
	 * 
	 * @param configurationFile
	 * @throws BeanContainerException
	 * @throws BeanPotException
	 */
	@Override
	public void setConfiguration(final String configurationFile) throws BeanContainerException {
		try {
			BeanPot.setConfiguration(configurationFile);
		} catch (final BeanPotException e) {
			throw new BeanContainerException("configuration problem", e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.BeanContainer#setConfigurationByResource(java.lang.Class,
	 *      java.lang.String)
	 */
	@Override
	public void setConfigurationByResource(final Class<? extends Object> class1, final String parameter)
			throws BeanContainerException {
		try {
			BeanPot.setConfigurationByResource(class1, parameter);
		} catch (final BeanPotException e) {
			throw new BeanContainerException("configuration problem", e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.BeanContainer#setDebug(boolean)
	 */
	@Override
	public void setDebug(final boolean booleanParameter) {
		BeanPot.setDebug(booleanParameter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.BeanContainer#setPostConfigData(java.lang.Integer)
	 */
	@Override
	public void setPostConfigData(final Object postConfigData) {
		// TODO implement an instance method for this
		BeanPot.setPostConfigData(postConfigData);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see net.sf.darwin.api.base.BeanContainer#setPreConfigData(java.lang.Object[])
	 */
	@Override
	public void setPreConfigData(final Object[] objects) {
		// TODO implement an instance method for this
		BeanPot.setPreConfigData(objects);
	}

	/**
	 * 
	 */
	@Override
	public void setValidate() {
		BeanPot.setValidate();
	}

	/**
	 * @param validate
	 *            true if we want to force DTD validation
	 * @param dtd
	 *            the URL of the DTD (if null then we use the current value).
	 */
	@Override
	public void setValidate(final boolean validate, final URL dtd) {
		BeanPot.setValidate(validate, dtd);
	}

	/**
	 * 
	 */
	@Override
	public void showBeans() {
		BeanPot.getBeanPot().showBeans(this._beanPot.getWriter());
	}

	private final BeanPot _beanPot;

	public static final String CONFIG_OUT_OF_THE_BOX = "outOfTheBox.xml"; //$NON-NLS-1$
}
