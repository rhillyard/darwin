/**
 * Cryptobase Project.
 * Copyright (C) 2011  UnitedHealth Group
 *
 * Module: CryptobaseSSLServerSocketFactory.java
 * Created on Jan 6, 2011
 * @version $Revision: 1.1 $
 */

package net.sf.darwin.platform.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

/**
 * TODO OBSOLETE
 * 
 * @author rhillya
 * 
 */
public class CryptobaseSSLServerSocketFactory extends SSLServerSocketFactory {

	/**
	 * 
	 */
	public CryptobaseSSLServerSocketFactory() {
		super();
		this._delegate = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
	}

	/**
	 * @return
	 * @throws IOException
	 * @see javax.net.ServerSocketFactory#createServerSocket()
	 */
	@Override
	public ServerSocket createServerSocket() throws IOException {
		final ServerSocket socket = this._delegate.createServerSocket();
		init(socket);
		return socket;
	}

	/**
	 * @param port
	 * @return
	 * @throws IOException
	 * @see javax.net.ServerSocketFactory#createServerSocket(int)
	 */
	@Override
	public ServerSocket createServerSocket(final int port) throws IOException {
		final ServerSocket socket = this._delegate.createServerSocket(port);
		init(socket);
		return socket;
	}

	/**
	 * @param port
	 * @param backlog
	 * @return
	 * @throws IOException
	 * @see javax.net.ServerSocketFactory#createServerSocket(int, int)
	 */
	@Override
	public ServerSocket createServerSocket(final int port, final int backlog) throws IOException {
		final ServerSocket socket = this._delegate.createServerSocket(port, backlog);
		init(socket);
		return socket;
	}

	/**
	 * @param port
	 * @param backlog
	 * @param ifAddress
	 * @return
	 * @throws IOException
	 * @see javax.net.ServerSocketFactory#createServerSocket(int, int,
	 *      java.net.InetAddress)
	 */
	@Override
	public ServerSocket createServerSocket(final int port, final int backlog, final InetAddress ifAddress) throws IOException {
		final ServerSocket socket = this._delegate.createServerSocket(port, backlog, ifAddress);
		init(socket);
		return socket;
	}

	/**
	 * @return
	 * @see javax.net.ssl.SSLServerSocketFactory#getDefaultCipherSuites()
	 */
	@Override
	public String[] getDefaultCipherSuites() {
		return this._delegate.getDefaultCipherSuites();
	}

	/**
	 * @return
	 * @see javax.net.ssl.SSLServerSocketFactory#getSupportedCipherSuites()
	 */
	@Override
	public String[] getSupportedCipherSuites() {
		return this._delegate.getSupportedCipherSuites();
	}

	private void init(final ServerSocket socket) {
		if (socket instanceof SSLServerSocket)
			((SSLServerSocket) socket).setEnabledCipherSuites(ServerUtilities.EnabledCipherSuites);
		else
			throw new RuntimeException("socket does not extend SSLServerSocket"); //$NON-NLS-1$
	}

	private final SSLServerSocketFactory _delegate;

}
