/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: Evolution_Test.java
 * Created on Aug 2, 2009
 * @version $Revision: 1.19 $
 */

package net.sf.darwin.platform.evolution;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Set;

import net.sf.darwin.api.base.Evolvable_;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.GenerationListener;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.ConsoleWriter;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "static-method" })
public class Evolution_Test {

	/**
	 */
	@Before
	public void setUp() {
		// do nothing
	}

	/**
	 */
	@After
	public void tearDown() {
		// do nothing
	}

	/**
	 * Test method for
	 * {@link com.rubecula.darwin.evolution.Evolution_#Evolution_()}.
	 */
	@Test
	public void testEvolution_1() {
		final Evolution evolution = new TestEvolution();
		evolution.run();
		final long clock = evolution.getClock();
		assertEquals("clock", 0L, clock);
	}

	/**
	 * @throws Exception
	 * 
	 */
	@Test
	public void testNext1() throws Exception {
		final Calendar start = Calendar.getInstance();
		final Evolution_Timed evolution = new Evolution_Calendar(start, TIME_FACTOR, false);
		final Calendar time = evolution.getTime();
		final Calendar copy = (Calendar) time.clone();
		assertEquals("start", start, time);
		assertFalse(evolution.next());
		copy.add(Calendar.MILLISECOND, TIME_FACTOR * evolution.getRate());
		assertEquals("one tick", copy, evolution.getTime());
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "synthetic-access" })
	@Test
	public void testNext2() throws Exception {
		final Evolution_ evolution = new Evolution_Calendar(Calendar.getInstance(), TIME_FACTOR, false);
		final TestEvolvable evolver = new TestEvolvable("test evolvable");
		final int maxGenerations = 2;
		evolver.setMaxGenerations(maxGenerations);
		evolution.addEvolvable(evolver, 2);
		evolution.addListener(new TestListener(evolution, maxGenerations));
		final Set<Evolvable> evolvables = evolution.getEvolvableKeys();
		assertEquals("count", 1, evolvables.size());
		final Evolvable[] array = evolvables.toArray(new Evolvable[0]);
		final Evolvable evolvable = array[0];
		assertTrue(evolution.next());
		// Note that this generator only fires on every other tick
		assertEquals("generation 0", 0, evolvable.getGeneration());
		assertTrue(evolution.next());
		assertEquals("generation 1", 1, evolvable.getGeneration());
		assertTrue(evolution.next());
		assertEquals("generation 1", 1, evolvable.getGeneration());
		assertFalse(evolution.next());
		assertEquals("generation end", maxGenerations, evolvable.getGeneration());
	}

	/**
	 * 
	 */
	@SuppressWarnings({ "synthetic-access" })
	@Test
	public void testRun() {
		final Evolution_ evolution = new Evolution_Calendar(Calendar.getInstance(), TIME_FACTOR, true);
		// evolution.setRate(500); // speed up so that each generation takes a
		// // second
		final int ticksPerGeneration = 2;
		final TestEvolvable evolvable = new TestEvolvable("test evolvable");
		evolvable.setMaxGenerations(MAX_GENERATIONS);
		evolution.addEvolvable(evolvable, ticksPerGeneration);
		evolution.addListener(new TestListener(evolution, MAX_GENERATIONS));
		evolution.run();
		System.out.println("Test complete");
	}

	/**
	 * @throws Exception
	 * 
	 */
	@Test
	public void testSchedule1() throws Exception {
		final Evolution_Timed evolution = new Evolution_Calendar(Calendar.getInstance(), TIME_FACTOR, false);
		final Runnable event = new Runnable() {

			@Override
			public void run() {
				System.out.print("Scheduled task run at: ");
				Evolver_.showTime(Calendar.getInstance(), new ConsoleWriter(System.out));
			}
		};
		final Calendar when = Calendar.getInstance();
		final int delay = 2;
		System.out.print("scheduling event for " + delay + " seconds from: ");
		Evolver_.showTime(when, new ConsoleWriter(System.out));
		when.add(Calendar.SECOND, delay);
		evolution.scheduleEvent(when, event);
		evolution.shutdown();
		System.out.println("Test complete");
	}

	/**
	 * @throws Exception
	 * 
	 */
	@SuppressWarnings({ "synthetic-access" })
	@Test
	public void testStart() throws Exception {
		final Evolution_ evolution = new Evolution_Calendar(Calendar.getInstance(), TIME_FACTOR, true);
		final int ticksPerGeneration = 2;
		final TestEvolvable evolvable = new TestEvolvable("test evolvable");
		evolvable.setMaxGenerations(MAX_GENERATIONS);
		evolution.addEvolvable(evolvable, ticksPerGeneration);
		evolution.addListener(new TestListener(evolution, MAX_GENERATIONS));
		evolution.run();
		// evolution.start(1000, true);
		while (evolution.isActive(true)) {
			try {
				Thread.sleep(100);
			} catch (final InterruptedException e) {
				// do nothing
			}
		}
		System.out.println("Test complete");
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings({ "synthetic-access" })
	@Test
	public void testStop() throws Exception {
		Evolution_ evolution = new Evolution_Calendar(Calendar.getInstance(), TIME_FACTOR, true);
		final int ticksPerGeneration = 2;
		final TestEvolvable evolvable = new TestEvolvable("test evolvable");
		evolvable.setMaxGenerations(MAX_GENERATIONS);
		evolution.addEvolvable(evolvable, ticksPerGeneration);
		evolution.addListener(new TestListener(evolution, MAX_GENERATIONS));
		evolution.run();
		final boolean stop = evolution.stop();
		assertTrue(stop);
		final int generation = evolvable.getGeneration();
		assertEquals("generation", MAX_GENERATIONS, generation);
		evolution.start(1000, false);
		System.out.println("Test complete");
		evolution = null;
	}

	/**
	 * 
	 */
	private static final int MAX_GENERATIONS = 3;

	/**
	 * Real time goes ten times as fast as computer time.
	 */
	private static final int TIME_FACTOR = 10;

	static private class TestEvolution extends Evolution_ {

		public TestEvolution() {
			super();
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 1275072355448424207L;
	}

	/**
	 * @author Robin Hillyard
	 * 
	 */
	private final static class TestEvolvable extends Evolvable_ {

		/**
		 * @param identifier
		 */
		public TestEvolvable(final String identifier) {
			super(identifier);
		}

		@Override
		public void cullMembers() {
			// Don't need to do anything in this situation
		}

		@Override
		public boolean nextGeneration() {
			Evolver_.showTime(Calendar.getInstance(), new ConsoleWriter(System.out));
			return super.nextGeneration();
		}

		@Override
		public void seedMembers() {
			// Don't need to do anything in this situation
		}

		@Override
		public String toString() {
			return getIdentifier() + " at generation: " + getGeneration();
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -2124396297499211383L;
	}

	/**
	 * @author Robin Hillyard
	 * 
	 */
	private final static class TestListener implements GenerationListener {
		private TestListener(final Evolution generator, final int generations) {
			this._evolution = generator;
			this._generations = generations;
		}

		@Override
		public void onGeneration(final Evolvable evolvable) {
			if (evolvable != null) {
				System.out.println("evolvable: " + evolvable + " at generation " + evolvable.getGeneration());
				if (evolvable.getGeneration() < this._generations) {
					assertEquals("count", 1, ((Evolution_) this._evolution).getEvolvableKeys().size());
				} else {
					assertEquals("done", this._generations, evolvable.getGeneration());
				}
			} else {
				// final boolean ok = _evolution.cancel();
				System.out.println("Evolution complete");
				assertEquals("count", 0, ((Evolution_) this._evolution).getEvolvableKeys().size());
			}
		}

		private final Evolution _evolution;

		private final int _generations;
	}

}
