/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: EvolutionTest.java
 * Created on Feb 24, 2009
 * @version $Revision: 1.2 $
 */

package net.sf.darwin.platform.evolution;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;

import net.sf.darwin.api.impl.EnvironmentChangeEvent;
import net.sf.darwin.api.impl.Fecundity_Saturated;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.EnvironmentListener;
import net.sf.darwin.core.Evolution;
import net.sf.darwin.core.Evolvable;
import net.sf.darwin.core.GenerationListener;
import net.sf.darwin.core.Taxon;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rubecula.beanpot.BeanPot;

/**
 * TODO we only need this class for the EnvironmentListenerTest class
 * 
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings({ "nls", "static-method" })
public class EvolutionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		BeanPot.setConfigurationByResource(EvolutionTest.class, "testEvolution.xml"); //$NON-NLS-1$
		BeanPot.setDebug(true);
		BeanPot.configure();
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		BeanPot.cleanup();
	}

	/**
	 * 
	 */
	@Test
	public void testConfigurationFecundity() {
		final Taxon system = (Taxon) BeanPot.getInstance().getBean("Darwin"); //$NON-NLS-1$
		final Fecundity_Saturated fecundity = (Fecundity_Saturated) system.getFecundity();
		assertEquals("fecundity 0", 5, fecundity.getFecundity(0));
		assertEquals("fecundity 0.15", 4, fecundity.getFecundity(0.15));
		assertEquals("fecundity 0.3", 3, fecundity.getFecundity(0.3));
		assertEquals("fecundity 0.5", 2, fecundity.getFecundity(0.5));
		assertEquals("fecundity 1.0", 1, fecundity.getFecundity(1.0));
		assertEquals("fecundity 2.0", 0, fecundity.getFecundity(2.0));
	}

	/**
	 * 
	 */
	@Test
	public void testSetupEvolution1() {
		final BeanPot beanPot = BeanPot.getInstance();
		final Evolution evolution = (Evolution) beanPot.getBean("Evolution"); //$NON-NLS-1$

		evolution.addListener(new GenerationListener() {
			@Override
			public void onGeneration(final Evolvable evolvable) {
				if (evolvable != null)
					beanPot.getWriter().println(
							"evolvable " + evolvable.getIdentifier() + ": generation=" + evolvable.getGeneration());
			}
		});
		evolution.init();

		evolution.run();
	}

	/**
	 * @throws Exception
	 * 
	 *             TODO determine why this just hangs up
	 */
	// @Test
	public void testSetupEvolution2() throws Exception {

		final BeanPot beanPot = BeanPot.getInstance();
		final Evolution evolution = (Evolution) beanPot.getBean("Evolution"); //$NON-NLS-1$
		final Environment environmentExp = (Environment) beanPot.getBean("EnvironmentExp"); //$NON-NLS-1$
		final Future<?> future = evolution.scheduleEvent(5,
				new EnvironmentChangeEvent(environmentExp, "SootDensity", Double.valueOf(7))); //$NON-NLS-1$

		evolution.addListener(new GenerationListener() {
			@Override
			public void onGeneration(final Evolvable evolvable) {
				if (evolvable != null)
					beanPot.getWriter().println(
							"evolvable " + evolvable.getIdentifier() + ": generation=" + evolvable.getGeneration());
			}
		});
		evolution.init();

		evolution.run();
		final boolean complete = evolution.waitUntilComplete(250);
		System.out.println("Peppered Moth is " + (complete ? "" : "not ") + "complete"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		if (complete) {
			try {
				final Object object = future.get();
				System.out.println("result of scheduled task: " + object); //$NON-NLS-1$
			} catch (final CancellationException e) {
				System.out.println("evolution already canceled"); //$NON-NLS-1$
			}
		}
		evolution.shutdown();
	}

	/**
	 * @author Robin Hillyard
	 * 
	 */
	public static final class EnvironmentListenerTest implements EnvironmentListener {
		/**
		 * @see net.sf.darwin.core.EnvironmentListener#onEnvironmentChange(net.sf.darwin.core.Environment)
		 */
		@Override
		public void onEnvironmentChange(final Environment env) {
			System.out.println("Environment Changed: "); //$NON-NLS-1$
			final Collection<String> keySet = env.getEcoSystem().factorKeys();
			for (final String key : keySet) {
				System.out.println("  key: " + key + " value: " + env.getEcoSystem().getFactor(key)); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

}
