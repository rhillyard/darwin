/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: FitnessEngine_Test.java
 * Created on Apr 1, 2009
 * @version $Revision: 1.4 $
 */

package net.sf.darwin.platform.evolution;

import static org.junit.Assert.assertTrue;
import net.sf.darwin.api.base.EcoFactor_Number;
import net.sf.darwin.api.base.Fitness_;
import net.sf.darwin.api.impl.FitnessFunction_ScaledNormal;
import net.sf.darwin.core.EcoFactor;
import net.sf.darwin.core.ExpressionMap;
import net.sf.darwin.core.FitnessException;
import net.sf.darwin.core.FitnessFunction;
import net.sf.darwin.core.FunctionListener;
import net.sf.darwin.core.HasExpressions;
import net.sf.darwin.core.Quantifiable;
import net.sf.darwin.core.Trait;
import net.sf.darwin.core.Variant;
import net.sf.darwin.platform.util.TestBase;

import org.junit.Test;

import com.rubecula.jexpression.EvalExpressionMutable;
import com.rubecula.jexpression.Evaluator;

/**
 * @author Robin Hillyard
 * 
 */
@SuppressWarnings("static-method")
public class FitnessEngine_Test extends TestBase {

	/**
	 */
	@Test
	public void dummy() {
		assertTrue(true);
	}

	final static public class EcoFactor_SootDensity extends EcoFactor_Number {

		/**
		 * @param value
		 */
		public EcoFactor_SootDensity(final double value) {
			super(KEY_SOOT_DENSITY, Double.valueOf(value));
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 5399132326839771733L;

		/**
		 * 
		 */
		public static final String KEY_SOOT_DENSITY = "SootDensity"; //$NON-NLS-1$

	}

	public static class WcSdFitness extends Fitness_ implements HasExpressions {

		/**
		 * 
		 */
		public WcSdFitness() {
			this((Evaluator) null, null);
		}

		/**
		 * @param evaluator
		 * @param functionListener
		 *            the function listener (may be null)
		 * 
		 */
		public WcSdFitness(final Evaluator evaluator, final FunctionListener functionListener) {
			super(new FitnessFunction_ScaledNormal(evaluator), functionListener);
		}

		/**
		 * @see net.sf.darwin.core.HasExpressions#getExpressions()
		 */
		@Override
		public ExpressionMap getExpressions() {
			final ExpressionMap result = new ExpressionMap();
			final FitnessFunction fitnessFunction = getFitnessFunction();
			if (fitnessFunction instanceof EvalExpressionMutable) {
				result.addExpression("Fitness: Wing color / soot density", (EvalExpressionMutable) fitnessFunction); //$NON-NLS-1$			
			}
			return result;
		}

		/**
		 * @throws FitnessException
		 * @see net.sf.darwin.core.Fitness#getFitness(net.sf.darwin.core.Trait,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public double getFitness(final Trait trait, final EcoFactor factor) throws FitnessException {
			final Variant variant = trait.getVariant();
			// TODO consider doing this via polymorphism
			if (variant instanceof MockVariant) {
				if (factor instanceof MockEcoFactor) {
					final double sootEquivalent = ((MockEnum) ((MockVariant) variant).getValue()).sootEquivalent();
					final double sootDensity = ((Quantifiable) factor).doubleValue();
					// XXX check that trait.getVariantKey() is correct (maybe
					// should be trait.getCharacterKey())
					return calculateFitness(sootEquivalent, sootDensity, trait.getVariantKey(), factor.getIdentifier());
				}
				return 0;
			}
			return 0;
		}

		/**
		 * @see net.sf.darwin.core.Fitness#getWeight(String,
		 *      net.sf.darwin.core.EcoFactor)
		 */
		@Override
		public int getWeight(final String character, final EcoFactor factor) {
			if (character.equals(MockEnum.ID)) {
				// TODO consider doing this via polymorphism
				if (factor instanceof MockEcoFactor)
					return 1;
				return 0;
			}
			return 0;
		}

		/**
		 * @return 0.25.
		 * 
		 * @see com.rubecula.darwin.domain.fitness.Fitness_#bandwidth(String)
		 */
		@Override
		protected double bandwidth(final String key) {
			return 0.2;
		}

		/**
		 * Scale down the eco factor by a factor of 10 before comparing with the
		 * soot equivalent.
		 * 
		 * @see com.rubecula.darwin.domain.fitness.Fitness_#scaleFactor(String)
		 */
		@Override
		protected double scaleFactor(final String key) {
			return 0.1;
		}

	}
}
