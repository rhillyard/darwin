package net.sf.darwin.platform.evolution;

import java.io.PrintWriter;
import java.util.Collection;

import net.sf.darwin.api.base.Registry_;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Variant;

public class MockRegistry extends Registry_<Object,Object,Object> {

	/**
	 * @param writer
	 *            the writer to which output will be sent.
	 * 
	 */
	public MockRegistry(final PrintWriter writer) {
		super();
		this._writer = writer;
	}

	/**
	 * Note the births
	 * 
	 * @see com.rubecula.darwin.domain.world.Registry_#registerBirths(Colony,
	 *      java.util.Collection)
	 */
	@Override
	public void registerBirths(final Colony<Object,Object,Object> colony, final Collection<Organism<Object,Object,Object>> neonates) {
		int carbonaria = 0;
		int typica = 0;

		for (final Organism organism : neonates) {
			final Phenotype phenotype = organism.getPhenotype();
			final Variant variant = phenotype.getTrait(MockEnum.ID).getVariant();
			final Object value = variant.getValue();
			if (value.equals(MockEnum.Carbonaria))
				carbonaria++;
			else if (value.equals(MockEnum.Typica))
				typica++;
		}

		this._writer
				.println("Births for population: " + colony.getIdentifier() + ": " + neonates.size() + ", typica: " + typica + ", carbonaria: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						+ carbonaria);
	}

	private final PrintWriter _writer;

}
