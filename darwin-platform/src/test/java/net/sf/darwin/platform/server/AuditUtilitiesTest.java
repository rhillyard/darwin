/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: AuditUtilitiesTest.java
 * Created on Nov 13, 2009
 * @version $Revision: 1.15 $
 */

package net.sf.darwin.platform.server;

import org.junit.Ignore;

/**
 * @author Robin Hillyard
 * 
 *         TODO re=implement this test suite.
 * 
 */
@Ignore
public class AuditUtilitiesTest {

	//	private static final String S_FORM_STRING = "formString"; //$NON-NLS-1$

	// /**
	// * @author Robin Hillyard
	// *
	// */
	// @ToString(allFields = true, allBeanMethods = false)
	// public static class TestLocus extends Locus_ {
	// @ToString(omit = true)
	// private static final long serialVersionUID = -2914282383315810255L;
	//
	// /**
	// * @param identifier
	// */
	// public TestLocus(final String identifier) {
	// super(identifier);
	// }
	//
	// /**
	// * @see net.sf.darwin.core.Locus#add(net.sf.darwin.core.Allele, int)
	// */
	// @Override
	// public String add(final Allele allele, final int frequency) {
	// return putAllele(allele, frequency);
	// }
	//
	// /**
	// * @return an array of Alleles
	// */
	// @ToString(child = true, detail = 1, showClass = true)
	// public Allele[] getAlleleArray() {
	// return getAlleleMap().values().toArray(new Allele[] {});
	// }
	//
	// /**
	// * @see net.sf.darwin.api.base.Locus_#getAlleleMap()
	// */
	// @Override
	// @ToString(child = true, detail = 1, priority = 0)
	// public Map<String, Allele> getAlleleMap() {
	// return super.getAlleleMap();
	// }
	//
	// /**
	// * @return the set of Alleles
	// */
	// @ToString(child = true, detail = 1)
	// public Collection<Allele> getAlleleSet() {
	// return getAlleleMap().values();
	// }
	//
	// /**
	// * @see net.sf.darwin.core.Locus#pickAllele()
	// */
	// @Override
	// public String pickAllele() {
	// return null;
	// }
	//
	// /**
	// * @see net.sf.darwin.api.base.Locus_#getAlleles()
	// */
	// @ToString(omit = true)
	// @Override
	// public Map<Allele, Integer> getAlleles() {
	// return super.getAlleles();
	// }
	//
	// @SuppressWarnings("unused")
	// @ToString()
	//		private final String forTestingOnly = "forTestingOnly"; //$NON-NLS-1$
	//
	// }
	//
	// /**
	// *
	// */
	// @ToString(allFields = true)
	// public static class TestTaxon extends Taxon_ {
	// /**
	// *
	// */
	// @ToString
	// private static final long serialVersionUID = 99L;
	//
	// /**
	// * @param identifier
	// * @param realm
	// * @param genomic
	// * @param phenome
	// * @param censusTaker
	// * @param mortality
	// * @param chooser
	// * @param fecundity
	// * @param junk
	// * XXX
	// */
	// public TestTaxon(final String identifier, final Realm realm, final
	// Genomic genomic, final Phenome phenome,
	// final Census censusTaker, final Mortality mortality, final MateChoice
	// chooser, final Fecundity fecundity,
	// final String junk) {
	// super(identifier, realm, genomic, phenome, censusTaker, mortality,
	// chooser, fecundity);
	// this._junk = junk;
	// }
	//
	// @SuppressWarnings("unused")
	// private final String _junk;
	// }
	//
	// /**
	// * @param locus
	// * @return the object as a String but surrounded by brackets.
	// */
	// public static String formString(final Locus locus) {
	// return OPEN_BRA + locus.getIdentifier() + CLOSE_BRA;
	// }
	//
	// /**
	// * @throws java.lang.Exception
	// */
	// @Before
	// public void setUp() throws Exception {
	// this._beanPot = new BeanContainer_BeanPot();
	//		this._beanPot.setConfigurationByResource(LocusTest.class, "testLocus.xml"); //$NON-NLS-1$
	// this._beanPot.setDebug(true);
	// this._beanPot.configure();
	// }
	//
	// /**
	// * Test method for
	// * {@link ToStringUtils#toString(java.lang.Object, boolean, boolean,
	// boolean, boolean, boolean, String, boolean, int, int)}
	// * .
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAudit0() {
	//		final Locus locus = (Locus) this._beanPot.getBean("Locus"); //$NON-NLS-1$
	// final String audit = ToStringUtils.toString(locus,
	// ToStringUtils.createDefaultInternalDetail(new Detail(true, true, true,
	// true, true, null, false, 10, Integer.MAX_VALUE)));
	// System.out.println(audit);
	// assertEquals(
	// "audit",
	// "Locus_Triversian Wing Color <alleleMap={Melanism:M, melanism:m}, frequencies={Melanism:1, melanism:4}, parent=No Sex>",
	// audit);
	// }
	//
	// /**
	// * Test method for
	// * {@link ToStringUtils#toString(java.lang.Object, boolean, boolean,
	// boolean, boolean, boolean, String, boolean, int, int)}
	// * .
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAudit1() {
	//		final Locus locus = (Locus) this._beanPot.getBean("Locus"); //$NON-NLS-1$
	// final String audit = ToStringUtils.toString(locus,
	// ToStringUtils.createDefaultInternalDetail(new Detail(true, true, true,
	// true, true, "  ", false, 10, Integer.MAX_VALUE)));
	// System.out.println(audit);
	// assertEquals("audit", "Locus_Triversian Wing Color" +
	// ToStringUtils.NEWLINE + "  alleleMap={" + ToStringUtils.NEWLINE
	// + "    Melanism:M" + ToStringUtils.NEWLINE + "    melanism:m}" +
	// ToStringUtils.NEWLINE + "  frequencies={"
	// + ToStringUtils.NEWLINE + "    Melanism:1" + ToStringUtils.NEWLINE +
	// "    melanism:4}" + ToStringUtils.NEWLINE
	// + "  parent=No Sex", audit);
	// }
	//
	// /**
	// * Test method for
	// * {@link ToStringUtils#toString(java.lang.Object, boolean, boolean,
	// boolean, boolean, boolean, String, boolean, int, int)}
	// * .
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAudit2() {
	// final Locus locus1 = new TestLocus("test");
	//		final Locus locus2 = (Locus) this._beanPot.getBean("Locus"); //$NON-NLS-1$
	// ((Locus_) locus1).setAlleles(((Locus_) locus2).getAlleles());
	// final String audit = ToStringUtils.toString(locus1,
	// ToStringUtils.createDefaultInternalDetail(new Detail(true, true,
	// true, true, true, "  ", false, 10, Integer.MAX_VALUE)));
	// assertEquals("audit", "TestLocus test" + ToStringUtils.NEWLINE +
	// "  forTestingOnly=forTestingOnly"
	// + ToStringUtils.NEWLINE + "  alleleArray=Allele_Dominance [" +
	// ToStringUtils.NEWLINE + "    M"
	// + ToStringUtils.NEWLINE + "    m]" + ToStringUtils.NEWLINE +
	// "  alleleSet=[" + ToStringUtils.NEWLINE + "    M"
	// + ToStringUtils.NEWLINE + "    m]" + ToStringUtils.NEWLINE +
	// "  frequencies={" + ToStringUtils.NEWLINE
	// + "    Melanism:1" + ToStringUtils.NEWLINE + "    melanism:4}" +
	// ToStringUtils.NEWLINE + "  alleleMap={"
	// + ToStringUtils.NEWLINE + "    Melanism:M" + ToStringUtils.NEWLINE +
	// "    melanism:m}", audit);
	// }
	//
	// /**
	// * Test method for
	// * {@link ToStringUtils#toString(java.lang.Object, boolean, boolean,
	// boolean, boolean, boolean, String, boolean, int, int)}
	// * .
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAudit3() {
	// final Locus locus1 = new TestLocus("test");
	//		final Locus locus2 = (Locus) this._beanPot.getBean("Locus"); //$NON-NLS-1$
	// ((Locus_) locus1).setAlleles(((Locus_) locus2).getAlleles());
	// final String audit = locus1.toString(new Detail(true, true, true, false,
	// true, null, false, 10, Integer.MAX_VALUE));
	// assertEquals(
	// "audit",
	// "TestLocus test <forTestingOnly=forTestingOnly, alleleArray=Allele_Dominance [M, m], alleleSet=[M, m], frequencies={Melanism:1, melanism:4}, alleleMap={Melanism:M, melanism:m}>",
	// audit);
	// }
	//
	// /**
	// */
	// @Test
	// public void testAudit4() {
	// final Integer i99 = Integer.valueOf(99);
	// assertEquals(i99.toString(), i99.toString(), ToStringUtils.toString(i99,
	// ToStringUtils
	// .createDefaultInternalDetail(new Detail(true, true, true, false, false,
	// null, false, 0, Integer.MAX_VALUE))));
	// }
	//
	// /**
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAudit5() {
	// final EcoFactor factor = new EcoFactor_Humboldtian("test factor");
	// final Environment environment = new Environment_Muirian(new
	// Realm_Wallacian());
	// environment.addFactor(factor);
	// assertEquals("test factor",
	// "EcoFactor_Humboldtian test factor <attribute=test factor>",
	// ToStringUtils.toString(factor,
	// ToStringUtils.createDefaultInternalDetail(new Detail(true, true, true,
	// false, false, null, false, 0,
	// Integer.MAX_VALUE))));
	// }
	//
	// /**
	// * Test of
	// * {@link TestTaxon#toString(boolean, boolean, boolean, boolean, boolean,
	// String, boolean, int, int)}
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAudit8() {
	// final TestTaxon taxon = new TestTaxon("test", null, null, null, null,
	// null, null, null, "junk");
	// final String audit = taxon.toString(new Detail(true, true, true, false,
	// true, null, false, 10, Integer.MAX_VALUE));
	// assertEquals("audit", "TestTaxon test <junk=junk, serialVersionUID=99>",
	// audit);
	// }
	//
	// /**
	// * Test method for
	// * {@link ToStringUtils#toString(java.lang.Object, boolean, boolean,
	// boolean, boolean, boolean, String, boolean, int, int)}
	// * .
	// *
	// * @throws Exception
	// */
	// @SuppressWarnings("nls")
	// @Test
	// public void testAuditValue() throws Exception {
	//		final Locus locus = (Locus) this._beanPot.getBean("Locus"); //$NON-NLS-1$
	// assertEquals("audit class but no detail", "Locus_Triversian Wing Color",
	// testAuditValue(locus, true, true, false, "", false, 0,
	// Integer.MAX_VALUE));
	// assertEquals("audit no detail", "Wing Color",
	// testAuditValue(locus, false, true, false, null, false, 0,
	// Integer.MAX_VALUE));
	// assertEquals("audit no detail", "W...", testAuditValue(locus, false,
	// true, false, null, false, 0, 1));
	// assertEquals("audit no recurse",
	// "Locus_Triversian Wing Color <frequencies={Melanism:1, melanism:4}>",
	// testAuditValue(locus, true, true, true, null, false, 10,
	// Integer.MAX_VALUE));
	// assertEquals("audit no recurse newlines", "Locus_Triversian Wing Color" +
	// ToStringUtils.NEWLINE + "  frequencies={"
	// + ToStringUtils.NEWLINE + "    Melanism:1" + ToStringUtils.NEWLINE +
	// "    melanism:4}",
	// testAuditValue(locus, true, true, true, "", false, 10,
	// Integer.MAX_VALUE));
	// assertEquals("audit recurse", "Locus_Triversian Wing Color" +
	// ToStringUtils.NEWLINE + "  alleleMap={"
	// + ToStringUtils.NEWLINE + "    Melanism:M" + ToStringUtils.NEWLINE +
	// "    melanism:m}" + ToStringUtils.NEWLINE
	// + "  frequencies={" + ToStringUtils.NEWLINE + "    Melanism:1" +
	// ToStringUtils.NEWLINE + "    melanism:4}",
	// testAuditValue(locus, true, true, true, "", true, 10,
	// Integer.MAX_VALUE));
	// assertEquals("audit recurse max 1", "Locus_Triversian Wing Color" +
	// ToStringUtils.NEWLINE + "  alleleMap={"
	// + ToStringUtils.NEWLINE + "    Melanism:M, ...}" + ToStringUtils.NEWLINE
	// + "  frequencies={"
	// + ToStringUtils.NEWLINE + "    Melanism:1, ...}",
	// testAuditValue(locus, true, true, true, "", true, 1, Integer.MAX_VALUE));
	// assertEquals("audit recurse max 1", "Locus_Triversian Wing Color" +
	// ToStringUtils.NEWLINE + "  alleleMap={2}"
	// + ToStringUtils.NEWLINE + "  frequencies={2}",
	// testAuditValue(locus, true, true, true, "", true, 0, Integer.MAX_VALUE));
	// }
	//
	// /**
	// * @throws Exception
	// */
	// @Test
	// @SuppressWarnings("nls")
	// public void testSeparator() throws Exception {
	// assertNull("null separator", testSeparator(null, null));
	// final String random = new String(new Random_Standard().nextInt() + "");
	// assertEquals("null prefix", random, testSeparator(null, random));
	// assertEquals("space separator", ToStringUtils.NEWLINE + random,
	// testSeparator(random, null));
	// }
	//
	// @SuppressWarnings({})
	// private String testAuditValue(final Object value, final boolean
	// showClass, final boolean showIdentifier,
	// final boolean showProperties, final String prefix, final boolean recurse,
	// final int maxElements, final int maxChars)
	// throws Exception {
	// return testAuditValueFormMethod(value, showClass, showIdentifier,
	// showProperties, prefix, recurse, maxElements, maxChars,
	// null);
	// }
	//
	// /**
	// * @param value
	// * @param showClass
	// * @param showIdentifier
	// * @param showProperties
	// * @param prefix
	// * @param recurse
	// * @param maxElements
	// * @param maxChars
	// * @param formStringMethod
	// * @return
	// * @throws InvocationTargetException
	// * @throws NoSuchMethodException
	// * @throws IllegalAccessException
	// */
	// @SuppressWarnings({ "boxing", "nls" })
	// private String testAuditValueFormMethod(final Object value, final boolean
	// showClass, final boolean showIdentifier,
	// final boolean showProperties, final String prefix, final boolean recurse,
	// final int maxElements, final int maxChars,
	// final Method formStringMethod) throws InvocationTargetException,
	// NoSuchMethodException, IllegalAccessException {
	// final Object[] paramObjects = new Object[] { value, showClass,
	// showIdentifier, showProperties, prefix, recurse,
	// maxElements, maxChars, formStringMethod };
	// final Class<?>[] paramClasses = new Class<?>[] { Object.class,
	// boolean.class, boolean.class, boolean.class, String.class,
	// boolean.class, int.class, int.class, Method.class };
	// return (String) TestBase.invokeStaticMethod(TEST_CLAZZ, "toStringValue",
	// paramClasses, paramObjects);
	// }
	//
	// /**
	// * @param prefix
	// * XXX
	// * @param alternate
	// * XXX
	// * @return
	// * @throws InvocationTargetException
	// * @throws NoSuchMethodException
	// * @throws IllegalAccessException
	// */
	// @SuppressWarnings("nls")
	// private String testSeparator(final String prefix, final String alternate)
	// throws InvocationTargetException,
	// NoSuchMethodException, IllegalAccessException {
	// return (String) TestBase.invokeStaticMethod(TEST_CLAZZ, "separator", new
	// Class<?>[] { String.class, String.class },
	// new Object[] { prefix, alternate });
	// }
	//
	// /**
	// *
	// */
	//	private static final String CLOSE_BRA = "]"; //$NON-NLS-1$
	//
	// /**
	// *
	// */
	//	private static final String OPEN_BRA = "["; //$NON-NLS-1$
	//
	// /**
	// *
	// */
	// private static final Class<ToStringUtils> TEST_CLAZZ =
	// ToStringUtils.class;
	//
	// private BeanContainer _beanPot;

}
