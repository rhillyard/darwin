/**
 * DARWIN Genetic Algorithms Framework Project.
 * Copyright (C) 2003,2007, 2009  Rubecula Software, LLC.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Module: MockCensus.java
 * Created on Jan 31, 2007
 * @version $Revision: 1.34 $
 */

package net.sf.darwin.platform.evolution;

import java.awt.Color;
import java.awt.Point;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import net.sf.darwin.api.base.Census_Darwinian;
import net.sf.darwin.api.base.SinkCensus;
import net.sf.darwin.core.Censusible;
import net.sf.darwin.core.Colony;
import net.sf.darwin.core.DarwinException;
import net.sf.darwin.core.Environment;
import net.sf.darwin.core.Gene;
import net.sf.darwin.core.Genes;
import net.sf.darwin.core.Individual;
import net.sf.darwin.core.Locus;
import net.sf.darwin.core.Organism;
import net.sf.darwin.core.Phenotype;
import net.sf.darwin.core.Variant;
import net.sf.darwin.core.Visualizable;
import net.sf.darwin.ui.base.Avatar;
import net.sf.darwin.ui.base.VisualizationFactory;
import net.sf.darwin.ui.factory.AvatarFactory;

import org.apache.commons.math.random.RandomGenerator;

/**
 * Implementation of {@link SinkCensus} specifically for the PepperedMoth
 * example and which concentrates on showing the mean wing color (and numbers of
 * organisms with each trait).
 * 
 * @author Robin Hillyard
 * 
 */
public final class MockCensus extends Census_Darwinian {

	/**
	 * @param printWriter
	 */
	public MockCensus(final PrintWriter printWriter) {
		super(printWriter);
	}

	/**
	 * 
	 * @see net.sf.darwin.api.base.domain.world.Census_Darwinian#census(Censusible,
	 *      int, Object)
	 */
	@Override
	public void census(final Censusible individual, final int depth, final Object context) {
		final boolean summaryOnly = false; // set to false if you want more
		// detail
		if (individual instanceof Colony) {
			final Colony colony = (Colony) individual;
			final String identifier = colony.getIdentifier();
			// if (identifier.equalsIgnoreCase("Experimental")) {
			final Environment environment = colony.getEnvironment();
			// TODO consider doing this via polymorphism
			if (environment instanceof MockEnvironment) {
				getPrintWriter()
						.println(
								"MockCensus for Colony: " + identifier + " with soot value: " + ((MockEnvironment) environment).getSootValue()); //$NON-NLS-1$ //$NON-NLS-2$
				getPrintWriter().println(
						"Generation: " + colony.getPopulation().getTaxon().getGeneration() + " in context: " + context); //$NON-NLS-1$ //$NON-NLS-2$
			}
			MockCensus.outputWingColorFrequencies(getPrintWriter(), colony, summaryOnly);
			getPrintWriter().println(""); //$NON-NLS-1$
			// }
		} else
			super.census(individual, depth, context);
	}

	public static class MockVisualizationFactory implements VisualizationFactory {

		/**
		 * Public primary constructor.
		 * 
		 * @param random
		 */
		public MockVisualizationFactory(final RandomGenerator random) {
			super();
			this._random = random;
			this.width = 80;
			this.height = 80;
		}

		/**
		 * 
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#getHeight()
		 */
		@Override
		public int getHeight() {
			return this.height;
		}

		/**
		 * 
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#getWidth()
		 */
		@Override
		public int getWidth() {
			return this.width;
		}

		/**
		 * 
		 * XXX properties is unused.
		 * 
		 * @return the result of invoking
		 *         {@link AvatarFactory#makeAvatar(Individual, Point, Color)}
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#makeAvatar(Individual,
		 *      java.util.Map)
		 */
		@Override
		public Avatar makeAvatar(final Individual individual, final Map<String, Object> properties) {
			final Point location = makeLocation(individual);
			final Color color = makeColor(individual);
			return AvatarFactory.makeAvatar(individual, location, color);
		}

		/**
		 * 
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#makeColor(Individual)
		 */
		@Override
		public Color makeColor(final Individual individual) {
			final Visualizable visualizable = individual.getVisualizable();
			// TODO consider doing this via polymorphism
			if (visualizable instanceof Colony) { // will be true
				// get the color (0: white, 3: black)
				if (individual instanceof Organism) { // will be true
					final MockEnum wingColor = MockVisualizationFactory.getWingColor((Organism) individual);
					final int ordinal = wingColor.ordinal();
					final int colors = MockEnum.values().length - 1;
					// final double sootEquivalent = wingColor.sootEquivalent();
					return MockVisualizationFactory.convertToGrayLevel(ordinal, colors);
				}
				return Color.red;
			}
			return Color.red;
		}

		/**
		 * 
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#makeLocation(Individual)
		 */
		@Override
		public Point makeLocation(final Individual individual) {
			final int x = (int) (getWidth() * this._random.nextFloat());
			final int y = (int) (getHeight() * this._random.nextFloat());
			return new Point(x, y);
		}

		/**
		 * 
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#setHeight(int)
		 */
		@Override
		public void setHeight(final int height) {
			this.height = height;
		}

		/**
		 * 
		 * @see com.rubecula.darwin.visualization.VisualizationFactory#setWidth(int)
		 */
		@Override
		public void setWidth(final int width) {
			this.width = width;
		}

		private int width;

		private int height;

		final private RandomGenerator _random;

		/**
		 * @param greyValue
		 *            the color in range 0=white ... <code>max</code>=black
		 * @param max
		 *            the maximum color value
		 * @return a shade of grey corresponding to the <code>geryValue</code>
		 *         given.
		 * 
		 *         XXX consider moving this method
		 */
		public static Color convertToGrayLevel(final float greyValue, final float max) {
			final float whiteness = 1 - greyValue / (max - 1);
			return new Color(whiteness, whiteness, whiteness);
		}

		/**
		 * TODO Consider moving this somewhere else.
		 * 
		 * @param organism
		 * @return the wing color as a string.
		 */
		public static MockEnum getWingColor(final Organism organism) {
			final Phenotype phenotype = organism.getPhenotype();
			if (phenotype != null) {
				final Variant variant = phenotype.getTrait(MockEnum.ID).getVariant();
				return (MockEnum) variant.getValue();
			}
			throw new DarwinException("getWingColor(): organism already dead: " + organism); //$NON-NLS-1$
		}

	}

	/**
	 * Private static method to output the wing color frequencies for census
	 * purposes.
	 * 
	 * XXX consider moving this elsewhere.
	 * 
	 * @param out
	 *            the output print stream.
	 * @param colony
	 *            the population to be censused.
	 * @param summaryOnly
	 *            if true then the total and average color are shown only.
	 */
	private static <E,U,W> void outputWingColorFrequencies(final PrintWriter out, final Colony<E, U,W> colony, final boolean summaryOnly) {

		final Map<Object, Integer> frequenciesTrait = new HashMap<>();
		final Map<Object, Integer> frequenciesAllele = new HashMap<>();
		final Map<Object, Integer> frequenciesAge = new HashMap<>();

		double totalFrequenciesTrait = 0;
		int totalFrequenciesAllele = 0;
		int totalFrequenciesAge = 0;

		int totalOrganisms = 0;

		synchronized (colony) {
			for (final Organism<E, U,W> organism : colony.getOrganisms()) {
				if (organism.isViable() || organism.getAge() == 0) {
					final double traitValue = MockCensus.updateTraitFrequencies(frequenciesTrait, organism);
					totalFrequenciesTrait += traitValue;
					final double alleleValueSum = MockCensus.updateAlleleFrequencies(frequenciesAllele, organism);
					totalFrequenciesAllele += alleleValueSum;
					final double ageValueSum = MockCensus.updateAgeFrequencies(frequenciesAge, organism);
					totalFrequenciesAge += ageValueSum;
					totalOrganisms++;
				}
			}
		}

		if (!summaryOnly)
			out.print(MessagesPepperedMoth.getString("PepperedMoth.10")); //$NON-NLS-1$

		for (final Object key : new TreeSet<>(frequenciesTrait.keySet())) {
			final Integer frequency = frequenciesTrait.get(key);
			final int colorFrequency = frequency.intValue();
			if (!summaryOnly)
				out.print(key + MessagesPepperedMoth.getString("PepperedMoth.11") + colorFrequency); //$NON-NLS-1$
			if (!summaryOnly)
				out.print(MessagesPepperedMoth.getString("PepperedMoth.12")); //$NON-NLS-1$			
		}
		if (!summaryOnly)
			out.println(MessagesPepperedMoth.getString("PepperedMoth.13")); //$NON-NLS-1$

		final double meanColor = totalFrequenciesTrait / totalOrganisms;
		final double meanAlleleVal = (double) totalFrequenciesAllele / totalOrganisms / 2;
		final double meanAgeVal = (double) totalFrequenciesAge / totalOrganisms;

		out.println(totalOrganisms
				+ MessagesPepperedMoth.getString("PepperedMoth.14") + meanColor + MessagesPepperedMoth.getString("PepperedMoth.15") + meanAlleleVal + " and mean age: " + meanAgeVal); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	}

	private static double updateAgeFrequencies(final Map<Object, Integer> frequencies, final Organism organism) {
		final int age = organism.getAge();
		MockCensus.updateFrequencyMap(frequencies, Integer.valueOf(age));
		return age;
	}

	/**
	 * Update the frequencies of the alleles of the non-sex genes
	 * 
	 * @param frequencies
	 * @param organism
	 * @return
	 */
	private static double updateAlleleFrequencies(final Map<Object, Integer> frequencies, final Organism organism) {
		double result = 0;
		final Genes genome = organism.getGenome();
		for (int i = 0; i < genome.size(); i++) {
			final Gene gene = genome.getGene(i);
			final Locus locus = gene.getLocus();
			final String identifier = locus.getIdentifier();

			if (!identifier.equals(Locus.ID_SEX)) {
				final int index0 = 0;
				final Boolean value0 = (Boolean) gene.getAllele(index0).getValue();
				final int index1 = 1;
				final Boolean value1 = (Boolean) gene.getAllele(index1).getValue();
				MockCensus.updateFrequencyMap(frequencies, value0);
				MockCensus.updateFrequencyMap(frequencies, value1);
				result += ((value0.booleanValue() ? 1 : 0) + (value1.booleanValue() ? 1 : 0));
			}
		}
		return result;
	}

	private static void updateFrequencyMap(final Map<Object, Integer> frequencies, final Object value) {
		Integer frequency = frequencies.get(value);
		if (frequency == null)
			frequency = Integer.valueOf(0);
		frequency = Integer.valueOf(frequency.intValue() + 1);
		frequencies.put(value, frequency);
	}

	private static double updateTraitFrequencies(final Map<Object, Integer> frequencies, final Organism organism) {
		final MockEnum wingColor = MockVisualizationFactory.getWingColor(organism);
		final double sootEquivalent = wingColor.sootEquivalent();
		updateFrequencyMap(frequencies, wingColor);
		return sootEquivalent;
	}

}