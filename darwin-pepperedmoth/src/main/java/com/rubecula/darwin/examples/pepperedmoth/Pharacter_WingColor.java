package com.rubecula.darwin.examples.pepperedmoth;

import net.sf.darwin.api.impl.Pharacter_Fisherian;

public class Pharacter_WingColor extends Pharacter_Fisherian<WingColor> {

	public Pharacter_WingColor(final String identifier) {
		super(identifier);
	}

	private static final long serialVersionUID = 4889060146498102323L;

}
